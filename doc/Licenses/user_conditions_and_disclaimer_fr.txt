Conditions d’utilisation et clause de non-responsabilité

En utilisant tout outil et/ou logiciel lié au modèle de culture STICS, vous acceptez que, 
même si toutes les précautions sont prises pour assurer la fiabilité de la suite logicielle
actuelle, les membres de l'équipe projet STICS ainsi que leur organismes d'affiliation 
(INRAE, CIRAD, Agriculture Canada, Gembloux University) déclinent toutes garanties,
qu'elles soient déclarées ou implicites, au sujet de ses performances, de sa fiabilité,
de sa précision de prédiction, de son exhaustivité ou de sa pertinence vis-à-vis de tout
objectif particulier. En conséquence, ils déclinent toute responsabilité (incluant sans 
restriction toute responsabilité par négligence) pour tous les coûts, pertes (données, temps,
pécunières), dommages (incluant des dommages directs, indirects ou accessoires) et tous 
les coûts qui pourraient être engendrés résultants de l'utilisation de tout outil lié au modèle
de cultureSTICS (code, fichiers exécutables, paquets R) de quelque façon que ce soit et quelle
qu'en soit la raison. Ces clauses d'usage s'appliquent à toutes les versions antérieures et futures 
des différents outils développés par les membres de l'équipe STICS, depuis sa création en 1996.
Tout autre contexte d'utilisation de ces outils doit être soumis à un accord spécifique entre les
parties concernées.

Janvier 2024
