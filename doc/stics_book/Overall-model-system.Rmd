# Overall description of the modelled system {#overall-model-system}

<!-- inserting authors list -->
```{r, echo=FALSE, comment="", results='asis'}
insert_chap_authors("overall-model-system")
insert_chap_reviewers("overall-model-system")
```

## Conceptual Framework {#conceptual-framework}

### Introduction

STICS specifications were set out precisely in 1996 (§ \@ref(SticsIntro)). In short, they were designed to create a single model for different crops that would be general, robust, simple, operational and flexible, and which could describe the main interactions within the soil-crop-atmosphere and be upscaled in time and space. This chapter explains the STICS conceptual framework and how it translates in terms of programming design. 

### Simulated processes

The STICS modellers built a deterministic functional process-based soil-crop model to describe the energy, water, C and N dynamical balances across the crop cycle and during fallow periods. The aim was to allow users to predict a large range of agronomical and environmental variables of interest. As a result, STICS simulates the processes (using 1D description) occurring in a cropping system at daily step over the soil profile and the whole plant cycle. The upper boundary of the simulated system is delimited by the low layer of the atmosphere (2 m height). This boundary is commonly characterised by standard weather variables (radiation, minimum and maximum temperatures, rainfall, reference evapotranspiration and possibly wind and humidity). The lower boundary corresponds to the soil/sub-soil interface where water and nutrients are leached (out of the crop rooting system). Figure \@ref(fig:fig-Overall-CropModel-structure) illustrate the conceptual framework of crop modelling which is based on the classic functional approach. 

<!--(ref:fig-Averall-CropModel-structure). Conceptual framework of a crop model, adapted from Bonhomme and Ruget (1991). -->

```{r fig-Overall-CropModel-structure, out.width='80%', fig.align='center', fig.cap='Schematic representation of a crop model, adapted from Bonhomme and Ruget (1991);* bioagressors can be accounted for thanks to STICS coupling', echo = FALSE}
knitr::include_graphics('data/Overall-system/CropModel_structure.jpg')
```

Phenology, which refers to the plant development, governs the crop cycle time period. Crop growth is driven by energy interception and results in plant carbon accumulation [@de_wit_simulation_1978]. Solar radiation absorbed by the foliage is transformed into aboveground biomass (energy conversion concept) <!--that is partitioned to the harvested organs when they become assimilate sinks--> whose a part is remobilised to the harvested organs when they become sinks. The crop nitrogen content depends on carbon accumulation in crop and mineral nitrogen availability in the soil (and N~2~ fixed from atmosphere for legumes). Depending on plant type, crop development is driven either by i) a thermal index (degree-days), ii) a photo-thermal index which also takes photoperiod into account or iii) a vernalo-photo-thermal index which also takes vernalisation into account. The development module drives the kinetics of the leaf area index ([$lai$](#var-lai-n)\index{lai(n)}) and the roots, which capture abiotic resources, and defines the harvested organ filling phase. Any water or nitrogen stresses will reduce leaf growth and biomass accumulation, and the degree of reduced growth is based on stress indices calculated by water and nitrogen balance modules. Other abiotic stresses, such as thermal (frost or high temperatures), trophic or waterlogging stresses are also taken into account, as limiting factors of growth and yield formation. The response function to environmental constraints can also integrate enzymatic activities, such as nitrate uptake. 

Crop organs are defined as plant compartments to witch  generic ecological functions are assigned. Here, the distinction between the structural and temporary pool of carbohydrates is a key point. Only the structural aspect is assigned to the leaves, stems, fruit, perennial reserves and roots. Conversely, the temporary reserves are not located into the crop organs, in order to support the model genericity for all crops, including annual cereals and perennial forage crops. The storage organs and harvested organs can be grains, fruits, tubers or even stems. The new STICS version 10 now considers the C and N fluxes to and from temporary and perennial storage organs.

The model simulates the water and nitrogen elements balances in soil, to assess plant availability and environmental losses (§ \@ref(overall-simulated-cycles)). Their descriptions are based on the classic compartmental approach, which defines different pools in the system, their evolution and their relationships (input and output functions; see for instance Figure \@ref(fig:fig-CNbal-decompores) from [@nicolardot_simulation_2001]. The functions encompass physical, physic-chemical and bio-chemical processes. The compartment size depends on the process and ranges from centimetric (elementary layer) for solute content and transfers to decimetric (pedologic layer) for defining soil properties .

The processes are described in a way which can be qualified of functional and process-based or analogic, which aims to favour a simple integration of all the processes interacting in the soil-crop system, at the crop cycle scale as well as the crop rotation scale. The most emblematic illustrations of this conceptual choice are:

-	The interception efficiency of the total solar radiation uses Beer's law, as an exponential response of the shadowing effect to leaf area index([$lai$](#var-lai-n)\index{lai(n)}), in the case of a homogeneous canopy.

-	The conversion of photo synthetically active radiation (PAR) in aerial biomass is based on the concept of radiation use efficiency (RUE), assuming a specific potential linear response of biomass accumulation, at a stable radiation level when there is no carbohydrate remobilisation [@monteith_solar_1972]; the simulated RUE depends on radiation level, water stress and N stress, CO2 concentration and the crop stage. 

-	The nitrogen uptake and accumulation in the plant is based on the concept of N dilution curve in the aerial biomass of the canopy, in the case of vegetative growth, for either an isolated plant or dense canopy [@lemaire_n_1997]. This allows the user to calculate the crucial concept of nitrogen nutrition index (NNI) which drives the N status of the crop in the vegetative phase.

-	Fruit filling includes the concept of dynamic harvest index, as proposed by @spaeth_linear_1985, for crop species with determinate development.

-	The downward soil water storage, when daily rainfall  exceeds the actual evapotranspiration amount, uses the tipping bucket concept, at the elementary layer infiltration scale, based on the hypothesis of infinite hydric conductivity.

-	Nitrate leaching is simulated using a mixing cell model, with similar results to those of the resolution of the convective-dispersive equations (Darcy's law and Richard's law),  except for soils with low hydric conductivity, with easier soil parameterisation and a much lower time of calculation [@van_der_ploeg_late_1995].

-	Soil C and N mineralisation are simulated using a compartmental approach, with only three pools of organic matter, which resembles the AMG model [@clivot_modeling_2019]; this compartmental approach allows an independent parameterisation linked to a residue typology [@nicolardot_simulation_2001]. This approach is based on the crucial concept of C/N ratio of residues which drives their decomposition and humification rates, and determines the C and N mineralisation rate of the residues.

These processes are implemented by default in the standard STICS version; if their basic hypothesis is not met, an alternative process, involving a more complex conceptual approach, is described and workable. For instance, for a heterogeneous canopy (e.g. row crops or intercropping systems), the simple 'big leaf' model associated with the Beer's law can be replaced by a more complex energy balance based on crop architecture and a resistive approach. This alternative process allows determine the sunlit and shaded leaf layers of the bispecific intercrop and then the light capture differentiation between these two layers and furthermore, using a spatial discretisation along the crop inter-row (§ \@ref(Microclimate-energy-budget-crop-temperature)).

The effect of crop management on the dynamics of the soil-crop-microclimate systems is also given particular attention (§ \@ref(SC-manage)). The reason is that crop specificities influence both ecophysiology and crop management (e.g. accounting for the various forms of forage cutting, fertilizer composition, plastic or crop residue mulching, etc.).

Finally, STICS model is either functional or process-based  at daily scale; it is mechanistic at the crop cycle scale, since it deals with the main interactions between the system components. It can design the emergent properties at the crop cycle and crop rotation scales. 

### Simulated cycles {#overall-simulated-cycles}

Crop development and crop growth are considered as a combination of responses to potential fluxes of energy or chemical elements  (Figure \@ref(fig:fig-Overall-CropModel-structure)). They are limited by environmental constraints which depend on physical, chemical and biological cycles.

With regard to physical cycles, the model first integrates the physical driving factors related to the climatic cycles of temperature and solar radiation (which have an annual period). The model then simulates the effective temperature, energy balance and water storage cycles in the soil and crop. Peculiarly, the model mobilises time discretisation at hourly scale for precise purposes: the daily cycle of the solar azimuth when using the resistive approach option for row crops (§ \@ref(MicroClimate-radiation-transfers)), the hourly climate state variables when using the Shuttleworth-Wallace submodel (§ \@ref(MicroClimate-canopy-moisture)), the water-filled pore space of elementary layers when simulating either nitrification or denitrification of soil nitrate (§ \@ref(CNbal-hourly-wfps)) and the soil surface ammonia concentration when simulation NH~3~ volatilisation (§ \@ref(CNbal-volat)). 

The biological cycles include those pertaining to the crop(s), which may cover one or more cycles a year, and those related to the soil microbial biomass which depends on the decomposing residues and soil organic matter. In established perennial crops, a new crop cycle resumes according to either the imposed or calculated date of crop regrowth. In consequence, the simulated RUE encompasses both aerial biomass and perennial reserves building as opposed fine root system.

The biogeochemical cycles correspond to those for carbon and nitrogen. Each part of their cycles occuring in the soil-crop-atmosphere system is modelled based on several kinetic principles driven by the system forcing variables and according to the law of conservation of mass. The simulated C balance at the soil-crop level is almost as complete as compared to their description in literature [@hyvonen_likely_2007]: all net C fluxes are simulated except for CH~4~ emission and dissolved organic or inorganic C in water drainage; however, only the difference between the gross primary production and its respective gross respiration is simulated. The N fluxes are complete, allowing to simulate positive or negative net N balance in function of the cropping system management and pedoclimatic conditions [@autret_long-term_2020]. 

### Additionnal specifications about code and output availability.

STICS has evolved to deal with a larger range of issues that require new skills [@beaudoin_modelisation_2019]. The latter required the following supplementary specifications: 

-	Code modularity (§ \@ref(overall-relationship-processes))

-	The options to force certain intermediate variables such as [$lai$](#var-lai-n)\index{lai(n)}, potential evapotranspiration (PET), phenological stages (e.g. emergence, harvest), etc... 

-	The availability of all state and intermediate variables of agronomical, ecophysiological or biogeochemical interest; note that crop yield refers to the biological yield, not farm yield (no mechanical losses simulated); these variables can now be used to calculate the greenhouse gas (GHG) balance due to C-N cycles (except in grazed grasslands or flooded rice field, since CH~4~ emissions are not yet simulated). 

## STICS validity domain {#overall-stics-validity-domain}

### General case {#overall-general-case}

The modelled system is the cycle of a crop (or bi-specific intercrop) growing either homogeneously or in row, in a given homogeneous soil under given homogeneous techniques and climatic conditions (§ \@ref(introduction-purpose)). The STICS validity domain can be considered as either  potential or actual. 

From a theoretical point of view, the potential validity domain is the intersection of the validity domains of all the  algorithms used; it depends on the simulated processes and the  options activated by the user. For instance, if the canopy is heterogeneous, the radiative transfer model must be used instead of Beer's law relevant only for homogeneous stands; if hydric conductivity is weak, simulation of water status in the macroporisity compartment is also required. Users can activate these more complex options to expand the validity domain, but they must show more caution when parameterising the model and, where possible, with evaluating the model's performance.

From a practical point of view, the actual validity domain covers all the situations for which the model has been shown to produce acceptable results. This empirical concept is based on the model-use bibliometric (§ \@ref(ways-stics-application)). Soil-crop model performance depends on how realistic is the representations are as well as the reliability of the code, and the quality of the parameters [@loague_statistical_1991]. Users who feed in the data and choose the simulation options and some parameters, can have a significant impact on the validity domain [@confalonieri_uncertainty_2016; @wallach_chaos_2021].

The STICS application domain can also be extended in space and time, thanks to the model's intrinsic ability for either successive simulations or bi-specific intercropping systems (see the next two  sections) or by coupling the model with GIS (§ \@ref(WSA-Simulation-over-large-spatial-scales)).


### Case of the crop rotation {#crop-rotation}

The STICS soil-crop model can simulate functioning of cropping systems, at the crop rotation scale as well as over the long term, by linking successive elementary simulations and taking crop and fallow periods into consideration, either with or without re-initialization after each USM (continuous mode). In continuous mode, the state variables required as initial values are transferred from the previous simulation (Figure \@ref(fig:fig-STICS-Chaining-USM-simplified)). The  variables that are transferred between successive simulation relate to the crops (stage, temperature, lai, biomass and N content of shoots, root length, biomass and N content of roots, biomass and N content of storage organs), non-decomposed residues (biomass and C-N contents of either mulch or buried residues and dead roots) and the soil (water, NO~3~ and NH~4~ content, organic C-N content in topsoil). In addition, residue predictions from the previous simulation become inputs for the following simulation. The list of transferred variables between simulations is slightly longer than the list of initial variables for the first simulation, because some of them are not available, namely the microbial biomass and the residue-sphere (mulch and dead roots). Thus, chaining simulations in a continuous mode provides supplementary information about the system state. However, the risk of model drift over along time needs caution [@beaudoin_evaluation_2008].

Chaining simulations can show long-term trends and impacts in the cropping system. Some state variables such as soil carbon content or soil mineral content, are also variables that are important from an agro-environmental point of view. These variables can be used evaluate the long-term impact of agricultural practices, such as sowing a cover crop versus leaving bare soil in autumn; then the user can simulate the fate of NO~3~ trapped by the cover crop instead of what would have been leached [@constantin_long-term_2012]. Moreover, the fluxes simulated like N leaching, N~2~O emission, and C sequestration can be cumulated, externally to the model, to assess the cropping system performances, especially the GHG emissions [@autret_long-term_2020]. Other illustrations of considering the cropping system are provided in § \@ref(ways-stics-application). 

```{r fig-STICS-Chaining-USM-simplified, out.width='80%', fig.align='center', fig.cap='Simplified representation of the transferred variables between two successive units of simulation (USM);(*) organic C&N contents are simulated outputs while their initiale values belong to the soil characterisation.', echo = FALSE}
knitr::include_graphics('data/Overall-system/A_STICS_§2_Chaining_USM_N&N+1_simplified.jpg')
```


### Case of intercropping: simulation of bi-specific intercrops in STICS{#Overall-Intercropping}

#### Background, challenges and choice of formalism type
Intercropping consists in growing multiple crops (annual or perennial) simultaneously, with each crop developing and growing at its own rate as a result of interspecific interactions and resource partitioning. This practice is traditional in the tropics and is beginning to be used, in either organic farming or conservation agriculture, in temperate climates to support the agroecological transition and mitigating the use of chemical inputs. Intercropping can be implemented with various spatial and genotype arrangements: mixed-on the row intercrops, strip intercrops, alley crops, mixed intercrops or even windbreaks, all of which exhibit differing levels of spatial heterogeneity.
Given the complexity of intercropping system, agronomic models can be especially helpful for performing comprehensive intercropping analyses [@caldwell_simulation_1995]. The intercrop modelling framework can be summarised using three approaches. The first of these, consistent with the initial principles of [@de_wit_simulation_1970;@de_wit_simulation_1978], is an extension of sole crop modelling. This principle consider that the system comprises two species instead of one and is simply organised within a kind of elementary pixel intended to represent the whole spatial design. In fact, this approach is the most operational [@de_vries_simulation_1993; @kiniry_general_1992], and focuses more on system dynamics than spatial heterogeneity. The second approach is based on a description of the intercropping system as a series of discrete crop-based or tree-based points with a flow of mass or energy between each. This spatially discretised approach can account for large spatial variations, with each point generally being simulated under the above-mentioned crop modelling principle, and the field response results from a spatially integrated calculation [@huth_framework_2002]. The last approach derives from architecture modelling and emphasises a realistic description of the 3D structure of the complex two-species canopy, which leads to fine-scale descriptions of processes [@sonohat_leaf_2002] at the organ level or the plant level. In this third approach, functional structural plant model (FSPM) were developed; however, accounting for system dynamics is more difficult because of the complexity of the interaction of organ dynamics and the whole plant behaviour. 

The STICS crop model was adapted according to the first approach [@brisson_adaptation_2004], with the aim of producing an operational model to support intercrop managements, while attempting to overcome the problems of unwarranted over-simplification. The STICS model considers only bispecific intercrops (parameters sets for two crops or two genotypes) intercrops. The adaptation of STICS’s conceptual basis and formalisations to intercropping relies first depends on a simplified definition of the complex agronomic system of intercropping. Users must then adapt the modules calculating resource capture (light, water and nitrogen) between the two associated crops. Users can also simulate niche complementarity for N resources in cereal-legume intercrops since the rate of N~2~ fixation by legumes can be favoured by the quickest cereal N uptake in the soil.

#### Representation of the intercropping system {#rep-intercrop-system}

Because the intercropping system is complex, the STICS model adopts some simplifying hypotheses. The soil-plant-atmosphere system is divided into three sub-systems (D, SU and LU) at the canopy level (Figure \@ref(fig:fig-Overall-intercropping-representation)): the dominant crop (D) and the understorey crop (U) are divided into two parts: a shaded part (SU) and a sunlit part (LU), each defined by a light microclimate. These light microclimates, estimated from a radiation interception (§ \@ref(Microclimate-energy-budget-crop-temperature)), drive the different behaviours of the sub-systems in terms of growth ([$lai$](#var-lai-n)\index{lai(n)}, dry matter accumulation) and water and nitrogen budgets (transpiration, nitrogen uptake, stress index) at daily time step. Estimating the water requirements for both associated crops depends on light partitioning coupled to a resistive scheme. The phasic development is considered the same for both parts of the understorey crop. The soil environment is also assumed to be the same for both crops (i.e. the horizontal differentiation within the soil profile is disregarded in favour of the vertical differentiation. The assumption is made that the interactions between the two root systems result from the influence of the soil on each crop root profile, based on its penetrability and water dynamics.

This theory is applied within the STICS code via multiple calls to the elementary subroutines and re-calculation of the state variables as a function of the considered sub-system. Specific modules or options were added to account for the ecophysiological features of these complex systems. These modules cover radiation interception and the energy budget that drives water requirements and microclimate, and root system dynamics which are influenced by soil status over the various layers of the whole soil profile. Shoot growth was slightly modified to account for the understorey shaded crop growing under limiting radiation. Those modules and options are described in the relevant thematic chapters of this book. Reciprocally, the involved formalism options can be applied for sole crop simulations, like the energy budget for row crops.

```{r fig-Overall-intercropping-representation, out.width='80%', fig.align='center', fig.cap='Simplified diagram of the model: on the right the system with its three sub-systems (D: dominant canopy; U: understorey canopy divided into  a shaded part (SU) and a sunlit part (LU)); in the centre, the number of calls to each module devoted to a particular part of the system; on the left, the modules (grouped according to the way they are named in the code). * Corresponds to the modules modified for the adaptation to intercropping (from @brisson_adaptation_2004).', echo = FALSE}
knitr::include_graphics('data/Overall-system/intercropping_representation.jpg')
```


## Relationship and priority between processes {#overall-relationship-processes}

### Description of the main modules 

STICS is a deterministic process-based model whose code is organised into modules, with each module composed of sub-modules dealing with specific processes/mechanisms. A first set of three modules deals with the ecophysiology of above-ground plant parts (phenology, shoot growth, root growth, yield formation). A second set of four modules deals with how the soil responds in interaction with underground plant parts (root growth, water balance, nitrogen balance, soil transfers). The crop management module deals with the interactions between the applied techniques and the soil-crop system. The microclimate module simulates the combined effects of climate and water balance on the temperature and air humidity within the canopy. 

Within each module, there are options that can be used to extend the scope of STICS application to various soil-crop systems. These options relate aspects of ecophysiology and crop management, such as:

* competition between vegetative organs and storage organs for assimilates (hereafter referred to as trophic competition);

* the canopy geometry when simulating radiation interception;

* description of the root density profile;

* use of a resistive approach to estimate the evaporative demand by plants;

* mowing of forage crops;

* plant residues or plastic mulching under vegetation.

Another of the model's strong point is its conceptual modularity: sub-programs are identified for each group of ecophysiological processes, such as N or water balance, crop growth, changes in soil C-N stocks... 


### Priorities within the daily loop

All the processes are simulated at a daily scale, in interaction with agricultural techniques implemented at the crop cycle scale. The STICS source code is built as a sequential program where the instructions are executed one after the other and always in the same order. This order requires hypotheses about the priorities between the processes. Figure \@ref(fig:fig-Overall-daily-loop-processus) summarises the calculation steps within the daily loop. 

At each step, another order between the elementary functions is defined. Users will find it is well worth their time to specify the order of priority of these elementary functions, especially <!--between and-->within the following items:

* Crop growth: shoot growth, leaf senescence, yield elaboration, C and N assimilate allocation.

* C and N transformation: fertiliser, volatilisation, mineralisation, nitrification, denitrification.

* Water requirements: soil evaporation, crop transpiration.

* N nutrition of the crop(s): symbiotic fixation for legumes, N uptake (minimum of N demand and N supply).

* Stress indices calculation: water, nitrogen and abiotic factors (frost, anoxia).

* N partitioning in the crop(s): to the grain; between leaves, stems and reserves. 


```{r fig-Overall-daily-loop-processus, out.width='80%', fig.align='center', fig.cap='Simplified diagram of the order in STICS between processes within the daily loop.', echo = FALSE}
knitr::include_graphics('data/Overall-system/daily_loop_processus.jpg')
```

## Model genericity 

### Means of model genericity

The aim of model genericity is to cover a wide range of crops over time (i.e. to simulate crop rotation) and under various soil and weather conditions. This can be achieved through:

-	Parameter values: the value of a given parameter can be set according the observed system; note the distinction between global parameters (e.g plant species); and local parameters (e.g. soil properties);

-	Process options: Some options can be activated to deal with variability in crops, soils and practices;

-	The conceptual framework: the functionnal or process-based description of processes are used to find a common description between general processes in the plant kingdom (e.g. temperature threshold, RUE for dry matter accumulation…);

-	The STICS code modularisation: it allows for coupling within software platforms;

Of course, genericity does come with some trade-offs:

-	Many parameters are equivalent when compared to those for mechanistic models, implemented in 3D and at an hourly scale;  

-	Some parameters are devoid of biophysical signifiance and must be mathematically calibrated;

-	The actual validity domain depends on the quality and the range of situations covered by the calibration and validation databases.

Note that the activation and parameterisation of certain options depends on the input availability. For example, the use of a resistive model is based on the availability of additional climatic variables allowing the calculation of PET by the model, which requires data of wind and air humidity (§ \@ref(Tools-Driving-Options)).

### Typology of the options driving the whole soil-crop-atmosphere system

The model genericity relies on existence of options which can be distinguished according to: i) their purpose and ii) the level of the modelled system they impact. These criteria can be crossed-referenced, like in the examples provided in Table\ \@ref(tab:tab-Tools-formalisation-options-simulations). There are two possible purposes and three levels for all the options.


<br/>
```{r tab-Overall-table-option-typology, out.width='80%', tab.align='center', echo = FALSE, results='asis'}
cat("<caption>(\\#tab:tab-Overall-table-option-typology) Examples of options according to their typology.</caption>")

table_typology_options()

```
<br/>

#### Distinguishing between options according to their purpose
<!--Distinguishing between formalism and model use options versus the purpose-->

First, a formalism option can be activated to take into account specific characteristics of the cropping system or to investigate various ways of representing the system behaviour. Formalism options automatically lead to new algorithms and parameters introduced. The option algorithms will be presented in detail in the chapters on formalisms (chapters \@ref(development) to \@ref(CNbal)).

Second, a STICS use option aims to modify the model's conditions of application without modifying formalisms or parameters. For instance, user can activate/deactivate the effects of water and/or nitrogen stress on crop growth to be able to simulate potential or actual growing conditions. The input variables can change, for instance with the option of forcing the [$lai$](#var-lai-n)\index{lai(n)} against a dataset. The use options at global level can be called strategical options while all the others can be called driving options (§ \@ref(Tools-Driving-Options)). 


#### Distinguishing between options according to the level of impact on the modelled system

##### Global level
Several compartments of the soil-crop-atmosphere system are directly impacted<!--, by the options whose purpose vary-->:  

First, there are five global formalism options, which varying implementation:     

-	Intercropping code activation allows the user to simulate two intercrops that impact the whole cropping system; 

-	The perennial/annual choice impacts both the crop perennial reserve and crop management;

-	The grasslands can be simulated using the code ‘fou’ which triggers the possibility of successive cuts of a forage crop and the existence of a residual [$lai$](#var-lai-n)\index{lai(n)} after harvest; 

- Climate change is a simulation option which calls for a single additional climatic variable (e.g. CO~2~ concentration) and requires the user to activate several formalism options;

-	Climate shelter forcing also affects the whole crop microclimate.

Secondly, there are three global use options (strategical):     

-	the choice between independent or linked successive USMs which affects the USM's initial soil and crop variables, except those of the first one;  

- The water stress deactivation option affects [$lai$](#var-lai-n)\index{lai(n)} expansion, crop growth and soil moisture;

- The nitrogen stress deactivation option affects [$lai$](#var-lai-n)\index{lai(n)} expansion, crop growth, N uptake and soil N mineral content.

##### Function level
The formalism options at this level are numerous (see chapters \@ref(development) to \@ref(CNbal)); they can have three aims:    

- to add either a compartment or a function, that strongly interacts with the other system components; for instance the snow option, in the climate module affects water, temperature and N loss emissions, as well as the macroporosity option in the soil module.

- to integrate the variability of crop traits. For instance, the option of indeterminate versus determinate species drives fruit filling. Notice that STICS does not directly integrate the C3-C4 plant trait, but takes it into into account through the RUE value and the N dilution curve parameters.

- to compare several theoretical representations of the function. For instance, the representation of the root system can be addressed in true density expansion versus the profile type, with or without trophic links with shoot growth. 


There are few use options at this scale; they include, for instance, options to force either the [$lai$](#var-lai-n)\index{lai(n)} or crop stages according to measured values.

##### Process level
The process level concerns the design of the formalism simulating a given process (see chapters \@ref(development) to \@ref(CNbal)). They can be linked to the processes such as the end of seed dormancy or the harvest decision criteria.

Distinguishing between formalism and application options is not critical at this scale; the possibility of forcing the fertiliser nitrogen efficiency can be considered as STICS option use.  
The various options allow the STICS users to adapt a specific pathway of activated options for each crop or cropping system. 


### Examples of scenario of parameterisation {#overall-parameterisation}

STICS allows for crop species genericity through the design of a 'parameterisation strategy' which combines activation of some formalisation options based on ecophysiological knowledge of the involved plant species. For example, the action of the photoperiod and the vernalisation requirements are activated, or not, in the module dedicated to plant development. There are formalism options of for each module which permit the model to take into account the specific ecophysiology of various crops, as shown in Figure \@ref(fig:fig-Overall-Model-plant-modularity). In wheat, for example, leaf surface growth is independent from trophic aspects, while in sugar beet, it depends on competition with storage organs. With intercropping, both plant files must include the options for radiative transfer, true density and resistive approach to simulate this system. 

The same principle applies for crop management, as illustrated in Figure \@ref(fig:fig-Overall-Model-technicals-modularity). Indeed, many different technical options may be activated or not, according to the crop onset, fertilisation type, irrigation management, canopy control or harvest decision rule. For intercropping, some techniques must be identical for both crops, according to site specific practical considerations, such as tillage or harvest criteria decision. 

With less modalities, similar illustrations could be applied to soil or climate station. Finally, from a practical point of view, there is an asymmetry between the plant file, for which the parameterisation is already set by STICS team for some crops and other input files, which characterise the management, soil, climate and initial conditions. Filling these input files requires a minimal data collection and expertise by the user (chapter \@ref(model-extension)). 


```{r fig-Overall-Model-plant-modularity, out.width='80%', fig.align='center', fig.cap='Examples of parameterisation strategies as the result of activating  a set options in each specific plante file. Acronym: PET = Potential EvapoTranspiration.', echo = FALSE}
knitr::include_graphics('data/Overall-system/options_plant_DR.jpg')
```


```{r fig-Overall-Model-technicals-modularity, out.width='80%', fig.align='center', fig.cap='Examples activated option pathways to parameterise the crop management. Acronym: lai = leaf area index.', echo = FALSE}
knitr::include_graphics('data/Overall-system/options_tech.jpg')
```


## Parameterised crop species

### Definitions

This explanation concerns the availability of plant species, for which three levels of quality of crop parameterisation can be defined: 

- Finalised parameterisation means the plant file is currently being used and tested against a large dataset. The set of parameters was calibrated based on literature review as well as a specific calibration database, before being tested against an independent database. Each new  STICS version is documented and tested against a standard protocol (§ \@ref(adaptation-to-a-new-crop)).

- Prototype parameterisation refers to a set of parameters whose performance quality is uncertain. Calibration was performed using either an overly limited dataset or an old STICS version. In the first case, prototype parameterisation can run with the current version, but users should exercise caution when doing so. In the second case, the parameterisation became incomplete and needs to be updated. In both cases, users running a prototype parameterisation are welcome to test it and, if possible, improve it and then share the performances within the STICS community, following the aforementioned protocol.

- Ongoing parameterisation refers to crop species at various stages of finalisation or awaiting for publication or for documentation of the validation database as regard the standard version. Users looking for more information about these species can contact the STICS team or post questions to the STICS community by using the forum.

### List of parameterised crops

The current list of finalised and prototypes crop species is available on the STICS website, when the model is loaded. Figure \@ref(fig:fig-Overall-Model-parameterised-plants) shows several kinds of plant files that are considered in STICS. Note that some species, such as turmeric and strawberry crops, which are perennial in botanical terms, can be parameterised as annuals using STICS, based on how farmers manage them. 
Additionnaly, the parameterisation of canola, finger-millet and marigold is ongoing while the parameterisation of triticale, white-clover, pigeon pea and winter fababean are waiting for documentation.  


```{r fig-Overall-Model-parameterised-plants, out.width='80%', fig.align='center', fig.cap='List of parameterised plant species as of 2021. The species given in bold have been fully parameterised while those in regular font are at the prototype one.', echo = FALSE}
knitr::include_graphics('data/Overall-system/parameterized_plants.jpg')
```


