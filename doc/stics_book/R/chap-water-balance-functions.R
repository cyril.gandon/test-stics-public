# Figures Chapter Water Balance
#

require(data.table)
require(reshape2)
# Figure 71

EOS_vs_LAI_fig_water_balance <- function()
{
  don_71 <- read.csv("data/chap_water_balance//figure71.csv",sep=";")
  sp_71 <- ggplot(don_71) + aes(x=LAI, y=EOS.ETP, color=SIMULATION) + geom_line()
  sp_71 + xlab(expression("lai ("*m^2~m^-2*")")) + ylab("eos/tetp") +
    labs(color = expression("extin"[P])) +
    scale_color_discrete(labels=c('Maize=0.7', 'Sunflower=0.9', 'Wheat=1.2')) +
    theme_few() + theme(legend.position = c(0.7, 0.8))
}
#
# Figure 72a
SESOL_vs_SEOS_a_fig_water_balance <- function()
{
  don_72a <- read.csv("data/chap_water_balance//figure72a.csv",sep=";")
  sp_72a <- ggplot(don_72a) + aes(x=SUMEOS, y=SUMESOL, color=SIMULATION) + geom_line() +
    geom_segment(aes(x=5,y=1, xend=5,yend=5),color="black",linetype="dotted",size=1.1) +
    geom_text(x=5, y=0.5, label="Q0",color="black",size=5)
  sp_72a + xlab(expression(sum(eos)~"(mm)")) + ylab(expression(sum(esol)~"(mm)")) +
    labs(color = expression("argi"[S])) +
    scale_color_discrete(labels=c('Clayey soil=60', 'Sandy soil=5')) +
    theme_few() + theme(legend.position = c(0.7, 0.4))
}
#
# Figure 72b
SESOL_vs_SEOS_b_fig_water_balance <- function()
{
  don_72b <- read.csv("data/chap_water_balance//figure72b.csv",sep=";")
  sp_72b <- ggplot(don_72b) + aes(x=SUMEOS, y=SUMESOL, color=SIMULATION) + geom_line() +
    geom_segment(aes(x=5,y=1, xend=5,yend=5),color="black",linetype="dotted",size=1.1) +
    geom_text(x=5, y=0.5, label="Q0",color="black",size=5)
  sp_72b + xlab(expression(sum(eos)~"(mm)")) + ylab(expression(sum(esol)~"(mm)")) +
    labs(color = expression("aclim"[C])) +
    scale_color_discrete(labels=c('No windy location=20', 'Windy location=14')) +
    theme_few() + theme(legend.position = c(0.7, 0.4))
}
#
# Figure 73
SESOL_vs_Time_fig_water_balance <- function()
{
  don_73 <- read.csv("data/chap_water_balance//figure73.csv",sep=";")
  sp_73 <- ggplot(don_73) + aes(x=DAYJUL, y=SUMESOL, color=SIMULATION) + geom_line()
  sp_73 + xlab("Julian day") + ylab(expression(sum(esol)~"(mm)")) +
    labs(color = expression("q0"[S])) +
    scale_color_discrete(labels=c('0', '10', '20')) +
    theme_few() + theme(legend.position = c(0.7, 0.2), legend.text.align = 1)
}
#
# Figure 74a
ESZ_vs_ESOL_a_fig_water_balance <- function()
{
  don_74a <- read.csv("data/chap_water_balance//figure74a.csv",sep=";")
  sp_74a <- ggplot(don_74a) + aes(x=VALUE, y=DEPTH, color=SIMULATION) +
    geom_line()+
    annotate(geom="text", label=expression(cfes[S]*"=5"), x=0.15, y=-50)
  #geom_text(x=0.15, y=-50, label="cfes=5",color="black",size=5)
  sp_74a + xlab("esz(z) / esol") + ylab("z (cm)") +
    labs(color = expression("zesx"[S])) +
    scale_color_discrete(labels=c('120', '30', '60', '90')) +
    theme_few() + theme(legend.position = c(0.8, 0.5), legend.text.align = 1)
}
#
# Figure 74b (***************REVOIR PROBLEME ligne ZESX=30 ***************)
ESZ_vs_ESOL_b_fig_water_balance <- function()
{
  don_74b <- read.csv("data/chap_water_balance//figure74b.csv",sep=";")
  ggplot(don_74b) + aes(x=VALUE, y=DEPTH, color=SIMULATION) + geom_line() + xlim(0,0.2) +
    #geom_text(x=0.165, y=-50, label="cfes=1",color="black",size=5) +
    annotate(geom="text", label=expression(cfes[S]*"=1"), x=0.165, y=-50) +
    xlab("esz(z) / esol") + ylab("z (cm)") +
    labs(color = expression("zesx"[S])) +
    scale_color_discrete(labels=c('120', '30', '60', '90')) +
    theme_few() + theme(legend.position = c(0.8, 0.5), legend.text.align = 1)
}
#
# Figure 75a
soil_plant_EOS_vs_LAI_a_fig_water_balance <- function()
{
  don_75a <- read.csv("data/chap_water_balance//figure75a.csv",sep=";")
  sp_75a <- ggplot(don_75a) + aes(x=LAI, y=VALUE, color=SIMULATION) + geom_line()
  sp_75a + xlab("lai") + ylab("") +
    theme_few() + theme(legend.position = c(0.75, 0.3), legend.title = element_blank())
}
#
# Figure 75b
soil_plant_EOS_vs_LAI_b_fig_water_balance <- function()
{
  don_75b <- read.csv("data/chap_water_balance//figure75b.csv",sep=";")
  sp_75b <- ggplot(don_75b) + aes(x=TAUXCOUV, y=VALUE, color=SIMULATION) + geom_line()
  sp_75b + xlab("tauxcouv") + ylab("") +
    theme_few() + theme(legend.position = c(0.3, 0.8), legend.title = element_blank())
}
#
# Figure 76
# Voir image ne peut pas faire mieux.
#

#
# Figure 710
TESTSTOMATE_vs_ZRAC_fig_water_balance <- function(){
  don_710 <- read.csv("data/chap_water_balance/figure710.csv",sep=";")
  sp_710 <- ggplot(don_710) + aes(x=ZRAC, y=VALUE, color=SIMULATION) + geom_line()
  sp_710 + xlab("zrac (cm)") + ylab("tetstomate (% vol)") +
    labs(color = "eop") +
    scale_color_discrete(labels=c("0.1 mm", "9 mm")) +
    theme_few() + theme(legend.position = c(0.7, 0.8), )
}


mulch_vs_soilcover_fig_water_balance <- function(){
  don = read.csv("data/chap_water_balance/soilcovermulch_dayappli.csv",sep="\t", check.names = FALSE)
  don$qmulch = don$qmulch / 10

  ggplot(reshape2::melt(don, "J", variable.name = "Variable")) +
    aes(x = J, y = value, color = Variable, linetype = Variable) +
    geom_line() +
    #ggtitle("Initial mulch amount = 6 t.ha-1") +
    scale_y_continuous(name = "Proportion of soil cover",
                       sec.axis = sec_axis(~ . *10 , name = expression("qmulch (t"~ha^{"-1"}*")")),
                       limits = c(0, 1)) +
    scale_linetype_manual(values = c("dashed", "solid", "solid"),
                          labels=c('qmulch',
                                   expression(kcouvmulch[G]*"=0.1"),
                                   expression(kcouvmulch[G]*"=0.3")))+
    scale_color_discrete(labels=c('qmulch',
                                  expression(kcouvmulch[G]*"=0.1"),
                                  expression(kcouvmulch[G]*"=0.3"))) +
    labs(x = expression(paste("Days since mulch application"))) +
    theme_few() + theme(legend.position = c(0.7, 0.8),
                        legend.text.align = 0,
                        legend.title = element_blank())
}


fruis_vs_qmulch_fig_water_balance <- function(){
  don <- read.csv("data/chap_water_balance/fruis_qmulch.csv", sep="\t", check.names = FALSE)
  ggplot(reshape2::melt(don, "Qmulch", variable.name = "Variable")) +
    aes(x = Qmulch, y = value, color = Variable) +
    geom_line() +
    ylim(0,0.6) +
    scale_color_discrete(labels=c(expression(ruisolnu[S]*"=0.5 and"~qmulchruis0[G]*"=1"),
                                  expression(ruisolnu[S]*"=0.2 and"~qmulchruis0[G]*"=1"),
                                  expression(ruisolnu[S]*"=0.5 and"~qmulchruis0[G]*"=3"))) +
    labs(x=expression("qmulch (t"~ha^{"-1"}*")"),y=expression(paste("fruis"))) +
    theme_few() + theme(legend.position = c(0.76, 0.85),
                        legend.title = element_blank(),
                        legend.text.align = 0)


}

effect_mulch_on_water <- function(){
  #don <- read.csv("data/chap_water_balance/WB-Manage_effect_mulch.csv", sep=";", check.names = FALSE)


  colors = c(
    "Plant transpiration (mm)" = "#1f9dcf",
    "Soil evaporation (mm)" = "#e5ae37",
    "Mulch evaporation (mm)" = "#b0afad"
  )

  fread("data/chap_water_balance/WB-Manage_effect_mulch.csv", data.table = FALSE) %>%
    reshape2::melt(id.vars = "type_mulch") %>%
    ggplot(aes(x = type_mulch, y = value, color = variable, fill = variable)) +
    geom_bar(stat = "identity", position = position_dodge()) +
    scale_fill_manual(values = colors) +
    scale_colour_manual(values = colors) +
    scale_x_discrete(
      labels =
        c(
          "Plant mulch (1t/ha)" = "Plant mulch\n(1t/ha)",
          "plant mulch (6t/ha)" = "Plant mulch\n(6t/ha)",
          "Black plastic mulch (50%cover)" = "Black plastic\nmulch\n(50%cover)",
          "plant mulch (6t/ha) + tillage" = "Plant mulch\n(6t/ha) + tillage"
        )
    ) +
    theme_minimal() +
    labs(color = "", fill = "", y = "", x = "") +
    theme(legend.position="bottom")

}




tab_water_balance_testomate <- function() {

  tb <- flextable(read.csv(file = file.path("data/chap_water_balance/table_testomate.csv"), header = TRUE, stringsAsFactors = FALSE, sep = ";"))


  tb <- set_header_labels(tb,
                          Parameters="Parameters",
                          Nominal.value="Nominal value",
                          tetstomate.sensitivity="tetstomate sensitivity",
                          tetstomate.sensitivity.1="tetstomate sensitivity")


  tb <- merge_at( tb, i = 1, j = 3:4, part = "header")

  tb <- set_table_properties(tb,layout="autofit")


  tb <- fontsize(tb,size=10,part="body")
  tb <- font(tb, fontname = "Times New Roman", part="all")

  tb <- align(tb, align = "right", part="body", j = 3:4 )

  tb <- align(tb, align = "center", part="body", j = 2 )

  tb <- bold(tb, i = 1, part = "header")

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_water_balance/html",
                                            file_name = "water_balance_testomate"))



}

tab_components_water_balance <- function() {

  tb <- flextable(read.csv(file = file.path("data/chap_water_balance/table_components-water-balance.csv"),
                           header = FALSE, stringsAsFactors = FALSE, sep = ";",
                           colClasses = "character"))


  tb <- set_header_labels(tb,
                          V1="Inputs",
                          V2="(mm)",
                          V3="Outputs",
                          V4="(mm)")


  tb <- set_table_properties(tb,layout="autofit", width=0.5)


  tb <- fontsize(tb,size=10,part="body")
  tb <- font(tb, fontname = "Times New Roman", part="all")

  tb <- align(tb, align = "right", part="body", j = c(2,4) )

  tb <- align(tb, align = "left", part="body", j = c(1,3) )

  tb <- bold(tb, i = 1, part = "header")

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_water_balance/html",
                                            file_name = "_components_water_balance"))




}

