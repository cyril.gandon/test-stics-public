# Functions to calculate and plot equations from chap 11 Management and crop environment
# D Ripoche et al.
# creation: 15 10 2020
# last version : 14 10 2020

library(ggplot2)
library(ggthemes)

#figure 6.1 du red book 2008

organisation <- function(xmax, ymax,N_input){

    if(N_input>xmax){Orga=ymax}
    else{Orga=(-ymax/(xmax^2))*N_input*((-2*xmax)+N_input)}

  return(Orga)
}

Norgeng_fig1 <- function(){

N_input<- seq(0,255,15)
# orngeng pour engrais type AN
Orgeng_AN <- sapply(N_input, FUN=organisation, xmax=400,ymax=46)
# orngeng pour engrais type urea
Orgeng_Urea <- sapply(N_input, FUN=organisation, xmax=400,ymax=38)
# orngeng pour engrais type NO3
Orgeng_NO3 <- sapply(N_input, FUN=organisation, xmax=400,ymax=25)

Zlabel <- "NH4NO3 [2,4,5]"
Norgeng_AN<-data.frame(Zlabel,N_input, Orga=Orgeng_AN)
Zlabel <- "UREA [1,3,5]"
Norgeng_Urea<-data.frame(Zlabel,N_input, Orga=Orgeng_Urea)
Zlabel <- "KNO3 [4]"
Norgeng_NO3<-data.frame(Zlabel,N_input, Orga=Orgeng_NO3)

DF <- rbind(Norgeng_AN, Norgeng_Urea, Norgeng_NO3)
DF$Zlabel <- factor(DF$Zlabel, levels = c("NH4NO3 [2,4,5]", "UREA [1,3,5]",  "KNO3 [4]" ))
# il faut maintenant ajouter les obs !
Orga_obs <- read.table(file = "./data/chap_SC-manage/experiment_Organisation_old_61.csv", header = TRUE, stringsAsFactors = FALSE, sep = ";")

p <- ggplot(data = DF, aes(x = N_input,y= Orga,  color = Zlabel)) +
  geom_line() +
   labs(x = expression("anit(t) (kgN/ha)"), y = "Norgeng (kg/ha)" ) +
  scale_x_continuous(limits = c(0, 250)) + xlab(expression("anit (kg N ha"^-1*")")) +
  scale_y_continuous(limits = c(0, 40)) + ylab(expression("Norgeng (kg ha"^1*")")) +
  scale_color_discrete(labels=c(expression(NH[4]*NO[3]~"[2, 4, 5]"),
                                "Urea [1, 3, 5]",
                                expression(KNO[3]~"[4]")))+
  theme_few() + theme(legend.title=element_blank(), legend.position = c(0.8, 0.2), legend.text.align = 0) +
  geom_point(data=Orga_obs, aes(y= N_organised_kg.ha, x = Input_N_kg.ha, color = name))


#rm(DF, Orgeng_AN, Norgeng_Urea, Norgeng_NO3, Orgeng_AN, Orgeng_Urea,Norgeng_NO3, Z)

p
}


#figure 6.2 du red book 2008

Nvoleng <- function(ymax, a,
                    Vol,Vabsmoy){
  Volat <-a*ymax/(a+Vabsmoy)*Vol
  return(Volat)
}
Ndeneng <- function(ymax, a,
                    Vabsmoy){
  Denit <- a*ymax/(a+Vabsmoy)
  return(Denit)
}
Norgeng <- function(ymax, xmax,
                    N_input){
  Orga <- organisation(xmax,ymax,N_input)/N_input*100
  #print(Orga)
  return(Orga)
}


Nlosses_fig2 <- function(){
#Nloss calculation
Vabsmoy= seq(0,4.0,0.3)

#vol_loss=Nvoleng(ymax=15,a=2,pH=7.5,Vol=0.67,x=X)
vol_loss <- sapply(Vabsmoy, FUN=Nvoleng, ymax=30,a=2,Vol=0.67)
#den_loss=Ndeneng(ymax=13, a=2,x=X)
den_loss <- sapply(Vabsmoy, FUN=Ndeneng, ymax=13,a=2)
#org_loss=Norgeng(ymax=13, a=2,N_input=100,x=X)
org_loss_fixe=Norgeng(ymax=38, xmax=400,N_input=100)
#org_loss <- data.frame(Vabsmoy,org_loss_fixe)

Zlabel <- "Nvoleng"
Nloss_vol<-data.frame(Zlabel,Vabsmoy, Nloss=vol_loss)
Zlabel <- "Ndenit"
Nloss_den<-data.frame(Zlabel,Vabsmoy, Nloss=den_loss)
Zlabel <- "Norgeng"
Nloss_org<-data.frame(Zlabel,Vabsmoy, Nloss=org_loss_fixe)

DF <- rbind(Nloss_vol, Nloss_den, Nloss_org)
DF$Zlabel <- factor(DF$Zlabel, levels = c("Nvoleng", "Ndenit",  "Norgeng" ))


p <- ggplot(data = DF, aes(x = Vabsmoy, y= Nloss, color = Zlabel)) +
  geom_line() +
  labs(x = expression("vabsmoy (kgN/ha/day)"), y= "N loss (%added)") +
  scale_x_continuous(limits = c(0, 4)) + xlab(expression("vabsmoy (kg ha"^-1*"day"^-1*")")) +
  scale_y_continuous(limits = c(0, 30)) + ylab("N loss (% added)") +
  theme_few() + theme(legend.title=element_blank(), legend.position = c(0.8, 0.80))
p

}
#
# pour edition des tableaux


# parameter-linked-DAF2

tab_parameter_linked_DAF2 <- function() {
  tb <- flextable(read.csv(file="./data/chap_SC-manage/parameters_linked_DAF2.csv",header = TRUE, stringsAsFactors = FALSE, sep = ";"))

  tb <- fontsize(tb,size=10,part="body")
  tb <- align(tb,align = "center", part="all")
  tb <- font(tb, fontname = "Times New Roman", part="all")
  tb <- bold(tb, i = 1, part = "header")
  tb <- mk_par(tb, j=1, part = "header", value = as_paragraph("DAF", as_sub("S")))
  tb <- mk_par(tb, j=2, part = "header", value = as_paragraph("infil", as_sub("S")))
  tb <- mk_par(tb, j=3, part = "header", value = as_paragraph("zesx", as_sub("S")))
  tb <- mk_par(tb, j=4, part = "header", value = as_paragraph("q0", as_sub("S")))

  tb <- set_table_properties(tb, layout="autofit", width = 0.5)

  #tb

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_SC-manage/html", file_name = "parameter_linked_DAF2"))

}

# wettability

tab_wettability <- function() {
  tb <- read.csv(file="./data/chap_SC-manage/wettability.csv",header = FALSE, stringsAsFactors = FALSE, sep = ";")

  col_names <- tb[1,]
  tb <- tb[2:dim(tb)[1],]
  tb <- flextable(tb)
  tb <- set_header_labels(tb, values = col_names)
  tb <- fontsize(tb,size=10,part="body")
  tb <- align(tb,align = "center", part="all")
  tb <- font(tb, fontname = "Times New Roman", part="all")
  tb <- bold(tb, i = 1, part = "header")
  tb <- align(tb, j = 1, align = "left", part = "all")
  tb <- set_table_properties(tb, layout="autofit")

  #tb

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_SC-manage/html", file_name = "wettability"))

}


tab_tec_fertilisers <- function() {

  tb <- flextable(read.csv(file="./data/chap_SC-manage/tec_fertilizer.csv",header = TRUE, stringsAsFactors = FALSE, sep = ";"))
  tb <- fontsize(tb,size=10,part="body")

  tb <- align(tb,align = "center", part="all")
  tb <- mk_par(tb, j = 1, part = "body", value = as_paragraph(Type, as_sub("t")))
  tb <- set_header_labels(tb, ammonium.nitrate="Ammonium nitrate", UAN.solution="UAN solution",
                          anhydrous.ammonia="Anhydrous ammonia",ammonium.sulfate	="Ammonium sulfate",
                          ammonium.phosphate	="Ammonium phosphate", calcium.nitrate	="Calcium nitrate",
                          fixed.efficiency="Fixed efficiency", urea="Urea")
  tb <- footnote(tb, i = 1, j = 9,
                 value = as_paragraph(
                   "With this option the deneng",as_sub("t")," voleng",as_sub("t"), " and orgeng", as_sub("t"),
                   "values represent the proportion of fertiliser which is denitrified, volatilised and immobilised in soil, respectively"), ref_symbols = c("1"), part = "header")
  tb <- add_header_row( x = tb, values = c("Code", "1","2","3","4","5","6","7","8"))
  #tb <- border_outer(tb, part="all",border = fp_border(color="black",width=1))
  tb <- fix_border_issues(tb)
  tb <- align(tb,align = "center", part="all")
  tb <- set_table_properties(tb, layout="autofit")
  tb <- font(tb, fontname = "Times New Roman", part="all")

  #tb

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_SC-manage/html", file_name = "tec_fertilisers"))


}

# tab_tec_fertilisers2

tab_tec_fertilisers2 <- function() {
  tb <- flextable(read.csv(file="./data/chap_SC-manage/tec_fertilizer2.csv",header = TRUE, stringsAsFactors = FALSE, sep = ";"))

  tb <- fontsize(tb,size=10,part="body")
  tb <- align(tb,align = "center", part="all")
  tb <- font(tb, fontname = "Times New Roman", part="all")
  tb <- bold(tb, i = 1, part = "header")
  tb <- mk_par(tb, j=3, part = "header", value = as_paragraph("engamm", as_sub("G")))
  tb <- mk_par(tb, j=4, part = "header", value = as_paragraph("orgeng", as_sub("G")))
  tb <- mk_par(tb, j=5, part = "header", value = as_paragraph("deneng", as_sub("G")))
  tb <- mk_par(tb, j=6, part = "header", value = as_paragraph("voleng", as_sub("G")))

  tb <- footnote(tb, i = 8, j=2,
                 value = as_paragraph(
                   "With this option the deneng",as_sub("G")," voleng",as_sub("G"), " and orgeng", as_sub("G"),
                   " values represent the proportion of fertiliser which is denitrified, volatilised and immobilised in soil, respectively"),
                   ref_symbols = c("1"), part = "body")


  tb <- set_table_properties(tb, layout="autofit", width = 0.5)

  #tb

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_SC-manage/html", file_name = "parameter_linked_DAF2"))

}



#type-of-residues

tab_type_of_residues <- function() {
  tb <- read.csv(file="./data/chap_SC-manage/type_of_residues.csv",header = FALSE, stringsAsFactors = FALSE, sep = ";")

  col_names <- tb[1,]
  tb <- tb[2:dim(tb)[1],]
  tb <- flextable(tb)
  tb <- set_header_labels(tb, values = col_names)
  tb <- fontsize(tb,size=10,part="body")
  tb <- align(tb,align = "center", part="all")
  tb <- font(tb, fontname = "Times New Roman", part="all")
  tb <- bold(tb, i = 1, part = "header")
  tb <- align(tb, j = 1, align = "left", part = "all")
  tb <- set_table_properties(tb, layout="autofit", width = 0.5)

  #tb

  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_SC-manage/html", file_name = "type_of_residues"))

}


tab_tec_organic_residues <-function(){


  tb <- flextable(read.csv(file="./data/chap_SC-manage/tec_organic_residues.csv",header = TRUE, stringsAsFactors = FALSE, sep = ";"))

  tb <- fontsize(tb,size=10,part="body")
  tb <- font(tb, fontname = "Times New Roman", part="all")
  tb <- align(tb,align = "center", part="all")
  tb <- mk_par(tb, j=2, i=2, part = "body", value = as_paragraph(Residue.code, as_sub("t")))
  tb <- mk_par(tb, j=3, i=2, part = "body", value = as_paragraph(average.rate, as_sub("t")))
  tb <- mk_par(tb, j=4, i=2, part = "body", value = as_paragraph(carbon.content, as_sub("t")))
  tb <- mk_par(tb, j=5, i=2, part = "body", value = as_paragraph(cn.ratio, as_sub("t")))
  tb <- mk_par(tb, j=6, i=2, part = "body", value = as_paragraph(mineral.n.content, as_sub("t")))
  tb <- mk_par(tb, j=7, i=2, part = "body", value = as_paragraph(water.content, as_sub("t")))
  tb <- mk_par(tb, j=3, i=1, part = "body", value = as_paragraph("t FM ha",as_sup("-1")))
  tb <- set_header_labels(tb, Type=" ", Residue.code="Residue code", average.rate="Average rate",
                          carbon.content="Carbon content", cn.ratio="C/N ratio",
                          mineral.n.content="Mineral N content", water.content="Water content",
                          reference="Reference (pers. com.)")
  tb <- border_remove(tb)
  tb <- hline_top(tb, part="header", border = fp_border(color="black",width=2))
  tb <- hline(tb, i=1 , j=3:8, part="header", border = fp_border(color="black",width=1))
  tb <- hline(tb, i=1 , j=2:8, part="body", border = fp_border(color="black",width=1))
  tb <- hline(tb, i=2 , part="body", border = fp_border(color="black",width=2))
  tb <- hline(tb, i=9 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=15 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=19 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=23 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=27 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=28 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=27 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=29 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=30 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=33 , part="body", border = fp_border(color="black",width=0.5))
  tb <- hline(tb, i=34 , part="body", border = fp_border(color="black",width=2))
  tb <- align(tb,align = "center", part="all")

  tb <- set_table_properties(tb, layout="autofit")


  #tb
  htmltools::includeHTML(gen_html_flextable(ft_object = tb, dir_path = "data/chap_SC-manage/html", file_name = "tec_organic_residues"))


}

effect_mulch_on_CandN <- function(){

  colors = c(
    "Yield (t/ha)" = "yellow3",
    "Biomass (t/ha)" = "tan1",
    "Mineralisation (Kg N ha-1)" = "darkgrey"
  )

  fread("data/chap_SC-manage/SC-Manage_effect_mulch_CandN.csv", data.table = FALSE) %>%
    reshape2::melt(id.vars = "type_mulch") %>%
    ggplot(aes(x = type_mulch, y = value, color = variable, fill = variable)) +
    geom_bar(stat = "identity", position = "dodge") +
    scale_fill_manual(values = colors,  labels = c(
      expression("Yield (t ha"^-1*")"),
      expression("Biomass (t ha"^-1*")"),
      expression("Mineralisation (kg N ha"^-1*")")
    )) +
    scale_colour_manual(values = colors, labels = c(
      expression("Yield (t ha"^-1*")"),
      expression("Biomass (t ha"^-1*")"),
      expression("Mineralisation (kg N ha"^-1*")")
    )) +
    scale_x_discrete(
      labels =
        c("Bare soil" = expression("bare soil"),
          "Plant mulch (1t/ha)" = expression("1 t"~ha^-1),
          "plant mulch (6t/ha)" = expression("6 t"~ha^-1),
          "Black plastic mulch (50%cover)" = "50% cover",
          "Plant mulch (6t/ha) + tillage" = expression("6 t"~ha^-1*" + tillage")
          #"Plant mulch (6t/ha) + tillage" = "Plant mulch\n(6t/ha) + tillage"
        )
    ) +
    annotate(geom = "text", x = 4, y = -5, label = "Plant mulch") +
    annotate(geom = "text", x = 1, y = -5, label = "No mulch") +
    annotate(geom = "text", x = 2, y = -5, label = "Black plastic") +
    #theme_minimal() +
    theme_few() +
    labs(color = "", fill = "", y = "", x = "") +
    theme(legend.position="bottom", panel.border = element_blank(),
          axis.ticks = element_blank(),axis.text.x = element_text(vjust = 1),
          panel.grid.major.y = element_line(color = "grey90"))

}

