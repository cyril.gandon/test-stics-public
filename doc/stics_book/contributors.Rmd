# Contributors to STICS formalization {-}

> A REVOIR : liste des contributeurs à compléter !

#### R. Antonioletti^(1)^, N. Beaudoin^(1)^, P. Bertuzzi^(1)^, T. Boulard^(1)^, N. Brisson^(1)^, S. Buis^(1)^, P. Burger^(1)^, F. Bussière^(1)^, Y.M. Cabidoche^(1)^, P. Cellier^(1)^, Y. Crozat^(2)^, P. Debaeke^(1)^, F. Devienne-Barret^(1)^, C. Durr^(1)^, M Duru^(1)^, B. Gabrielle^(1)^, I. Garcia de Cortazar Atauri ^(1)^, C. Gary^(1)^, F. Gastal^(1)^, P. Gate^(3)^, J.P. Gaudillère^(1)^, S. Génermont^(1)^, M. Guérif^(1)^, C. Hénault^(1)^, G. Helloux^(1)^, B. Itier^(1)^, M.H. Jeuf froy^(1)^, E. Justes ^(1)^, M. Launay ^(1)^, S. Lebonvallet ^(1)^, G. Lemaire^(1)^, F. Maraux^(4)^, B. Mary^(1)^, T. Morvan^(1)^, B. Nicolardot^(1)^, B. Nicoullaud^(1)^, H. Ozier-Lafontaine^(1)^, L. Pagès^(1)^, B. Rebière^(4)^, S. Recous^(1)^, G. Richard^(1)^, R. Roche^(1)^, J. Roge<span class="scri">r-</span>Estrade^(1)^, F. Ruget^(1)^, C. Salon^(1)^, B. Seguin^(1)^, J. Sierra ^(1)^, H. Sinoquet^(1)^, J. Tournebize^(4)^ , R. Tournebize^(1)^, C. Valancogne^(1)^, A.S. Voisin^(1)^, D. Zimmer^(4)^ {-}

* ^(1)^ : INRAE
* ^(2)^ : ESA-Angers
* ^(3)^ : ARVALIS Institut du végétal
* ^(4)^ : Cemagref

Dominique Ripoche is responsible for developing and maintaining the STICS Fortran code, its development and maintenance

English revised by Teri Jones-Villeneuve.

> A REVOIR : adapter par rapport aux conventions !!!!

**Notations:** The illustrations and equations are numbered by chapter. The variable names used in the equations are listed in appendix 1. and the parameters are identifiable by indices: “T” for technical parameters, “S” for soil parameters, “C” for climate parameters, “P” for genotype-independent plant parameters, “V” for genotype-dependent plant parameters and “G” for general parameters. The index “I” is used for the initial state of the key variables. When used in the text, these variables are written in capital letters. In the equations the variables (VAR) are referred to for the current day, I, by writing them as VAR(I) while this reference is omitted in the text for readability. In a summation over time, the time daily variable is named J.

