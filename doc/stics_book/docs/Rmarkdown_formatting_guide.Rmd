# Rmarkdown formatting guide {#formatting-guide}
## Introduction
This guide is written itself as an Rmarkdown **bookdown** document for showing how to use the syntax and helping to write the Stics Red Book with minimal instructions.

For getting more details about formatting documents with base Rmarkdown and advanced bookdown syntax refer to the following online resources: 

* Link to Rmarkdown documentation: <http://rmarkdown.rstudio.com>, <https://bookdown.org/yihui/rmarkdown>

* Link to the R Markdown Cheat Sheet: <https://rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf>

* Link to thereference guide: <https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf>

* Link to Bookdown free online book [@xie_bookdown_2020]: <https://bookdown.org/yihui/bookdown>

* Link to Rmarkdown Cookbook free online book: <https://bookdown.org/yihui/rmarkdown-cookbook>

* Link to Latex equation resources for Rmarkdown:    
  * Syntax description    
<http://www.math.mcgill.ca/yyang/regression/RMarkdown/example.html>    
<https://en.wikibooks.org/wiki/LaTeX/Mathematics>    
<https://fr.wikibooks.org/wiki/LaTeX/%C3%89crire_des_math%C3%A9matiques>    
<https://www.overleaf.com/learn/latex/mathematical_expressions>    
  * Tutorial:    
<https://www.latex-tutorial.com/tutorials/amsmath/>
  * Tools:    
  <https://equalx.sourceforge.io/downloads.html>

* Link to Pandoc documentation: <https://pandoc.org/MANUAL.html>


This document is only focusing on formatting syntax. For prior R packages or system dependencies installations and versionning tools and practices refer to the [software prerequisites](#soft-pre-tips) documentation with items about:

* Subversion and tools
* Zotero and tools
* R software and packages

## Formatting syntax 
### Commenting lines
Adding commented lines or text blocks in an Rmd file may be usefull for giving information about writing progression / steps or what to do in some parts, and so on ...

For doing this, html commenting syntax must be used directly in text, for example as follows:

````markdown
This is some text ...    
<!-- this is a commented line -->
<!-- This is a
commented lines block -->
````

Commented lines font and color will appear as R commented lines in the text editor and of course will not appear in the final book document.

### Base text formatting syntax

#### Character Styling
* *Italics* are rendered using asterisks around words or text: `*Italics*`
* **Bold face** is rendered using two asterisks around words or text: `**Bold face**`
* Combining _**italics and bold face**_ may be done with `_**italics and bold face**_`
* `Inline computer code` or unformatted text is rendered using "backtricks": `` `Inline computer code` ``

#### Advanced character formatting, colorizing, background ?
<!-- package flair, html ..., css -->
<!-- evaluate if it is usefull in this doc or not ! --> 

#### Paragraph styling

* Paragraphs are created by inserting blank lines    
Without a blank line    
```
    Paragraph 1
    Paragraph 2
```    
This will berendered as:    
Paragraph 1
Paragraph 2

With a blank line
```
    Paragraph 1

    Paragraph 2
```    
This will berendered as:

Paragraph 1

Paragraph 2

* Plain code blocks

````
```
This text is displayed verbatim / preformatted
```
````

Or indent by four spaces (the text color in Rstudio editor will change if enough blanks):
````
    This text is displayed verbatim / preformatted
````
 
This will be rendered as follows

```
This text is displayed verbatim / preformatted
```

> --- Remark: the first way of formatting in this case is preferable because it is explicit and well identified in the Rmd file !


#### Lists

Unordered and numbered items lists may be used for structuring text. Nested sub-lists can be also defined using identation.  

Unordered list start with `*`, `-` or `+`

````
* Item 1
   * sub-Item 1
   * sub-Item 2
````

* Item 1
   * sub-Item 1
   * sub-Item 2

Ordered list starts with `1.`, `2.`,... or `1.` for each item.
````
1. Item 1
   1. sub-Item 1
   1. sub-Item 2
2. Item 2
````

1. Item 1
   1. sub-Item 1
   1. sub-Item 2
2. Item 2

Both list types may be mixed together
````
* Item 1
   1. sub-Item 1
      * sub-sub-item 1
      * sub-sub-item 2
   1. sub-Item 2
      * sub-sub-item 1
      * sub-sub-item 2
* Item 2
````

* Item 1
   1. sub-Item 1
      * sub-sub-item 1
      * sub-sub-item 2
   1. sub-Item 2
      * sub-sub-item 1
      * sub-sub-item 2
* Item 2

A new paragraph may be **attached and aligned** to a list item by using four blank spaces at the end of the item title; _**spaces are symbolized with dots**_ in the following example.
````
* Item 1
   * sub-Item 1....
   A new paragraph aligned with the preceeding list item
   * sub-Item 2:
   A paragraph following the preceeding list item    
````

* Item 1
   * sub-Item 1    
   A new paragraph aligned with the preceeding list item
   * sub-Item 2:
   A paragraph following the preceeding list item    
   
   

#### Blockquotes

Blockquotes are used for formatting a text block as a citation text by using `>` and the output depends on the customization of the book style.

A blockquote may be written like this: 

```
> "I thoroughly disapprove of duels. If a man should challenge me,
  I would take him kindly and forgivingly by the hand and lead him
  to a quiet place and kill him."
>
> --- Mark Twain

```

And the output will be rendered as follows:

> "I thoroughly disapprove of duels. If a man should challenge me,
  I would take him kindly and forgivingly by the hand and lead him
  to a quiet place and kill him."
>
> --- Mark Twain


#### Links

##### Static links
Links can be made to web addresses (url) or local files addresses, but the link syntax used is identical

````
   * see rmarkdown documentation on [ Rstudio web site ](http://rmarkdown.rstudio.com)

   * see [ this file ](file:///path/to/file)

````

   * see rmarkdown documentation on [ Rstudio web site ](http://rmarkdown.rstudio.com)

   * see [ this file ](file:///path/to/file)    
   In that case, a full path must be used, for example
     * Under windows: `file:///c:/path/to/file`
     * under unix like systems: `file:///home/path/to/file`    
     * A portable syntax may be used (making links work if knitting the Rmd document on every machi) using directly `html` syntax and a relative path to the file    
   For exemple:
   ````
   See in the BibTex file <a href=R_references.bib>R_references.bib</a>
   ````
   See in the BibTex file <a href=./R_references.bib>R_references.bib</a>
   

##### Arbitrary links
You can also link to [**arbitrary text**]{#atext}. Anchor looks like this: `[arbitrary text]{#atext}`. The hash is required.

Then a link to the previously created anchor is done like this `[reference to arbitrary text](#atext)` and it will be rendered: 
[reference to arbitrary text](#atext)

A link to a word `A` which definition is located somewhere else in the document is done with `[see definition](#A)`: [see definition](#A)

##### Dynamic links using chunks variables

<!-- TODO: fix showing verbation code with ('r file_part') -->

Some content may be defined dynamically by using code chunks, depending on the context (location of a directory for example).

So a file path may be caculated in a code chunk like this:

```{r html_file_path, echo = FALSE, results='hide'}
book_dir_url <- "./../_book"
part_file <- file.path(book_dir_url, "canopy-microclimate.html")
```
<!-- to render verbatim code chunk-->
````
```{r html_file, eval=TRUE}`r ''`
part_file <- file.path(book_dir_url, "canopy-microclimate.html")
```
````

And a Rmarkdown link in the text may be written as follows using the variable `part_file`, previouly calculated in the above chunk code (<span style="background-color: #ffff99;">*Note: for the moment the following example display is not adequate, **no line break needed**, this will be fixed in the future*</span>):

<!-- to render verbatim inline code chunk, but line break is necessary for not evaluating part_file -->
```{r}`r ''`
In the section [canopy-microclimate](`r
part_file`)
```

<!--This will show a verbatim inline R expression `` `r
1+1` `` in the output.-->
<!--````
In the section [microclimate and energy balance](`r part_file`)
````-->


This will be rendered as:

> In the section [canopy-microclimate](`r part_file`)...


#### Foot notes

````
This is a short footnote [^a-note]
This is a short inline footnote ^[an inline footnote description]
This is a long footnote [^a-long-note]

Labelled footnotes content is filled like this on on or multiple lines 
[^a-note]: This is a footnote description

[^a-long-note]: This is a long footnote description

      With idented paragraphs to include them
      
      in the footnote text over multiple lines
      
      * item 1
      * item 2
````

This is a short labeled footnote [^a-note]
This is a short inline footnote ^[This is an inline footnote description]
This is a long labeled footnote [^a-long-note]

[^a-note]: This is a footnote description

[^a-long-note]: This is a long footnote description

      With idented paragraphs to include them
      
      in the footnote text over multiple lines
      
      * item 1
      * item 2


### Document section labelling and references
Sections are defined as chapter and their sub-parts whatever their level is (starting with one `#` or several ones)

#### Automatic labelling
All document sections (including chapters, sub-sections) are automatically `tagged` or labelled internally with a name constructed from their title. 

So, a section written like this: 

```{r eval=FALSE}
### Base text formatting syntax
```
May be rendered (*depending on its location*) like this:

> <h3><span style="background-color: #ffff99;"><span class="header-section-number">2.1</span> Base text formatting syntax</span></h3>

But in fact, it is automatically internally named `base-text-formatting-syntax`, and one can make a reference (link) to it somewhere else in the document like this (*even in another chapter, which exists in another file*):

* Using a link label    
`see in [this section](#base-text-formatting-syntax)`    
will produce a link to the section (*which exists upward in this document*):   

> see in [this section](#base-text-formatting-syntax)

* Automatic link label (section number)    
`see in \@ref(base-text-formatting-syntax)`    
will produce a link to the section (*which exists upward in this document*):    

> see in \@ref(base-text-formatting-syntax)


#### Manual labelling

Manual labelling is also possible by adding a label of the section (`{#label-name-of-the-section}`), whithout any special characters and using lower case letters, besides its title like this:

```{r eval=FALSE}
### Base text formatting syntax {#base-formatting}
```
This label must be informative about the section content and of course must be unique among all Rmd files used for producing the complete document (messages will be displayed if not while the document building).

And the referencing syntax is the same as shown before replacing `base-text-formatting-syntax` with `base-formatting` regarding the abovee example.

> Note that manual referencing is an advantage, because references to sections **will not change**. While with automated referencing, all sections cross-references must be changed in the whole document when sections' title change. 

#### Excluding parts from numbering
To exclude a chapter or section from numbering process, a special label can be set besides its title: `{-}`. This is the case for example for a `preamble` or `preface` section: 

`## Preface {-}`  


### Specificities linked to the `bookdown` package use

In a `bookdown` project, a chapter title is defined by using only **one hashtag** like this
 
`# A chapter title`, which will produce a chapter level title with automatic numbering and automatic referencing. 

Each chapter is stored in a separate Rmd file (and thus contain only one ## title), and its file name starts whith a numerical identifier for classifying chapters in the right order when building the document ^[Another solution exists when chapters files are not numbered by specifying files list in the right order int the `yaml` header of the Index.Rmd file].

As, for chapters, all the subsequent sections will be automatically numbered as in an usual RMarkdown document (if specified in the *yaml* header of the index.Rmd file).

The `bookdown` package offers much more when using specific output formats included in it. Using for example `bookdown::html_document2` or `bookdown::pdf_document2` instead of standard `html` or `pdf` formats, allows to automatically **number figures, tables, equations, ... and crossreferencing these elements**.

Bibliographic citations are also automatically formatted (according to a style, may be changed).

The list of figures, of tables and also the bibliography references list are automatically generated while buiding the document, and therefore updated when elements are modified in the source document (`Rmd file`).

All the formatting syntax are detailed in the next sections.


### Document elements formatting

#### Referencing words ?
**TODO: to be completed**

* This is possible only when using a pdf document output

* Referencing terms : using `\index{word}`

* Citing referenced words

#### Citing bibliographic references    
Bibliographic references are read from a BibTex file (`.bib`) which is being generated from an export from the Zotero base, for example. 

Bibliographic citations are set using references label attached to each reference in the file.    
For example citing in a document the following reference existing in a bib file, two possibilities exist depending on the context.
````
@book{xie_bookdown_2020,
	title = {bookdown: {Authoring} {Books} and {Technical} {Documents} with {R} {Markdown}},
	url = {https://github.com/rstudio/bookdown},
	author = {Xie, Yihui},
	year = {2020}
}
````

Citations may be **manually inserted** using the following syntax:

  * Using citation `[@xie_bookdown_2020]` will produce in the text : <span style="background-color: #ffff99;">[@xie_bookdown_2020]</span>   
  * Without braces `@xie_bookdown_2020`, citation allows to use a reference inside phrases like this: 
````
according to @xie_bookdown_2020, the bookdown package ... 
````
It is rendered as: according to <span style="background-color: #ffff99;">@xie_bookdown_2020</span>, the bookdown package ... 

But, citations may also **be inserted** using the `citr` Rstudio addin helping to find a reference inside a bibtex file and formatting it in the text. This functionnality is accessible through the `Addins` menu, using item `Insert Citations`.


```{r citations-1, out.width='50%', fig.align='left', echo = FALSE}
knitr::include_graphics('images/formatting_guide/citations-1.png')
```

By clicking in the Search references field, the references list is displayed and browsable. But, searching may be perfomed by typing authors names or words from a publication title for example.

```{r citations-2, out.width='70%', fig.align='left', echo = FALSE}
knitr::include_graphics('images/formatting_guide/citations-2.png')
```

When one or several references are selected, citations are inserted  (at the place of the cursor) according to the selected format (with or without braces) in the text by using `Insert citation`.


```{r citations-3, out.width='70%', fig.align='left', echo = FALSE}
knitr::include_graphics('images/formatting_guide/citations-3.png')
```

> <span style="background-color: #ffff99;">TODO</span>: Add link to documentation containing BibTex files format description and file generation .... 



#### Formating and citing equations

Equations are to be written using the Latex syntax (see in [Introduction](#introduction)), and the RStudio addin **Input Latex Math** (In Rstudio IDE use menu: Addins -> `Input Latex Math`) is helpfull for formating them.

Equations may be either written `inline` directly inside the text or using a `Latex` syntax for defining a block that may contain one or several lines.

* Inline equations are written using a leading and ending `$` around the code    

So an equation inserted in piece of text as follows:

```{r eval=FALSE}
The following equation $U_i = \frac{\beta \times A}{T_i}$ is used for ...
```

will be rendered like this:

> The following equation $U_i = \frac{\beta \times A}{T_i}$ is used for ...

More complex or longer equations need to be embedded in a specific equation block, which also allows to label them. In that case equations will be automatically numbered and references to them are easy to do (see below).

* For a single line equation    

The code in the file:
```{r eval=FALSE}
\begin{equation}
ALBEDOLAI(I) = ALBVEG_G - (ALBVEG_G - ALBSOL(I)) \times e^{(-0.75 \times LAI(I))} (\#eq:first-equation)
\end{equation}
```

will produce: 
\begin{equation}
ALBEDOLAI(I) = ALBVEG_G - (ALBVEG_G - ALBSOL(I)) \times e^{(-0.75 \times LAI(I))} (\#eq:first-equation)
\end{equation}


* For a multiple-lines equation

By default when using the **align** environment, each line is numbered. So, for avoiding this and for example giving a unique reference for a set of equations, `\nonumber` is added at the end of each line, except the last one containing the equation reference. A line break is done using `\\`.

```{r eval=FALSE}
\begin{align}
      sin(H)  &= sin(LAT_C) \times sin(DEC) + cos(LAT_C) \times cos(DEC) \times cos(\theta) \nonumber \\
      cos(A)  &= \frac{- cos(LAT_C) \times sin(DEC) + sin(LAT_C) \times cos(DEC) \times cos(\theta)}{cos(H)} \nonumber \\
      tan(H) &= G \times sin(A + ORIENTRANG_T) (\#eq:second-equation)
\end{align}
```
\begin{align}
      sin(H)  &= sin(LAT_C) \times sin(DEC) + cos(LAT_C) \times cos(DEC) \times cos(\theta) \nonumber \\
      cos(A)  &= \frac{- cos(LAT_C) \times sin(DEC) + sin(LAT_C) \times cos(DEC) \times cos(\theta)}{cos(H)} \nonumber \\
      tan(H) &= G \times sin(A + ORIENTRANG_T) (\#eq:second-equation)
\end{align}

With the **array** environment, no need to use *\\nonumber* command, and the equation number
is centered regarding lines block.

```{r eval=FALSE}
\begin{equation}
\begin{array}
      sin(H)  &= sin(LAT_C) \times sin(DEC) + cos(LAT_C) \times cos(DEC) \times cos(\theta) \\
      cos(A)  &= \frac{- cos(LAT_C) \times sin(DEC) + sin(LAT_C) \times cos(DEC) \times cos(\theta)}{cos(H)} \\
      tan(H) &= G \times sin(A + ORIENTRANG_T) (\#eq:second-equation-a)
\end{array}
\end{equation}
```

\begin{equation}
\begin{array}{l}
      sin(H)  &= sin(LAT_C) \times sin(DEC) + cos(LAT_C) \times cos(DEC) \times cos(\theta) \\
      cos(A)  &= \frac{- cos(LAT_C) \times sin(DEC) + sin(LAT_C) \times cos(DEC) \times cos(\theta)}{cos(H)} \\
      tan(H) &= G \times sin(A + ORIENTRANG_T) (\#eq:second-equation-a)
\end{array}
\end{equation}

The **&** sign may be also used to align expressions between lines in both cases.

* Citing equations

The above equations are referenced by using a label directly at the end of the last line with user defined labels like `(\#eq:second-equation)` (must be unique in the whole document).

The label may be used to cite an equation reference like this:

```{r eval=FALSE}
The equation of albedolai (\@ref(eq:first-equation)) is defined using ... 
```

It will be rendered as:

The equation of albedolai \@ref(eq:first-equation) is defined using ...



### Using R code chunks for illustrations generation
R code `chunks` may be used in order to produce figures and other tables from external data or R functions execution.

Formatting options may be set in the individual codes chunks, or overloaded locally if global options are set for the whole document.

#### Naming code chunks

A unique label must be assigned to code chunks in order to make references to these elements. But in any case, it is preferable by default to assign a label to every code chunk.

The content of a figure label is free, but must not contain special characters other than `-`, `_` and numbers are allowed.
It must appear in the first place after `r` in the braces. In the above example the chunk name for the figure is <span style="background-color: #ffff99;">name-of-the-figure</span>:

<!-- chunk display -->
````
```{r name-of-the-figure, ... figure options ..., fig.cap='Figure title ...', echo = FALSE}`r ''`
R code instructions ...
```
````

#### Generating and citing figures

##### Figure options

<!-- TODO
list some options, especially title
abnd give link to options list
BUT, tell that in the future, these options will be defined globally
for a whole document, and will be sepcific to the Stics book according to the 
graphical editorial chart
-->

##### Static figures
Images can be inserted in documents by using the`include_graphics` function by giving to it the image file path.
<!-- TODO
List or link to supported images files formats: png, gif, jpg ...
Local files and web urls !
https://www.r-bloggers.com/wrapper-of-knitrinclude_graphics-to-handle-urls-pdf-outputs
-->

<!-- Static image chunk display --> 
````
```{r fig-img-59, out.width='50%', fig.align='center', fig.cap='Proportion of global radiation (RAINT/TRG) intercepted for wheat, maize and sunflower (EXTIN~P~ values of respectively 0.5, 0.7 and 0.9).', echo = FALSE}`r ''`
knitr::include_graphics('./images/formatting_guide/e9782759201693_i0059.jpg')
```
````

```{r fig-img-59, out.width='50%', fig.align='center', fig.cap='Proportion of global radiation (RAINT/TRG) intercepted for wheat, maize and sunflower (EXTIN~P~ values of respectively 0.5, 0.7 and 0.9).', echo = FALSE}
knitr::include_graphics('./images/formatting_guide/e9782759201693_i0059.jpg')
```

<!-- It will be replaced with a function of the SticsPubRTools later ... -->

##### Dynamic figures
* From data files    
In code chunks, data may be extracted from files (csv, Excel,...), treated and displayed as plots as follows: 
````
```{r fig-rdif-vs-rsrso, echo = FALSE , fig.align='center', fig.cap='Relationship between the diffuse to total radiation ratio (RDIF) and the total to extraterrestrial radiation ratio (RSRSO).' } `r ''`
 d <- read.table("data/RDIF_vs_RSRSO.csv", sep = ";", stringsAsFactors = FALSE, header = TRUE)
plot(x = d$RSRSO, y = d$RDIF, xlab = "RSRSO", ylab = "RDIF", type = "l")
```
````

```{r rdif-vs-rsrso, echo = FALSE , fig.align='center', fig.cap='Relationship between the diffuse to total radiation ratio (RDIF) and the total to extraterrestrial radiation ratio (RSRSO).' } 
 d <- read.table("data/RDIF_vs_RSRSO.csv", sep = ";", stringsAsFactors = FALSE, header = TRUE)
plot(x = d$RSRSO, y = d$RDIF, xlab = "RSRSO", ylab = "RDIF", type = "l")
```

* From equations    
Equations can be implemented first as R functions, in R script files apart from the Rmd file (see \@ref(writing-equations-functions)). They must be first sourced (inside a Rmd file) and can be easily called within Rmd chunks as follows. But, in our case sourcing files contained in the book package `R` folder is automatically done.


<!-- chunk display -->
````
```{r logistic-eq, echo = FALSE , fig.align='center', fig.cap='Logistic curve using default parameters...' }`r ''`
x <- -5:25
plot(x,logistic_fun(x), type = "l", xlab = "x", ylab = "f(x)")
```
````

```{r logistic-eq, echo = FALSE , fig.align='center', fig.cap='Logistic curve using default parameters...' }
x <- -5:25
plot(x,logistic_fun(x), type = "l", xlab = "x", ylab = "f(x)")
```


##### Citing figures

All figures produced into code chunks (with plot, ggplot,...) may be cited in the same way with the syntax `\@ref(fig:chunk-name)`, by using the `fig:` prefix before the chunk name. 

So the link to the previous logistics curve plot (chunk name = logistics-eq) will be set as `\@ref(fig:logistic-eq)` and will be rendered as: \@ref(fig:logistic-eq).

#### Tables
##### Tables building

Tables may be built directly in a RMarkdown document according to Markdown syntax. And any type may be used.

To be able to cross-reference a Markdown table, a labelled caption must be defined in order to insert links.
So, a table and its caption in Rmarkdown looks like this:
````
Table: (\#tab:tab-1) Title of the simple table
Col. A   Col. B
-------- --------
    5.1     3.5
    4.9     10.2
````
And it is rendered as:

Table: (\#tab:tab-1) Title of the simple table

Col. A   Col. B
-------- --------
    5.1     3.5
    4.9     10.2
    


But, this kind of table is hard coded in the document. So, generating tables from data is preferable.

##### Tables generating
Some packages are usefull to render tables, and it is also possible for an html output document to insert dynamic tables ...

For the moment, we will describe basic table formatting with the `knitr` package, **kable** and this will be used as a default. But we mention here other usefull packages for advanced table formatting that may be used later.
Functions are to be used directly in code chunks.

* Using `knitr` **kable** function    
This is the most convenient way for including tables because there are some internal tricks in **knitr** to make it work with **bookdown**


````
```{r tab-2, out.width="50%"}`r ''`
tb <- read.table(file = "./data/table.csv", header = TRUE, stringsAsFactors = FALSE, sep = ";")
tb_col <- c("Physiological function","Trophic stress index")

knitr::kable(tb, col.names = tb_col,
            caption = 'Effect of trophic stress on physiological functions through the various trophic stress indices.', booktabs = TRUE)
```
````

```{r tab-2, out.width="50%", echo=FALSE}
tb <- read.table(file = "./data/table.csv", header = TRUE, stringsAsFactors = FALSE, sep = ";")
tb_col <- c("Physiological function","Trophic stress index")

knitr::kable(tb, col.names = tb_col,
            caption = 'Effect of trophic stress on physiological functions through the various trophic stress indices.', booktabs = TRUE)
```







Tables are automatically numbered using their chunk labels and can be [referenced](#referencing-table) .

* Other packages for advanced formatting
   * Other packages list : https://bookdown.org/yihui/rmarkdown-cookbook/table-other.html
   * kableExtra (https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html, https://bookdown.org/yihui/rmarkdown-cookbook/kableextra.html)
   
      * Using pipe operator : kable(dt) %>% kable_styling()
   
      * using format from other packages, making it compatible with kableExtra:
         from tables: toKable(), from xtable: xtable2kable()
   
   * Gt tables (https://blog.rstudio.com/2020/04/08/great-looking-tables-gt-0-2/)    
   
```{r tab-simple-table-2, out.width="50%"}
names(tb_col) <- names(tb)
tb %>%
gt() %>% 
tab_header(title="Effect of trophic stress on physiological functions through the various trophic stress indices.", subtitle = "Subtitle test") %>%
  cols_label(.list=as.list(tb_col))
```


> Note: A title is inserted in the table header, but for the moment it is not possible to set a reference to that kind of table and cross-referencing it. It will come soon as said here: https://community.rstudio.com/t/how-to-set-a-reference-in-rmarkdown-to-a-gt-table/23781

In the future, the idea is that table functions (to be selected) will be embedded in the **SticsPubRTools** package functions for automatic rendering data tables without giving any specific information (as shown in functions calls) in order to format them in an homogeneous way over the whole document.

##### Referencing a table {#referencing-table}

An automatic label is generated by `kable` with the chunk name. For example, the label of the code chunk named `tab-2` (see upward in \@ref(tables-generating)),  will be `tab:tab-2`. Or for Markdown tables, the label is to be set manually as shown in \@ref(tables-building) (`tab:tab-1` in the example).

The syntax to reference tables is `\@ref(tab:tab-1)` or `\@ref(tab:tab-2)`. And the numbered links to the tables will be rendered respectively as *\@ref(tab:tab-1)* and *\@ref(tab:tab-2)*.


### Writing equations functions

#### Location
R files containing functions are stored in the `R` directory in the root directory of the package (as `data` or `images` folders).

#### Files and content
Creating a source file script containing equations functions (see: example of chapter 7).
For a better or clearer organisation, separate files are to be created by chapter. 

For the chapter 7, the file is named `07-chap-function.R`

So, chapter writers are only working on a file dedicated to their own chapter equations.

Here after is an example of 3 functions gathered in one file. Default parameters values may be set either in the function arguments or given when calling the function inside a code chunk.

```{r functions-script, eval = FALSE}
## Functions of the "R/XX-chap-functions.R" script file
logistic_fun <- function(x, L = 100, k = 0.5, x0 = 10) {
  L/(1 + exp(-k*(x-x0)))
}

x2_fun <- function(x) {
  x^2
}

sqrt_fun <- function(x) {
  if (x < 0) return(NA)
  sqrt(x)
}
```


#### Availability for calls into the book parts
Every R file in the book package R directory will be automatically sourced while compiling the book document and all the functions are callable from R code chunks inserted in every Rmd document, __*even if functions are loaded from the `Index.Rmd` master file*__.

#### Usage
Nothing particular is to be done in code chunks, but just:

* create functions inputs (variables, parameters)
* call functions
* use function outputs for creating plots, tables,...

For doing this, refer to the section: [Dynamic figures](#dynamic-figures)



## Building the book

The functions used for generating html documents or other format, for the whole book or some parts are embeded in a function belonging to the `SticsPubRTools` which have been normally installed previously (see [R software prerequisites](#soft-pre-tips)

### The outpout formats
Rmarkdown offers the possibility to produce output documents in several format: gitbook, pdf book, html book, epub book, even word book.
But, here specific builtin Bookdown format will be used because they offer specific capabilities, as crossreferencing things, and others. 

So, Bookdown formats `html_document2` and `pdf_document2` are used.

### Generating the whole book 
#### From Rstudio

The book can be produced using the `Build Book` button inside the `Build` tab within the Environement, History, ... pane (see \@ref(fig:build-book-rstudio)).
A previewing windows is automatically opened. But the output file may be viewed inside a web browser by clicking on `Open in Browser`.

```{r build-book-rstudio, out.width='50%', fig.align='center', fig.cap='Rstudio `Build Book` button', echo = FALSE}
knitr::include_graphics('./images/formatting_guide/build_book_rstudio.png')
```


#### On the command line

The [`SticsPubRTools`](https://github.com/SticsRPacks/SticsPubRTools) package provides a `generate_book` function for generating a `bookdown` document.

From the directory of the book package (i.e. corresponding to which is got using the `getwd()` command) or from any other directory given as an argument to the `generate_book` function.

```{r eval=FALSE}
library(SticsPubRTools)

## Building the whole book
generate_book()
## or 
generate_book(book_pkg_dir = "/path/to/book/package")

```

### Previewing specific book parts
<!-- TODO : describe use cases for generating and updating the specific parts 
and mention that if other parts remain empty or don't contains parts, equations, and so on 
cited in the part to be regenerated the numbering will be broken -->

It is possible to render and preview only a specific chapter, the one your are focusing or working on. For that purpose, the command `generate_book()` can be also used.

For example if you want to generate only the chapter 07 as a html file, just pass the file name as follows, considering that the current directory is the package one:

```{r eval=FALSE}
generate_book("07-chap.Rmd")
```
This will generate all the html book files, but only the desired one will contain its content. 
<!-- TODO: check if the links to other parts if any existing content in the other parts are preserved ? -->
<!-- TODO : use later the function of the SticsPubRTools allowing to generate the full book or parts -->
```{r calc_html_path, echo=FALSE, results='hide'}
part_file <- file.path(book_dir_url, "_book","microclimate-and-energy-balance.html")
print(part_file)
```

The generated file is stored in a `_book` sub-directory of the book package directory. So, the above example will produce the file [microclimate-and-energy-balance.html](`r part_file`).

But the output may not be accurate because cross-references to other chapters will not work in that particular case.

It is also possible with the same command to generate, several html files by giving to the function a list of files as follows:

<!-- TODO : use later the function of the SticsPubRTools allowing to generate the full book or parts -->
```{r eval=FALSE}
generate_book(c("index.Rmd", "07-chap.Rmd")
```

For a specific output format other than the html one, using the pdf one:
```{r eval=FALSE}
generate_book(output_format = "bookdown::pdf_document2")
```





## Glossary definition
[A]{#A}: definition of A

[AA]{#AA}: definition of AA

[Z]{#Z}: definition of Z






