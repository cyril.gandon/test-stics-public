![Bandeau Stics](doc/images/bandeau-stics-readme.jpg)

# STICS (Simulateur mulTIdisciplinaire pour les Cultures Standard)

Le modèle Stics est un modèle de culture dynamique, générique et robuste permettant de simuler le système sol-atmosphère-culture, reconnu internationalement.

https://stics.paca.hub.inrae.fr/


## Content

This repository is composed of 3 Fortran programs:
 - `/stics` contains the main program and the source of STICS
 - `/pre_stics` is a preprocessor. It generates useful fortran files based on list of inputs and outputs variables contained in `/doc/inputs.csv`, `/doc/outputs.csv` and `/doc/profil.csv`
 - `/e2e` is the program that executes the end to end (e2e) tests.


## Compilation

To compile STICS, the preprocessor and the e2e, the preferred method is with the [Fortran Package Manager (fpm)](https://fpm.fortran-lang.org/). See the installation guide [here](https://fpm.fortran-lang.org/install/index.html).

Once fpm is installed, you can use various command:
 - `fpm install` generates the executable
 - `fpm run` launch the program
 - `fpm test` launch the unit tests

The best way to verify that fpm and STICS have been correctly installed on a machine is to run the e2e tests. It will run the preprocessor, then it will run STICS multiple times, once for each test, and check that outputs are the same as the expected outputs.

```
cd e2e
fpm run
```
