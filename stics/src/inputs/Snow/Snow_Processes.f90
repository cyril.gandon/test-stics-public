module Snow_Processes

implicit none

real, parameter :: pi = 3.1415926536

contains 

subroutine snow_process_3(Snowaccu,Snowmelt,Sdepth,preciprec, &
& tminrec,tmaxrec,Sdry,Swet,ps, &
& P_tsmax,P_trmax,P_DKmax,P_Kmin,P_Tmf,P_SWrf,P_Pns, &
& P_E,P_prof,P_tminseuil,P_tmaxseuil,tmax,tmin,jul,precip,Sdry_in, &
& Swet_in,ps_in,Sdepth_in,in_debug)

implicit none


! Output variables for current day
real, intent(out) :: Snowaccu ! snowfall accumulation (mm water equivalent)*
real,  intent(out) :: Snowmelt ! snowmelt (mm water equivalent)*
real,  intent(out) :: Sdepth ! snow cover depth (m)*
real,  intent(out) :: preciprec ! recalculated precipitation (mm)*
real,  intent(out) :: tminrec ! recalculated minimum temperature (°C)*
real,  intent(out) :: tmaxrec ! recalculated maximum temperature (°C)*

! Updated values for current day, init for next day call
real,  intent(out) :: Sdry ! water in solid state in the snow cover (mm)
real,  intent(out) :: Swet ! water in liquid state in the snow cover (mm)
real,  intent(out) :: ps ! density of snow cover (kg m-3) 


! Parameters
real , intent(in) :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (°C)
real , intent(in) :: P_trmax ! tmax above which all precipitation is assumed to be rain (°C)
real , intent(in) :: P_DKmax ! difference between the maximum and the minimum melting rates (mm °C-1 day-1)
real , intent(in) :: P_Kmin ! minimum melting rate on 21 December (mm °C-1 day-1)
real , intent(in) :: P_Tmf ! threshold temperature for snow melting (°C)
real , intent(in) :: P_SWrf ! degree-day temperature index for refreezing (mm °C-1 day-1)
real , intent(in) :: P_Pns ! density of the new snow(kg m-3)
real , intent(in) :: P_E ! snow compaction parameter (mm mm-1 day-1)
real , intent(in) :: P_prof ! snow cover threshold for snow insulation (cm)
real , intent(in) :: P_tminseuil ! minimum temperature when snow cover is higher than prof
real , intent(in) :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof

! Input variables
integer, intent(in) :: jul ! = current day of year for the calculation
real , intent(in) :: tmax ! = current maximum air temperature (°C) (lue dans le fichier climatique)
real , intent(in) :: tmin ! = current minimum air temperature (°C)(lue dans le fichier climatique)
real , intent(in) :: precip ! = current precipitation (mm)(lue dans le fichier climatique)

! Snow cover variables input values: previous day values
real , intent(in) :: Sdry_in ! water in solid state in the snow cover (mm)
real , intent(in) :: Swet_in ! water in liquid state in the snow cover (mm)
real , intent(in) :: ps_in ! density of snow cover (kg m-3) 
real , intent(in) :: Sdepth_in ! snow cover depth (m)*

! For debuggging
logical,optional, intent(in) :: in_debug


! Local variables 
real :: tavg ! average temperature (min+max)/2
real :: M ! snow in the process of melting (mm day-1)
real :: Mrf ! liquid water in the snow cover in the process of refreezing (mm day-1)
real :: K ! melting rate (mm °C-1 day-1)
real :: tmp_sdry,tmp_swet,frac_sdry
real :: fs ! fraction of snow
real :: Sdepth_cm ! snow depth (cm)
logical :: debug

debug=.FALSE.
if (present(in_debug)) then
     debug=in_debug
endif

if (debug) then
   print *, "Begin computing snow process 3"
endif

! Initializations
! Local variables
tavg=0.
M=0.
Mrf=0.
K=0.
tmp_sdry=0.
tmp_swet=0.
frac_sdry=0.
fs=0.
Sdepth_cm=0.
! Output variables
Snowaccu=0.
Snowmelt=0.
Sdepth=0.
preciprec=precip
tminrec=tmin
tmaxrec=tmax
Sdry=0.
Swet=0.
ps=0.


!print *, "snow process 3 : P_tsmax:",P_tsmax
!print *, "snow process 3 : jul :",jul
!print *, "snow process 3 : tmin :",tmin
!print *, "snow process 3 : tmax :",tmax
!print *, "snow process 3 : precip :",precip

! Calculating average temperature
tavg = (tmax+tmin)/2.
!print *, "snow process 3 : tavg :",tavg

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                     Snow variables calculation                           !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Snow accumulation (unit cm)
if (tmax.lt.P_tsmax) fs=1.
if ((tmax.ge.P_tsmax).and.(tmax.le.P_trmax)) then
    fs=(P_trmax-tmax)/(P_trmax-P_tsmax)
endif
Snowaccu=fs*precip  ! /P_Pns ! see Pns =100
!print *, "snow process 3 : after Snowaccu calculation"


! M calculation
K=(P_DKmax/2.)*(-SIN((2.*pi*real(jul)/366.)+(9./16.)*pi)) & 
& +P_Kmin+(P_DKmax/2.)
!print *, "snow process 3 : K :",K
!print *, "snow process 3 : after K calculation"
if ( tavg.gt.P_Tmf ) then
    M = K * ( tavg - P_Tmf )
end if

!print *, "snow process 3 : after M calculation"


! Mrf calculation
if ( tavg.lt.P_Tmf ) then 
    Mrf = P_SWrf * ( P_Tmf - tavg )
end if


! Snow dry  calculation
if (M.le.Sdry_in) then 
    tmp_sdry=Snowaccu+Mrf-M+Sdry_in
    if (tmp_sdry.lt.0.) then
        Sdry=0.001
    else
        Sdry=tmp_sdry
    endif
endif
!print *, "snow process 3 : after Sdry calculation"

! Snow wet calculation
if (Mrf.le.Swet_in) then
    tmp_swet=Swet_in+(precip-Snowaccu)+M-Mrf
    frac_sdry=0.1*Sdry
    if (tmp_swet.lt.frac_sdry) then
        Swet=tmp_swet
    else 
        Swet=frac_sdry
    endif
endif


! ps calculation
! Sdepth_in =  = day before, Sdepth current day value
!if( Sdepth_in.ne.0.) then 
if ( ABS(Sdepth_in).gt.1.0E-8 ) then
    !if( ( Sdry_in + Swet_in ).ne.0. ) then
    if ( ABS( Sdry_in +  Swet_in ).gt.1.0E-8 ) then
        ps = ( Sdry_in +  Swet_in )  / Sdepth_in
    else
        ps=ps_in
    endif
endif


! Snow melt calculation
!if( ps.ne.0.) then
if( ABS(ps).gt.1.0E-8 ) then
    Snowmelt = M  / ps
end if


! Snow depth calculation
if(Snowmelt.le.(Sdepth_in+Snowaccu/100)) then 
    Sdepth=(Snowaccu/100+Sdepth_in-Snowmelt-(Sdepth_in*P_E))!*100. ! cf Pns = 100
endif



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                Meteorological data calculation                           !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Precipitations
if ((Sdry+Swet).lt.(Sdry_in+Swet_in)) then
    ! CASE 1: preciprec calculation according
    ! to XL snow model version October 2014
    ! preciprec=(Sdry_in+Swet_in)-(Sdry+Swet)-Mrf
    !
    ! CASE 2: preciprec calculation according
    ! to XL snow model version march 2015
    preciprec=preciprec+(Sdepth_in-Sdepth)*100-Mrf
endif
preciprec=preciprec-Snowaccu

! Sdepth unit transformation m -> cm
Sdepth_cm=Sdepth*P_Pns
!
! Minimum temperature calculation
if (Sdepth_cm.gt.P_prof) then
    if(tmin.lt.P_tminseuil) then
        tminrec=P_tminseuil
    else
        if (tmin.gt.P_tmaxseuil) then
            tminrec=P_tmaxseuil
        ! done in tminrec init
        !else
            !tminrec=tmin
        endif
    endif
else
    if (Sdepth_cm.gt.0.) then
        tminrec=P_tminseuil-(1-(Sdepth_cm/P_prof)) &
        *(ABS(tmin)+P_tminseuil)
    ! done in tminrec init
    !else
        !tminrec=tmin
    endif
endif


! Maximum temperature  calculation
if (Sdepth_cm.gt.P_prof) then
    if(tmax.lt.P_tminseuil) then
        tmaxrec=P_tminseuil
    else
        if (tmax.gt.P_tmaxseuil) then
            tmaxrec=P_tmaxseuil
        ! done in tmaxrec init
        !else
            !tmaxrec=tmax
        endif
    endif
else
    if (Sdepth_cm.gt.0.) then
    if (tmax.le.0.) then
            tmaxrec=P_tmaxseuil-(1-(Sdepth_cm/P_prof))*(-tmax)
        else
            tmaxrec=0.
        endif
    ! done in tmaxrec init
    !else
        !tmaxrec=tmax
    endif
endif

!print *, "End snow process 3"

end subroutine snow_process_3


end module Snow_Processes
