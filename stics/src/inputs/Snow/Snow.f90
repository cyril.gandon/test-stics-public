module Snow

  USE Snow_Processes
  use iso_c_binding !, only: c_double, c_int,c_float
  use messages
  implicit none
  
  integer,parameter  :: models_nb=3
  

  ! Definition de la structure du snow (variables d'etat et parametres)
  type snowvar_
     ! Variables
     real  :: Snowaccu ! daily snowfall accumulation (mm water equivalent)
     real  :: Snowmelt ! daily snowmelt (mm water equivalent)
     real  :: Sdepth ! snow cover depth (m)
     real  :: preciprec ! recalculated daily precipitation (mm)
     real  :: tminrec ! recalculated daily minimum temperature (°C)
     real  :: tmaxrec ! recalculated daily maximum temperature (°C) 
     real :: Sdry ! water in solid state in the snow cover (mm)
     real :: Swet ! water in liquid state in the snow cover (mm)
     real :: ps ! density of snow cover (kg m-3) 
     
     ! cumulatives data
     integer :: ndays_sdepth_over_3cm ! number of days with a snow depth over 3 cm
     integer :: ndays_sdepth_over_10cm ! number of days with a snow depth over 10 cm
     
     ! Parameters
     real :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (°C)
     real :: P_trmax ! tmax above which all precipitation is assumed to be rain (°C)
     real :: P_DKmax ! difference between the maximum and the minimum melting rates (mm °C-1 day-1)
     real :: P_Kmin ! minimum melting rate on 21 December (mm °C-1 day-1)
     real :: P_Tmf ! threshold temperature for snow melting (°C)
     real :: P_SWrf ! degree-day temperature index for refreezing (mm °C-1 day-1)
     real :: P_Pns ! density of the new snow(kg m-3)
     real :: P_E ! snow compaction parameter (mm mm-1 day-1)
     real :: P_prof ! snow cover threshold for snow insulation (cm)
     real :: P_tminseuil ! minimum temperature when snow cover is higher than prof (°C)
     real :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof (°C)
     
     ! added for testing if in vars can be updated from state vars
     logical :: computed  
  end type snowvar_
  
  ! definition de la structure des entrees externes du snow
  type snowin_
     integer :: model_code
     character(len=30) ::  snow_model
     real, pointer  :: tmax ! = daily maximum air temperature (°C) (lue dans le fichier climatique)
     real, pointer  :: tmin ! = daily minimum air temperature (°C)(lue dans le fichier climatique)
     real, pointer  :: precip ! = daily precipitation (mm)(lue dans le fichier climatique)
     !real, pointer :: jul ! = day number in year
     integer, pointer :: jul ! = day number in year
     integer, pointer :: year ! =  year
     integer, pointer :: month ! = month number in year
     integer, pointer :: day ! = day number in month
     ! for snow cover init
     real :: Sdry ! water in solid state in the snow cover (mm)
     real :: Swet ! water in liquid state in the snow cover (mm)
     real :: ps ! density of snow cover (kg m-3) 
     real :: Sdepth ! snow cover depth (m)
  end type snowin_
  
  
  type snow_
        type(snowin_)  :: in
        type(snowvar_)  :: var
  end type snow_
  

  
  type snow_models_
     character(len=30) :: name(models_nb)
     character(len=30) :: tag(models_nb)
     integer :: code(models_nb)
     integer :: number
  end type snow_models_
  
 logical :: proc_debug = .FALSE.
 ! keeping models list (with name, id)
 type(snow_models_) :: models

  PUBLIC :: zero_snow, init_snow, compute_snow,disp_snowout,disp_snowpar
  PUBLIC :: read_par_file_3, check_snow_3,get_default_par_3
  PUBLIC :: write_to_file !,write_to_screen
contains

  ! fonction de mise a zero des structures
  subroutine zero_snow(snow_s)
    type(snow_), intent(out)  :: snow_s

     !print *, 'Begin zero_snow'

     ! State variables
     snow_s%var%Snowaccu=0.
     snow_s%var%Snowmelt=0.
     snow_s%var%Sdepth=0.
     snow_s%var%preciprec=0.
     snow_s%var%tminrec=0.
     snow_s%var%tmaxrec=0.
     snow_s%var%Sdry=0.
     snow_s%var%Swet=0.
     snow_s%var%ps=0.
     
     ! Cumulative variables
     snow_s%var%ndays_sdepth_over_3cm=0
     snow_s%var%ndays_sdepth_over_10cm=0
     
     ! Parameters
     snow_s%var%P_tsmax=0.
     snow_s%var%P_trmax=0.
     snow_s%var%P_DKmax=0.
     snow_s%var%P_Kmin=0.
     snow_s%var%P_Tmf=0.
     snow_s%var%P_SWrf=0.
     snow_s%var%P_Pns=0.
     snow_s%var%P_E=0.
     snow_s%var%P_prof=0.
     snow_s%var%P_tminseuil=0.
     snow_s%var%P_tmaxseuil=0.
     
     ! input variables
     ! pointers init (time & meteo)
     snow_s%in%tmax => NULL() 
     snow_s%in%tmin => NULL() 
     snow_s%in%precip => NULL() 
     snow_s%in%jul => NULL() 
     snow_s%in%year => NULL() 
     snow_s%in%month => NULL() 
     snow_s%in%day => NULL() 
     
     ! previous day variables init
     snow_s%in%Sdry=0.
     snow_s%in%Swet=0.
     snow_s%in%ps=0.
     snow_s%in%Sdepth=0.

     ! numerical model code
     snow_s%in%model_code=0
     
     ! Setting computed status
     snow_s%var%computed=.FALSE.
     
     !print *, 'End zero_snow'
     
  end subroutine  zero_snow

  ! tmax,tmin,precip,jul
  ! year,month,day
  subroutine connect_snow_tmax(snow_s, tmax)
    type(snow_), intent(inout)  :: snow_s
    real, intent(in), target :: tmax
    snow_s%in%tmax => tmax
  end subroutine connect_snow_tmax
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_tmax(snow_s, tmax)
     type(snow_), intent(inout)  :: snow_s
     real(c_float), intent(in),target :: tmax
     snow_s%in%tmax => tmax
  end subroutine c_connect_snow_tmax
  
  subroutine connect_snow_tmin(snow_s, tmin)
    type(snow_), intent(inout)  :: snow_s
    real, intent(in), target :: tmin
    snow_s%in%tmin => tmin
  end subroutine connect_snow_tmin
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_tmin(snow_s, tmin)
     type(snow_), intent(inout)  :: snow_s
     real(c_float), intent(in),target :: tmin
     snow_s%in%tmin => tmin
  end subroutine c_connect_snow_tmin
  
  subroutine connect_snow_precip(snow_s, precip)
    type(snow_), intent(inout)  :: snow_s
    real, intent(in), target :: precip
    snow_s%in%precip => precip
  end subroutine connect_snow_precip
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_precip(snow_s, precip)
     type(snow_), intent(inout)  :: snow_s
     real(c_float), intent(in),target :: precip
     snow_s%in%precip => precip
  end subroutine c_connect_snow_precip
  
  subroutine connect_snow_jul(snow_s, jul)
    type(snow_), intent(inout)  :: snow_s
    integer, intent(in), target :: jul
    snow_s%in%jul => jul
  end subroutine connect_snow_jul
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_jul(snow_s, jul)
     type(snow_), intent(inout)  :: snow_s
     integer(c_int), intent(in),target :: jul
     snow_s%in%jul => jul
  end subroutine c_connect_snow_jul
  
  subroutine connect_snow_year(snow_s, year)
    type(snow_), intent(inout)  :: snow_s
    integer, intent(in), target :: year
    snow_s%in%year => year
  end subroutine connect_snow_year
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_year(snow_s, year)
     type(snow_), intent(inout)  :: snow_s
     integer(c_int), intent(in),target :: year
     snow_s%in%year => year
  end subroutine c_connect_snow_year
  
  subroutine connect_snow_month(snow_s, month)
    type(snow_), intent(inout)  :: snow_s
    integer, intent(in), target :: month
    snow_s%in%month => month
  end subroutine connect_snow_month
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_month(snow_s, month)
     type(snow_), intent(inout)  :: snow_s
     integer(c_int), intent(in),target :: month
     snow_s%in%month => month
  end subroutine c_connect_snow_month
  
  subroutine connect_snow_day(snow_s, day)
    type(snow_), intent(inout)  :: snow_s
    integer, intent(in), target :: day
    snow_s%in%day => day
  end subroutine connect_snow_day
  
  ! for using from C code!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine c_connect_snow_day(snow_s, day)
     type(snow_), intent(inout)  :: snow_s
     integer(c_int), intent(in),target :: day
     snow_s%in%day => day
  end subroutine c_connect_snow_day

subroutine connect_snow_all(snow_s,year,month,day,jul,tmax,tmin,precip)
    type(snow_), intent(inout)  :: snow_s
    integer, intent(in), target :: year,month,day,jul
    real, intent(in), target :: tmax,tmin,precip
    call connect_snow_tmax(snow_s,tmax)
    call connect_snow_tmin(snow_s,tmin)
    call connect_snow_precip(snow_s,precip)
    call connect_snow_jul(snow_s,jul)
    call connect_snow_year(snow_s,year)
    call connect_snow_month(snow_s,month)
    call connect_snow_day(snow_s,day)
end subroutine connect_snow_all
  
subroutine is_connected_var(connected,snow_s,varname)
    logical , intent(out)  :: connected
    type(snow_), intent(in)  :: snow_s
    !real, intent(in) :: targ
    character(len=*), intent(in) :: varname
    connected=.FALSE.
    select case (trim(varname))
        case ("tmin")
            ! print *, 'address snow_s%in%tmin :',snow_s%in%tmin
            if (associated(snow_s%in%tmin)) then
               connected=.TRUE.
               !print *, 'snow_s%in%tmin is currently associated'
            endif
        case ("tmax")
            ! print *, 'address snow_s%in%tmax :',snow_s%in%tmax
            if (associated(snow_s%in%tmax)) then
               connected=.TRUE.
               !print *, 'snow_s%in%tmax is currently associated'
            endif
        case ("precip")
            ! print *, 'address snow_s%in%precip :',snow_s%in%precip
            if (associated(snow_s%in%precip)) then
               connected=.TRUE.
               !print *, 'snow_s%in%precip is currently associated'
            endif
        case ("jul")
            ! print *, 'address snow_s%in%jul :',snow_s%in%jul
            if (associated(snow_s%in%jul)) then
               connected=.TRUE.
               !print *, 'snow_s%in%jul is currently associated'
            endif
        case ("year")
            ! print *, 'address snow_s%in%year :',snow_s%in%year
            if (associated(snow_s%in%year)) then
               connected=.TRUE.
               !print *, 'snow_s%in%year is currently associated'
            endif   
        case ("month")
            ! print *, 'address snow_s%in%month :',snow_s%in%month
            if (associated(snow_s%in%month)) then
               connected=.TRUE.
               !print *, 'snow_s%in%month is currently associated'
            endif   
        case ("day")
            ! print *, 'address snow_s%in%day :',snow_s%in%day
            if (associated(snow_s%in%day)) then
               connected=.TRUE.
               !print *, 'snow_s%in%day is currently associated'
            endif   
        case default
            print *, 'Unknown structure field in snow structure'
    end select
end subroutine is_connected_var
  
  subroutine set_models()
    ! setting models list 
    models%name(1)="snow_process_3"
    models%tag="default"
    models%code(1)=3
    models%number=1
  end subroutine set_models

  ! Snow structure initialization
  subroutine init_snow(logger, snow_s,codemodl,Sdry,Swet,ps,Sdepth, &
    P_tsmax,P_trmax,P_DKmax,P_Kmin,P_Tmf,P_SWrf,P_Pns,P_E,P_prof, &
    P_tminseuil,P_tmaxseuil) !,abandon)

    type(logger_), intent(in) :: logger
    type(snow_), intent(inout)  :: snow_s
    character(len=30) ::  snow_model
    integer, intent(in)   :: codemodl
    ! day-1 init values
    real, intent(in) :: Sdry !  water in solid state in the snow cover (mm)
    real, intent(in) :: Swet ! water in liquid state in the snow cover (mm)
    real, intent(in) :: ps ! density of snow cover (kg m-3) 
    real, intent(in) :: Sdepth ! snow cover depth (m)
    ! Parameters
    real, intent(in) :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (°C)
    real, intent(in) :: P_trmax ! tmax above which all precipitation is assumed to be rain (°C)
    real, intent(in) :: P_DKmax ! difference between the maximum and the minimum melting rates (mm °C-1 day-1)
    real, intent(in) :: P_Kmin ! minimum melting rate on 21 December (mm °C-1 day-1)
    real, intent(in) :: P_Tmf ! threshold temperature for snow melting (°C)
    real, intent(in) :: P_SWrf ! degree-day temperature index for refreezing (mm °C-1 day-1)
    real, intent(in) :: P_Pns ! density of the new snow(kg m-3)
    real, intent(in) :: P_E ! snow compaction parameter (mm mm-1 day-1)
    real, intent(in) :: P_prof ! snow cover threshold for snow insulation (cm)
    real, intent(in) :: P_tminseuil ! minimum temperature when snow cover is higher than prof
    real, intent(in) :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof
    
    ! local
    real :: snowpar(11)
    
    !print *, 'Begin init_snow'
    
    call zero_snow(snow_s)
    
   
    ! Filling models structure of the module
    call set_models()

   ! Getting model name based on code in snow_model
    call get_model(logger, codemodl,snow_model)
    snow_s%in%snow_model=snow_model
    
    ! inputs
    snow_s%in%Sdry=Sdry
    snow_s%in%Swet=Swet
    snow_s%in%ps=ps
    snow_s%in%Sdepth=Sdepth
    
    snowpar(1:11) = (/ P_tsmax,P_trmax,P_DKmax,P_Kmin, &
                         P_Tmf,P_SWrf,P_Pns,P_E,P_prof, &
                         P_tminseuil,P_tmaxseuil /)
    
    !if (all(snowpar.eq.0)) then
    if (all(snowpar.lt.1.0E-8)) then
       call set_default_par_3(snow_s)
    else
    ! variables
    snow_s%var%P_tsmax=P_tsmax
    snow_s%var%P_trmax=P_trmax
    snow_s%var%P_DKmax=P_DKmax
    snow_s%var%P_Kmin=P_Kmin
    snow_s%var%P_Tmf=P_Tmf
    snow_s%var%P_SWrf=P_SWrf
    snow_s%var%P_Pns=P_Pns
    snow_s%var%P_E=P_E
    snow_s%var%P_prof=P_prof
    snow_s%var%P_tminseuil=P_tminseuil
    snow_s%var%P_tmaxseuil=P_tmaxseuil
    end if
    
    ! validation of snow model parameter values 
    call check_snow_3(snow_s)
  
   !print *, 'End init_snow'
  end subroutine  init_snow

  subroutine get_model(logger, codemodl,model_name)
   type(logger_), intent(in) :: logger
  ! getting model name from code, in models structure
     integer :: codemodl
     character(len=30) :: model_name
     integer :: i_find
     logical :: found
     i_find = 1 
     found = .FALSE.
     do while (i_find <= models%number)
     ! if snow model is not set, getting default model name
      if (codemodl==0.and.models%tag(i_find)=='default') then 
         found = .TRUE.
         exit
      end if
      ! the searched code has been found
      if (codemodl==models%code(i_find)) then
         found = .TRUE.
         exit
      end if
      i_find=i_find+1
     end do
     ! raising error if model code not found
     if (.NOT.found) then
        call exit_error(logger, 'Error in getting snow model name: ' // to_string(codemodl) // ' is an unknown code')     
     end if
     ! finally getting the found name
     model_name=trim(models%name(i_find))
     return
  end subroutine get_model
  
  subroutine compute_snow(logger, snow_s)
   type(logger_), intent(in) :: logger
  ! Computing temperatures, rainfall,... 
    type(snow_), intent(inout)  :: snow_s
    logical :: connected(4)
    logical :: debug
    integer :: out_cum_days
    debug=.TRUE.
    out_cum_days=0
    
    call is_connected_var(connected(1),snow_s,"tmin")
    
    call is_connected_var(connected(2),snow_s,"tmax")
    
    call is_connected_var(connected(3),snow_s,"precip")
    
    !call is_connected_var(connected,snow_s,"year")
    
    call is_connected_var(connected(4),snow_s,"jul")
    
    ! Returning execution error when missing connection(s)
    if (.NOT.all(connected)) then
      call exit_error(logger, 'Missing connection for variable(s) in snow_s%in structure, aborting !')
    endif
    
    !print *, 'Begin compute_snow'
    
    select case (trim(snow_s%in%snow_model))
        case ("snow_process_3")
            ! print *, "snow_process_3"
            ! snow_s%in : Sdepth, Sdry, Swet, ps are updated bfore call
            ! for day-1
            call snow_process_3(snow_s%var%Snowaccu,snow_s%var%Snowmelt, & 
            & snow_s%var%Sdepth,snow_s%var%preciprec, &
            & snow_s%var%tminrec,snow_s%var%tmaxrec,snow_s%var%Sdry, &
            & snow_s%var%Swet,snow_s%var%ps,snow_s%var%P_tsmax, &
            & snow_s%var%P_trmax,snow_s%var%P_DKmax, snow_s%var%P_Kmin, &
            & snow_s%var%P_Tmf,snow_s%var%P_SWrf,snow_s%var%P_Pns,&
            & snow_s%var%P_E,snow_s%var%P_prof,snow_s%var%P_tminseuil,&
            & snow_s%var%P_tmaxseuil,snow_s%in%tmax,snow_s%in%tmin, & 
            & snow_s%in%jul,snow_s%in%precip,snow_s%in%Sdry,snow_s%in%Swet, &
            & snow_s%in%ps,snow_s%in%Sdepth,proc_debug)
    
           
        case default
            call exit_error(logger, 'Error, snow model unknown: ' // trim(snow_s%in%snow_model))
    end select
    
    ! Compute done status
    snow_s%var%computed=.TRUE.
    
    ! For resetting to 0 cumulative variables if jul == 1
    if ( snow_s%in%jul.lt.2 ) then
       snow_s%var%ndays_sdepth_over_3cm = 0
       snow_s%var%ndays_sdepth_over_10cm = 0
    end if

    ! Computing cumulative variables
    !print *, "Sdepth : ",snow_s%var%Sdepth
    !print *, "in cum days snow 3cm :", snow_s%var%ndays_sdepth_over_3cm
    call calc_days_over_depth(snow_s%var%Sdepth,0.03,snow_s%var%ndays_sdepth_over_3cm,out_cum_days)
    snow_s%var%ndays_sdepth_over_3cm = out_cum_days

    call calc_days_over_depth(snow_s%var%Sdepth,0.1,snow_s%var%ndays_sdepth_over_10cm,out_cum_days)
    snow_s%var%ndays_sdepth_over_10cm = out_cum_days
    
    ! for initializing %in with %var for next day
    call update_snow_in(logger, snow_s)
    
    !print *, 'End compute_snow'
  end subroutine  compute_snow


  subroutine update_snow_in(logger, snow_s)
    ! For updating inputs for day n+1 with computed variables for day n  
    type(logger_), intent(in) :: logger
    type(snow_), intent(inout)  :: snow_s
    if (.NOT.snow_s%var%computed) then
        call exit_error(logger, 'Error updating input vars from outputs : compute must be executed before ...')
    end if
    
    snow_s%in%Sdepth=snow_s%var%Sdepth
    snow_s%in%Sdry=snow_s%var%Sdry
    snow_s%in%Swet=snow_s%var%Swet
    snow_s%in%ps=snow_s%var%ps
    
    ! update done, setting computed status back to .FALSE.
    snow_s%var%computed=.FALSE.
    
  end subroutine update_snow_in


!   subroutine write_to_screen(snow_s,j)
!    type(snow_), intent(in)  :: snow_s
!    integer, intent(in) :: j
!    ! print *, 'OUTPUT DATA'
!     !do j = 1, snow_s%var%nj
!      ! print *,'j=',j,', Snowaccu= ',snow_s%var%Snowaccu
!     !end do
!   end subroutine write_to_screen

  ! ACTUALLY ONLY ONE DAY IS WRITTEN FOR EACH CALL
  !subroutine write_to_file(snow_s,file_name,j)
  subroutine write_to_file(snow_s,j,fileunit)
   type(snow_), intent(in)  :: snow_s
   !character(len=200) :: file_name
   character(len=1)  :: sep
   integer :: j,fileunit
   
   sep=';'
   !fileunit=11
111 format('Doe;Year;Month;Day;Jul;Snowaccu;Snowmelt;Sdepth;tminrec;tmaxrec;preciprec',$)
   !open(fileunit,file=file_name,position='append')
   if (j.eq.1) then
      write(fileunit,111)
      write(fileunit,*)
   endif 
    !do j = 1, snow_s%var%nj
      !write(fileunit,*) j,sep,snow_s%var%year,sep,snow_s%var%month,sep,snow_s%var%day, &
        !                 sep,snow_s%var%jul,sep,snow_s%var%Snowaccu,sep,snow_s%var%Snowmelt, &
        !                 sep,snow_s%var%Sdepth,sep,snow_s%var%tminrec,sep,snow_s%var%tmaxrec,&
        !                 sep,snow_s%var%preciprec
      ! jul, year,month, day as in vars
      write(fileunit,*) j,sep,snow_s%in%year,sep,snow_s%in%month,sep,snow_s%in%day, &
                         sep,snow_s%in%jul,sep,snow_s%var%Snowaccu,sep,snow_s%var%Snowmelt, &
                         sep,snow_s%var%Sdepth,sep,snow_s%var%tminrec,sep,snow_s%var%tmaxrec,&
                         sep,snow_s%var%preciprec
      ! print *, j,sep,snow_s%in%year,sep,snow_s%in%month,sep,snow_s%in%day, &
        !                 sep,snow_s%in%jul
    !end do
    !close(fileunit)
  end subroutine write_to_file
  
  
  subroutine disp_snowout(snow_s,j)
   type(snow_), intent(in)  :: snow_s
   integer, intent(in) :: j
   write(*,*) '----------------------------------------'
   print *, 'Day from beginning : ', j
   print *, 'Snowaccu :',snow_s%var%Snowaccu
   print *, 'Snowmelt :',snow_s%var%Snowmelt
   print *, 'Sdepth :',snow_s%var%Sdepth
   print *, 'preciprec :',snow_s%var%preciprec
   print *, 'tminrec :',snow_s%var%tminrec
   print *, 'tmaxrec :',snow_s%var%tmaxrec
   write(*,*) '----------------------------------------'
  end subroutine disp_snowout
  
  subroutine disp_snowpar(snow_s)
   type(snow_), intent(in)  :: snow_s
   write(*,*) '----------------------------------------'
   print *, 'Parameters :'
   print *, 'P_tsmax :',snow_s%var%P_tsmax
   print *, 'P_trmax :',snow_s%var%P_trmax
   print *, 'P_DKmax :',snow_s%var%P_DKmax
   print *, 'P_Kmin :',snow_s%var%P_Kmin
   print *, 'P_Tmf :',snow_s%var%P_Tmf
   print *, 'P_SWrf :',snow_s%var%P_SWrf
   print *, 'P_Pns :',snow_s%var%P_Pns
   print *, 'P_E :',snow_s%var%P_E
   print *, 'P_prof :',snow_s%var%P_prof
   print *, 'P_tminseuil :',snow_s%var%P_tminseuil
   print *, 'P_tmaxseuil :',snow_s%var%P_tmaxseuil
   write(*,*) '----------------------------------------'
  end subroutine disp_snowpar


 subroutine check_snow_3(snow_s)
   type(snow_), intent(in)  :: snow_s
   logical :: chk(11),check
   chk(:)=.FALSE.
   check=.FALSE.
   ! logical value .TRUE. means out of bounds
     !chk(1)=snow_s%var%P_tsmax.LE.0
     !chk(2)=snow_s%var%P_trmax.LE.0
     chk(1)=snow_s%var%P_tsmax.GT.snow_s%var%P_trmax
     chk(2)=snow_s%var%P_trmax.LT.snow_s%var%P_tsmax
     chk(3)=snow_s%var%P_DKmax.LE.0.
     chk(4)=snow_s%var%P_Kmin.LE.0.
     chk(5)=snow_s%var%P_Tmf.LE.0.
     chk(6)=snow_s%var%P_SWrf.LE.0.
     chk(7)=snow_s%var%P_Pns.LE.0.
     chk(8)=snow_s%var%P_E.LE.0.
     chk(9)=snow_s%var%P_prof.LE.0.
     !chk(10)=snow_s%var%P_tminseuil.LE.0.
     !chk(11)=snow_s%var%P_tmaxseuil.LE.0.
     chk(10)=snow_s%var%P_tminseuil.GT.snow_s%var%P_tmaxseuil
     chk(11)=snow_s%var%P_tmaxseuil.LT.snow_s%var%P_tminseuil
  if (check) then
        write(*,*) '----------------------------------------'
        print *, 'Wrong parameters values' 
        print *, 'P_tsmax :',chk(1)
        print *, 'P_trmax :',chk(2)
        print *, 'P_DKmax :',chk(3)
        print *, 'P_Kmin :',chk(4)
        print *, 'P_Tmf :',chk(5)
        print *, 'P_SWrf :',chk(6)
        print *, 'P_Pns :',chk(7)
        print *, 'P_E :',chk(8)
        print *, 'P_prof :',chk(9)
        print *, 'P_tminseuil :',chk(10)
        print *, 'P_tmaxseuil :',chk(11)
        write(*,*) '----------------------------------------'
   
   endif
  end subroutine check_snow_3

 subroutine get_default_par_3(P_tsmax,P_trmax,P_DKmax,  &
    P_Kmin,P_Tmf,P_SWrf,P_Pns,P_E,P_prof,P_tminseuil,P_tmaxseuil)
    ! Getting default set of parameters values
    ! Parameters
    real, intent(out)  :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (°C)
    real, intent(out)  :: P_trmax ! tmax above which all precipitation is assumed to be rain (°C)
    real, intent(out)  :: P_DKmax ! difference between the maximum and the minimum melting rates (mm °C-1 day-1)
    real, intent(out)  :: P_Kmin ! minimum melting rate on 21 December (mm °C-1 day-1)
    real, intent(out)  :: P_Tmf ! threshold temperature for snow melting (°C)
    real, intent(out)  :: P_SWrf ! degree-day temperature index for refreezing (mm °C-1 day-1)
    real, intent(out)  :: P_Pns ! density of the new snow(kg m-3)
    real, intent(out)  :: P_E ! snow compaction parameter (mm mm-1 day-1)
    real, intent(out)  :: P_prof ! snow cover threshold for snow insulation (cm)
    real, intent(out)  :: P_tminseuil ! minimum temperature when snow cover is higher than prof
    real, intent(out)  :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof
    
    P_tsmax = -2.
    P_trmax = 1.
    P_DKmax = 1.5
    P_Kmin = 2.
    P_Tmf = 0.5
    P_SWrf = 0.01
    P_Pns = 100.
    P_E = 0.02
    P_prof = 10.
    P_tminseuil = -0.5
    P_tmaxseuil = 0.
    
 end subroutine get_default_par_3
 
 subroutine set_default_par_3(snow_s)
    type(snow_), intent(inout)  :: snow_s
    ! Parameters
    real :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (°C)
    real :: P_trmax ! tmax above which all precipitation is assumed to be rain (°C)
    real :: P_DKmax ! difference between the maximum and the minimum melting rates (mm °C-1 day-1)
    real :: P_Kmin ! minimum melting rate on 21 December (mm °C-1 day-1)
    real :: P_Tmf ! threshold temperature for snow melting (°C)
    real :: P_SWrf ! degree-day temperature index for refreezing (mm °C-1 day-1)
    real :: P_Pns ! density of the new snow(kg m-3)
    real :: P_E ! snow compaction parameter (mm mm-1 day-1)
    real :: P_prof ! snow cover threshold for snow insulation (cm)
    real :: P_tminseuil ! minimum temperature when snow cover is higher than prof
    real :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof
    
    
    ! Getting default parameters values
    call get_default_par_3(P_tsmax,P_trmax,P_DKmax,  &
    P_Kmin,P_Tmf,P_SWrf,P_Pns,P_E,P_prof,P_tminseuil,P_tmaxseuil)
    
    ! filling structure
    snow_s%var%P_tsmax=P_tsmax
    snow_s%var%P_trmax=P_trmax
    snow_s%var%P_DKmax=P_DKmax
    snow_s%var%P_Kmin=P_Kmin
    snow_s%var%P_Tmf=P_Tmf
    snow_s%var%P_SWrf=P_SWrf
    snow_s%var%P_Pns=P_Pns
    snow_s%var%P_E=P_E
    snow_s%var%P_prof=P_prof
    snow_s%var%P_tminseuil=P_tminseuil
    snow_s%var%P_tmaxseuil=P_tmaxseuil
 
 
 end subroutine set_default_par_3

 subroutine read_par_file_3(fileunit,P_tsmax,P_trmax,P_DKmax,  &
    P_Kmin,P_Tmf,P_SWrf,P_Pns,P_E,P_prof,P_tminseuil,P_tmaxseuil,  &
    abandon)
    integer, intent(in) :: fileunit
    integer, intent(inout) :: abandon
    character(len=25) :: vartmp
  
    ! Parameters
    real, intent(out)  :: P_tsmax ! maximum daily air temperature (tmax) below which all precipitation is assumed to be snow (°C)
    real, intent(out)  :: P_trmax ! tmax above which all precipitation is assumed to be rain (°C)
    real, intent(out)  :: P_DKmax ! difference between the maximum and the minimum melting rates (mm °C-1 day-1)
    real, intent(out)  :: P_Kmin ! minimum melting rate on 21 December (mm °C-1 day-1)
    real, intent(out)  :: P_Tmf ! threshold temperature for snow melting (°C)
    real, intent(out)  :: P_SWrf ! degree-day temperature index for refreezing (mm °C-1 day-1)
    real, intent(out)  :: P_Pns ! density of the new snow(kg m-3)
    real, intent(out)  :: P_E ! snow compaction parameter (mm mm-1 day-1)
    real, intent(out)  :: P_prof ! snow cover threshold for snow insulation (cm)
    real, intent(out)  :: P_tminseuil ! minimum temperature when snow cover is higher than prof
    real, intent(out)  :: P_tmaxseuil  ! maximum temperature when snow cover is higher than prof
    
    ! Getting default parameters values
    call get_default_par_3(P_tsmax,P_trmax,P_DKmax,  &
    P_Kmin,P_Tmf,P_SWrf,P_Pns,P_E,P_prof,P_tminseuil,P_tmaxseuil)
    
    ! Reading parameters values from file, overloading 
    ! default values if the parameter name is detected !
    do while (.TRUE.)
      read (fileunit,*,err=250,end=150) vartmp
      print *, vartmp
      if (vartmp == 'tsmax') then
         read (fileunit,*,err=250,end=150) P_tsmax
         print *, P_tsmax
         CYCLE
      endif
      
      if (vartmp == 'trmax') then
         read (fileunit,*,err=250,end=150) P_trmax
         print *, P_trmax
         CYCLE
      endif
      
      if (vartmp == 'DKmax') then
         read (fileunit,*,err=250,end=150) P_DKmax
         print *, P_DKmax
         CYCLE
      endif
      
      if (vartmp == 'Kmin') then
         read (fileunit,*,err=250,end=150) P_Kmin
         print *, P_Kmin
         CYCLE
      endif
      
      if (vartmp == 'Tmf') then
         read (fileunit,*,err=250,end=150) P_Tmf
         print *, P_Tmf
         CYCLE
      endif
      
      if (vartmp == 'SWrf') then
         read (fileunit,*,err=250,end=150) P_SWrf
         print *, P_SWrf
         CYCLE
      endif
      
      if (vartmp == 'Pns') then
         read (fileunit,*,err=250,end=150) P_Pns
         print *, P_Pns
         CYCLE
      endif
      
      if (vartmp == 'E') then
         read (fileunit,*,err=250,end=150) P_E
         print *, P_E
         CYCLE
      endif
      
      if (vartmp == 'prof') then
         read (fileunit,*,err=250,end=150) P_prof
         print *, P_prof
         CYCLE
      endif
      
      if (vartmp == 'tminseuil') then
         read (fileunit,*,err=250,end=150) P_tminseuil
         print *, P_tminseuil
         CYCLE
      endif
      
      if (vartmp == 'tmaxseuil') then
         read (fileunit,*,err=250,end=150) P_tmaxseuil
         print *, P_tmaxseuil
         CYCLE
      endif
    end do
    return

150 continue
    print *, 'abandon 1: FIN DE FICHIER PAR ATTEINTE'
    abandon = 1
    return

250 continue
    print *, 'abandon 2: ERREUR DE LECTURE DU FICHIER PAR'
    abandon = 2
    return
  
  end subroutine read_par_file_3
  
  subroutine read_init_file(fileunit,P_snowcode,snow_model,climatic_file_name,  &
  snowpar_file_name,file_name_0,file_name_1,snow_out_file,Sdepth,    &
  Sdry,Swet,ps,nj, abandon)
    integer, intent(in):: fileunit
    integer, intent(out):: P_snowcode ! module use activation
    character(len=200), intent(out) ::  climatic_file_name,snowpar_file_name,snow_out_file
    character(len=200), intent(out) :: file_name_0,file_name_1 ! ,snow_model
    character(len=200) :: vartmp
    real, intent(out) :: Sdry ! water in solid state in the snow cover (mm)
    real, intent(out) :: Swet ! water in liquid state in the snow cover (mm)
    real, intent(out) :: ps ! density of snow cover (kg m-3) 
    real, intent(out) :: Sdepth ! snow cover depth (cm)
    integer, intent(out) :: nj ! number of days to treat
    integer, intent(inout) :: abandon
    integer:: snow_model
    abandon=0
    
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) P_snowcode
     !print *, P_snowcode
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) snow_model
     !print *, snow_model
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) climatic_file_name
     print *, climatic_file_name
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) snowpar_file_name
     print *, snowpar_file_name
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) file_name_0
     !print *, file_name_0
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) file_name_1
     !print *, file_name_1
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) snow_out_file
     !print *, snow_out_file
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) Sdepth
     !print *, Sdepth
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) Sdry
     !print *, Sdry
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) Swet
     !print *, Swet
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) ps
     !print *, ps
     read (fileunit,*,err=250,end=150) vartmp
     read (fileunit,*,err=250,end=150) nj
     print *, nj

     return

150 continue
    print *, 'abandon 1: FIN DE FICHIER INI ATTEINTE'
    print *, 'erreur sur ',vartmp
    abandon=1
    return

250 continue
    print *, 'abandon 2: ERREUR DE LECTURE DU FICHIER INI'
    abandon=2
    return
  
  end subroutine read_init_file
  
  
  subroutine calc_days_over_depth(depth,depth_threshold,in_cum_days,out_cum_days)
  ! Calculating counts of days when snow depth is over a threshold depth
     real, intent(in)   :: depth
     real, intent(in)   :: depth_threshold
     integer, intent(in) :: in_cum_days
     integer, intent(out) :: out_cum_days
     ! getting previous day count
     out_cum_days=in_cum_days
     
     if (depth > depth_threshold) then
        out_cum_days=in_cum_days + 1
     endif
     
  end subroutine calc_days_over_depth
  
  
end module Snow

