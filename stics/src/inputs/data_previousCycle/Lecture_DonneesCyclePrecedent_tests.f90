module Lecture_DonneesCyclePrecedent_tests
  use testdrive, only: error_type, unittest_type, new_unittest, check

  use Lecture_DonneesCyclePrecedent_m
  use Ecriture_DonneesFinDeCycle_m
  use Stics
  use Parametres_Generaux
  use Plante
  use Root
  use Itineraire_Technique
  use Sol
  use messages
  implicit none

contains
  subroutine collect(testsuite)
     type(unittest_type), allocatable, intent(out) :: testsuite(:)

     testsuite = [ &
                 new_unittest("test_Lecture_DonneesCyclePrecedent_banana", test_Lecture_DonneesCyclePrecedent_banana) &
                 , new_unittest("test_Lecture_DonneesCyclePrecedent_empty", test_Lecture_DonneesCyclePrecedent_empty) &
                 ]

  end subroutine

  !> Write an empty recup.tmp file and try to read it
  subroutine test_Lecture_DonneesCyclePrecedent_empty(error)
    type(error_type), allocatable, intent(out) :: error
    type(logger_) :: logger
    type(Stics_Communs_) :: sc  
    type(Parametres_Generaux_) :: pg  
    type(Plante_) :: p(1)
    type(Root_) :: r(1)
    type(ITK_) :: itk(1)  
    type(Sol_) :: soil
    character(len=255) :: cwd
    character(:), allocatable :: path

    sc%P_nbplantes = 1
    sc%n = 1
    logger = get_silent_logger()

    call getcwd(cwd)
    path = join_path(trim(cwd),'src','inputs','data_previousCycle','test_data','recup.tmp.txt')
    call Ecriture_DonneesFinDeCycle(logger, path, sc, pg, p, r, itk, soil)
    call Lecture_DonneesCyclePrecedent(logger, path, sc, pg, p, r, itk, soil)
    call erase_path(path)
  end subroutine

  !> Read a recup.tmp file generated with a banana usm
  subroutine test_Lecture_DonneesCyclePrecedent_banana(error)
    type(error_type), allocatable, intent(out) :: error
    type(logger_) :: logger
    type(Stics_Communs_) :: sc  
    type(Parametres_Generaux_) :: pg  
    type(Plante_) :: p(1)
    type(Root_) :: r(1)
    type(ITK_) :: itk(1)  
    type(Sol_) :: soil
    character(len=255) :: cwd
    character(:), allocatable :: path

    sc%P_nbplantes = 1
    sc%n = 1
    sc%P_iwater = 301
    sc%NH = 5
    logger = get_silent_logger()

    call getcwd(cwd)
    path = join_path(trim(cwd),'src','inputs','data_previousCycle','test_data','recup_banana.tmp.txt')
    call Lecture_DonneesCyclePrecedent(logger, path, sc, pg, p, r, itk, soil)
  end subroutine
end module

