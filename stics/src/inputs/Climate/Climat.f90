! Module climat
! - Description of the struture Climat_
! - reading of meteorologicals datas
module Climat
   use messages
   use dates_utils, only: days_count
   use climate_file_m

   implicit none

!DR 17/09/2012 je remplace le 731 par nb_days_max
   integer, parameter :: nb_days_max = 731  ! maximum size of the table climate
! DR 30112020 merge trunk
   integer, parameter :: nb_years_max = 100

   real, parameter :: snow_cum_days_threshold = 0. ! number of days for indicating significant lasting of snow depth (over 3 and 10 cm)

   type Climat_
   !> Mean temperature by year (year, mean)
   real, allocatable :: tmoy_an(:, :)
   !> All rows data of the input climate file
   type(climate_file_row_), allocatable :: rows(:)

!: tableaux de transition
!  real, dimension(:), allocatable :: ttrr    ! (731)
!  real, dimension(:), allocatable :: ttmin   ! (731)
!  real, dimension(:), allocatable :: ttmax   ! (731)
!  real, dimension(:), allocatable :: ttrg    ! (731)
!  real, dimension(:), allocatable :: ttetp   ! (731)
!  real, dimension(:), allocatable :: ttpm    ! (731)
!  real, dimension(:), allocatable :: ttvent  ! (731)
!  real, dimension(:), allocatable :: ttco2   ! (731)

!: tableaux finaux
!  real, dimension(:), allocatable :: tetp    ! (731) (possible 0:731, car climabri a besoin de t(n-1). Qd n = 1, pas de tetp(0).)
!  real, dimension(:), allocatable :: trr     ! (731)
!  real, dimension(:), allocatable :: tmoy    ! (0:731)
!  real, dimension(:), allocatable :: tmin    ! (731)
!  real, dimension(:), allocatable :: tmax    ! (731)
!  real, dimension(:), allocatable :: trg     ! (731)
!  real, dimension(:), allocatable :: tpm     ! (731)
!  real, dimension(:), allocatable :: tvent   ! (731)

!: pour les cultures sous abri
!  real, dimension(:), allocatable :: trrext  ! (731)
!  real, dimension(:), allocatable :: tmoyext ! (0:731)
!  real, dimension(:), allocatable :: tminext ! (731)
!  real, dimension(:), allocatable :: tmaxext ! (731)
!  real, dimension(:), allocatable :: trgext  ! (731)
!  real, dimension(:), allocatable :: tpmext  ! (731)

!: tableaux de transition
      real, dimension(nb_days_max) :: ttrr    ! (nb_days_max)
      real, dimension(nb_days_max) :: ttmin   ! (nb_days_max)
      real, dimension(nb_days_max) :: ttmax   ! (nb_days_max)
      real, dimension(nb_days_max) :: ttrg    ! (nb_days_max)
      real, dimension(nb_days_max) :: ttetp   ! (nb_days_max)
      real, dimension(nb_days_max) :: ttpm    ! (nb_days_max)
      real, dimension(nb_days_max) :: ttvent  ! (nb_days_max)
      real, dimension(nb_days_max) :: ttco2   ! (nb_days_max)

!: tableaux finaux
      real, dimension(nb_days_max) :: tetp    ! (nb_days_max) (possible 0:731, car climabri a besoin de t(n-1). Qd n = 1, pas de tetp(0).)        // OUTPUT // Efficient potential evapotranspiration (entered or calculated) // mm day-1
      real, dimension(nb_days_max) :: trr     ! (nb_days_max)     // OUTPUT // Rainfall  // mm.day-1
      real, dimension(0:nb_days_max) :: tmoy    ! (0:nb_days_max)     // OUTPUT // Mean active temperature of air // degree C
      real, dimension(nb_days_max) :: tmin    ! (nb_days_max)     // OUTPUT // Minimum active temperature of air // degree C
      real, dimension(nb_days_max) :: tmax    ! (nb_days_max)        // OUTPUT // Maximum active temperature of air // degree C
      real, dimension(nb_days_max) :: trg     ! (nb_days_max)        // OUTPUT // Active radiation (entered or calculated) // MJ.m-2
      real, dimension(nb_days_max) :: tpm     ! (nb_days_max)        // OUTPUT // Vapour pressure in air // mbars
      real, dimension(nb_days_max) :: tvent   ! (nb_days_max)        // OUTPUT // Mean speed of B2vent // m.s-1
!: DR - 23/09/2014 on le declare en reel comme les autres sinon pbs dans le calcu de fco2s
      real, dimension(nb_days_max) :: co2     ! ! (nb_days_max)      // OUTPUT // CO2 concentration // ppm

!: pour les cultures sous abri
      real, dimension(nb_days_max) :: trrext  ! (nb_days_max)
      real, dimension(0:nb_days_max) :: tmoyext ! (0:nb_days_max)        // OUTPUT // Mean temperature of external air // degree C
      real, dimension(nb_days_max) :: tminext ! (nb_days_max)        // OUTPUT // Minimum temperature of external air // degree C
      real, dimension(nb_days_max) :: tmaxext ! (nb_days_max)        // OUTPUT // Maximum temperature of external air // degree C
      real, dimension(nb_days_max) :: trgext  ! (nb_days_max)        // OUTPUT // Exterior radiation // MJ.m-2
      real, dimension(nb_days_max) :: tpmext  ! (nb_days_max)

      integer :: nitetcult(0:nb_days_max)    ! // OUTPUT // Number of iterations to calculate TCULT // SD

      real :: tutilrnet
      real :: difftcult
      real :: daylen
      real :: humimoy
      real :: humair    ! // OUTPUT // Air moisture // 0-1
      real :: phoi          ! // OUTPUT // Photoperiod // hours
      real :: phoi_veille   ! // OUTPUT // Photoperiod // hours
      real :: etpp(0:nb_days_max)    ! // OUTPUT // Potential evapotranspiration as given by Penman formula // mm day-1

!: ML - 29/10/12 - calcul de la duree d'humectation
      real :: dureehumec ! // OUTPUT // wetness duration    // hour
      real :: dureeRH1
      real :: dureeRH2
      real :: dureeRH ! // OUTPUT //duration of night relative humidity higher than a given threshold   // hour
!  integer :: compteurhumheure
!: ML fin

!: DR - 09/01/06 - rajout de variables pour rapport
      real :: Ctculttout    ! // OUTPUT // Crop temperature (TCULT) integrated over the simulation period //  degree C
      real :: Ctairtout
      real :: somdifftculttair    ! // OUTPUT // Cumulated temperature differences (TCULT-TAIR) over the simulation period //  degree C
      !dr 14/09/2012 inutiles
!  real :: somtroseecult    ! // OUTPUT // Cumulated dew point temperatures (from TCULT) over the simulation period //  degree C
!  real :: somtroseeair    ! // OUTPUT // Cumulated dew point temperatures (from TAIR) over the simulation period //  degree C
      real :: Ctetptout    ! // OUTPUT // Potential evapotranspiration (PET) integrated over the simulation period // mm
      real :: Cetmtout    ! // OUTPUT // Maximum evapotranspiration integrated over the simulation period // mm
      real :: Crgtout    ! // OUTPUT // Global radiation integrated over the simulation period // Mj/m2

!: DR - 08/09/06
      real :: tncultmat    ! // OUTPUT // Average of minimum crop temperatures (TCULTMIN) between LAX and REC // degree C
      real :: amptcultmat     ! // OUTPUT // Mean range of tcult between lax and rec // degree C

      integer :: dureelaxrec
      integer :: nbjechaudage     ! // OUTPUT // Number of shrivelling days between LAX and REC // jours

      integer :: julfin
      integer :: julzero

! Je cree aussi des variables pour stocker les jours, mois et annees de debut et fin de fichier
      integer :: jourzero
      integer :: jourfin
      integer :: moiszero
      integer :: moisfin
      integer :: anneezero
      integer :: anneefin

      real :: humair_percent    ! // OUTPUT // Air moisture // %

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ! Snow module daily outpouts
      real, dimension(nb_days_max) :: Snowaccu !< // OUTPUT // daily snowfall accumulation // mm water equivalent

      real, dimension(nb_days_max)  :: Snowmelt !< // OUTPUT // daily snowmelt // mm water equivalent

      real, dimension(nb_days_max)  :: Sdepth !< // OUTPUT // snow cover depth // m

      real, dimension(nb_days_max)  :: preciprec !< // OUTPUT // recalculated daily precipitation //  mm

      real, dimension(nb_days_max)  :: tminrec !< // OUTPUT // recalculated daily minimum temperature // degree C

      real, dimension(nb_days_max)  :: tmaxrec !< // OUTPUT // recalculated daily maximum temperature // degree C

      ! for snow cover output: for keeping last value for current year

      real  :: Sdry ! water in solid state in the snow cover (mm)

      real  :: Swet ! water in liquid state in the snow cover (mm)

      real  :: ps ! density of snow cover (kg m-3)
      ! Snow module cumulative outputs
      integer :: ndays_sdepth_over_3cm(2) ! number of days with a snow depth over 3 cm
      integer :: ndays_sdepth_over_10cm(2) ! number of days with a snow depth over 10 cm

   end type Climat_

contains

   !! Fill the climatic data with a subset of climat file corresponding to current year
   subroutine fill_climate_values(logger, c, P_culturean, last_index)
      type(logger_), intent(in) :: logger
      type(Climat_), intent(inout) :: c
      integer, intent(in)    :: P_culturean  ! // PARAMETER // crop status 1 = over 1 calendar year ,other than 1  = on two calendar years (winter crop in northern hemisphere) // code 0/1 // P_USM/USMXML // 0
      integer, intent(inout) :: last_index

      integer :: i, k, i_2nd_year, last_jday
      integer :: year1_days, year2_days
      character(len=30) :: station
      type(climate_file_row_), allocatable :: rows(:)

      rows = c%rows
      station = rows(last_index)%station
      c%anneezero = rows(last_index)%year
      c%moiszero = rows(last_index)%month
      c%jourzero = rows(last_index)%day
      c%julzero = rows(last_index)%julian

      year1_days = days_count(c%anneezero)
      ! lecture de la premiere annee
      do i = c%julzero, year1_days
         last_jday = i
         c%ttmin(i) = rows(last_index)%ttmin
         c%ttmax(i) = rows(last_index)%ttmax
         c%ttrg(i) = rows(last_index)%ttrg
         c%ttetp(i) = rows(last_index)%ttetp
         c%ttrr(i) = rows(last_index)%ttrr
         c%ttvent(i) = rows(last_index)%ttvent
         c%ttpm(i) = rows(last_index)%ttpm
         c%ttco2(i) = rows(last_index)%ttco2
         last_index = last_index + 1
         if(last_index > size(rows)) then
            exit
         end if
      end do

      ! if crop on multiple year
      if (P_culturean /= 1) then
         i_2nd_year = last_index
         year2_days = days_count(c%anneezero + 1)
         do k = 1, year2_days
            last_jday = last_jday + 1
            c%ttmin(last_jday) = rows(i_2nd_year)%ttmin
            c%ttmax(last_jday) = rows(i_2nd_year)%ttmax
            c%ttrg(last_jday) = rows(i_2nd_year)%ttrg
            c%ttetp(last_jday) = rows(i_2nd_year)%ttetp
            c%ttrr(last_jday) = rows(i_2nd_year)%ttrr
            c%ttvent(last_jday) = rows(i_2nd_year)%ttvent
            c%ttpm(last_jday) = rows(i_2nd_year)%ttpm
            c%ttco2(last_jday) = rows(i_2nd_year)%ttco2

            i_2nd_year = i_2nd_year + 1
            if(i_2nd_year > size(rows)) then
               exit
            end if
         end do
      end if

      c%julfin = last_jday
   end subroutine

   subroutine writeSnowStateVariables(path, c, first_sim_day, last_sim_day)
      character(*), intent(in) :: path
      type(Climat_), intent(in) :: c
      integer, intent(in) :: first_sim_day
      integer, intent(in) :: last_sim_day
      integer :: day_idx
      integer :: unit, file_closed

      open (newunit=unit, file=path, status=unknown, action=write_)

      day_idx = last_sim_day - first_sim_day + 1
      write (unit, *) c%Sdepth(day_idx), c%Sdry, c%Swet, c%ps

      close (unit)

   end subroutine writeSnowStateVariables

   subroutine readSnowStateVariables(Sdepth, Sdry, Swet, ps)
      real, intent(out) :: Sdry ! water in solid state in the snow cover (mm)
      real, intent(out) :: Swet ! water in liquid state in the snow cover (mm)
      real, intent(out) :: ps ! density of snow cover (kg m-3)
      real, intent(out) :: Sdepth ! snow cover depth (m)

      integer :: unit, file_closed

      open (newunit=unit, file=swow_file, status=unknown, action=read_)

      Sdepth = 0.
      Sdry = 0.
      Swet = 0.
      ps = 0.
      read (unit, *) Sdepth, Sdry, Swet, ps

      close (unit)

   end subroutine readSnowStateVariables

end module Climat

