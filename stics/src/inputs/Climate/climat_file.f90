module climate_file_m
    use stics_system
    use messages
    use messages_data
    use stics_files
 
    implicit none
 
    type climate_file_row_
       character(len=30) :: station
       integer :: year, month, day, julian
       !> minimum active temperature of atmosphere (degreeC)
       real :: ttmin
       !> maximum active temperature of atmosphere (degreeC)
       real :: ttmax
       !> active radiation (entered or calculated) (MJ.m-2)
       real :: ttrg
       !> efficient potential evapotranspiration (entered or calculated) (mm.d-1)
       real :: ttetp
       !> daily rainfall (mm.d-1)
       real :: ttrr
       !> mean daily wind speed at 2 m high above soil (m.s-1)
       real :: ttvent
       !> water vapour pressure in air (hPa)
       real :: ttpm
       !> CO2 concentration (ppm)
       real :: ttco2
    end type
 
 contains
 
    function read_climate_file(logger, path) result(rows)
       type(logger_), intent(in) :: logger
       character(*), intent(in) :: path
 
       logical :: exists
       integer :: iostat, rows_count, iostat_row, i, unit
       type(climate_file_row_), allocatable :: rows(:)
 
       exists = path_exist(path)
       if (.not. exists) then
          call exit_error(logger, "Climate file doesn't exist: [" // path // "]")
       end if
 
       open (newunit=unit, file=path, status=old, action=read_, iostat=iostat)
       if (iostat .ne. 0) then
          call exit_error(logger, "Error opening climate file: [" // path // "]")
       end if

       rows_count = number_of_rows(unit)

       allocate(rows(rows_count))

       do i=1,rows_count
        read (unit, *, iostat=iostat_row) &
        rows(i)%station, rows(i)%year, rows(i)%month, rows(i)%day, rows(i)%julian, rows(i)%ttmin, rows(i)%ttmax, &
        rows(i)%ttrg, rows(i)%ttetp, rows(i)%ttrr, rows(i)%ttvent, rows(i)%ttpm, rows(i)%ttco2

        if (iostat_row .ne. 0) then
            call exit_error(logger, "Error reading line number ["//to_string(i)//"] of climate file: [" // path // "]")
         end if

         if (rows(i)%ttmin <= -999 .or. rows(i)%ttmax <= -999 .or. rows(i)%ttrg <= -999 .or. rows(i)%ttrr <= -999) then
            call EnvoyerMsgHistorique(logger, MISSING_CLIMATE_VALUE)
            call EnvoyerMsgHistorique(logger, trim(rows(i)%station)//", "//to_string(rows(i)%year)//", "//&
                to_string(rows(i)%month)//", "//to_string(rows(i)%day))
         end if
       end do

       close(unit)
    end function
 end module
 
 