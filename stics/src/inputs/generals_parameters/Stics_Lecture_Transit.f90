!*********************************************************************
!     lecture et initialisation des parametres nouvelle version
!     en attente d'etre redirige dans leurs fichiers respectifs
!     fichier paramv6.par
!     23/11/07 ajout des parametres d'adaptation au CC de la MO
!********************************************************************
! subroutine of general parameters v6
! - reading of the generals parameters of the file tempoparv6.sti
module Stics_Lecture_Transit_m
   use stics_system
   use Stics
   use messages
   use messages_data
   use stics_files
   implicit none
   private
   public :: Stics_Lecture_Transit
contains
   type(Stics_Transit_) function Stics_Lecture_Transit(logger, path, P_nbplantes) result(t)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      integer, intent(in)::P_nbplantes

      character(len=30) :: nomVar

      logical :: file_open, exists
      integer :: fpar, iostat
      exists = path_exist(path)

      if (.not. exists) then
         call exit_error(logger, "New formalisms parameters file doesn't exist: [" // path // "]")
      end if

      open (newunit=fpar, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening new formalisms parameters file: [" // path // "]")
      end if

      t = transit_ctor()

! DR 08/11/2016 on ne evut pas pouvoir utiliser les formalismes d'effet du CC sur la denit et nit avant que joel revisit les formalismes
      t%P_code_adaptCC_miner = 2
      t%P_code_adaptCC_nit = 2
      t%P_code_adaptCC_denit = 2
      t%P_code_adapt_MO_CC = 2

      ! lecture des parametres
!specificites cultures fauchees
!***********************************
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codetempfauche
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_coefracoupe(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_coefracoupe(2)
!specificites Quinoa
!***********************************
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_codepluiepoquet
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_nbjoursrrversirrig
!dynamique des talles
!***********************
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_swfacmin
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codetranspitalle
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codedyntalle(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SurfApex(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilMorTalle(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SigmaDisTalle(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_VitReconsPeupl(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilReconsPeupl(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_MaxTalle(1)
!! dr 10/06/2010 nouevaux parametres
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilLAIapex(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_tigefeuilcoupe(1)
! DR et ML et SYL 15/06/09
! ************************
! introduction de la fin des modifications de Sylvain (nadine et FR)
! dans le cadre du projet PERMED
! ### SYL 19-02-2009
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codedyntalle(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SurfApex(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilMorTalle(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SigmaDisTalle(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_VitReconsPeupl(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilReconsPeupl(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_MaxTalle(2)
! ! dr 10/06/2010 nouevaux parametres
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_SeuilLAIapex(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_tigefeuilcoupe(2)
! ###
! DR et ML et SYL 15/06/09 FIN introduction de la fin des modifications de Sylvain
! DR et ML et SYL 15/06/09
! ************************
! introduction de la fin des modifications de Sylvain (nadine et FR)
! dans le cadre du projet PERMED
! #### SYL 26/02/2009

!Deplafonnement des reserves pour le cycle reproducteur
!*******************************************************
! DR 10/06/2010 on indexe sur le nb de plantes
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resplmax(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resplmax(2)
!calcul du stade de debut montaison pour les prairies perennes
!**************************************************************
! DR 10/06/2010 nouveaux parametres de 71
! DR et ML et SYL 16/06/09
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codemontaison(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codemontaison(2)
! ####
! DR et ML et SYL 15/06/09 FIN introduction de la fin des modifications de Sylvain
!Prise en compte du CC sur les matieres organiques
!****************************************************
! DR 08/11/2016 je ne les lis plus je mets les codes =2 en constante  en attendant que joel et bruno regardent les formailsmes de jorge de plus pres
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adapt_MO_CC
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_periode_adapt_CC
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_an_debut_serie_histo
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_an_fin_serie_histo
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_param_tmoy_histo
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adaptCC_miner
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adaptCC_nit
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_adaptCC_denit
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_TREFdenit1
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_TREFdenit2
!test humidite dans decision semis
!********************************************
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_nbj_pr_apres_semis
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_eau_mini_decisemis
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_humirac_decisemis
!pilotage des fertilisations
!**********************************
! DR 05/04/2011 on les passe dans paramv6
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_codecalferti
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_ratiolN
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_dosimxN
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_codetesthumN      ! => Stics_Communs ?? A classer ou dupliquer

      ! Bruno : les 3 nouveaux param

      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_codeNmindec
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_rapNmindec
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_fNmindecmin

! ML 17/10/2013 on ajoute le code de couplage avce le module pathogene
! DR 07/02/2014 j'enleve la possibilite d'appel a Mila qui fait l'objet d'une version branch Mila
! je laisse les codes de calcul de duree d'humectation qui peut servir a tous
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codepatho
! ML 29102012 on ajoute les 2 codes de calcul de la temp de rosee et de la duree d'humectation
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_codetrosee
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_codeSWDRH

! DR 06/05/2015 je rajoute un code pouyr tester la mortalite des racines
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codemortalracine

      ! Ajout Joel 4/2/15
      ! Parametres nit denit et N2O
      ! Joel et DR 19/10/2016 creation des options nitrification et denitrification
      !DR 08/11/2016 je migre les parametres de nitrifiaction et denitrifiaction dans les param_gen pour ne pas faire de doublonsavce ceux
      ! deja existants dans le param_gen

! DR 05/02/2016 pour regles de semis agmipWheat
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_type_project
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_rules_sowing_AgMIP
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_Flag_Agmip_rap

      ! DR 23/03/2014 pour Constance et pour la preochaine version on peut faire plusieurs eclaircissages
      ! DR 23/03/2016 pour le rendre generique je le sort dans param_newform
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_option_thinning
! DR 30/03/2016 je met a part la possiblite d'apporter plusieurs types deengrais , c'est au dela de la notion de paturage
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_option_engrais_multiple
      ! DR 29/03/2016 option pour activer la gestion en paturage
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_option_pature
! DR 07/04/2016 les parametres de l'option pature
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_coderes_pature
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_pertes_restit_ext
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_Crespc_pature
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_Nminres_pature
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_eaures_pature
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_coef_calcul_qres
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_engrais_pature
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_coef_calcul_doseN

      call EnvoyerMsgHistorique(logger, 'codeNmindec', t%P_codeNmindec)
      call EnvoyerMsgHistorique(logger, 'rapNmindec', t%P_rapNmindec)
      call EnvoyerMsgHistorique(logger, 'fNmindecmin ', t%P_fNmindecmin)

      ! Ajout Loic et Bruno fevrier 2014
      !    valeurs initiales des nouveaux pools
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_maperenne0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_QNperenne0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_QNrestemp0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_msrac0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_QNrac0
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_codejourdes
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_codetauxexportfauche
! fin ajout mars 2017
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_code_gdh_Wang
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_tdoptdeb    ! Optimal temperature for calculating duration between dormancy and bud break
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_code_hautfauche_dyn
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=80,err=250) t%P_Hautfauche  ! Cutting height of forage crops
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_code_CsurNsol_dynamic  ! option to calulate Csursol dynamic
! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
! DR 23/08/2019 j'introduit le Finert de Bruno "proprement" pour comparaison avec trunk
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codeFinert
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_Finert1  ! C/N soil factor 1 (nd)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_Finert2  ! C/N soil factor 2 (nd)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_codeFunctionFinert
! Bruno et Florent juin 2018 parametre P_humirac
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_humirac  ! 1 = la fonction F_humirac atteint un plateau (ancien code) / 2 = la fonction n'atteint pas de plateau (identique a la phase germination-levee)
! fin ajout
!  DR 04/03/2019 j'ajoute un code dans param_newform pour desactiver le nouveau calcul de BM pour l'evaluation SMS
! voir ce qu'on fait avce les CAS , pour le moment je l'indice sur la plante
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_auto_profres(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resk(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resz(1)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_auto_profres(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resk(2)
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_resz(2)

      ! DR 12/06/2019 ajout de l'option to mix the humus on the depth itrav1-itrav2 (1) or on the depth 1-itrav2 (2) // 1,2
! PL, 12/04/2022: option inutile maintenant
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_depth_mixed_humus

      ! DR 12/06/2019 ajout de l'option de calcul du stock initial d'N selon bruno (2) or selon v9. (1) // 1,2
! DR 11/04/2022 on commente le calcul facon 9.1 qui ne sera plus utilis�
!      read (fpar,*,end=80,err=250) nomVar
!      read (fpar,*,end=90,err=250) t%P_code_stock_BM
      ! Ajout Loic Avril 2021 : code pour activer les options ISOP + renseignement du pourcentage de legumineuses
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_code_ISOP
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_code_pct_legume
      read (fpar, *, end=80, err=250) nomVar
      read (fpar, *, end=90, err=250) t%P_pct_legume

!dr et ml 17/10/2013 forcage de codeSWDRH si couplage avce Mila
! dr 07/02/2014 j'enleve aussi le forcage devevnu inutile
!      if (t%P_codepatho .eq.1)then
!        call EnvoyerMsgHistorique(logger, '(/)')
!        call EnvoyerMsgHistorique(logger, MESSAGE_408)
!        t%P_codeSWDRH=1
!      endif

      ! domi - 27/01/04 - je rajoute un test sur P_ratiol<0
      if (t%P_ratiolN < 0.0) then
         call EnvoyerMsgHistorique(logger, '(/)')
         call EnvoyerMsgHistorique(logger, MESSAGE_395)
         t%P_ratiolN = 1.0
      end if

      ! PB - 16/03/2004
      ! en mode de simulation CAS on ne peut pas calculer automatiquement les fertilisations
      if (P_nbplantes > 1) t%P_codecalferti = 2

! dr 05/04/2011 sont passe dans paramv6
      if (t%P_codecalferti == 1) then
         call EnvoyerMsgHistorique(logger, 'P_ratiolN ', t%P_ratiolN)
         call EnvoyerMsgHistorique(logger, 'P_codetesthumN ', t%P_codetesthumN)
      end if

! 31/05/2018 manquait la fermeture du fichier param_newform
      close (fpar)
      return

80    call exit_error(logger, 'End of new formalisms parameters file :  missing lines in ' // path)

90    call exit_error(logger, 'End of new formalisms parameters file ['// path // '] :  missing value for ' // nomVar)

250   call exit_error(logger, 'Error reading new formalisms parameters file in ' // path)

   end function Stics_Lecture_Transit

   type(Stics_Transit_) pure function transit_ctor() result(t)
      t%P_codlocirrig = 0 ! variable d'harmonisation des codlocirrg des differents fichiers techniques de la simulation (cultures associees)

      t%bdilI(:) = 0.
      t%adilI(:) = 0.
      t%adilmaxI(:) = 0.
      t%bdilmaxI(:) = 0.

      t%deltabso(:, :) = 0.   ! pour chaque plante, AO/AS
      t%dltamsN(:, :) = 0.    ! pour chaque plante, AO/AS

      t%humectation = .false.
      t%nbjhumec = 0

      t%pfmax2 = 0.

      t%pluiesemis = 0.

      t%QH2Oi = 0.
      t%QNO3i = 0.
      t%QNH4i = 0.

      !  t%P_codetempfauche = 0
      !  t%P_coefracoupe(:) = 0.            ! 2 plantes

      t%dltarestempN(:, :) = 0.         ! 2 plantes, AO et AS
      t%dltarestemp(:, :) = 0.         ! 2 plantes, AO et AS
      t%dltarespstruc(:, :) = 0.
      t%innlax = 1.
      ! from trunk (i.e. v9: TODO see if usefull ?
      t%dltaremobilN(:, :) = 0.         ! 2 plantes, AO et AS

      t%denstalle(:) = 0.
      t%sptalle(:) = 0.

      t%spfourrage = 0.
      t%nspfourrage = 0.
      t%P_codepluiepoquet = 0
      t%P_nbjoursrrversirrig = 0

      !  t%P_SurfApex(:) = 0.
      !  t%P_SeuilMorTalle(:) = 0.
      !  t%P_SigmaDisTalle(:) = 0.
      !  t%P_VitReconsPeupl(:) = 0.
      !  t%P_SeuilReconsPeupl(:) = 0.
      !  t%P_MaxTalle(:) = 0.
      t%PerTalle(:) = 0
      !  t%P_swfacmin = 0.
      !  t%LAIapex(:) = 0.
      !  t%P_codetranspitalle = 0
      !  t%P_codedyntalle = 0

      t%dateirr(:, :) = 0
      t%nbapirr(:) = 0

      ! *- azote
      ! DR 05/04/2011 sont passes dans paramv6
      ! *- code calcul automatique des fertilisations
      ! dr 05/04/2011 mis dans paramv6
      t%P_codecalferti = 0
      t%P_codetesthumN = 0
      t%P_dosimxN = 0.
      t%P_ratiolN = 0.

      t%dateN(:, :) = 0
      t%nbapN(:) = 0
      !  t%P_resplmax(:) = 0.

      t%P_code_adapt_MO_CC = 0
      t%P_code_adaptCC_miner = 0
      t%P_code_adaptCC_nit = 0
      t%P_code_adaptCC_denit = 0
      t%P_periode_adapt_CC = 0
      t%P_an_debut_serie_histo = 0
      t%P_an_fin_serie_histo = 0
      t%P_param_tmoy_histo = 0.

      !  t%P_nbj_pr_apres_semis = 0
      !  t%P_eau_mini_decisemis = 0
      !  t%P_humirac_decisemis = 0.

      ! DR 06/05/2015 je rajoute un code pour tester la mortalite des racines
      !  t%P_codemortalracine = 0

      !  t%P_option_thinning = 0
      t%P_option_pature = 0

      t%P_coderes_pature = 0
      t%P_pertes_restit_ext = 0
      t%P_Crespc_pature = 0
      t%P_Nminres_pature = 0
      t%P_eaures_pature = 0
      t%P_coef_calcul_qres = 0
      t%P_engrais_pature = 0
      t%P_coef_calcul_doseN = 0

      t%P_humirac = 1
      !  t%P_codejourdes = 0
      ! DR 08/11/2016 les parametres nit et denit sont dans le param_newform

      ! DR le 01/06/2017 on initialise les nouveaux parametres de la mineralisation
      ! Ajout Bruno fevrier 2017 new mineralization model
      !t%P_Gmin1 = 0.  ! mineralization rate constant (day-1)
      !t%P_Gmin2 = 0.  ! clay content factor (%-1)
      !t%P_Gmin3 = 0.  ! CaCO3 content factor (%-1)
      !t%P_Gmin4 = 0.  ! pH factor 1 (pH-1)
      !t%P_Gmin5 = 0.  ! pH factor 2 (pH)
      !t%P_Gmin6 = 0.  ! C/N soil factor 1 (nd)
      !t%P_Gmin7 = 0.  ! C/N soil factor 2 (nd)
      ! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
      ! DR 01/062017 a voir avce Bruno car pas utilises dans mineralisation mais dans initnonsol ??
      !t%P_codeFinert = 0
      !t%P_codeFunctionFinert = 0
      !t%P_Finert1  = 0. ! C/N soil factor 1 (nd)
      !t%P_Finert2  = 0. ! C/N soil factor 2 (nd)
      !t%P_codeFunctionFinert= 0
      ! fin ajout

      ! DR 03/07/2018 activation du melange H2O N lors du travail du sol
      !t%P_codemixN_H2O = 0
      !t%P_tmix = 0.
      !  DR 04/03/2019 j'ajoute un code dans param_newform pour desactiver le nouveau calcul de BM pour l'evaluation SMS
      ! je mets aussi les parametres resk et resz
      ! t%P_code_auto_profres(:) = 0
      ! t%P_resk(:) = 0
      ! t%P_resz(:) = 0

      !DR 23/08/2019 code to activate the calculation of CsurNsol as Chumt/Nhumt // SD // PARAM// 1
      t%P_code_CsurNsol_dynamic = 0

      ! Loic Avril 2021 : ISOP
      t%P_code_ISOP = 0
      t%P_code_pct_legume = 0
      t%P_pct_legume = 0
   end function transit_ctor
end module Stics_Lecture_Transit_m

