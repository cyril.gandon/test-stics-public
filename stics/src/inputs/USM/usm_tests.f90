module usm_tests
   use stics_system
   use USM
   use Plante
   use Stics
   use messages
   use testdrive, only: error_type, unittest_type, new_unittest, check

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_read_usm_from_file", test_read_usm_from_file) &
                  ]

   end subroutine

   subroutine test_read_usm_from_file(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: path
      type(Stics_Communs_) :: sc
      type(Plante_), dimension(2) :: plants
      character(len=255) :: cwd

      call getcwd(cwd)
      path = join_path(trim(cwd),'src','inputs','USM','new_travail_test.usm')

      sc = read_usm_from_file(get_silent_logger(), path, plants)

      call check(error, CODE_CULTURE, sc%P_codesimul)
      call check(error, 1, sc%codoptim)
      call check(error, 2, sc%P_codesuite)
      call check(error, 2, sc%P_nbplantes)
      call check(error, "TEST_NAME_USM", sc%P_usm)
      
      call check(error, 30, sc%P_iwater)
      call check(error, 300, sc%P_ifwater)
      call check(error, "banane_ini.xml", sc%P_ficInit)
      call check(error, 3, sc%P_ichsl)
      call check(error, "solbanane", sc%P_nomsol)
      call check(error, "climbanj_sta.xml", sc%P_ficStation)
      call check(error, "climbanj.1996", sc%P_wdata1)
      call check(error, "climbanj.1996", sc%P_wdata2)
      call check(error, 4, sc%nbans)
      call check(error, 5, sc%P_culturean)

      call check(error, "proto_banana_plt.xml", plants(1)%P_fplt)
      call check(error, "banane_tec.xml", plants(1)%P_ftec)
      call check(error, "flai1.xml", plants(1)%P_flai)

      call check(error, "proto_banana_plt2.xml", plants(2)%P_fplt)
      call check(error, "banane_tec2.xml", plants(2)%P_ftec)
      call check(error, "flai2.xml", plants(2)%P_flai)
   end subroutine
end module
