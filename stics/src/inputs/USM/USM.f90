module USM
   use stics_system
   use messages
   use stics_files
   use Stics_Zero_m
   use Stics
   use Plante
   implicit none

   private
   public :: read_usm_from_file
contains

   !> Reading of the simulation description (files: plant, soil, itk, ...) in travail.usm
   type(Stics_Communs_) function read_usm_from_file(logger, path, plants) result(sc)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      type(Plante_), intent(inout) :: plants(:)

      character(len=255) :: vartmp, P_fplt, P_ftec, P_flai, P_usm, P_codesimul_buffer
      character(:), allocatable :: P_codesimul
      integer :: plant_index
! DR le 21/06/2017 pour David et la calibration Atcha , je force le LAI pour qi le fichier est present sinon on ne peut pas faire d'optimisation
!  ou de lancement en usms independantes
! ******* forcage LAI a regarder quand on modifiera l'usm pour ajouter un code : codeforcage_lai
      integer :: fusm, iostat
      logical :: exists

      exists = path_exist(path)
      if (.not. exists) then
         call exit_error(logger, "Usm file doesn't exist: ["//path//"]")
      end if

      open (newunit=fusm, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening usm file: ["//path//"]")
      end if

      sc = Stics_Zero()

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) P_codesimul_buffer
      P_codesimul = adjustl(trim(P_codesimul_buffer))
      if (P_codesimul == 'culture') then
         sc%P_codesimul = CODE_CULTURE
      else if (P_codesimul == 'feuille') then
         sc%P_codesimul = CODE_FEUILLE
      else
         call exit_error(logger, "Unknown P_codesimul ["//P_codesimul//"] in usm file ["//path//"]. &
            &P_codesimul should be either ["//CODE_CULTURE//"] or ["//CODE_FEUILLE//"].")
      end if

!: Lecture du code d'optimisation
      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%codoptim

!: Lecture du code de poursuite de simulation
      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_codesuite

!: Lecture des parametres de la simulation
      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_nbplantes

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) P_usm
      sc%P_usm = adjustl(trim(P_usm))

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_iwater

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_ifwater

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_ficInit

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_ichsl

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_nomsol

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_ficStation

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_wdata1

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_wdata2

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%nbans

      read (fusm, *, err=250, end=90) vartmp
      read (fusm, *, err=250, end=90) sc%P_culturean

      do plant_index = 1, sc%P_nbplantes

         read (fusm, *, err=250, end=90) vartmp
         read (fusm, *, err=250, end=90) P_fplt
         plants(plant_index)%P_fplt = adjustl(trim(P_fplt))

         read (fusm, *, err=250, end=90) vartmp
         read (fusm, *, err=250, end=90) P_ftec
         plants(plant_index)%P_ftec = adjustl(trim(P_ftec))

         read (fusm, *, err=250, end=90) vartmp
         read (fusm, *, err=250, end=90) P_flai
         plants(plant_index)%P_flai = adjustl(trim(P_flai))

! DR le 21/06/2017 pour David et la calibration Atcha , je force le LAI pour qi le fichier est present sinon on ne peut pas faire d'optimisation
!  ou de lancement en usms independantes
! ******* forcage LAI a regarder quand on modifiera l'usm pour ajouter un code : codeforcage_lai
!        i_lai=index(sc%P_flai(i),'.lai')
!        if(i_lai.ne.0.and.sc%P_flai(i).ne.'default.lai'.and.sc%P_flai(i).ne.'null')then
!            sc%P_codesimul='feuille'
!            call EnvoyerMsgHistorique(logger, MESSAGE_157)
!        endif
      end do

! DR 24/09/2018 pour l'histoire des pbs d'enchainement en serie climatique je conserve le P_ifwater initial
      sc%P_ifwater0 = sc%P_ifwater
      sc%P_culturean0 = sc%P_culturean
      close (fusm)
      return

90    call exit_error(logger, "End of usm file ["//path//"] :  missing value for "//vartmp)


250   call exit_error(logger, "Error reading usm file: ["//path//"]")

   end function read_usm_from_file
end module USM

