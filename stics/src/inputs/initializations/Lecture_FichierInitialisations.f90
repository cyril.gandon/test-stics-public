! subroutine of reading initialization parameters/
! - reading of the initializations parameters in the file ficini.txt
module lecinitialisations_m
   use stics_files
   use stics_system
   use Stics
   use Plante
   use Sol
   use messages
   use Station
   implicit none
   private
   public :: lecinitialisations
contains
   subroutine lecinitialisations(logger, path, sc, p, soil, sta)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      type(Stics_Communs_), intent(inout) :: sc
      type(Plante_), intent(inout) :: p(sc%P_nbplantes)
      type(Sol_), intent(inout) :: soil
      type(Station_), intent(inout) :: sta

      integer :: fini, iostat
      logical :: exists

      exists = path_exist(path)

      if (.not. exists) then
         call exit_error(logger, "Init file doesn't exist: [" // path // "]")
      end if

      open (newunit=fini, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening Init file: [" // path // "]")
      end if

      call read_from_unit(logger, fini, path, sc, p, soil, sta)
      close (fini)

   end subroutine lecinitialisations

   subroutine read_from_unit(logger, fini, path, sc, p, soil, sta)
      type(logger_), intent(in) :: logger
      integer, intent(in) :: fini
      character(*), intent(in) :: path

      type(Stics_Communs_), intent(inout) :: sc
      type(Plante_), intent(inout) :: p(sc%P_nbplantes)
      type(Sol_), intent(inout) :: soil
      type(Station_), intent(inout) :: sta

      character(len=30) :: nomVar
      integer :: i, k, nbp

      read (fini, *, err=250, end=90) nomVar
      read (fini, *, err=250, end=90) nbp

      if (nbp /= sc%P_nbplantes) then
         call exit_error(logger, MESSAGE_5162 //  to_string(sc%P_nbplantes))
      end if

      do i = 1, sc%P_nbplantes
         read (fini, *, err=250, end=90) nomVar
         read (fini, *, err=250, end=90) p(i)%P_stade0
         read (fini, *, err=250, end=90) p(i)%P_lai0
         read (fini, *, err=250, end=90) p(i)%P_magrain0
         read (fini, *, err=250, end=90) p(i)%P_zrac0
         read (fini, *, err=250, end=90) nomVar
         read (fini, *, err=250, end=90) p(i)%P_code_acti_reserve_ini

! Loic fevrier 2021
! code_acti_reserve == 1
         read (fini, *, err=250, end=90) p(i)%P_maperenne0
         read (fini, *, err=250, end=90) p(i)%P_QNperenne0
         read (fini, *, err=250, end=90) p(i)%P_masecnp0
         read (fini, *, err=250, end=90) p(i)%P_QNplantenp0
! code_acti_reserve == 2
         read (fini, *, err=250, end=90) p(i)%P_masec0
         read (fini, *, err=250, end=90) p(i)%P_QNplante0
         read (fini, *, err=250, end=90) p(i)%P_restemp0
         read (fini, *)
         read (fini, *, err=250, end=90) (p(i)%P_densinitial(k), k=1, nblayers_max)   !5)
      end do
! Loic Fevrier 20201 : ces lignes de code servent a sauter des lignes de lecture quand il n'y a qu'une seule plante
      if (sc%P_nbplantes == 1) then
         do k = 1, 16
            read (fini, *)
         end do
      end if

      read (fini, *) nomVar
      read (fini, *, err=250, end=90) (sc%P_Hinitf(k), k=1, nblayers_max)   !5)
      read (fini, *) nomVar
      read (fini, *, err=250, end=90) (soil%P_NO3initf(k), k=1, nblayers_max)   !5)
      read (fini, *) nomVar
      read (fini, *, err=250, end=90) (sc%P_NH4initf(k), k=1, nblayers_max)   !5)

      ! snow initializations detection
      ! 2022/04/14, PL: changes according to other init variables
      ! used ini initialisation files.
      read (fini, *, end=80, err=250) nomVar
      read (fini, *, end=80, err=250) nomVar
      if (nomVar == 'Sdepth0') then
         read (fini, *, end=90, err=250) sta%P_Sdepth0
         ! print *, 'Sdepth0: ',sta%P_Sdepth0
      end if
      read (fini, *, end=80, err=250) nomVar
      if (nomVar == 'Sdry0') then
         read (fini, *, end=90, err=250) sta%P_Sdry0
         !print *, 'Sdry0: ',sta%P_Sdry0
      end if
      read (fini, *, end=80, err=250) nomVar
      if (nomVar == 'Swet0') then
         read (fini, *, end=90, err=250) sta%P_Swet0
         !print *, 'Swet0: ',sta%P_Swet0
      end if
      read (fini, *, end=80, err=250) nomVar
      if (nomVar == 'ps0') then
         read (fini, *, end=90, err=250) sta%P_ps0
         !print *, 'ps0: ',sta%P_ps0
      end if

      return

80    return

90    call exit_error(logger, 'End of ini file :  missing value for ' // nomVar)

250   call exit_error(logger, 'Error reading ini file: ' // path)

   end subroutine read_from_unit
end module lecinitialisations_m

