! Module of crop management
!- Description of the structure ITK_
!- reading of ITK parameters
module Itineraire_Technique
   use stdlib_ascii, only: to_lower
   use phenology_utils
   use messages
   use messages_data
   use stics_system
   use Parametres_Generaux, only: Parametres_Generaux_
   use Stics, only: Stics_Communs_

   implicit none

   !> Options to activate the beginning and the ending date in case of automatic irrigation for P_codedate_irrigauto
   !> Irrigation dates are set
   integer, parameter :: IRRIG_AUTO_DATE = 1
   !> Irrigation dates are determined by stages
   integer, parameter :: IRRIG_AUTO_STAGE = 2
   !> No automatic irrigation
   integer, parameter :: IRRIG_AUTO_NO = 3

   type ITK_

      integer :: ipl    ! indice associe

      character(len=45) :: P_ressuite  ! // PARAMETER // Name of residue type (for the next crop) // straw, roots, crops, nothing // PARTEC // 0
      character(len=3) :: P_stadecoupedf  ! // PARAMETER // Stage of automatic cut // * // PARTEC // 0

      logical :: lecfauche

      integer :: P_codefauche  ! // PARAMETER // option of cut modes for forage crops: yes (1), no (2) // code 1/2 // PARTEC // 0
      integer :: P_codeaumin  ! // PARAMETER // harvest as a function of grain/fruit water content // code 1/2 // PARTEC // 0
      integer :: P_codetradtec  ! // PARAMETER // description of crop structure with use of radiation transfers: yes (2), non(1) // code1/2 // PARTEC // 0
      integer :: P_codlocirrig  ! // PARAMETER // code of irrigation localisation: 1= above the foliage, 2= below the foliage above the soil, 3 = in the soil // code 1/2/3 // PARTEC // 0
      integer :: P_codlocferti  ! // PARAMETER // code of fertilisation localisation:  1: at the soil surface, 2 = in the soil // code 1/2 // PARTEC // 0
      integer :: P_codemodfauche  ! // PARAMETER // option defining the cut mode: (1) automatic calculation depending on phenologic and trophic state crop,  (2)  calendar pre-established in days,  (3) calendar pre-established in degrees.days // code 1/2/3 // PARTEC // 0
      integer :: P_codeffeuil  ! // PARAMETER // option technique of thinning: yes (1), no (2) // code 1/2 // PARTEC // 0
      integer :: P_codecalirrig  ! // PARAMETER // automatic calculation of irrigation: yes (1), no (2) // code 1/2 // PARTEC // 1
      integer :: P_codestade  ! // PARAMETER // option of forcing for one or several development stages: yes (2), no (1) // code 1/2 // PARTEC // 0
      integer :: P_codcalrogne  ! // PARAMETER // option of the way of calculation of tipping // code 1/2 // PARTEC // 0
      integer :: P_codepaillage  ! // PARAMETER // option: option: 1 = no cover, 2 = plastic cover partly covering the soil   // code 1/2 // PARTEC // 0
      integer :: P_codeclaircie  ! // PARAMETER // option to activate techniques of fruit removal  // code 1/2 // PARTEC // 0
      integer :: P_codcaleffeuil  ! // PARAMETER // option of the way of caluclation of leaf removal // code 1/2 // PARTEC // 0
      integer :: P_codrognage  ! // PARAMETER // option of foliage control // code 1/2 // PARTEC // 0
      integer :: P_codhauteff  ! // PARAMETER // leaf removal heitgh // code 1/2 // PARTEC // 0
      integer :: P_codetaille  ! // PARAMETER // option of pruning // code 1/2 // PARTEC // 0
      integer :: P_codrecolte  ! // PARAMETER // harvest mode : all the plant (1) or just the fruits (2) // code 1/2 // PARTEC // 0
      integer :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0

      integer :: P_iplt0  ! // PARAMETER // Date of sowing // julian day // PARTEC // 1
      integer :: P_ilev  ! // PARAMETER // Day of the stage LEV (emergence) when the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_iamf  ! // PARAMETER // Day of the stage AMF (maximal of leaf growth , end of juvenile phase ) when the stage is observed (else 999) // * // PARTEC // 1
      integer :: P_ilax  ! // PARAMETER // Day of the stage LAX(maximal leaf area index) when the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_idrp  ! // PARAMETER // Day of the stage DRP (beginning of grain filling) when the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_isen  ! // PARAMETER // Day of the stage SEN (beginning of net senescence) when the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_imat  ! // PARAMETER // Day of the stage MAT (physiological maturity) when the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_irec  ! // PARAMETER // Day of the stage REC (harvest) when the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_irecbutoir  ! // PARAMETER // Date of harvest  butoir (if the crop has not finished his cycle at this date, the harvest is imposed) // julian day // PARTEC // 1
      integer :: P_variete  ! // PARAMETER // variety number in the technical file // SD // PARTEC // 1
      integer :: nap
      integer :: napN
      ! DR 01/04/2016 je discretise les apports uree due au pature et engrais classique
      integer :: napN_uree
      integer :: napN_engrais

      integer :: P_nbjres  ! // PARAMETER // number of residue addition // days // PARTEC // 1
      integer :: P_nbjtrav  ! // PARAMETER // number of cultivations oprerations // days // PARTEC // 1
      integer :: nbcoupe
      integer :: P_julfauche(20)     ! // PARAMETER // Day in year of each cut (table)  // julian day // PARTEC // 1
      integer :: P_nbcueille  ! // PARAMETER // number of fruit harvestings // code 1/2 // PARTEC // 0
      integer :: P_ilan  ! // PARAMETER // Day of the stage LAN () if the stage is observed (else 999) // julian day // PARTEC // 1
      integer :: P_iflo  ! // PARAMETER // flowering day // julian day // PARTEC // 1
      integer :: P_locirrig  ! // PARAMETER // Depth of water apply (when irrigation is applied in depth of soil) // cm // PARTEC // 1

      integer :: P_engrais(300)  ! // PARAMETER // fertilizer type  : 1 =Nitrate.of ammonium ,2=Solution,3=urea,4=Anhydrous ammoniac,5= Sulfate of ammonium,6=phosphate of ammonium,7=Nitrateof calcium,8= fixed efficiency    // * // PARTEC // 1
      integer :: P_locferti  ! // PARAMETER // Depth of nitrogen apply (when fertiliser is applied in depth of soil) // cm // PARTEC // 1
      integer :: P_cadencerec  ! // PARAMETER // number of days between two harvests // day // PARTEC // 1
      integer :: P_julrogne  ! // PARAMETER // day of plant shapening // julian day // PARTEC // 1

      integer :: P_juleclair(10)  ! // PARAMETER // julian day of fruits removal // julian day // PARTEC // 1
      integer :: P_juleffeuil  ! // PARAMETER // julian day of leaf removal // julian day // PARTEC // 1
      integer :: P_jultaille  ! // PARAMETER // pruning day // julian day // PARTEC // 1

      integer :: P_jultrav(11)     ! // PARAMETER // Day in year of the soil cultivation operations and/or apply of organic residues  // julian day // PARTEC // 1
      integer :: P_julres(11)     ! // PARAMETER // Day in year of the residue apply  // julian day // PARTEC // 1
      integer :: P_coderes(11)     ! // PARAMETER // residue type: 1=crop residues,  2=residues of CI,  3=manure,  4=compost OM,  5=mud SE,  6=vinasse,  7=corn,  8=other // code 1 to 10 // PARTEC // 0
      integer :: P_upvttapN(300)     ! // PARAMETER // thermal time from emergence (UPVT units) driving fertilization // degree C // PARTEC // 1
      integer :: P_julapN(300)     ! // PARAMETER // dates in julian days of fertilizer application // julian day // PARTEC // 1
      integer :: P_julapI(300)     ! // PARAMETER // dates in julian days of irrigation  // julian day // PARTEC // 1
      integer :: P_upvttapI(300)     ! // PARAMETER // thermal time from emergence (UPVT units) driving irrigation // degree C // PARTEC // 1

      real :: P_profsem  ! // PARAMETER // Sowing depth // cm // PARTEC // 1
      real :: P_ratiol  ! // PARAMETER // Water stress index below which we start an irrigation in automatic mode (0 in manual mode) // between 0 and 1 // PARTEC // 1
      real :: P_dosimx  ! // PARAMETER // maximum water amount of irrigation authorised at each time step (mode automatic irrigation) // mm day-1 // PARTEC // 1
      real :: P_doseirrigmin      ! DR le 12/09/07 pour benjamin      // PARAMETER // minimal amount of irrigation // mm // PARTEC // 1
      real :: P_profmes  ! // PARAMETER // measure depth of soil water reserve // cm // PARTEC // 1
      real :: P_densitesem  ! // PARAMETER // Sowing density  // plants.m-2 // PARTEC // 1
      real :: P_concirr  ! // PARAMETER // Nitrogen concentration of water // kgN mm-1 // PARTEC // 1
      real :: P_effirr  ! // PARAMETER // irrigation efficiency // SD // PARTEC // 1
      real :: P_lairesiduel(0:20)     ! // PARAMETER // Residual leaf index after each cut (table) // m2 leaf  m-2 soil // PARTEC // 1
      real :: P_hautcoupedefaut  ! // PARAMETER // Cut height for forage crops (calendar calculated) // m // PARTEC // 1
      real :: P_hautcoupe(0:20)     ! // PARAMETER // Cut height for forage crops (calendar fixed) // m // PARTEC // 1
      real :: P_msresiduel(-1:20)     ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
!  real :: P_anitcoupe(20)     ! // PARAMETER // amount of mineral fertilizer applications at each cut (forage crop) // kg N ha-1 // PARTEC // 1
      real :: P_anitcoupe(0:20)     ! // PARAMETER // amount of mineral fertilizer applications at each cut (forage crop) // kg N ha-1 // PARTEC // 1
      integer :: P_engraiscoupe(10)    ! // PARAMETER // fertilizer type  : 1 =Nitrate.of ammonium ,2=Solution,3=urea,4=Anhydrous ammoniac,5= Sulfate of ammonium,6=phosphate of ammonium,7=Nitrateof calcium,8= fixed efficiency    // * // PARTEC // 1
      real :: P_tauxexportfauche(0:20)     ! Fraction of cut which is exported
      real :: P_tempfauche(0:20)     ! // PARAMETER // Sum of development units between 2 cuts  // degree.days // PARTEC // 1
      ! 29/03/2016 prise en compte du paturage
      integer :: P_restit(0:20)   ! // PARAMETER // option of restitution in case of pasture yes (1), no (2) // 1-2 // PARTEC // 1
      real :: P_mscoupemini(0:20)   ! // PARAMETER // Minimum threshold value of dry matter to make a cut // t ha-1 // PARTEC // 1

      real :: P_largrogne  ! // PARAMETER // width of shapening // m // PARTEC // 1
      real :: P_margerogne  ! // PARAMETER // allowed quantity of biomass inbetween two shapenings when asking automatic shapening  // t ha-1 // PARTEC // 1
      real :: P_hautrogne  ! // PARAMETER // cutting height // m // PARTEC // 1
      real :: P_effeuil  ! // PARAMETER // proportion of daily leaf removed at thinning // 0-1  // PARTEC // 1
      real :: P_interrang  ! // PARAMETER // Width of the P_interrang // m // PARTEC // 1
      real :: P_orientrang  ! // PARAMETER // Direction of ranks // rd (0=NS) // PARTEC // 1
      real :: P_Crespc(11)     ! // PARAMETER // carbon proportion in organic residue //  // PARTEC // 1
      real :: P_proftrav(11)     ! // PARAMETER // Depth of residue incorporation  (max. 40 cm) // cm // PARTEC // 1
      real :: P_profres(11)     ! // PARAMETER // minimal depth of organic residue incorporation  // cm // PARTEC // 1
      real :: P_CsurNres(11)     ! // PARAMETER // C/N ratio of residues // g g-1 // PARTEC // 1
      real :: P_Nminres(11)     ! // PARAMETER // N mineral content of organic residues  // % fresh matter // PARTEC // 1
      real :: P_eaures(11)     ! // PARAMETER // Water amount of organic residues  // % fresh matter // PARTEC // 1
      real :: P_qres(11)     ! // PARAMETER // amount of crop residue or organic amendments applied to the soil (fresh weight) // t MF ha-1 // PARTEC // 1
      real :: P_doseI(300)     ! // PARAMETER // irrigation amount // mm.jour-1 // PARTEC // 1
      real :: P_doseN(300)     ! // PARAMETER // fertilizer amount // kgN.jour-1 // PARTEC // 1
      real :: P_h2ograinmin  ! // PARAMETER // minimal water content allowed at harvest // g eau g-1 MF // PARTEC // 1
      real :: P_h2ograinmax  ! // PARAMETER // maximal water content allowed at harvest // g water g-1 MF // PARTEC // 1
      real :: P_CNgrainrec  ! // PARAMETER // minimal grain nitrogen content for harvest  // 0-1 // PARTEC // 1
      real :: P_huilerec  ! // PARAMETER // minimal oil content allowed for harvest // g huile g-1 MF // PARTEC // 1
      real :: P_sucrerec  ! // PARAMETER // minimal sugar rate at harvest // g sucre g-1 MF // PARTEC // 1

      real :: P_nbinfloecl(10)  ! // PARAMETER // "number of inflorescences or fruits removed at  fruit removal   // nb pl-1 // PARTEC // 1
      real :: P_laidebeff  ! // PARAMETER // LAI of the beginning of leaf removal // m2 m-2 // PARTEC // 1
      real :: P_laieffeuil  ! // PARAMETER // LAI of the end of leaf removal // m2 m-2 // PARTEC // 1
      real :: P_biorognem  ! // PARAMETER // minimal biomass to be removed when tipping (automatic calculation) // t ha-1 // PARTEC // 1
      integer :: P_nb_eclair  ! // PARAMETER // removal number // 1-10 // PARTEC // 1

      integer :: P_codeDSTtass  ! // PARAMETER // activation of compaction at sowing and harvest : yes (1), no (2) // code1/2 // PARTEC // 0
      integer :: P_codedecisemis  ! // PARAMETER // activation of moisture effect on the decision to harvest // code 1/2 // PARTEC // 0
      integer :: P_codedecirecolte  ! // PARAMETER // activation of moisture and frost effects on the decision to harvest // code 1/2 // PARTEC // 0
      integer :: P_nbjmaxapressemis  ! // PARAMETER // maximal delay (number of days) of sowing date in case of soil compaction activation // jours // PARPLT // 1
      integer :: P_nbjseuiltempref  ! // PARAMETER // number of days for which we check if there is no frost sowing conditions when decision to sow is activated // jours // PARTEC // 1
      integer :: P_nbjmaxapresrecolte  ! // PARAMETER // maximal delay (number of days) of harvest date in case of soil compaction activation // jours // PARTEC // 1
      integer :: P_codeDSTnbcouche  ! // PARAMETER // activation of the number of compacted soil layers: one layer (1), two layers (2) // code 1/2 // PARTEC // 0
      real :: P_profhumsemoir  ! // PARAMETER // soil depth for which moisture is considered as a reference to allow or not sowing in the case of soil compaction activation // cm // PARTEC // 1
      real :: P_profhumrecolteuse  ! // PARAMETER // soil depth for which moisture is considered as a reference to allow or not harvesting in the case of soil compaction activation // cm // PARTEC // 1
      real :: P_dasemis  ! // PARAMETER // bulk density modification as a result of sowing // g.cm-3 // PARTEC // 1
      real :: P_darecolte  ! // PARAMETER // bulk density modification as a result of harvest // g.cm-3 // PARSOL // 1

! DR 02/07/08 fractionnement de l'azote
      real :: P_fracN(731)     ! // PARAMETER // percentage of P_Qtot_N applied // % // PARTEC // 1
      integer :: P_codefracappN  ! // PARAMETER // option of cut modes for forage crops: yes (1), no (2) // code 1/2 // PARTEC // 0
      real :: P_Qtot_N  ! // PARAMETER // amount of mineral fertilizer applications  // kg N ha-1 // PARTEC // 1

      integer ::  P_codeDST  ! // PARAMETER // activation of modifications of physical soil deep conditions through cropping management : yes (1), no (2) // code 1/2 // PARTEC // 0
      real :: P_dachisel  ! // PARAMETER // bulk density modification as a result of soil management (Chisel) // g.cm-3 // PARTEC // 1
      real :: P_dalabour  ! // PARAMETER // bulk density modification as a result of soil management (Plough) // g.cm-3 // PARTEC // 1
      real :: P_rugochisel  ! // PARAMETER // bare soil rugosity lenght (P_z0solnu) for a chisel when soil compaction is activated // m // PARTEC // 1
      real :: P_rugolabour  ! // PARAMETER // bare soil rugosity lenght (P_z0solnu) for a plough when soil compaction is activated // m // PARPLT // 1

      integer :: P_codabri  ! // PARAMETER // option of calculation of the climate under a shelter // code 1/2 // PARTEC // 0
      integer :: P_julouvre2  ! // PARAMETER // day of opening of the shelter // julian day // PARTEC // 1
      integer :: P_julouvre3  ! // PARAMETER // day of opening of the shelter // julian day // PARTEC // 1

      real :: P_transplastic  ! // PARAMETER // transmission coefficient of the shelter plastic // SD // PARTEC // 1
      real :: P_surfouvre1  ! // PARAMETER // surface proportion of the shelter opened the first day of opening // SD // PARTEC // 1
      real :: P_surfouvre2  ! // PARAMETER // surface proportion of the shelter opened the second day of opening // SD // PARTEC // 1
      real :: P_surfouvre3  ! // PARAMETER // surface proportion of the shelter opened the third day of opening // SD // PARTEC // 1

      real :: P_couvermulchplastique  ! // PARAMETER // proportion of soil covered by the cover  // SD // PARTEC // 1
      real :: P_albedomulchplastique  ! // PARAMETER // P_albedo of plastic cover // SD // PARTEC // 1

      real :: P_hautmaxtec  ! // PARAMETER // maximal height of the plant allowed by the management // m // PARTEC // 1
      real :: P_largtec  ! // PARAMETER // technical width // m // PARTEC // 1
      integer :: P_codepalissage  ! // PARAMETER // option: no (1),  yes2) // code 1/2 // PARTEC // 0
      integer :: P_coderecolteassoc  ! // PARAMETER // option to harvest intercrop species simultaneously, at the physiological maturity date of the earliest one: yes(1), no (2) // code 1/2 // PARTEC // 0
      integer :: P_codedateappH2O  ! // PARAMETER // irrigation application dates given as sum of temperatures / yes (1), no (2) // code 1/2 // PARTEC // 0
      integer :: numeroappI

      integer :: numeroappN
      integer :: P_codedateappN  ! // PARAMETER // mineral fertilizer application dates given as sum of temperatures / yes (1), no (2) // code 1/2 // PARTEC // 0
      real :: mscoupemini_courant  ! // variable // Minimum threshold value of dry matter to make a cut // t ha-1 //

      logical  :: flag_eclairmult  ! // variable // flag for activation or not of the option several thinning // 1-2 //
      logical  :: flag_pature  ! // variable // flag for activation or not of pasture and restitution// 1-2 //
!  logical  :: flag_plusieurs_engrais  ! // variable // flag for activation or not of several fertilisations type// 1-2 //
!  logical  :: flag_jourdes ! // variable // flag for activation or not of crop destruction
!  logical  :: flag_tauxexportfauche ! // variable // flag for activation or not of exportation rate for forages

! Loic fevrier 2021 : migrations depuis le param_newform ou le fichier plante
      integer :: P_code_auto_profres   ! // PARAMETER // option to calculate the value of profres with the function profres = proftrav *(1-exp(-resk.(proftrav-resz)) (1 = yes, calcul automatique file, 2 = no, read in tec file) // SD// PARTEC //1
      real    :: P_resk   ! // PARAMETER // option to calculate the value of profres with the function profres = proftrav *(1-exp(-resk.(proftrav-resz)) // SD// PARTEC //1
      real    :: P_resz  ! // PARAMETER // option to calculate the value of profres with the function profres = proftrav *(1-exp(-resk.(proftrav-resz)) // SD// PARTEC //1
      integer :: P_nbj_pr_apres_semis  ! // PARAMETER // days number to calculate rainfall need to start sowing (is codesemis is activated) // day // PARTEC // 1
      integer :: P_eau_mini_decisemis  ! // PARAMETER // minimum amount of rainfall to start sowing (when codesemis is activated) // mm // PARTEC // 1
      real    :: P_humirac_decisemis  ! // PARAMETER // effect of soil moisture for sowing decision ( from 0 to 1 : 0 = no sensitivity to drought; 1 = very sensitive) // SD // PARTEC // 1
      integer :: P_codedate_irrigauto ! // PARAMETER // option to activate the beginning and the ending date in case of automatic irrigation  : dates (1),stages (2), no (3) // code 1/2 //PARTEC // 1
      integer :: P_datedeb_irrigauto  ! // PARAMETER // date of beginning automatic irrigations in julian day // julian day //PARTEC // 1
      integer :: P_datefin_irrigauto ! // PARAMETER // date of ending automatic irrigations in julian day // julian day //PARTEC // 1
      character(len=3) :: P_stage_start_irrigauto ! // PARAMETER // phenological stage of beginning automatic irrigations in julian day // char//PARTEC // 1
      character(len=3) :: P_stage_end_irrigauto ! // PARAMETER // phenological stage of ending automatic irrigations in julian day // char//PARTEC // 1
      integer :: P_code_autoressuite
      real    :: P_Stubblevegratio
      integer :: P_code_hautfauche_dyn! // PARAMETER // option to activate dynamic calculation of residual lai on biomass after cutting :yes (1), no(2) // code 1/2 //PARTEC // 1
      integer :: P_codejourdes ! // PARAMETER // option to simulate plant destruction (for perennial crops only) // 1 : yes ; 2 : no // PARTEC // 1
      integer :: P_juldes  ! // PARAMETER // Day of the plant destruction (0 if no destruction) // julian day // PARTEC // 1
      integer :: P_codetempfauche  ! // PARAMETER // option of the reference temperature to compute cutting sum of temperatures : upvt (1), udevair (2) // code 1/2 // PARTEC // 0

   end type ITK_

contains

!*********************************************************************
!   reading crop management parameters in the file fic1_tec.txt
!*********************************************************************

   subroutine ITK_Lecture(logger, path, itk)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      type(ITK_), intent(inout) :: itk

      integer :: nbcoupe2, nbcoupe3, i

      character(len=30) :: nomVar
      logical :: exists
      integer :: ftec, iostat
      exists = path_exist(path)

      if (.not. exists) then
         call exit_error(logger, "Tec file doesn't exist: [" // path // "]")
      end if

      open (newunit=ftec, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening tec file: [" // path // "]")
      end if

! DR 01/02/2011 on scinde le tableau apport de residus et travail du sol en 2 tableaux
      ! lecture des apports de residus
!supply of organic residus
!***************************
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbjres
      if (itk%P_nbjres > 0) then
         do i = 1, itk%P_nbjres
            read (ftec, *, err=250, end=80) nomVar
            read (ftec, *, err=250, end=90) itk%P_julres(i), itk%P_coderes(i), &
               itk%P_qres(i), itk%P_Crespc(i), itk%P_CsurNres(i), itk%P_Nminres(i), itk%P_eaures(i)
         end do
      end if

!soil tillage
!******************
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_code_auto_profres
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_resk
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_resz
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbjtrav
      if (itk%P_nbjtrav > 0) then
         do i = 1, itk%P_nbjtrav
            read (ftec, *, err=250, end=80) nomVar
            read (ftec, *, err=250, end=90) itk%P_jultrav(i), itk%P_profres(i), itk%P_proftrav(i)
! ajout calcul automatique de profres : profres = proftrav *(1-exp(-resk.(proftrav-resz))
!             if(itk%P_proftrav(i) == 0.) itk%P_proftrav(i) = 1.
            if (abs(itk%P_proftrav(i)) < 1.0D-8) itk%P_proftrav(i) = 1.
!  DR 04/03/2019 j'ajoute un code dans param_newform pour desactiver le nouveau calcul de BM pour l'evaluation SMS
            if (itk%P_code_auto_profres .eq. 1) then
! j'y mets aussi les parametres resk et resz
!  les 2 valeurs sont passees en parametres
!            resk = 0.14
!            resz = 5.
               itk%P_profres(i) = itk%P_proftrav(i)*amax1(0., 1.-exp(-itk%P_resk*(itk%P_proftrav(i) - itk%P_resz)))
               itk%P_profres(i) = nint(itk%P_profres(i) + 0.5)
               !print *, 'profres, proftrav', itk%P_profres(i),itk%P_proftrav(i)
            end if
         end do
      end if
!sowing
!*****************
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_iplt0
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_profsem
      read (ftec, *, err=250, end=80) nomVar
      ! DR 27/08/07 on renomme densite P_densitesem car densite varie
      read (ftec, *, err=250, end=90) itk%P_densitesem
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_variete
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codetradtec
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_interrang
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_orientrang
      ! DR 23/10/07 lecture des options de semis
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codedecisemis
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbjmaxapressemis
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbjseuiltempref
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbj_pr_apres_semis
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_eau_mini_decisemis
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_humirac_decisemis
!phenological stages
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codestade
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_ilev
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_iamf
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_ilax
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_isen
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_ilan
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_iflo
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_idrp
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_imat
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_irec

! irrigation
!***************************
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_effirr
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codecalirrig
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_ratiol
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_dosimx
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_doseirrigmin
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codedate_irrigauto
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_datedeb_irrigauto
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_datefin_irrigauto
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_stage_start_irrigauto
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_stage_end_irrigauto
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codedateappH2O
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%nap
! *- lecture des irrigations
      if (itk%nap > 0) then
         do i = 1, itk%nap
            if (itk%P_codedateappH2O /= 1) then
               read (ftec, *, err=250, end=80) nomVar
               read (ftec, *, err=250, end=90) itk%P_julapI(i), itk%P_doseI(i)
            else
               read (ftec, *, err=250, end=80) nomVar
               read (ftec, *, err=250, end=90) itk%P_upvttapI(i), itk%P_doseI(i)
            end if
         end do
      end if

      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codlocirrig
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_locirrig
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_profmes
      !fertilisation
!************************
!      read (ftec,*,err=250, end = 80) nomVar
!      if(itk%flag_plusieurs_engrais)then
!          read (ftec,*,err=250, end = 90)
!      else
!          read (ftec,*,err=250, end = 90) itk%P_engrais(1)
!      endif
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_concirr
      ! DR 23/10/07 on deplace P_codedateappn ici pour la lecture suivant code
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codedateappN
      ! DR 2/07/08 on rajoute 2 parametres pour permettre le fractionnent de l'azote
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codefracappN
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_Qtot_N
      !
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%napN
      ! *- lecture des fertilisations
      if (itk%napN > 0) then
         do i = 1, itk%napN
            if (itk%P_codedateappN /= 1) then
               if (itk%P_codefracappN == 1) then
! Loic Fevrier 2021 : plus besoin du flag_plusieurs_engrais, �a n'est plus une option
                  read (ftec, *, err=250, end=80) nomVar
!              if(itk%flag_plusieurs_engrais)then
                  read (ftec, *, err=250, end=90) itk%P_julapN(i), itk%P_doseN(i), itk%P_engrais(i)
!              else
!                  read (ftec,*,err=250, end = 90) itk%P_julapN(i), itk%P_doseN(i)
!              endif
               else
                  read (ftec, *, err=250, end=80) nomVar
!              if(itk%flag_plusieurs_engrais)then
                  read (ftec, *, err=250, end=90) itk%P_julapN(i), itk%P_fracN(i), itk%P_engrais(i)
!              else
!                  read (ftec,*,err=250, end = 90) itk%P_julapN(i), itk%P_fracN(i)
!              endif
               end if
            else
               if (itk%P_codefracappN == 1) then
                  read (ftec, *, err=250, end=80) nomVar
!              if(itk%flag_plusieurs_engrais)then
                  read (ftec, *, err=250, end=90) itk%P_upvttapN(i), itk%P_doseN(i), itk%P_engrais(i)
!              else
!                  read (ftec,*,err=250, end = 90) itk%P_upvttapN(i), itk%P_doseN(i)
!              endif
               else
                  read (ftec, *, err=250, end=80) nomVar
!              if(itk%flag_plusieurs_engrais)then
                  read (ftec, *, err=250, end=90) itk%P_upvttapN(i), itk%P_fracN(i), itk%P_engrais(i)
!              else
!                  read (ftec,*,err=250, end = 90) itk%P_upvttapN(i), itk%P_fracN(i)
!              endif
               end if
            end if
         end do
      end if

      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codlocferti
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_locferti
! DR 05/04/2011 on les passe dans paramv6 : sc%P_codecalferti,sc%P_ratiolN,sc%P_dosimxN
!      read (ftec,*,err=250, end = 80) nomVar
!      read (ftec,*,err=250, end = 90) sc%P_codetesthumN      ! => Stics_Communs ?? A classer ou dupliquer

! harvest
!***************************
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_irecbutoir
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_ressuite
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_code_autoressuite
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_Stubblevegratio
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codcueille
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbcueille
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_cadencerec
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codrecolte
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codeaumin
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_h2ograinmin
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_h2ograinmax
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_sucrerec
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_CNgrainrec
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_huilerec
      ! DR 23/10/07
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_coderecolteassoc
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codedecirecolte
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_nbjmaxapresrecolte
!special techniques
!*****************************
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codefauche
      ! *- domi - 10/01/2012 - P_mscoupemini deplace

! DR 29/03/2016 pour efese on ajoute 2 variables correspondant au type d'intervention fauche ou paturage , on indice le mscoupe mini specifique a la coupe
!    if(.not.itk%flag_pature)then
!      read (ftec,*,err=250, end = 80) nomVar
!      read (ftec,*,err=250, end = 90) itk%P_mscoupemini(1)
!    endif
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_code_hautfauche_dyn
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codetempfauche
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codemodfauche
      read (ftec, *, err=250, end=80) nomVar

      if (itk%P_codemodfauche == 1) then
         itk%lecfauche = .false.
      else
         itk%lecfauche = .true.
      end if

      read (ftec, *, err=250, end=90) itk%P_hautcoupedefaut
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_stadecoupedf
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) nbcoupe2
      if (itk%P_codemodfauche == 2) then
         ! *- on lit des jours
         do i = 1, nbcoupe2
!            itk%P_tauxexportfauche(i) = 1.  ! valeur par defaut
            read (ftec, *, err=250, end=80) nomVar
!            if(itk%flag_pature) then
            read (ftec, *, err=250, end=90) itk%P_julfauche(i), itk%P_hautcoupe(i), itk%P_lairesiduel(i), &
               itk%P_msresiduel(i), itk%P_anitcoupe(i), itk%P_engraiscoupe(i), &
               itk%P_tauxexportfauche(i), itk%P_restit(i), itk%P_mscoupemini(i)
!            else
!                itk%P_tauxexportfauche(i) = 1.
!                if(itk%flag_tauxexportfauche) then
!                read (ftec,*,err=250, end = 90) itk%P_julfauche(i),itk%P_hautcoupe(i),itk%P_lairesiduel(i), &
!                itk%P_msresiduel(i), itk%P_anitcoupe(i), itk%P_tauxexportfauche(i)
!                else
!                    read (ftec,*,err=250, end = 90) itk%P_julfauche(i),itk%P_hautcoupe(i),itk%P_lairesiduel(i), &
!                    itk%P_msresiduel(i), itk%P_anitcoupe(i)
!                endif
!            endif
         end do
         itk%nbcoupe = nbcoupe2
      else     ! codemodfauche =/ 2
         do i = 1, nbcoupe2
            read (ftec, *, err=250, end=80) nomVar
            read (ftec, *, err=250, end=80) nomVar
         end do
      end if

      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) nbcoupe3
      if (itk%P_codemodfauche == 3) then
         ! *- on lit des degres.jours
         do i = 1, nbcoupe3
!          itk%P_tauxexportfauche(i) = 1.  ! valeur par defaut
            read (ftec, *, err=250, end=80) nomVar
!          if(itk%flag_pature) then
            read (ftec, *, err=250, end=90) itk%P_tempfauche(i), itk%P_hautcoupe(i), itk%P_lairesiduel(i), itk%P_msresiduel(i), &
               itk%P_anitcoupe(i), itk%P_engraiscoupe(i), itk%P_tauxexportfauche(i), &
               itk%P_restit(i), itk%P_mscoupemini(i)
!          else
!          itk%P_tauxexportfauche(i) = 1.
!          if(itk%flag_tauxexportfauche) then
!                read (ftec,*,err=250, end = 90) itk%P_julfauche(i),itk%P_hautcoupe(i),itk%P_lairesiduel(i),itk%P_msresiduel(i),  &
!                                   itk%P_anitcoupe(i),itk%P_tauxexportfauche(i)
!          else
!            read (ftec,*,err=250, end = 90) itk%P_julfauche(i),itk%P_hautcoupe(i),itk%P_lairesiduel(i),itk%P_msresiduel(i),  &
!                                   itk%P_anitcoupe(i)
!          endif
!          endif
         end do
         itk%nbcoupe = nbcoupe3
      else
         do i = 1, nbcoupe3
            read (ftec, *, err=250, end=80) nomVar
            read (ftec, *, err=250, end=80) nomVar
         end do
      end if

      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codepaillage
      ! TODO: ici, on testait le code paillage de la plante principale uniquement. Pb ?
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_couvermulchplastique
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_albedomulchplastique
      read (ftec, *, err=250, end=80) nomVar

      read (ftec, *, err=250, end=90) itk%P_codrognage
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_largrogne
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_hautrogne
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_biorognem
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codcalrogne
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_julrogne
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_margerogne
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codeclaircie
      read (ftec, *, err=250, end=80) nomVar

!      if(itk%flag_eclairmult)then
      read (ftec, *, err=250, end=90) itk%P_nb_eclair
      do i = 1, itk%P_nb_eclair
         read (ftec, *, err=250, end=80) nomVar
         read (ftec, *, err=250, end=90) itk%P_juleclair(i), itk%P_nbinfloecl(i)
      end do
!      else
!         itk%P_nb_eclair=1
!         read (ftec,*,err=250, end = 90) itk%P_juleclair(1)
!         read (ftec,*,err=250, end = 80) nomVar
!         read (ftec,*,err=250, end = 90) itk%P_nbinfloecl(1)
!      endif
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codeffeuil
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codhauteff
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codcaleffeuil
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_laidebeff
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_effeuil
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_juleffeuil
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_laieffeuil
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codetaille
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_jultaille
      ! DR 23/10/07
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codepalissage
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_hautmaxtec
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_largtec
      !
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codabri
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_transplastic
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_surfouvre1
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_julouvre2
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_surfouvre2
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_julouvre3
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_surfouvre3
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codejourdes
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_juldes
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codeDST
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_dachisel
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_dalabour
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_rugochisel
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_rugolabour
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codeDSTtass
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_profhumsemoir
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_dasemis
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_profhumrecolteuse
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_darecolte
      read (ftec, *, err=250, end=80) nomVar
      read (ftec, *, err=250, end=90) itk%P_codeDSTnbcouche

      close(ftec)
      return

80    call exit_error(logger, 'End in tec file, missing lines: ' // path)

90    call exit_error(logger, 'End of tec file [' // path // '] :  missing value for ' // nomVar)

250   call exit_error(logger, 'Error reading tec file: ' // path)

   end subroutine ITK_Lecture

!*********************************************************************
!   writing in the cookie
!!     in the file history.sti
!*********************************************************************
! TODO : se debarrasser de sc et t, par exemple en transferant/dupliquant les parametres incrimines
   subroutine ITK_Ecriture_Tests(logger, itk, pg, sc, path)
      type(logger_), intent(in) :: logger
      type(ITK_), intent(inout) :: itk
      type(Stics_Communs_), intent(in) :: sc
      type(Parametres_Generaux_), intent(in) :: pg
      character(*), intent(in) :: path

      integer :: is, i
      character(len=500) :: tmp
      character(:), allocatable :: lc_stadecoupedf

      call EnvoyerMsgHistorique(logger, '  ')
      call EnvoyerMsgHistorique(logger, MESSAGE_152, path)
      call EnvoyerMsgHistorique(logger, MESSAGE_5005)

! ** test sur les valeurs de P_ressuite
! dr 25/08/08 pour la vigne on enleve pour incorporer les bois de taille
! DR 10/09/2012 on a change les noms des ressuite suivant BM

! DR 10/09/2012 je garde ca pour faire la correspondance avce 6.9 qui normalement est geree par le maf6versmodulo
      if (index(itk%P_ressuite, 'acine') > 0) itk%P_ressuite = 'roots'
      if (index(itk%P_ressuite, 'aille') > 0) itk%P_ressuite = 'straw+roots'
      if (index(itk%P_ressuite, 'haume') > 0) itk%P_ressuite = 'stubble+roots'
      if (index(itk%P_ressuite, 'ulture') > 0) itk%P_ressuite = 'whole_crop'
      if (index(itk%P_ressuite, 'ois') > 0) itk%P_ressuite = 'prunings'
      if (index(itk%P_ressuite, 'ucun') > 0 .or. index(itk%P_ressuite, 'ien') > 0) itk%P_ressuite = 'roots'

      if (index(itk%P_ressuite, 'roots') == 0 .and. index(itk%P_ressuite, 'whole_crop') == 0 .and. &
          index(itk%P_ressuite, 'straw+roots') == 0 .and. index(itk%P_ressuite, 'stubble+roots') == 0 .and. &
          index(itk%P_ressuite, 'stubbleveg') == 0 .and. index(itk%P_ressuite, 'prunings') == 0) then
         itk%P_ressuite = 'roots'
      end if

! tests sur les interventions travail du sol
      do is = 1, itk%P_nbjtrav
         if (itk%P_jultrav(is) < sc%P_iwater .and. itk%P_jultrav(is) /= 0) then
            call exit_error(logger, MESSAGE_146)
         end if
         if (itk%P_jultrav(is) <= 0) call EnvoyerMsgHistorique(logger, MESSAGE_149)

         if (itk%P_proftrav(is) <= 0.) itk%P_proftrav(is) = 1.
         if (itk%P_profres(is) < 1.) itk%P_profres(is) = 1.
         if (itk%P_profres(is) > itk%P_proftrav(is)) itk%P_profres(is) = itk%P_proftrav(is)
      end do
! DR 01122020 merge trunk
! tests sur les apports de residus
      do is = 1, itk%P_nbjres
         !DR 20/09/2019 je renforce le  message si le type de residu est incorrect
         if (itk%P_coderes(is) <= 0 .or. itk%P_coderes(is) > pg%nbResidus) then
            call exit_error(logger, MESSAGE_150)
         else
            if (itk%P_coderes(is) > 10 .and. (itk%P_coderes(is) .ne. pg%nbResidus)) then
               call EnvoyerMsgHistorique(logger, MESSAGE_150)
               call EnvoyerMsgHistorique(logger, MESSAGE_1150, (itk%P_coderes(is) - 10))
            end if
         end if
      end do

      ! The locirrig parameter must be greater than 0
      if (itk%P_codlocirrig == 3) then
         if(itk%P_locirrig <= 0.) then
            call exit_error(logger, MESSAGE_151)
         end if
      else
         itk%P_locirrig = 1
      end if

      if (itk%P_codlocirrig == 3 .and. itk%P_locirrig > 50.) then
         call EnvoyerMsgHistorique(logger, MESSAGE_393)
         itk%P_locirrig = 50
      end if

      if (itk%P_ratiol > 1. .or. itk%P_ratiol < 0.) then
         call EnvoyerMsgHistorique(logger, '(/)')
         call EnvoyerMsgHistorique(logger, MESSAGE_394)
         itk%P_ratiol = 1.
      end if

      if (itk%P_profsem < 1.) then
         call EnvoyerMsgHistorique(logger, MESSAGE_396)
         itk%P_profsem = 1
      end if

      call EnvoyerMsgHistorique(logger, 'P_nbjtrav ', itk%P_nbjtrav)
      if (itk%P_nbjtrav > 0) call EnvoyerMsgHistorique(logger, 'P_jultrav P_profres P_proftrav ')

      do i = 1, itk%P_nbjtrav
         write (tmp, '(i6,2f9.0)') itk%P_jultrav(i), itk%P_profres(i), itk%P_proftrav(i)
         call EnvoyerMsgHistorique(logger, tmp)
      end do
      call EnvoyerMsgHistorique(logger, 'P_nbjres ', itk%P_nbjres)
      if (itk%P_nbjres > 0) call EnvoyerMsgHistorique(logger, MESSAGE_510)

      do i = 1, itk%P_nbjres
         write (tmp, '(i6,i7,5f9.1)') itk%P_julres(i), itk%P_coderes(i), &
            itk%P_qres(i), itk%P_Crespc(i), itk%P_CsurNres(i), itk%P_Nminres(i), itk%P_eaures(i)
         call EnvoyerMsgHistorique(logger, tmp)

! DR 27/05/2020 on force croco a 1.0 en attendant  de pouvoir traiter la prise en compte des PROs sur une branche
! donc pour le moment tout passe en decomposable
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
         if (pg%P_CroCo(itk%P_coderes(i)) .eq. 0) then
            call EnvoyerMsgHistorique(logger, ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            write (tmp, 1111) itk%P_coderes(i)
1111        format(' !!!!!!!  type of residu used : ', i5, '  is not parameterized')
            call EnvoyerMsgHistorique(logger, trim(tmp))
            call EnvoyerMsgHistorique(logger, ' !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            ! le forcage de Croco est fait dans ResidusApportSurfaceSol
         end if
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      end do

      call EnvoyerMsgHistorique(logger, 'P_iplt0 ', itk%P_iplt0)
      call EnvoyerMsgHistorique(logger, 'P_profsem ', itk%P_profsem)
      call EnvoyerMsgHistorique(logger, 'P_densitesem ', itk%P_densitesem)
      call EnvoyerMsgHistorique(logger, 'P_variete ', itk%P_variete)

      if (itk%P_codetradtec == 1) then
         call EnvoyerMsgHistorique(logger, 'P_interrang ', itk%P_interrang)
         call EnvoyerMsgHistorique(logger, 'P_orientrang ', itk%P_orientrang)
      end if
! Loic Fevrier 2021 : ajout de codedecisemis
      if (itk%P_codedecisemis == 1) then
         call EnvoyerMsgHistorique(logger, 'P_nbjmaxapressemis ', itk%P_nbjmaxapressemis)
         call EnvoyerMsgHistorique(logger, 'P_nbjseuiltempref ', itk%P_nbjseuiltempref)
         call EnvoyerMsgHistorique(logger, 'P_nbj_pr_apres_semis ', itk%P_nbj_pr_apres_semis)
         call EnvoyerMsgHistorique(logger, 'P_eau_mini_decisemis ', itk%P_eau_mini_decisemis)
         call EnvoyerMsgHistorique(logger, 'P_humirac_decisemis ', itk%P_humirac_decisemis)
      end if
! Fin ajout
      if (itk%P_codestade == 1) then
         call EnvoyerMsgHistorique(logger, 'P_ilev ', itk%P_ilev)
         call EnvoyerMsgHistorique(logger, 'P_iamf ', itk%P_iamf)
         call EnvoyerMsgHistorique(logger, 'P_ilax ', itk%P_ilax)
         call EnvoyerMsgHistorique(logger, 'P_iflo ', itk%P_iflo)

         if (itk%P_iflo /= 999) then
            call EnvoyerMsgHistorique(logger, '')
            call EnvoyerMsgHistorique(logger, MESSAGE_398)
            call EnvoyerMsgHistorique(logger, '')
         end if
         call EnvoyerMsgHistorique(logger, 'P_idrp ', itk%P_idrp)
         call EnvoyerMsgHistorique(logger, 'P_imat ', itk%P_imat)
         call EnvoyerMsgHistorique(logger, 'P_isen ', itk%P_isen)
         call EnvoyerMsgHistorique(logger, 'P_irec ', itk%P_irec)
         call EnvoyerMsgHistorique(logger, 'P_ilan ', itk%P_ilan)
      end if

      call EnvoyerMsgHistorique(logger, 'P_effirr ', itk%P_effirr)
      call EnvoyerMsgHistorique(logger, 'nap ', itk%nap)

      if (itk%nap > 0) then
         do i = 1, itk%nap
            write (tmp, *) 'P_julapI,P_doseI ', itk%P_julapI(i), itk%P_doseI(i)
            call EnvoyerMsgHistorique(logger, tmp)
         end do
      end if

      call EnvoyerMsgHistorique(logger, 'P_codlocirrig ', itk%P_codlocirrig)

      if (itk%P_codlocirrig == 3) call EnvoyerMsgHistorique(logger, 'P_locirrig ', itk%P_locirrig)

      if (itk%P_codecalirrig == 1) then
         call EnvoyerMsgHistorique(logger, 'P_ratiol ', itk%P_ratiol)
         call EnvoyerMsgHistorique(logger, 'P_dosimx ', itk%P_dosimx)
! Loic Fervier 2021 :
         call EnvoyerMsgHistorique(logger, 'P_doseirrigmin ', itk%P_doseirrigmin)
         call EnvoyerMsgHistorique(logger, 'P_codedate_irrigauto ', itk%P_codedate_irrigauto)
         if (itk%P_codedate_irrigauto == IRRIG_AUTO_DATE) then
            call EnvoyerMsgHistorique(logger, 'P_datedeb_irrigauto ', itk%P_datedeb_irrigauto)
            call EnvoyerMsgHistorique(logger, 'P_datefin_irrigauto ', itk%P_datefin_irrigauto)
         end if
         if (itk%P_codedate_irrigauto == IRRIG_AUTO_STAGE) then
            if(is_valid_stage(itk%P_stage_start_irrigauto)) then
               call EnvoyerMsgHistorique(logger, 'P_stage_start_irrigauto ', itk%P_stage_start_irrigauto)
            else
               call exit_error(logger, IRRIGATION_START_STAGE_ERROR // ' ['//itk%P_stage_start_irrigauto//']')
            end if

            if(is_valid_stage(itk%P_stage_end_irrigauto)) then
               call EnvoyerMsgHistorique(logger, 'P_stage_end_irrigauto ', itk%P_stage_end_irrigauto)
            else
               call exit_error(logger, IRRIGATION_END_STAGE_ERROR // ' ['//itk%P_stage_end_irrigauto//']')
            end if
         end if
      end if

      call EnvoyerMsgHistorique(logger, 'P_profmes', itk%P_profmes)
      call EnvoyerMsgHistorique(logger, 'P_concirr ', itk%P_concirr)
      ! call EnvoyerMsgHistorique(logger, 'P_engamm', pg%P_engamm(itk%P_engrais(1)))
      ! call EnvoyerMsgHistorique(logger, 'P_orgeng', pg%P_orgeng(itk%P_engrais(1)))
      ! call EnvoyerMsgHistorique(logger, 'P_deneng', pg%P_deneng(itk%P_engrais(1)))
      ! call EnvoyerMsgHistorique(logger, 'P_voleng', pg%P_voleng(itk%P_engrais(1)))

      call EnvoyerMsgHistorique(logger, 'napN ', itk%napN)
      if (itk%napN > 0) then
         do i = 1, itk%napN
            if (itk%P_codedateappN /= 1) then
               write (tmp, *) 'P_julapN,P_doseN,P_engrais ', itk%P_julapN(i), itk%P_doseN(i), itk%P_engrais(i)
               call EnvoyerMsgHistorique(logger, tmp)
            else
               write (tmp, *) 'upvttN,P_doseN,P_engrais ', itk%P_upvttapN(i), itk%P_doseN(i), itk%P_engrais(i)
               call EnvoyerMsgHistorique(logger, tmp)
            end if
            call EnvoyerMsgHistorique(logger, 'P_engamm', pg%P_engamm(itk%P_engrais(i)))
            call EnvoyerMsgHistorique(logger, 'P_orgeng', pg%P_orgeng(itk%P_engrais(i)))
            call EnvoyerMsgHistorique(logger, 'P_deneng', pg%P_deneng(itk%P_engrais(i)))
            call EnvoyerMsgHistorique(logger, 'P_voleng', pg%P_voleng(itk%P_engrais(i)))
         end do
      end if

      call EnvoyerMsgHistorique(logger, 'P_codlocferti ', itk%P_codlocferti)
      if (itk%P_codlocirrig == 3) call EnvoyerMsgHistorique(logger, 'P_locferti ', itk%P_locferti)

      if (itk%P_codefauche == 1 .and. itk%P_codcueille /= 2) then
         call EnvoyerMsgHistorique(logger, MESSAGE_379)
         itk%P_codcueille = 2
      end if

      call EnvoyerMsgHistorique(logger, 'P_irecbutoir ', itk%P_irecbutoir)
      call EnvoyerMsgHistorique(logger, 'P_ressuite ', itk%P_ressuite)
      call EnvoyerMsgHistorique(logger, 'P_code_autoressuite ', itk%P_code_autoressuite)
      if (itk%P_code_autoressuite == 1) then
         call EnvoyerMsgHistorique(logger, 'P_Stubblevegratio ', itk%P_Stubblevegratio)
      end if

      if (itk%P_codcueille == 1) then
         call EnvoyerMsgHistorique(logger, MESSAGE_382)
      else
         call EnvoyerMsgHistorique(logger, MESSAGE_383)
         if (itk%P_nbcueille == 1) then
            call EnvoyerMsgHistorique(logger, MESSAGE_384)
         else
            call EnvoyerMsgHistorique(logger, MESSAGE_385, itk%P_cadencerec)
            call EnvoyerMsgHistorique(logger, MESSAGE_386)
         end if
      end if

      if (itk%P_codrecolte == 1) call EnvoyerMsgHistorique(logger, MESSAGE_387)
      if (itk%P_codrecolte == 2) then
         if (itk%P_codeaumin == 1) then
            call EnvoyerMsgHistorique(logger, MESSAGE_388, itk%P_h2ograinmin)
         else
            call EnvoyerMsgHistorique(logger, MESSAGE_389, itk%P_h2ograinmax)
         end if
      end if
      if (itk%P_codrecolte == 3) call EnvoyerMsgHistorique(logger, MESSAGE_390, itk%P_sucrerec)
      if (itk%P_codrecolte == 4) call EnvoyerMsgHistorique(logger, MESSAGE_391, itk%P_CNgrainrec)
      if (itk%P_codrecolte == 5) call EnvoyerMsgHistorique(logger, MESSAGE_392, itk%P_huilerec)

      if (itk%P_codefauche == 1) then
         call EnvoyerMsgHistorique(logger, 'P_mscoupemini ', itk%P_mscoupemini(1))
         call EnvoyerMsgHistorique(logger, 'P_code_hautfauche_dyn ', itk%P_code_hautfauche_dyn)
         call EnvoyerMsgHistorique(logger, 'P_codetempfauche ', itk%P_codetempfauche)
         if (itk%P_codemodfauche == 1) then
            call EnvoyerMsgHistorique(logger, 'P_hautcoupedefaut ', itk%P_hautcoupedefaut)
            call EnvoyerMsgHistorique(logger, 'P_stadecoupedf ', itk%P_stadecoupedf)
! DR 15/11/05 on interdit la coupe a un stade reproducteur
            lc_stadecoupedf = to_lower(itk%P_stadecoupedf)
            if (lc_stadecoupedf == flo .or. &
                lc_stadecoupedf == drp .or. lc_stadecoupedf == des .or. &
                lc_stadecoupedf == mat .or. lc_stadecoupedf == rec) then
               call exit_error(logger, MESSAGE_397)
            end if

         else
            call EnvoyerMsgHistorique(logger, 'nbcoupe ', itk%nbcoupe)
            if (itk%P_codemodfauche == 2) then
               do i = 1, itk%nbcoupe
                  write (tmp, *) 'P_julfauche,P_hautcoupe,P_lairesiduel,P_msresiduel,P_anitcoupe', &
                     'P_engraiscoupe,P_tauxexportfauche,P_restit,P_mscoupemini', &
                     itk%P_julfauche(i), itk%P_hautcoupe(i), itk%P_lairesiduel(i), &
                     itk%P_msresiduel(i), itk%P_anitcoupe(i), itk%P_engraiscoupe(i), &
                     itk%P_tauxexportfauche(i), itk%P_restit(i), itk%P_mscoupemini(i)
                  call EnvoyerMsgHistorique(logger, tmp)
                  if (itk%P_lairesiduel(i) > 0. .and. itk%P_msresiduel(i) <= 0.) &
                     call EnvoyerMsgHistorique(logger, 'P_lairesiduel >0 et P_msresiduel <=0')
               end do
            end if

            if (itk%P_codemodfauche == 3) then
               do i = 1, itk%nbcoupe
                  write (tmp, *) 'P_tempfauche,P_hautcoupe,P_lairesiduel,P_msresiduel,P_anitcoupe', &
                     'P_engraiscoupe,P_tauxexportfauche,P_restit,P_mscoupemini', &
                     itk%P_tempfauche(i), itk%P_hautcoupe(i), itk%P_lairesiduel(i), &
                     itk%P_msresiduel(i), itk%P_anitcoupe(i), itk%P_engraiscoupe(i), &
                     itk%P_tauxexportfauche(i), itk%P_restit(i), itk%P_mscoupemini(i)
                  call EnvoyerMsgHistorique(logger, tmp)
                  if (itk%P_lairesiduel(i) > 0. .and. itk%P_msresiduel(i) <= 0.) &
                     call EnvoyerMsgHistorique(logger, 'P_lairesiduel >0 et P_msresiduel <=0')
               end do
            end if
         end if
      end if

      if (itk%P_codepaillage == 2) then
         call EnvoyerMsgHistorique(logger, 'P_couvermulchplastique', itk%P_couvermulchplastique)
         call EnvoyerMsgHistorique(logger, 'P_albedomulchplastique', itk%P_albedomulchplastique)
         if (itk%P_nbjres > 0) then
            call exit_error(logger, MESSAGE_5600)
         end if
         if (index(itk%P_ressuite, 'root') == 0) then
            call exit_error(logger, MESSAGE_601)
         end if
      end if

      if (itk%P_codrognage == 2) then
         call EnvoyerMsgHistorique(logger, 'P_largrogne', itk%P_largrogne)
         call EnvoyerMsgHistorique(logger, 'P_hautrogne', itk%P_hautrogne)
         call EnvoyerMsgHistorique(logger, 'P_biorognem', itk%P_biorognem)
         if (itk%P_codcalrogne == 1) call EnvoyerMsgHistorique(logger, 'P_julrogne ', itk%P_julrogne)
         if (itk%P_codcalrogne == 2) call EnvoyerMsgHistorique(logger, 'P_margerogne ', itk%P_margerogne)
      end if

      if (itk%P_codeclaircie == 2) then
         do i = 1, itk%P_nb_eclair
            call EnvoyerMsgHistorique(logger, 'P_juleclair', itk%P_juleclair(i))
            call EnvoyerMsgHistorique(logger, 'P_nbinfloecl', itk%P_nbinfloecl(i))
         end do
      end if

      if (itk%P_codeffeuil == 2) then
         if (itk%P_codhauteff == 1) call EnvoyerMsgHistorique(logger, MESSAGE_380)
         if (itk%P_codhauteff == 2) call EnvoyerMsgHistorique(logger, MESSAGE_381)
         if (itk%P_codcaleffeuil == 1) then
            call EnvoyerMsgHistorique(logger, 'P_laidebeff ', itk%P_laidebeff)
            call EnvoyerMsgHistorique(logger, 'P_effeuil ', itk%P_effeuil)
         else
            call EnvoyerMsgHistorique(logger, 'P_juleffeuil ', itk%P_juleffeuil)
            call EnvoyerMsgHistorique(logger, 'P_laieffeuil ', itk%P_laieffeuil)
         end if
      end if

      if (itk%P_codabri == 2) then
         if (sc%P_nbplantes > 1) then
            call exit_error(logger, MESSAGE_511)
         end if
         call EnvoyerMsgHistorique(logger, 'P_transplastic', itk%P_transplastic)
         call EnvoyerMsgHistorique(logger, 'P_surfouvre1', itk%P_surfouvre1)
         call EnvoyerMsgHistorique(logger, 'P_julouvre2', itk%P_julouvre2)
         call EnvoyerMsgHistorique(logger, 'P_surfouvre2', itk%P_surfouvre2)
         call EnvoyerMsgHistorique(logger, 'P_julouvre3', itk%P_julouvre3)
         call EnvoyerMsgHistorique(logger, 'P_surfouvre3', itk%P_surfouvre3)
      end if

      call EnvoyerMsgHistorique(logger, 'P_codejourdes', itk%P_codejourdes)
      if (itk%P_codejourdes == 1) then
         call EnvoyerMsgHistorique(logger, 'P_juldes', itk%P_juldes)
      end if

      if (itk%P_codcueille == 0) then
         call exit_error(logger, MESSAGE_169)
      end if

! fin mouchard

      if (pg%P_codemsfinal == 1 .and. itk%P_codcueille == 2) call EnvoyerMsgHistorique(logger, MESSAGE_399)
      if (itk%P_codedate_irrigauto == IRRIG_AUTO_DATE .and. itk%P_datedeb_irrigauto > itk%P_datefin_irrigauto) then
         call EnvoyerMsgHistorique(logger, MESSAGE_408)
      end if
   end subroutine ITK_Ecriture_Tests

   type(ITK_) pure function ITK_Ctor(plant_index) result(itk)
      integer, intent(in) :: plant_index

      itk%ipl = plant_index

      itk%P_ressuite = ''
      itk%P_stadecoupedf = ''
      itk%lecfauche = .false.

      itk%P_codefauche = 0
      itk%P_codeaumin = 0
      itk%P_codetradtec = 0
      itk%P_codlocirrig = 0
      itk%P_codlocferti = 0
      itk%P_codemodfauche = 0
      itk%P_codeffeuil = 0
      itk%P_codecalirrig = 0
      itk%P_codestade = 0
      itk%P_codcalrogne = 0
      itk%P_codepaillage = 0
      itk%P_codeclaircie = 0
      itk%P_codcaleffeuil = 0
      itk%P_codrognage = 0
      itk%P_codhauteff = 0
      itk%P_codetaille = 0
      itk%P_codrecolte = 0
      itk%P_codcueille = 0

      itk%P_iplt0 = 0
      itk%P_ilev = 0
      itk%P_iamf = 0
      itk%P_ilax = 0
      itk%P_idrp = 0
      itk%P_isen = 0
      itk%P_imat = 0
      itk%P_irec = 0
      itk%P_irecbutoir = 0
      itk%P_variete = 0
      itk%nap = 0
      itk%napN = 0

      itk%P_nbjres = 0
      itk%P_nbjtrav = 0
      itk%nbcoupe = 0
      itk%P_julfauche(:) = 0
      itk%P_nbcueille = 0
      itk%P_ilan = 0
      itk%P_iflo = 0
      itk%P_locirrig = 0
      itk%P_engrais = 0
      itk%P_locferti = 0
      itk%P_cadencerec = 0
      itk%P_julrogne = 0
      itk%P_juleclair(:) = 0
      itk%P_juleffeuil = 0
      itk%P_jultaille = 0
      itk%P_jultrav(:) = 0
      itk%P_julres(:) = 0
      itk%P_coderes(:) = 0
      itk%P_upvttapN(:) = 0
      itk%P_julapI(:) = 0
      itk%P_julapN(:) = 0

      itk%P_profsem = 0.
      itk%P_ratiol = 0.
      itk%P_dosimx = 0.
      itk%P_profmes = 0.
      itk%P_densitesem = 0.
      itk%P_concirr = 0.
      itk%P_effirr = 0.
      itk%P_lairesiduel(:) = 0.
      itk%P_hautcoupedefaut = 0.
      itk%P_hautcoupe(:) = 0.
      itk%P_msresiduel(:) = 0.
      itk%P_anitcoupe(:) = 0.
      itk%P_tempfauche(:) = 0.
      itk%P_largrogne = 0.
      itk%P_margerogne = 0.
      itk%P_hautrogne = 0.
      itk%P_effeuil = 0.
      itk%P_interrang = 0.
      itk%P_orientrang = 0.
      itk%P_Crespc(:) = 0.
      itk%P_proftrav(:) = 0.
      itk%P_profres(:) = 0.
      itk%P_CsurNres(:) = 0.
      itk%P_Nminres(:) = 0.
      itk%P_eaures(:) = 0.
      itk%P_qres(:) = 0.
      itk%P_doseI(:) = 0.
      itk%P_doseN(:) = 0.
      itk%P_h2ograinmin = 0.
      itk%P_h2ograinmax = 0.
      itk%P_CNgrainrec = 0.
      itk%P_huilerec = 0.
      itk%P_sucrerec = 0.
      itk%P_mscoupemini = 0.
      itk%P_nbinfloecl(:) = 0.
      itk%P_laidebeff = 0.
      itk%P_laieffeuil = 0.
      itk%P_biorognem = 0.

      itk%P_codeDSTtass = 0
      itk%P_codedecisemis = 0
      itk%P_codedecirecolte = 0
      itk%P_nbjmaxapressemis = 0
      itk%P_nbjseuiltempref = 0
      itk%P_nbjmaxapresrecolte = 0
      itk%P_codeDSTnbcouche = 0
      itk%P_profhumsemoir = 0.
      itk%P_profhumrecolteuse = 0.
      itk%P_dasemis = 0.
      itk%P_darecolte = 0.

      itk%P_fracN(:) = 0.
      itk%P_codefracappN = 0
      itk%P_Qtot_N = 0

      itk%P_codeDST = 0
      itk%P_dachisel = 0.
      itk%P_dalabour = 0.
      itk%P_rugochisel = 0.
      itk%P_rugolabour = 0.

      itk%P_codabri = 0
      itk%P_julouvre2 = 0
      itk%P_julouvre3 = 0
      itk%P_transplastic = 0.
      itk%P_surfouvre1 = 0.
      itk%P_surfouvre2 = 0.
      itk%P_surfouvre3 = 0.

      itk%P_couvermulchplastique = 0.
      itk%P_albedomulchplastique = 0.

      itk%P_hautmaxtec = 0.
      itk%P_largtec = 0.
      itk%P_codepalissage = 0
      itk%P_coderecolteassoc = 0

!  itk%flag_eclairmult = .false.
      itk%flag_pature = .false.
      itk%P_restit(:) = 0
      itk%P_mscoupemini(:) = 0.
!  itk%flag_jourdes = .false.
!  itk%flag_tauxexportfauche = .false.

! Loic fevrier 2021 : migrations depuis le param_newform
      itk%P_code_auto_profres = 0.
      itk%P_resk = 0.
      itk%P_resz = 0.
      itk%P_nbj_pr_apres_semis = 0.
      itk%P_eau_mini_decisemis = 0.
      itk%P_humirac_decisemis = 0.
      itk%P_codedate_irrigauto = IRRIG_AUTO_NO
      itk%P_datedeb_irrigauto = 0.
      itk%P_datefin_irrigauto = 0.
      itk%P_tauxexportfauche(:) = 0.
      ! Loic Juin 2021 : ajout type d'engrais apporte le jour de coupe
      itk%P_engraiscoupe(:) = 0.
  end function ITK_Ctor

end module Itineraire_Technique

