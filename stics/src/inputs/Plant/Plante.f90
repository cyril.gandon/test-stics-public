! Module of Plant parameters
! - Description  of the structure Plante_
! - reading of the plant parameters
module Plante
   use stics_system
   use Stics, only: nbCouchesSol, maxdayg
   use messages
   use stics_files

   implicit none
   private

   public Plante_
   public AS, AO, AOAS
   public Plante_Lecture_fichier, init_file_output, close_plants_files, plant_ctor

! Les codes symboliques du module Plante
   integer, parameter :: PLANTE_METHOD_STICS_V6 = 6         ! Code symbolique. define the use herited reading method of Stics V6
   integer, parameter :: nb_variete_max = 30
! Module parameters index for AO (ombre), AS (soleil), AOAS (ombre+soleil)
   integer, parameter  :: AS = 1
   integer, parameter  :: AO = 2
   integer, parameter  :: AOAS = 0

   type Plante_
      logical   :: is_dominant
      logical   :: estVivante

!  parametres plantes
!***********************
      real      :: P_adil  ! // PARAMETER // Parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // N% MS // PARPLT // 1
      real      :: P_bdil  ! // PARAMETER // parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // SD // PARPLT // 1
      real      :: P_belong  ! // PARAMETER // parameter of the curve of coleoptile elongation // degree.days -1 // PARPLT // 1
      real      :: P_celong  ! // PARAMETER // parameter of the subsoil plantlet elongation curve // SD // PARPLT // 1
      real      :: P_cgrain  ! // PARAMETER // slope of the relationship between grain number and growth rate  // grains gMS -1 jour // PARPLT // 1
      real      :: P_cgrainv0  ! // PARAMETER // number of grains produced when growth rate is zero // grains m-2 // PARPLT // 1
      real      :: P_coefmshaut  ! // PARAMETER // ratio biomass/ useful height cut of crops  // t ha-1 m-1 // PARPLT // 1
      real      :: P_contrdamax  ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
      real      :: P_debsenrac  ! // PARAMETER // Sum of degrees.days defining the beginning of root senescence (life-time of a root) // degree.days // PARPLT // 1
      real      :: P_draclong  ! // PARAMETER // Maximum rate of root length production // cm root plant-1 degree.days-1 // PARPLT // 1
      real      :: P_efcroijuv  ! // PARAMETER // Maximum radiation use efficiency during the juvenile phase(LEV-AMF) // g MJ-1 // PARPLT // 1
      real      :: P_efcroirepro  ! // PARAMETER // Maximum radiation use efficiency during the grain filling phase (DRP-MAT) // g MJ-1 // PARPLT // 1
      real      :: P_efcroiveg  ! // PARAMETER // Maximum radiation use efficiency during the vegetative stage (AMF-DRP) // g MJ-1 // PARPLT // 1
      real      :: P_elmax  ! // PARAMETER // Maximum elongation of the coleoptile in darkness condition // cm // PARPLT // 1
      real      :: P_fixmax  ! // PARAMETER // maximal symbiotic fixation // kg ha-1 j-1 // PARPLT // 1      // OUTPUT // Maximal symbiotic uptake // kg ha-1 j-1
      real      :: P_h2ofeuiljaune  ! // PARAMETER // water content of yellow leaves // g water g-1 MF // PARPLT // 1
      real      :: P_h2ofeuilverte  ! // PARAMETER // water content of green leaves // g water g-1 MF // PARPLT // 1
      real      :: P_h2ofrvert  ! // PARAMETER // water content of fruits before the beginning of hydrous evolution (DEBDESHYD) // g water g-1 MF // PARPLT // 1
      real      :: P_h2oreserve  ! // PARAMETER // reserve water content // g eau g-1 MF // PARPLT // 1
      real      :: P_h2otigestruc  ! // PARAMETER // structural stem part water content // g eau g-1 MF // PARPLT // 1
      real      :: P_Kmabs1  ! // PARAMETER // Constant of nitrogen uptake by roots for the high affinity system // micromole. cm root-1 // PARPLT // 1
      real      :: P_Kmabs2  ! // PARAMETER // Constant of nitrogen uptake by roots for the low affinity system // micromole. cm root-1 // PARPLT // 1
      real      :: P_kmax  ! // PARAMETER // Maximum crop coefficient for water requirements (= ETM/ETP) // SD // PARPLT // 1
      real      :: P_kstemflow  ! // PARAMETER // Extinction Coefficient connecting leaf area index to stemflow // * // PARPLT // 1
      real      :: P_longsperac  ! // PARAMETER // specific root length // cm g-1 // PARPLT // 1
      real      :: P_lvfront  ! // PARAMETER // Root density at the root front // cm root.cm-3 soil // PARPLT // 1
      real      :: P_mouillabil  ! // PARAMETER // maximum wettability of leaves // mm LAI-1 // PARPLT // 1
      real      :: P_rsmin  ! // PARAMETER // Minimal stomatal resistance of leaves // s m-1 // PARPLT // 1
      real      :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
      real      :: P_spfrmax  ! // PARAMETER // maximal sources/sinks value allowing the trophic stress calculation for fruit onset // SD // PARPLT // 1
      real      :: P_spfrmin  ! // PARAMETER // minimal sources/sinks value allowing the trophic stress calculation for fruit onset // SD // PARPLT // 1
      real      :: P_splaimax  ! // PARAMETER // maximal sources/sinks value allowing the trophic stress calculation for leaf growing // SD // PARPLT // 1
      real      :: P_splaimin  ! // PARAMETER // Minimal value of ratio sources/sinks for the leaf growth  // between 0 and 1 // PARPLT // 1
      real      :: P_stemflowmax  ! // PARAMETER // Maximal fraction of rainfall which flows out along the stems  // between 0 and 1 // PARPLT // 1
      real      :: P_stpltger  ! // PARAMETER // Sum of development allowing germination // degree.days // PARPLT // 1
      real      :: P_tcmin  ! // PARAMETER // Minimum temperature of growth // degree C // PARPLT // 1
      real      :: P_tcmax  ! // PARAMETER // Maximum temperature of growth // degree C // PARPLT // 1
      real      :: P_tdebgel  ! // PARAMETER // temperature of frost beginning // degree C // PARPLT // 1
      real      :: P_tdmin  ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
      real      :: P_tdmax  ! // PARAMETER // Maximum threshold temperature for development // degree C // PARPLT // 1
      real      :: P_tempdeshyd  ! // PARAMETER // increase in the fruit dehydration due to the increase of crop temperature (Tcult-Tair) // % water degree C-1 // PARPLT // 1
      real      :: P_tgellev90  ! // PARAMETER // temperature corresponding to 90% of frost damage on the plantlet  // degree C // PARPLT // 1
      real      :: P_tgmin  ! // PARAMETER // Minimum threshold temperature used in emergence stage // degree C // PARPLT // 1
      real      :: P_tletale  ! // PARAMETER // lethal temperature for the plant // degree C // PARPLT // 1
      real      :: P_tmaxremp  ! // PARAMETER // maximal temperature for grain filling // degree C // PARPLT // 1
      real      :: P_tminremp  ! // PARAMETER // Minimal temperature for grain filling // degree C // PARPLT // 1
      real      :: P_Vmax1  ! // PARAMETER // Rate of slow nitrogen absorption (high affinity system) // micromole cm-1 h-1 // PARPLT // 1
      real      :: P_Vmax2  ! // PARAMETER // Rate of rapid nitrogen absorption (high affinity system) // micromole cm-1 h-1 // PARPLT // 1
      real      :: P_zlabour  ! // PARAMETER // Depth of ploughing  // cm // PARPLT // 1
      real      :: P_zprlim  ! // PARAMETER // Maximum depth of the root profile for the reference profile // cm  // PARPLT // 1
      real      :: P_zpente  ! // PARAMETER // Depth where the root density is half of the surface root density for the reference profile // cm  // PARPLT // 1

      real      :: P_adilmax  ! // PARAMETER // Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // N% MS // PARPLT // 1
      real      :: P_bdilmax  ! // PARAMETER // Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // SD // PARPLT // 1
      real      :: P_masecNmax  ! // PARAMETER // Threshold of aerial biomass below which there is no nitrogen dilution // t ha-1 // PARPLT // 1
      real      :: P_INNmin  ! // PARAMETER // Minimum value of INN authorised for the crop // SD // PARPLT // 1
      real      :: P_inngrain1  ! // PARAMETER // INN minimal for net absorption of nitrogen during grain filling  // SD // PARPLT // 1
      real      :: P_inngrain2  ! // PARAMETER // INN minimal for null net absorption of nitrogen during grain filling  // SD // PARPLT // 1
      real      :: P_stlevdno  ! // PARAMETER // phasic duration between emergence and beginning of nodulation  // degres.jours // PARPLT // 1
      real      :: P_stdnofno  ! // PARAMETER // phasic duration between the beginning and the end of nodulation // degree.days // PARPLT // 1
      real      :: P_stfnofvino  ! // PARAMETER // phasic duration between the end of the nodulation and the end of the nodule life   // degree.days // PARPLT // 1
      real      :: P_vitno  ! // PARAMETER // rate of nodule onset expressed as a proportion of P_fixmax   per degree day   // degree.days-1 // PARPLT // 1
      real      :: P_profnod  ! // PARAMETER // nodulation depth // cm // PARPLT // 1
      real      :: P_concNnodseuil  ! // PARAMETER // maximal soil nitrogen threshold for nodule onset  // kg.ha-1.mm-1 // PARPLT // 1
      real      :: P_concNrac0  ! // PARAMETER // soil nitrogen threshold forbiding nodule activity // kg.ha-1.mm-1 // PARPLT // 1
      real      :: P_concNrac100  ! // PARAMETER // soil nitrogen threshold for full nodule activity  // kg.ha-1.mm-1 // PARPLT // 1
      real      :: P_tempnod1  ! // PARAMETER // cardinal temperature for nodule activity   // degree C // PARPLT // 1
      real      :: P_tempnod2  ! // PARAMETER // cardinal temperature for nodule activity   // degree C // PARPLT // 1
      real      :: P_tempnod3  ! // PARAMETER // cardinal temperature for nodule activity   // degree C // PARPLT // 1
      real      :: P_tempnod4  ! // PARAMETER // cardinal temperature for nodule activity   // degree C // PARPLT // 1
      real      :: P_coeflevamf  ! // PARAMETER // multiplier coefficient of the development phase LEVAMF to use crop temperature // SD // PARPLT // 1
      real      :: P_coefamflax  ! // PARAMETER // multiplier coefficient of the development phase AMFLAX to use crop temperature // SD // PARPLT // 1
      real      :: P_coeflaxsen  ! // PARAMETER // multiplier coefficient of the development phase LAXSEN to use crop temperature // SD // PARPLT // 1
      real      :: P_coefsenlan  ! // PARAMETER // cultiplier coefficient of the development phase SENLAN to use crop temperature // SD // PARPLT // 1
      real      :: P_coefdrpmat  ! // PARAMETER // multiplier coefficient of the development phase DRPMAT to use crop temperature // SD // PARPLT // 1
      real      :: P_coefflodrp  ! // PARAMETER // multiplier coefficient of the development phase FLODRP to use crop temperature // SD // PARPLT // 1
      real      :: P_coeflevdrp  ! // PARAMETER // multiplier coefficient of the development phase LEVDRP to use crop temperature // SD // PARPLT // 1
      real      :: P_ratiodurvieI  ! // PARAMETER // life span of early leaves expressed as a proportion of the life span of the last leaves emitted P_DURVIEF // SD // PARPLT // 1
      real      :: P_ratiosen  ! // PARAMETER // fraction of senescent biomass (by ratio at the total biomass) // between 0 and 1 // PARPLT // 1
      real      :: P_ampfroid  ! // PARAMETER // semi thermal amplitude thermique for vernalising effect // degree C // PARPLT // 1
      real      :: P_laicomp  ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1
      real      :: P_stressdev  ! // PARAMETER // maximum phasic delay allowed  due to stresses  // SD // PARPLT // 1
      real      :: P_jvcmini  ! // PARAMETER // Minimum number of vernalising days  // day // PARPLT // 1
! DR 19/09/2016 changement de type
      integer   :: P_julvernal  ! // PARAMETER // julian day (between 1 and 365) accounting for the beginning of vernalisation for perennial crops // julian day // PARPLT // 1
      real      :: P_tfroid  ! // PARAMETER // optimal temperature for vernalisation // degree C // PARPLT // 1
      real      :: P_q10  ! // PARAMETER // P_Q10 used for the dormancy break calculation  // SD // PARPLT // 1
      real      :: P_phyllotherme  ! // PARAMETER // thermal duration between the apparition of two successive leaves on the main stem // degree C day // PARPLT // 1

      real      :: P_laiplantule  ! // PARAMETER // Plantlet Leaf index at the plantation // m2 leaf  m-2 soil // PARPLT // 1
      real      :: P_masecplantule  ! // PARAMETER // initial shoot biomass of plantlet // t ha-1 // PARPLT // 1
      real      :: P_zracplantule  ! // PARAMETER // depth of the initial root front of the plantlet  // cm // PARPLT // 1
      real      :: P_vlaimax  ! // PARAMETER // ULAI at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
      real      :: P_pentlaimax  ! // PARAMETER // parameter of the logistic curve of LAI growth  // SD // PARPLT // 1
      real      :: P_udlaimax  ! // PARAMETER // ulai from which the rate of leaf growth decreases  // SD // PARPLT // 1
      real      :: P_abscission  ! // PARAMETER // sensescent leaf proportion falling on the soil // SD // PARPLT // 1
      real      :: P_parazofmorte  ! // PARAMETER // C/N ratio of dead leaves when the crop NNI is equal to 1 // SD // PARPLT // 1
      real      :: P_innturgmin  ! // PARAMETER // parameter of the nitrogen stress function active on leaf expansion (INNLAI), that is a bilinear function passing by the point of coordinate (P_innmin, P_innturgmin) // SD // PARPLT // 1
      real      :: P_dlaimin  ! // PARAMETER // accelerating parameter for the lai growth rate // SD // PARAMV6/PLT // 1

      real      :: P_tustressmin  ! // PARAMETER //  threshold stress (min(turfac,inns)) under which there is an effect on the LAI (supplementary senescence by ratio at the natural senescence) // SD // PARPLT // 1
      real      :: P_durviesupmax  ! // PARAMETER // proportion of additional lifespan due to an overfertilization // SD // PARPLT // 1
      real      :: P_tauxrecouvmax  ! // PARAMETER // maximal soil cover rate // m2 plante m-2 sol // PARPLT // 1
      real      :: P_tauxrecouvkmax  ! // PARAMETER // soil cover rate corresponding to the maximal crop coefficient for water requirement  // m2 plante m-2 sol // PARPLT // 1
      real      :: P_pentrecouv  ! // PARAMETER // parameter of the logistic curve of the soil cover rate increase // * // PARPLT // 1
      real      :: P_infrecouv  ! // PARAMETER // ulai at the stage AMF (inflexion point of the soil cover rate increase) // SD // PARPLT // 1
      real      :: P_rapforme  ! // PARAMETER // Ratio thickness/width of the crop form (negative when the base of the form < top) // SD // PARPLT // 1
      real      :: P_adfol  ! // PARAMETER // parameter determining the leaf density evolution within the chosen shape // m-1 // PARPLT // 1
      real      :: P_dfolbas  ! // PARAMETER // minimal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
      real      :: P_dfolhaut  ! // PARAMETER // maximal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1
      real      :: P_remobres  ! // PARAMETER // proportion of daily remobilisable carbon reserve // SD // PARPLT // 1
      real      :: P_temax  ! // PARAMETER // Maximal threshold temperature for the biomass growth  // degree C // PARPLT // 1
      real      :: P_teoptbis  ! // PARAMETER // optimal temperature for the biomass growth (if there is a plateau between P_teopt and P_teoptbis) // degree C // PARPLT // 1
      real      :: P_slamin  ! // PARAMETER // minimal SLA of green leaves // cm2 g-1 // PARPLT // 1
      real      :: P_envfruit  ! // PARAMETER // proportion envelop/P_pgrainmaxi in weight  // SD // PARPLT // 1
      real      :: P_sea  ! // PARAMETER // specifique surface of fruit envelops // cm2 g-1 // PARPLT // 1
      real      :: P_irmax  ! // PARAMETER // Maximum harvest index // SD // PARPLT // 1
      ! Ajout Lo�c Avril 2021
      real      :: P_irazomax  ! // PARAMETER // Maximum nitrogen harvest index // SD // PARPLT // 1
      real      :: P_afpf  ! // PARAMETER // parameter of the  function logistic defining sink strength of fruits (indeterminate growth) : relative fruit age at which growth is maximal // SD // PARPLT // 1
      real      :: P_bfpf  ! // PARAMETER // parameter of the logistic curve defining sink strength of fruits (indeterminate growth) :  rate of maximum growth proportionately to maximum weight of fruits // * // PARPLT // 1
      real      :: P_sensanox  ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1
      real      :: P_allocfrmax  ! // PARAMETER // maximal daily allocation towards fruits // SD // PARPLT // 1
      real      :: P_tgeljuv90  ! // PARAMETER // temperature corresponding to 90 % of frost damage on the LAI (juvenile stage) // degree C // PARPLT // 1
      real      :: P_tgelveg90  ! // PARAMETER // temperature corresponding to 90 % of frost damage on the LAI (adult stage) // degree C // PARPLT // 1
      real      :: P_tgelflo10  ! // PARAMETER // temperature corresponding to 10 % of frost damages on the flowers or the fruits // degree C // PARPLT // 1
      real      :: P_tgelflo90  ! // PARAMETER // temperature corresponding to 90 % of frost damages on the flowers or the fruits // degree C // PARPLT // 1

      integer      :: P_codelegume  ! // PARAMETER // 1 when the plant  is a legume crop, or 2 // code 1/2 // PARPLT // 0
      integer      :: P_codcalinflo  ! // PARAMETER // option of the way of calculation of the inflorescences number  // code 1/2 // PARPLT // 0
      integer      :: P_codgeljuv  ! // PARAMETER // activation of LAI frost at the juvenile stadge // code 1/2 // PARPLT // 0
      integer      :: P_codebeso  ! // PARAMETER // option computing of water needs by the k.ETP (1) approach  or resistive (2) approach // code 1/2 // PARPLT // 0
      integer      :: P_codeintercept  ! // PARAMETER // option of simulation rainfall interception by leafs: yes (1) or no (2) // code 1/2 // PARPLT // 0
      integer      :: P_codeindetermin  ! // PARAMETER // option of  simulation of the leaf growth and fruit growth : indeterminate (2) or determinate (1) // code 1/2 // PARPLT // 0
      integer      :: P_codetremp  ! // PARAMETER // option of heat effect on grain filling: yes (2), no (1) // code 1/2 // PARPLT // 0
      integer      :: P_codetemprac  ! // PARAMETER // option calculation mode of heat time for the root: with crop temperature (1)  or with soil temperature (2) // code 1/2 // PARPLT // 0
      integer      :: P_coderacine  ! // PARAMETER // Choice of estimation module of growth root in volume: standard profile (1) or by actual density (2) // code 1/2 // PARPLT // 0
      integer      :: P_codgellev  ! // PARAMETER // activation of plantlet frost // code 1/2 // PARPLT // 0
      integer      :: P_codazofruit  ! // PARAMETER // option of activation of the direct effect of the nitrogen plant status upon the fruit/grain number // code 1/2 // PARPLT // 0
      integer      :: P_codemonocot  ! // PARAMETER // option plant monocot(1) or dicot(2) // code 1/2 // PARPLT // 0
      integer      :: P_codetemp  ! // PARAMETER // option calculation mode of heat time for the plant : with air temperature (1)  or crop temperature (2) // code 1/2 // PARPLT // 0
      integer      :: P_codegdh  ! // PARAMETER // hourly (1) or daily (2) calculation of development unit // code 1/2 // PARPLT // 0
      integer      :: P_codephot  ! // PARAMETER // option of plant photoperiodism: yes (1), no (2) // code1/2 // PARPLT // 0
      integer      :: P_coderetflo  ! // PARAMETER // option slowness action of water stress before the stage DRP: yes  (1), no (2) // code 1/2 // PARPLT // 0
      integer      :: P_codebfroid  ! // PARAMETER // option of calculation of chilling requirements // code 1/2 // PARPLT // 0
      integer      :: P_codedormance  ! // PARAMETER // option of calculation of dormancy and chilling requirement // code 1/2 // PARPLT // 0
      integer      :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
      integer      :: P_codegermin  ! // PARAMETER // option of simulation of a germination phase or a delay at the beginning of the crop (1) or  direct starting (2) // code 1/2 // PARPLT // 0
      integer      :: P_codehypo  ! // PARAMETER // option of simulation of a  phase of hypocotyl growth (1) or planting of plantlets (2) // code 1/2 // PARPLT // 0
      integer      :: P_codeir  ! // PARAMETER // option of computing the ratio grain weight/total biomass: proportional to time(1), proportional to sum temperatures (2) // code 1/2 // PARPLT // 0
      integer      :: P_codelaitr  ! // PARAMETER // choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0
      integer      :: P_codlainet  ! // PARAMETER // option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI)// code 1/2 // PARPLT // 0
      integer      :: P_codetransrad  ! // PARAMETER // simulation option of radiation 'interception: law Beer (1), radiation transfers (2) // code 1/2 // PARPLT // 0
      integer      :: codetransradb
      integer      :: P_codgelveg  ! // PARAMETER // activation of LAI frost at adult stage // code 1/2 // PARPLT // 0
      integer      :: P_codgelflo  ! // PARAMETER // activation of frost at anthesis // code 1/2 // PARPLT // 0
      integer      :: P_nbfgellev  ! // PARAMETER // leaf number at the end of the juvenile phase (frost sensitivity)  // nb pl-1 // PARPLT // 1
      integer      :: P_nboite  ! // PARAMETER // "Number of  box  or  age class  of fruits for the fruit growth for the indeterminate crops " // SD // PARPLT // 1
      integer      :: P_idebdorm  ! // PARAMETER // day of the dormancy entrance // julian day // PARPLT // 1
      integer      :: P_ifindorm  ! // PARAMETER // dormancy break day // julian day // PARPLT // 1
      integer      :: P_nlevlim1  ! // PARAMETER // number of days after germination decreasing the emerged plants if emergence has not occur // days // PARPLT // 1
      integer      :: P_nlevlim2  ! // PARAMETER // number of days after germination after which the emerged plants are null // days // PARPLT // 1
      integer      :: P_nbfeuilplant  ! // PARAMETER // leaf number per plant when planting // nb pl-1 // PARPLT // 1
      integer      :: P_forme  ! // PARAMETER // Form of leaf density profile  of crop: rectangle (1), triangle (2) // code 1/2 // PARPLT // 0

      character(len=3) ::  P_stoprac  ! // PARAMETER // stage when root growth stops (LAX or SEN) // * // PARPLT // 0
      character(len=3) :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0

! parametres varietaux
      integer   :: nbVariete
! DR 17/09/2012 je mets une dimension de tableau facilement modifiable
      character(len=15), dimension(nb_variete_max) :: P_codevar    ! // PARAMETER // variety name // SD // PARPLT // 0
      real, dimension(nb_variete_max)      :: P_adens        ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_afruitpot    ! // PARAMETER // maximal number of set fruits per degree.day (indeterminate growth) // nbfruits degree.day-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_croirac      ! // PARAMETER // Growth rate of the root front  // cm degree.days-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_dureefruit   ! // PARAMETER // total growth period of a fruit at the setting stage to the physiological maturity // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_durvieF      ! // PARAMETER // maximal  lifespan of an adult leaf expressed in summation of P_Q10=2 (2**(T-Tbase)) // P_Q10 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_jvc          ! // PARAMETER // Number of vernalizing days // day // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_nbgrmax      ! // PARAMETER // Maximum number of grain // grains m-2 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_pgrainmaxi   ! // PARAMETER // Maximum weight of one grain (at 0% water content) // g // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stamflax     ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stlevamf     ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stlevdrp     ! // PARAMETER // Sum of development units between the stages LEV and DRP // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stflodrp     ! // PARAMETER // phasic duration between FLO and DRP (only for indication) // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stlaxsen     ! // PARAMETER // Sum of development units between the stages LAX and SEN // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stsenlan     ! // PARAMETER // Sum of development units between the stages SEN et LAN // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stdrpmat     ! // PARAMETER // Sum of development units between the stages DRP and MAT // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_sensiphot    ! // PARAMETER // photoperiod sensitivity (1=insensitive) // SD // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stdrpdes     ! // PARAMETER // phasic duration between the DRP stage and the beginning of the water fruit dynamics  // degree.days // PARPLT // 1
! Migration Fev 2021 Loic :
      real, dimension(nb_variete_max)      :: P_stdordebour  ! // PARAMETER // phasic duration between the dormancy break and the bud break  // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_phobase  ! // PARAMETER // Base photoperiod  // hours // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_phosat  ! // PARAMETER // saturating photoperiod // hours // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_psisto  ! // PARAMETER // absolute value of the potential of stomatal closing // bars // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_psiturg  ! // PARAMETER // absolute value of the potential of the beginning of decrease of the cellular extension // bars // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_bdens  ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_hautbase  ! // PARAMETER // Base height of crop // m // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_hautmax  ! // PARAMETER // Maximum height of crop // m // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_dlaimax  ! // PARAMETER // Maximum rate of the setting up of LAI // m2 leaf plant-1 degree d-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_dlaimaxbrut  ! // PARAMETER // Maximum rate of the setting up of LAI // m2 leaf plant-1 degree d-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_innsen  ! // PARAMETER // parameter of the nitrogen stress function active on senescence (innnsenes), bilinear function of the INN using the point (P_innmin, P_innsen) // SD // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_rapsenturg  ! // PARAMETER // threshold soil water content active to simulate water sensecence stress as a proportion of the turgor stress // SD // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_extin  ! // PARAMETER // extinction coefficient of photosynthetic active radiation canopy // SD // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_ktrou  ! // PARAMETER // Extinction Coefficient of PAR through the crop  (radiation transfer) // * // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_vitirazo  ! // PARAMETER // Rate of increase of the nitrogen harvest index // g grain g plant -1 day-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_vitpropsucre  ! // PARAMETER // increase rate of sugar harvest index  // g sugar g MS-1  j-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_vitprophuile  ! // PARAMETER // increase rate of oil harvest index  // g oil g MS-1 j-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_temin  ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_teopt  ! // PARAMETER // Optimal temperature for the biomass growth // degree C // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_slamax  ! // PARAMETER // maximal SLA of green leaves // cm2 g-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_tigefeuil  ! // PARAMETER // stem (structural part)/leaf proportion // SD // PARPLT // 1
      integer, dimension(nb_variete_max)   :: P_nbjgrain  ! // PARAMETER // Period to compute NBGRAIN // days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_nbgrmin  ! // PARAMETER // Minimum number of grain // grains m-2  // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_vitircarb  ! // PARAMETER // Rate of increase of the carbon harvest index // g grain g plant -1 day-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_vitircarbT  ! // PARAMETER // Heat rate of increase of the carbon harvest index  // g grain g plant-1 degree.day-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_stdrpnou  ! // PARAMETER // Sum of development units between the stages DRP and NOU (end of  setting) // degree.days // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_nbinflo  ! // PARAMETER // imposed number of inflorescences  // nb pl-1 // PARPLT // 1      // OUTPUT // Number of inflorescences // SD
      real, dimension(nb_variete_max)      :: P_inflomax  ! // PARAMETER // maximal number of inflorescences per plant // nb pl-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_pentinflores  ! // PARAMETER // parameter of the calculation of the inflorescences number  // SD // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_deshydbase  ! // PARAMETER // phenological rate of evolution of fruit water content (>0 or <0) // g water.g MF-1.degree C-1 // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_khaut  ! // PARAMETER // Extinction Coefficient connecting leaf area index to height crop // * // PARAM // 1
      real, dimension(nb_variete_max)      :: P_tgellev10  ! // PARAMETER // temperature corresponding to 10% of frost damage on the plantlet  // degree C // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_tgeljuv10  ! // PARAMETER // temperature corresponding to 10 % of frost damage on the LAI (juvenile stage) // degree C // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_tgelveg10  ! // PARAMETER // temperature corresponding to 10 % of frost damage on the LAI (adult stage) // degree C // PARPLT // 1
      real, dimension(nb_variete_max)      :: P_swfacmin  ! // PARAMETER // minimul value for drought stress index (turfac, swfac, senfac) // SD // PARPLT // 1

      integer   :: P_codazorac   ! // PARAMETER // activation of the nitrogen effect on root partitioning within the soil  // code 1/2 // PARPLT // 0
      integer   :: P_codtrophrac ! // PARAMETER // trophic effect on root partitioning within the soil // code 1/2/3 // PARPLT // 0
      real      :: P_minefnra    ! // PARAMETER // effect of soil nitrogen on root soil partitioning // SD // PARPLT // 1
      real      :: P_maxazorac   ! // PARAMETER // effect of soil nitrogen on root soil partitioning  // kg N ha-1 mm-1 // PARPLT // 1
      real      :: P_minazorac   ! // PARAMETER // effect of soil nitrogen on root soil partitioning // kg N ha-1 mm-1 // PARPLT // 1

      real      :: P_repracpermax  ! // PARAMETER // maximum of root biomass respective to the total biomass (permanent trophic link) // SD // PARPLT // 1
      real      :: P_repracpermin  ! // PARAMETER // minimum of root biomass respective to the total biomass (permanent trophic link) // SD // PARPLT // 1
      real      :: P_krepracperm   ! // PARAMETER // parameter of biomass root partitioning : evolution of the ratio root/total (permanent trophic link) // SD // PARPLT // 1
      real      :: P_repracseumax  ! // PARAMETER // maximum of root biomass respective to the total biomass (trophic link by thresholds) // SD // PARPLT // 1
      real      :: P_repracseumin  ! // PARAMETER // minimum of root biomass respective to the total biomass (trophic link by thresholds) // SD // PARPLT // 1
      real      :: P_krepracseu    ! // PARAMETER // parameter of biomass root partitioning : evolution of the ratio root/total (trophic link by thresholds) // SD // PARPLT // 1

      ! Ajout Bruno aout 2018 : parametres racinaires
      integer   :: P_codedisrac   ! actif (1) profil standard de distribution, inactif (2) emission racines prop a quantite existante
      real      :: P_kdisrac      ! constante de distribution des nouvelles racines dans le profil de sol
      real      :: P_alloperirac  ! taux d'allocation des reserves du perisperme a l'apex racinaire (au stade levee)

! Loic : simulation de 2 classes de racines
      integer   :: P_code_diff_root   ! if (P_code_diff_root == 2) P_lvmax = P_lvopt
      real      :: P_lvmax            ! Maximum root length density  // cm cm-3 soil
      real      :: P_rapdia
      real      :: P_RTD
      real      :: P_propracfmax
!  real      :: P_zramif
      integer   :: P_code_rootdeposition
      real      :: P_parazorac
      integer   :: P_code_acti_reserve
      integer   :: P_code_acti_reserve_ini
      real      :: P_PropresP
      real      :: P_PropresPN
      real      :: P_Efremobil
      real      :: P_Propres
      real      :: P_tauxmortresP
      real      :: P_Parazoper
      real      :: P_ParazoTmorte
      real      :: P_inilai
! Loic Novembre 2017 : option sur la partition de biomasse vers les racines
      integer   :: P_code_stress_root
!#############################################################################################################################################
! Loic Fev 2021 : Migration options et parametres nouveaux fichiers plantes v10
      integer   :: P_codephot_part ! // PARAMETER // option for simulation of the effect of decreasing photoperiod on biomass allocation // code 1/2 // PARPLT // 0
      integer   :: P_codemontaison ! // PARAMETER // code to stop the reserve limitation from the stem elongation // code 1/2 // PARPLT // 0
      integer   :: P_code_WangEngel ! // PARAMETER // option to activate Wang et Engel (1998) effect of temperature on development units for emergence :yes (1), no(2) // code 1/2 //PARPLT // 0
      real      :: P_tdoptdeb ! // PARAMETER // Optimal temperature for calculating duration between dormancy and bud break (Wang et Engel 1998) // degree C // PARPLT // 1
      integer   :: P_codemortalracine ! // PARAMETER // calculation of the root death at cutting date for grasslands : function of dry matter production between two successives cut (1), no specific root death at cutting (2) // code 1/2 //PARPLT // 1
      real      :: P_coefracoupe ! // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARPLT // 1
      real      :: P_resplmax ! // PARAMETER // maximal reserve biomass // t ha-1 // PARPLT // 1
      real      :: P_rayon  ! // PARAMETER // Average radius of roots // cm  // PARPLT // 1
      integer   :: P_codetranspitalle  ! // PARAMETER // Choice of the ratio used to calculate tiller mortality: et/etm (1) ou epc2 / eopC (2) // code 1/2 // PARAMV6 // 0
      integer   :: P_codedyntalle  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
      real      :: P_SurfApex  ! // PARAMETER // equivalent surface of a transpiring apex // m2 // PARAMV6/PLT // 1
      real      :: P_SeuilMorTalle ! // PARAMETER // relative transpiring threshold to calculate tiller mortality // mm // PARAMV6/PLT // 1
      real      :: P_SigmaDisTalle ! // PARAMETER // Coefficient used for the gamma law calculating tiller mortality //  // PARAMV6/PLT // 1
      real      :: P_VitReconsPeupl ! // PARAMETER // thermal time for the regeneration of the tiller population // nb tillers/degree C/m2 // PARAMV6 // 1
      real      :: P_SeuilReconsPeupl ! // PARAMETER // tiller density threshold below which the entire population won't be regenerated // nb tillers/m2 // PARAMV6/PLT // 1
      real      :: P_MaxTalle ! // PARAMETER // maximal density of tillers/m2 // Nb tillers/ // PARAMV6/PLT // 1
      real      :: P_SeuilLAIapex ! // PARAMETER // Maximal value of LAI+LAIapex when LAIapex isn't nil // m2/m2 // PARAMV6/PLT // 1
      real      :: P_tigefeuilcoupe ! // PARAMETER // stem (structural part)/leaf proportion the cutting day // SD // PARAMV6/PLT // 1

!#############################################################################################################################################
      integer   :: P_codefixpot  ! // PARAMETER // option of calculation of the maximal symbiotic fixation // code 1/2/3 // PARPLT // 0
      real      :: P_fixmaxveg  ! // PARAMETER // parameter to calculate symbiotic fixation as a function of the plant growth   // kg N  (t MS)-1 // PARPLT // 1
      real      :: P_fixmaxgr  ! // PARAMETER // parameter to calculate symbiotic fixation as a function of the plant growth   // kg N  (t MS)-1 // PARPLT // 1

      real      :: P_tdmindeb  ! // PARAMETER // minimal thermal threshold for hourly calculation of phasic duration between dormancy and bud breaks // degree C // PARPLT // 1
      real      :: P_tdmaxdeb  ! // PARAMETER // maximal thermal threshold for hourly calculation of phasic duration between dormancy and bud breaks // degree C // PARPLT // 1
      integer   :: P_codegdhdeb  ! // PARAMETER // option of calculation of the bud break date in hourly or daily growing degrees  // code 1/2 // PARPLT // 0

      integer  :: P_codeplisoleN  ! // PARAMETER // code for N requirement calculations at the beginning of the cycle: dense plant population (1), isolated plants (2, new formalisation) // code 1/2 // PARPLT // 0
      real     :: P_Nmeta      ! // PARAMETER // fraction of metabolic nitrogen in the plantlet // % // PARPLT // 1
      real     :: P_masecmeta  ! // PARAMETER // biomass of the plantlet containing the metabolic nitrogen // t ha-1 // PARPLT // 1

! DR 21/04/2011 P_Nres est renomme P_Nreserve car on garde Nres pour la quantite d'azote des residus au meme titre que Cres
      real      :: P_Nreserve  ! // PARAMETER // maximal amount of nitrogen in the plant reserves (distance between the maximal dilution curve and the critical dilution curve) (as a percentage of the aboveground dry weight) // % // PARPLT // 1

      integer   :: P_codeINN  ! // PARAMETER // option to compute INN: cumulated (1), instantaneous (2)  // code 1/2 // PARPLT // 0
      real      :: P_INNimin  ! // PARAMETER // INNI (instantaneous INN) corresponding to P_INNmin // SD // PARPLT // 1
      real      :: P_potgermi  ! // PARAMETER // humidity threshold from which seed humectation occurs, expressed in soil water potential  // Mpa // PARPLT // 1
      integer   :: P_nbjgerlim  ! // PARAMETER // Threshold number of day after grain imbibition without germination lack // days // PARPLT // 1
      real      :: P_propjgermin  ! // PARAMETER // minimal proportion of the duration P_nbjgerlim when the temperature is higher than the temperature threshold P_Tdmax  // % // PARPLT // 1

      real      :: P_cfpf  ! // PARAMETER // parameter of the first potential growth phase of fruit, corresponding to an exponential type function describing the cell division phase. // SD // PARPLT // 1
      real      :: P_dfpf  ! // PARAMETER // parameter of the first potential growth phase of fruit, corresponding to an exponential type function describing the cell division phase. // SD // PARPLT // 1
      real      :: P_tcxstop  ! // PARAMETER // threshold temperature beyond which the foliar growth stops // degree C // PARPLT // 1

! NB le 13/05/05 battance
      real      :: P_vigueurbat  ! // PARAMETER // indicator of plant vigor allowing to emerge through the crust  // between 0 and 1 // PARPLT // 1

! DR le 03/01/06 parametres inaki stressphot
      real      :: P_phobasesen  ! // PARAMETER // photoperiod under which the photoperiodic stress is activated on the leaf lifespan // heures // PARPLT // 1
      real      :: P_dltamsmaxsen  ! // PARAMETER // threshold value of deltams from which there is no more photoperiodic effect on senescence // t ha-1j-1 // PARPLT // 1
      integer   :: P_codestrphot  ! // PARAMETER // activation of the photoperiodic stress on lifespan : yes (1), no (2) // code 1/2 // PARPLT // 0

      real :: P_dltamsminsen  ! // PARAMETER // threshold value of deltams from which the photoperiodic effect on senescence is maximal // t ha-1j-1 // PARPLT // 1
      real :: P_alphaphot  ! // PARAMETER // parameter of photoperiodic effect on leaf lifespan // P_Q10 // PARPLT// 1
      real :: P_alphaco2  ! // PARAMETER // coefficient allowing the modification of radiation use efficiency in case of  atmospheric CO2 increase // SD // PARPLT // 1

! **************************************
! parametres specifiques d'initialisation de l'etat de la plante pour le debut de la simulation
      character(len=3) :: P_stade0  ! // PARAMETER // Crop stage used at the beginning of simulation // * // INIT // 0
! ** pas de distinction ombre/soleil pour les variables de depart
      real       :: P_lai0  ! // PARAMETER // Initial leaf area index // m2 m-2 // INIT // 1
      real       :: P_magrain0  ! // PARAMETER // initial grain dry weight // g m-2 // INIT // 1
      real       :: P_zrac0  ! // PARAMETER // initial depth of root front  // cm // INIT // 1
! parametres d'initialisation si code_acti_reserve == 1
      real       :: P_maperenne0  ! // PARAMETER // initial value of biomass of storage organs in perennial crops // t ha-1 // INIT // 1
      real       :: P_QNperenne0  ! // PARAMETER // initial value of nitrogen amount in storage organs in perennial crops // kg ha-1 // INIT // 1
      real       :: P_masecnp0  ! // PARAMETER // initial non perennial organs biomass // t ha-1 // INIT // 1
      real       :: P_QNplantenp0  ! // PARAMETER // initial non perennial organs N content // kg ha-1 // INIT // 1
      ! parametres d'initialisation si code_acti_reserve == 2
      real       :: P_masec0  ! // PARAMETER // initial biomass // t ha-1 // INIT // 1
      real       :: P_QNplante0  ! // PARAMETER // initial N content // kg ha-1 // INIT // 1
      real       :: P_restemp0  ! // PARAMETER // initial reserve biomass // t ha-1 // INIT // 1

! initialisation profil racinaire
      real, dimension(5)  :: P_densinitial  ! // PARAMETER // Table of initial root density of the 5 horizons of soil for fine earth // cm cm-3 // INIT // 1

! DR 14/04/2016 on ajoute les varaibles pariries
      real       :: mafeuiltombe0(0:2)   ! // OUTPUT // Dry matter of fallen leaves // t.ha-1
      real       :: masecneo0(0:2)  ! // OUTPUT // Newly-formed dry matter  // t.ha-1
      real       :: msneojaune0(0:2)   ! // OUTPUT // Newly-formed senescent dry matter  // t.ha-1
      real       :: dltamsen0(0:2)      ! // OUTPUT // // t.ha-1
      real       :: mafeuiljaune0(0:2)   ! // OUTPUT // // t.ha-1

! **************************************
! *         Variables plante           *
! **************************************
!** declarations  integer
      integer      :: ficbil
      integer      :: group
      integer      :: parapluie
      integer      :: codeinstal

      integer      :: nsencour
      integer      :: nsencourpre
      integer      :: ndebsenracf    ! first day when fine roots start to die
      integer      :: ndebsenracg    ! first day when coarse roots start to die
      integer      :: ndecalf        ! number of days remaining in the previous simulation without mortality of fine roots
      integer      :: ndecalg        ! number of days remaining in the previous simulation without mortality of coarse roots
      integer      :: nsencourpreracf
      integer      :: nsencourpreracg
! Bruno juin 2017 jultrav et julres ont pour dimension 11 et pas 10
      integer      :: numjtrav(11)
      integer      :: numjres(11)

      integer      :: nnou
      integer      :: nbj0remp      ! // OUTPUT // Number of shrivelling days //
      integer      :: nbjgel      ! // OUTPUT // Number of frosting days active on the plant // SD
      integer      :: nfauche(10)

      integer      :: nlanobs
      integer      :: nlax
      integer      :: nrecobs
      integer      :: nlaxobs
      integer      :: nlev
      integer      :: nlevobs
      integer      :: nmatobs
      integer      :: nplt
      integer      :: ndrp
      integer      :: nbrecolte
      integer      :: nrecint(20)
      integer      :: ndebdes
      integer      :: ntaille
      integer      :: nger
      integer      :: igers   ! // OUTPUT // Date of germination // jour julien
      integer      :: inous   ! // OUTPUT // Date of end of setting of harvested organs // jour julien
      integer      :: idebdess   ! // OUTPUT // Date of onset of water dynamics in harvested organs // jour julien
      integer      :: ilans   ! // OUTPUT // Date of LAN stage (leaf index nil) // jour julien
      integer      :: iplts   ! // OUTPUT // Date of sowing or planting // jour julien
      integer      :: compretarddrp
      integer      :: iamfs   ! // OUTPUT // Date of AMF stage // jour julien
      integer      :: idebdorms   ! // OUTPUT // Date of entry into dormancy // jour julien
      integer      :: idrps   ! // OUTPUT // Date of start of filling of harvested organs // jour julien
      integer      :: ifindorms   ! // OUTPUT // Date of emergence from dormancy // jour julien
      integer      :: iflos   ! // OUTPUT // Date of flowering // jour julien
      integer      :: ilaxs   ! // OUTPUT // Date of LAX stage (leaf index maximum) // jour julien
      integer      :: namf
      integer      :: imats   ! // OUTPUT // Date of start of physiological maturity // jour julien
      integer      :: irecs   ! // OUTPUT // Date of harvest (first if several) // jour julien
      integer      :: isens   ! // OUTPUT // Date of SEN stage // jour julien
      integer      :: nsen
      integer      :: nsenobs

      integer      :: ndebdorm
      integer      :: nfindorm
      integer      :: nbfeuille   ! // OUTPUT // Number of leaves on main stem // SD
      integer      :: nflo
      integer      :: nfloobs
      integer      :: nstopfeuille
      integer      :: nstopres
      integer      :: mortplante
      integer      :: mortplanteN
! DR 09/03/2016 on passe le nb d'eclaircissage possible a 10
      integer      :: neclair(10)
      integer      :: nrogne
      integer      :: neffeuil
      integer      :: jdepuisrec
      integer      :: namfobs
      integer      :: nlan
      integer      :: nrec
      integer      :: nrecbutoir

      integer      :: ndes
      integer      :: nmat
      integer      :: ndrpobs
      integer      :: ndebdesobs
      integer      :: ilevs   ! // OUTPUT // Date of emergence // jour julien
      integer      :: numcoupe  ! // OUTPUT // Cut number // SD
      integer      :: nst1coupe
      integer      :: nst2coupe
      integer      :: ndebsen
      integer      :: nbjTmoyIpltJuin
      integer      :: nbjTmoyIpltSept

! merge trunk 23/11/2020
      ! 15/04/2020 DR j'ajoute 1 variable operate
      integer     :: day_after_emergence

      integer      :: ndno
      integer      :: nfno
      integer      :: nfvino

      real      :: fixpotC
      real      :: fixreelC
      real      :: offrenodC
      real      :: fixmaxC
      real      :: QfixC
      real      :: fixpotfno
      real      :: propfixpot
      integer      :: nrecalpfmax

      logical      :: fauchediff
      logical      :: sioncoupe
      logical      :: onarretesomcourdrp
      logical      :: etatvernal
      logical      :: gelee

      real      :: LRACH(5)      ! // OUTPUT // Root length density in the horizon i // cm.cm-3
      real      :: lracf(5)      ! // OUTPUT // Length density of fine roots in the horizon i // cm.cm-3
      real      :: lracg(5)      ! // OUTPUT // Length density of coarse roots in the horizon i // cm.cm-3
      real      :: msracf(5)     ! // OUTPUT // Biomass of fine roots in the horizon i // t.ha-1
      real      :: msracg(5)     ! // OUTPUT // Biomass of coarse roots in the horizon i // t.ha-1
      real      :: msracmortf(5) ! // OUTPUT // Cumulative biomass of dead fine roots in the horizon i // t.ha-1
      real      :: msracmortg(5) ! // OUTPUT // Cumulative biomass of dead coarse roots in the horizon i // t.ha-1
! Ajout Loic Aout 2016
      real      :: ratioFT       ! Leaves to stem ratio
      real      :: ratioTF       ! stem to leave ratio: variable taking the value of 0 or P_tigefeuil
      real      :: dltaremobsen  ! // Leaves biomass remobilized during senescence t ha-1 j-1
      real      :: CNrespstruc
      real      :: msresgel(0:2)
      real      :: QNresgel(0:2)
      real      :: msresgel0
      real      :: QNresgel0
      real      :: masecavantfauche
      real      :: QNplanteavantfauche
! Ajout Florent C. et Fabien, Octobre 2019
      real      :: Qfix0

      real      :: cinterpluie   ! // OUTPUT // Amount of rain intercepted by the leaves // mm
      real      :: totpl
      real      :: rc   ! // OUTPUT // Resistance of canopy  // s.m-1
      real      :: fco2s ! // OUTPUT // specie-dependant effect on stomate closure  // SD
      real      :: lracz(nbCouchesSol)
      real      :: cumlracz   ! // OUTPUT // Sum of the effective root lengths  // cm root.cm -2 soil
      real      :: dfol   ! // OUTPUT //  "Within the shape  leaf density" // m2 m-3
      real      :: rombre   ! // OUTPUT // Radiation fraction in the shade // 0-1
      real      :: rsoleil   ! // OUTPUT // Radiation fraction in the full sun // 0-1
      real      :: somcourfauche  ! // OUTPUT //actual sum of temperature between 2 cuts // 0-1
      ! DR et Fr 13/02/2015
      real      :: reste_apres_derniere_coupe
      real      :: QNplanteres
! DR et ML 04/09/2014 on indexe sur AO/AS les variables de l'abscission
      real      :: QCplantetombe(0:2)
      real      :: QNplantetombe(0:2)

      real      :: QCrogne ! Bruno: ajout variables cumul des quantites C et N rognees 22/05/2012
      real      :: QNrogne
      real      :: QCrac      ! C des racines vivantes
      real      :: QNrac      ! N des racines vivantes
      real      :: msracmort  ! Biomass of dead roots
      real      :: QCracmort  ! C des racines mortes
      real      :: QNracmort  ! N des racines mortes
      real      :: udevlaires(10)
      real      :: cescoupe
      real      :: cetcoupe
      real      :: cepcoupe
      real      :: cetmcoupe
      real      :: cprecipcoupe
      real      :: cep   ! // OUTPUT // Transpiration integrated over the cropping season  // mm
      real      :: cprecip   ! // OUTPUT // Water supply integrated over the cropping season // mm
      real      :: cet   ! // OUTPUT // Evapotranspiration integrated over the cropping season // mm
      real      :: ces   ! // OUTPUT // Evaporation integrated over the cropping season // mm
      real      :: cetm   ! // OUTPUT // Maximum evapotranspiration integrated over the cropping season // mm
      real      :: masectot

      real      :: rendementsec
      real      :: str1coupe
      real      :: stu1coupe
      real      :: str2coupe
      real      :: stu2coupe
      real      :: inn1coupe
      real      :: diftemp1coupe
      real      :: inn2coupe
      real      :: diftemp2coupe
      real      :: qressuite       ! quantity of aerial residues from the previous crop // t.ha-1
      real      :: qressuite_tot   ! quantity of total residues (aerials + roots) from the previous crop // t.ha-1
      real      :: QNressuite
      real      :: QNressuite_tot
      real      :: QCressuite
      real      :: QCressuite_tot
      real      :: CsurNressuite_tot   !  // OUTPUT // Carbon to Nitrogen ratio of total harvest residues (aerials + roots) // t.ha-1

      real      :: pfmax
      real      :: stemflow   ! // OUTPUT // Water running along the stem // mm
      real      :: precrac(nbCouchesSol)

      real      :: lracsenzf(nbCouchesSol)
      real      :: lracsenzg(nbCouchesSol)
      real      :: drlsenf(nbCouchesSol)
      real      :: drlseng(nbCouchesSol)

      !> length of fine roots
      real      :: rlf(nbCouchesSol)
      !> length of coarse roots
      real      :: rlg(nbCouchesSol)
      real      :: rlf_veille(nbCouchesSol)      ! On se passe du tableau temporel !
      real      :: rlg_veille(nbCouchesSol)      ! On se passe du tableau temporel !
      real      :: rlf0(nbCouchesSol)             ! Root length of fine roots at time 0
      real      :: rlg0(nbCouchesSol)             ! Root length of coarse roots at time 0
      real      :: somtemprac
      real      :: somtempracvie                    ! cumulative thermal time over successive runs
      real      :: poussracmoy   ! // OUTPUT // "Average index of the effect of soil constraints on the rooting profile (option  true density )" // 0-1
      real      :: Emd   ! // OUTPUT // Direct evaporation of water intercepted by leafs  // mm
      real      :: diftemp1
      real      :: diftemp2
      real      :: flrac(nbCouchesSol)
      real      :: ftemp   ! // OUTPUT // Temperature-related EPSIBMAX reduction factor // 0-1
      real      :: udevcult   ! // OUTPUT // Effective temperature for the development, computed with TCULT // degree.days
      real      :: udevair   ! // OUTPUT // Effective temperature for the development, computed with TAIR // degree.days
      real      :: ebmax
      real      :: fpari
      real      :: efdensite_rac   ! // OUTPUT // density factor on root growth // 0-1
      real      :: efdensite     ! // OUTPUT // density factor on leaf area growth  // 0-1
      real      :: ulai(0:731)                ! temporel pour senescence    // OUTPUT // Daily relative development unit for LAI // 0-3
      real      :: caljvc
      real      :: somelong
      real      :: somger
      real      :: cdemande   ! // OUTPUT // Sum of daily nitrogen need of the plant   // kg.ha-1
      real      :: zrac   ! // OUTPUT // Depth reached by root system // cm
      real      :: znonli
      real      :: deltaz   ! // OUTPUT // Deepening of the root front  // cm day-1
      real      :: difrac
      real      :: resrac   ! // OUTPUT // Soil water reserve in the root zone // mm
      real      :: Scroira

! DR 11/06/2013 ajout des variables de stress racinaire
      ! 11/06/2013 la variable efda devient une varaible de sortie journaliere pour Simtraces
      real     :: efda      ! // OUTPUT // efda defines the effect of soil compaction through bulk density // 0-1
      real     :: efnrac_mean    ! // OUTPUT // effect of mineral nitrogen, which contributes to the root distribution in the layers with high mineral nitrogen content. // 0-1
      real     :: humirac_mean ! // OUTPUT // soil dryness // 0-1
      real     :: humirac(nbCouchesSol) ! // OUTPUT //profile soil dryness // 0-1
      real     :: efnrac(nbCouchesSol) ! // OUTPUT // effect of minral nitrogen on roots // 0-1
      real     :: rlj ! // OUTPUT // roots length growth rate  // m.d-1
      real     :: dltmsrac_plante

! ** variables qui etaientt AO/AS et qu'on a rendu communes
      real      :: cumraint   ! // OUTPUT // Sum of intercepted radiation  // Mj.m-2
      real      :: cumrg   ! // OUTPUT // Sum of global radiation during the stage sowing-harvest   // Mj.m-2

      real     :: irrigprof(0:nbCouchesSol)
      real     :: stlevdrp0
      real     :: stsenlan0
      real     :: stlaxsen0
      real     :: remobil
      real     :: somcourdrp
      real     :: dtj(0:maxdayg)     ! temporel pour senescence (racinaire)    duree de vie maximale = 10 ans
      ! A VOIR copie depuis trunk
      !real      :: qressuite       ! // OUTPUT // quantity of aerial residues from the previous crop // t.ha-1
  !! DR et EC 25/07/2012 on ajoute Qressuite_tot pour prendre en compte les racines dans le bilan
      !real      :: qressuite_tot   ! // OUTPUT // quantity of total residues (aerials + roots) from the previous crop // t.ha-1
      !real      :: CsurNressuite_tot   !  // OUTPUT // Carbon to Nitrogen ratio of total harvest residues (aerials + roots) // t.ha-1
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!
      real :: Nexporte      ! // OUTPUT // total of exported nitrogen // kgN.ha-1
      real :: Nrecycle      ! // OUTPUT // total of recycle nitrogen (unexported nitrogen at harvest + nitrogen from the fallen leaves) // kgN.ha-1
      real :: MSexporte     ! // OUTPUT // total of exported carbon // t.ha-1
      real :: MSrecycle     ! // OUTPUT // total of recycle carbon (unexported nitrogen at harvest + nitrogen from the fallen leaves) // t.ha-1
      real :: p1000grain    ! // OUTPUT // 1000 grain weight // g.m-2
      real :: somudevair    ! // OUTPUT // sum of air temperature from sowing // degree C
      real :: somudevcult   ! // OUTPUT // sum of crop temperature from sowing // degree C
      real :: somupvtsem    ! // OUTPUT // sum of development units from sowing // degree C

      real      :: CsurNressuite
      real      :: cumlraczmaxi

      real      :: cumdevfr
      real      :: nbfruit
      real      :: tetstomate   ! // OUTPUT // Threshold soil water content limiting transpiration and photosynthesis  // % vol
      real      :: teturg   ! // OUTPUT // Threshold soil water content limiting surface area growth of leaves // % vol
      real      :: rltotf   ! // OUTPUT // Total length of fine roots  // cm root.cm -2 soil
      real      :: rltotg   ! // OUTPUT // Total length of coarse roots  // cm root.cm -2 soil
      real      :: rltot   ! // OUTPUT // Total length of roots  // cm root.cm -2 soil
      real      :: rfpi   ! // OUTPUT // Slowing effect of the photoperiod on plant development  // 0-1
      real      :: lracsentotf   ! // OUTPUT // Total length of fine senescent roots  // cm root.cm -2 soil
      real      :: lracsentotg   ! // OUTPUT // Total length of coarse senescent roots  // cm root.cm -2 soil
      real      :: largeur   ! // OUTPUT // Width of the plant shape  // m
! ** nadine modifs pour boucler bilan d'azote 8/03/99

      real      :: utno
      real      :: pfv
      real      :: pfj
      real      :: pftot
      real      :: pfe
      real      :: pft
      real      :: wcf
      real      :: wct
      real      :: wce
      real      :: stpltlev
      ! DR 18/07/2012 je rajoute la germination
      real      :: stpltger
      real      :: stdrpsen
      real      :: stmatrec
      real      :: upvtutil
      real      :: upvt(0:731)                ! pas besoin du tableau temporel            // OUTPUT // Daily development unit  // degree.days
      real      :: somcour   ! // OUTPUT // Cumulative units of development between two stages // degree.days
      real      :: reajust
      real      :: upobs(0:731)               ! tableau de forcage
      real      :: stlevamf0
      real      :: stamflax0
      real      :: varrapforme

      real      :: anoxmoy   ! // OUTPUT // Index of mean anoxia on the root depth  // 0-1
      real      :: chargefruit   ! // OUTPUT // Amount of filling fruits per plant // nb fruits.plant-1
      real      :: coeflev
      real      :: cumflrac
      real      :: densitelev
      real      :: durvieI
      real      :: exobiom   ! // OUTPUT // Index of excess water active on surface growth // 0-1

      real      :: etr_etm1
      real      :: etm_etr1
      real      :: etr_etm2
      real      :: etm_etr2

      real      :: exofac   ! // OUTPUT // Variable for excess water // 0-1
      real      :: exofac1
      real      :: exofac2
      real      :: exolai   ! // OUTPUT // Index for excess water active on growth in biomass // 0-1
      real      :: fco2  ! // OUTPUT // specie-dependant CO2 effect on radiation use efficiency // SD
      real      :: fgelflo   ! // OUTPUT // Frost index on the number of fruits // 0-1
      real      :: fgellev
      real      :: fstressgel   ! // OUTPUT // Frost index on the LAI // 0-1
      real      :: ftempremp
      real      :: gel1   ! // OUTPUT // frost damage on foliage before amf // %
      real      :: gel2   ! // OUTPUT // frost damage on foliage after amf // %
      real      :: gel3   ! // OUTPUT // frost damage on foliage or fruits // %
      real      :: izrac   ! // OUTPUT // Index of excess water stress on roots  // 0-1
      real      :: idzrac
      real      :: nbfrote
      real      :: rmaxi   ! // OUTPUT // Maximum water reserve utilised // mm
      real      :: somcourutp
      real      :: somcourno
      real      :: somfeuille
      real      :: somtemp   ! // OUTPUT // Sum of temperatures // degree C.j
      real      :: somupvt
      real      :: somupvtI

      real      :: stdrpmatini
      real      :: stflodrp0
      real      :: stlevflo
      real      :: TmoyIpltJuin   ! // OUTPUT // Sum of temperatures from sowing or planting (IPLT stage) until June the 30th // degree C
      real      :: TmoyIpltSept   ! // OUTPUT // Sum of temperatures from sowing or planting (IPLT stage) until September the 30th // degree C
      real      :: zracmax
      real      :: rfvi   ! // OUTPUT // Slowing effect of the vernalization on plant development // 0-1
      real      :: racnoy(0:731)
      real      :: msrac(0:731)               ! pas besoin du tableau temporel, une simple sauvegarde de n-1 pourrait suffire    // OUTPUT // Estimated dry matter of the roots // t.ha-1
      real      :: tdevelop(0:731)            ! temporel pour senescence
      real      :: cu(0:731)                  ! pas besoin du tableau temporel
      real      :: utp(0:731)                 ! pas besoin du tableau temporel

      integer   :: nst1
      integer   :: nst2

      real      :: inn1
      real      :: inn2
      real      :: str1
      real      :: str2
      real      :: stu1
      real      :: stu2

      real      :: etm_etr1moy
      real      :: etr_etm1moy
      real      :: etr_etm2moy
      real      :: etm_etr2moy

      real      :: exofac1moy   ! // OUTPUT // "Mean value of the variable for excess water during the vegetative stage (from emergence  LEV  to fruit establishment  DRP )  " // 0-1
      real      :: exofac2moy   ! // OUTPUT // "Mean value of the variable for excess water during the reproductive stage (from fruit establishment  DRP  to maturity  MAT )  " // 0-1
      real      :: inn1moy   ! // OUTPUT // Average of index of nitrogen stress over the vegetative stage // SD
      real      :: inn2moy   ! // OUTPUT // Average index of nitrogen stress over the reproductive stage // SD
      real      :: swfac1moy   ! // OUTPUT // Average of index of stomatic water stress over the vegetative stage // 0-1
      real      :: swfac2moy   ! // OUTPUT // Average of index of stomatic water stress over the reproductive stage // 0-1
      real      :: turfac1moy   ! // OUTPUT // Average of index of turgescence water stress over the vegetative stage // 0-1
      real      :: turfac2moy   ! // OUTPUT // Average of index of turgescence water stress over the reproductive stage // 0-1

      real      :: mafrais_nrec
      real      :: pdsfruitfrais_nrec
      real      :: mabois_nrec
      real      :: H2Orec_nrec
      real      :: chargefruit_nrec
      real      :: CNgrain_nrec
      real      :: CNplante_nrec
      ! DR 26/07/2016 on garde le CNplante de la veille pour le calcul des restitutions paturage
      real      :: CNplante_veille
      real      :: QNgrain_nrec
      real      :: QNplantenp_nrec
      real      :: QNrac_nrec
      real      :: masecnp_nrec
      real      :: magrain_nrec
      real      :: mafruit_nrec
      real      :: Qfix_nrec
      real      :: Qngrain_ntailleveille    ! on enregistre la valeur de QNgrain de la veille du jour de taille

      real      :: nbgraingel
      real      :: QNplanteCtot
! EC 06/08/2012 ajout d'une variable quantite de N exportee pour les cultures fauchees
      real      :: QNplantefauche
! dr 170306 rajout variables cumulees
      real      :: ctmoy   ! // OUTPUT // Air temperature integrated over the cropping season // degree C
      real      :: ctcult   ! // OUTPUT // Crop temperature (TCULT) integrated over the cropping season // degree C
! DR 29/12/2014 ajout de cumul de tcult max pour faire moyenne pour Giacomo Agmip Canopy temp
      real      :: ctcultmax   ! // OUTPUT // maximum Crop temperature (TCULT) integrated over the cropping season // degree C

      real      :: cetp   ! // OUTPUT // Potential evapotranspiration (PET) integrated over the cropping season // mm
      real      :: crg   ! // OUTPUT // Global radiation integrated over the cropping season // Mj/m2
      real      :: cum_et0   ! // OUTPUT // maximum evapotranspiration (eop+eos)/mm

! DR 20/07/06 on compte le nb de jours ou l'on a repousse la fauche
      integer   :: nbrepoussefauche
      !DR 26/06/2015 je garde les valuers de la derniere coupe quand est n'a pas pu se faire pour l'annee d'apres
      real      :: anitcoupe_anterieure
      real      :: hautcoupe_anterieure
      real      :: msresiduel_anterieure
      real      :: lairesiduel_anterieure
      real      :: ulai0
      real      :: durvie0(0:2)
      integer   :: codebbch0

      real    :: restit_anterieure
      real    :: mscoupemini_anterieure
      real      :: tempfauche_realise
      real      :: tempeff   ! // OUTPUT // Efficient temperature for growth // degree C
      real      :: tempfauche_ancours(0:20)

! DR et FR 08/06/2016 on conserve les sommes auquels sont effectivement realisees les fauches
      real      :: som_vraie_fauche(20)
      real      :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
      ! dr 12/09/2012 j'ajoute la densite equivalente comme variable distincte de densite
      real      :: densiteequiv   ! // OUTPUT // Actual sowing density // plants.m-2
      real      :: stdrpdes0
      real      :: str1intercoupe   ! // OUTPUT // stomatal water stress average during the cut (for forage crops in vegetative phase : emergence to maximum LAI )  // 0-1
      real      :: stu1intercoupe   ! // OUTPUT // turgescence water stress average during the cut (for forage crops in vegetative phase : emergence to maximum LAI )  // 0-1
      real      :: inn1intercoupe   ! // OUTPUT // nitrogen stress (inn) average during the cut (cut crop vegetative phase: emergence to maximum LAI)  // 0-1
      real      :: diftemp1intercoupe   ! // OUTPUT // mean difference between crop surface temperature and air temperature during the cut (cut crop vegetative phase: emergence to maximum LAI) // degree C
      real      :: str2intercoupe   ! // OUTPUT // stomatal water stress average during the cut (cut crop reproductive phase)  // 0-1
      real      :: stu2intercoupe   ! // OUTPUT // turgescence water stress average during the cut (for forage crops in reproductive phase: maximum LAI  to maturity)  // 0-1
      real      :: inn2intercoupe   ! // OUTPUT // nitrogen stress (inn) average during the cut (for forage crops in reproductive phase: maximum LAI  to maturity)  // 0-1
      real      :: diftemp2intercoupe   ! // OUTPUT // mean difference between crop surface temperature and air temperature during the cut (cut crop reproductive phase: maximum LAI  to maturity) // degree C
      ! DR 03/09/2012 on l'enleve ne sert visiblement pas
      ! real      :: totircoupe(20)   ! // OUTPUT // Total amount of water inputs for a given cut // mm
      ! real      :: totapNcoupe(20)   ! // OUTPUT // Total amount of fertiliser N added for a given cut // kgN.ha-1

      integer   :: ficdrat

! DR 31/01/08 CLIMATOR : on garde les dates des stades lax pour la prairie
      integer   :: ilaxs_prairie(10)

! DR et ML et SYL 15/06/09
! **************************
! introduction de la fin des modifications de Sylvain (nadine et FR) dans le cadre du projet PERMED
! SYL 12/09/07
      real    :: mortmasec   ! // OUTPUT // Dead tiller biomass  // t.ha-1
      real    :: densitemax
      real    :: drlsenmortalle   ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.d-1
      integer :: imontaisons   ! // OUTPUT // Date of start of stem elongation // julian day
      real    :: mortalle   ! // OUTPUT // number of dead tillers per day // tillers.d-1

      real    :: densiteger
      real      :: mafruit   ! // OUTPUT // Dry matter of harvested organs // t.ha-1
      real      :: matuber   ! // OUTPUT // Dry matter of harvested organs for sugarbeet // t.ha-1
      real      :: matuber_nrec ! DR 06/04/2012 on garde le matuber a la recolte
      real      :: dNdWcrit
      real      :: dNdWmax
      real      :: flurac   ! // OUTPUT // Nitrogen absorption flow associated with the limiting absorption capacity of the plant // kgN ha-1 j-1
      real      :: flusol   ! // OUTPUT // Nitrogen absorption flux associated with limiting transfer soil  --> root  // kgN ha-1 j-1
      ! 05/05/2015 DR et FR ajout de la variable recoltee pour les prairies
      real      :: msrec_fou   ! // OUTPUT // Dry matter of harvestable organs for forages// t.ha-1
      ! Loic Avril 2021 ajout de variables de sortie pour isop
      real      :: msrec_fou_coupe ! // OUTPUT // Dry matter of harvested organs for forages at cutting // t.ha-1
      real      :: msrec_fou_tot ! // OUTPUT // Dry matter of harvestable organs for forages at cutting // t.ha-1
      real      :: mafou       ! // New formed dry matter for forages // t.ha-1

! dr - 08/04/2010
! on a ajoute le codebbch pour ne pas avoir a modifier les fichiers dans la nouvelle version.
      character(len=3) :: P_stadebbchplt  ! // PARAMETER // equivalent stage in BBCH-scale (sowing) //  // PARPLT // 0
      character(len=3) :: P_stadebbchger  ! // PARAMETER // equivalent stage in BBCH-scale (germination) //  // PARPLT // 0
      character(len=3) :: P_stadebbchlev  ! // PARAMETER // equivalent stage in BBCH-scale (emergence) //  // PARPLT // 0
      character(len=3) :: P_stadebbchamf  ! // PARAMETER // equivalent stage in BBCH-scale (amf) //  // PARPLT // 0
      character(len=3) :: P_stadebbchlax  ! // PARAMETER // equivalent stage in BBCH-scale (lax) //  // PARPLT // 0
      character(len=3) :: P_stadebbchsen  ! // PARAMETER // equivalent stage in BBCH-scale (senescence) //  // PARPLT // 0
      character(len=3) :: P_stadebbchflo  ! // PARAMETER // equivalent stage in BBCH-scale (flowering) //  // PARPLT // 0
      character(len=3) :: P_stadebbchdrp  ! // PARAMETER // equivalent stage in BBCH-scale (drp) //  // PARPLT // 0
      character(len=3) :: P_stadebbchnou  ! // PARAMETER // equivalent stage in BBCH-scale (fruit set) //  // PARPLT // 0
      character(len=3) :: P_stadebbchdebdes  ! // PARAMETER // equivalent stage in BBCH-scale (debdes) //  // PARPLT // 0
      character(len=3) :: P_stadebbchmat  ! // PARAMETER // equivalent stage in BBCH-scale (maturity) //  // PARPLT // 0
      character(len=3) :: P_stadebbchrec  ! // PARAMETER // equivalent stage in BBCH-scale (harvest) //  // PARPLT // 0
      character(len=3) :: P_stadebbchfindorm  ! // PARAMETER // equivalent stage in BBCH-scale (end of dormancy) //  // PARPLT // 0

      ! DR 19/09/2012 on les met dans la plante
      real :: profextN      ! // OUTPUT // Average depth of Nitrogen absorption // cm
      real :: profexteau    ! // OUTPUT // Average depth of water absorption // cm
      integer :: age_prairie     ! // OUTPUT // forage crop age from sowing // an
      real :: RsurRUrac     ! // OUTPUT // Fraction of available water reserve (R/RU) over the root profile // 0 to 1
      real :: RUrac     ! // OUTPUT // maximum available water reserve over the root profile // mm
      real :: somcourmont   ! // OUTPUT // Cumulatied units of development from the start of vernalisation // degree.days
      real :: psibase ! P_nbplantes       // OUTPUT // Predawn leaf water potential potentiel foliaire de base // Mpascal
      integer :: nbjpourdecisemis      ! // OUTPUT // "Number of days until sowing is launched when it's postponed by the   sowing decision  option activation" // days
      integer :: nbjpourdecirecolte      ! // OUTPUT // "Number of days until harvest is launched when it's postponed by the  harvest decision  option activation" // days
      real :: mortreserve   ! // OUTPUT // Reserve biomass corresponding to dead tillers // t.ha-1.d-1

      integer  :: codeperenne0  ! // variable de sauvergarde  // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // // 0

!DR 19/02/2016 j'ajoute les chgts unites, je ne les indice pas sur AOAS puisque c'est que des sorties
      real  ::   masec_kg_ha
      real  ::   mafruit_kg_ha
      real  ::   mafeuil_kg_ha
      real  ::   matigestruc_kg_ha
      real  :: gel1_percent   ! // OUTPUT // frost damage on foliage before amf // %
      real  :: gel2_percent   ! // OUTPUT // frost damage on foliage after amf // %
      real  :: gel3_percent   ! // OUTPUT // frost damage on foliage or fruits // %
      real  :: nbinflo_recal  ! // OUTPUT // imposed number of inflorescences  // nb pl-1
      integer :: codebbch_output      ! // OUTPUT // BBCH stage (see plant file) // 1-99
      real      :: ebmax_gr  ! // OUTPUT // Maximum radiation use efficiency during the vegetative stage (AMF-DRP) // cg MJ-1
      real      :: fpari_gr   ! // OUTPUT // radiation factor on the calculation of conversion efficiency // g MJ-1
      real  :: lai_to_sen !// variable de sauvergarde du lai : P_lai0 ou lai residuel apres coupe pour le faire senescer

! DR 17/11/2021 j'ajoute les variables qui sotn issus de calcul dans CorrespondanceVariableDeSorties.f90
      real    :: CsurNrac
      real    :: CsurNracmort
      real    :: HI_C
      real    :: HI_N

! ********************************
! *       Variables AO/AS        *
! ********************************

!: Les variables AO/AS pourraient etre indicees non pas sur un indice fixe egal a 2
!: En effet, la plante principale n'a que la partie AS, et seule la plante dominee a AO/AS
!: On pourrait donc creer un taille de tableau dynamique selon la dominance de la plante.

      real      :: surf(2) ! surface de la plante sur chaque composante A l'Ombre, Au Soleil            // OUTPUT // Fraction of surface in the shade // 0-1
      real      :: surfSous(2) ! surface de la plante sur chaque composante A l'Ombre, Au Soleil
      real      :: tursla(0:2)
      real      :: allocfruit(0:2)   ! // OUTPUT // Allocation ratio of  assimilats to the  fruits 0 to 1  // 0-1
      real      :: mafeuiljaune(0:2)   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
      real      :: somsenreste(0:2)
      real      :: restemp(0:2)   ! // OUTPUT // Biomass reserves (carbohydrates) in shoots that can be accumulated or mobilized for crop growth // t ha-1
      real      :: deltares(0:2)
      real      :: misenreserve(0:2)
      real      :: innlai(0:2)   ! // OUTPUT // Index of nitrogen stress active on leaf growth // P_innmin to 1
      real      :: lairogne(0:2)
      real      :: biorogne(0:2)
      real      :: sla(0:2)   ! // OUTPUT // Specific surface area // cm2 g-1
      real      :: cumdltares(0:2)
      real      :: innsenes(0:2)   ! // OUTPUT // Index of nitrogen stress active on leaf death // P_innmin to 1
      real      :: senfac(0:2)   ! // OUTPUT // Water stress index on senescence // 0-1
      real      :: deltatauxcouv(0:2)
      real      :: mafeuiltombe(0:2)   ! // OUTPUT // Dry matter of fallen leaves // t.ha-1
      real      :: dltamstombe(0:2)
      real      :: mafeuil(0:2)   ! // OUTPUT // Dry matter of leaves // t.ha-1
      real      :: mafeuilverte(0:2)   ! // OUTPUT // Dry matter of green leaves // t.ha-1
      real      :: matigestruc(0:2)   ! // OUTPUT // Dry matter of stems (only structural parts) // t.ha-1
      real      :: mareserve(0:2)
      real      :: masecveg(0:2)   ! // OUTPUT // Vegetative dry matter // t.ha-1
      real      :: maenfruit(0:2)   ! // OUTPUT // Dry matter of harvested organ envelopes // t.ha-1
      real      :: pfeuiljaune(0:2)   ! // OUTPUT // Proportion of yellow leaves in total biomass // 0-1
      real      :: ptigestruc(0:2)   ! // OUTPUT // Proportion of structural stems in total biomass // 0-1
      real      :: penfruit(0:2)   ! // OUTPUT // Proportion of fruit envelopes in total biomass // 0-1
      real      :: preserve(0:2)   ! // OUTPUT // Proportion of reserve in the total biomass // 0-1
      real      :: deshyd(0:2, 0:50)
      real      :: eaufruit(0:2, 0:50)
      real      :: frplusp(0:2)
      real      :: sucre(0:2)   ! // OUTPUT // Sugar content of fresh harvested organs // % (of fresh weight)
      real      :: huile(0:2)   ! // OUTPUT // Oil content of fresh harvested organs // % (of fresh weight)
      real      :: sucrems(0:2)
      real      :: huilems(0:2)
      real      :: biorognecum(0:2)
      real      :: lairognecum(0:2)
      real      :: pgraingel(0:2)
      real      :: laieffcum(0:2)
      real      :: bioeffcum(0:2)
      real      :: rdtint(0:2, 0:20)
      real      :: nbfrint(0:2, 20)
      real      :: pdsfruittot(0:2)
      real      :: h2orec(0:2)   ! // OUTPUT // Water content of harvested organs // 0-1
      real      :: sucreder(0:2)
      real      :: huileder(0:2)
      real      :: teauint(0:2, 20)
      real      :: fapar(0:2)   ! // OUTPUT // Proportion of radiation intercepted // 0-1
      real      :: mafeuilp(0:2)
      real      :: matigestrucp(0:2)
      real      :: mabois(0:2)   ! // OUTPUT // Prunning dry weight // t.ha-1
      real      :: eai(0:2)
! DR 07/06/2017 j'ajoute la variable leai qui est la somme de la surface foliaire pour la photosuynthese des feuilles et des epis
      real      :: leai(0:2)
      real      :: spfruit(0:2)   ! // OUTPUT // Index of trophic stress applied to the number of fruits // 0 to 1
      real      :: dltaremobil(0:2)   ! // OUTPUT // Amount of perennial reserves remobilised // g.m-2.j-1
      real      :: mafrais(0:2)   ! // OUTPUT // Aboveground fresh matter // t.ha-1
      real      :: mafraisfeuille(0:2)
      real      :: mafraisrec(0:2)
      real      :: mafraisres(0:2)
      real      :: mafraistige(0:2)
      real      :: masecvegp(0:2)

      real      :: durvie(0:2, 0:731)     !  Actual life span of the leaf surface //  degree C
      real      :: abso(0:2, 0:731)       !  Daily N uptake rate by crop (without fixation)  // kg N ha-1 d-1
      real      :: pfeuil(0:2, 0:731)            ! pas besoin du tableau temporel    // OUTPUT // Proportion of leaves in total biomass // 0-1
      real      :: pfeuilverte(0:2, 0:731)       ! temporel pour senescence    // OUTPUT // Proportion of green leaves in total non-senescent biomass // 0-1

      real      :: CNgrain(0:2)  ! a la base, CNgrain n'est pas un tableau temporel, les calculs ne sont pas prevus pour, donc on supprime l'indice temporel    // OUTPUT // Nitrogen concentration of grains  // %
      ! (0:2,0:731)
      ! besoin de la valeur pour le jour nrec, on cree une variable de stockage pour cette valeur pour supprimer le tableau temporel ?
      real      :: CNplante(0:2) ! a la base, CNplante n'est pas un tableau temporel, les calculs ne sont pas prevus pour, donc on supprime l'indice temporel    // OUTPUT // Nitrogen concentration of entire plant  // %
      ! (0:2,0:731)
      ! besoin de la valeur pour le jour nrec, on cree une variable de stockage pour cette valeur pour supprimer le tableau temporel ?
      real      :: deltai(0:2, 0:731)            ! temporel pour senescence    // OUTPUT // Daily increase of the green leaf index // m2 leaf.m-2 soil
      real      :: demande(0:2)    ! // OUTPUT // Daily nitrogen requirement of the aerial organs          // kg.ha-1.d-1
      real      :: demandetot    ! Daily N requirement of the whole crop // kg.ha-1.d-1
      real      :: demandeper(0:2)
      real      :: demanderac(0:2)
      real      :: dltags(0:2)   ! // OUTPUT // Growth rate of the grains  // t ha-1.j-1
      real      :: dltamsen(0:2)   ! // OUTPUT // Senescence rate // t ha-1 j-1
      real      :: dltams(0:2, 0:731)            ! temporel pour senescence    // OUTPUT // Growth rate of the plant  // t ha-1.j-1
      real      :: dltamsres(0:2)
      real      :: dltaisenat(0:2)
      real      :: dltaisen(0:2)   ! // OUTPUT // Daily increase of the senescent leaf index // m2.m-2 sol.j-1
      real      :: eop(0:2)   ! // OUTPUT // Maximum transpiration flux  // mm
      real      :: ep(0:2)   ! // OUTPUT // Actual transpiration flux  // mm j-1
      real      :: epsib(0:2)   ! // OUTPUT // Radiation use efficiency  // t ha-1.Mj-1. m-2
      real      :: epz(0:2, nbCouchesSol)
      real      :: fpft(0:2)   ! // OUTPUT // Sink strength of fruits  // g.m-2 j-1
      real      :: fpv(0:2, 731)                 ! pas besoin du tableau temporel    // OUTPUT // Sink strength of developing leaves // g.j-1.m-2
      real      :: hauteur(0:2)   ! // OUTPUT // Height of canopy // m

      real      :: inn0(0:2)
      real      :: inn(0:2)   ! // OUTPUT // Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
      real      :: inns(0:2)   ! // OUTPUT // Index of nitrogen stress active on growth in biomass // P_innmin to 1
      real      :: interpluie(0:2)   ! // OUTPUT // Water intercepted by leaves // mm
      real      :: irazo(0:2, 0:731)             ! pas besoin du tableau temporel    // OUTPUT // Nitrogen harvest index  // gN grain gN plant-1
      real      :: ircarb(0:2, 0:731)            ! pas besoin du tableau temporel    // OUTPUT // Carbon harvest index // g  grain g plant-1
      real      :: lai(0:2, 0:731)   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
      real      :: laisen(0:2, 0:731)            ! pas besoin du tableau temporel(a verifier)    // OUTPUT // Leaf area index of senescent leaves // m2 leafs  m-2 soil
      real      :: magrain(0:2, 0:731)           ! pas besoin du tableau temporel(a verifier mais a 99% sur)
      real      :: masec(0:2, 0:731)             ! On doit pouvoir se passer du tableau temporel, simplement en sauvegardant la valeur n-1 (et encore...)    // OUTPUT // Aboveground dry matter  // t.ha-1
      real      :: masecneo(0:2)   ! // OUTPUT // Newly-formed dry matter  // t.ha-1
      real      :: masecneov(0:2)
      real      :: masecpartiel(0:2)
      real      :: mouill(0:2)
      real      :: msneojaune(0:2)   ! // OUTPUT // Newly-formed senescent dry matter  // t.ha-1
      real      :: msneojaunev(0:2)
      real      :: msres(0:2)
      real      :: msresv(0:2)
      real      :: msresjaune(0:2)   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
      real      :: msresjaunev(0:2)
      real      :: msjaune(0:2)   ! // OUTPUT // Senescent dry matter  // t.ha-1
      real      :: naer(0:2)
      real      :: nbgrains(0:2)
      real      :: nbgrainsv(0:2)
      real      :: nfruit(0:2, 50)   ! // OUTPUT // Number of fruits in box 5 // nb fruits
      real      :: nfruitv(0:2, 50), nfruitnou(0:2)
      real      :: pdsfruitfrais(0:2)   ! // OUTPUT // Total weight of fresh fruits // g m-2
      real      :: pgrain(0:2)
      real      :: pdsfruit(0:2, 50)   ! // OUTPUT // Weight of fruits in box 3 // g m-2
      real      :: pousfruit(0:2)   ! // OUTPUT // Number of fruits transferred from one box to the next  // nb fruits
      real      :: QNgrain(0:2)  ! a la base, QNgrain n'est pas un tableau temporel, les calculs ne sont pas prevus pour, donc on supprime l'indice temporel    // OUTPUT // Amount of nitrogen in harvested organs (grains / fruits) // kg ha-1
      ! (0:2,0:731)
      ! besoin de la valeur pour le jour nrec, on cree une variable de stockage pour cette valeur pour supprimer le tableau temporel ?
      ! besoin de la valeur pour le jour ntaille, on cree une variable de stockage pour cette valeur pour supprimer le tableau temporel ?
      real      :: QNplantenp(0:2, 0:731)          ! On doit pouvoir se passer du tableau temporel, simplement en sauvegardant la valeur n-1 (et encore...)    // OUTPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1
      real      :: QCplante     ! // OUTPUT // Amount of C in the aerial part of the plant  // kg.ha-1
      real      :: raint(0:2)   ! // OUTPUT // Photosynthetic active radiation intercepted by the canopy  // Mj.m-2
      real      :: remobilj(0:2)   ! // OUTPUT // Amount of biomass remobilized on a daily basis for the fruits  // g.m-2 j-1
      real      :: sourcepuits(0:2)   ! // OUTPUT // Pool/sink ratio // 0-1
      real      :: splai(0:2)   ! // OUTPUT // Pool/sink ratio applied to leaves // 0-1
      real      :: swfac(0:2)   ! // OUTPUT // Index of stomatic water stress  // 0-1
      real      :: teaugrain(0:2)
      real      :: turfac(0:2)   ! // OUTPUT // Index of turgescence water stress  // 0-1
      real      :: vitmoy(0:2)   ! // OUTPUT // mean growth rate of the canopy (dans le livre c'est en g mais ca colle pas)  // g.m-2.d-1
      real      :: offrenod(0:2)   ! // OUTPUT // Amount of fixed nitrogen by nodules // kg.ha-1.j-1
      real      :: fixreel(0:2)   ! // OUTPUT // Actual rate of symbiotic uptake // kg ha-1 j-1
      real      :: fixpot(0:2)   ! // OUTPUT // Potential rate of symbiotic uptake // kg ha-1 j-1
      real      :: fixmaxvar(0:2)
      real      :: Qfix(0:2)   ! // OUTPUT // Quantity of nitrogen fixed by nodosities // kg.ha-1
! Ajout Lolo Juin 2016
      real      :: Qfixtot(0:2) ! // OUTPUT // Total quantity of nitrogen fixed by nodosities during a crop cycle // kg.ha-1
      real      :: Ndfa        ! // OUTPUT // Proportion of total plant N issued from N fixation // unitless
      real      :: deltahauteur(0:2)

! DR 16/12/2005 je rajoute un stress sur la photoperiode pour la vigne
      real      :: strphot(0:2)
      real      :: strphotveille(0:2)
      real      :: photnet(0:2, 731)             ! pas besoin du tableau temporel    // OUTPUT // net photosynthesis // t ha-1.j-1
      real      :: masecdil(0:2)
      real      :: laimax(0:2) ! // OUTPUT // maximal LAI // SD

! Ajouts Loic et Bruno fevrier 2014
! Lolo Juin 2018: ajout de la dimension AO AS aux organes perennes pour pouvoir faire des CAS avec des cultures perennes
      real      :: maperenne(0:2)
      real      :: QNperenne(0:2)
      real      :: QNplante
      real      :: resperenne(0:2)
      real      :: QNresperenne(0:2)
      real      :: resperennestruc
      real      :: QNresperennestruc
      real      :: maperennemort(0:2)
      real      :: QCperennemort(0:2)
      real      :: QNperennemort(0:2)
      real      :: dltaperennesen(0:2)
      real      :: dltaQNperennesen(0:2)
      real      :: QNrestemp
      real      :: QNvegstruc
      real      :: QNveg
      real      :: QCO2resperenne(0:2)
      real      :: mstot
      real      :: QNtot
      real      :: QNtot0
      real      :: masecnp(0:2, 0:731)
      real      :: mafauche
      real      :: mafauchetot
      real      :: QNfauche
      real      :: QNfauchetot
      real      :: dltamsrac
      real      :: QNabso(0:2)           ! Cumulative N uptake by plant without N fixation // kg N ha-1
      real      :: QNabsoaer(0:2)   ! Cumulative N uptake by aerial organs including N fixation // kg N ha-1
      real      :: QNabsorac(0:2)        ! Cumulative N uptake by roots including N fixation // kg N ha-1
      real      :: QNabsoper(0:2)          ! Cumulative N uptake by perennial organs including N fixation  // kg N ha-1
      real      :: QNabsotot(0:2)        ! Cumulative N uptake by plant including N fixation  // kg N ha-1
      real      :: absotot(0:2)
      real      :: absoaer(0:2)
      real      :: absorac(0:2)
      real      :: absoper(0:2)
      real      :: dltaremobilN(0:2) ! daily remobilised N from perennial organs (kg N ha-1)
      real      :: pNvegstruc
      real      :: pNrestemp
      real      :: QNfeuille
      real      :: QNtige
      real      :: QNfeuille0
      real      :: QNtige0
      ! Ajout Loic juin 2021
      real      :: QNrac0
      real      :: msrac0
      real      :: QNrestemp0

! PL - 11/7/2019
! Converting to scalar variables
      !real      :: H2Orec_percent(0) ! // OUTPUT // Water content of harvested organs in percentage// %
      !real      :: sucre_percent(0)   ! // OUTPUT // Sugar content of fresh harvested organs // % (of fresh weight)
      !real      :: huile_percent(0)   ! // OUTPUT // Oil content of fresh harvested organs // % (of fresh weight)
      real      :: H2Orec_percent ! // OUTPUT // Water content of harvested organs in percentage// %
      real      :: sucre_percent   ! // OUTPUT // Sugar content of fresh harvested organs // % (of fresh weight)
      real      :: huile_percent   ! // OUTPUT // Oil content of fresh harvested organs // % (of fresh weight)

      real      :: et0 ! // OUTPUT // eos+eop // mm

! DR 22/04/2016 la variable uniquement calculee dans bilan est a sortir pour le rapport
      real     :: qexport  ! Biomass exported t ha-1
      real     :: QNexport ! // OUTPUT // Amount of nitrogen exported at harvest (harvested and removed parts)// kgN.ha-1

      integer  :: day_cut  ! // OUTPUT // Cut day // day
! DR 23/09/2016 on a ajoute 3 variables pour efese representant les valeurs avant coupe
      real     :: lai_mx_av_cut ! // OUTPUT // LAI before cut (for cut crops , for others = lai(n) )
      real     :: masec_mx_av_cut ! // OUTPUT // Aboveground dry matter before cut(for cut crops , for others = masec(n) )  // t.ha-1
      real     :: QNplante_mx_av_cut ! // OUTPUT // Amount of nitrogen taken up by the plant before cut(for cut crops , for others = Qnplante(n) )// kgN.ha-1
      ! DR 24/05/2018 on ajoute le poids d'un grain en mg
      real :: grain_dry_weight_mg ! // OUTPUT // Grain unit dry weight
      real      :: dracf
      real      :: dracg
      real      :: longsperacf
      real      :: longsperacg
      real      :: debsenracf
      real      :: debsenracg
      real      :: mafeuil0
      real      :: mafeuilverte0
      real      :: matigestruc0
      real      :: cumdltaremobsen
      real      :: mafeuiltombefauche
! enregistrement du stade de developpement des nodules et de la somme de degres C pour les cultures perennes fixatrices
      real      :: somcourno0
      real      :: ndno0
      real      :: nfno0

      ! PL, 7/10/2020
      real      :: urac   ! // OUTPUT // daily relative development unit for root growth  // 1-3

!! merge trunk 23/11/2020
      real      :: tsol_mean_plt_ger_0_dpthsow
      real      :: tsol_sum_plt_ger_0_dpthsow
      real      :: tsol_min_plt_ger_0_dpthsow

      real      :: tsol_mean_0_profsem
      real      :: tsol_min_0_profsem

      real      :: tsol_mean_ger_lev_0_dpthsow
      real      :: tsol_sum_ger_lev_0_dpthsow
      real      :: tsol_min_ger_lev_0_dpthsow

      integer   :: nb_days_frost_amf_120
      real      :: mean_swfac_flo_p_m_150
      real      :: som_temp_seuil

      real      :: tab_swfac(0:735)
      real      :: sum_udev

      integer   :: flo_moins_150
      integer   :: flo_plus_150

      integer   :: iflos_minus_150
      integer   :: iflos_plus_150
! Loic fevrier 2021 : variable utilis�e dans dynamique talle
      real      :: LAIapex

      character(:), allocatable :: report_filename
      integer :: report_unit

      character(:), allocatable :: daily_output_filename
      integer :: daily_unit

      character(:), allocatable :: bilan_output_filename
      integer :: bilan_unit

      character(:), allocatable :: agmip_report_filename
      integer :: agmip_report_unit

      character(:), allocatable :: agmip_daily_output_filename
      integer :: agmip_daily_unit

      logical :: header_report_exist

      integer :: nbcoupe_reel
      real :: inni(0:2)       ! AO/AS  
      !> date of sowing
      integer :: iplt
            
      character(:), allocatable :: P_fplt !   // PARAMETER // name of the plant file // SD // P_USM/USMXML // 0
      character(:), allocatable :: P_ftec !   // PARAMETER // name of the technique file // SD // P_USM/USMXML // 0
      character(:), allocatable :: P_flai !   // PARAMETER // name of the file LAI // SD // P_USM/USMXML // 0

      ! * on garde les unites froids pour l'enchainement des perennes dans recup.tmp
      real :: cu0
      real :: somelong0
      integer :: nfindorm0

      integer :: nmontaison
      real :: somtemphumec

      !> year // variable type // date // soil depth (cm)
      real, allocatable :: tabprof(:,:,:,:)
   end type Plante_
contains

    type(Plante_) pure function plant_ctor() result(plant)
        plant%report_filename = ''
        plant%agmip_report_filename = ''

        plant%agmip_daily_output_filename = ''
        plant%daily_output_filename = ''
        plant%bilan_output_filename = ''
        plant%header_report_exist = .false.
        plant%nbcoupe_reel = 0
        plant%inni(:) = 0.       ! AO/AS
        plant%iplt = 0
        plant%cu0 = 0.0
        plant%somelong0 = 0.0
        plant%nfindorm0 = 0
        plant%nmontaison = 0
        plant%somtemphumec = 0.0
    end function

   subroutine Plante_Lecture_fichier(logger, path, plant)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      type(Plante_), intent(inout) :: plant

      logical :: exists
      integer :: unit, iostat

      exists = path_exist(path)
      if (.not. exists) then
         call exit_error(logger, "Plant file doesn't exist: [" // path // "]")
      end if

      open (newunit=unit, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening plant file: [" // path // "]")
      end if

      call Plante_Lecture_V6(logger, plant, unit)
      close(unit)
   end subroutine Plante_Lecture_fichier

!========================================================================================!
!========================================================================================!
!========================================================================================!

   subroutine Plante_Lecture_V6(logger, p, fp)
      type(logger_), intent(in) :: logger
      type(Plante_), intent(inout) :: p
      integer, intent(in) :: fp

      character(len=30) :: nomVar
      integer  :: j

!plant name and clade
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, fmt='(A3)', err=250) p%P_codeplante
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codemonocot
!effect of atmospheric CO2 concentration
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_alphaco2
!phasic development
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tdmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tdmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codetemp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codegdh
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coeflevamf
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coefamflax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coeflaxsen
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coefsenlan
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coeflevdrp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coefdrpmat
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coefflodrp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codephot
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codephot_part
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coderetflo
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stressdev
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codebfroid
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_jvcmini
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_julvernal
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tfroid
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_ampfroid
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tdmindeb
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tdmaxdeb
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codedormance
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_ifindorm
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_q10
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_idebdorm
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codegdhdeb
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_code_WangEngel
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tdoptdeb
!emergence and starting
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tgmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codeperenne
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codegermin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stpltger
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_potgermi
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_nbjgerlim
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_propjgermin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codehypo
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_belong
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_celong
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_elmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_nlevlim1
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_nlevlim2
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_vigueurbat
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_laiplantule
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_nbfeuilplant
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_masecplantule
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_zracplantule
!leaves
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_phyllotherme
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_laicomp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tcmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tcmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tcxstop
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codelaitr
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_vlaimax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_pentlaimax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_udlaimax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_ratiodurvieI
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_ratiosen
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_abscission
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_parazofmorte
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_innturgmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_dlaimin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codlainet
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tustressmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_durviesupmax
! DR 23/10/07
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codestrphot
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_phobasesen
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_dltamsmaxsen
! 30/01/2012 issu de param_gen
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_dltamsminsen
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_alphaphot
!
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tauxrecouvmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tauxrecouvkmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_pentrecouv
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_infrecouv
!radiation interception
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codetransrad
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_forme
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_rapforme
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_adfol
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_dfolbas
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_dfolhaut
!shoot biomass growth
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_temax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_teoptbis
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_efcroijuv
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_efcroiveg
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_efcroirepro
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_remobres
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coefmshaut
!partitioning of biomass in organs
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_slamin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_envfruit
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_sea
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_code_acti_reserve
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_PropresP
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_PropresPN
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Efremobil
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Propres
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tauxmortresP
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Parazoper
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_ParazoTmorte
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_inilai
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_resplmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codemontaison
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codedyntalle
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codetranspitalle
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_SurfApex
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_SeuilMorTalle
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_SigmaDisTalle
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_VitReconsPeupl
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_SeuilReconsPeupl
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_MaxTalle
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_SeuilLAIapex
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tigefeuilcoupe
!yield formation
!*********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_irazomax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codeindetermin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_cgrain
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_cgrainv0
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codeir
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_irmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_nboite
       ! This parameter is only used for indeterminate growing plant
      ! For determinate ones the value is set to -999 as for all
      ! useless parameters
      if(p%P_nboite < 1 ) p%P_nboite = 1
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_allocfrmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_afpf
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_bfpf
! DR 23/10/07
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_cfpf
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_dfpf
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_spfrmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_spfrmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_splaimin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_splaimax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codcalinflo
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codetremp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tminremp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tmaxremp
! roots
!************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_sensanox
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stoprac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_sensrsec
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_contrdamax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_rayon
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codetemprac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codemortalracine
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coefracoupe
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_coderacine
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_zlabour
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_zpente
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_zprlim
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_draclong
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_debsenrac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_lvfront
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_longsperac
! Bruno aout 2018 ajout
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codedisrac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_kdisrac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_alloperirac
! fin ajout
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codazorac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_minefnra
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_minazorac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_maxazorac
! DR 23/10/07
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codtrophrac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_repracpermax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_repracpermin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_krepracperm
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_repracseumax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_repracseumin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_krepracseu
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_code_stress_root
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_code_rootdeposition
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_parazorac
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_code_diff_root
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_lvmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_rapdia
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_RTD
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_propracfmax
!
! frost
!**********************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tletale
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tdebgel
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codgellev
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_nbfgellev
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tgellev90
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codgeljuv
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tgeljuv90
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codgelveg
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tgelveg90
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codgelflo
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tgelflo10
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tgelflo90
!water
!***************************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_h2ofeuilverte
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_h2ofeuiljaune
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_h2otigestruc
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_h2oreserve
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_h2ofrvert
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tempdeshyd
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codebeso
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_kmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_rsmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codeintercept
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_mouillabil
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stemflowmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_kstemflow
! nitrogen
!****************************
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Vmax1
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Kmabs1
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Vmax2
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Kmabs2
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_adil
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_bdil
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_masecNmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_INNmin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_INNimin
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_inngrain1
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_inngrain2
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_bdilmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codeplisoleN
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_adilmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Nmeta
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_masecmeta
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_Nreserve
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codeINN
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codelegume
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stlevdno
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stdnofno
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stfnofvino
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_vitno
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_profnod
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_concNnodseuil
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_concNrac0
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_concNrac100
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tempnod1
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tempnod2
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tempnod3
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_tempnod4
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codefixpot
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_fixmax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_fixmaxveg
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_fixmaxgr
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_codazofruit
!correspondance code BBCH
!*************************************
! DR et ML 27/10/09 introduction des stades bbch
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchplt
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchger
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchlev
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchamf
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchlax
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchsen
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchflo
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchdrp
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchnou
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchdebdes
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchmat
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchrec
      read (fp, *, err=250, end=80) nomVar
      read (fp, *, err=250, end=90) p%P_stadebbchfindorm

      do j = 1, nb_variete_max
! Phenological stages and development ***********************
         read (fp, *, err=250, end=70) nomVar
         read (fp, *, err=250, end=90) p%P_codevar(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stlevamf(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stamflax(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stlevdrp(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stflodrp(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stdrpdes(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_jvc(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stdordebour(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_sensiphot(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_phobase(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_phosat(j)
! Leaves **************************************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_adens(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_bdens(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_hautbase(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_hautmax(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_khaut(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_durvieF(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stlaxsen(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stsenlan(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_dlaimax(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_dlaimaxbrut(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_innsen(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_rapsenturg(j)
! Radiations interception ********************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_extin(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_ktrou(j)
! Shoot biomass growth ***********************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_temin(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_teopt(j)
! Biomass partitioning ***********************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_slamax(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_tigefeuil(j)
! Yield formation ****************************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_pgrainmaxi(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_vitpropsucre(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_vitprophuile(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_vitirazo(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_deshydbase(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_nbjgrain(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_nbgrmin(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_nbgrmax(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stdrpmat(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_vitircarb(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_vitircarbT(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_afruitpot(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_dureefruit(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_stdrpnou(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_nbinflo(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_inflomax(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_pentinflores(j)
! Roots **************************************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_croirac(j)
! Frost **************************************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_tgellev10(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_tgeljuv10(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_tgelveg10(j)
! Water Stress *******************************************
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_psisto(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_psiturg(j)
         read (fp, *, err=250, end=80) nomVar
         read (fp, *, err=250, end=90) p%P_swfacmin(j)
      end do

      p%nbVariete = j

      ! si on arrive ici, il n'y a pas eu d'erreur, on quitte la routine.
      return

70    p%nbVariete = j - 1
      return

80    call exit_error(logger, 'End of station file :  missing lines ')

90    call exit_error(logger, 'End of station file :  missing value for ' // nomVar)

250   call exit_error(logger, 'Error reading plant file ! derniere variable avant erreur: ' // nomVar)

   end subroutine

    subroutine init_file_output(plant, plant_index, plant_count, usm_name)
        type(Plante_), intent(inout) :: plant
        integer, intent(in) :: plant_index
        integer, intent(in) :: plant_count
        character(*), intent(in) :: usm_name

        character(:), allocatable :: daily_prefix, report_filename, agmip_report_filename, bilan_prefix

        if(plant_count==1) then
            report_filename = 'mod_rapport.sti'
            agmip_report_filename = 'mod_rapport_AgMIP.sti'
            daily_prefix = 'mod_s'
            bilan_prefix = 'mod_b'
        else
            if (plant_index == 1) then
                report_filename  = 'mod_rapportP.sti'
                agmip_report_filename = 'mod_rapportP_AgMIP.sti'
                daily_prefix = 'mod_sp'
                bilan_prefix = 'mod_bp'
            else 
                report_filename  = 'mod_rapportA.sti'
                agmip_report_filename = 'mod_rapportA_AgMIP.sti'
                daily_prefix = 'mod_sa'
                bilan_prefix = 'mod_ba'
            end if
        end if
        plant%report_filename = report_filename
        plant%agmip_report_filename = agmip_report_filename

        plant%agmip_daily_output_filename = daily_prefix//usm_name//'.st3'
        plant%daily_output_filename = daily_prefix//usm_name//'.sti'
        plant%bilan_output_filename = bilan_prefix//usm_name//'.sti'

    end subroutine

    subroutine close_plants_files(plants)
      type(Plante_), intent(in) :: plants(:)
      integer :: plant_index
      do plant_index = 1, size(plants)
          call close_plant_files(plants(plant_index))
      end do
    end subroutine

    subroutine close_plant_files(plant)
      type(Plante_), intent(in) :: plant
      close(plant%report_unit)
      close(plant%daily_unit)
      close(plant%bilan_unit)
      close(plant%agmip_report_unit)
      close(plant%agmip_daily_unit)
    end subroutine
end module Plante
