module plant_checks_m
    use messages
    use messages_data
    use Tests
    use Plante

    implicit none

    contains

    !> Checks plants parameters value for coherence
    subroutine Plante_Tests(logger, p, P_variete)
        type(logger_), intent(in) :: logger
        type(Plante_), intent(inout) :: p
        integer, intent(in) :: P_variete  ! // PARAMETER // variety number in the technical file // SD // PARTEC // 1

        ! DR 05/01/2022 on met un test sur la coherence entre le parametre P_code_acti_reserve_ini
        ! du fichier ini et celui du fichier plante qu'on vient de lire

        if (p%P_code_acti_reserve_ini .ne. p%P_code_acti_reserve) then
            call exit_error(logger, MESSAGE_447)
        end if

        if (p%nbVariete < P_variete) then
            call EnvoyerMsg(logger, MESSAGE_439)
            call EnvoyerMsg(logger, 'P_variete = ' // to_string(P_variete))
            call EnvoyerMsg(logger, 'p%nbVariete = ' // to_string(p%nbVariete))
            call exit_error(logger, MESSAGE_440)
        end if

        ! Ajout Florent C. et Fabien octobre 2019
        if (p%P_coderacine == 1 .and. p%P_code_rootdeposition == 1) then
            call exit_error(logger, MESSAGE_52)
        end if

! * = == = == = == = == = = = > DR 04/01/2018 tests sur les valeurs max < min  < = == = == = == = == = == = * c
        call raise_error_if_min_gt_max(logger, p%P_tdmin, p%P_tdmax, OPTION_1118)
        call raise_error_if_min_gt_max(logger, p%P_tdmindeb, p%P_tdmaxdeb, OPTION_1119)
        call raise_error_if_min_gt_max(logger, p%P_tcmin, p%P_tcmax, OPTION_1120)
        call raise_error_if_min_gt_max(logger, p%P_temin(P_variete), p%P_temax, OPTION_1121)  ! voir ce qu'on fait de teopt et teoptbis
        call raise_error_if_min_gt_max(logger, real(p%P_nlevlim1), real(p%P_nlevlim2), OPTION_1122)
        call raise_error_if_min_gt_max(logger, p%P_slamin, p%P_slamax(P_variete), OPTION_112)
        call raise_error_if_min_gt_max(logger, p%P_hautbase(P_variete), p%P_hautmax(P_variete), OPTION_116)

! DR 09/01/2018 **** 40 ans d'Edwige , bon anniversaire !!!!! *******
! * = == = == = == = == = = = > DR 04/01/2018 tests sur les valeurs activees avce -999 < = == = == = == = == = == = * c
! DR 03/01/2018 je mets en place un test systematique sur les parametres a -999 suivant l'activation des options

        if(p%P_codetemp == 2) then
            call raise_error_if_999(logger, 'codetemp', OPTION_2_MISSING,&
                p%P_coeflevamf, p%P_coefamflax, p%P_coeflaxsen, p%P_coefsenlan, &
                p%P_coeflevdrp, p%P_coefdrpmat, p%P_coefflodrp)
        end if

        if(p%P_codephot == 1) then
            call raise_error_if_999(logger, 'codephot', OPTION_1_MISSING, &
                p%P_phobase(P_variete), p%P_phosat(P_variete), p%P_sensiphot(P_variete))
        end if

        if(p%P_coderetflo == 1) then
            call raise_error_if_999(logger, 'coderetflo',OPTION_1_MISSING, p%P_stressdev)
        end if

        call check_P_codebfroid(logger, p, P_variete)

        if(p%P_codegermin == 1) then
            call raise_error_if_999(logger, 'codegermin', OPTION_1_MISSING, &
                    p%P_tgmin, p%P_stpltger, p%P_potgermi, real(p%P_nbjgerlim), p%P_propjgermin)
        end if

        if (p%P_codeperenne .eq. 1) then
            select case (p%P_codehypo)
            case (1)
                call raise_error_if_999(logger, 'codehypo', OPTION_1_MISSING, &
                    p%P_belong, p%P_celong, p%P_elmax, real(p%P_nlevlim1), real(p%P_nlevlim2), p%P_vigueurbat)
            case (2)
                call raise_error_if_999(logger, 'codehypo', OPTION_2_MISSING, &
                    p%P_laiplantule, real(p%P_nbfeuilplant), p%P_masecplantule, p%P_zracplantule)
            end select
        end if

        call check_P_codelaitr(logger, p, P_variete)

        select case (p%P_codetransrad)
        case (1)
            call raise_error_if_999(logger, 'codetransrad', OPTION_1_MISSING, p%P_extin(P_variete))
        case (2)
            call raise_error_if_999(logger, 'codetransrad', OPTION_2_MISSING,&
                real(p%P_forme), p%P_rapforme, p%P_adfol, p%P_dfolbas, p%P_dfolhaut)
        end select

        call check_P_codeindetermin(logger, p, P_variete)

        if(p%P_codetremp == 1) then
            call raise_error_if_999(logger, 'codetremp', OPTION_1_MISSING, p%P_tminremp, p%P_tmaxremp)
        end if

        call raise_error_if_min_gt_max(logger, p%P_tminremp, p%P_tmaxremp, OPTION_1124)


        call check_P_coderacine(logger, p, P_variete)

        if (p%P_codgellev == 2) then
            call raise_error_if_999(logger, 'codgellev', OPTION_2_MISSING,&
                    real(p%P_nbfgellev), p%P_tgellev90, p%P_tgellev10(P_variete))
            call raise_error_if_min_gt_max(logger, p%P_tgellev90, p%P_tgellev10(P_variete), OPTION_1128)
        end if

        if (p%P_codgeljuv == 2) then
            call raise_error_if_999(logger, 'codgeljuv', OPTION_2_MISSING, p%P_tgeljuv90, p%P_tgeljuv10(P_variete))
            call raise_error_if_min_gt_max(logger, p%P_tgeljuv90, p%P_tgeljuv10(P_variete), OPTION_1129)
        end if

        if (p%P_codgelveg == 2) then
            call raise_error_if_999(logger, 'codgelveg', OPTION_2_MISSING, p%P_tgelveg90, p%P_tgelveg10(P_variete))
            call raise_error_if_min_gt_max(logger, p%P_tgelveg90, p%P_tgelveg10(P_variete), OPTION_1130)
        end if

        if (p%P_codgelflo == 2) then
            call raise_error_if_999(logger, 'codgelflo', OPTION_2_MISSING, p%P_tgelflo10, p%P_tgelflo90)
            call raise_error_if_min_gt_max(logger, p%P_tgelflo90, p%P_tgelflo10, OPTION_1131)
        end if

        select case (p%P_codebeso)
        case (1)
            call raise_error_if_999(logger, 'codebeso', OPTION_1_MISSING, p%P_kmax)
        case (2)
            call raise_error_if_999(logger, 'codebeso', OPTION_2_MISSING, p%P_rsmin)
        end select

        if(p%P_codeintercept == 1) then
            call raise_error_if_999(logger, 'codeintercept', OPTION_1_MISSING, p%P_mouillabil, p%P_stemflowmax, p%P_kstemflow)
        end if

        select case (p%P_codeplisoleN)
        case (1)
            call raise_error_if_999(logger, 'codeplisoleN', OPTION_1_MISSING, p%P_adilmax, p%P_bdilmax)
        case (2)
            call raise_error_if_999(logger, 'codeplisoleN', OPTION_2_MISSING, p%P_Nmeta, p%P_masecmeta, p%P_Nreserve)
        end select

        if(p%P_codelegume == 2) then
            call raise_error_if_999(logger, 'codelegume', OPTION_2_MISSING, &
                p%P_stlevdno, p%P_stdnofno, p%P_stfnofvino, p%P_vitno, &
                p%P_profnod, p%P_concNnodseuil, p%P_concNrac0, p%P_concNrac100, &
                p%P_tempnod1, p%P_tempnod2, &
                p%P_tempnod3, p%P_tempnod4)
            select case (p%P_codefixpot)
            case (1)
                call raise_error_if_999(logger, 'codefixpot', OPTION_1_MISSING, p%P_fixmax)
            case (2)
                call raise_error_if_999(logger, 'codefixpot', OPTION_2_MISSING, p%P_fixmaxveg, p%P_fixmaxgr)
            end select
        end if

    end subroutine
    
    subroutine check_P_codebfroid(logger, p, P_variete)
        type(logger_), intent(in) :: logger
        type(Plante_), intent(in) :: p
        integer, intent(in) :: P_variete

        select case (p%P_codebfroid)
        case (2)
            call raise_error_if_999(logger, 'codebfroid', OPTION_2_MISSING, &
              real(p%P_jvcmini), real(p%P_julvernal), p%P_tfroid, p%P_ampfroid, real(p%P_jvc(P_variete)))
        case (3)
            call raise_error_if_999(logger, 'codebfroid', OPTION_3_MISSING,&
                real(p%P_forme), p%P_stdordebour(P_variete), p%P_tdmindeb, p%P_tdmaxdeb)
  
            select case (p%P_codedormance)
            case (1)
                call raise_error_if_999(logger, 'codedormance', OPTION_1_MISSING, real(p%P_ifindorm))
            case (3)
                call raise_error_if_999(logger, 'codedormance', OPTION_3_MISSING, p%P_q10, real(p%P_idebdorm))
            end select
        end select
    end subroutine

    subroutine check_P_codelaitr(logger, p, P_variete)
        type(logger_), intent(in) :: logger
        type(Plante_), intent(in) :: p
        integer, intent(in) :: P_variete

        select case (p%P_codelaitr)
        case (1)
            call raise_error_if_999(logger, 'codelaitr', OPTION_1_MISSING, &
              p%P_vlaimax, p%P_pentlaimax, p%P_udlaimax, p%P_ratiodurvieI, &
              p%P_tcmin, p%P_tcmax, p%P_ratiosen, p%P_abscission, &
              p%P_parazofmorte, p%P_innturgmin, p%P_dlaimin)
              ! level 2
              select case (p%P_codlainet)
              case (1)
                  call raise_error_if_999(logger, 'codlainet', OPTION_1_MISSING, p%P_tustressmin,&
                      p%P_stlaxsen(P_variete), p%P_stsenlan(P_variete), p%P_dlaimax(P_variete))
              case (2)
                  call raise_error_if_999(logger, 'codlainet', OPTION_2_MISSING, p%P_durviesupmax,&
                      p%P_dlaimaxbrut(P_variete), p%P_innsen(P_variete), p%P_rapsenturg(P_variete))
              end select
  
              if (p%P_codestrphot .eq. 1) then
                  call raise_error_if_999(logger, 'codestrphot', OPTION_1_MISSING,&
                      p%P_phobasesen, p%P_dltamsmaxsen, p%P_dltamsminsen, p%P_alphaphot)
                  ! Les parametres n'ont pas la meme signification, pas a comparer:
                  ! P_dltamsminsen a renommer en ????strphot
                  ! call test_parameters(logger, p%P_dltamsminsen,p%P_dltamsmaxsen,OPTION_1123)
              end if
        case (2)
            call raise_error_if_999(logger, 'codelaitr', OPTION_2_MISSING, &
              p%P_tauxrecouvmax, p%P_tauxrecouvkmax, p%P_pentrecouv, p%P_infrecouv)
        end select
    end subroutine

    
    subroutine check_P_codeindetermin(logger, p, P_variete)
        type(logger_), intent(in) :: logger
        type(Plante_), intent(in) :: p
        integer, intent(in) :: P_variete

        select case (p%P_codeindetermin)
        case (1)
            call raise_error_if_999(logger, 'codeindetermin', OPTION_1_MISSING, p%P_cgrain, p%P_cgrainv0,&
                      real(p%P_nbjgrain(P_variete)), p%P_nbgrmin(P_variete), &
                      p%P_nbgrmax(P_variete), p%P_stdrpmat(P_variete))
  
              select case(p%P_codeir)
              case (1)
                  call raise_error_if_999(logger, 'codeir', OPTION_1_MISSING, p%P_irmax, p%P_vitircarb(P_variete))
              case (2)
                  call raise_error_if_999(logger, 'codeir', OPTION_2_MISSING, p%P_vitircarbT(P_variete))
              end select
        case (2)
            call raise_error_if_999(logger, 'codeindetermin', OPTION_2_MISSING,&
                real(p%P_nboite), p%P_allocfrmax, p%P_afpf, p%P_bfpf, &
                p%P_cfpf, p%P_dfpf, p%P_spfrmin, &
                p%P_spfrmax, p%P_splaimin, p%P_splaimax, &
                p%P_afruitpot(P_variete), p%P_dureefruit(P_variete), p%P_stdrpnou(P_variete))
              select case(p%P_codcalinflo)
              case (1)
                  call raise_error_if_999(logger, 'codcalinflo', OPTION_1_MISSING, p%P_nbinflo(P_variete))
              case (2)
                  call raise_error_if_999(logger, 'codcalinflo', OPTION_2_MISSING,&
                            p%P_inflomax(P_variete), p%P_pentinflores(P_variete))
              end select
      end select
    end subroutine


    subroutine check_P_coderacine(logger, p, P_variete)
        type(logger_), intent(in) :: logger
        type(Plante_), intent(in) :: p
        integer, intent(in) :: P_variete
    
        select case(p%P_coderacine)
        case (1)
            call raise_error_if_999(logger, 'coderacine', OPTION_1_MISSING, p%P_zlabour, p%P_zpente, p%P_zprlim)
        case (2)
            call raise_error_if_999(logger, 'coderacine', OPTION_2_MISSING,&
                p%P_draclong, p%P_debsenrac, p%P_lvfront, p%P_longsperac)
            
            if(p%P_codazorac == 1) then
                call raise_error_if_999(logger, 'codazorac', OPTION_1_MISSING, p%P_minefnra, p%P_minazorac, p%P_maxazorac)
                call raise_error_if_min_gt_max(logger, p%P_minazorac, p%P_maxazorac, OPTION_1125)
            end if

            select case(p%P_codtrophrac)
            case (1)
                call raise_error_if_999(logger, 'codtrophrac', OPTION_1_MISSING,&
                        p%P_repracpermax, p%P_repracpermin, p%P_krepracperm)
            case (2)
                call raise_error_if_999(logger, 'codtrophrac', OPTION_2_MISSING,&
                        p%P_repracseumax, p%P_repracseumin, p%P_krepracseu)
                call raise_error_if_min_gt_max(logger, p%P_repracseumin, p%P_repracseumax, OPTION_1127)
            end select
        end select
    end subroutine
end module
 