!*********************************************************************
! reading of continous values of leaf area index
! - in the case of forcing lai ( file *.lai )
!*********************************************************************
module Lecture_Lai_m
   use stics_system
   use messages
   use Stics
   use Plante
   use Itineraire_Technique
   implicit none
   private
   public :: Lecture_Lai
contains
   subroutine Lecture_Lai(logger, sc, p, itk)
      type(logger_), intent(in) :: logger
      
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(inout) :: p
      type(ITK_), intent(in) :: itk

      logical :: consec
      integer :: j, nblai, date(3), i, iderlai, ipremlai, premier, path_length, day
      character(len=3) :: sufflai

      real :: tlai(731), sbvmin
      integer :: nl(731)

      logical :: exists
      integer :: flai, iostat
      character(len=4) :: nlai
      character(:), allocatable :: path

      path = p%P_flai
      exists = path_exist(path)
      if (.not. exists) then
         call exit_error(logger, "Lai file doesn't exist: [" // path // "]")
      end if

      open (newunit=flai, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening Lai file: [" // path // "]")
      end if

      p%lai(:, :) = 0.
      nblai = 0

! on extrait l'extension du fichier pour determiner si c'est
! un fichier .obs ou .lai
! ici on extrait les 3 dernieres lettres du nom du fichier lai
      path_length = len(path)
      sufflai = path(path_length - 2:path_length)

      if (sufflai == 'obs' .or. sufflai == 'OBS') then

         ! les lai observees   fichier (*.obs)
         premier = 0
         do i = 1, 731
            read (flai, 100, end=30) (date(j), j=1, 3), nl(i), tlai(i)
100         format(i4, 2i3, i4, f8.2)
            if (tlai(i) > 0.0 .and. premier == 0) then
               ipremlai = i
               premier = 1
            end if
            if (tlai(i) > 0.0 .and. premier == 1) iderlai = i

         end do
30       nblai = i - 1

! lecture des LAI
! *****************

         ! test sur les valeurs manquantes
         consec = .true.
         do i = ipremlai, iderlai
            ! if (tlai(i) == 0) then
            if (abs(tlai(i)) .lt. 1.0E-8) then
               consec = .false.
            end if
         end do

         ! le fichier lai a des donnees manquantes
         if (.not. consec) then
            call exit_error(logger, MESSAGE_101)
         end if

         ! changement de calendrier
         do i = sc%P_iwater, nblai
            day = nl(i) - sc%P_iwater + 1
            p%lai(:, day) = tlai(i)
!        write(*,*)n,lai(ipl,ens,n)
         end do

      else

         ! fichier des lai estime par ajust (*.lai)
         ! changement de format des LAI ajustes + 1ligne
         do i = 1, 8
            read (flai, *)
         end do

         do i = 1, 731
            read (flai, *, end=65) j, nl(j), tlai(j)
         end do

65       nblai = i - 1

         ! changement de calendrier
         do i = 1, nblai
            day = nl(i) - sc%P_iwater + 1
            if (day < 0) CYCLE
            p%lai(:, day) = tlai(i)

            ! calcul de l'IF senescent
            if (p%lai(AS, day) < p%lai(AS, day - 1)) then
               p%laisen(:, day) = p%lai(AS, day - 1) - p%lai(AS, day)
            else
               p%laisen(:, day) = 0.0
            end if

            if (p%P_codeindetermin == 2) then
               ! NB - le 21/04 - recalcul d'un sbv approprie
               sbvmin = p%P_slamin/(1.0 + p%P_tigefeuil(itk%P_variete))
               p%fpv(:, day) = (p%lai(AS, day) - p%lai(AS, day - 1))*1e4/sbvmin
               p%fpv(:, day) = max(p%fpv(AS, day), 0.0)
            else
               p%fpv(:, day) = 0.0
            end if
         end do

      end if

      close (flai)

   end subroutine Lecture_Lai
end module Lecture_Lai_m

