! Module to test the parameters after the reading and stop the running if necessary
module Tests
use stdlib_strings, only: to_string
use messages

implicit none
    contains

    !> sp de test pour verifier que le min est < au max 
    subroutine raise_error_if_min_gt_max(logger, param_min, param_max, message)
        type(logger_), intent(in) :: logger
        real, intent(in) :: param_min, param_max
        character(*), intent(in) :: message

        if (param_min.gt.param_max) then
            call EnvoyerMsg(logger, message)
            call EnvoyerMsg(logger, 'min ' // to_string(param_min))
            call EnvoyerMsg(logger, 'max ' // to_string(param_max))
        endif
    end subroutine

    !> Return true if value is set to -999, false otherwise
    pure logical function is_value_999(val) result(is_999)
        real, intent(in), optional :: val

        if (present(val)) then
            if(int(val) == -999) then
                is_999 = .true.
            end if
        endif
        is_999 = .false.
    end function

    subroutine raise_error_if_999 (logger, code_name, message, value_p1, value_p2, value_p3, value_p4, value_p5,  &
                            value_p6,value_p7, value_p8, value_p9, value_p10, value_p11, value_p12, value_p13)
        type(logger_), intent(in) :: logger
        character(*), intent(in) :: code_name
        character(*), intent(in) :: message
        real, intent(in) :: value_p1
        real, intent(in),optional :: value_p2
        real, intent(in),optional :: value_p3
        real, intent(in),optional :: value_p4
        real, intent(in),optional :: value_p5
        real, intent(in),optional :: value_p6
        real, intent(in),optional :: value_p7
        real, intent(in),optional :: value_p8
        real, intent(in),optional :: value_p9
        real, intent(in),optional :: value_p10
        real, intent(in),optional :: value_p11
        real, intent(in),optional :: value_p12
        real, intent(in),optional :: value_p13

        if (is_value_999(value_p1)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p2)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p3)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p4)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p5)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p6)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p7)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p8)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p9)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p10)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p11)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p12)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
        if (is_value_999(value_p13)) then
            call EnvoyerMsg(logger, message // code_name)
        endif
    end subroutine
end module
