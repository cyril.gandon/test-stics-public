module soil_computation_m
   use Stics
   use Plante
   use Sol

   implicit none
contains
   pure function compute_rl(sc, plant, soil) result(soil_values)
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(in) :: plant
      type(Sol_), intent(in) :: soil

      real, allocatable :: soil_values(:)

      integer :: iz
      allocate (soil_values(soil%profsol), source=0.)

      do iz = 1, soil%profsol
         soil_values(iz) = plant%rlf(iz) + plant%rlg(iz)
      end do
   end function

   pure function compute_msrac(sc, plant, soil) result(soil_values)
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(in) :: plant
      type(Sol_), intent(in) :: soil

      real, allocatable :: soil_values(:)
      real :: msraccum, msrac
      integer :: iz
      allocate (soil_values(soil%profsol), source=0.)
      msraccum = 0.
      do iz = 1, soil%profsol
         msrac = (plant%rlf(iz)/plant%longsperacf + plant%rlg(iz)/plant%longsperacf)*100.
         msraccum = msraccum + msrac
         soil_values(iz) = msraccum
      end do
   end function

   pure function compute_p_rlf(sc, plant, soil) result(soil_values)
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(in) :: plant
      type(Sol_), intent(in) :: soil

      real, allocatable :: soil_values(:)
      real :: rlcum
      integer :: iz
      allocate (soil_values(soil%profsol), source=0.)
      do iz = 1, soil%profsol
         soil_values(iz) = 0.
         rlcum = plant%rlf(iz) + plant%rlg(iz)
         if (rlcum > 0.) then
            soil_values(iz) = plant%rlf(iz)/rlcum
         end if
      end do
   end function
   pure function compute_p_rlg(sc, plant, soil) result(soil_values)
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(in) :: plant
      type(Sol_), intent(in) :: soil

      real, allocatable :: soil_values(:)
      real :: rlcum
      integer :: iz
      allocate (soil_values(soil%profsol), source=0.)
      do iz = 1, soil%profsol
         soil_values(iz) = 0.
         rlcum = plant%rlf(iz) + plant%rlg(iz)
         if (rlcum > 0.) then
            soil_values(iz) = plant%rlg(iz)/rlcum
         end if
      end do
   end function

   pure function compute_p_rl(sc, plant, soil) result(soil_values)
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(in) :: plant
      type(Sol_), intent(in) :: soil

      real, allocatable :: soil_values(:)
      real :: rltot, rlcum
      integer :: iz
      allocate (soil_values(soil%profsol), source=0.)
      rltot = 0.
      do iz = 1, soil%profsol
         rltot = rltot + plant%rlf(iz) + plant%rlg(iz)
      end do
      rlcum = 0.
      do iz = 1, soil%profsol
         if (rltot > 0.) then
            rlcum = rlcum + (plant%rlf(iz) + plant%rlg(iz))/rltot
         end if
         soil_values(iz) = rlcum
      end do
   end function

end module

