module soil_profile_tests
   use stdlib_string_type
   use stics_system
   use messages
   use soil_profile_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   use test_utils

   implicit none
   private
   public :: collect
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_one_variable_dates", test_one_variable_dates) &
                  , new_unittest("test_one_variable", test_one_variable_frequency) &
                  , new_unittest("test_frequency_too_high", test_frequency_too_high) &
                  , new_unittest("test_multiple_variable", test_multiple_variable) &
                  ]

   end subroutine

   function get_directory_path() result(full_path)
      character(:), allocatable :: full_path
      character(len=255) :: cwd

      call getcwd(cwd)
      full_path = join_path(trim(cwd),'src','inputs','outputs_management','test_data')
   end function

   subroutine test_one_variable_dates(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path
      type(logger_) :: logger
      integer, allocatable :: expected_dates(:)
      type(soil_profile_settings_) :: actual
      file_path = join_path(get_directory_path(),'prof.mod.test_dates.txt')

      actual = Lecture_Profil(logger, file_path, 1, 2000, 2001, 365*2)

      call check(error, size(actual%var_names), 1)
      call check(error, char(actual%var_names(1)), "Chum")

      expected_dates = (/ 1, 32, 61, 92, 122/)
   
      call check_array_i(error, actual%dates, expected_dates)
   end subroutine

   subroutine test_one_variable_frequency(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path
      type(logger_) :: logger
      integer, allocatable :: expected_dates(:)
      type(soil_profile_settings_) :: actual
      integer :: i, expected_frequency

      expected_frequency = 10
      file_path = join_path(get_directory_path(),'prof.mod.test_frequency.txt')

      actual = Lecture_Profil(logger, file_path, 1, 2000, 2001, 365*2)
      call check(error, size(actual%var_names), 1)
      call check(error, char(actual%var_names(1)), "Chum")

      allocate(expected_dates(73))
      expected_dates(1) = 1
      do i = 2, 73
         expected_dates(i) = expected_dates(i-1) + expected_frequency
      end do
      call check_array_i(error, actual%dates, expected_dates)
   end subroutine

   subroutine test_frequency_too_high(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path
      type(logger_) :: logger
      integer, allocatable :: expected_dates(:)
      type(soil_profile_settings_) :: actual
      integer :: i, expected_frequency

      file_path = join_path(get_directory_path(),'prof.mod.test_frequency_too_high.txt')

      actual = Lecture_Profil(logger, file_path, 1, 2000, 2001, 365)
 
      expected_dates = (/ 1, 18, 35, 53, 70 ,87, 105, 122, 139, 157, 174, 191, 209, 226, 243, 261, 278, 295, 313, 330, 347, 365 /)
      call check_array_i(error, actual%dates, expected_dates)
   end subroutine

   subroutine test_multiple_variable(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path
      type(logger_) :: logger
      type(soil_profile_settings_) :: actual
      type(string_type), allocatable :: expected(:)

      file_path = join_path(get_directory_path(),'prof.mod.test_multiple_variable.txt')

      allocate(expected(3), source=string_type(''))
      expected = (/ string_type('Chum'), string_type('efnrac'), string_type('%rl') /)
      actual = Lecture_Profil(logger, file_path, 1, 2000, 2001, 365*2)
      call check_array_string(error, actual%var_names, expected)
   end subroutine
end module
