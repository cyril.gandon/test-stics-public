! Lecture des variables du fichier Rapport (rapport.sti)
! On va lire dans rap.mod la liste des variables dont
! on souhaite avoir les valeurs ecrites dans le fichier rapport.sti
! D'abord on recuperer les dates et/ou les stades pour lesquels
! on veut ces valeurs, puis on lit le nom des variables souhaitees.
! Read variables File Report (rapport.sti)
!! We will read rap.mod the list of variables which
!! one wishes to have the values written to the file rapport.sti
!!
!! First we get the dates and / or stages for which we want these values
!! then we read the name of the desired variables.
module Lecture_VariablesRapport_m
use phenology_utils
use stdlib_string_type
use Stics
use stics_system
use messages
use stics_files
implicit none
private
public :: Lecture_VariablesRapport
contains
subroutine Lecture_VariablesRapport(logger, path, sc, names)
  type(logger_), intent(in) :: logger
  character(*), intent(in) :: path
  type(Stics_Communs_), intent(inout) :: sc
  type(string_type), intent(out), allocatable :: names(:)

  type(string_type), allocatable :: rows(:)

    integer :: i, nboccurstd
    logical :: exists
    integer :: frap
    integer :: codeenteterap, file_row_length, iostat
    character(len=255) :: cell
    character(len=9) :: staderap(20)

    staderap(:) = '********'
    
    sc%rapplt = .FALSE.
    sc%rapger = .FALSE.
    sc%raplev = .FALSE.
    sc%raplax = .FALSE.
    sc%rapflo = .FALSE.
    sc%rapdrp = .FALSE.
    sc%rapsen = .FALSE.
    sc%raprec = .FALSE.
    sc%rapamf = .FALSE.
    sc%rapfin = .FALSE.

    sc%rapdebdes = .FALSE.
    sc%rapdebdebour = .FALSE.
    sc%rapmat = .FALSE.

    sc%rapdebdorm = .FALSE.
    sc%rapfindorm = .FALSE.

    exists = path_exist(path)
    if (.not. exists) then
       call EnvoyerMsg(logger, "Warning: Report variables file doesn't exist: [" // path // "]")
       allocate(names(0))
       return
    end if

    open (newunit=frap, file=path, status=old, action=read_, iostat=iostat)
    if (iostat .ne. 0) then
      call exit_error(logger, "Error opening report variables file: [" // path // "]")
    end if
    file_row_length = number_of_rows(frap)
    allocate(rows(file_row_length))

    ! Lecture codeentete ou pas

      read(frap,*,end=300) sc%codeaucun
      ! PL, 24/09/2020, sc%codeenteterap is now a vector of 2
      read(frap,*,end=300) codeenteterap
      sc%codeenteterap(:) = codeenteterap
      read(frap,*,end=300) sc%codetyperap

! domi 07/10/03 pour Vianney on peut lire des dates et des stades
! si codetyperap = 1 --> dates
! si codetyperap = 2 --> stades
! si codetyperap = 3 --> dates puis stades

    ! on lit des dates
      if (sc%codetyperap == 1 .or. sc%codetyperap == 3) then

      ! on lit le nombre de dates
        read(frap,*,end=300) sc%nboccurrap
      ! on borne ce nombre par la taille max du tableau
        sc%nboccurrap = min(sc%nboccurrap,SIZE(sc%daterap))
        do i = 1,sc%nboccurrap
           read(frap,*,end=300)sc%date_calend_rap(i,3),sc%date_calend_rap(i,2),sc%date_calend_rap(i,1)

        end do

      endif

    ! on lit des stades
      if (sc%codetyperap == 2 .or. sc%codetyperap == 3) then

      ! on lit le nombre de stades
        read(frap,*,end=300) nboccurstd
      ! on borne ce nombre par la taille max du tableau
        nboccurstd = min(nboccurstd,size(staderap))

      ! on lit nboccurstd stades
        do i = 1,nboccurstd

          read(frap,'(a9)',end=913) staderap(i)

        ! en fonction des stades lus, on met a VRAI les booleens correspondants
! DR 29/12/09 javastics donne plt et non nplt
!         if (staderap(i) == 'nplt') sc%rapplt=.TRUE.
          if (staderap(i) == plt) sc%rapplt=.TRUE.
        ! DR 08/01/07 pour Sophie on rajoute l'ecriture a la germination
          if (staderap(i) == ger) sc%rapger=.TRUE.
          if (staderap(i) == lev) sc%raplev=.TRUE.
          if (staderap(i) == amf) sc%rapamf=.TRUE.
          if (staderap(i) == lax) sc%raplax=.TRUE.
          if (staderap(i) == flo) sc%rapflo=.TRUE.
          if (staderap(i) == drp) sc%rapdrp=.TRUE.
          if (staderap(i) == sen) sc%rapsen=.TRUE.
          if (staderap(i) == rec) sc%raprec=.TRUE.
          if (staderap(i) == fin) sc%rapfin=.TRUE.

        ! DR 30/07/08 3 stades ont ete rajoutes
          if (staderap(i) == mat) sc%rapmat=.TRUE.
          if (staderap(i) == debdes) sc%rapdebdes=.TRUE.
          if (staderap(i) == debdebour) sc%rapdebdebour=.TRUE.

        ! DR 12/08/08 2 stades ajoutes
          if (staderap(i) == findorm) sc%rapfindorm=.TRUE.
          if (staderap(i) == debdorm) sc%rapdebdorm=.TRUE.
          if (staderap(i) == start) sc%rapdeb=.TRUE.
        ! Bruno avril 2018 1 stade ajoute pour les fauches
          if (staderap(i) == cut) sc%rapcut =.TRUE.
        end do
      endif

913   continue

! on lit le nom des variables dont on veut ecrire la valeur dans le fichier Rapport.
! Bruno j'augmente le nb de variables maxi du rapport  100 --> 999
! DR 23/11/2020 merge du trunk
! c'etait pass� de 100 � 300 dans le trunk mis ici c'est deja a 999 , je laisse 300 cra c'est pas coherent avce le nb d'ecriture dans le format
! qui est rest� a 200
      do i = 1,300
        read(frap,'(a255)',end=299) cell
        cell = trim(cell)
! test Bruno juin 2017 en cas de plantes associees ajout d'un 2 au nom de la variable
            if(sc%P_nbplantes == 2) then
               if(cell == 'cep')         cell = 'cep2'
               if(cell == 'QNabso')      cell = 'QNabso2'
               if(cell == 'QCressuite')  cell = 'QCressuite2'
               if(cell == 'QNressuite')  cell = 'QNressuite2'
               if(cell == 'QCracmort')   cell = 'QCracmort2'
               if(cell == 'QNracmort')   cell = 'QNracmort2'
               if(cell == 'QCperennemort')  cell = 'QCperennemort2'
               if(cell == 'QNperennemort')  cell = 'QNperennemort2'
               if(cell == 'QNplantetombe')  cell = 'QNplantetombe2'
               if(cell == 'QNrogne')     cell = 'QNrogne2'
             endif

      rows(i) = trim(cell)
! fin modif
      end do
    ! on a lu le fichier jusqu'a sa fin, on calcul le nombre de variables lues pour le stocker.
    
299   allocate(names(i - 1))
      names = rows(1:size(names))
300   close(frap)

! dr 11/03/2013 j'ajoute la variable   sc%codeenteterap_agmip
        sc%codeenteterap_agmip = codeenteterap

return
end subroutine Lecture_VariablesRapport
end module Lecture_VariablesRapport_m
 
