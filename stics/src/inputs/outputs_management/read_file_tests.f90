module read_file_tests
   use stdlib_string_type
   use stics_system
   use messages
   use read_file_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   use test_utils, only: check_array_string

   implicit none
   private
   public :: collect
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_empty_file", test_empty_file) &
                  , new_unittest("test_filled_file", test_filled_file) &
                  ]

   end subroutine collect

   function get_directory_path() result(full_path)
      character(:), allocatable :: full_path
      character(len=255) :: cwd

      call getcwd(cwd)
      full_path = join_path(trim(cwd),'src','inputs','outputs_management','test_data')
   end function get_directory_path

   subroutine test_empty_file(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path
      type(string_type), allocatable :: actual(:), expected(:)
      type(logger_) :: logger

      file_path = join_path(get_directory_path(),'var_empty.txt')

      allocate(expected(0))

      actual = read_file(logger, file_path)
      call check_array_string(error, actual, expected)
   end subroutine

   subroutine test_filled_file(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path
      type(string_type), allocatable :: actual(:), expected(:)
      type(logger_) :: logger

      file_path = join_path(get_directory_path(),'var_filled.txt')

      allocate(expected(3), source=string_type())
      expected = (/ string_type('1'), string_type('3'), string_type('f90'), &
      string_type('verylongvalueverylongvalueverylongvalueverylongvalueverylongvalueverylongvalueverylongvalueverylongvalue') /)

      actual = read_file(logger, file_path)
      call check_array_string(error, actual, expected)
   end subroutine test_filled_file
end module read_file_tests
