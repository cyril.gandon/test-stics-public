module soil_profile_m
   use stdlib_string_type
   use string_utils, only: split_sentence
   use stics_system
   use messages
   use messages_data
   use Stics
   use stics_files
   use dates_utils
   use Plante
   use math_utils
   use soil_profile_settings_m
   use Sol
   implicit none

   !> List of fixed dates
   integer, parameter :: FIXED_DATE_OPTION = 1
   !> choix date de debut et frequence
   integer, parameter :: START_DATE_FREQUENCY_OPTION = 2

contains
   type(soil_profile_settings_) function Lecture_Profil( &
      logger, path, start_simulation_day, first_year, last_year, sim_duration &
      ) result(res)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      integer, intent(in) :: start_simulation_day, first_year, last_year, sim_duration

      logical :: exists
      character(255) :: buffer
      integer :: code_option, unit, iostat, dates_count, i

      exists = path_exist(path)
      if (.not. exists) then
         call EnvoyerMsg(logger, "Warning: Profile variables file doesn't exist: ["//path//"]")
         allocate (res%dates(0))
         allocate (res%var_names(0))
         return
      end if

      open (newunit=unit, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening profile variables file: ["//path//"]")
      end if

      read (unit, *, iostat=iostat) code_option
      if (iostat .ne. 0) then
         call exit_error(logger, SOIL_PROFIL_READ_ERROR)
      end if

      if (code_option .ne. FIXED_DATE_OPTION .and. code_option .ne. START_DATE_FREQUENCY_OPTION) then
         allocate (res%dates(0))
         allocate (res%var_names(0))
         return
      end if

      !> First line of the file contain the variables name we want to output 
      read (unit, '(A)', iostat=iostat) buffer
      if (iostat .ne. 0) then
         call exit_error(logger, SOIL_PROFIL_READ_ERROR)
      end if
      res%var_names = split_sentence(buffer)

      if (code_option == FIXED_DATE_OPTION) then
         block
            type(day_dmy_), allocatable :: days(:)
            integer :: day_num, i
            days = read_days(unit)
            allocate (res%dates(size(days)))
            do i = 1, size(days)
               day_num = day_number_dmy(days(i))
               if (days(i)%year .eq. first_year) then
                  res%dates(i) = day_num
               else if (days(i)%year .eq. last_year) then
                  res%dates(i) = day_num + days_count(first_year)
               else
                  call exit_error(logger, SOIL_PROFIL_YEAR_ERROR // to_string(days(i)%year))
               end if
            end do
         end block
      else
         block
            integer :: start_day, start_month, start_year, start_date, j, min_result
            real :: frequency
            read (unit, *, iostat=iostat) frequency
            if (iostat .ne. 0) then
               call exit_error(logger, SOIL_PROFIL_READ_ERROR)
            end if
            read (unit, *, iostat=iostat) start_day, start_month, start_year
            if (iostat .ne. 0) then
               call exit_error(logger, SOIL_PROFIL_READ_ERROR)
            end if
            start_date = day_number(start_day, start_month, start_year)

            ! tests de coherence
            if (start_date < start_simulation_day) then
               start_date = start_simulation_day
               call EnvoyerMsg(logger, SOIL_PROFIL_DATES_PROBLEM)
            end if

            dates_count = int((sim_duration - 1)/frequency) + 1
            min_result = 22
            if (dates_count .lt. min_result) then
               !> Ensure we got at least min_results
               res%dates = uniform_values(start_date, start_date + sim_duration - 1, min_result)
            else
               allocate (res%dates(dates_count))
               do j = 1, size(res%dates)
                  res%dates(j) = int(start_date + ((j - 1)*frequency))
               end do
            end if
         end block
      end if

      close (unit)
   end function

   FUNCTION read_days(unitNumber) result(lines)
      INTEGER, INTENT(IN) :: unitNumber
      INTEGER :: i, count, iostat
      TYPE(day_dmy_), ALLOCATABLE :: lines(:)

      CHARACTER(100) :: buffer
      INTEGER :: day, month, year

      ! Revenir au début du fichier
      REWIND (unitNumber)

      ! Ignore first 2 lines
      READ (unitNumber, *)
      READ (unitNumber, *)

      ! Compter le nombre de lignes dans le fichier
      count = 0
      DO
         READ (unitNumber, *, IOSTAT=iostat) buffer
         IF (iostat /= 0) EXIT  ! Quitter la boucle si la fin du fichier est atteinte
         count = count + 1
      END DO

      ! Allouer l'espace pour stocker les lignes
      ALLOCATE (lines(count))

      ! Revenir au début du fichier
      REWIND (unitNumber)

      ! Ignore first 2 lines again
      READ (unitNumber, *)
      READ (unitNumber, *)

      ! Lire les lignes et stocker les entiers dans la liste
      DO i = 1, count
         READ (unitNumber, *) day, month, year
         lines(i)%day = day
         lines(i)%month = month
         lines(i)%year = year
      END DO
   END FUNCTION

   subroutine write_profil(logger, sc, p)
      type(logger_), intent(in) :: logger
      type(Stics_Communs_), intent(in) :: sc
      type(Plante_), intent(in) :: p(:)

      integer :: var_name_index, i_year, i_soil, j, i_day, ipl
      integer :: iostat, unit, var_count
      character(:), allocatable :: path, var_name, filename

      do ipl = 1, sc%P_nbplantes
         var_count = size(sc%soil_profile_settings%var_names)
         do var_name_index = 1, var_count
            var_name = char(sc%soil_profile_settings%var_names(var_name_index))
            filename = get_file_name(sc%P_nbplantes, ipl, var_name)
            path = join_path(logger%output_path, filename)
            open (newunit=unit, file=path, status=replace_, action=write_, iostat=iostat)
            if (iostat .ne. 0) then
               call exit_error(logger, "Error opening ouput profil file: ["//path//"]")
            end if
            do i_year = 1, size(p(ipl)%tabprof, 1)

               !! Write the variable name in the first line
               write (unit, *) var_name

               !! Write headers with dates
               write (unit, 210) (sc%soil_profile_settings%dates(i_day), i_day=1, size(sc%soil_profile_settings%dates))

               !! Write values
               do i_soil = 1, size(p(ipl)%tabprof, 4)
                  write (unit, 100) i_soil, &
                     (p(ipl)%tabprof(i_year, var_name_index, i_day, i_soil), i_day=1, size(p(ipl)%tabprof, 3))
               end do
            end do
            close (unit)
         end do
      end do

210   format(' cm  ', 600(i11))
100   format(i5, 600f11.5)
   end subroutine

   !> Return the file name where the soil profile should be written
   function get_file_name(plant_count, plant_index, var_name) result(filename)
      integer, intent(in) :: plant_index
      integer, intent(in) :: plant_count
      character(*), intent(in) :: var_name

      character(:), allocatable :: filename

      if (plant_count == 1) then
         filename = 'mod_profil_'//var_name//'.sti'
      else
         if (plant_index == 1) then
            filename = 'mod_profilP_'//var_name//'.sti'
         else
            filename = 'mod_profilA_'//var_name//'.sti'
         end if
      end if
   end function
end module
