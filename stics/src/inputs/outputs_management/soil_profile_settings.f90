module soil_profile_settings_m
   use stdlib_string_type

   !> Contains value needed to output file mod_profile.sti, init by reading prof.mod
   type :: soil_profile_settings_
      !> Variable names the user wants to output
      type(string_type), allocatable :: var_names(:)
      !> List of dates the user wants to output
      integer, allocatable :: dates(:)
   end type
end module
