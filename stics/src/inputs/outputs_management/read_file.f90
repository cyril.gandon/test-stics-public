
! Read the selected output variables in the file var.mod
!! one wishes to have the values ​​written to the file mod_s*.sti
module read_file_m
   use stdlib_string_type
   use stics_system
   use messages
   use stics_files
   implicit none
   private
   public :: read_file
contains
 function read_file(logger, path) result(values_trimmed)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path
      type(string_type), allocatable :: values(:), values_trimmed(:)

      integer :: i, eof, unit, iostat, file_row_length
      logical :: exists
      character(len=255) :: raw_content
      character(len=:), allocatable :: trimmed_content
      
      exists = path_exist(path)
      if (.not. exists) then
         call EnvoyerMsg(logger, "Warning: Daily variable names file doesn't exist: [" // path // "]")
         allocate(values_trimmed(0))
         return
      end if

      open (newunit=unit, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening daily variable names file: [" // path // "]")
      end if

      file_row_length = number_of_rows(unit)

      allocate(values(file_row_length), source=string_type())

      i = 0
      do
         read (unit, '(a255)', iostat=eof) raw_content
         if (eof.ne.0) exit

         trimmed_content = adjustl(trim(raw_content))
         if(len(trimmed_content).gt.0) then
            i = i + 1
            values(i) = trimmed_content
         endif
      end do
      close(unit)

      allocate(values_trimmed(i))
      if(i.gt.0) then
         values_trimmed = values(1:i)
      end if
    end function read_file
end module read_file_m
