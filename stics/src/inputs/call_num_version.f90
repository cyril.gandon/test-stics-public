module call_num_version_m
implicit none
private
public :: call_num_version
contains
subroutine call_num_version (codeversion)



  implicit none

! sous programme de recuperation du nom et du numero de version

! dr 20/06/2013 j'ajoute le numero de version et la date genere automatiquement a la pose d'un tag ??

  character(len=50), intent(OUT) :: codeversion

  character(len=10) :: dateversion

  character(len=30) :: nomversion
        nomversion='10.1.0'
        dateversion='2024-01-12'

     codeversion = trim(nomversion)//'_'//trim(dateversion)

end subroutine call_num_version
end module call_num_version_m