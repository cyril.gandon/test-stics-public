!- Description of the structure Root_

module Root

USE Stics, only: nbCouchesSol, maxdayf, maxdayg

implicit none

!  integer, parameter :: maxdayf = 1096           ! Taille maximale du tableau drlf (perennes)
!  integer, parameter :: maxdayg = 3653           ! Taille maximale des tableaux drlg et dtj
!  integer, parameter :: maxdayf = 731            ! Taille maximale du tableau drlf (annuelles)
!  integer, parameter :: maxdayg = 731            ! Taille maximale des tableaux drlg et dtj

type Root_

  real    :: drlf(nbCouchesSol,maxdayf)       ! temporel pour senescence (racinaire) duree de vie maximale = maxdayf
  real    :: drlg(nbCouchesSol,maxdayg)       ! temporel pour senescence (racinaire) duree de vie maximale = maxdayg

end type Root_

end module Root
 
 
