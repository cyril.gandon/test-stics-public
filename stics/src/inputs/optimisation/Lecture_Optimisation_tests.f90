module Lecture_Optimisation_tests
   use Stics
   use Plante
   use Itineraire_Technique
   use Sol
   use Climat
   use Station
   use Parametres_Generaux
   use Lecture_Optimisation_m
   use testdrive, only: error_type, unittest_type, new_unittest, check

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_Lecture_Optimisation", test_Lecture_Optimisation) &
                  ]
   end subroutine

   subroutine test_Lecture_Optimisation(error)
      type(error_type), allocatable, intent(out) :: error
      type(Stics_Communs_) :: commons
      type(Parametres_Generaux_) :: pg
      type(Plante_) :: plants(2)
      type(ITK_):: itk(2)
      type(Sol_) :: soil
      type(Climat_) :: climate
      type(Station_) :: station
      type(Stics_Transit_):: transit
      logical :: optim_error

      call Lecture_Optimisation(commons, pg, plants, itk, soil, station, transit, 1, "albedo", 4., optim_error)

      call check(error, optim_error, .false.)
      call check(error, soil%P_albedo, 4.)

      call Lecture_Optimisation(commons, pg, plants, itk, soil, station, transit, 1, "does not exists", 4., optim_error)
      call check(error, optim_error, .true.)

   end subroutine
end module

