module forced_parameters_tests
   use stics_system
   use forced_parameters_m
   use testdrive, only: error_type, unittest_type, new_unittest, check

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_forced_parameters", test_get_forced_parameters) &
                  ]
   end subroutine

   subroutine test_get_forced_parameters(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: path
      type(forced_parameter_), allocatable :: forced_parameters(:)
      character(len=255) :: cwd

      call getcwd(cwd)
      path = join_path(trim(cwd),'src','inputs','optimisation','param_test.sti')

      forced_parameters = get_forced_parameters(get_silent_logger(), path)

      call check(error, size(forced_parameters), 4)
      call check(error, forced_parameters(1)%name, "albedo")
      call check(error, forced_parameters(1)%value, 2.)

      call check(error, forced_parameters(2)%name, "pH0")
      call check(error, forced_parameters(2)%value, 3.)

      call check(error, forced_parameters(3)%name, "P_Vabs2")
      call check(error, forced_parameters(3)%value, 4.1)

      call check(error, forced_parameters(4)%name, "should_trim")
      call check(error, forced_parameters(4)%value, 5.9)
   end subroutine
end module
