module forced_parameters_m
   use stics_system
   use messages

   implicit none

   !> Value and name of a forced parameter
   type forced_parameter_
      character(:), allocatable :: name
      real :: value
   end type

contains
   function get_forced_parameters(logger, path) result(parameters)
      type(logger_), intent(in) :: logger
      character(*), intent(in) :: path

      type(forced_parameter_), allocatable :: parameters(:)
      
      logical :: exists
      integer :: unit, iostat, i, count, iostat_count
      character(len=255) :: buffer

      exists = path_exist(path)
      if (.not. exists) then
         call exit_error(logger, "Forced parameters file doesn't exist: [" // path // "]")
      end if

      open (newunit=unit, file=path, status=old, action=read_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, "Error opening Forced parameters file: [" // path // "]")
      end if

      read (unit, *, iostat=iostat_count) count
      if(iostat_count .ne. 0) then
         call exit_error(logger, "Error reading first line of Forced parameters file, should be an integer: [" // path // "]")
      end if
      allocate(parameters(count))
      do i = 1, count
         read (unit, *, end=80, err=250) buffer
         parameters(i)%name = adjustl(trim(buffer))
         read (unit, *, end=80, err=250) parameters(i)%value
      end do

      close(unit)
      return

80    call exit_error(logger, "Error in forced parameters file, missing lines: [" // path // "]")

250   call exit_error(logger, "Error reading forced parameters file: [" // path // "]")

   end function
end module
