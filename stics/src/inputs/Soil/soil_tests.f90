module soil_tests
   use stics_system
   use messages
   use Sol
   use testdrive, only: error_type, unittest_type, new_unittest, check

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_read_sol_from_file", test_read_sol_from_file) &
                  ]

   end subroutine

   subroutine test_read_sol_from_file(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: path
      type(Sol_) :: soil
      character(len=1) :: sep
      character(len=255) :: cwd
      sep = filesep()

      call getcwd(cwd)
      path = trim(cwd)//sep//'src'//sep//'inputs'//sep//'Soil'//sep//'test_data'//sep//'param.sol'

      call Sol_Lecture(get_silent_logger(), path, soil)

      call check(error, 1, soil%P_numsol)
      call check(error, 11, soil%P_profdrain)

      call check(error, 11, soil%P_epc(1))
      call check(error, 12, soil%P_epc(2))
      call check(error, 13, soil%P_epc(3))
      call check(error, 14, soil%P_epc(4))
      call check(error, 40, soil%P_epc(5))

   end subroutine
end module
