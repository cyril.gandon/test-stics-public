! subroutine to read all files parameters
module Stics_Lectures_m
use stdlib_string_type
use stdlib_strings, only: to_string
USE stics_files
USE Stics
USE Plante
use plant_checks_m
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE Module_AgMIP
use messages
use daily_output_m
use lecinitialisations_m, only: lecinitialisations
use Stics_Lecture_Transit_m, only: Stics_Lecture_Transit
use Plante_Ecriture_m, only: Plante_Ecriture
use forced_parameters_m
use Lecture_Optimisation_m, only: Lecture_Optimisation
use read_file_m, only: read_file
use Lecture_VariablesRapport_m, only: Lecture_VariablesRapport
use Ecriture_Transit_m, only: Ecriture_Transit
use Initialisation_PrairiePerenne_m, only: Initialisation_PrairiePerenne
use climate_utils
implicit none
contains
subroutine Stics_Lectures(logger,sc,pg,p,itk,soil,c,sta,transit)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT) :: pg  
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)  
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)  
  type(Sol_),                 intent(out) :: soil  
  type(Climat_),              intent(INOUT) :: c  
  type(Station_),             intent(INOUT) :: sta  
  type(Stics_Transit_),       intent(out) :: transit  

    integer :: ipl, ii, i
    character(:), allocatable :: soil_path, station_path

    integer :: nbpar
    character (len = 4) :: ntec, nplt

    integer :: codeRetour  

    integer :: codesnowout 
    character(:), allocatable :: init_path, itk_path, plant_path
    type(string_type), allocatable :: report_variable_names(:), var_mod_content(:), unknown_vars(:), unknown_vars2(:)
    type(forced_parameter_), allocatable :: forced_parameters(:)
    type(forced_parameter_) :: forced_parameter
    character(:), allocatable :: filename_report

    integer :: numplt, param_index
    integer :: pltread
    integer :: valopt
    logical :: error

    numplt=1
    pltread=0
  
    call EnvoyerMsgEcran(logger, "lecinitialisations")

      soil = Sol_Zero()

      ! Getting ini file infos
      !! Lecture du fichier d'initialisations : ficini.txt (format JavaStics)
      if (index(sc%P_ficInit, '.xml') .eq. 0) then
        ! if file is not an xml, take it into account
        init_path = sc%P_ficInit
      else
        init_path = default_init_path
      endif

      call lecinitialisations(logger, init_path, sc,p,soil,sta)
      
    !: Lecture du fichier des parametres en attente d'etre affectes a leurs fichiers respectifs
      transit = Stics_Lecture_Transit(logger, new_formalisms_path, sc%P_nbplantes)
       

    ! Pour chaque plante de la culture, on va lire un fichier technique et un fichier plante
      do ipl = 1, sc%P_nbplantes

      ! Ouverture des fichiers de sorties journalieres
       if (iand(pg%P_flagEcriture, ECRITURE_SORTIESJOUR) > 0) then

          ! 23/03/2016 pour Constance on a ajoute un param pour activer ou non la lecture des eclaircissages multiples
!          if(t%P_option_thinning.eq.1) then
!              itk(i)%flag_eclairmult=.TRUE.
!          else
!              itk(i)%flag_eclairmult=.FALSE.
!          endif

!          if(t%P_option_engrais_multiple.eq.1) then
!              itk(i)%flag_plusieurs_engrais=.TRUE.
!          else
!              itk(i)%flag_plusieurs_engrais=.FALSE.
!          endif

! DR 31/05/2017 avant la sortie de la version je desactive en dur la possibilite d'activation des patures
! Bruno juillet 2018 on re-active pour l'etude 4 p mille
!           if(t%P_option_pature.eq.1) then
!              call EnvoyerMsgHistorique(logger, MESSAGE_2100)
!              t%P_option_pature = 2
!           endif
! fin 31/05/2017
           if(transit%P_option_pature.eq.1) then
                itk(ipl)%flag_pature=.TRUE.
           else
                itk(ipl)%flag_pature=.FALSE.
           endif
        endif

      ! Ouverture des fichiers rapports
      ! Sorties specifiques pour AGMIP
        if (iand(pg%P_flagEcriture, ECRITURE_AGMIP) > 0) then
          call read_projects_specificities(logger,depths_paramv6, sc)
        endif

        ii = index(sc%P_wdata1,'.')
        sc%wlieu = sc%P_wdata1(1:ii-2)

      ! Lecture du fichier technique
      if (index(p(ipl)%P_ftec, '.xml') .eq. 0) then
        itk_path = p(ipl)%P_ftec
      else
        itk_path = 'fictec' // to_string(ipl) // '.txt'
      endif

      call ITK_Lecture(logger, itk_path, itk(ipl))

      call ITK_Ecriture_Tests(logger, itk(ipl), pg, sc, itk_path)

      sc%napini(ipl) = itk(ipl)%nap
      sc%napNini(ipl) = itk(ipl)%napN
      sc%nbjresini(ipl) = itk(ipl)%P_nbjres

      ! Lecture du fichier de parametres de la plante
      if (index(p(ipl)%P_fplt, '.xml') .eq. 0) then
        plant_path = p(ipl)%P_fplt
      else
        plant_path = 'ficplt' // to_string(ipl) // '.txt'
      endif
        
        call Plante_Lecture_fichier(logger, plant_path, p(ipl))
        call init_file_output(p(ipl), ipl, sc%P_nbplantes, sc%P_usm)
        call Plante_Tests(logger, p(ipl),itk(ipl)%P_variete)

        call Plante_Ecriture(logger, p(ipl),sc,sta,itk(ipl),pg)

        sc%plante_ori(ipl)=.TRUE.
      ! Par defaut, la plante lue en premier est consideree comme dominante et les suivantes comme dominees.
      ! TODO : voir si on peut mettre les affectations de is_dominant ailleurs
        p(ipl)%is_dominant = .FALSE.

      ! si sol nu pas de test/ecriture
       if (p(ipl)%P_codeplante == CODE_BARESOIL) itk(ipl)%P_iplt0 = sc%P_iwater
      ! si sol nu, pas de plante vivante
       if (p(ipl)%P_codeplante == CODE_BARESOIL) then
          p(ipl)%estVivante = .FALSE.
       else
          p(ipl)%estVivante = .TRUE.
       endif

! 08/11/2018 test pour l activation de hautfauchedyn pour la luzerne dans la version perenne
! solution temporaire avant migration vers fichier plante puis vers fichier tec
!      if (p(i)%P_codeplante == CODE_FODDER .and. p(i)%P_code_acti_reserve.eq.1)then
!         t%P_code_hautfauche_dyn = 1
!      endif
!
! stockage des variables par profil
! disabling for restoring old functionning, call from Stics_jour_After
!       if ( stics_files%flag_profil ) then 
!           call Lecture_Profil(sc,p(i),soil,i)
!       end if
     enddo

     p(1)%is_dominant = .TRUE.

    ! Lecture du fichier station
     
     ! DR 17/07/2012 pour gerer les lectures de fichiers parametres pour Optimistics on regarde si les noms de fichiers sont des xml ou pas
     if (index(sc%P_ficStation, '.xml') .eq. 0) then
       station_path = sc%P_ficStation
     else
       station_path = default_station_path
     endif
     call Station_Lecture(logger, station_path, sta)

    ! Ecriture du fichier historique pour la structure Station

    !DR 13/12/2017 deplacement du test sur codeclichange ici et besoin de alphaCO2
      ! modifying codesnow if codeabri == 1
      call Station_Ecriture(logger, sta,itk(1)%P_codabri,p(1)%P_alphaCO2,pg%P_codesnow,codesnowout)
      pg%P_codesnow=codesnowout

    ! pour les cultures associees, il faut SW, sinon stop
      if (sc%P_nbplantes > 1 .and. sta%P_codeetp /= ETP_SW) then
        call exit_error(logger, INTERCROP_STATION_ETP_MUST_BE_SW)
      endif

    ! On effectue un test de coherence des parametres pour le transfert radiatif
      if (sc%P_nbplantes > 1) then
        if (itk(1)%P_codetradtec == 2) then
           call exit_error(logger, INTERCROP_TRANSRAD_ERROR)
        endif
        p(1)%codetransradb = p(1)%P_codetransrad
        p(1)%P_codetransrad = 2
      endif

      do ipl = 1, sc%P_nbplantes
        if (p(ipl)%P_codetransrad == 2) then
           if ( itk(ipl)%P_interrang >= 999    .or.   &
                itk(ipl)%P_interrang <= 0.     .or.   &
                itk(ipl)%P_orientrang >= 999.  .or.   &
                p(ipl)%P_ktrou(itk(ipl)%P_variete) >= 999.         .or.   &
                p(ipl)%P_forme >= 999.         .or.   &
                p(ipl)%P_rapforme >= 999.      .or.   &
                p(ipl)%P_hautbase(itk(ipl)%P_variete) >= 999.      .or.   &
                p(ipl)%dfol >= 999.) then
             call exit_error(logger, MISSING_TRANSRAD_PARAMS_ERROR)
           end if
        endif
      end do

! DR 11/12/2014 on verifie la coherence des parametres pour l'etp SW et le codebeso
     if(sta%P_codeetp == ETP_SW .and. p(1)%P_codebeso==1 ) then
      call exit_error(logger, MISSING_ETP_SW_PARAMS_ERROR)
     endif

! DR  et Fr 30/05/2016 on verifie la coherence des parametres pour l'etp PC ou PE et le codebeso
     if(sta%P_codeetp .ne.ETP_SW .and. p(1)%P_codebeso==2 ) then
        call EnvoyerMsgHistorique(logger, ETP_CODEBESO_1_WARNING)
     endif
    ! Lecture des parametres de sol
     if (index(sc%P_nomsol, '.sol') > 0) then
       soil_path = sc%P_nomsol
     else
       soil_path = default_soil_path
     endif
     call Sol_Lecture(logger, soil_path, soil)
      
! Bruno - initialisation du nouveau parametre rapport C/N du sol
      !if(soil%P_CsurNsol0 == 0.)  then
      if(abs(soil%P_CsurNsol0).lt.1.0E-8)  then
        soil%CsurNsol = 1./pg%P_Wh
      else
        soil%CsurNsol = soil%P_CsurNsol0
      endif

      !if(soil%P_penterui == 0.) soil%P_penterui = 0.33   !DR 27/07/2012 - externalisation du parametre penterui
      if(abs(soil%P_penterui).lt.1.0E-8) soil%P_penterui = 0.33   !DR 27/07/2012 - externalisation du parametre penterui

    ! Pour tester les parametres du sol
     call Sol_Tests(logger, soil, nbCouchesSol, sc%P_ichsl, pg%P_masvolcx, sc%beta_sol)
     do ipl = 1, sc%P_nbplantes
          call sol_test_itk(logger, itk(ipl),soil%P_profhum)
     enddo

     call Sol_Ecriture(logger,soil,pg,sc)

!: MODULE D'OPTIMISATION DES PARAMETRES D'UNE PLANTE
!- Dans le cas ou le modele est lance pour une optimisation, lecture des parametres de depart
!- si codoptim=1, on optimise s'il y a lieu les parametres de la Culture Principale
!- si codoptim=2  on optimise s'il y a lieu les parametres de la Culture Associee
      if (sc%codoptim /= 0) then ! si le P_codeoptim est different de zero, on optimise
        
        ! Getting names, values and number of parameters from the param.sti file
        forced_parameters = get_forced_parameters(logger, forced_parameter_path)
        
      ! DR 19/09/2012 je passe les parametres a forcer en arguments
        call EnvoyerMsgHistorique(logger, 'Lecture param.sti')
        numplt=1
        pltread=0
        do param_index = 1,size(forced_parameters)
          forced_parameter = forced_parameters(param_index)
          ! checking plt tags read
          if (pltread.gt.2) then
            call exit_error(logger, 'Erreur lecture param.sti: detection de plus de 2 plantes (tags plt) !')
          endif

          ! Detecting plt tag
          if (forced_parameter%name == 'plt') then

              valopt=int(forced_parameter%value)

              ! checking plt value
              if ((valopt > 2).AND.(valopt < 1)) then
                call exit_error(logger, 'Erreur lecture param.sti: numero de plante incorrect (0 ou >2) !')
              endif

              ! detecting same plt num
              if ((numplt == valopt).AND.(pltread > 0)) then
                  call exit_error(logger, 'Erreur lecture param.sti: doublon pour le numero de plante !')
              endif
              numplt=valopt
              pltread=pltread+1
            CYCLE
          endif
          call Lecture_Optimisation(sc,pg,p,itk,soil,sta,transit, numplt,forced_parameter%name,forced_parameter%value, error)
          if (error .and. sc%n == 1) then
            call EnvoyerMsgHistorique(logger, '')
            call EnvoyerMsgHistorique(logger, forced_parameter%name//': unknown variable name (check case sensitivity)')
          end if
        enddo
      endif

    ! Fetch daily variables names in file var.mod
    if (iand(pg%P_flagEcriture, ECRITURE_SORTIESJOUR) > 0) then
      var_mod_content = read_file(logger, ouput_variables_path)
      call filter_unknown_variable_names(var_mod_content, sc%daily_var_names, unknown_vars)
      do i=1, size(unknown_vars)
        call EnvoyerMsg(logger, 'WARNING in ' // ouput_variables_path // ': [' // char(unknown_vars(i))&
          // '] is not a known variable name (check case sensitivity).')
      end do
    else
      allocate(sc%daily_var_names(0))
    end if
    sc%daily_formats = build_daily_formats(sc%daily_var_names, pg%exponent_form)

    ! Lecture des variables du fichier rapport.sti
    if (iand(pg%P_flagEcriture, ECRITURE_RAPPORTS) > 0) then
      call Lecture_VariablesRapport(logger, report_variables_path, sc, report_variable_names)
      call filter_unknown_variable_names(report_variable_names, sc%valrap, unknown_vars2)

      do i=1, size(unknown_vars2)
        call EnvoyerMsg(logger, 'WARNING in ' // report_variables_path // ': [' // char(unknown_vars2(i))&
          // '] is not a known variable name (check case sensitivity).')
      end do
    else
      allocate(sc%valrap(0))
    end if

    allocate(sc%valsortie_flo(size(sc%valrap)), source=0.)
    allocate(sc%valsortie_mat(size(sc%valrap)), source=0.)
    allocate(sc%valsortie_iplt(size(sc%valrap)), source=0.)
    allocate(sc%valsortie_rec(size(sc%valrap)), source=0.)

    ! ecritures des messages dans l'history
    if (numplt == 1) then
      call Ecriture_Transit(logger, transit, p(1)%P_codeplante)
    else
      call Ecriture_Transit(logger, transit,p(1)%P_codeplante, p(2)%P_codeplante)
    end if

    !call Ecriture_Transit(logger, transit,p(1)%P_codeplante,p(2)%P_codeplante )
end subroutine Stics_Lectures

  !> Split an array of variable names, by distinguish if they exists in the function `CorrespondanceVariablesDeSorties`.
  pure subroutine filter_unknown_variable_names(var_names, known_vars, unknown_vars)
    type(string_type), allocatable, intent(in) :: var_names(:)

    type(string_type), allocatable, intent(out) :: known_vars(:), unknown_vars(:)

    ! We don't care about those type, we just want to know if the variable exist
    type(Stics_Communs_) :: sc  
    type(Plante_) :: p  
    type(Sol_) :: soil  
    type(Climat_) :: c  
    type(Station_) :: sta  
  
    logical, allocatable :: known_logicals(:)
    real, allocatable :: dummy(:)

    allocate(known_logicals(size(var_names)))
    allocate(dummy(size(var_names)))

    call get_variable_value(var_names, sc, p, soil, c, sta, dummy, known_logicals)

    ! Keep only variable names that has been read successfully
    known_vars = pack(var_names, known_logicals)
    unknown_vars = pack(var_names, known_logicals.eqv..false.)

  end subroutine
end module Stics_Lectures_m
 
