module Stics_Lectures_tests
  use stdlib_string_type
  use testdrive
  use test_utils
  use Stics_Lectures_m
  implicit none

contains

  subroutine collect(testsuite)
     type(unittest_type), allocatable, intent(out) :: testsuite(:)

     testsuite = [ &
                 new_unittest("test_filter_unknown_variable_names", test_filter_unknown_variable_names) &
                 ,new_unittest("test_filter_unknown_variable_names_empty", test_filter_unknown_variable_names_empty) &
                 ]

  end subroutine

  subroutine test_filter_unknown_variable_names_empty(error)
    type(error_type), allocatable, intent(out) :: error

    type(string_type), allocatable :: input(:)
    type(string_type), allocatable :: actual_known_vars(:), actual_unknown_vars(:)

    allocate(input(0))

    call filter_unknown_variable_names(input, actual_known_vars, actual_unknown_vars)

    call check(error, size(actual_known_vars), 0)
    call check(error, size(actual_unknown_vars), 0)
  end subroutine

  subroutine test_filter_unknown_variable_names(error)
     type(error_type), allocatable, intent(out) :: error

     type(string_type), allocatable :: input(:)
     type(string_type), allocatable :: expected_known_vars(:), expected_unknown_vars(:)
     type(string_type), allocatable :: actual_known_vars(:), actual_unknown_vars(:)

     input = (/ string_type('AZnit(1)'), string_type('UNKNOWN1'), string_type('Qminr'), string_type('UNKNOWN2') /)
     expected_known_vars = (/ string_type('AZnit(1)'), string_type('Qminr') /)
     expected_unknown_vars = (/ string_type('UNKNOWN1'), string_type('UNKNOWN2') /)

     call filter_unknown_variable_names(input, actual_known_vars, actual_unknown_vars)

     call check_array_string(error, actual_known_vars, expected_known_vars)
     call check_array_string(error, actual_unknown_vars, expected_unknown_vars)
  end subroutine
end module
