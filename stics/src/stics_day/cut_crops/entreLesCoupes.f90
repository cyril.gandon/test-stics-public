module entreLesCoupes_m
use iso_fortran_env, only: real32
implicit none
private
public :: entreLesCoupes
contains
subroutine entreLesCoupes(n,numcoupe,nbcoupe,lai,P_lairesiduel,somcour,nlev,nlax,swfac,turfac,inns,tcult,tmoy,nmat,   &
                          udevlaires,nst1coupe,str1coupe,stu1coupe,inn1coupe,diftemp1coupe,str1intercoupe,          &
                          stu1intercoupe,inn1intercoupe,diftemp1intercoupe,nst2coupe,str2coupe,stu2coupe,inn2coupe, &
                          diftemp2coupe,str2intercoupe,stu2intercoupe,inn2intercoupe,diftemp2intercoupe)

  integer, intent(IN)    :: numcoupe
  integer, intent(IN)    :: nbcoupe  
  real(real32),    intent(IN)    :: lai                         ! (aoas,n)        // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  real(real32),    intent(IN)    :: P_lairesiduel(1:nbcoupe)     ! // PARAMETER // Residual leaf index after each cut (table) // m2 leaf  m-2 soil // PARTEC // 1
  real(real32),    intent(IN)    :: somcour      ! // OUTPUT // Cumulated units of development between two stages // degree.days
  integer, intent(IN)    :: nlev
  integer, intent(IN)    :: nlax
  integer, intent(IN)    :: n
  real(real32),    intent(IN)    :: swfac                       ! (aoas)      // OUTPUT // Index of stomatic water stress  // 0-1
  real(real32),    intent(IN)    :: turfac                      ! (aoas)      // OUTPUT // Index of turgescence water stress  // 0-1
  real(real32),    intent(IN)    :: inns                        ! (aoas)      // OUTPUT // Index of nitrogen stress active on growth in biomass // P_innmin to 1
  real(real32),    intent(IN)    :: tcult      ! // OUTPUT // Crop surface temperature (daily average) // degree C
  real(real32),    intent(IN)    :: tmoy                        ! (n)     // OUTPUT // Mean active temperature of air // degree C
  integer, intent(IN)    :: nmat

  real(real32),    intent(INOUT) :: udevlaires(1:nbcoupe)
  integer, intent(INOUT) :: nst1coupe
  real(real32),    intent(INOUT) :: str1coupe
  real(real32),    intent(INOUT) :: stu1coupe
  real(real32),    intent(INOUT) :: inn1coupe
  real(real32),    intent(INOUT) :: diftemp1coupe
  real(real32),    intent(INOUT) :: str1intercoupe      ! // OUTPUT // stomatal water stress average during the cut (cut crop vegetative phase)  // 0-1
  real(real32),    intent(INOUT) :: stu1intercoupe      ! // OUTPUT // turgescence water stress average during the cut (cut crop vegetative phase)  // 0-1
  real(real32),    intent(INOUT) :: inn1intercoupe      ! // OUTPUT // nitrogen stress (inn) average during the cut (cut crop vegetative phase)  // 0-1
  real(real32),    intent(INOUT) :: diftemp1intercoupe      ! // OUTPUT // mean difference between crop surface temperature and air temperature during the cut (cut crop vegetative phase) // degree C
  integer, intent(INOUT) :: nst2coupe
  real(real32),    intent(INOUT) :: str2coupe
  real(real32),    intent(INOUT) :: stu2coupe
  real(real32),    intent(INOUT) :: inn2coupe
  real(real32),    intent(INOUT) :: diftemp2coupe
  real(real32),    intent(INOUT) :: str2intercoupe      ! // OUTPUT // stomatal water stress average during the cut (cut crop reproductive phase)  // 0-1
  real(real32),    intent(INOUT) :: stu2intercoupe      ! // OUTPUT // turgescence water stress average during the cut (cut crop reproductive phase)  // 0-1
  real(real32),    intent(INOUT) :: inn2intercoupe      ! // OUTPUT // nitrogen stress (inn) average during the cut (cut crop reproductive phase)  // 0-1
  real(real32),    intent(INOUT) :: diftemp2intercoupe      ! // OUTPUT // mean difference between crop surface temperature and air temperature during the cut (cut cropreproductive phase) // degree C

! variables locales
  integer :: kcoupe

      if (numcoupe == 1) then
        do kcoupe = 1,nbcoupe
          !if (udevlaires(kcoupe) == 0 .and. lai >= P_lairesiduel(kcoupe)) udevlaires(kcoupe) = somcour
          if (abs(udevlaires(kcoupe)).lt.1.0E-8 .and. lai >= P_lairesiduel(kcoupe)) udevlaires(kcoupe) = somcour
        end do
      endif

      if (nlev > 0 .and. (nlax == 0 .or. n == nlax)) then
        nst1coupe = nst1coupe + 1
        str1coupe = str1coupe + swfac
        stu1coupe = stu1coupe + turfac
        inn1coupe = inn1coupe + inns
        diftemp1coupe = diftemp1coupe + tcult - tmoy
        str1intercoupe = str1coupe / nst1coupe
        stu1intercoupe = stu1coupe / nst1coupe
        inn1intercoupe = inn1coupe / nst1coupe
        diftemp1intercoupe = diftemp1coupe / nst1coupe
      endif

      if (nlax > 0 .and. (nmat == 0.or.n == nmat)) then
        nst2coupe = nst2coupe + 1
        str2coupe = str2coupe + swfac
        stu2coupe = stu2coupe + turfac
        inn2coupe = inn2coupe + inns
        diftemp2coupe = diftemp2coupe + tcult - tmoy
        str2intercoupe = str2coupe / nst2coupe
        stu2intercoupe = stu2coupe / nst2coupe
        inn2intercoupe = inn2coupe / nst2coupe
        diftemp2intercoupe = diftemp2coupe / nst2coupe
      endif

return
end subroutine entreLesCoupes
end module entreLesCoupes_m
