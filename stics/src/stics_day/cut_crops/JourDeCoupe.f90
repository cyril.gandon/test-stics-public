module JourDeCoupe_m
use phenology_utils
use messages
use stics_files
USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
USE Station
USE Sol
USE Bilans
use Ecriture_Rapport_m, only: Ecriture_Rapport
use ApportFauche_m, only: ApportFauche
use mulch_m, only: mulch
use restitution_pature_uree_m, only: restitution_pature_uree
use restitution_pature_organique_m, only: restitution_pature_organique
implicit none
private
public :: JourDeCoupe
contains
subroutine JourDeCoupe(logger,sc,pg,soil,p,itk,t,sta,c,ipl)
   type(logger_), intent(in) :: logger
   integer, intent(in) :: ipl
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT) :: pg
  type(Plante_),              intent(INOUT) :: p  
  type(ITK_),                 intent(INOUT) :: itk  
  type(Stics_Transit_),       intent(INOUT) :: t  
  type(Sol_),                 intent(INOUT) :: soil
  type(Station_),             intent(INOUT) :: sta
  type(Climat_),              intent(INOUT) :: c

  integer :: i, jj, ens, ires, numfauche
  real    :: cumdeltai, cumdeltams, NC
  real    :: varintlai(sc%n), varintms(sc%n)
  integer :: k   ! , aoas, as, ao
!  real    :: masecavantfauche, QNplanteavantfauche
  real    :: save_tempfauche(0:20)
  real    :: pfeuilvavantfauche   ! Loic Juin 2016: proportion de feuilles vertes avant la fauche
  real    :: CNvegstruc           ! N concentration in structural part of vegetative organs
  real    :: Cfeuil               ! C content in leaves
  real    :: CNfeuil
  real    :: masecexportable, QNexportable
  integer :: nbresid

  !aoas = AOAS
  !as = AS
  !ao = AO

! Loic Avril 2021 : ajout enregistrement de la biomasse de fourrage recoltee pour ISOP
          if (t%P_code_ISOP == 1) then
            p%msrec_fou_coupe = p%msrec_fou_coupe + p%msrec_fou
          endif
! Sauvegarde du numero de coupe avant qu'il soit change
          numfauche = p%numcoupe

          pfeuilvavantfauche = p%mafeuilverte(AOAS)/p%masecnp(aoas,sc%n)
          CNvegstruc = p%QNvegstruc / (p%masecnp(AOAS,sc%n) - p%restemp(AOAS))
          CNfeuil = p%QNfeuille / p%mafeuil(AOAS)

          if (itk%P_codemodfauche == 2) then
            if (sc%n > p%nfauche(p%numcoupe+1) .and. p%nfauche(p%numcoupe+1) > 0) then
              do  jj = p%numcoupe,itk%nbcoupe
                p%nfauche(jj) = p%nfauche(jj+1)
              enddo
              return
            endif
          endif

   !dr et FR 25/06/2015 on ne veux pas supprimer la coupe 1 initiale en cas de repousse de fauche de l'annee d'avant
   ! donc on decale les coupes d'apres
        if (itk%P_codemodfauche == 3) then
            if(sc%numcult.gt.1 .and. p%fauchediff .and. p%numcoupe == 1) then
                save_tempfauche = p%tempfauche_ancours
            else
                if(p%fauchediff) then
                    p%tempfauche_ancours(p%numcoupe) = p%somcourfauche
                    p%tempfauche_ancours(p%numcoupe+1) = p%somcourfauche + itk%P_tempfauche(p%numcoupe+1)
                else
                    p%tempfauche_ancours(p%numcoupe+1) = p%somcourfauche + itk%P_tempfauche(p%numcoupe+1)!!DR 11/06/2016
                endif
            endif
        endif

          p%som_vraie_fauche(p%numcoupe) = p%somcourfauche
        ! on garde memoire de la date de fauche
          p%nfauche(p%numcoupe) = sc%n
       ! domi le 23/01/98 : les dates de fauches sont dans le calendrier hydrique
       ! faut faire attention le jour de la coupe de ne pas ecrire le jour d'avant
          p%day_cut = p%nfauche(p%numcoupe) + sc%P_iwater - 1 ! TODO: tCal1J...

        !: modif domi 25-08-97  pour les bilans intercoupe
        !- on soustrait le cumul de la coupe d'avant
          p%cescoupe      = p%ces      - p%cescoupe
          p%cepcoupe      = p%cep      - p%cepcoupe
          p%cetmcoupe     = p%cetm     - p%cetmcoupe
          p%cetcoupe      = p%cet      - p%cetcoupe
          p%cprecipcoupe  = p%cprecip  - p%cprecipcoupe

 ! Modif Bruno avril 2018 : on n'energsitre la coupe dans rapport que si le stade = 'cut'

          if (iand(pg%P_flagEcriture, ECRITURE_RAPPORTS) > 0 .and. sc%rapcut) then
            call Ecriture_Rapport(logger,sc,pg,soil,c,sta,p,t, .FALSE., ipl)
          end if

        ! le repoussement de la date de fauche est fini
          p%fauchediff = .FALSE.


          ! DR 04/04/2016 on ecrase anitcoupe si c'est une pature   1=pature, 2=fauche
          ! DR 30/11/2016 on fait les apports dans tous les cas pature ou pas

          sc%anit(sc%n+1) = sc%anit(sc%n+1) + itk%P_anitcoupe(p%numcoupe)
          sc%anit_engrais(sc%n+1) = sc%anit(sc%n+1)
          ! Modif Loic Juin 2021 : le type d'engrais est d�sormais renseign� pour chaque coupe
          ! sc%type_ferti(sc%n+1) = itk%P_engrais(1)
          sc%type_ferti(sc%n+1) = itk%P_engraiscoupe(p%numcoupe)

! 11/07/2016 DR et FR on affecte mscoupemini_courant pour' la coupe d'apres
!         if( itk%flag_pature)then
          itk%mscoupemini_courant = itk%P_mscoupemini(p%numcoupe)
!         else
!             itk%mscoupemini_courant = itk%P_mscoupemini(1)
!         endif

! Modif Loic Juin 2016: ajout d'une option pour determiner la biomasse residuelle a partir de la hauteur de fauche (P_Hautfauche)
! Modif Loic Novembre 2020 : msres calcule dans gestion des coupes donc on ne fait que le lire ici
!          if (t%P_code_hautfauche_dyn == 2) then
!             p%msres(AOAS) = itk%P_msresiduel(numfauche) ! on reinitialise toutes les composantes de msres (AOAS, AO & AS)
!             p%msres(AS) = itk%P_msresiduel(numfauche)
!             p%msres(AO) = itk%P_msresiduel(numfauche)
!          else
!             p%msres(AOAS) = t%P_Hautfauche * p%P_coefmshaut
!             p%msres(AS) = t%P_Hautfauche * p%P_coefmshaut
!             p%msres(AO) = t%P_Hautfauche * p%P_coefmshaut
!          endif
           p%msres(AOAS) = itk%P_msresiduel(numfauche) ! on reinitialise toutes les composantes de msres (AOAS, AO & AS)
           p%msres(AS) = itk%P_msresiduel(numfauche)
           p%msres(AO) = itk%P_msresiduel(numfauche)
! Fin modif Loic Novembre 2020

          if (p%P_codedyntalle == 1) then
             p%matigestruc(AS) = p%msres(AS) / (1.+(1./p%P_tigefeuilcoupe))
             p%matigestruc(AO) = p%msres(AO) / (1.+(1./p%P_tigefeuilcoupe))
          else
             p%matigestruc(AS) = p%msres(AS) / (1.+(1./p%P_tigefeuil(itk%P_variete)))
             p%matigestruc(AO) = p%msres(AO) / (1.+(1./p%P_tigefeuil(itk%P_variete)))
          endif

          p%somcour = p%udevlaires(numfauche)
          p%somcourdrp = p%udevlaires(numfauche)
          if (p%namf /= 0 .and. (p%ndrp == 0 .or. p%nflo == 0)) then
             p%somcourdrp = p%somcourdrp - p%upvt(sc%n)
             p%onarretesomcourdrp = .TRUE.
          endif

          p%P_stlevamf(itk%P_variete) = p%stlevamf0
          p%P_stamflax(itk%P_variete) = p%stamflax0
          p%P_stlaxsen(itk%P_variete) = p%stlaxsen0
          p%P_stsenlan(itk%P_variete) = p%stsenlan0
          p%P_stlevdrp(itk%P_variete) = p%stlevdrp0

        ! Modif Loic Avril 2016: maintenant on utilise masecnp qui represente la biomasse aerienne.
        ! On met a jour la les differents compartiments qui constituent la biomasse aerienne.
        ! On calcule une biomasse de feuilles vertes residuelle afin de determiner le LAI residuel.
          p%masecavantfauche = p%masecnp(AOAS,sc%n)
        ! Loic Oct 2020 :
          p%masec(:,:) = 0.
          p%masecnp(:,:) = 0.

          if (itk%P_code_hautfauche_dyn == 2 .and. p%P_code_acti_reserve == 2) then
             p%masecnp(:,sc%n) = itk%P_msresiduel(numfauche)
             p%masecveg(:) = itk%P_msresiduel(numfauche)
             p%lai(:,sc%n) = itk%P_lairesiduel(numfauche)
             ! Loic oct 2020 : a la recherche de la v9 perdue...
!             p%restemp(:) = p%preserve(:) * itk%P_msresiduel(numfauche)
          else
             p%masecnp(:,sc%n) = p%msres(:)
             p%masecveg(:) = p%msres(:)
! Calcul de la biomasse de feuilles vertes restant apres la fauche:
! On retire les feuilles jaunes et les feuilles tombees ainsi que la biomasse remobilisee durant la senescence:
! ce sont les feuilles a la base de la plante qui senescent en premier
             p%mafeuilverte(:) = (p%msres(:) * pfeuilvavantfauche) - (p%mafeuiltombefauche + p%cumdltaremobsen + p%mafeuiljaune(:))
             if (p%mafeuilverte(AOAS) <= 0.) p%mafeuilverte(:) = 0.
             p%mafeuiljaune(:) = p%msres(:) * p%pfeuiljaune(:)
             p%mafeuil(:) = p%mafeuiljaune(:) + p%mafeuilverte(:)
             if (p%mafeuilverte(AOAS) > 0.) then
                p%restemp(:)  = p%P_propres * p%msres(:)
             else
                p%restemp(:) = 0.
             endif
             p%matigestruc(:) = p%msres(:) - p%mafeuil(:) - p%restemp(:)
! LAI restant apres la fauche
             p%lai(:,sc%n) = (p%P_slamax(itk%P_variete) * p%mafeuilverte(:)) / 100.
          endif
! MS recoltable si on exporte tout
          masecexportable = p%masecavantfauche - p%masecnp(aoas,sc%n)
! MS effectivement recoltee
          p%mafauche = masecexportable * itk%P_tauxexportfauche(numfauche)
          p%mafauchetot = p%mafauchetot + p%mafauche
! Bruno octobre 2018 : ajout de rendementsec qui est la production totale de la prairie
          p%rendementsec = p%mafauchetot

        ! Commentaire Loic Septembre 2016 :
        ! Il faudrait enregistrer l'age des feuilles au moment de la fauche... Tout au moins pour la luzerne.
        ! Je trouve ennuyeux de prolonger la duree de vie des feuilles de la luzerne apres la fauche...
        ! Quid de la fetuque ?
          ! DR 25/06/2020 si on a qu'une plante on affecte on ne fait les calculs que sur AOAS et AO sinon on a masec(AO et AS) =0 a cet endroit
          do ens = as,ao
             cumdeltai = 0.
             cumdeltams = 0.
             do i = 1, sc%n
                varintlai(i) = 0.
                varintms(i) = 0.
                cumdeltai = cumdeltai + p%deltai(ens,i)
                cumdeltams = cumdeltams + p%dltams(ens,i)
                if (cumdeltai > p%lai(ens,sc%n)) p%deltai(ens,i) = 0.
                if (cumdeltams > p%masecnp(ens,sc%n)) p%dltams(ens,i) = 0.
             end do
             ! Loic sept 2020 : correction bug identifi� par Domi, j'enregistre la partition de la biomasse
             ! vers les feuilles vertes pour les calculs de la biomasse s�nescente de feuilles apres une fauche
             ! Je mets sous condition mais je pense qu'il y a un probleme dans la v9
             ! Loic juin 2021 : tout ceci n'est de toutes fa�ons pas bon je desactive les modifs pour le moment a cause de NaN
             k = 0
             do i = 1, sc%n
               if (p%deltai(ens,i) > 0.) then
                  k = k+1
                  varintlai(sc%n-k+1) = p%deltai(ens,i)
                  varintms(sc%n-k+1) = p%dltams(ens,i)
                  ! if (p%P_code_acti_reserve == 1) varintpf(sc%n-k+1) = p%pfeuilverte(ens,i)
               endif
             end do

             do i = 1, sc%n
                p%deltai(ens,i) = varintlai(i)
                p%dltams(ens,i) = varintms(i)
                ! if (p%P_code_acti_reserve == 1) p%pfeuilverte(ens,i) = varintpf(i)
             end do
          end do

! EC 07/08/2012 Pour le calcul de la quantite de N exportee a la coupe
          p%QNplanteavantfauche = p%QNplantenp(aoas,sc%n)

          if (itk%P_code_hautfauche_dyn == 2) then
             if (itk%P_msresiduel(numfauche) <= p%P_masecNmax) then
                NC = p%P_adil * p%P_masecNmax**(-p%P_bdil)
             else
                NC = p%P_adil * itk%P_msresiduel(numfauche)**(-p%P_bdil)
             endif
             p%CNplante(ao) = p%inn(ao) * NC
             p%CNplante(as) = p%inn(as) * NC
             p%CNplante(aoas) = p%inn(aoas) * NC
          endif

        ! calcul de QNplante
        ! Loic Juin 2016: ajout d'un calcul pour la hauteur de fauche
          if (itk%P_code_hautfauche_dyn == 2) then
             p%QNplantenp(as,sc%n) = p%CNplante(as) * itk%P_msresiduel(numfauche) * p%surf(as) * 10.
             p%QNplantenp(ao,sc%n) = p%CNplante(ao) * itk%P_msresiduel(numfauche) * p%surf(ao) * 10.
          else
             p%QNplantenp(as,sc%n) = p%CNplante(as) * p%masecnp(as,sc%n) * p%surf(as) * 10.
             p%QNplantenp(ao,sc%n) = p%CNplante(ao) * p%masecnp(ao,sc%n) * p%surf(ao) * 10.
          endif
          p%QNplantenp(aoas,sc%n) = p%QNplantenp(ao,sc%n) + p%QNplantenp(as,sc%n)
          ! Loic Oct 2020 : mise a jour de QNplante comme dans la v9
          if (p%P_code_acti_reserve == 2) then
              p%QNplante = p%QNplantenp(aoas,sc%n)
              p%QNperenne(aoas) = 0.
              p%QNperenne(as) = 0.
              p%QNperenne(ao) = 0.
          endif
! Modif Loic Juin 2016: Mise a jour des pools d'azote dans la plante lors de la fauche
!   On considere que les reserves temporaires sont contenues majoritairement dans les feuilles = RuBisCO,
!   d'ou la ponderation par la proportion de feuilles vertes pour le calcul de QNrestemp

! modif Bruno : pour prendre en compte les 2 options de code_hautfauche_dyn
          if (p%P_code_acti_reserve == 1) then
!         if (p%P_code_acti_reserve == 1 .and. t%P_code_hautfauche_dyn == 1) then
!            p%QNrestemp = p%QNplantenp(aoas,sc%n) * p%pNrestemp * p%mafeuilverte(aoas)/p%masecnp(aoas,sc%n)
             p%QNrestemp = p%QNplantenp(aoas,sc%n) * p%pNrestemp * p%mafeuilverte(aoas)/p%masecavantfauche
             if (p%QNrestemp == 0.) then
                p%QNplantenp(as,sc%n)   = CNvegstruc * p%masecnp(as,sc%n) * p%surf(as)
                p%QNplantenp(ao,sc%n)   = CNvegstruc * p%masecnp(ao,sc%n) * p%surf(ao)
                p%QNplantenp(aoas,sc%n) = p%QNplantenp(ao,sc%n) + p%QNplantenp(as,sc%n)
             endif
             p%QNvegstruc = p%QNplantenp(aoas,sc%n) - p%QNrestemp
             if (p%QNrestemp == 0.) then
                p%QNtige = p%QNvegstruc
                p%QNfeuille = 0.
             else
                p%QNfeuille = p%mafeuil(aoas) * CNfeuil
                p%QNtige = p%QNvegstruc - p%QNfeuille
             endif
             p%QNtot = p%QNplantenp(aoas,sc%n) + p%QNperenne(aoas) + p%QNrac
             p%QNplante = p%QNplantenp(aoas,sc%n) + p%QNperenne(aoas)
          endif

          ! Loic Oct 2020 : dernier bug v10 v9 ?
          if (p%P_code_acti_reserve == 1) p%QNgrain = 0.

          p%QNveg = p%QNplantenp(aoas,sc%n)
! Ajout Loic septembre 2016 : j'enregistre l'azote structural et la biomasse structurale
! de la souche pour le calcul de l'azote et de la biomasse residuels dans le cas d'un gel lethal
          Cfeuil = 0.44
          p%QNresgel(:) = p%QNvegstruc - p%mafeuilverte(:) * Cfeuil * p%P_parazofmorte / t%innlax
          p%msresgel(:) = p%matigestruc(:)
          p%Qfix(:) = 0.
! N recoltable si on exporte tout
          QNexportable = p%QNplanteavantfauche - p%QNplantenp(aoas,sc%n)
! N effectivement recolte
          p%QNfauche = QNexportable * itk%P_tauxexportfauche(numfauche)
          p%QNfauchetot = p%QNfauchetot + p%QNfauche
          p%QNexport = p%QNfauchetot ! Loic : pour le calcul du surplus azote
! Biomasse (et N) fauchee non recoltee
          p%qressuite =  masecexportable * (1. - itk%P_tauxexportfauche(numfauche))
          p%QNressuite = QNexportable    * (1. - itk%P_tauxexportfauche(numfauche))

! Ecriture du bilan intercoupe
          if (iand(pg%P_flagEcriture, ECRITURE_BILAN) > 0) then
            do i = 1, sc%P_nbplantes
               call ecrireBilanIntercoupe(sc,pg,p,itk)
            end do
          endif
! Bruno juin 2017 pas besoin de rapport intermediaire, il est fait a la fin de l'usm
!          if(numfauche == itk%nbcoupe) then
!            if ( flag_rapport ) call Ecriture_Rapport(sc,pg,soil,c,sta,p,t)
!          endif
! apport au sol de la biomasse fauchee non recoltee
          if(p%qressuite > 1.e-6) then
             ires = 2
             call ApportFauche(logger, p%qressuite, p%QNressuite, pg%P_CNresmin(ires), pg%P_CNresmax(ires), pg%P_Qmulchdec(ires),&
                     pg%P_awb(ires), pg%P_bwb(ires), pg%P_cwb(ires), pg%P_akres(ires), pg%P_bkres(ires), pg%P_ahres(ires),  &
                     pg%P_bhres(ires), sc%Wb(ires),  sc%kres(ires), sc%hres(ires), pg%P_CroCo(ires), sc%Cres(1,ires),       &
                     sc%Nres(1,ires), sc%Cnondec(ires), sc%Nnondec(ires), sc%QCapp, sc%QNapp,     &
                     sc%QCresorg, sc%QNresorg, p%QCressuite, p%CsurNressuite, p%qressuite_tot, p%QNressuite_tot,            &
                     p%QCressuite_tot, sc%Chum(1), sc%Nhum(1), sc%Chumi, sc%Nhumi, soil%CsurNsol, sc%Chuma, sc%Nhuma)

! Mise a jour du mulch
             nbresid = (pg%nbResidus-1)/2
             call mulch(nbresid, sc%Cres(1,1:nbresid), sc%Nres(1,1:nbresid), sc%Cbio(1,1:nbresid),   &
                        sc%Nbio(1,1:nbresid),  sc%Cnondec(1:nbresid), sc%Nnondec(1:nbresid), sc%Cmulchnd, sc%Nmulchnd, &
                        sc%Cmulchdec, sc%Nmulchdec, sc%Cmulch, sc%Nmulch, sc%Cbmulch, sc%Nbmulch, sc%qmulch)
          endif
! Initialisations (deplacees)
          p%gel1 = 1.
          p%gel2 = 1.
          p%gel3 = 1.

          p%inns(as) = p%inns(aoas)
          p%swfac(as) = p%swfac(aoas)
          p%turfac(as) = p%turfac(aoas)
          p%pgrain(as) = p%pgrain(aoas)
          p%teaugrain(as) = p%teaugrain(aoas)

          if (p%surf(ao) > 0.) then
             p%inns(ao) = p%inns(aoas)
             p%swfac(ao) = p%swfac(aoas)
             p%turfac(ao) = p%turfac(aoas)
             p%pgrain(ao) = p%pgrain(aoas)
             p%teaugrain(ao) = p%teaugrain(aoas)
          else
             p%inns(ao) = 0.
             p%swfac(ao) = 0.
             p%turfac(ao) = 0.
             p%pgrain(ao) = 0.
             p%teaugrain(ao) = 0.
          endif
        ! on remet a zero les compteurs du stress
          p%nst1coupe = 0
          p%str1coupe = 0.
          p%stu1coupe = 0.
          p%inn1coupe = 0.
          p%diftemp1coupe = 0.
          p%nst2coupe = 0
          p%str2coupe = 0.
          p%stu2coupe = 0.
          p%inn2coupe = 0.
          p%diftemp2coupe = 0.

! Fin modifs et ajouts Loic

          p%cumdltares(:) = 0.
          p%masecneo(:) = 0.
          p%msresjaune(:) = 0.
          p%msneojaune(:) = 0.
          p%mafeuiljaune(:) = 0.

        !DR 23/06/2020 ca donne des ecarts avec la v9.0 on teste de le mettre a 0 comme dans la v9
        ! Loic sept 2020 : j'utilise le code acti reserve car pour moi c'est un bug de la v9
          if (p%P_code_acti_reserve == 1) then
            p%mafeuilp(:) = p%mafeuil(:)    ! Loic: prendre la valeur de mafeuil residuelle laissee apres la fauche
          else
            p%mafeuilp(:) = 0.
          endif
          p%matigestrucp(:) = p%matigestruc(:)
          p%magrain(:,sc%n) = 0.
          p%magrain(:,sc%n-1) = 0.
          p%CNgrain = 0.

          p%nsencour = 1
          p%nsencourpre = sc%n
          p%masecpartiel(as) = 0.
          p%masecpartiel(ao) = 0.

        ! on veut des cumuls inter-coupe (ces,cet,cep, ...): on conserve le cumul de la coupe d'avant
          p%cescoupe = p%ces
          p%cepcoupe = p%cep
          p%cetmcoupe = p%cetm
          p%cetcoupe = p%cet
          p%cprecipcoupe = p%cprecip
          p%nbrepoussefauche = 0

          p%dltamstombe(:) = 0.
          p%dltaisen(:) = 0.
          p%laisen(:,sc%n) = 0.
          p%pdsfruit(:,:) = 0
          p%nbfruit = 0.
          p%cumrg = 0.

          p%mafeuiltombefauche = 0.
          ! Loic sept 2020 : ajout code_acti_reserve, ces variables n'�taient pas r�initialisees le jour
          ! de la coupe dans la version 9
          if (p%P_code_acti_reserve == 1) then
              p%nstopres = 0
              p%mafruit = 0.
              p%pdsfruittot = 0.
              p%chargefruit = 0.
              p%mafeuiltombefauche = 0.
              p%cumdltaremobsen = 0.
              p%dltaremobsen = 0.
              p%maenfruit = 0.
              p%fpft = 0.
              p%fpv = 0.
              p%nfruit(AOAS,p%P_nboite) = 0.
              p%dltags = 0.
              p%spfruit = 0.
              p%splai = 0.
              p%eai = 0.
              p%somtemprac = 0.
              p%codeinstal = 1
          endif

          p%numcoupe = p%numcoupe + 1
        ! On reinitialise les stades, on repart comme pour une levee
          p%nplt = sc%n
          p%nlev = sc%n
        ! 05/05/2015 DR et FR On ne veut pas repartir a un stade anterieur au stade initial
          if(p%P_stade0.eq.amf)then
              p%namf = sc%n
          else
              p%namf = 0
          endif
          p%nlax = 0
          p%ndrp = 0
          p%nsen = 0
          p%nlan = 0
          p%nmat = 0
          p%nrec = 0
          p%nnou = 0
          if (p%P_code_acti_reserve == 1) then
              p%nflo = 0
              p%ndebdes = 0
          endif
 ! DR 30/03/2016 on va prendre en compte les restitutions d'uree pour le jour d'apres
          if(itk%flag_pature)then
             if(itk%P_restit(p%numcoupe-1).eq.1) then
                 sc%flag_onacoupe=.TRUE.
                 call restitution_pature_uree(p%CNplante_veille, p%msrec_fou, t%P_coef_calcul_doseN, t%P_pertes_restit_ext,&
                                              sc%anit_uree(sc%n+1))
                 if(sc%anit_uree(sc%n+1).gt.0.)then
                      ! on garde anit_uree idependant puisque pas du meme type que les apports par engrais(anit et anicoupe)
                       itk%napN_uree=itk%napN_uree+1
                       itk%napN=itk%napN+1
                 endif
                 itk%P_nbjres=itk%P_nbjres+1
                 call restitution_pature_organique(p%CNplante_veille, p%msrec_fou, t%P_pertes_restit_ext,  &
                     t%P_coderes_pature, t%P_Crespc_pature, t%P_Nminres_pature, t%P_eaures_pature, t%P_coef_calcul_qres, &
                     itk%P_qres(itk%P_nbjres), itk%P_CsurNres(itk%P_nbjres), itk%P_coderes(itk%P_nbjres),   &
                     itk%P_Crespc(itk%P_nbjres), itk%P_Nminres(itk%P_nbjres), itk%P_eaures(itk%P_nbjres))

                 p%numjres(itk%P_nbjres) = sc%n+1
                 sc%CsurNres_pature = itk%P_CsurNres(itk%P_nbjres)
                 sc%qres_pature = itk%P_qres(itk%P_nbjres)
             endif
          endif
 return
end subroutine JourDeCoupe
end module JourDeCoupe_m
