! *-----------------------------------------------------------------* c
! *   fonction de simulation de la dynamique des talles d'un couvert* c
! *   prairial par une approche stochastique basee sur le           * c
! *   fonctionnement hydrique du couvert et en particulier          * c
! *   des apex fonctionnels a l'interieur des gaines                * c
! *   auteurs :
! *   Francois Lelievre, Sylvain Satger, Francoise Ruget & Nadine Brisson
! *   le 08/03/2007
! *   parametres a indicer sur les plantes
!     P_SurfApex : surface equivalente des apex transpirants
!     P_SeuilMorTalle : seuil de transpiration relative en deca duquel meurent les talles
!     cteloigamma : Changement de signification: parametre 1 servant a definir les parametres de la loi gamma
!                   Si paramgamma=1, cteloigamma=CV
!                   Si paramgamma=2,
!                   Si paramgamma=3, cteloigamma=sigma
!                  (avant: CV de la loi gamma de la distribution des talles en fonction de leur transpiration relative)
!     cteloigamma2
!     P_VitReconsPeupl: vitesse thermique de reconstitution du peuplement de talles
!     P_SeuilReconsPeupl: seuil de densite en dessous duquel le peuplement ne pourra pas se reconstituer en totlite
!     P_MaxTalle : peuplement maximal en nombre de talles/m2
!     variables d'entree
!     densite : densite de talles courante
!     densitemax : densite potentielle de reconstitution du peuplement a affecter des la
!     premiere mortalite de talles
!     transpi(generique) c'est-e-dire soit epC2 : transpiration(mm) soit et : evapotranspiration de l'ensemble du couvert (sol +plantes)
!     transpipot(generique) c'est-e-dire soit eopC : transpiration maximale(mm) soit etm : evapotranspiration potentielle du couvert
!     mortalle : nombre de talles disparues ce jour
!     LAIC(ipl,n)
!     tempeff : temperature efficace journali2re
! *-----------------------------------------------------------------* c
!
! DR introduction des modifs de Sylvain le 23/05/08
! *************************************************
! 15/06/09
! Introduction des modifs de Sylvain Satger
! on a recupere dynamictalle en entier
! *************************************************
module dynamictalle_new_m
use stdlib_specialfunctions_gamma, only: regularized_gamma_p
implicit none
private
public :: dynamictalle_new
contains
subroutine dynamictalle_new(P_SurfApex,P_SeuilMorTalle,P_SigmaDisTalle,P_VitReconsPeupl,  &
                        P_SeuilReconsPeupl,P_MaxTalle,densite,transpi,          &
                        transpipot,LAIapex,P_SeuilLAIapex,tempeff,mortalle,   &
                        deltainet,reserve,mortrestalle,deltai,lain,         &
                        densitemax,masecnp,mortmasec,drlsenmortalle)

  
!: Arguments

  real , intent(IN) :: P_SurfApex  ! // PARAMETER // equivalent surface of a transpiring apex // mm2 // PARAMV6/PLT // 1
  real , intent(IN) :: P_SeuilMorTalle  ! // PARAMETER // relative transpiring threshold to calculate tiller mortality // mm // PARAMV6/PLT // 1
  real , intent(IN) :: P_SigmaDisTalle  ! // PARAMETER = P_SigmaDisTalle// parameter of the gamma function // mm // PARAMV6/PLT // 1
  real , intent(IN) :: P_VitReconsPeupl  ! // PARAMETER // thermal time for the regeneration of the tiller population // nb tillers/degree C/mm2 // PARAMV6 // 1
  real , intent(IN) :: P_SeuilReconsPeupl  ! // PARAMETER // tiller density threshold below which the entire population won't be regenerated // nb tillers/m2 // PARAMV6/PLT // 1
  real , intent(IN) :: P_MaxTalle  ! // PARAMETER // maximal density of tillers/m2 // Nb tillers/ // PARAMV6/PLT // 1
  real , intent(INOUT) :: densite      ! // OUTPUT // Actual sowing density // plants.m-2
  real , intent(IN) :: transpi
  real , intent(IN) :: transpipot
  real , intent(OUT) :: LAIapex
  real , intent(IN) :: P_SeuilLAIapex  ! // PARAMETER // Maximal value of LAI+LAIapex when LAIapex isn't nil // m2/m2 // PARAMV6/PLT // 1
  real , intent(IN) :: tempeff      ! // OUTPUT // Efficient temperature for growth // degree C
  real , intent(OUT) :: mortalle      ! // OUTPUT // number of dead tillers per day (// tillers.j-1
  real , intent(IN) :: deltainet
  real , intent(IN) :: reserve
  real , intent(OUT) :: mortrestalle
  real , intent(IN) :: deltai      ! // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real , intent(IN) :: lain
  real , intent(INOUT) :: densitemax
  real , intent(IN) :: masecnp      ! // OUTPUT // Aboveground dry matter  // t.ha-1
  real , intent(OUT) :: mortmasec      ! // OUTPUT // Dead tiller biomass  // t.ha-1
  real , intent(OUT) :: drlsenmortalle      ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.j-1

!: Variables locales
  real :: mu  
  real :: seuilsurscale  
  real :: shape_  
  real :: paramgamma  !  
  real ::  cteloigamma2  
  real :: cteloigamma


! DR 26/02/2019 je regarde si la fonction log_gamma est une fonction intrinseque
! oui log_gamma est egal � la fonction gammln
!  real :: toto
!  toto = log_gamma(10.)
!  write(*,*)toto
!  toto = gammln(10.)
!  write(*,*)toto

      
    ! definition des parametres de la loi gamma(shape, seuilsurscale)
      paramgamma = 1
      cteloigamma2 = -999
      cteloigamma = P_SigmaDisTalle
      if (transpipot > 0.) then
        mu = transpi/transpipot
      else
        return
      endif

    ! mortalite des talles conditionnelle a la croissance
    ! en LAI relative a l'etat du peuplement
      if (deltai <= 0.03*(densite/P_Maxtalle) .and. densite > 0.) then

    ! calcul des talles qui meurent en fonction de la loi gamma

      ! FR et SYL 091208 introduction de la possibilite de choisir un
      ! ET constant ou variant dans le sens de la moyenne ou inversement
        !if (paramgamma == 1) then
        if (abs(paramgamma-1).lt.1.0E-8) then
          shape_ = 1 / (cteloigamma**2)
          seuilsurscale = P_SeuilMorTalle / ((cteloigamma**2) * mu)
        else
          !if (paramgamma == 2) then
          if (abs(paramgamma-2).lt.1.0E-8) then
            shape_ = (mu**(cteloigamma+2)) / cteloigamma2
            seuilsurscale = (P_SeuilMorTalle * (mu**(cteloigamma+1))) / cteloigamma2
          else
! modif 09/12 correction de la fonction d'apres mail FR du 06/07/2020
!            shape_ = (mu**mu) / cteloigamma
!            seuilsurscale = (P_SeuilMorTalle * mu) / cteloigamma
            shape_ = (mu*mu) / (cteloigamma*cteloigamma)
            seuilsurscale = (P_SeuilMorTalle * mu) / cteloigamma**2
          endif
        endif

      ! SYL 060907 prise en compte de la diminution de la biomasse (mortmasec) et de la densite racinaire
      ! (drlsenmortalle) quand il y a mortalite d talles
        mortalle = regularized_gamma_p(shape_,seuilsurscale) * densite
!        write(5555,*)'mortalle',mortalle,densite

        mortrestalle = mortalle * reserve / densite
        mortmasec = mortalle * masecnp / densite
        drlsenmortalle = mortalle / densite
        densite = densite - mortalle
!        write(5555,*)'1. densite',densite
      else

      ! reconstitution de la population des talles
      ! calcul d'un maximum fonction de la densite courante
        mortalle = 0.0

      ! SYL 040907
        mortmasec = 0.0
        mortrestalle = 0.0
        drlsenmortalle = 0.0

        if (densite < P_SeuilReconsPeupl) then
          densitemax = min(densitemax, P_MaxTalle*(-8e-7*densite**2+1.9e-3*densite))
        endif

      ! reconstitution
        if (deltainet > 0.03*(densite/P_Maxtalle)) then
          densite = min(densite+P_VitReconsPeupl*densite*tempeff,densitemax)
        endif

      endif
      
    ! calcul du LAI apex
    ! NB et Sylvain 300807
      if (lain > P_SeuilLAIapex) then
        LAIapex = 0
      else
        LAIapex = min((P_SurfApex*densite),(P_SeuilLAIapex-lain))
      endif
!        write(5555,*)'2. densite',densite
return
end subroutine dynamictalle_new
end module dynamictalle_new_m      
