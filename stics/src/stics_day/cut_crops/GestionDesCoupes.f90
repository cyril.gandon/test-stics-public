module GestionDesCoupes_m
use phenology_utils
use stics_files
use messages
USE Stics
USE Parametres_Generaux
USE Plante
USE Root
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Bilans
use JourDeCoupe_m, only: JourDeCoupe
use Ecriture_DonneesFinDeCycle_m, only: Ecriture_DonneesFinDeCycle
use entreLesCoupes_m, only: entreLesCoupes
implicit none
private
public :: GestionDesCoupes
contains
subroutine GestionDesCoupes(logger,sc, pg, p, r, itk, soil, c, sta, t)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT)    :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil
  type(Climat_),              intent(INOUT) :: c
  type(Station_),             intent(INOUT) :: sta
  type(Stics_Transit_),       intent(INOUT) :: t

  integer :: ncoupedf
  integer ::  i
  integer ::  k  
  integer :: compteur, ifau
  character(:), allocatable :: path_sequence_file

    ! pour alleger l'ecriture
    !integer ::  AS, AO, AOAS
    !AS = sc%AS
    !AO = sc%AO
    !AOAS = sc%AOAS

  do i = 1,sc%P_nbplantes
    ifau = p(i)%numcoupe
    if (itk(i)%P_codefauche == 1) then
      if(sc%onafaitunecoupedelanneedavant) then
          compteur = 1
      else
          compteur = 0
      endif
      if(ifau .gt. itk(i)%nbcoupe+compteur) p(i)%somcourfauche = 0.

! DR et FR 12/07/2016 on n'est jamais en fauche repoussee uen debut d'annee
      if(sc%n.eq.1)p(i)%fauchediff=.FALSE.


      if (itk(i)%lecfauche) then   ! on dispose des dates de fauche
          if (( sc%n == p(i)%nfauche(ifau) .or. p(i)%fauchediff .or. (p(i)%somcourfauche >= p(i)%tempfauche_ancours(ifau)   &
                  !.and. p(i)%tempfauche_ancours(ifau) /= 0.))) then
                  .and. abs(p(i)%tempfauche_ancours(ifau)).gt.1.0E-8))) then
              p(i)%sioncoupe = .TRUE.
          else
              p(i)%sioncoupe = .FALSE.
          endif
      else
      ! parametrage du stade de coupe automatique NB le 20/08/97
          if (itk(i)%P_stadecoupedf == lax) ncoupedf = p(i)%nlax
          if (itk(i)%P_stadecoupedf == drp) ncoupedf = p(i)%ndrp
          if (itk(i)%P_stadecoupedf == sen) ncoupedf = p(i)%nsen
          if (itk(i)%P_stadecoupedf == lan) ncoupedf = p(i)%nlan
          if (itk(i)%P_stadecoupedf == mat) ncoupedf = p(i)%nmat
          if (itk(i)%P_stadecoupedf == rec) ncoupedf = p(i)%nrec
          if (itk(i)%P_stadecoupedf == amf) ncoupedf = p(i)%namf
          if ((sc%n /= 1 .and. sc%n == ncoupedf) .or. p(i)%fauchediff) then
              p(i)%sioncoupe = .TRUE.
          else
              p(i)%sioncoupe = .FALSE.
          endif
      endif
      if (p(i)%sioncoupe) then

      ! On ne recolte que si :
      ! - le recoltable est superieur a mscoupe_mini
      ! - le lai est superieur a lairesiduel
      ! - la biomasse est superieure a msresiduel

          if (p(i)%msrec_fou < itk(i)%mscoupemini_courant .or. p(i)%lai(0,sc%n) < itk(i)%P_lairesiduel(ifau) .or. &
              &   p(i)%masecnp(0,sc%n) < itk(i)%P_msresiduel(ifau) ) then
               p(i)%sioncoupe = .FALSE.
               p(i)%fauchediff = .TRUE.
               p(i)%nbrepoussefauche = p(i)%nbrepoussefauche+1
               ! Florent C et Fabien, Octobre 2019
               ! On met � zero mafauche et QNfauche si la fauche n'est pas possible
               p%mafauche = 0.
               p%QNfauche = 0.
               call EnvoyerMsgHistorique(logger, MESSAGE_75,sc%n)
          else
              sc%flag_onacoupe = .FALSE.
              call JourDeCoupe(logger,sc,pg,soil,p(i),itk(i),t,sta,c,i)
              path_sequence_file = join_path(logger%output_path, retrieval_file)
              call Ecriture_DonneesFinDeCycle(logger, path_sequence_file, sc, pg, p, r, itk, soil)
          endif
! 10/06/2016 DR et FR on veut pouvoir couper le dernier jour aussi si les conditions sont atteintes
        if (sc%n == sc%maxwth) then
            p(i)%P_stlevamf(itk(i)%P_variete) = p(i)%stlevamf0
            p(i)%P_stamflax(itk(i)%P_variete) = p(i)%stamflax0
            p(i)%P_stlaxsen(itk(i)%P_variete) = p(i)%stlaxsen0
            p(i)%P_stsenlan(itk(i)%P_variete) = p(i)%stsenlan0
            p(i)%P_stlevdrp(itk(i)%P_variete) = p(i)%stlevdrp0

            p(i)%hautcoupe_anterieure = itk(i)%P_hautcoupe(ifau)
            p(i)%lairesiduel_anterieure = itk(i)%P_lairesiduel(ifau)
            p(i)%msresiduel_anterieure = itk(i)%P_msresiduel(ifau)
            p(i)%anitcoupe_anterieure = itk(i)%P_anitcoupe(ifau)

            p(i)%tempfauche_realise = p(i)%tempfauche_ancours(ifau-1)
            p(i)%restit_anterieure = itk(i)%P_restit(ifau)
            p(i)%mscoupemini_anterieure = itk(i)%P_mscoupemini(ifau)

! PL, 26/11/2018 : A VOIR SI ON CONSERVE CE BLOC, EXISTANT DANS TRUNK
! COMMENTE POUR L INSTANT
! 15/04/2016 on tente de garder les variables de stades de la vegetaion a la fin de l'annee
! faudrait qu'on le fasse dans tous les cas a la fin de l'annee meme si on est pas en fauche repossee, cas ou on a pas atteint la somme
! ce cas la n'arrive que quand on est en somme
! TODO

!            p(i)%ulai0 = p(i)%ulai(sc%n)
!            p(i)%durvie0(0:2) = p(i)%durvie(0:2,sc%n)
!            p(i)%codebbch0 =  p(i)%codebbch_output
             ! DR et Fr 20/07/2016 on garde les valeurs initiales pour la prairie dans le cas ou elle meurt on va repartir sur les valeurs initiales
!            if(abs(p(i)%ulai0-3).lt.1.0E-8)then
!                  p(i)%P_stade0 =  sc%stade0_ini(i)
!                  p(i)%P_lai0 =sc%lai0_ini(i)
!                  p(i)%P_masec0 = sc%masec0_ini(i)
!                  p(i)%P_QNplante0 = sc%QNplante0_ini(i)
!                  p(i)%P_magrain0 = sc%magrain0_ini(i)
!                  p(i)%P_zrac0 = sc%zrac0_ini(i)
!                  p(i)%P_resperenne0 = sc%resperenne0_ini(i)
!                  p(i)%P_densinitial(:) = sc%densinitial_ini(i,:)
!                  p(i)%ulai0 =1
!                  sc%onreinitialise_ulai(i) = .TRUE.		  
!              endif

        endif
        ! 27/12/2016 on doit aussi ecrire la fin de bilan quand on a plus de coupes que prevues, c'est qu'on laisse pousser a la fin
         if(p(i)%fauchediff) then
               if (sc%n == p(i)%nrecbutoir) then
                    if (iand(pg%P_flagEcriture, ECRITURE_BILAN) > 0) then
                          call ecrireBilanFinCultureFauchee(sc,p(i),itk(i))
                          call ecrireBilanEauCN(sc,pg,soil,p(i),itk(i))!, t)
                    endif
                    p(i)%nbcoupe_reel = 0
                    do k = 1, itk(i)%nbcoupe
                         if (p(i)%nfauche(k) > 0) then
                            p(i)%nbcoupe_reel = p(i)%nbcoupe_reel + 1
                         endif
                    enddo
               endif
               return
        endif
      end if ! fin du sioncoupe
      call entreLesCoupes(sc%n,ifau,itk(i)%nbcoupe,p(i)%lai(AOAS,sc%n),itk(i)%P_lairesiduel(1:itk(i)%nbcoupe),   &
                          p(i)%somcour,p(i)%nlev,p(i)%nlax,p(i)%swfac(AOAS),p(i)%turfac(AOAS),p(i)%inns(AOAS),    &
                          sc%tcult,c%tmoy(sc%n),p(i)%nmat,                                                                 &
                          p(i)%udevlaires(1:itk(i)%nbcoupe),p(i)%nst1coupe,p(i)%str1coupe,p(i)%stu1coupe,p(i)%inn1coupe,   &
                          p(i)%diftemp1coupe,p(i)%str1intercoupe,p(i)%stu1intercoupe,p(i)%inn1intercoupe,                  &
                          p(i)%diftemp1intercoupe,p(i)%nst2coupe,p(i)%str2coupe,p(i)%stu2coupe,p(i)%inn2coupe,             &
                          p(i)%diftemp2coupe,p(i)%str2intercoupe,p(i)%stu2intercoupe,p(i)%inn2intercoupe,                  &
                          p(i)%diftemp2intercoupe)

        if (sc%n == sc%maxwth) then
            p(i)%P_stlevamf(itk(i)%P_variete) = p(i)%stlevamf0
            p(i)%P_stamflax(itk(i)%P_variete) = p(i)%stamflax0
            p(i)%P_stlaxsen(itk(i)%P_variete) = p(i)%stlaxsen0
            p(i)%P_stsenlan(itk(i)%P_variete) = p(i)%stsenlan0
            p(i)%P_stlevdrp(itk(i)%P_variete) = p(i)%stlevdrp0

          !DR 26/06/2015 on est en fin d'annee et on a pas coupe on stocke les prescriptions de fauche pour l'annee d'apres
            p(i)%hautcoupe_anterieure = itk(i)%P_hautcoupe(ifau)
            p(i)%lairesiduel_anterieure = itk(i)%P_lairesiduel(ifau)
            p(i)%msresiduel_anterieure = itk(i)%P_msresiduel(ifau)
            p(i)%anitcoupe_anterieure = itk(i)%P_anitcoupe(ifau)

            p(i)%tempfauche_realise = p(i)%tempfauche_ancours(ifau-1)
            p(i)%restit_anterieure = itk(i)%P_restit(ifau)
            p(i)%mscoupemini_anterieure = itk(i)%P_mscoupemini(ifau)

! 15/04/2016 on tente de garder les variables de stades de la vegetaion a la fin de l'annee
! faudrait qu'on le fasse dans tous les cas a la fin de l'annee meme si on est pas en fauche repossee, cas ou on a pas atteint la somme
! ce cas la n'arrive que quand on est en somme
! TODO
            p(i)%ulai0 = p(i)%ulai(sc%n)
            p(i)%durvie0(0:2) = p(i)%durvie(0:2,sc%n)
            p(i)%codebbch0 = p(i)%codebbch_output
        endif

    ! le jour de la recolte butoir
!      if (sc%n == p(i)%nrecbutoir .or. sc%n == sc%maxwth) then
! 01/09/2017 l'ete est fini d'apres Loic !!
      if (sc%n == sc%maxwth) then
        if (iand(pg%P_flagEcriture, ECRITURE_BILAN) > 0) then
            call ecrireBilanFinCultureFauchee(sc,p(i),itk(i))
            call ecrireBilanEauCN(sc,pg,soil,p(i),itk(i))
        endif

        p(i)%nbcoupe_reel = 0
        do k = 1, itk(i)%nbcoupe
            if (p(i)%nfauche(k) > 0) p(i)%nbcoupe_reel = p(i)%nbcoupe_reel + 1
        enddo
      endif
    endif
  end do

return
end subroutine GestionDesCoupes
end module GestionDesCoupes_m
