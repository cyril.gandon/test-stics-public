! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This subroutine calls the modules involved in allocation of assimilates
!   - densiteRacinaire.f90 : root density profile
!   - calpsibase.f90 : plant water potential
!   - repartir.f90 : dimensioning of organs
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
module allocation_m
use messages
USE Stics
USE Parametres_Generaux
USE Plante
USE Root
USE Itineraire_Technique
USE Sol
use croira_m, only: densiteracinaire
use calpsibase_m, only: calpsibase
use repartir_m, only: repartir
implicit none
private
public :: allocation
contains
subroutine allocation(logger, sc, pg, p, r, itk, soil, t)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc
  type(Parametres_Generaux_), intent(IN)    :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil
  type(Stics_Transit_),       intent(INOUT) :: t

!: Variables locales
  integer :: ens, n, i
  integer :: irac, idim, ilif, ilig
  real    :: repracmin, repracmax, kreprac
  real    :: sao, sas
  integer :: nbCou, nbCoum ! ao, as ,aoas

    !ao = sc%ao
    !as = sc%as
    !aoas = sc%aoas
    n = sc%n
    nbCou = nbCouchesSol
    nbCoum = nbCouchesSol

    do i = 1, sc%P_nbplantes
       sas = p(i)%surf(as)
       sao = p(i)%surf(ao)
        do ens = as,ao
          if (p(i)%surf(ens) > 0. .or. i>1) then ! on ne travaille que si la surface de la plante est superieur a zero.
            if (ens == as) then
              if (p(i)%P_codtrophrac == 1) then
                  repracmax = p(i)%P_repracpermax
                  repracmin = p(i)%P_repracpermin
                  kreprac = p(i)%P_krepracperm
              endif
              if (p(i)%P_codtrophrac == 2) then
                  repracmax = p(i)%P_repracseumax
                  repracmin = p(i)%P_repracseumin
                  kreprac = p(i)%P_krepracseu
              endif
              if (p(i)%P_codtrophrac == 3) then
                  repracmax = 0.
                  repracmin = 0.
                  kreprac = 0.
              endif
            endif
! Modif Loic: je deplace le calcul de chargefruit depuis sortie.f90 car on en a besoin dans la routine repartir.f90
              if (p(i)%P_codeindetermin == 1) p(i)%chargefruit = p(i)%nbgrains(aoAS)
              if (p(i)%P_codeindetermin == 2) then
                  p(i)%chargefruit = p(i)%nbfruit
                  if (pg%P_codefrmur == 1) then
                      p(i)%chargefruit = p(i)%nbfruit + p(i)%nfruit(aoAS,p(i)%P_nboite)
                  endif
              endif

               call EnvoyerMsgEcran(logger, 'avant densite racinaire')
               irac = int(p(i)%zracmax) + 1
               idim = irac                ! precedemment c'etait nbCou
  ! Modif pour Record 27/08/2018
               ilif = n + p(i)%ndecalf    ! precedemment c'etait maxdayf
               ilig = n + p(i)%ndecalg    ! precedemment c'etait maxdayg
  !            ilif = sc%maxdayf
  !            ilig = sc%maxdayg
  ! fin modif
               call densiteRacinaire(&
                 logger, n, idim, nbCoum, sc%anox(1:idim), sc%hur(1:idim), sc%humin(1:idim), sc%dacouche(1:idim),  &
                        pg%P_daseuilbas ,pg%P_daseuilhaut, pg%P_dacohes, pg%P_lvopt, p(i)%P_coderacine, p(i)%P_contrdamax,  &
                        p(i)%P_sensanox, p(i)%P_zlabour, p(i)%P_zpente, p(i)%P_zprlim, itk(i)%P_profsem, p(i)%P_codazorac,  &
                        p(i)%P_minazorac, p(i)%P_maxazorac, p(i)%P_minefnra, p(i)%P_codtrophrac, p(i)%P_coefracoupe,        &
                        p(i)%P_stamflax(itk(i)%P_variete), p(i)%P_lvfront, p(i)%P_laicomp, p(i)%P_adens(itk(i)%P_variete),  &
                        p(i)%P_bdens(itk(i)%P_variete), p(i)%P_lvmax, p(i)%P_draclong, p(i)%P_sensrsec, &
                        p(i)%P_stlevamf(itk(i)%P_variete), p(i)%P_vlaimax,                                                     &
                        p(i)%longsperacf, p(i)%longsperacg, p(i)%debsenracf,p(i)%debsenracg,p(i)%P_codedyntalle,               &
!                        p(i)%P_propracfmax, p(i)%P_zramif, p(i)%P_longsperac, itk(i)%P_msresiduel(p(i)%numcoupe), p(i)%sioncoupe,&
                        p(i)%P_propracfmax, p(i)%P_longsperac, itk(i)%P_msresiduel(p(i)%numcoupe), p(i)%sioncoupe,&
                        p(i)%codeinstal, p(i)%nger, p(i)%nlev, p(i)%nrec, soil%nit(1:idim), soil%amm(1:idim), soil%profsol, &
                        p(i)%lai(0,n), sc%tauxcouv(n), p(i)%drlsenmortalle, p(i)%densite, repracmin, repracmax, &
                        kreprac, p(i)%idzrac, p(i)%precrac(1:idim), p(i)%lai(0,n-1), p(i)%dltams(0,n), sc%nstoprac,         &
                        p(i)%somtemprac, p(i)%msrac(n-1), p(i)%zrac, p(i)%znonli, p(i)%deltaz, ilig, p(i)%dtj(1:ilig),      &
                        p(i)%zracmax, p(i)%racnoy(n), p(i)%flrac(1:idim), p(i)%lracz(1:idim), p(i)%msrac(n), p(i)%cumflrac, &
                        p(i)%efdensite_rac, p(i)%difrac, p(i)%cumlracz, p(i)%poussracmoy, p(i)%cumlraczmaxi, p(i)%dltamsrac,&
                        p(i)%nsencourpreracf, p(i)%nsencourpreracg, p(i)%ndebsenracf, p(i)%ndebsenracg, p(i)%ndecalf,       &
                        p(i)%ndecalg, ilif, p(i)%lracsentotf, p(i)%lracsentotg, p(i)%rltotf, p(i)%rltotg,                   &
                        p(i)%rlf_veille(1:idim), p(i)%rlg_veille(1:idim), p(i)%rlf(1:idim), p(i)%rlg(1:idim),               &
                        r(i)%drlf(1:idim,1:ilif), r(i)%drlg(1:idim,1:ilig), p(i)%lracsenzf(1:idim), p(i)%lracsenzg(1:idim), &
                        p(i)%drlsenf(1:idim), p(i)%drlseng(1:idim), p(i)%somtempracvie, p(i)%efda, p(i)%efnrac_mean,        &
                        p(i)%humirac_mean, p(i)%humirac(1:idim), p(i)%efnrac(1:idim), p(i)%rlj,    &
                        p(i)%dltaremobil(ens), p(i)%P_code_diff_root, p(i)%P_code_acti_reserve,       &
                        p(i)%inn(ens), p(i)%turfac(ens), p(i)%P_code_stress_root, p(i)%P_codemonocot, p(i)%P_kdisrac,          &
                        p(i)%P_codedisrac, p(i)%P_alloperirac, p(i)%P_pgrainmaxi(itk(i)%P_variete), t%P_humirac,            &
                        sc%hucc(1:idim), p(i)%masecnp(ens,n), p(i)%P_codemortalracine,p(i)%QNrac, p(i)%P_parazorac,         &
                        p(i)%P_code_rootdeposition, p(i)%urac, p(i)%msrac0, p(i)%QNrac0, t%P_code_ISOP)


               call calpsibase(p(i)%zrac, nbCou, sc%hur(1:nbCou), sc%sat(1:nbCou),sc%humin(1:nbCou), sc%hucc(1:nbCou),    &
                               sc%dacouche(1:nbCou), pg%P_psihumin, p(i)%lracz(1:nbCou), p(i)%lai(aoas,n), pg%P_psihucc,  &
                               soil%P_codefente, p(i)%psibase)

               if (p(i)%P_codelaitr == 1 .and. p(i)%nlev > 0) then
               !! merge trunk 23/11/2020
               ! DR 06/05/2020 j'ajoute la densit� pour tests sur messages dans history
               call repartir(&
                 logger, n, p(i)%nrec, itk(i)%P_codcueille, p(i)%P_codeperenne, p(i)%nlev, p(i)%nlax, itk(i)%P_nbcueille,      &
                            sc%tustress, p(i)%P_slamin, p(i)%P_slamax(itk(i)%P_variete), p(i)%P_codlainet, p(i)%P_codemonocot,     &
                            sc%P_codesimul, p(i)%dltaisen(ens),                                                                  &
                            p(i)%P_envfruit, p(i)%chargefruit, p(i)%ndrp, p(i)%ndebdes,                                            &
                            p(i)%P_sea, p(i)%ntaille, itk(i)%P_codetaille, p(i)%dltams(ens,n), p(i)%lai(ens,n-1),                  &
                            p(i)%restemp(ens), p(i)%masecveg(ens), p(i)%pdsfruittot(ens), p(i)%tursla(ens), p(i)%sla(ens),         &
                            p(i)%mafeuilverte(ens), p(i)%mafeuil(ens), p(i)%mafeuilp(ens), p(i)%lai(ens,n), p(i)%deltai(ens,n),    &
                            p(i)%maenfruit(ens), p(i)%eai(ens), p(i)%mareserve(ens), p(i)%deltares(ens), p(i)%mabois(ens),         &
                            p(i)%masec(ens,n), p(i)%msresjaune(ens), p(i)%mafeuiljaune(ens), p(i)%msneojaune(ens),                 &
                            p(i)%matigestruc(ens), p(i)%pfeuil(ens,n), p(i)%pfeuilverte(ens,n), p(i)%pfeuiljaune(ens),             &
                            p(i)%ptigestruc(ens), p(i)%penfruit(ens), p(i)%preserve(ens), p(i)%masecnp(ens,n), p(i)%maperenne(ens),&
                            p(i)%QNveg, p(i)%QNvegstruc, p(i)%QNrestemp, p(i)%matigestrucp(ens), p(i)%P_codelaitr,                 &
                            p(i)%P_hautmax(itk(i)%P_variete), p(i)%P_hautbase(itk(i)%P_variete),                                   &
                            p(i)%P_khaut(itk(i)%P_variete), p(i)%hauteur(ens), p(i)%laisen(ens,n), p(i)%nstopres,                  &
                            p(i)%dltamsen(ens), p(i)%dltaremobsen, p(i)%remobilj(ens), p(i)%P_inilai, p(i)%P_adilmax,              &
                            p(i)%QNplantenp(ens,n), p(i)%QNfeuille, p(i)%QNperenne(ens), p(i)%P_code_acti_reserve, p(i)%codeinstal,&
                            p(i)%dltags(ens), p(i)%msresgel(ens), itk(i)%P_codefauche, p(i)%ratioTF, p(i)%fstressgel,              &
                            p(i)%P_restemp0, p(i)%P_codeplante, sc%numcult, pg%P_codeinitprec, p(i)%densite)


            ! on recalcule les sommes ponderees par les surfaces ombre/soleil des variables AO/AS
              p(i)%lai(aoas,n) =        p(i)%lai(as,n) * sas        + p(i)%lai(ao,n) * sao
              p(i)%restemp(aoas) =      p(i)%restemp(ao) * sao      + p(i)%restemp(as) * sas
              p(i)%masecveg(aoas) =     p(i)%masecveg(ao) * sao     + p(i)%masecveg(as) * sas
              p(i)%pdsfruittot(aoas) =  p(i)%pdsfruittot(ao) * sao  + p(i)%pdsfruittot(as) * sas
              p(i)%tursla(aoas) =       p(i)%tursla(ao) * sao       + p(i)%tursla(as) * sas
              p(i)%sla(aoas) =          p(i)%sla(ao) * sao          + p(i)%sla(as) * sas
              p(i)%mafeuilverte(aoas) = p(i)%mafeuilverte(ao) * sao + p(i)%mafeuilverte(as) * sas
              p(i)%mafeuil(aoas) =      p(i)%mafeuil(ao) * sao      + p(i)%mafeuil(as) * sas
              p(i)%mafeuilp(aoas) =     p(i)%mafeuilp(ao) * sao     + p(i)%mafeuilp(as) * sas
              p(i)%matigestrucp(aoas) = p(i)%matigestrucp(ao) * sao + p(i)%matigestrucp(as) * sas
              p(i)%deltai(aoas,n) =     p(i)%deltai(ao,n) * sao     + p(i)%deltai(as,n) * sas
              p(i)%maenfruit(aoas) =    p(i)%maenfruit(ao) * sao    + p(i)%maenfruit(as) * sas
              p(i)%eai(aoas) =          p(i)%eai(ao) * sao          + p(i)%eai(as) * sas
              p(i)%mareserve(aoas) =    p(i)%mareserve(ao) * sao    + p(i)%mareserve(as) * sas
              p(i)%deltares(aoas) =     p(i)%deltares(ao) * sao     + p(i)%deltares(as) * sas
              p(i)%mabois(aoas) =       p(i)%mabois(ao) * sao       + p(i)%mabois(as) * sas
              p(i)%masecnp(aoas,n) =    p(i)%masecnp(ao,n) * sao    + p(i)%masecnp(as,n) * sas
              p(i)%msresjaune(aoas) =   p(i)%msresjaune(ao) * sao   + p(i)%msresjaune(as) * sas
              p(i)%mafeuiljaune(aoas) = p(i)%mafeuiljaune(ao) * sao + p(i)%mafeuiljaune(as) * sas
              p(i)%msneojaune(aoas) =   p(i)%msneojaune(ao) * sao   + p(i)%msneojaune(as) * sas
              p(i)%matigestruc(aoas) =  p(i)%matigestruc(ao) * sao  + p(i)%matigestruc(as) * sas
              p(i)%masecneo(aoas) =     p(i)%masecneo(ao) * sao     + p(i)%masecneo(as) * sas
              p(i)%msres(aoas) =        p(i)%msres(ao) * sao        + p(i)%msres(as) * sas
              p(i)%mafeuiltombe(aoas) = p(i)%mafeuiltombe(ao) * sao + p(i)%mafeuiltombe(as) * sas

              if (p(i)%masecnp(aoas,n) > 0.) then
                p(i)%ptigestruc(aoas) =    p(i)%matigestruc(aoas)  / p(i)%masecnp(aoas,n)
                p(i)%penfruit(aoas) =      p(i)%maenfruit(aoas)    / p(i)%masecnp(aoas,n)
                p(i)%preserve(aoas) =      p(i)%restemp(aoas)      / p(i)%masecnp(aoas,n)
                p(i)%pfeuil(aoas,n) =      p(i)%mafeuil(aoas)      / p(i)%masecnp(aoas,n)
                p(i)%pfeuiljaune(aoas) =   p(i)%mafeuiljaune(aoas) / p(i)%masecnp(aoas,n)
                p(i)%pfeuilverte(aoas,n) = p(i)%mafeuilverte(aoas) / p(i)%masecnp(aoas,n)
              else
                p(i)%ptigestruc(aoas) = 0.
                p(i)%penfruit(aoas) = 0.
                p(i)%preserve(aoas) = 0.
                p(i)%pfeuil(aoas,n) = 0.
                p(i)%pfeuiljaune(aoas) = 0.
                p(i)%pfeuilverte(aoas,n) = 0.
              endif
            endif ! fin test codelaitr
          endif   ! fin test surf(ens) > 0
        end do    ! fin boucle ens
      end do      ! fin boucle nbplantes

return
end subroutine allocation
end module allocation_m
 
