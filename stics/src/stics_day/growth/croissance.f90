! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This subroutine calls the main modules involved in crop growth simulation:
!   - biomaer.f90 : shoot biomass growth
!   - senescen.f90 : leaf senescence
!   - ApportFeuillesMortes.f90 : decomposition of leaves fallen at soil surface
!   - fruit.f90 : yield formation for indeterminate crops
!   - grain.f90 : yield formation for determinate crops
!   - eauqual.f90 : water content evolution in harvested organs
!   - croissanceFrontRacinaire.f90 : root front growth
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! J'enleve la structure root
module croissance_m
use messages
use string_utils, only: to_string_a
use stics_files
USE Stics
USE Parametres_Generaux
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
use biomaer_m, only: biomaer
use senescen_m, only: senescen
use ApportFeuillesMortes_m, only: ApportFeuillesMortes
use GEL_m, only: GEL
use fruit_m, only: fruit
use grain_m, only: grain
use eauqual_m, only: eauqual
use croira_m, only: croissanceFrontRacinaire
use plant_utils
implicit none
private
public :: croissance
contains
subroutine croissance(logger, sc, pg, p, itk, soil, c, sta, t)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc
  type(Parametres_Generaux_), intent(INOUT)    :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
!  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil
  type(Climat_),              intent(INOUT) :: c
  type(Station_),             intent(INOUT) :: sta  
  type(Stics_Transit_),       intent(INOUT) :: t

!: Variables locales
  integer :: ens  
  integer :: n  
  integer :: i ! indice plante  
  integer :: ic  !  
  integer :: ii  !
  integer :: iz
!  real :: repracmin, repracmax, kreprac
  real :: trg_bak, densiteassoc, sao, sas
  integer :: profhoriz  
  integer :: ires, nbCou ! ao, as, aoas

    !AO = sc%ao
    !AS = sc%as
    !AOAS = sc%aoas
    n = sc%n
    nbCou = nbCouchesSol

      ! ** on distingue la culture principale et la culture associe car les traitements sont diffrents.
      ! Pour la culture principale on a notament besoin de calculer la variable originehaut
      ! et pour la culture associe d'affecter des trg(n) rapports au rayonnement intercept par la culture principale

    if (sc%P_nbplantes > 1) then
        densiteassoc = p(1)%densite / p(1)%P_bdens(itk(1)%P_variete) * p(2)%P_bdens(itk(2)%P_variete)
    else
        densiteassoc = 0.
    endif

    do i = 1, sc%P_nbplantes
       do ens = as,ao
          sas = p(i)%surf(AS)
          sao = p(i)%surf(AO)
          if (p(i)%surf(ens) > 0. .or. i>1) then ! on ne travaille que si la surface de la plante est suprieur a zro.
            if (p(i)%P_codeplante /= CODE_BARESOIL) then
              if (i < sc%P_nbplantes) then
                sc%originehaut = max(p(i+1)%hauteur(AO),p(i+1)%hauteur(AS))
              else
                sc%originehaut = 0.
              endif
              trg_bak = c%trg(n)
              if (i > 1) c%trg(n) = trg_bak * p(i-1)%rsoleil

              call biomaer(n, i, ens, p(i)%nlev, p(i)%P_codeperenne, p(i)%nplt, p(i)%P_codehypo, p(i)%P_codegermin,             &
                           p(i)%P_masecplantule, p(i)%P_adil, p(i)%namf, p(i)%P_adilmax, p(i)%nrec, itk(i)%P_codcueille,        &
                           itk(i)%P_codefauche, p(i)%P_codelaitr, p(i)%P_codetransrad, p(i)%P_efcroijuv, p(i)%P_efcroiveg,      &
                           p(i)%ndrp, p(i)%P_efcroirepro, p(i)%chargefruit, pg%P_coefb, sc%tcult,                               &
                           p(i)%P_teopt(itk(i)%P_variete), p(i)%P_teoptbis, p(i)%P_temin(itk(i)%P_variete),                     &
                           p(i)%P_temax, sta%P_codeclichange, p(i)%P_alphaco2, c%co2(n), p(i)%P_resplmax ,p(i)%densite,         &
                           pg%P_codeh2oact, p(i)%swfac(ens), p(i)%exobiom, pg%P_codeinnact, p(i)%dltaremobil(ens),              &
                           p(i)%fpv(ens,n), p(i)%fpft(ens), p(i)%P_remobres, itk(i)%P_msresiduel(p(i)%numcoupe-1),              &
                           itk(i)%P_nbcueille,  p(i)%rdtint(ens, p(i)%nbrecolte-1), p(i)%CNgrain(ens), p(i)%P_codemontaison,    &
                           p(i)%nmontaison, p(i)%masecnp(ens,n-1), p(i)%masecnp(ens,n), p(i)%QNplantenp(ens,n-1),               &
                           p(i)%QNplantenp(ens,n), p(i)%inns(ens), p(i)%inn(ens), p(i)%innlai(ens), sc%cumdltaremobilN,         &
                           p(i)%ebmax, p(i)%ftemp, p(i)%epsib(ens), p(i)%fco2, p(i)%dltams(ens,n), p(i)%dltamsen(ens),          &
                           p(i)%dltamstombe(ens), p(i)%restemp(ens), t%dltamsN(i,ens),                                          &
                           p(i)%photnet(ens,n),p(i)%sourcepuits(ens),p(i)%dltaremobilN(ens),p(i)%remobilj(ens),                 &
                           p(i)%cumdltares(ens),p(i)%magrain(ens,n-1),p(i)%magrain(ens,n),p(i)%masecneo(ens),p(i)%surf,         &
                           p(i)%surfSous,sc%P_nbplantes,p%P_extin(itk(i)%P_variete),p(i)%cumrg,p(i)%cumraint,p(i)%fapar(ens),   &
                           sc%delta,p(i)%P_adfol,p(i)%lairognecum(ens),p(i)%laieffcum(ens),p(i)%P_dfolbas,p(i)%P_dfolhaut,      &
                           p(i)%dfol,sc%rdif,p(i)%parapluie,p(i)%raint(ens),pg%P_parsurrg,p(i)%P_forme,p(i)%lai(ens,n),         &
                           p(i)%laisen(ens,n),p(i)%eai(ens),itk(i)%P_interrang,p(i)%nlax,p(i)%nsen,p(i)%P_codlainet,            &
                           p(i)%P_hautbase(itk(i)%P_variete),itk(i)%P_codepalissage,itk(i)%P_hautmaxtec,itk(i)%P_largtec,       &
                           sc%originehaut,p(i)%hauteur(ens),p(i)%deltahauteur(ens),p(i)%P_hautmax(itk(i)%P_variete),            &
                           p(i)%varrapforme,p(i)%largeur,sc%jul,c%trg(n),sta%P_latitude,p(i)%rombre,p(i)%rsoleil,               &
                           itk(i)%P_orientrang,p(i)%P_ktrou(itk(i)%P_variete),sc%tauxcouv(n),p(i)%P_khaut(itk(i)%P_variete),    &
                           sc%surfAO,sc%surfAS,p(i)%fpari,p(i)%P_Propres,p(i)%P_PropresP,p(i)%P_tauxmortresP,p(i)%P_Efremobil,  &
                           p(i)%maperenne(ens),p(i)%mafeuil(ens),p(i)%mafeuilverte(ens),p(i)%masecveg(ens),p(i)%resperenne(ens),&
                           p(i)%maperennemort(ens), p(i)%QCperennemort(ens), p(i)%QCO2resperenne(ens), p(i)%masec(ens,n),       &
                           p(i)%QNrestemp, p(i)%QNresperenne(ens), p(i)%QNperenne(ens), p(i)%QNveg, p(i)%QNvegstruc,            &
                           p(i)%QNperennemort(ens), t%dltarestempN(i,ens), t%dltarestemp(i,ens), t%innlax,                      &
                           p(i)%dltaperennesen(ens), p(i)%dltaQNperennesen(ens), p(i)%nstopres, c%tmin(n),                      &
                           p(i)%P_tgelveg10(itk(i)%P_variete), p(i)%P_PropresPN, t%dltarespstruc(i,ens), p(i)%pNvegstruc,       &
                           p(i)%pNrestemp, p(i)%P_codeplante, p(i)%QNfeuille, p(i)%QNtige, p(i)%P_code_acti_reserve,            &
                           p(i)%ndes, c%phoi, c%phoi_veille, p(i)%P_phobase(itk(i)%P_variete), p(i)%P_codeindetermin,           &
                           p(i)%P_restemp0, p(i)%demande(ens), p(i)%P_QNperenne0, pg%P_codemsfinal, p(i)%P_codephot)

              ! on met a jour le cumul des parties AO/AS de masec, dltams calcules dans biomaer
              ! TODO: si on passe pour chaque partie AO/AS on effectue le calcul 2 fois... y'a pas mieux a faire ?
              p(i)%masecnp(0,n) = p(i)%masecnp(AS,n) * sas + p(i)%masecnp(AO,n) * sao
              p(i)%dltams(0,n)  = p(i)%dltams(AS,n) * sas  + p(i)%dltams(AO,n)  * sao
              p(i)%QNperennemort(0) = p(i)%QNperennemort(AO) * sao + p(i)%QNperennemort(AS) * sas
              p(i)%QCperennemort(0) = p(i)%QCperennemort(AO) * sao + p(i)%QCperennemort(AS) * sas
              p(i)%maperenne(0) = p(i)%maperenne(AO) * sao + p(i)%maperenne(AS) * sas
              p(i)%QNperenne(0) = p(i)%QNperenne(AO) * sao + p(i)%QNperenne(AS) * sas
              p(i)%resperenne(0) = p(i)%resperenne(AO) * sao + p(i)%resperenne(AS) * sas
              p(i)%QNresperenne(0) = p(i)%QNresperenne(AO) * sao + p(i)%QNresperenne(AS) * sas
              p(i)%dltaperennesen(0) = p(i)%dltaperennesen(AO) * sao + p(i)%dltaperennesen(AS) * sas
              p(i)%dltaQNperennesen(0) = p(i)%dltaQNperennesen(AO) * sao + p(i)%dltaQNperennesen(AS) * sas
              p(i)%maperennemort(0) = p(i)%maperennemort(AO) * sao + p(i)%maperennemort(AS) * sas

              !: On en profite pour attribuer les surfaces ombre/soleil aux plantes dominees
              !DR et ML 20/04/2016 on ne calcule les surfaces que si les 2 plantes ont leve (et pas uniquement la principale)
              ! on rajoute une condition sur la levee des 2 plantes
              
              if (sc%P_nbplantes ==2) then
                if (p(1)%nlev.gt.0 .and. p(2)%nlev.gt.0 .and. i < sc%P_nbplantes) then
                     p(i+1)%surf(AS) = p(i)%surfSous(AS)
                     p(i+1)%surf(AO) = p(i)%surfSous(AO)
                endif
              endif

              if (i > 1) c%trg(n) = trg_bak
              p(i)%gelee = .FALSE.

              !if ((p(i)%P_codelaitr == 1.and.p(i)%qressuite_tot == 0).or.(p(i)%P_codelaitr == 1.and.p(i)%P_codeplante==CODE_FODDER)) then
              if ((p(i)%P_codelaitr == 1.and.abs(p(i)%qressuite_tot).lt.1.0E-8).or.      &
                  (p(i)%P_codelaitr == 1.and.p(i)%P_codeplante==CODE_FODDER)) then
                  call EnvoyerMsgDebug(logger, 'avant senesc ' // to_string_a(p(i)%dltams(ens,1:40)))
                  call EnvoyerMsgDebug(logger, 'avant senesc ' // to_string_a(p(i)%dltams(ens,41:60)))
                call senescen(&
                  logger, p(i)%nlev, n, sc%nbjmax, p(i)%lai(ens,n), pg%P_codeinnact, pg%P_codeh2oact, p(i)%senfac(ens),&
                          p(i)%innsenes(ens), p(i)%P_codlainet, p(i)%P_codeperenne, p(i)%nbfeuille, p(i)%P_nbfgellev,          &
                          p(i)%P_codgellev, sc%tcultmin, p(i)%P_tletale, p(i)%P_tdebgel, p(i)%P_tgellev90,                     &
                          p(i)%P_tgellev10(itk(i)%P_variete), i, p(i)%densitelev, densiteassoc, p(i)%P_codgeljuv,              &
                          p(i)%P_tgeljuv90,p(i)%P_tgeljuv10(itk(i)%P_variete), p(i)%P_codgelveg, p(i)%P_tgelveg90,             &
                          p(i)%P_tgelveg10(itk(i)%P_variete), p(i)%nstopfeuille, p(i)%somcour, p(i)%restemp(ens), p(i)%ndrp,   &
                          p(i)%nrec, p(i)%ndes, pg%P_QNpltminINN, sc%numcult, pg%P_codeinitprec, p(i)%ulai(1:sc%nbjmax),       &
                          p(i)%P_vlaimax, p(i)%durvieI, p(i)%P_durvieF(itk(i)%P_variete), p(i)%inn(ens), p(i)%P_durviesupmax,  &
                          p(i)%P_codestrphot, c%phoi, p(i)%P_phobasesen, p(i)%dltams(ens,1:sc%nbjmax),                         &
                          itk(i)%P_msresiduel(p(i)%numcoupe-1), p(i)%P_ratiosen, p(i)%tdevelop(1:n), p(i)%somtemp,             &
                          p(i)%pfeuilverte(ens,1:sc%nbjmax), p(i)%deltai(ens,1:sc%nbjmax), p(i)%lai_to_sen,                    &
                          sc%dernier_n, p(i)%nsencour, p(i)%dltaisen(ens), p(i)%dltamsen(ens), p(i)%fstressgel,                &
                          p(i)%fgellev, p(i)%gelee, p(i)%densite, p(i)%laisen(ens,n-1:n), p(i)%nlan,                           &
                          p(i)%P_stsenlan(itk(i)%P_variete), p(i)%nsen, p(i)%P_stlaxsen(itk(i)%P_variete), p(i)%namf,          &
                          p(i)%nlax, p(i)%P_stlevamf(itk(i)%P_variete), p(i)%P_stamflax(itk(i)%P_variete), p(i)%nrecbutoir,    &
                          p(i)%mortplante, p(i)%nst2, p(i)%mortplanteN, p(i)%durvie(ens,1:sc%nbjmax),                          &
                          p(i)%strphot(ens), p(i)%msres(ens), p(i)%dltamsres(ens), p(i)%ndebsen, p(i)%somsenreste(ens),        &
                          p(i)%msresjaune(ens), p(i)%QNplantenp(ens,n), p(i)%P_dltamsminsen,                                   &
                          p(i)%P_dltamsmaxsen, p(i)%P_alphaphot, p(i)%strphotveille(ens), p(i)%cumdltaremobsen,                &
                          p(i)%nstopres, p(i)%dltaremobsen, p(i)%mafeuilverte(ens), p(i)%codeinstal, p(i)%P_code_acti_reserve, &
                          p(i)%lai(ens,n-1), p(i)%mafeuiljaune(ens), p(i)%nmat, p(i)%P_lai0, P(i)%P_codeplante)

                ires = 2
                ! write(711,*)'n',n
                call ApportFeuillesMortes(logger,p(i)%P_codeplante, p(i)%P_codemonocot, p(i)%P_codedyntalle, &
                           p(i)%P_code_acti_reserve, p(i)%P_abscission, p(i)%P_parazofmorte, p(i)%fstressgel,   &
                           p(i)%inn(ens), p(i)%mortmasec, p(i)%surf(ens), pg%P_CNresmin(ires), pg%P_CNresmax(ires),  &
                           pg%P_Qmulchdec(ires), p(i)%QNplantenp(ens,n), p(i)%dltamsen(ens), p(i)%dltamstombe(ens), &
                           p(i)%mafeuiltombe(ens), p(i)%QCplantetombe(ens), p(i)%QNplantetombe(ens), sc%QCapp, sc%QNapp, &
                           sc%QCresorg, sc%QNresorg, sc%Cres(1,ires), sc%Nres(1,ires), &
                           sc%Cnondec(ires), sc%Nnondec(ires), pg%P_awb(ires), pg%P_bwb(ires), pg%P_cwb(ires), pg%P_CroCo(ires), &
                           pg%P_akres(ires), pg%P_bkres(ires), pg%P_ahres(ires), pg%P_bhres(ires), sc%Wb(ires) ,sc%kres(ires), &
                           sc%hres(ires), p(i)%msneojaune(ens),  p(i)%mafeuiljaune(ens), p(i)%lai(ens,n), p(i)%laisen(ens,n), &
                           p(i)%masecnp(ens,n), p(i)%QNvegstruc, p(i)%mafeuiltombefauche, p(i)%msresgel(ens), p(i)%QNresgel(ens), &
                           p(i)%matigestruc(ens), p(i)%QNfeuille, p(i)%QNtige, p(i)%mafeuil(ens), sc%Chum(1), sc%Nhum(1),   &
                           sc%Chumi, sc%Nhumi, soil%CsurNsol, sc%Chuma, sc%Nhuma)

                p(i)%masecnp(AOAS,n) = p(i)%masecnp(AS,n) * sas + p(i)%masecnp(AO,n) * sao
                p(i)%QNplantenp(AOAS,n) = p(i)%QNplantenp(AS,n) * sas + p(i)%QNplantenp(AO,n) * sao
              endif

 ! Affectation des sensibilites au gel des fleurs et des fruits
              if (p(i)%nflo > 0 .and. p(i)%nrec == 0) then
                p(i)%fgelflo = GEL(p(i)%P_codgelflo,sc%tcultmin,p(i)%P_tletale,p(i)%P_tdebgel,p(i)%P_tgelflo90,p(i)%P_tgelflo10)
                if (p(i)%fgelflo < 1.) p(i)%gelee = .TRUE.
              endif
              if (p(i)%gelee .eqv. .TRUE.) p(i)%nbjgel = p(i)%nbjgel + 1
              if (p(i)%P_codeindetermin == 2) then

              ! on sauvegarde la valeur de nfruit
                 p(i)%nfruitv(AO,p(i)%P_nboite) = p(i)%nfruit(AO,p(i)%P_nboite)
                 p(i)%nfruitv(AS,p(i)%P_nboite) = p(i)%nfruit(AS,p(i)%P_nboite)
 ! print *,'avant fruit'
                 call fruit(&
                    logger,n, p(i)%namf, p(i)%P_codcalinflo, p(i)%P_pentinflores(itk(i)%P_variete), p(i)%densite,       &
                           p(i)%P_restemp0, p(i)%cumdltares(ens), p(i)%P_inflomax(itk(i)%P_variete), p(i)%ndrp,          &
                           itk(i)%P_nbcueille, p(i)%nrec, p(i)%P_nboite, p(i)%P_dureefruit(itk(i)%P_variete),            &
                           p(i)%fpv(ens,n), p(i)%dltams(ens,n), p(i)%remobilj(ens), p(i)%P_spfrmin, p(i)%P_spfrmax,      &
                           p(i)%P_afruitpot(itk(i)%P_variete), p(i)%upvt(n), p(i)%fgelflo, p(i)%P_codazofruit,           &
                           pg%P_codeinnact, p(i)%inns(ens), p(i)%nnou, itk(i)%P_codeclaircie, itk(i)%P_nb_eclair,        &
                           p(i)%neclair(1:itk(i)%P_nb_eclair), itk(i)%P_nbinfloecl(1:itk(i)%P_nb_eclair),                &
                           p(i)%P_codetremp, sc%tcultmin, sc%tcultmax, p(i)%P_tminremp, p(i)%P_tmaxremp, p(i)%nbrecolte, &
                           p(i)%rdtint(ens,1:p(i)%nbrecolte), p(i)%P_allocfrmax, p(i)%nmat, p(i)%P_afpf, p(i)%P_bfpf,    &
                           p(i)%P_cfpf, p(i)%P_dfpf, p(i)%pfmax, p(i)%P_nbinflo(itk(i)%P_variete),                       &
                           p(i)%nfruit(ens,1:p(i)%P_nboite), p(i)%pdsfruit(ens,1:p(i)%P_nboite),                         &
                           p(i)%fpft(ens), p(i)%sourcepuits(ens), p(i)%spfruit(ens), p(i)%nbfruit, p(i)%nfruitnou(ens),  &
                           p(i)%nbfrote, sc%devjourfr, p(i)%cumdevfr, p(i)%pousfruit(ens), p(i)%ftempremp, p(i)%nbj0remp,&
                           p(i)%dltags(ens), p(i)%frplusp(ens), p(i)%ircarb(ens,n), p(i)%magrain(ens,n-1:n),             &
                           p(i)%allocfruit(ens), p(i)%masecnp(ens,n), p(i)%compretarddrp, p(i)%pdsfruittot(ens),         &
                           p(i)%QNgrain(ens), p(i)%CNgrain(ens), p(i)%P_code_acti_reserve, p(i)%dltaremobil(ens))

                ! on met a jour le cumul des parties AO/AS de masec qui a pu etre modifie dans la routine <fruit>
                ! TODO: si on passe pour chaque partie AO/AS on effectue le calcul 2 fois... y'a pas mieux a faire ?
                 p(i)%masecnp(AOAS,n) = p(i)%masecnp(AS,n) * sas + p(i)%masecnp(AO,n) * sao

              ! TODO: Attention, il y a un pb avec ce genre de cumul, car si on le fait plusieurs fois pour une meme journee n
              !       alors on cumule plusieurs fois la meme valeur ce qui fausse les resultats.
              !       Pour l'instant, je desactive le calcul dans cumAOetAS.f90
                 p(i)%nfruit(AOAS,1:p(i)%P_nboite) = p(i)%nfruit(AOAS,1:p(i)%P_nboite)                             &
                       + (p(i)%nfruit(AO,1:p(i)%P_nboite) - p(i)%nfruitv(AO,1:p(i)%P_nboite)) * sao  &
                       + (p(i)%nfruit(AS,1:p(i)%P_nboite) - p(i)%nfruitv(AS,1:p(i)%P_nboite)) * sas

                 p(i)%pdsfruit(ens,1:p(i)%P_nboite) = p(i)%pdsfruit(AO,1:p(i)%P_nboite) * sao &
                                                    + p(i)%pdsfruit(AS,1:p(i)%P_nboite) * sas

                 p(i)%fpft(AOAS) =        p(i)%fpft(AS) * sas        + p(i)%fpft(AO) * sao
                 p(i)%sourcepuits(AOAS) = p(i)%sourcepuits(AO) * sao + p(i)%sourcepuits(AS) * sas
                 p(i)%spfruit(AOAS) =     p(i)%spfruit(AO) * sao     + p(i)%spfruit(AS) * sas
                 p(i)%nfruitnou(AOAS) =   p(i)%nfruitnou(AO) * sao   + p(i)%nfruitnou(AS) * sas
                 p(i)%pousfruit(AOAS) =   p(i)%pousfruit(AO) * sao   + p(i)%pousfruit(AS) * sas
                 p(i)%dltags(AOAS) =      p(i)%dltags(AS) * sas      + p(i)%dltags(AO) * sao

                 p(i)%ircarb(AOAS,n) = (p(i)%ircarb(AO,n) + p(i)%ircarb(AS,n)) / sc%P_nbplantes
                 p(i)%magrain(AOAS,n) =  p(i)%magrain(AO,n) * sao  + p(i)%magrain(AS,n) * sas
                 p(i)%allocfruit(AOAS) = p(i)%allocfruit(AO) * sao + p(i)%allocfruit(AS) * sas
                 ! Ajout Loic Juin2018
                 p(i)%QNgrain(AOAS) =  p(i)%QNgrain(AS) * sas +  p(i)%QNgrain(AO) * sao
                 p(i)%CNgrain(AOAS) =  p(i)%CNgrain(AS) * sas +  p(i)%CNgrain(AO) * sao

              else

                 call grain(&
                   logger, n, p(i)%ndrp, p(i)%nrec, p(i)%nlev, p(i)%nrecbutoir, p(i)%P_nbjgrain(itk(i)%P_variete),             &
                            p(i)%dltams(ens,1:p(i)%ndrp),p(i)%P_cgrain, p(i)%P_cgrainv0,                                        &
                            p(i)%P_nbgrmin(itk(i)%P_variete), p(i)%P_nbgrmax(itk(i)%P_variete), p(i)%P_codazofruit,             &
                            pg%P_codeinnact, p(i)%inns(ens), p(i)%fgelflo, p(i)%P_codeir, p(i)%P_vitircarb(itk(i)%P_variete),   &
                            p(i)%P_irmax, p(i)%P_vitircarbT(itk(i)%P_variete), p(i)%somcourdrp, p(i)%nmat,                      &
                            p(i)%masecnp(ens,n-1:n), p(i)%P_codetremp, sc%tcultmin, sc%tcultmax, p(i)%P_tminremp,               &
                            p(i)%P_tmaxremp, p(i)%P_pgrainmaxi(itk(i)%P_variete), p(i)%ircarb(ens,n-1:n), p(i)%nbgrains(ens),   &
                            p(i)%pgrain(ens), p(i)%CNgrain(ens), p(i)%vitmoy(ens), p(i)%nbgraingel, p(i)%pgraingel(ens),        &
                            p(i)%dltags(ens), p(i)%ftempremp, p(i)%magrain(ens,n-1:n), p(i)%nbj0remp, p(i)%pdsfruittot(ens))

              !DR 25/09/2012 tous ces cumuls sont a exterioriser pourquoi sont-ils la ???

                 p(i)%dltags(AOAS) =   p(i)%dltags(AS) * sas + p(i)%dltags(AO) * sao
                 p(i)%ircarb(AOAS,n) = (p(i)%ircarb(AO,n) + p(i)%ircarb(AS,n)) / min(2,sc%P_nbplantes) ! on triche pour avoir une division par 1 ou 2

                 p(i)%nbgrains(AOAS) = p(i)%nbgrains(AO)  * sao + p(i)%nbgrains(AS) * sas
                 p(i)%CNgrain(AOAS) =  p(i)%CNgrain(AO) * sao   + p(i)%CNgrain(AS) * sas
                 p(i)%vitmoy(AOAS) =   p(i)%vitmoy(AO) * sao    + p(i)%vitmoy(AS) * sas
                 p(i)%magrain(0,n) =   p(i)%magrain(0,n-1) + (p(i)%magrain(AO,n) - p(i)%magrain(AO,n-1)) * sao  &
                                    + (p(i)%magrain(AS,n) - p(i)%magrain(AS,n-1)) * sas

                 p(i)%pgrain(AOAS) = (p(i)%pgrain(AO) + p(i)%pgrain(AS)) / min(2,sc%P_nbplantes) ! on triche pour avoir une division par 1 ou 2

              endif  ! fin test codeindetermin

! Modif Loic: je deplace le calcul de chargefruit depuis sortie.f90 car on en a besoin dans la routine repartir.f90
              if (p(i)%P_codeindetermin == 1) p(i)%chargefruit = p(i)%nbgrains(AOAS)
              if (p(i)%P_codeindetermin == 2) then
                  p(i)%chargefruit = p(i)%nbfruit
                  if (pg%P_codefrmur == 1) then
                      p(i)%chargefruit = p(i)%nbfruit + p(i)%nfruit(AOAS,p(i)%P_nboite)
                  endif
              endif

              call eauqual(&
                 logger,n,p(i)%P_deshydbase(itk(i)%P_variete),p(i)%P_tempdeshyd,sc%tcult,sc%tairveille,p(i)%ndrp, &
                           p(i)%nrec,p(i)%P_codeindetermin,p(i)%P_nboite,p(i)%P_stdrpdes(itk(i)%P_variete),          &
                           p(i)%P_dureefruit(itk(i)%P_variete),p(i)%nfruit(ens,1:p(i)%P_nboite),p(i)%pousfruit(ens), &
                           p(i)%pdsfruit(ens,1:p(i)%P_nboite),p(i)%P_h2ofrvert,p(i)%frplusp(ens),                    &
                           p(i)%P_vitpropsucre(itk(i)%P_variete),p(i)%P_vitprophuile(itk(i)%P_variete),              &
                           p(i)%magrain(ens,n),p(i)%somcourdrp,p(i)%nmat,p(i)%maenfruit(ens),p(i)%nlev,              &
                           p(i)%masecnp(ens,n),p(i)%mafeuilverte(ens),p(i)%P_h2ofeuilverte,p(i)%mafeuiljaune(ens),   &
                           p(i)%P_h2ofeuiljaune,p(i)%matigestruc(ens),p(i)%P_h2otigestruc,p(i)%restemp(ens),         &
                           p(i)%P_h2oreserve,p(i)%deshyd(ens,0:p(i)%P_nboite),p(i)%eaufruit(ens,0:p(i)%P_nboite),    &
                           p(i)%ndebdes,p(i)%teaugrain(ens),p(i)%h2orec(ens),p(i)%sucre(ens),p(i)%huile(ens),        &
                           p(i)%sucreder(ens),p(i)%huileder(ens),p(i)%sucrems(ens),p(i)%huilems(ens),                &
                           p(i)%pdsfruitfrais(ens),p(i)%mafraisrec(ens),p(i)%CNgrain(ens),                           &
                           p(i)%mafraisfeuille(ens),p(i)%mafraistige(ens),p(i)%mafraisres(ens),p(i)%mafrais(ens))

              p(i)%CNgrain(AOAS) = p(i)%CNgrain(AO) * sao + p(i)%CNgrain(AS) * sas
              p(i)%h2orec(AOAS)  = p(i)%h2orec(AO) * sao  + p(i)%h2orec(AS) * sas
              p(i)%sucre(AOAS)   = p(i)%sucre(AO)  * sao  + p(i)%sucre(AS)  * sas
              p(i)%huile(AOAS)   = p(i)%huile(AO)  * sao  + p(i)%huile(AS)  * sas

              p(i)%pdsfruitfrais(AOAS)  = p(i)%pdsfruitfrais(AO)*sao  + p(i)%pdsfruitfrais(AS)*sas
              p(i)%mafraisrec(AOAS)     = p(i)%mafraisrec(AS) * sas   + p(i)%mafraisrec(AO) * sao
              p(i)%mafraisfeuille(AOAS) = p(i)%mafraisfeuille(AS)*sas + p(i)%mafraisfeuille(AO)*sao
              p(i)%mafraistige(AOAS)    = p(i)%mafraistige(AS) * sas  + p(i)%mafraistige(AO) * sao
              p(i)%mafraisres(AOAS)     = p(i)%mafraisres(AS) * sas   + p(i)%mafraisres(AO) * sao
              p(i)%mafrais(AOAS)        = p(i)%mafrais(AO) * sao      + p(i)%mafrais(AS) * sas

              p(i)%sucreder(AOAS) = p(i)%sucreder(AS)
              p(i)%huileder(AOAS) = p(i)%huileder(AS)
              p(i)%sucrems(AOAS)  = p(i)%sucrems(AS)
              p(i)%huilems(AOAS)  = p(i)%huilems(AS)
            endif ! fin test codeplante /= snu

!            if (ens == as) then
!               if (p(i)%P_codtrophrac == 1) then
!                  repracmax = p(i)%P_repracpermax
!                  repracmin = p(i)%P_repracpermin
!                  kreprac = p(i)%P_krepracperm
!               endif
!               if (p(i)%P_codtrophrac == 2) then
!                  repracmax = p(i)%P_repracseumax
!                  repracmin = p(i)%P_repracseumin
!                  kreprac = p(i)%P_krepracseu
!               endif
!               if (p(i)%P_codtrophrac == 3) then
!                  repracmax = 0.
!                  repracmin = 0.
!                  kreprac = 0.
!               endif

            ! Calcul d'anoxmoy
               profhoriz = sc%nhe
               p(i)%anoxmoy = 0.
               do ic = sc%nh, 1, -1
                  do ii = 1, int(soil%P_epc(ic))
                     iz = profhoriz - ii + 1
                     if (iz <= int(p(i)%zrac)) p(i)%anoxmoy = p(i)%anoxmoy + sc%anox(iz)
                  end do
                  profhoriz = profhoriz - int(soil%P_epc(ic))
               end do
               p(i)%anoxmoy = p(i)%anoxmoy / max(p(i)%zrac,1.)

          ! Calcul de la reserve maximale en eau utilisee
               if (n > p(i)%nplt .and. p(i)%nrec == 0) then
                  p(i)%rmaxi = 0.
                  do iz = int(itk(i)%P_profsem), int(p(i)%zrac)+1
                    if (sc%hur(iz) < sc%hurmini(iz)) then
                       sc%hurmini(iz) = sc%hur(iz)
                    endif
                       p(i)%rmaxi = p(i)%rmaxi + sc%hucc(iz) - sc%hurmini(iz)
                  end do
               endif

               call croissanceFrontRacinaire(&
                  logger, n, sc%nh, nbCou, sc%hur(1:nbCou), sc%hucc(1:nbCou), sc%tcult, sc%tsol(1:nbCou), &
                        sc%humin(1:nbCou), sc%dacouche(1:nbCou), sc%P_codesimul, soil%P_epc(1:sc%nH), soil%P_obstarac,       &
                        pg%P_daseuilbas, pg%P_daseuilhaut, pg%P_dacohes, p(i)%P_zrac0, p(i)%P_coderacine, p(i)%P_codeplante, &
                        p(i)%P_codeperenne, p(i)%P_codehypo, p(i)%P_codegermin, p(i)%P_zracplantule, p(i)%P_stoprac,         &
                        p(i)%P_codetemprac, p(i)%P_tcmin, p(i)%P_tcmax, p(i)%P_tgmin, p(i)%P_croirac(itk(i)%P_variete),   &
                        p(i)%P_contrdamax, p(i)%P_codeindetermin, p(i)%P_sensanox, p(i)%nplt, p(i)%nrec, p(i)%codeinstal, &
                        p(i)%nger, p(i)%nsen, p(i)%nlax, p(i)%nflo, p(i)%nmat, p(i)%nlev, p(i)%namf, p(i)%izrac,          &
                        itk(i)%P_profsem, itk(i)%P_codcueille, p(i)%deltai(0,n), p(i)%lai(0,n), p(i)%sioncoupe,           &
                        p(i)%P_sensrsec, p(i)%P_codedyntalle, p(i)%P_tcxstop, p(i)%dltams(0,n), p(i)%rlf0(1:nbCou),       &
                        p(i)%rlg0(1:nbCou), sc%nhe, sc%nstoprac, p(i)%zrac, p(i)%zracmax, p(i)%znonli, p(i)%rlf_veille(1:nbCou), &
                        p(i)%rlg_veille(1:nbCou), p(i)%deltaz, p(i)%dtj(n+p(i)%ndecalg), p(i)%cumlracz, p(i)%poussracmoy,  &
                        p(i)%lracsenzf(1:nbCou), p(i)%lracsenzg(1:nbCou), p(i)%tempeff, p(i)%efda, p(i)%humirac_mean,      &
                        soil%profsol, t%P_humirac, p(i)%P_code_acti_reserve)

            endif ! fin test snu
!          endif   ! fin test surf
        end do    ! fin boucle ens
      end do      ! fin boucle nbplantes

return
end subroutine croissance
end module croissance_m
 
