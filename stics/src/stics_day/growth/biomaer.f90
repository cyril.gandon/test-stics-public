! *******************
! *-  sous-programme de croissance
! *-  version 6.x   le 09/05/2005
! *-  integration des reserves  hivernales
! *-  P_remobres = proportion de reserves mobilisables
! *-             par rapport a la croissance
! *-             a relier a la quantite totale de reserve
! This module calculates the shoot biomass growth thanks to the radiation intercepted by foliage.
! - Stics book paragraphe 3.3, page 54-57
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 3.3, page 54-57
!
!! This module calculates the shoot biomass growth thanks to the radiation intercepted by foliage.
!!
!! The linear relationship between accumulated biomass in the plant and radiation intercepted by foliage, demonstrated by Monteith (1972), defines the
!! radiation use efficiency (or RUE ) as the slope of this relationship. synthesizes the processes of photosynthesis and respiration.
!! Its calculation (ratio between above-ground biomass and absorbed radiation) implies that this parameter also takes account of a carbon allocation coefficient
!! between above-ground and below-ground parts of the plant.  Obviously, because of underlying physiological processes this ratio varies somewhat, due to stresses,
!! temperature and phenology.  To take account of these effects, Sinclair (1986) proposed that RUE should be considered as a physiological function,
!! to which stress indices should be applied.
!! In STICS the calculation of the daily production of shoot biomass (dltams) relies on the RUE concept (though the relationship between dltams and raint
!! is slightly parabolic) taking into account various factors known to influence the elementary photosynthesis and respiration processes,
!! mostly as stresses (ftemp, swfac, inns and exobiom).  Dltams accumulated day by day gives the shoot biomass of the canopy, masec.
!!
!! Influence of radiation and phasic development:
!!
!! the accumulation of shoot biomass depends on the intercepted radiation (raint), and is almost linear but slightly asymptotic at high intercepted light values.
!! It is simulated in STICS by a parabolic function involving a maximum radiation use efficiency specific to each species, ebmax.
!! The parameter coefb stands for the radiation saturating effect. This effect is the result, even buffered, of the saturation occurring within a short time step
!! on the individual leaf scale and is easily observed when daily calculations are made with instantaneous formulae of canopy photosynthesis ;
!! such calculations lead to a value of 0.0815. The efficiency, ebmax, may differ during the juvenile (ILEV-IAMF), vegetative (IAMF-IDRP) and
!! reproductive (IDRP-IMAT) phases (corresponding respectively to the parameters efcroijuv, efcroiveg and efcroirepro).
!! Classically, efcroijuv=1/2 efcroiveg is used to take account of the preferential migration of assimilates towards the roots at the beginning of the cycle.
!! The difference between efcroiveg and efcroirepro arises from the biochemical composition of storage organs: e.g. for oil or protein crops efcroirepro is less
!! than efcroiveg because the respiratory cost to make oil and protein is higher than for glucose.
!!
!! Effect of atmospheric CO2 concentration:
!!
! The CO2C parameter stands for the atmospheric CO2 concentration, which can be higher than the current value, assumed to be 350 ppm. The formalisation
!! chosen in STICS was adapted from Stockle et al. (1992): the effect of CO2C on the radiation use efficiency is expressed by an exponential relationship,
!! for which the parameter is calculated so that the curve passes through the point (600, alphaco2).
!!
!! There are two categories of reserves that can be mobilized :
!   1) the aerial reserves (carbohydrates, called restemp) : they can be mobilized during one cycle (flux remobilj). Their initial value (called restemp0) is user defined.
!!   Each day the maximal proportion of the reserves that can be remobilised is remobres, until aerial reserves are exhausted.
!!   These reserves are only called upon if the newly formed assimilates (dltams) fail to satisfy the sink strength (fpv and fpft), which leads to a first calculation of
!!   the source/sinks variable (sourcepuits). These remobilisations contribute to increasing the source/sink ratio the following day because they are counted
!!   in the variable dltams.
!!   Aerial N reserves (QNrestemp) can be mobilized in fruit during maturation phase (balance calculation)
!!    Their initial value (called QNrestemp0) is also user defined.
!!
!!   2) the storage reserves (rhizomes, woody materials, ...) of perennial crops (called resperenne).
!   - Reserve built up and used during the cycle: reserves built up during the vegetative cycle (variable restemp) and reused later on simply contribute
!!   to the estimation of the source/sink ratio for indeterminate crops.  The maximum quantity which can be remobilised per day (remobilj) is calculated
!!   similarly to dltaremobil.
!!   If the plant is both perennial and indeterminate, the reserves originating from the storage organs are first used (dltaremobil)
!!   and when exhausted the current cycles reserves (remobilj) can be used.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module biomaer_m
use raytrans_m, only: raytrans
use plant_utils
implicit none
private
public :: biomaer
contains
subroutine biomaer(n, ipl, ens, nlev, P_codeperenne, nplt, P_codehypo, P_codegermin, P_masecplantule, P_adil, namf, P_adilmax,   &
                   nrec, P_codcueille, P_codefauche, P_codelaitr, P_codetransrad, P_efcroijuv, P_efcroiveg, ndrp, P_efcroirepro, &
                   ! DR 13/02/2019 je rajoute chargefruit pour mettre le test comme il est dans le trunk
                   chargefruit,                                                                                                  &
                   P_coefb, tcult, P_teopt, P_teoptbis, P_temin, P_temax, P_codeclichange, P_alphaco2, co2, P_resplmax, densite, &
                   P_codeh2oact, swfac, exobiom, P_codeinnact, dltaremobil, fpv, fpft, P_remobres, P_msresiduel, P_nbcueille,    &
                   rdtint, CNgrain, P_codemontaison, nmontaison, masecnp_veille, masecnp, QNplantenp_veille, QNplantenp, inns,   &
                   inn, innlai, cumdltaremobilN, ebmax, ftemp, epsib, fco2, dltams, dltamsen, dltamstombe, restemp, dltamsN,     &
                   photnet, sourcepuits, dltaremobilN, remobilj, cumdltares, magrain_veille, magrain, masecneo, surface,         &
                   surfaceSous, P_nbplantes, P_extin, cumrg, cumraint, fapar, delta, P_adfol, lairognecum, laieffcum, P_dfolbas, &
                   P_dfolhaut, dfol, rdif, parapluie, raint, P_parsurrg, P_forme, lai, laisen, eai, P_interrang, nlax, nsen,     &
                   P_codlainet, P_hautbase, P_codepalissage, P_hautmaxtec, P_largtec, originehaut, hauteur, deltahauteur,        &
                   P_hautmax, varrapforme, largeur, jul, trg, P_latitude, rombre, rsoleil, P_orientrang, P_ktrou, tauxcouv,      &
                   P_khaut, surfAO, surfAS, fpari, P_Propres, P_PropresP, P_tauxmortresP, P_Efremobil, maperenne, mafeuil,       &
                   mafeuilverte, masecveg, resperenne, maperennemort, QCperennemort, QCO2resperenne, masec, QNrestemp,           &
                   QNresperenne, QNperenne, QNveg, QNvegstruc, QNperennemort, dltarestempN, dltarestemp, innlax, dltaperennesen, &
                   dltaQNperennesen, nstopres, tmin, P_tgelveg10, P_PropresPN ,dltarespstruc, pNvegstruc, pNrestemp,             &
                   P_codeplante, QNfeuille, QNtige, P_code_acti_reserve, ndes, phoi, phoi_veille, P_phobase, P_codeindetermin,   &
                   P_restemp0, demande, P_QNperenne0, P_codemsfinal, P_codephot)

    implicit none

  integer, intent(IN)    :: ipl
  integer, intent(IN)    :: ens
  integer, intent(IN)    :: nlev
  integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0 
  integer, intent(IN)    :: nplt
  integer, intent(IN)    :: n
  integer, intent(IN)    :: P_codehypo  ! // PARAMETER // option of simulation of a  phase of hypocotyl growth (1) or planting of plantlets (2) // code 1/2 // PARPLT // 0 
  integer, intent(IN)    :: P_codegermin  ! // PARAMETER // option of simulation of a germination phase or a delay at the beginning of the crop (1) or  direct starting (2) // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_masecplantule  ! // PARAMETER // initial shoot biomass of plantlet // t ha-1 // PARPLT // 1 
  real,    intent(IN)    :: P_adil  ! // PARAMETER // Parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // N% MS // PARPLT // 1 
  integer, intent(IN)    :: namf
  real,    intent(IN)    :: P_adilmax  ! // PARAMETER // Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // N% MS // PARPLT // 1 
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0 
  integer, intent(IN)    :: P_codefauche  ! // PARAMETER // option of cut modes for forage crops: yes (1), no (2) // code 1/2 // PARTEC // 0 
  integer, intent(IN)    :: P_codelaitr  ! // PARAMETER // choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0 
  integer, intent(INOUT) :: P_codetransrad  ! // PARAMETER // simulation option of radiation 'interception: law Beer (1), radiation transfers (2) // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_efcroijuv  ! // PARAMETER // Maximum radiation use efficiency during the juvenile phase(LEV-AMF) // g MJ-1 // PARPLT // 1 
  real,    intent(IN)    :: P_efcroiveg  ! // PARAMETER // Maximum radiation use efficiency during the vegetative stage (AMF-DRP) // g MJ-1 // PARPLT // 1 
  integer, intent(IN)    :: ndrp
  real,    intent(IN)    :: P_efcroirepro  ! // PARAMETER // Maximum radiation use efficiency during the grain filling phase (DRP-MAT) // g MJ-1 // PARPLT // 1 
! DR 13/02/2019 je rajoute chargefruit pour mettre le test comme il est dans le trunk
  real,    intent(IN)    :: chargefruit   ! // OUTPUT // Amount of filling fruits per m-2 // nb fruits.m-2
  real,    intent(IN)    :: P_coefb  ! // PARAMETER // parameter defining radiation effect on  conversion efficiency // SD // PARAM // 1 
  real,    intent(IN)    :: tcult   ! // OUTPUT // Crop surface temperature (daily average) // degree C
  real,    intent(IN)    :: P_teopt  ! // PARAMETER // Optimal temperature for the biomass growth // degree C // PARPLT // 1
  real,    intent(IN)    :: P_teoptbis  ! // PARAMETER // optimal temperature for the biomass growth (if there is a plateau between P_teopt and P_teoptbis) // degree C // PARPLT // 1
  real,    intent(IN)    :: P_temin  ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
  real,    intent(IN)    :: P_temax  ! // PARAMETER // Maximal threshold temperature for the biomass growth  // degree C // PARPLT // 1
  integer, intent(IN)    :: P_codeclichange  ! // PARAMETER // option for climatel change : yes (2), no (1)  // code 1/2 // STATION // 0 
  real,    intent(IN)    :: P_alphaco2  ! // PARAMETER // coefficient allowing the modification of radiation use efficiency in case of  atmospheric CO2 increase // SD // PARPLT // 1 
  real,    intent(IN)       :: co2
  real,    intent(IN)    :: P_resplmax  ! // PARAMETER // maximal reserve biomass // t ha-1 // PARAMV6 // 1 
  real,    intent(IN)    :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
  integer, intent(IN)    :: P_codeh2oact  ! // PARAMETER // code to activate  water stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0 
  real,    intent(IN)    :: swfac   ! // OUTPUT // Index of stomatic water stress  // 0-1
  real,    intent(IN)    :: exobiom   ! // OUTPUT // Index of excess water active on surface growth // 0-1
  integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0 
  real,    intent(IN)    :: fpv   ! // OUTPUT // Sink strength of developing leaves // g.j-1.m-2
  real,    intent(IN)    :: fpft   ! // OUTPUT // Sink strength of fruits  // g.m-2 j-1
  real,    intent(IN)    :: P_remobres  ! // PARAMETER // proportion of daily remobilisable carbon reserve // SD // PARPLT // 1 
  real,    intent(IN)    :: P_msresiduel  ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1 
  integer, intent(IN)    :: P_nbcueille  ! // PARAMETER // number of fruit harvestings // code 1/2 // PARTEC // 0 
  real,    intent(IN)    :: rdtint  
  real,    intent(IN)    :: CNgrain   ! // OUTPUT // Nitrogen concentration of grains  // %
  integer, intent(IN)    :: P_codemontaison  ! // PARAMETER // code to stop the reserve limitation from the stem elongation // code 1/2 // PARAMV6 // 0 
  integer, intent(IN)    :: nmontaison  

! Modifs Bruno et Loic fevrier 2013
    real,    intent(IN)    :: mafeuil
    real,    intent(IN)    :: mafeuilverte
    real,    intent(IN)    :: masecveg
    real,    intent(IN)    :: P_Propres
    real,    intent(IN)    :: P_PropresP
    real,    intent(IN)    :: P_tauxmortresP
    real,    intent(IN)    :: P_Efremobil
    real,    intent(INOUT) :: masecnp_veille    ! valeur de la veille
    real,    intent(INOUT) :: masecnp           ! valeur du jour courant   // OUTPUT // Aboveground dry matter  // t.ha-1
    real,    intent(INOUT) :: QNplantenp_veille ! valeur de la veille
    real,    intent(INOUT) :: QNplantenp        ! valeur du jour courant  // OUTPUT // Amount of nitrogen accumulated in the plant  // kgN.ha-1
    real,    intent(INOUT) :: inns        ! Index of nitrogen stress active on growth in biomass // P_innmin to 1
    real,    intent(INOUT) :: inn         ! Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
    real,    intent(INOUT) :: innlai      ! Index of nitrogen stress active on leaf growth // P_innmin a 1
    real,    intent(INOUT) :: cumdltaremobilN
    real,    intent(INOUT) :: ebmax
    real,    intent(INOUT) :: ftemp      ! Temperature-related EPSIBMAX reduction factor // 0-1
    real,    intent(INOUT) :: epsib      ! Radiation use efficiency  // t ha-1.Mj-1. m-2
    real,    intent(INOUT) :: fco2
    real,    intent(INOUT) :: dltams     ! Growth rate of the plant  // t ha-1.j-1
    real,    intent(INOUT) :: dltamsen   ! Senescence rate // t ha-1 j-1
    real,    intent(INOUT) :: dltamstombe
    real,    intent(INOUT) :: restemp    ! C crop reserve, during the cropping season, or during the intercrop period (for perenial crops) // t ha-1
    real,    intent(INOUT) :: dltamsN
    real,    intent(INOUT) :: photnet    ! net photosynthesis // t ha-1.j-1
    real,    intent(INOUT) :: sourcepuits   ! Pool/sink ratio // 0-1
    real,    intent(INOUT) :: dltaremobil   ! Amount of perennial reserves remobilised // g.m-2.j-1
    real,    intent(INOUT) :: dltaremobilN
    real,    intent(INOUT) :: remobilj   ! Amount of biomass remobilized on a daily basis for the fruits  // g.m-2 j-1
    real,    intent(INOUT) :: cumdltares
    real,    intent(INOUT) :: magrain_veille  ! magrain_veille => valeur precedente (valeur de la veille par ex.)
    real,    intent(INOUT) :: magrain         ! magrain => valeur actuelle (valeur du jour courant par ex.)
    real,    intent(INOUT) :: masecneo   ! Newly-formed dry matter  // t.ha-1
    real,    intent(IN)    :: surface(2)
    real,    intent(OUT)   :: surfaceSous(2)
    integer, intent(IN)    :: P_nbplantes  ! // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0 
    real,    intent(INOUT) :: P_extin(P_nbplantes)  ! // PARAMETER // extinction coefficient of photosynthetic active radiation canopy // SD // PARPLT // 1
    real,    intent(INOUT) :: cumrg       ! Sum of global radiation during the stage sowing-harvest   // Mj.m-2
    real,    intent(INOUT) :: cumraint    ! Sum of intercepted radiation  // Mj.m-2
    real,    intent(OUT)   :: fapar       ! Proportion of radiation intercepted // 0-1
    real,    intent(OUT)   :: delta

    ! TRANSRAD
    real,    intent(IN)    :: P_adfol  ! // PARAMETER // parameter determining the leaf density evolution within the chosen shape // m-1 // PARPLT // 1 
    real,    intent(IN)    :: lairognecum
    real,    intent(IN)    :: laieffcum
    real,    intent(IN)    :: P_dfolbas  ! // PARAMETER // minimal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1 
    real,    intent(IN)    :: P_dfolhaut  ! // PARAMETER // maximal foliar density within the considered shape // m2 leaf m-3 // PARPLT // 1 
    real,    intent(OUT)   :: dfol   !  "Within the shape  leaf density" // m2 m-3
    real,    intent(OUT)   :: rdif   ! Ratio between diffuse radiation and global radiation  // 0-1
    integer, intent(OUT)   :: parapluie
    real,    intent(OUT)   :: raint   ! Photosynthetic active radiation intercepted by the canopy  // Mj.m-2
    real,    intent(IN)    :: P_parsurrg  ! // PARAMETER // coefficient PAR/RG for the calculation of PAR  // * // STATION // 1 
    integer, intent(IN)    :: P_forme  ! // PARAMETER // Form of leaf density profile  of crop: rectangle (1), triangle (2) // code 1/2 // PARPLT // 0 
    real,    intent(IN)    :: lai     ! Leaf area index (table) // m2 leafs  m-2 soil
    real,    intent(IN)    :: laisen   ! Leaf area index of senescent leaves // m2 leafs  m-2 soil
    real,    intent(IN)    :: eai
    real,    intent(IN)    :: P_interrang  ! // PARAMETER // Width of the P_interrang // m // PARTEC // 1 
    integer, intent(IN)    :: nlax
    integer, intent(IN)    :: nsen
    integer, intent(IN)    :: P_codlainet  ! // PARAMETER // option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
    real,    intent(IN)    :: P_hautbase  ! // PARAMETER // Base height of crop // m // PARPLT // 1 
    integer, intent(IN)    :: P_codepalissage  ! // PARAMETER // option: no (1),  yes2) // code 1/2 // PARTEC // 0 
    real,    intent(IN)    :: P_hautmaxtec  ! // PARAMETER // maximal height of the plant allowed by the management // m // PARTEC // 1 
    real,    intent(IN)    :: P_largtec  ! // PARAMETER // technical width // m // PARTEC // 1 
    real,    intent(IN)    :: originehaut
    real,    intent(INOUT) :: hauteur   ! Height of canopy // m
    real,    intent(INOUT) :: deltahauteur
    real,    intent(INOUT) :: P_hautmax  ! // PARAMETER // Maximum height of crop // m // PARPLT // 1 
    real,    intent(INOUT) :: varrapforme
    real,    intent(OUT)   :: largeur   ! Width of the plant shape  // m
    integer, intent(IN)    :: jul
    real,    intent(IN)    :: trg   ! Active radiation (entered or calculated) // MJ.m-2
    real,    intent(IN)    :: P_latitude  ! // PARAMETER // Latitudinal position of the crop  // degree // STATION // 0 
    real,    intent(OUT)   :: rombre   ! Radiation fraction in the shade // 0-1
    real,    intent(OUT)   :: rsoleil   ! Radiation fraction in the full sun // 0-1
    real,    intent(IN)    :: P_orientrang  ! // PARAMETER // Direction of ranks // rd (0=NS) // PARTEC // 1 
    real,    intent(IN)    :: P_ktrou  ! // PARAMETER // Extinction Coefficient of PAR through the crop  (radiation transfer) // * // PARPLT // 1 

    real,    intent(IN)    :: tauxcouv   ! Cover rate // SD
    real,    intent(IN)    :: P_khaut  ! // PARAMETER // Extinction Coefficient connecting leaf area index to height crop // * // PARAM // 1 
    real,    intent(INOUT) :: surfAO
    real,    intent(INOUT) :: surfAS

    real,    intent(INOUT) :: maperenne
    real,    intent(INOUT) :: resperenne
    real,    intent(INOUT) :: maperennemort ! // Cumulative biomass of dead perennial organs  // t ha-1 /
    real,    intent(INOUT) :: QCperennemort ! // Cumulative C in dead perennial organs  // kg ha-1 /
    real,    intent(INOUT) :: QCO2resperenne
    real,    intent(INOUT) :: masec          ! Total Biomass (plant + perennial organs)  // t.ha-1
    real,    intent(INOUT) :: QNrestemp
    real,    intent(INOUT) :: QNresperenne
    real,    intent(INOUT) :: QNperennemort
    real,    intent(INOUT) :: QNperenne
    real,    intent(INOUT) :: QNveg
    real,    intent(INOUT) :: QNvegstruc
    real,    intent(INOUT) :: dltarestempN
    real,    intent(INOUT) :: dltarestemp  ! daily biomass flow between temporary reserves (restemp) and perennial reserves (maperenne) // t ha-1 d-1 /
    real,    intent(INOUT) :: innlax
    real,    intent(INOUT) :: dltaperennesen    ! // OUTPUT // daily mortality biomass flow of perennial organs  // t ha-1 d-1
    real,    intent(INOUT) :: dltaQNperennesen  ! // OUTPUT // daily N loss due to mortality of perennial organs // kg ha-1 d-1
    integer, intent(INOUT) :: nstopres          ! jour d'arret de la mise en reserve due au gel
    real,    intent(IN)    :: tmin
    real,    intent(IN)    :: P_tgelveg10
    real,    intent(OUT)   :: fpari  ! Cover rate // SD
    real,    intent(IN)    :: P_PropresPN
    real,    intent(OUT)   :: dltarespstruc     ! daily biomass flow towards structural perennial reserves // t ha-1 d-1/
    real,    intent(OUT)   :: pNvegstruc        ! proportion of N in structural biomass
    real,    intent(OUT)   :: pNrestemp         ! proportion of N in temporary reserves
    character(len=3),intent(IN) :: P_codeplante
    real,    intent(INOUT) :: QNfeuille
    real,    intent(INOUT) :: QNtige
    integer, intent(IN)    :: P_code_acti_reserve
    integer, intent(IN)    :: ndes
    real,    intent(IN)    :: phoi
    real,    intent(IN)    :: phoi_veille
    real,    intent(IN)    :: P_phobase
    integer, intent(IN)    :: P_codeindetermin
    real,    intent(IN)    :: P_restemp0
    real,    intent(IN)    :: demande
    real,    intent(IN)    :: P_QNperenne0
    integer, intent(IN)    :: P_codemsfinal
    integer, intent(IN)    :: P_codephot

    real :: kco2
    real :: remob
    real :: rdtote
    real :: maverte           ! biomass of green organs // t ha-1 /
    real :: restempmax        ! maximum amount of reserves biomass in aerials // t ha-1 /
    real :: resperennemax     ! maximum amount of reserves biomass in perennials // t ha-1 /
    real :: alloresp          ! partition coefficient of biomass reserves towards perennial reserves // SD /
    real :: dltaresper        ! daily biomass flow towards perennial reserves that can be mobilized  // t ha-1 d-1/
    real :: tauxmortp         ! daily mortality rate of perennial organs
    real :: dltaremobilbrut   ! daily biomass decrease in perennial organs
    real :: dltaCO2resperenne ! daily CO2 emissions due to perennial organs respiration during remobilisation
    real :: dltaresperennesen ! daily senescence of
    real :: transfN
    real :: dltaQNresperennesen
    real :: CNrestemp
    real :: CNresperenne
    real :: dltarespstrucN
    real :: dltaresperN
    real :: CCperenne         ! C concentration in perennial organs //g kg-1 DM/
    real :: dltarestemp2

    !DR 09/08/2019 j'ajoute un flag pour faire des ecritures de debug
    logical :: dbg_biom
    dbg_biom=.FALSE.

    CCperenne = 440.

    transfN = 0.
! 1. Pas de biomasse avant la levee

    ! ** AVANT LE STADE NLEV (LEVEE):
    ! *- dans le cas des cultures annuelles, deux cas:
    ! *-
    ! *- a) pour les cultures qui se plantent et admettent un temps de latence avant le demarrage de la croissance:
    ! *-    initialisation de masecnp a P_masecplantule a partir de la plantation
    ! *-    et calcul de QNplantenp par la courbe de dilution.
    ! *-
    ! *- b) pour les cultures qui se sement: initialisation de masecnp a 0 a partir du semis
    ! *-
    ! *- c) pour les cultures qui se plantent et demarrent directement
    ! *-    initialisation de masecnp a P_masecplantule a partir de la levee
    ! *-    qui est aussi la plantation
    ! *-    et calcul de QNplantenp par la courbe de dilution.


!    write(*,*)'biomaer deb',n,masecnp
    if (nlev == 0) then
! Modif Simon juin 2017 pour etre plus general
! TODO 31/08/2017 a tester par Simon
        if (P_codeperenne == 1 .and. n >= nplt) then
            if (P_codehypo == 2 .and. P_codegermin == 1) then
                masecnp = P_masecplantule
                QNplantenp_veille = P_adil * P_masecplantule * 10.
                inns   =  1.
                inn    = 1.
                innlai = 1.
                return
            else
                masecnp = 0.
                return
            endif
        else
            masecnp = 0.
            cumdltaremobilN = 0.
            return
        endif
    endif

    !: le jour de la levee
    if (n == nlev .and. P_codehypo == 2 .and. namf == 0) then
        masecnp_veille = P_masecplantule
        QNplantenp = P_adilmax * P_masecplantule * 10.
        inns   = 1.
        inn    = 1.
        innlai = 1.
    endif
 if(dbg_biom) write(3333,*)'deb biomaer **** n, masecnp, dltams',n, masecnp,dltams,'dltaremobil',dltaremobil,&
           'restemp', restemp
 ! Modifs Bruno et Loic fevrier 2013
 !  Le flux de mortalite des organes perennes se produit en permanence a condition que ?
 !  et que la temperature soit superieure a la temperature de base
 ! Loic aout 2019 : suite investigations vigne v9 vs vX le combat de l'annee
     if (P_code_acti_reserve == 1) then
        ! PL : 4/10/2022, added condition on P_codephot, for photoperiodic plants
        if (lai == 0. .or. (P_codephot == 1 .and. phoi < P_phobase)) then
            tauxmortp = 0.
        else
            tauxmortp = P_tauxmortresP
        endif
        dltaperennesen = tauxmortp * maperenne
        dltaresperennesen = tauxmortp * resperenne
        maperennemort = maperennemort + dltaperennesen
        QCperennemort = QCperennemort + dltaperennesen*CCperenne     ! flux cumule

        dltaQNperennesen = tauxmortp * QNperenne
        dltaQNresperennesen = tauxmortp * QNresperenne
        QNperennemort = QNperennemort + dltaQNperennesen     ! flux cumule
 !  actualisation des pools de reserve
        maperenne = maperenne - dltaperennesen
        resperenne = resperenne - dltaresperennesen
        QNperenne = QNperenne - dltaQNperennesen
        QNresperenne = QNresperenne - dltaQNresperennesen
     endif

 ! Le lendemain de la recolte, la biomasse et la quantite d'azote aerienne deviennent nulles
 ! Modifs Bruno et Loic fevrier 2013
 ! Loic sept 2020 : ajout codemsfinal
   if (nrec > 0 .and. n > nrec .and. P_codcueille == 1 .and. P_codefauche /= 1.or.n == ndes) then
       if (P_codemsfinal == 2) then
            masecnp = 0.
            masec = maperenne
            restemp = 0.
            QNveg = 0.
            QNvegstruc = 0.
            QNrestemp = 0.
            QNfeuille = 0.
            QNtige = 0.
            QNplantenp = 0.
        else
            masecnp = masecnp_veille
            QNplantenp = QNplantenp_veille
        endif
        ! DR 16/03/2023 on met a zero aussi raint : si plus de lai plus de rayonnement intercepté
        raint = 0.
        fapar = 0.
     return
     endif

! Ajout Loic Novembre 2017 : on ne rentre pas dans les calculs le jour de la recolte pour les plantes perennes
! pour eviter le defaut de bilan N dans la plante du aux calculs de flux de mise en reserve
! DR et IGC 24/05/2019 ce teste pour la vigne pose pb
! on essaie de le conditionner sur code_acti_reserve
!     if (nrec > 0 .and. n == nrec .and. P_codeperenne == 2) return
!     if (nrec > 0 .and. n == nrec .and. P_codeperenne == 2.and.P_code_acti_reserve==1) return
! Loic sept 2020 : je suis oblige de modifier les conditions car ca pose probleme pour le bilan N des plantes fourrageres
     if (nrec > 0 .and. n == nrec .and. P_codeperenne == 2.and.P_code_acti_reserve==1.and.P_codeplante /= CODE_FODDER) return
! === CALCUL DE LA MATIERE SECHE FORMEE ===

    ! pour le moment on interdit raytrans avec l'option taux de couverture

    if (P_codelaitr == 2) P_codetransrad = 1
    !TODO: il vaudrait mieux faire ce test dans une partie initialisation en amont et n'avoir ici qu'un test
    !      de verification, qui enverrait un message a l'utilisateur pour le prevenir qu'en l'etat actuel du code
    !      on force une loi de beer dans le cas du taux de couverture.
! 2. rayonnement intercepte
!      if(dbg_biom) write(4444,*)'dans biomaeravant raytrans n , raint',n, raint
!      if(dbg_biom) write(4444,*)P_codetransrad,P_extin,cumrg,cumraint,fapar,delta,P_adfol,lairognecum,laieffcum,P_dfolbas,P_dfolhaut,dfol,rdif
!      if(dbg_biom) write(4444,*)  parapluie,raint,P_parsurrg,P_forme,lai,laisen,eai,P_interrang,nlax,nsen,P_codlainet,P_hautbase,P_codepalissage
!      if(dbg_biom) write(4444,*)  P_hautmaxtec,P_largtec,originehaut,hauteur,deltahauteur,P_hautmax,varrapforme,largeur,jul,trg,P_latitude
!      if(dbg_biom) write(4444,*)  rombre,rsoleil,P_orientrang,P_ktrou,P_codelaitr,P_khaut,tauxcouv,surface,surfaceSous,ipl,P_nbplantes,ens
!      if(dbg_biom) write(4444,*)  surfAO,surfAS

    call raytrans(P_codetransrad,P_extin,cumrg,cumraint,fapar,delta,P_adfol,lairognecum,laieffcum,P_dfolbas,P_dfolhaut,dfol,rdif,&
         parapluie,raint,P_parsurrg,P_forme,lai,laisen,eai,P_interrang,nlax,nsen,nrec, P_codlainet,P_hautbase,P_codepalissage,&
         P_hautmaxtec,P_largtec,originehaut,hauteur,deltahauteur,P_hautmax,varrapforme,largeur,jul,trg,P_latitude,     &
         rombre,rsoleil,P_orientrang,P_ktrou,P_codelaitr,P_khaut,tauxcouv,surface,surfaceSous,ipl,P_nbplantes,ens,     &
         surfAO,surfAS, n)
        if(dbg_biom) write(3333,*)'biomaer', n, fapar, 'raint',raint,'lai',lai
! 3. Matiere seche potentielle (dltams)
      if (namf == 0)   ebmax = P_efcroijuv / 100.
      if (ndrp == 0 .and. namf > 0)  ebmax = P_efcroiveg / 100.
      if (ndrp /= 0)   ebmax = P_efcroirepro / 100.

    ! Modifs Bruno et Loic janvier 2014
    ! le test de Inaki pour la vigne peut-il etre remplace par le test suivant ?
    ! A verifier aussi pour les prairies
    !  if(dbg_biom) write(4444,*)'ebmax',ebmax,namf,ndrp,nrec,P_codcueille
! DR 23/01/2019 on avait ce test sur le trunk il faudra regarder
!   if (ndrp /= 0 .and. chargefruit <= 0.0) ebmax = P_efcroiveg / 100.
! DR 13/02/2019 je remets le test comme dans le trunk et je regarde si ca a une incidence sur ma rotation
! DR 13/02/2019 ca modifie les reserves des l'usm2 des rotations, je remets comme avant
    if (nrec > 0 .and. P_codcueille == 1) ebmax = P_efcroiveg   / 100.
! Loic avril 2019 : je remets en place ce test pour la vigne en ajoutant une condition code_acti_reserve
    if (ndrp /= 0 .and. chargefruit <= 0.0 .and. P_code_acti_reserve == 2) ebmax = P_efcroiveg / 100.

 !     if(dbg_biom) write(4444,*)'ebmax',ebmax,namf,ndrp,nrec,P_codcueille
     !: Calcul de l'effet rayonnement
      fpari = ebmax - (P_coefb * raint)
 !     if(dbg_biom) write(4444,*)'fpari',ebmax,P_coefb,raint
     ! ** calcul de l'effet temperature ftemp
      ftemp = calculerEffetTemperature(tcult,P_teopt,P_teoptbis,P_temin,P_temax)

    ! ** calcul de epsilon b
      epsib = fpari*ftemp
!        if(dbg_biom) write(4444,*)'epsib',epsib ,fpari,ftemp,ebmax,'ndrp',ndrp,'namf',namf

    ! ** effet CO2 sur efficience de conversion
      if (P_codeclichange == 2) then
          kco2 = -log(2.-P_alphaco2)/(600.-350.)
          fco2 =  2.-exp(-kco2*(co2-350.))
          epsib = epsib*fco2
      endif

      dltams = epsib*raint
      if(dbg_biom) write(3333,*)'****n epsib,raint,dltams,ebmax',n,epsib,raint,dltams,ebmax
! 4. Matiere seche reelle

 ! Modifs Bruno et Loic fevrier 2013
 ! *********************************

    ! On propose 2 formalismes de calcul du maximum de reserves (restempmax) :
    !  1) ancien formalisme avec resplmax si maperenne = 0 (pourra etre rempace par test sur codeperenne)
    !      on annule la croissance aerienne lorsque restemp >= restempmax (P_resplmax*densite)
    !      dans tous les cas sauf si P_codemontaison=1 (actif) et qu'on est apres la montaison (nmontaison>0)
    !  2) nouveau formalisme avec propres si maperenne > 0
    !       lorsque restemp atteint restempmax, on stocke le surplus d'assimilats dans les organes de stockage perenne (rhizomes, bois, ...)
!Loic et DR le 18/10/2016 on utilise plutot le code d'activation des resrves perennes P_code_acti_reserve plutot que la variable (pas bien ...)
!    if (maperenne == 0.) then
      if (P_code_acti_reserve == 2) then
        if (P_codemontaison == 2 .or. (P_codemontaison == 1 .and. nmontaison == 0)) then
            restempmax = P_resplmax * densite * 10.
            if (restemp > restempmax) dltams = 0.
        endif
      endif
          if(dbg_biom) write(3333,*)'dltams apres compare reserve perenne',n,restemp,restempmax,dltams

      if (raint <= 0.) dltams = 0.
! ** effet des stress
    ! *influence de l'exces d'eau
      if (P_codeh2oact == 1) dltams = dltams*swfac
      dltams = dltams*exobiom
      if(dbg_biom) write(3333,*)'dltams apres swfac,exobiom,',swfac,exobiom,dltams
!   Modif bug Loic : je mets a jour dltamsN plus bas pour prendre en compte le stress azote de la veille et
!   la remobilisation quand on active le code_acti_reserve
      dltamsN = dltams
      if(dbg_biom) write(3333,*)'dltamsN', dltamsN
    ! influence du stress azote
    if (P_codeinnact == 1) dltams = dltams*inns
            if(dbg_biom) write(3333,*)'dltams apres inns',dltams,inns
      photnet = dltams

 ! Modif BUG Loic et Bruno fevrier 2013 ?  la maj de dltams est faite plus loin apres calcul de dltaremobil
 ! Loic aout 2019 : pour la vigne de la v9 on remet a jour dltams ici, c'est le dltaremobil du jour d'avant
 ! DR 26/08/2019 pourquoi ici ? pour la vigne dltaremobil est calcule plus bas
    if (P_code_acti_reserve == 2) dltams = dltams + dltaremobil

    ! ** recalcul de l'epsilon b et changement d'unite
    if (raint > 0.) then
        epsib = dltams/raint*100.
    else
        epsib = 0.
    endif
    epsib = min(epsib,ebmax*100.)

! ** 1er Calcul du rapport source/puits

     sourcepuits = 1.
     if ((fpv+fpft) > 0.) sourcepuits = min(1., dltams*100./(fpv + fpft))
     dltaremobilbrut = 0.
     remobilj = 0.
     dltaremobilN = 0.

! Remobilisation chez les plantes ayant des organes de reserve perennes
! pas de remobilisation des reserves perennes quand il y a de la mise en reserve


    if (P_code_acti_reserve == 1) then
        dltaremobilbrut = 0.
        if (sourcepuits < 1.) then
            remob = (fpv + fpft)/100. - dltams
            remob = min(remob,P_remobres * resperenne)
            dltaremobilbrut = min(remob,resperenne)
        if (n >= ndrp.and. ndrp /= 0.) dltaremobilbrut = 0.
        endif
   ! flux de carbone
        dltaremobil = P_efremobil*dltaremobilbrut
        dltaCO2resperenne = 0.40*(1.-P_efremobil)*dltaremobilbrut
        QCO2resperenne = QCO2resperenne + dltaCO2resperenne  ! flux cumule
   ! flux d'azote
        CNresperenne = 0.
        if (resperenne > 0.) CNresperenne = QNresperenne/resperenne
        dltaremobilN = min(CNresperenne * dltaremobilbrut,QNresperenne)
    endif

! Modifs BUG Loic et Bruno fevrier 2013 dltams est maj ici
! Modif bug Loic: dltamsN doit egalement prendre en compte la remobilisation
! Attention dans ce cas on prend aussi en compte l'effet du stress N de la veille
! Je l'active pour les plantes simulees avec un compartiment de reserves perennes.
! Loic aout 2019 : pour la vigne dans la v9 on ne prend pas en compte les remobilisations du jour
    if (P_code_acti_reserve == 1) dltams = dltams + dltaremobil
    if (P_code_acti_reserve == 1) dltamsN = dltams

! 2eme calcul du rapport source/puits
! Loic decembre 2019 : pour retirer les remobilisations comme dans la v9.1
    if (P_code_acti_reserve == 1.or.P_codeindetermin == 2.or.P_codeperenne == 2) then
    sourcepuits = 1.
    if ((fpv+fpft) > 0.) sourcepuits = min(1., dltams*100./(fpv + fpft))
    remobilj = 0.

! Modif Loic et Bruno fevrier 2013 toutes les plantes peuvent profiter des reserves temporaires et avoir remobilj > 0
! ********************************
! Remobilisation des reserves chez les indeterminees dans la v9
! Calcul des reserves mobilisables avant l'effet des stress
    if (sourcepuits < 1.) then
       remob = (fpv + fpft)/100. - dltams
       remob = min(remob,P_remobres * restemp)
       remobilj = min(remob,restemp)
       if (P_code_acti_reserve == 1) then
          restemp = restemp - remobilj
       else
! comme dans la 9.1 pour les plantes non parametrees dans la v10
           if (cumdltares < P_restemp0) then
             dltaremobil = min(remob,restemp)
             dltaremobilN = demande
             cumdltaremobilN = cumdltaremobilN + dltaremobilN
             if (cumdltaremobilN > P_QNperenne0) dltaremobilN = 0.0
             restemp = restemp - dltaremobil
             remobilj = 0.
           else
             remobilj = min(remob,restemp)
             dltaremobil = 0.
             dltaremobilN = 0.
           endif
           ! Loic Oct 2020 : mise a jour de QNperenne et QNplantenp pour retrouver les simulations de la v9
           ! Loic Fevrier 2021 : il ne faut pas mettre a jour QNplantenp_veille ni QNperenne
           ! QNperenne = QNperenne - dltaremobilN
           ! QNplantenp_veille = QNplantenp_veille + dltaremobilN
         if(dbg_biom) write(3333,*) 'remobil fin test, restemp',remob,restemp, remobilj, 'dltaremobil',dltaremobil
       endif
    else
    ! DR 26/08/2019 sourcepuit>=1. pour la vigne dans trunk on ne remobilisait rien
    ! Loic sept 2020 : je mets code_acti_reserve qui est plus general
    ! if(P_codeplante.eq.CODE_VINE)the
      if(P_code_acti_reserve.eq.2)then
         dltaremobil = 0.
         dltaremobilN = 0.
         remobilj = 0.
      endif
    endif
    endif

         if(dbg_biom) write(3333,*) 'remobil fin, resperenne',remob,restemp, remobilj
! Calcul de QNvegstruc et QNrestemp pour toutes les plantes
! Modif Loic Mai 2016: Il y a plusieurs problemes dans ce formalisme.
! Creation d'une nouvelle routine repartirN pour que QNplantenp, QNvegstruc et QNrestemp soient
! mis a jour au jour le jour.

 ! Loic : Enregistrement de l'INN au stade LAX avant la mise en reserve de l'azote
     if (nlax == 0) innlax = inn
 ! Modif Loic mars 2014 arret definitif de mise en reserve de C et N chez les perennes apres un gel
 ! Loic sept 2020 : ajout code_acti_reserve
     if (nlax /= 0 .and. tmin < P_tgelveg10 .and. phoi < phoi_veille .and. P_code_acti_reserve == 1) nstopres = n
 ! Mise en reserve de biomasse chez les perennes
 ! a) calcul de restempmax pour les plantes perennes
     if (P_code_acti_reserve == 1) then
        if(mafeuil == 0.) then
            maverte = 0.
        else
            maverte = mafeuilverte/mafeuil*masecveg
        endif
        restempmax = P_Propres*maverte
 ! b) calcul du flux de stockage vers les organes perennes (dltarestemp)
        if(restemp <= restempmax .or. nstopres > 0) then
            dltarestemp = 0.
        else
            dltarestemp = restemp-restempmax
            restemp = restempmax             ! Mise a jour du pool
        endif
 ! c) partition du Flux vers la partie structurale ou la partie reserve des organes perennes
        resperennemax = P_PropresP * maperenne
 !    Modification Loic Novembre 2016
        resperenne = resperenne + dltarestemp
       ! write(7777,*)n,'resperenne',resperenne
        if (resperenne < resperennemax) then
            alloresp = 1.
            dltarestemp2 = 0.
        else
            alloresp = P_PropresP
            dltarestemp2 = resperenne - resperennemax
        endif
        dltaresper = alloresp * dltarestemp2
        dltarespstruc = (1.-alloresp) * dltarestemp2
        ! Ajout Loic Septembre 2016
        if (dltarestemp > 0.) then
            dltaremobilbrut = 0.
            dltaremobilN = 0.
            ! Loic Janvier 2024 : oubli corrige !!!
            dltams = dltams - dltaremobil
            dltaremobil = 0.
            dltamsN = dltams
        endif
        ! ajout Bruno-Simon avril 2017 pour maintenir maperenne a 0 apres jour de destruction
        if (n >= ndes .and. ndes > 0) then
          dltarestemp = 0.
          dltaremobilbrut = 0.
        endif
! d) actualisation des pools de reserve
        maperenne  = maperenne + dltarestemp - dltaremobilbrut
        resperenne = resperenne + dltaresper - dltaremobilbrut - dltarestemp2
        ! Ajout Loic septembre 2016 : pour les plantes perennes il faut retirer les flux de mise en reserve
        ! a dltams pour calculer dltamsN : La courbe de dilution s'applique a la biomasse aerienne
        dltamsN = max(0.,dltams - dltarestemp)
 ! Mise en reserve d'azote chez les perennes
 ! a) flux N vers les organes perennes :
        CNrestemp =  0.
        dltarespstrucN = 0.
        dltaresperN = 0.
        dltarestempN = 0.
        if(restemp > 0.) CNrestemp = QNrestemp/restemp
        dltarestempN   = min(QNrestemp, dltarestemp * CNrestemp)
        if (dltarestempN > 0.) then
          if (dltarespstruc > 0.) then
            dltarespstrucN = dltarestempN * (1. - P_propresPN)
            dltaresperN = dltarestempN * P_propresPN
          else
            dltarespstrucN = 0.
            dltaresperN = dltarestempN
          endif
        endif

! Modif Loic Avril 2016: il y avait un probleme a prelever tout l'azote pour les organes perennes dans la biomasse aerienne non perenne
! c) Lorsqu'il y a croissance foliaire, il n'y pas de mise en reserve N, il y a une demande en azote de la part des organes perennes (demandep) voir dans bNpl.f90
       if (nlax == 0.) then
          dltarestempN = 0.
          dltaresperN  = 0.
          dltarespstrucN = 0.
          transfN = 0.
       endif
! d) mise a jour des pools N
        QNrestemp    = QNrestemp - dltarestempN
        QNresperenne = QNresperenne + dltaresperN - transfN - dltaremobilN
        QNperenne    = QNperenne + dltarestempN - dltaremobilN
     endif

     QNplantenp   = QNplantenp_veille + dltaremobilN - dltarestempN
     QNveg        = QNveg + dltaremobilN - dltarestempN

!     if(dbg_biom) write(4444,*)'QNplante : QNrestemp, QNresperenne,QNperenne,QNplantenp,QNveg',QNrestemp, QNresperenne,QNperenne,QNplantenp,QNveg
! Fin modif Loic et Bruno pour perenne

! Cumul de remobilisation (sert au calcul du nb inflorescences au stade amf)
     cumdltares = cumdltares + dltaremobil + remobilj
! Pour les cultures fauchees demarrage a la MS residuelle de la coupe precedente
     if (n == nlev .and. P_codefauche == 1)  masecnp_veille = P_msresiduel

! Actualisation des pools de MS
! Modifs Bruno et Loic fevrier 2013 : dltamstombe etant calcule plus tard, masecnp est actualise dans apportfeuillesmortes
     masecnp = masecnp_veille + dltams - dltarestemp

   if(dbg_biom) write(3333,*)'apres masec',n,masecnp_veille, dltams , dltarestemp,masecnp
    if (masecnp < 0.) then
        dltamsen = 0.
        dltamstombe = 0.
        masecnp = 0.
!  Modif Bruno fevrier 2018 : pour avoir un magrain non nul le jour de recolte
!        magrain = 0.
        if(nrec > 0 .and. n > nrec) magrain = 0.
    endif

    if (P_codcueille == 2 .and. P_codeplante /= CODE_FODDER) then
        if (n == nrec .and. nrec > 0) then
          if (P_nbcueille == 1) rdtote = magrain_veille / 100.
          if (P_nbcueille == 2) rdtote = rdtint / 100.
          masecnp = masecnp - rdtote
          ! 29/08/2019 LS et DR dans 9.1 on prend QNplante de la veille car ici il n'est pas encore calule
          ! NON ici QNplante est calcul�!
          QNplantenp = QNplantenp - (CNgrain * rdtote * 10.)
          ! QNplantenp = QNplantenp_veille - (CNgrain * rdtote * 10.)
          if(dbg_biom) write(3333,*)'QNplante apres rec',QNplantenp
        endif
    endif

    !: DR 05/03/08 - on ne fait ca que dans le cas de la prairie qui a des coupes
    !- sinon pb avec les autres cultures
    !- calcul de la matiere seche neoformee
    if (P_codefauche == 1) then
        masecneo = masecnp - P_msresiduel
    endif
    ! Ajout Loic Mai 2016: enregistrement de la partition de l'azote dans la partie aerienne
    pNvegstruc = QNvegstruc / QNveg
    pNrestemp  = QNrestemp / QNveg
      if(dbg_biom) write(3333,*)'fin biomaer masecnp QNplantenp', masecnp, QNplantenp
      if(dbg_biom) write(3333,*)'==='
      if(dbg_biom) write(3333,*)'dltaremobil fin biomaer',dltaremobil


!      write(*,*)'biomaer fin',n,masecnp

    return

end

!============================================


! ** calcul de l'effet temperature ftemp
real function calculerEffetTemperature(tcult,P_teopt,P_teoptbis,P_temin,P_temax)

    implicit none

    !: Arguments
    real :: tcult   ! Crop surface temperature (daily average) // degree C
    real :: P_teopt  ! // PARAMETER // Optimal temperature for the biomass growth // degree C // PARPLT // 1
    real :: P_teoptbis  ! // PARAMETER // optimal temperature for the biomass growth (if there is a plateau between P_teopt and P_teoptbis) // degree C // PARPLT // 1
    real :: P_temin  ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
    real :: P_temax  ! // PARAMETER // Maximal threshold temperature for the biomass growth  // degree C // PARPLT // 1

    !: Variables locales
    real :: ftemp   ! Temperature-related EPSIBMAX reduction factor // 0-1

    !: Introduction d'un plateau entre P_teopt et P_teoptbis
    if (tcult < P_teopt) then
        ftemp = 1 - ((tcult - P_teopt) / (P_temin - P_teopt))**2
    endif

    if (tcult > P_teoptbis) then
        ftemp = 1 - ((tcult - P_teoptbis) / (P_temax - P_teoptbis))**2
    endif

    if (tcult >= P_teopt .and. tcult <= P_teoptbis) then
        ftemp = 1
    endif

    if (ftemp < 0.0) ftemp = 0.01

    calculerEffetTemperature = ftemp ! affectation de la variable de retour

    return
end function calculerEffetTemperature
end module biomaer_m
 
