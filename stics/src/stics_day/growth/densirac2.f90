! ************************************************************************* c
! * NB - le 30/5/98                                                       * c
! * croissance racinaire selon groupe racine (17/03/98)                   * c
! * compte-rendu F. Devienne                                              * c
! * rlj : longueur racinaire fournie par la plante en m jour-1            * c
! * P_draclong : parametre specifique donnant la vitesse de croissance      * c
! * racinaire par plante et par degre.jour                                * c
! * drliz : repartition de la longueur dans chaque couche                 * c
! * poussrac(z) : indice entre 0 et 1 definissant les obstacles physiques * c
! * a la croissance                                                       * c
! * P_croirac : croissance du front racinaire en cm par degre.jour          * c
! * debsen : jour definissant l'entre en senescence                       * c
! * ratio de senescence                                                   * c
! * prop = 1 repartition proportionnelle aux racines presentes            * c
! * prop = 0 repartition equitable                                        * c
! ************************************************************************* c
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This module calculates the root density profile according to the 'true density' option.
!!
! - Stics book paragraphe 5.2.2, page 90-94
!
!! With this option, growth in root length is first calculated, and then distributed to each layer of the soil profile. For sown crops, this calculation begins
!! at emergence: between germination and emergence, it is assumed that only the root front grows. For transplanted or perennial crops, the calculation is
!! initiated with an existing root density profile. After a lifetime characteristic of the species, the roots senesce and enter the mineralization process as
!! crop residue at the end of the crop cycle. Root density above 0.5 cm.cm-3 is not taken into account for water and nitrogen absorption.
!  - Growth in root length
!!   To ensure the robustness of the model, we have chosen to simulate the growth in root length directly, without passing through the root mass, because
!!   the specific length (root length/mass ratio) varies depending on the stresses suffered by the plant.  Two options are available to calculate the root length.
!!   With the first option, we have adopted a formulation similar to that used for the above-ground growth of leaves (Brisson et al., 1998a). With the second, a
!!   trophic link between shoot growth and root growth allows increase in root length to be calculated.
!       - Self-governing production : growth in root length is calculated using a logistic function that is analogous to that of leaves.
!!      A first calculation of the root length growth rate describes a logistic curve. This value is then multiplied by the effective crop temperature,
!!      the plant density combined with an inter-plant competition factor that is characteristic for the variety, and the water logging stress index.
!!      Then a second term is added corresponding to the growth at the root front (nouvrac), depending on the front growth rate (deltaz).
!!      The logistic curve describing the root length growth rate depends on the maximum root growth parameter draclong and on the normalized root development
!!      unit urac, ranging from 1 to 3 (such as ulai) and is thermally driven, even when the plant has vernalisation or photoperiod requirements.
!!      The plant parameters pentlaimax and vlaimax are the ones already used for the calculation of leaf growth rate.
!!      The thermal function rlj relies on crop temperature and cardinal temperatures (tcmin and tcmax) which are the same values as for the leaf area growth
!!      calculation. The inter-plant competition function is the same as the one calculated for the leaf area growth. Unlike the leaf area index,
!!      water and nitrogen deficiencies in the plant do not play any role in root growth, which results in the promotion of root growth relative to
!!      above-ground growth in the event of stress.  In contrast, anoxia acts via the water-logging stress index derived from the anox indicator.
!       - Trophic-linked production : the root length growth may rely on the daily production of shoot biomass (dltams) and on a dynamic underground/total biomass
!!      partitioning coefficient (reprac). The parameter longsperac is the specific root length/root mass ratio. The plant density effect is not taken
!!      into account because it is already integrated in the shoot biomass production. This value can replace calculation or just act as a threshold according
!!      to the chosing option.
!  - Distribution in the profile
!!   The new root length is then distributed in each layer of the soil profile in proportion to the roots present and as a function of the soil constraints.
!!   A "root sink strength" is defined by the proportion of roots present in the layer. This does not concern the root front, whose growth in density is
!!   defined by lvfront. This potential 'root sink strength' is then reduced by the soil constraints in each layer. Each constraint is defined at the layer level,
!!   in the form of an index between 0 and 1, and assumed to be independent of the others. The resulting index poussrac is the product of elementary indices:
!!   humirac defines the effect of soil dryness, taking account of the plant's sensitivity to this effect. efda defines the effect of soil compaction through
!!   bulk density. The anoxia index of each soil layer anox(iz) is assigned the value of 1 if the horizon has reached saturation; it is associated with the
!!   sensitivity of the plant to water logging sensanox. efnrac defines the effect of mineral nitrogen, which contributes to the root distribution in the
!!   layers with high mineral nitrogen content. It depends on the specific parameters minazorac, maxazorac and minefnra which characterize the sensitivity
!!   of plant root growth to the mineral nitrogen content in the soil. This last constraint is optional and can be inactivated in the model.
!  - Senescence
!!   A thermal duration in degree days (stdebsenrac) defines the lifespan of roots. Thus, the history of root production per layer is memorized in order
!!   to make disappear by senescence the portion of roots stdebsenrac set earlier. The profile of dead roots is lracsenz while the corresponding total
!!   amount is lracsentot.
!  - Root density profiles
!!   The living root density profile is rl, while the total amount is rltot. For water and nitrogen absorption, an efficient root length density (lracz)
!!   is calculated by applying the threshold lvopt (by default equals 0.5 cm cm-3) to the total root length density, RL.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------*
!
module densirac2_m
USE F_humirac_m, only: F_humirac
USE math_utils, only: from_linear
use messages
use messages_data
implicit none
private
public :: densirac2
contains
   subroutine densirac2(logger,n, nbCouches, nbCoum, dacouche, hur, humin, anox, nstoprac, P_codazorac, P_minazorac, P_maxazorac,&
                    P_minefnra, P_codtrophrac, P_coefracoupe, P_daseuilbas, P_daseuilhaut, P_dacohes, P_contrdamax, P_sensrsec,  &
                    P_sensanox, P_stlevamf, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens, P_draclong, P_vlaimax,           &
                    P_longsperac, P_codedyntalle, drlsenmortalle, P_profsem, P_msresiduel, P_lvopt, profsol, densite, lai_veille,&
                    maxdayf, maxdayg, nit, amm, nrec, nger, nlev, codeinstal, poussracmoy, zrac, somtemprac, dtj, idzrac,        &
                    efdensite_rac, dltams, repracmin, repracmax , kreprac, sioncoupe, debsenracf, debsenracg,          &
!                    P_propracfmax, P_zramif, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf, ndebsenracg,   &
                    P_propracfmax, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf, ndebsenracg,   &
                    ndecalf, ndecalg, nsencourpreracf, nsencourpreracg, rlf, rlg, drlf, drlg, lracsenzf, lracsenzg, lracsentotf, &
                    lracsentotg, rltotf, rltotg, rlf_veille, rlg_veille, dltarltotf, dltarltotg, dltasentotf, dltasentotg,       &
                    drlsenf, drlseng, somtempracvie, efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj, dltaremobil, lai,&
                    P_code_acti_reserve, inn, turfac, P_code_stress_root, P_codemonocot, P_kdisrac, P_alloperirac, P_pgrainmaxi, &
                    P_humirac, hucc, P_codemortalracine, masecnp, urac)
                    

  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: n  
  integer,  intent(IN)   :: nbCouches      ! number of soil layers containing roots
  integer,  intent(IN)   :: nbCoum         ! maximum number of soil layers = nbCouchesSol
  real,    intent(IN)    :: dacouche(nbCouches)  
  real,    intent(IN)    :: hur(nbCouches)  
  real,    intent(IN)    :: humin(nbCouches)  
  real,    intent(IN)    :: anox(nbCouches)  
  integer, intent(IN)    :: nstoprac  
  integer, intent(IN)    :: P_codazorac  ! // PARAMETER // activation of the nitrogen influence on root partitioning within the soil profile  // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_minazorac  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // kg N ha-1 mm-1 // PARPLT // 1 
  real,    intent(IN)    :: P_maxazorac  ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning  // kg N ha-1 mm-1 // PARPLT // 1 
  real,    intent(IN)    :: P_minefnra   ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // SD // PARPLT // 1
  integer, intent(IN)    :: P_codtrophrac  ! // PARAMETER // trophic effect on root partitioning within the soil // code 1/2/3 // PARPLT // 0 
  real,    intent(IN)    :: P_coefracoupe  ! // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARAMV6/PLT // 1 
  real,    intent(IN)    :: P_daseuilbas   ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real,    intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1 
  real,    intent(IN)    :: P_dacohes  ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1 
  real,    intent(IN)    :: P_contrdamax ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
  real,    intent(IN)    :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1 
  real,    intent(IN)    :: P_sensanox  ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1 
  real,    intent(IN)    :: P_stlevamf  ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1 
  real,    intent(IN)    :: P_stamflax  ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1 
  real,    intent(IN)    :: P_lvfront   ! // PARAMETER // Root density at the root front // cm root.cm-3 soil // PARPLT // 1
  real,    intent(IN)    :: P_laicomp   ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1
  real,    intent(IN)    :: P_adens     ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1
  real,    intent(IN)    :: P_bdens     ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real,    intent(IN)    :: P_draclong  ! // PARAMETER // Maximum rate of root length production // cm root plant-1 degree.days-1 // PARPLT // 1 
  real,    intent(IN)    :: P_vlaimax   ! // PARAMETER // ULAI  at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  real,    intent(IN)    :: P_longsperac ! // PARAMETER // specific root length // cm g-1 // PARPLT // 1
  integer, intent(IN)    :: P_codedyntalle  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0 
  real,    intent(IN)    :: P_lvopt      ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1
  real,    intent(IN)    :: P_profsem    ! // PARAMETER // Sowing depth // cm // PARTEC // 1

  real,    intent(IN)    :: nit(nbCouches)
  real,    intent(IN)    :: amm(nbCouches)
  integer, intent(IN)    :: profsol
  integer, intent(IN)    :: nrec
  integer, intent(IN)    :: nger
  integer, intent(IN)    :: nlev
  logical, intent(IN)    :: sioncoupe
  integer, intent(IN)    :: codeinstal
  real,    intent(IN)    :: idzrac

  real,    intent(IN)    :: drlsenmortalle  ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.j-1
  real,    intent(IN)    :: densite      ! // OUTPUT // Actual sowing density // plants.m-2
  real,    intent(IN)    :: P_msresiduel ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  real,    intent(IN)    :: lai_veille                  ! lai(n-1)
  real,    intent(IN)    :: dltams       ! // OUTPUT // Growth rate of the plant // t ha-1.d-1
  real,    intent(IN)    :: repracmin  
  real,    intent(IN)    :: repracmax  
  real,    intent(IN)    :: kreprac
  real,    intent(INOUT) :: somtemprac
  real,    intent(INOUT) :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,    intent(INOUT) :: racnoy
  real,    intent(INOUT) :: flrac(nbCouches)
  real,    intent(INOUT) :: lracz(nbCouches)
  real,    intent(INOUT) :: efda      ! // OUTPUT // effect of soil compaction through bulk density // 0-1
  real,    intent(INOUT) :: efnrac_mean    ! // OUTPUT // effect of mineral nitrogen, which contributes to the root distribution in the layers with high mineral nitrogen content. // 0-1
  real,    intent(INOUT) :: humirac_mean ! // OUTPUT // soil dryness // 0-1
  real,    intent(INOUT) :: humirac_z(nbcouches)
  real,    intent(INOUT) :: efnrac_z(nbcouches)
  real,    intent(OUT)   :: rlj ! // OUTPUT // roots length growth rate  // m.d-1

  real,    intent(OUT)   :: efdensite_rac
  real,    intent(OUT)   :: poussracmoy   ! // OUTPUT // Effect of soil constraints on the rooting profile (option true density )" // 0-1
  real,    intent(OUT)   :: cumlracz      ! // OUTPUT // Sum of the effective root length // cm root.cm-2 soil
  real,    intent(OUT)   :: cumflrac
  real,    intent(OUT)   :: cumlraczmaxi

! Modifs Loic et Bruno fevrier 2014
  real,    intent(IN)    :: debsenracf               ! Lifespan of fine roots  (degrees C.days)
  real,    intent(IN)    :: debsenracg               ! Lifespan of coarse roots  (degrees C.days)
  real,    intent(IN)    :: P_propracfmax            !
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!  real,    intent(IN)    :: P_zramif
  integer, intent(IN)    :: maxdayf
  integer, intent(IN)    :: maxdayg               !
  real,    intent(IN)    :: dtj(maxdayg)    ! // OUTPUT // Daily efficient temperature for the root growing  // degree C.j-1

  real,    intent(INOUT) :: rlf(nbCouches)           ! length of fine roots in layer iz on day n
  real,    intent(INOUT) :: rlg(nbCouches)           ! length of coarse roots in layer iz on day n
  real,    intent(INOUT) :: rlf_veille(nbCouches)    ! length of fine roots in layer iz on day n-1
  real,    intent(INOUT) :: rlg_veille(nbCouches)    ! length of coarse roots in layer iz on day n-1
  real,    intent(INOUT) :: drlsenf(nbCouches)       ! length of dead fine roots in layer iz on day n
  real,    intent(INOUT) :: drlseng(nbCouches)       ! length of dead coarse roots in layer iz on day n

  real,    intent(INOUT) :: drlf(nbCouches,maxdayf)     ! length of fine roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlg(nbCouches,maxdayg)     ! length of coarse roots emitted in layer iz on day n
  integer, intent(INOUT) :: ndebsenracf              ! first day when fine roots start to die
  integer, intent(INOUT) :: ndebsenracg              ! first day when coarse roots start to die
  integer, intent(INOUT) :: nsencourpreracf          ! last day at which fine root mortality is calculated
  integer, intent(INOUT) :: nsencourpreracg          ! last day at which coarse root mortality is calculated
  integer, intent(INOUT) :: ndecalf                  ! number of days remaining in the previous simulation without mortality of fine roots
  integer, intent(INOUT) :: ndecalg                  ! number of days remaining in the previous simulation without mortality of coarse roots
  real,    intent(INOUT) :: dltarltotf           ! length of fine roots emitted over the root profile on day n   cm.cm-2 sol
  real,    intent(INOUT) :: dltarltotg           ! length of coarse roots emitted over the root profile on day n  cm.cm-2 sol
  real,    intent(INOUT) :: dltasentotf          ! length of fine roots which die on day n over the root profile  cm.cm-2 sol
  real,    intent(INOUT) :: dltasentotg          ! length of coarse roots which die on day n over the root profile  cm.cm-2 sol
  real,    intent(INOUT) :: lracsenzf(nbCouches)     ! length of fine roots which die in layer iz on day n
  real,    intent(INOUT) :: lracsenzg(nbCouches)     ! length of coarse roots which die in layer iz on day n
  real,    intent(INOUT) :: somtempracvie        ! cumulative thermal time during successive runs (used to calculate initiation of root mortality)

  real,    intent(OUT)   :: lracsentotf  ! // OUTPUT // Total length of fine senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: lracsentotg  ! // OUTPUT // Total length of coarse senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotf       ! // OUTPUT // Total length of fine roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotg       ! // OUTPUT // Total length of coarse roots // cm root.cm-2 soil

  real,    intent(IN)    :: dltaremobil
  real,    intent(IN)    :: lai
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: inn
  real,    intent(IN)    :: turfac
  integer, intent(IN)    :: P_code_stress_root

  ! Florent Juin 2018
  real,    intent(IN)    :: P_kdisrac
  real,    intent(IN)    :: P_alloperirac
  real,    intent(IN)    :: P_pgrainmaxi
  integer, intent(IN)    :: P_codemonocot
  integer, intent(IN)    :: P_humirac
  real,    intent(IN)    :: hucc(nbCouches)
  integer, intent(IN)    :: P_codemortalracine
  real,    intent(IN)    :: masecnp

  ! PL, 7/10/2020 
  real,    intent(OUT)   :: urac   ! // OUTPUT // daily relative development unit for root growth  // 1-3

!: Variables locales
  integer :: iz
  integer :: i
  real :: poussrac(nbCouches)  
  real :: pondetot(nbCouches)  
  real :: azo  
  real :: daz  
  real :: efanoxd  
  real :: efnrac
  real :: reprac  
  real :: rlj1  
  real :: stsen  
  real :: ponderation 
  ! PL, 7/10/2020  
  !real :: urac
  integer :: nsencouracf
  integer :: nsencouracg
  integer :: ndf
  integer :: ndg
  real :: drliz
  real :: drlizf
  real :: drlizg
  real :: durvieracinef
  real :: durvieracineg
  real :: propracf
  real :: propracfiz

  ! Florent Juin 2018
  real    :: masem
  real    :: masem_rac
  real    :: nouvrac_sem, nouvracj
  real    :: rl_veille
  integer :: zsup
  integer :: irac, nrac, nhet
  real    :: dis(nbCouches)
  real    :: stress

! Florent Juin 2018 :
! Pas de densite racinaire attribuee pendant la phase [germination-levee]
! Attribution d'une densite racinaire le jour de la levee fonction des reserves de la graine (perisperme)
        if (nlev == 0 .and. codeinstal == 0) return
! Repartition de la densite racinaire sur la couche [zsup,zrac]
       ! zsup = 1 cm    pour les Monocot (equivalence du plateau de tallage et des racines nodales)
       ! zsup = profsem pour les Dicot
        if (P_codemonocot == 1) then
            zsup = 1
        else ! Dicot
            zsup = int(P_profsem)
        endif

        irac = int(zrac)                 ! numero de la derniere couche contenant des racines au jour n
!       if((zrac-irac).gt.1.e-5) irac = irac + 1
        if((zrac-irac).gt.0.) irac = irac + 1
        nrac = irac - zsup + 1           ! nombre de couches contenant des racines au jour n

! Attribution d'une densite racinaire le jour de la levee fonction des reserves de la graine (perisperme)
!       if (n == nlev .and. codeinstal == 0) then
        if (n >= nlev .and. nlev > 0 .and. codeinstal == 0) then
        ! Biomasse de grain seme et ayant leve
           masem = P_pgrainmaxi * densite / 100.           !  t/ha
        ! Biomasse potentielle de racines produites (alloperirac = taux de conversion des reserves (perisperme) en biomasse racinaire)
           masem_rac = masem * P_alloperirac
        ! Conversion biomasse de racine --> longueur racinaire
           nouvrac_sem = masem_rac * P_longsperac / 100.      ! en cm/cm2

        ! Duree de la phase heterotrophe apres levee = duree phase germination-levee
           nhet = nlev - nger + 1
        ! Longueur de racine emise par jour
           nouvracj = nouvrac_sem/(2 * nhet - 1)
        endif

      dltarltotf = 0.
      dltarltotg = 0.
      dltasentotf = 0.
      dltasentotg = 0.

! Modif Loic Fevrier 2017 : apres recolte et destruction de la culture on met a jour les differents "pools"
! de racines. On zappe ensuite la routine car il n'y a plus de croissance racinaire !
      if (nrec /= 0 .and. n >= nrec .and. P_code_acti_reserve == 2) return

! Ajout juillet 2018
      nsencouracf = nsencourpreracf
      nsencouracg = nsencourpreracg
      ndf = n + ndecalf
      ndg = n + ndecalg
      if (nger == 0) return

 ! Calcul de poussrac = effet Da x effet H2O x effet N x effet T x effet anoxie
      humirac_mean = 0.
      efnrac_mean = 0.
      poussracmoy = 0.
! Le calcul de poussrac se fait sur la zone qui contient des racines, cad [zsup, irac]
      do iz = zsup, irac
        !: 1. Effet Da (contrainte a la penetration racinaire) : efda
        !------------------------------------------------------------
        !- Fonction Jones et al. 1991  + these de B. Rebiere, adaptee avec la densite apparente
        !- P_daseuilbas et P_daseuilhaut : seuils min et max de densite apparente
        !- P_contrdamax : taux maximal de reduction de vitesse de croissance racinaire

        daz = dacouche(max(1,irac))
        efda = from_linear(daz,P_daseuilbas,P_daseuilhaut,1.,P_contrdamax)
        if (daz < P_dacohes) efda = daz / P_dacohes

        !: 2. Effet H2O (sensibilite a l'anoxie) : P_sensanox
        !--------------------------------------------------
        humirac_z(iz) = F_humirac(hur(iz),humin(iz),hucc(iz),P_sensrsec,P_humirac)
        poussrac(iz) = humirac_z(iz) * efda * (1. - anox(iz) * P_sensanox)
        humirac_mean = humirac_mean + humirac_z(iz)

        !: 3. Effet N sur la repartition des racines (option non activee) : efnrac
        !-------------------------------------------------------------------------
        if (P_codazorac == 1) then
          azo = nit(iz) + amm(iz)
          efnrac = from_linear(azo,P_minazorac,P_maxazorac,P_minefnra,1.)
          efnrac_z(iz) = efnrac
          efnrac_mean = efnrac_mean + efnrac
          poussrac(iz) = poussrac(iz) * efnrac
        endif
        poussracmoy = poussracmoy + poussrac(iz)
 ! Repartition de l'allocation des assimilats aux racines en fonction de la profondeur (courbe de Gauss)
        dis(iz) = P_lvfront + (1.-P_lvfront)*exp(-P_kdisrac*(iz**2))

      end do

      humirac_mean = humirac_mean/nrac
      poussracmoy = poussracmoy/nrac
      efnrac_mean = 1.
      if (P_codazorac == 1) efnrac_mean = efnrac_mean/nrac

      !- Calcul d'une ponderation incluant presence de racine et contraintes sol
      ponderation = 0.
      do iz = zsup, irac
         pondetot(iz) = max(0., dis(iz) * poussrac(iz))
         ponderation = ponderation + pondetot(iz)
      end do

 ! Calcul de la croissance totale de racines (rlj en cm/m2/jour) entre la levee et le stade nstoprac

 ! on calcule la somme des dtj (degres.jours racines) seulement a partir du stade levee
      ! Modif Loic sept 2020 : j'ajoute code_acti_reserve pour retrouver la v9
      if (P_code_acti_reserve == 1) then
        if (nlev > 0) then
            somtemprac = somtemprac + dtj(ndg)
            somtempracvie = somtempracvie + dtj(ndg)
        endif
      else
        somtemprac = somtemprac + dtj(ndg)
        somtempracvie = somtempracvie + dtj(ndg)
      endif

!      poussracmoy = poussracmoy/(int(zrac)-int(P_profsem)+2)
! Correction bug Loic janvier 2021
      if (n == nlev.and.P_code_acti_reserve == 2) then
        somtemprac = 0.
        somtempracvie = 0.
      endif
      if (n == nlev.and.P_code_acti_reserve == 1) then
        somtemprac = 0.
      endif
! Fin correction bug
      if (nstoprac == 0) then
        !: Definition d'une unite de dl racinaire urac (somme de degres.jours racine)
        urac = min(1. + (2. * somtemprac / (P_stlevamf + P_stamflax)), 3.)
        if (nlev == 0) urac = 1.
        !: Introduction de l'indice de stress de densite racinaire idzrac / NB - le 06/06
        efanoxd = 1. - (1. - idzrac) * P_sensanox

        !- Calcul de l'effet densite sur la mise en place du LAI pour les stics-plante
        efdensite_rac = 1.
        if (urac >= 1.) then
          if (lai_veille < P_laicomp) then
            efdensite_rac = 1.
          else
            !: domi - 02/07/2002: pb si GEL total densite = 0 et pb de log(0)
            !if ( densite == 0.) then
            if (abs(densite).lt.1.0E-8) then
               efdensite_rac = 0.
            else
              efdensite_rac = min(exp(P_adens * (log(densite / P_bdens))), 1.)
            endif
          endif
        else
          efdensite_rac = min(exp(P_adens * (log(densite / P_bdens))), 1.)
        endif

        ! Modif Florent Juin 2018
        ! Calcul du rlj sans nouvrac avec coef 1.e-4
         rlj = 1.e-4 * (P_draclong / (1. + exp(5.5 * (P_vlaimax - urac))) * efdensite_rac * densite * dtj(ndg) * efanoxd)
        ! rlj : cm rac.cm-2.d-1 ; P_draclong : cm rac.plant-1.degreed-1 ; densite : plant.m-2

         if (P_codtrophrac /= 3) then
           ! Option trophique : calcul d'une fonction de repartition reprac = souterrain/total
           ! la longueur de racine au niveau du front est soustraite de la longueur produite
           reprac = (repracmax-repracmin) * (exp(-kreprac * (urac - 1.))) + repracmin
          ! Loic Oct 2020 : ajout code_acti_reserve
           if (P_code_acti_reserve == 1) then
             rlj1 = reprac/(1.-reprac) * P_longsperac * 1.e-2 * (dltams - dltaremobil)
           else
             rlj1 = reprac/(1.-reprac) * P_longsperac * 1.e-2 * dltams
           endif
           ! rlj1 : cm rac.cm-2.day-1 ; P_longsperac : cm.g-1 ; dltams : t.ha-1.d-1

           if (P_codtrophrac == 1) rlj = rlj1
           if (P_codtrophrac == 2 .and. rlj > rlj1) rlj = rlj1

           ! Florent Juin 2018 : prise en compte du plus fort des stress (eau ou azote)
           ! Avant : s'il y avait un stress eau, on ne prenait pas en compte le stress azote
           ! Loic Octobre 2021 : il y a un souci car on peut diviser par 0
           if (P_code_stress_root == 1) then
             stress = amin1(inn,turfac)
             if (stress > 0) then
                 rlj = rlj / amin1(1.,stress)
             else
                 rlj = 0
             endif
           endif

           ! Loic Juin 2016: ajout d'une condition sinon nouvrac etait mis a zero avant la levee car il n'y a pas
           ! encore de biomasse produite par la plante (dltams = 0.)
         endif ! codetrophrac /= 3

         if (nlev == 0) rlj = 0.
       else ! si nstoprac > 0
         rlj = 0.
       endif  ! Fin du test nstoprac == 0
! Ajout de nouvracj a rlj le jour de levee
       ! if(n == nlev) rlj = rlj + nouvracj * (nlev - nger)
       if(n == nlev  .and. codeinstal == 0) rlj = rlj + nouvracj * (nhet)
       ! if(n > nlev .and. n <= nlev+nhet) rlj = rlj + nouvracj
       if(n > nlev .and. n <= nlev+nhet-1  .and. codeinstal == 0) rlj = rlj + nouvracj

! Modifs Loic et Bruno fevrier 2014
! Calcul de la mortalite des racines : a revoir
      if (lai == 0. .and. nouvracj == 0.) rlj = 0.
! NB - 19/02/2008: mort d'une partie des racines lors de la fauche des cultures fourrageres
!- P_coefracoupe permet de ponderer la mortalite des racines par espece (entre 0 et 1)
! Modif Loic Mai 2016: si on lie la croissance des racines a la production de biomasse et avec la simulation du turn-over
! des racines on doit pouvoir reproduire les variations de biomasse de racines. J'ajoute donc une condition.
! Modif Loic avril 2019 : modification de la condition pour que l'on ne rentre dans le code que pour la vX
! Modif Loic Fev 2021 : maintenant codemortalracine permet d'activer ou de desactiver une mortalite racinaire
! specifique due a une coupe (option reservee aux fourrages)
!      if (sioncoupe .and. P_code_acti_reserve == 2) then
      if (sioncoupe .and. P_codemortalracine.eq.1) then
          durvieracinef = debsenracf * (1 - exp(-P_coefracoupe * P_msresiduel / masecnp))
          durvieracineg = debsenracg * (1 - exp(-P_coefracoupe * P_msresiduel / masecnp))
!        else
!          durvieracinef = debsenracf * (1 - exp(-P_coefracoupe * P_msresiduel / masectot))
!          durvieracineg = debsenracg * (1 - exp(-P_coefracoupe * P_msresiduel / masectot))
!        endif
      else
        durvieracinef = debsenracf
        durvieracineg = debsenracg
      endif

! Jour du debut de senescence racinaire
      if (somtempracvie > durvieracinef .and. ndebsenracf == 0.) ndebsenracf = n
      if (somtempracvie > durvieracineg .and. ndebsenracg == 0.) ndebsenracg = n

      do iz = zsup, irac
 ! 1) Calcul de la longueur de racines emises (croissance brute)
        !: Si on est apres le stade nstoprac alors drliz = 0.
        drliz = 0.
        if (nstoprac == 0 .and. ponderation > 0.) then
   ! Distribution de la longueur racinaire au prorata de poussrac et de la profondeur (nlev>0)
            if (n >= nlev .or. codeinstal == 1) drliz = rlj * pondetot(iz) / ponderation
        endif

 ! Loic et Bruno fevrier 2014
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!        if(zrac <= P_zramif) then
!           propracf = 0.
!        else
!           propracf = amax1(0., P_propracfmax * (1. -iz/(zrac-P_zramif)))
!        endif
         propracf = P_propracfmax
        ! Modif Loic Aout 2016 : test de differentes options de calcul
!****************************************************************************************************************
! Option 1
!        drlizf = drliz*propracf
!        drlizg = drliz*(1.-propracf)
!        if (drlizf < drlsenf(iz) .and. drlizg > drlseng(iz)) then
!           drlizf = min (drliz, drlsenf(iz))
!           drlizg = drliz - drlizf
!        endif
!****************************************************************************************************************
! Option 2
!        drlizf = drliz*propracf
!        drlizg = drliz*(1.-propracf)
!        if (drlizf < drlsenf(iz) .and. drlizg > drlseng(iz)) then
!           drlizf = drliz
!           drlizg = drliz - drlizf
!        endif
!****************************************************************************************************************
! Option 3
        rl_veille = rlf_veille(iz) + rlg_veille(iz)
        propracfiz = propracf
        if(rl_veille > 0.) propracfiz = rlf_veille(iz)/rl_veille ! ratio racines fines / racines de la veille
        if (propracfiz < propracf) then
            drlizf = drliz              ! length of new fine roots emitted on day n in layer iz
            drlizg = 0.                 ! length of new coarse roots emitted on day n in layer iz
        else
            drlizf = drliz*propracf
            drlizg = drliz*(1.-propracf)
        endif
!****************************************************************************************************************
!       drlf(ndf,iz) = drlizf
!       drlg(ndg,iz) = drlizg
        drlf(iz,ndf) = drlizf
        drlg(iz,ndg) = drlizg

 ! 2a) Calcul de la senescence racinaire des racines fines
        if (ndebsenracf == 0) then
            drlsenf(iz) = 0.
        else
! Recherche du dernier jour de senescence des racines fines (nsencouracf)
            stsen = 0.
            do i = 1, ndf
                stsen = stsen + dtj(ndg-i)          ! remplace i par i+1  ***** a verifier ! *****
                if (stsen >= durvieracinef) then
                  nsencouracf = ndf-i               ! remplace i par i+1  ***** a verifier ! *****
                  EXIT ! on sort de la boucle
                endif
            end do
! Cumul de longueur racinaire emise entre nsencourprerac et nsencourac
            if (nsencourpreracf < nsencouracf) then
               drlsenf(iz) = 0.
               do i = nsencourpreracf+1, nsencouracf
 !                 drlsenf(iz) = drlsenf(iz) + drlf(i,iz)
                   drlsenf(iz) = drlsenf(iz) + drlf(iz,i)
               end do
            else
               drlsenf(iz) = 0.
            endif
        endif

! 2b) Calcul de la senescence racinaire des grosses racines
        if (ndebsenracg == 0) then
            drlseng(iz) = 0.
        else
! Recherche du dernier jour de senescence des grosses racines (nsencouracg)
            stsen = 0.
            do i = 1, ndg
                stsen = stsen + dtj(ndg-i)             ! remplace i par i+1  ***** a verifier ! *****
                if (stsen >= durvieracineg) then
                  nsencouracg = ndg-i                  ! remplace i par i+1  ***** a verifier ! *****
                  EXIT ! on sort de la boucle
                endif
            end do
! Cumul de longueur racinaire emise entre nsencourprerac et nsencourac
            if (nsencourpreracg < nsencouracg) then
               drlseng(iz) = 0.
               do i = nsencourpreracg+1, nsencouracg
                   drlseng(iz) = drlseng(iz) + drlg(iz,i)
               end do
            else
               drlseng(iz) = 0.
            endif
        endif  ! fin test ndebsenracg = 0

! 3) Accroissement net des racines dans chaque couche
!    l'accroissement de densite racinaire commence au jour nlev (au lieu de nger)

        if (n >= nlev .or. codeinstal == 1) then
      ! SYL 120907 Mortalite d'une proportion de racine en lien avec la mortalite de talles
          if (P_codedyntalle == 1) drlsenf(iz) = drlsenf(iz) + rlf_veille(iz) * drlsenmortalle
          if (P_codedyntalle == 1) drlseng(iz) = drlseng(iz) + rlg_veille(iz) * drlsenmortalle
          rlf(iz) = rlf_veille(iz) + drlizf - drlsenf(iz)
          rlg(iz) = rlg_veille(iz) + drlizg - drlseng(iz)

        !: sb - 06/03/2007 :
        !- Lorsque la quantite de racine devient tres faible, c'est que les racines ont disparu.
        !- En pratique, dans ce cas, la longueur de racine ne devient jamais exactement egale a 0
        !- en raison de la facon de calculer la senescence et des erreurs d'arrondis. Cette longueur est
        !- mise a 0 seulement dans les cas ou le calcul la donne negative. Pour eviter les imprecisions
        !- et instabilite numeriques que cela genere, on a choisi de fixer la longueur de racine a 0 des
        !- qu'elle est inferieure a une quantite tres faible.

          if (rlf(iz) < 1.d-10) then
            rlf(iz) = 0.
            drlsenf(iz) = rlf_veille(iz) + drlizf
          endif
           if (rlg(iz) < 1.d-10) then
            rlg(iz) = 0.
            drlseng(iz) = rlg_veille(iz) + drlizg
          endif
          lracsenzf(iz) = lracsenzf(iz) + drlsenf(iz)
          lracsenzg(iz) = lracsenzg(iz) + drlseng(iz)
        endif

        lracz(iz) = rlf(iz) + rlg(iz)
        dltarltotf = dltarltotf + drlizf
        dltarltotg = dltarltotg + drlizg
        dltasentotf = dltasentotf + drlsenf(iz)
        dltasentotg = dltasentotg + drlseng(iz)

      end do ! fin boucle sur la profondeur

! Calcul de la densite efficace de racines vis a vis de l'absorption d'eau et d'azote
      cumlracz = 0.
      cumflrac = 0.
      do iz = zsup, irac
        flrac(iz) = min(lracz(iz),P_lvopt) / P_lvopt

        lracz(iz) = min(lracz(iz),P_lvopt) * F_humirac(hur(iz),humin(iz),hucc(iz),0.,P_humirac)
        cumlracz = cumlracz + lracz(iz)
        cumflrac = cumflrac + flrac(iz)
        cumlraczmaxi = cumlracz
        racnoy = racnoy + flrac(iz) * anox(iz)
      end do

      !: Test sur la densite racinaire
!      if (cumlracz <= 0 .and. nrec == 0) call EnvoyerMsgHistorique(logger, MESSAGE_31, n)
      if (cumlracz <= 0 .and. nrec == 0 .and. nlev > 0) then
      call EnvoyerMsgHistorique(logger, MESSAGE_31, n)
      call EnvoyerMsgHistorique(logger, MESSAGE_31,cumlracz)
      call EnvoyerMsgHistorique(logger, MESSAGE_31,cumflrac)
      endif

      nsencourpreracf = nsencouracf
      nsencourpreracg = nsencouracg
      lracsentotf = 0.
      lracsentotg = 0.
      rltotf = 0.
      rltotg= 0.

      do iz = 1, profsol
        if (sioncoupe) lracsenzf(iz) = 0.
        if (sioncoupe) lracsenzg(iz) = 0.
        lracsentotf = lracsentotf + lracsenzf(iz)
        lracsentotg = lracsentotg + lracsenzg(iz)
        rltotf = rltotf + rlf(iz)
        rltotg = rltotg + rlg(iz)
        rlf_veille(iz) = rlf(iz)
        rlg_veille(iz) = rlg(iz)
      end do
      !: NB - 20/02/08 : recalcul de zrac
      ! Loic Avril 2019 : je remets la mise a jour de Zrac pour comparaison avec la v9
      ! j'ajoute l'option code_acti_reserve pour que ca ne s'applique pas au nouveau parametrage de la luzerne
      if (sioncoupe .and. P_code_acti_reserve == 2) then
        do iz = int(zrac), 10, -1
          if (rlg(iz) < 0.1 * P_lvfront) then
            zrac = iz
          endif
        end do
      endif
return
end subroutine densirac2
end module densirac2_m
