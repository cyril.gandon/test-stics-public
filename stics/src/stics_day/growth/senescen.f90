! *******************************************************************
!    version 6.0
!    reste pb cultures fauchees a voir avec Domi
!--------------------------------------------------
!    calcul de la senescence de la matiere seche
!    avec eventuellement une partie residuelle pour les cultures fauchees
!
!    calcul de la senescence du LAI pour l'option LAI brut
! derniere modif 30/05/05
! *******************************************************************
! In STICS shoot senescence only concerns leaves: dry matter and LAI.  For cut crops, it also affects residual biomass after cutting.
! - Stics book paragraphe 3.1.2, page 44-46
! While in the first versions of the model senescence was implicit (Brisson et al., 1998a), it is now explicit, with a clear distinction between natural
!! senescence due to the natural ageing of leaves, and senescence accelerated by stresses (water, nitrogen, frost). The concept of leaf lifespan,
!! used for example by Maas (1993), is applied to the green leaf area and biomass produced. The leaf area and part of the leaf biomass produced on a given day
!! is therefore lost through senescence once the lifetime has elapsed (Duru et al., 1995). This part corresponds to the ratiosen parameter, taking into account
!! the part which was remobilised during its senescence.
!!
!! Calculation of lifespan:
!!
!! The natural lifespan of leaves (durage) is defined by two values: the  lifespan of early leaves, or durvieI (expressed as a proportion of durvieF ) and the
!! lifespan of the last leaves emitted, or durvieF (assumed genotype-dependent). Until the IAMF stage, the natural lifespan, calculated for the day when the
!! leaves are emitted (I0) is durvieI; from IAMF to ILAX, the natural lifespan increases between durvieI and durvieF as a function of the leaf development variable ULAI.
!! Because of water or nitrogen stress, the current lifespan may be shortened if the stress undergone is more intense than previous stresses.
!! Specific stress indices for senescence are introduced (senfac and innsenes).  Frost (fstressgel that can be either fgeljuv or fgelveg) may also reduce or
!! even cancel  lifespan.  In the event of over-fertilisation with nitrogen (inn >1), the foliage lifespan is increased from the IAMF stage up to a maximum
!! given by the durviesupmax parameter.
!! The lifespan of leaves is not expressed in degree.days (like phasic development), because this has the disadvantage of stopping any progression as soon as
!! the temperature is lower than the base temperature (tdmin).  To remedy this problem, the senescence course (somsen) is expressed by cumulated Q10 units
!! (with Q10=2), i.e. an exponential type function.
!! The senescence course between I0 and I is affected by the same cardinal temperatures as phasic development and can be slown down by stresses. The lifespan parameter
!! of the leaf (durvieF) expressed in Q10 units represents about 20% of the same  lifespan expressed in degree.days.
!!
!! Calculation of senescence:
!!
!! Material produced on day I0 disappears by senescence after a period corresponding to durvie(I0). Depending on the evolution of temperature and of lifespan
!! as a function of phenology and stresses, senescence can vary from one day to another and affect several days of production (J=I0, I0+1, ) or, on the contrary,
!! none (durvieE(I0)>somsen(I)).  This principle is applied to the biomass (dltamsen) and leaf area index (dltaisen). In general, the leaf biomass produced
!! does not completely disappear (remobilisation):  the ratiosen (<1) parameter enables the definition of the senescent proportion with reference to production.
!! It is the pfeuilverte ratio which defines the proportion of leaves in the biomass produced.
!! The cumulative senescent foliage is laisen. If the crop is a forage crop benefiting from residual dry matter from the previous cycle (msresiduel parameter),
!! the senescence of residual dry matter (deltamsresen) starts as from cutting.  It occurs at a rate estimated from the relative daily lifespan course and
!! weighted by the remobilisation (ratiosen).
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module senescen_m
USE GEL_m, only: GEL
use messages
use messages_data
use stressphot_m, only: stressphot
use plant_utils
implicit none
private
public :: senescen
contains
subroutine senescen(logger, nlev, n, nbjmax, lai, P_codeinnact, P_codeh2oact, senfac, innsenes, P_codlainet, P_codeperenne,   &
  nbfeuille, P_nbfgellev, P_codgellev, tcultmin, P_tletale, P_tdebgel, P_tgellev90, P_tgellev10, ipl, densitelev,        &
                    densiteassoc, P_codgeljuv, P_tgeljuv90, P_tgeljuv10, P_codgelveg, P_tgelveg90, P_tgelveg10,                 &
                    nstopfeuille, somcour, restemp, ndrp, nrec, ndes, P_QNpltminINN, numcult, P_codeinitprec, ulai, P_vlaimax,  &
                    durvieI, P_durvieF, inn, P_durviesupmax, P_codestrphot, phoi, P_phobasesen, dltams, P_msresiduel,           &
                    P_ratiosen, tdevelop, somtemp, pfeuilverte, deltai, lai_to_sen, dernier_n, nsencour, dltaisen, dltamsen,    &
                    fstressgel, fgellev, gelee, densite, laisen, nlan, P_stsenlan, nsen, P_stlaxsen, namf, nlax, P_stlevamf,    &
                    P_stamflax, nrecbutoir, mortplante, nst2, mortplanteN, durvie, strphot, msres, dltamsres, ndebsen,          &
                    somsenreste, msresjaune, QNplantenp, P_dltamsminsen, P_dltamsmaxsen, P_alphaphot,             &
                    strphotveille, cumdltaremobsen, nstopres, dltaremobsen, mafeuilverte, codeinstal, P_code_acti_reserve,      &
                    laiveille, mafeuiljaune, nmat, P_lai0, P_codeplante)
  type(logger_), intent(in) :: logger
  integer, intent(INOUT) :: nlev
  integer, intent(IN)    :: n  
  integer, intent(IN)    :: nbjmax  
  real,    intent(INOUT) :: lai   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0 
  integer, intent(IN)    :: P_codeh2oact  ! // PARAMETER // code to activate  water stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0 
  real,    intent(IN)    :: senfac   ! // OUTPUT // Water stress index on senescence // 0-1
  real,    intent(IN)    :: innsenes   ! // OUTPUT // Index of nitrogen stress active on leaf death // P_innmin to 1
  integer, intent(IN)    :: P_codlainet  ! // PARAMETER //option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0 
  integer, intent(IN)    :: nbfeuille   ! // OUTPUT // Number of leaves on main stem // SD
  integer, intent(IN)    :: P_nbfgellev  ! // PARAMETER // leaf number at the end of the juvenile phase (frost sensitivity)  // nb pl-1 // PARPLT // 1 
  integer, intent(IN)    :: P_codgellev  ! // PARAMETER // activation of plantlet frost // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: tcultmin  
  real,    intent(IN)    :: P_tletale  ! // PARAMETER // lethal temperature for the plant // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tdebgel  ! // PARAMETER // temperature of frost beginning // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgellev90  ! // PARAMETER // temperature corresponding to 90% of frost damage on the plantlet  // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgellev10  ! // PARAMETER // temperature corresponding to 10% of frost damage on the plantlet  // degree C // PARPLT // 1
  integer, intent(IN)    :: ipl  
  real,    intent(IN)    :: densitelev  
  real,    intent(IN)    :: densiteassoc  
  integer, intent(IN)    :: P_codgeljuv  ! // PARAMETER // activation of LAI frost at the juvenile stadge // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_tgeljuv90  ! // PARAMETER // temperature corresponding to 90 % of frost damage on the LAI (juvenile stage) // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgeljuv10  ! // PARAMETER // temperature corresponding to 10 % of frost damage on the LAI (juvenile stage) // degree C // PARPLT // 1
  integer, intent(IN)    :: P_codgelveg  ! // PARAMETER // activation of LAI frost at adult stage // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_tgelveg90  ! // PARAMETER // temperature corresponding to 90 % of frost damage on the LAI (adult stage) // degree C // PARPLT // 1
  real,    intent(IN)    :: P_tgelveg10  ! // PARAMETER // temperature corresponding to 10 % of frost damage on the LAI (adult stage) // degree C // PARPLT // 1
  integer, intent(IN)    :: nstopfeuille  
  real,    intent(INOUT) :: somcour   ! // OUTPUT // Cumulated units of development between two stages // degree.days
  integer, intent(IN)    :: ndrp  
  integer, intent(INOUT) :: nrec
  integer, intent(INOUT) :: ndes
  integer, intent(IN)    :: nmat
  real,    intent(IN)    :: P_QNpltminINN  ! // PARAMETER // minimal amount of nitrogen in the plant allowing INN computing // kg ha-1 // PARAM // 1 
  integer, intent(IN)    :: numcult  
  integer, intent(IN)    :: P_codeinitprec  ! // PARAMETER // reinitializing initial status in case of chaining simulations : yes (1), no (2) // code 1/2 // PARAM // 0 
  ! dans l'ideal max(n,dernier_n) ! (dernier_n)
  real,    intent(IN)    :: ulai(nbjmax)  !   // OUTPUT // Daily relative development unit for LAI // 0-3
  real,    intent(IN)    :: P_vlaimax  ! // PARAMETER // ULAI at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  real,    intent(IN)    :: durvieI  
  real,    intent(IN)    :: P_durvieF  ! // PARAMETER // maximal  lifespan of an adult leaf expressed in summation of P_Q10=2 (2**(T-Tbase)) // P_Q10 // PARPLT // 1 
  real,    intent(IN)    :: inn   ! // OUTPUT // Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
  real,    intent(IN)    :: P_durviesupmax  ! // PARAMETER // proportion of additional lifespan due to an overfertilization // SD // PARPLT // 1 
  integer, intent(IN)    :: P_codestrphot  ! // PARAMETER // activation of the photoperiodic stress on lifespan : yes (1), no (2) // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: phoi   ! // OUTPUT // Photoperiod // hours
  real,    intent(IN)    :: P_phobasesen  ! // PARAMETER // photoperiod under which the photoperiodic stress is activated on the leaf lifespan // heures // PARPLT // 1 
  ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(INOUT) :: dltams(nbjmax)  !   // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,    intent(IN)    :: P_msresiduel  ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1 
  real,    intent(IN)    :: P_ratiosen  ! // PARAMETER // fraction of senescent biomass (by ratio at the total biomass) // between 0 and 1 // PARPLT // 1 
  real,    intent(IN)    :: tdevelop(n) ! 1 to n
  real,    intent(IN)    :: somtemp   ! // OUTPUT // Sum of temperatures // degree C.j
  ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(IN)    :: pfeuilverte(nbjmax) !  // OUTPUT // Proportion of green leaves in total non-senescent biomass // 0-1
   ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(IN)    :: deltai(nbjmax)  !   // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real,    intent(IN)    :: P_lai0  ! // PARAMETER // Initial leaf area index // m2 m-2 // INIT // 1
  character(len=3), intent(IN) :: P_codeplante ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0

  integer, intent(INOUT) :: dernier_n  
  integer, intent(INOUT) :: nsencour  
  real,    intent(INOUT) :: dltaisen   ! // OUTPUT // Daily increase of the senescent leaf index // m2.m-2 sol.j-1
  real,    intent(INOUT) :: dltamsen   ! // OUTPUT // Senescence rate // t ha-1 j-1
  real,    intent(INOUT) :: fstressgel   ! // OUTPUT // Frost index on the LAI // 0-1
  real,    intent(INOUT) :: fgellev  
  logical, intent(INOUT) :: gelee  
  real,    intent(INOUT) :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
  real,    intent(INOUT) :: laisen(0:1) ! veille (0), aujourd'hui (1)    // OUTPUT // Leaf area index of senescent leaves // m2 leafs  m-2 soil
  integer, intent(INOUT) :: nlan  
  real,    intent(INOUT) :: P_stsenlan  ! // PARAMETER // Sum of development units between the stages SEN et LAN // degree.days // PARPLT // 1 
  integer, intent(INOUT) :: nsen  
  real,    intent(INOUT) :: P_stlaxsen  ! // PARAMETER // Sum of development units between the stages LAX and SEN // degree.days // PARPLT // 1 
  integer, intent(INOUT) :: namf  
  integer, intent(INOUT) :: nlax  
  real,    intent(INOUT) :: P_stlevamf  ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1 
  real,    intent(INOUT) :: P_stamflax  ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1 
  integer, intent(INOUT) :: nrecbutoir  
  integer, intent(INOUT) :: mortplante  
  integer, intent(INOUT) :: nst2  
  integer, intent(INOUT) :: mortplanteN  
  ! dans l'ideal max(n,dernier_n) ! (dernier_n) ! ndebsen a dernier_n, puis si n < ndebsen, 1 a n
  real,    intent(INOUT) :: durvie(nbjmax) !   // OUTPUT // Actual life span of the leaf surface //  degree C
  real,    intent(OUT)   :: strphot  
  real,    intent(INOUT) :: msres  
  real,    intent(INOUT) :: dltamsres  
  integer, intent(INOUT) :: ndebsen  
  real,    intent(INOUT) :: somsenreste  
  real,    intent(INOUT) :: msresjaune   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
!  real,    intent(INOUT) :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  real,    intent(INOUT) :: restemp   ! // OUTPUT // C crop reserve, during the cropping season // t ha-1

  real,    intent(IN)    :: QNplantenp   ! // OUTPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1

! STRESS PHOTOPERIODIQUE
  real,    intent(IN)    :: P_dltamsminsen  ! // PARAMETER // threshold value of deltams from which the photoperiodic effect on senescence is maximal // t ha-1j-1 // PARAM // 1 
  real,    intent(IN)    :: P_dltamsmaxsen  ! // PARAMETER // threshold value of deltams from which there is no more photoperiodic effect on senescence // t ha-1j-1 // PARPLT // 1 
  real,    intent(IN)    :: P_alphaphot  ! // PARAMETER // parameter of photoperiodic effect on leaf lifespan // P_Q10 // PARAM // 1 
  real,    intent(INOUT) :: strphotveille  
! -- FIN STRESS PHOTOPERIODIQUE
! Ajout Loic Juin 2016
  real,    intent(INOUT) :: cumdltaremobsen ! // cumulative amount of leaves biomass remobilized during senescence t ha-1
  integer, intent(INOUT) :: nstopres
! Modif Loic et Bruno fevrier 2013 : mise a jour du pool restemp
  real,    intent(OUT)   :: dltaremobsen ! // Leaves biomass remobilized during senescence t ha-1 j-1
  real,    intent(IN)    :: mafeuilverte
  integer, intent(IN)    :: codeinstal
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(INOUT) :: laiveille
  real,    intent(IN)    :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  !real,    intent(IN)    :: masecveg
  real,    intent(IN)    :: lai_to_sen ! lai for senescence in case au cut crop

  integer :: i  !  
  integer :: ncompteur  !  
  integer :: debut  !  
  integer :: fin  
  real    :: vitsenres  !  
  real    :: durage  !  
  real    :: durviesup  !  
  real    :: senstress  !  
  real    :: somsen  !  
  real    :: msresTMP  

      !:On shunte ce programme si la plante n'est pas levee ou si le lai est nul
 logical :: dbg_sen
 dbg_sen=.FALSE.
if (dbg_sen) write(1111,*) 'dltams',dltams(1:40)
if (dbg_sen) write(1111,*) 'dltams',dltams(41:60)

      if (nlev == 0) then
        ! *- 07/09/06 DR et NB et IGC (la sainte famille)
        ! *- apres un 2 jours d'errements nadine a ete notre lumiere !!
        ! *- Qu'elle en soit louee !
        ! *- initialisation de nsencour au debut du cycle
        nsencour = n
        return
      endif

      !: PB - 08/03/2005 - si encore de la matiere jaune on continue la senescence
      ! Modif Loic juin 2018 : je ne comprends pas pourquoi on calcule encore la senescence alors
      ! que le lai est nul et qu'il n'y a plus de biomasse de feuilles vertes... Je modifie la condition!
!      if (lai <= 0 .and. mafeuiljaune <= 0.) then
      if (P_code_acti_reserve == 1) then
        if (lai <= 0 .and. mafeuilverte <= 0.) then
          dltaisen = 0.
          dltamsen = 0.
          dltaremobsen = 0.
          return
        endif
      else
        if (lai <= 0 .and. mafeuiljaune <= 0.) then
          dltaisen = 0.
          dltamsen = 0.
          ! Loic sept 2020 : pour coller � la v9...
          ! dltaremobsen = 0.
          return
        endif
      endif

! ** calcul de la duree de vie
! *---------------------------
! *-  a) calcul du LAI net : on utilise version 4 avec une duree de vie fixe (stlevsenms)
! *-  b) calcul du LAI brut: la duree de vie est fonction de l'age de la plante
! *-                         (duree de vie potentielle x facteurs de stress)
! *-   duree de vie potentielle des feuilles (durage) calculee dans calai
! *-   facteurs de stress eau et azote calcules a partir de senfac (transpi.for)
! *-   et innsenes (absorn.for)
! *-------------------------------------------------------------------
! *- specifiques de la senescence


! ** Introduction codeHxN - NB - le 12/04/05
! *- ML - le 29/05/07 : suppression de la possibilite de multiplier les deux stress
! *- hydrique et azote pour reduire la duree de vie des feuilles;
! *- donc suppression du codeHxN
!      if (P_codeinnact == 1 .and. P_codeh2oact == 1) then
!         if (codeHxN == 1) then
!           senstress = min(senfac,innsenes)
!         else
!           senstress = senfac*innsenes
!         endif
!      endif

      if (P_codeinnact == 1 .and. P_codeh2oact == 1) then
        senstress = min(senfac,innsenes)
      endif

      if (P_codeinnact == 2 .and. P_codeh2oact == 1) then
        senstress = min(senfac,1.)
      endif

      if (P_codeinnact == 1 .and. P_codeh2oact == 2) then
        senstress = min(1.,innsenes)
      endif

      if (P_codeinnact == 2 .and. P_codeh2oact == 2) senstress = 1.

      if (P_codlainet == 1) senstress = 1.


      if(dbg_sen) write(1111,*)'senstress = min(senfac,innsenes)',senstress,senfac,innsenes

! ** calcul du STRESS lie au GEL  (utilisation de la fonction GEL.for)
! *-------------------------------------------------------------------
! *- GEL entre levee et plantule diminue la densite de population uniquement pour les annuelles
! Modif bug Loic Octobre 2016: Attention pour la luzerne codeperenne = 1... J'ajoute codeinstal
!      if (P_codeperenne == 1)  then
! Attention : teste pour la luzerne mais verifier la genericite
! Loic sept 2020 : ajout code acti reserve pour retrouver la v9
      if (P_code_acti_reserve == 1) then
         if (P_codeperenne == 1.and.codeinstal == 0) then
            if (nbfeuille <= P_nbfgellev) then
                fgellev = GEL(P_codgellev,tcultmin,P_tletale,P_tdebgel,P_tgellev90,P_tgellev10)
                if (fgellev < 1.) gelee = .TRUE.
                if (ipl == 1) then
                    densite = min(densite, densitelev*fgellev)
                else
                    densite = min(densite, (densitelev + densiteassoc)*fgellev)
                endif
            endif
         endif
      else
         if (P_codeperenne == 1) then
            if (nbfeuille <= P_nbfgellev) then
                fgellev = GEL(P_codgellev,tcultmin,P_tletale,P_tdebgel,P_tgellev90,P_tgellev10)
                if (fgellev < 1.) gelee = .TRUE.
                if (ipl == 1) then
                    densite = min(densite, densitelev*fgellev)
                else
                    densite = min(densite, (densitelev + densiteassoc)*fgellev)
                endif
            endif
         endif
      endif

! ** GEL de l'appareil vegetatif a deux stades: juvenil (avant AMF) et adulte (apres AMF)
      if (namf <= 0) then
        fstressgel = GEL(P_codgeljuv,tcultmin,P_tletale,P_tdebgel,P_tgeljuv90,P_tgeljuv10)
      else
        fstressgel = GEL(P_codgelveg,tcultmin,P_tletale,P_tdebgel,P_tgelveg90,P_tgelveg10)
      endif

      if (fstressgel < 1.) gelee = .TRUE.

! ** GEL LETAL pour la plante
      if (fstressgel <= 0.) then
         dltaisen  = lai
! Modif Loic Septembre 2016 : dltamsen correspond normalement a la biomasse de feuilles senescentes.
! DR et LS le 24/05/2022 : visiblement ce n'est pas masecveg qui faut faire senescer mais bien mafeuilverte dans tous les cas
!         if (P_code_acti_reserve == 2) then
!            dltamsen  = masecveg
!         else
            dltamsen = mafeuilverte
!         endif
         laisen(1) = laisen(0) + dltaisen


      if( dltaisen.gt.4) then
        call EnvoyerMsgDebug(logger, "Warning, dltaisen greater than 4: " // to_string(dltaisen))
      endif

! Ajout Bruno fevrier 2018
! si le gel des feuilles est total, si c'est une plante annuelle, toute la plante meurt le lendemain (racines incluses)
         if (P_codeperenne == 1 .and. nrec == 0) then
         ! DR 16/03/2020 en pleine crise coronavirus , les plantes ne sont plus detruites avant d'etre recolt�es
         ! if on a eu un gel total mais qu'on a depass� amf on ne detruit pas la recolte car au canada ils recoltent, on retourne au sol seulement les reliquats habituels
! DR et LS le 24/05/2022 cette specificit� du mais canadien pose pb pour les plantes type colza ou moutarde qui sont totalement detruites en cas de gel
! on ajoute un test pour dire que c'est si on est apres maturit� et si la plante est du mais .
!          if (n .gt. nmat.and.nrec == 0 )then
          if (n .gt. nmat .and. nmat .gt. 0. .and. nrec == 0 .and. P_codeplante.eq.CODE_MAIZE)then
          ! on ne fait pas de destruction
          else
            ndes = n+1
            restemp = 0.
          endif
         endif

! ** affectation des stades
         if (nstopfeuille > 0 .and. nlan == 0) then
            nlan = n
            P_stsenlan = somcour
            if (nsen == 0) then
               nsen = n
               P_stlaxsen = somcour
               P_stsenlan = 0.
            endif
         else
            if (restemp <= 0.and.P_codeperenne == 1) then
               if (namf == 0) then
                  namf = n
                  nlax = n
                  nsen = n
                  nlan = n
                  P_stlevamf = somcour
                  P_stamflax = 0.
                  P_stlaxsen = 0.
                  P_stsenlan = 0.
               endif
               if (nlax == 0 .and. namf > 0.and.P_codeperenne == 1) then
                  nlax = n
                  nsen = n
                  nlan = n
                  P_stamflax = somcour
                  P_stlaxsen = 0.
                  P_stsenlan = 0.
               endif
               if (nsen == 0 .and. nlax > 0.and.P_codeperenne == 1) then
                  nsen = n
                  nlan = n
                  P_stlaxsen = somcour
                  P_stsenlan = 0.
               endif
            endif
         endif
         if (restemp <= 0 .and. P_codeperenne == 1) then
            call EnvoyerMsgHistorique(logger, MESSAGE_1167)
          !: modif - NB - 20/09 - mort le jour d'apres pour ecriture rapport
            nrecbutoir = n+1
            mortplante = 1
          !: pour non plantade dans bilan.for - TODO : a mettre ici ou dans bilan ?
            if (ndrp == 0) nst2 = 1
         endif
      ! Ajout Loic Septembre 2016: mise a jours des pools & des stades pour les plantes perennes a reserves
         if (P_code_acti_reserve == 1 .and. P_codeperenne == 2) then
            restemp = 0.
            dltaremobsen = 0.
            nsencour = n
            dltams = 0.
            laiveille = 0.
            nlev = n
            namf = 0
            nlax = 0
            nsen = 0
            nlan = 0
            nstopres = 0
            somcour = 0.
         endif
        goto 30
      ! Fin ajout
      endif ! Fin gel lethal

      if (n == nlev .and. P_codeperenne == 1) then
        nsencour = nlev
      endif

      ! *- DR et NB - 25/08/08 - s'il n'y a plus d'azote dans la plante on la fait mourir
      ! DR 11/04/2012 pour le prairie on a des pbs car quand on coupe on a plus d'azote dans la  plante
      ! donc je mets < au lieu de <=
      if (namf > 0 .and. nrec == 0 .and. QNplantenp < P_QNpltminINN) then
        call EnvoyerMsgHistorique(logger, MESSAGE_2099)
      ! modif NB 20/09 mort le jour d'apres pour ecriture rapport
        nrecbutoir  = n+1
        mortplanteN = 1
      ! pour non plantade dans bilan.for
        if (ndrp == 0) nst2 = 1
      endif

! *- NB - le 22/12/2003 - pb senescence perenne vu avec FR
! *- PB - 05/01/2005 - mise en commentaire jusqu'a nouvel ordre.
! *- Sinon cela perturbe le calcul de la senescence lors de l'enchainement des perennes.
      if (n == nlev .and. P_codeperenne == 2 .and. numcult == 1) then
        nsencour = 1
      endif
! 11/01/2017 pour la senescence des perennes on demarre la senescence le premier jour toutes les annees si on est en reinitialisation
      if (n == nlev .and. P_codeperenne == 2 .and. P_codeinitprec == 1) then
        nsencour = 1
      endif

! ** calcul de la duree de vie
! *----------------------------
! *-  nsenscour = jour ou la biomasse senescente a ete produite
! *-  durage = duree de vie potentielle fonction de la P_phenologie
! *-  (varie entre durvieI et P_durvieF; durvieI avant lax)

      debut = nsencour
      if (P_codeperenne == 2 .and. P_codeinitprec == 2 .and. numcult > 1 .and. nsencour > n) then
        fin = dernier_n
      else
        fin = n
        dernier_n = n
      endif

      if(dbg_sen)  write(1111,*)'i, pour le calcul de durvie',debut,fin
5     do i = debut, fin
! 	write(*,*)'i',debut,fin
        if (ulai(i) <= P_vlaimax) then
          durage = durvieI
        else
          durage = durvieI + ((ulai(i) - P_vlaimax) / (3. - P_vlaimax)) * (P_durvieF - durvieI)
        endif
        if(dbg_sen) write(1111,*)n,'durage', i, durvieI,durage,ulai(i)
        ! ** application des stress qui raccourcissent la duree de vie
        if (i < fin) then
          durvie(i) = min(durvie(i), durage * min(senstress, fstressgel))
        else
          durvie(i) = durage * min(senstress, fstressgel)
        endif
          if(dbg_sen)  write(1111,*)n,'=i=',i, 'durvie(i)',durvie(i), 'durage ', durage,'stress',senstress, fstressgel,'namf',namf
        ! ** augmentation de duree de vie maxi en cas de surfertilisation azotee
        if (namf > 0 .and. i > namf .and. P_codlainet == 2) then
          if (inn > 1.) then
            durviesup = P_durvieF * min(P_durviesupmax, (inn - 1.))
            ! write(1111,*)n,'durviesup surferti',P_durvieF,durviesup
          else
            durviesup = 0.
          endif
        else
          durviesup = 0.
        endif
        durvie(i) = durvie(i) + durviesup
        ! write(1111,*)n,'durvie(i) durvisup',durvie(i), durviesup,inn
        ! domi 04/01/06 pour inaki insertion du stress photoperiodique
        ! NB le 22/06/06 introduction dans la boucle des "i"
        if (P_codestrphot == 1 .and. nlax /= 0) then
          if (phoi < P_phobasesen) then
            call stressphot(dltams(n), P_dltamsminsen, P_dltamsmaxsen, P_alphaphot, strphot, strphotveille)
            durvie(i) = durvie(i) * strphot
            if (dbg_sen) write(1111,*)n,'durvie(i) strphot',durvie(i) ,strphot
          endif
        endif
         ! write(1111,*)n,'durvie(i) strphot',durvie(i) ,strphot
      end do
       if(dbg_sen) write(1111,*)n,'fin do i , durvie(n) : ', durvie(n),' durvie(i) ',(durvie(i),i=debut,fin)

      if (P_codeperenne == 2 .and. P_codeinitprec == 2 .and. numcult > 1 .and. debut > n) then
        debut = 1
        fin = n
        goto 5
      endif

      ! ** la base de temps de la senescence est calculee dans develop.for
      ! *- tdevelop(n) est un P_Q10 somme dans la variable somtemp

      ! ** pour les cultures fauchees : senescence de la matiere seche residuelle
      ! *- qui demarre des la coupe
      ! *- PB - 25/01/2004 - ajout de msresTMP et dltamsres pour calculer le delta msres(n-1)<->msres(n)
      ! *- et ce, pour pallier a un probleme dans le calcul de msresjaune pour les perennes enchainees fauchees
      if (msres > 0.) then
        vitsenres = P_msresiduel / durvieI
        msresTMP = msres - (P_ratiosen * vitsenres * tdevelop(n))
        msresTMP = max(0.,msresTMP)
        dltamsres = max(0., msres - msresTMP)
        msres = msresTMP
      else
        dltamsres = 0.
      endif



      ! ** pour les autres cultures ou pour la matiere seche neoformee


      ! ** DOMI VOIR POUR PRAIRIE UTILISATION DE somcourfauche
      ! ** 2/ senescence de la matiere seche neoformee qui demarre au stade
      ! *- debut de senescence de la matiere seche identifie par le parcours
      ! *- de developpement depuis la levee : stlevsenms
      ! *- on calcule vitsen le jour du debut de senescence : ndebsen
      if(dbg_sen) write(1111,*)'*** n avant calcul senescence',n,debut,fin, durvie(i), durvie(n), somtemp
       ! write(1111,*)'avant calcul scen somtemp',n,somtemp,'durvie(n)',durvie(n)
      if (somtemp >= durvie(n)) then
         if(dbg_sen) write(1111,*)'premier jour scen somtemp',n,somtemp,'durvie(n)',durvie(n)
        if (ndebsen == 0) then ! premier jour de senescence
          ndebsen = n
          nsencour = nlev
          dltamsen =  dltams(nsencour) * P_ratiosen * pfeuilverte(nsencour)
          ! Modif Loic et Bruno fevrier 2013 : fraction remobilisable du flux de senescence (vers restemp)
          ! Loic sept 2020 : ajout code acti reserve
          if (P_code_acti_reserve == 1) dltaremobsen =  dltams(nsencour) * (1.-P_ratiosen) * pfeuilverte(nsencour)

          ! Ajout Loic Aout 2016: il y avait un bug car on arretait la mise en reserve de l'azote mais
          ! pas la mise en reserve de C...
          if (nstopres > 0.) then
             dltamsen = dltams(nsencour) * pfeuilverte(nsencour)
             dltaremobsen = 0.
          endif
          ! Fin ajout Loic
          if (P_codlainet == 2) then
! Remarque Loic Juillet 2016: ne faudrait-il mieux pas enregistrer nsencour et l'age des feuilles pour les cultures fauchees?
! DR 24/03/2020 en plein confinement ce n'est pas P_lai0 qu'il faut faire senescer mais le lairesiduel si on a eu des coupe precedemment
! Loic sept 2020 : je suis ok avec toi Domi mais dans la v9 c'est P_lai0 ^^ A modifier dans la v9.0
            if (P_code_acti_reserve == 1) then
                dltaisen = deltai(nsencour) + lai_to_sen
            else
                dltaisen = deltai(nsencour) + P_lai0
            endif
            if(dbg_sen) write(1111,*)'jour 1 ==== dltaisen ', dltaisen
            if(dbg_sen) write(1111,*)'n nsencour deltai(nsencour) lai_to_sen dltaisen ', &
                                       n, nsencour, deltai(nsencour), lai_to_sen, dltaisen
          endif
          if(dbg_sen) write(1111,*)'ndebsen',ndebsen,'nsencour',nsencour,'dltams(nsencour)',dltams(nsencour)

        ! ** enlever le reste du LAI si deltai(nsencour) = 0.

        else  ! jours suivants
          if(dbg_sen) write(1111,*)n,'somsenreste',somsenreste
          somsen = somsenreste
          dltamsen = 0.
! Modif Loic et Bruno fevrier 2013 : fraction remobilisable du flux de senescence (vers restemp)
          dltaremobsen = 0.

          ncompteur = 0
          if (P_codlainet == 2) dltaisen = 0.

! ** calcul de la somme de temperature depuis le dernier jour de senescence
! *- PB - 05/01/2005 - ajout d'un test pour l'enchainement des perennes. On calcul somsen
! *- a partir du nsencour du cycle precedent jusqu'e la fin du cycle precedent et
! *- on l'ajoute a la somme des temperatures depuis le debut du cycle courant jusqu'au jour n
           ! write(1111,*)'nsencour',nsencour
          if (P_codeperenne == 2 .and. P_codeinitprec == 2 .and. numcult > 1 .and. nsencour > n) then
            do i = nsencour+1, dernier_n
              somsen = somsen + tdevelop(i)
            end do
            do i = 1, n
              somsen = somsen + tdevelop(i)
            end do
          else
            do i = nsencour+1, n
              somsen = somsen + tdevelop(i)
            end do
          endif

   ! write(1111,*)'n, somsen, durvie(nsencour+1)',n,somsen,durvie(nsencour+1)
  if(dbg_sen) write(1111,'(i4,f16.8)')n,somsen

          if (somsen > durvie(nsencour+1)) then
20          nsencour = nsencour + 1
            if(dbg_sen)write(2222,'(2i4)')n,nsencour

! *- PB - 05/01/2004 - ajout d'un test lors de l'enchainement des perennes pour que nsencour
! *- ne depasse pas dernier_n. Si nsencour>dernier_n alors on a atteint la fin du cycle precedent,
! *- et donc on doit repartir du debut du cycle courant. Soit nsencour = 1
            if (P_codeperenne == 2 .and. P_codeinitprec == 2 .and. numcult > 1 .and. nsencour > dernier_n) then
              nsencour = 1
            endif

            somsen = somsen - durvie(nsencour)
            if (somsen < 0.) then
              somsen = 0.
              nsencour = nsencour - 1
               ! write(1111,*)'somsen<0',somsen,'nsencour',nsencour
              goto 40 ! TODO : a priori goto 40 pourrait etre remplace par goto 30s
            endif
            dltamsen = dltamsen + (dltams(nsencour) * P_ratiosen * pfeuilverte(nsencour))
            if(dbg_sen)write(1111,*)'dltams(nsencour)',dltams(nsencour),'P_ratiosen',P_ratiosen,'dltams(nsencour)',dltams(nsencour)
            if(dbg_sen)write(1111,*)'dltamsen',dltamsen
          ! Modif Loic et Bruno fevrier 2013 : fraction remobilisable du flux de senescence (vers restemp)
          ! Loic sept 2020 : ajout code acti reserve
            if (P_code_acti_reserve == 1) dltaremobsen = dltaremobsen + dltams(nsencour) * (1.-P_ratiosen) * pfeuilverte(nsencour)
          ! Ajout Loic Aout 2016: il y avait un bug car on arretait la mise en reserve de l'azote mais
          ! pas la mise en reserve de C...
          if (nstopres > 0.) then
             dltamsen = dltams(nsencour) * pfeuilverte(nsencour)
             dltaremobsen = 0.
          endif
          ! Loic sept 2020 : ajout code acti reserve
          if (P_code_acti_reserve == 1) cumdltaremobsen = cumdltaremobsen + dltaremobsen
          ! Fin ajout Loic
             if(dbg_sen) write(1111,*)'somsen',somsen,'nsencour',nsencour,'dltams(nsencour)',dltams(nsencour)
            ncompteur = ncompteur+1
            if (P_codlainet == 2) then
              dltaisen = dltaisen + deltai(nsencour)
              if( dltaisen.gt.4) then
                call EnvoyerMsgDebug(logger, "Warning, dltaisen greater than 4: " //  to_string(dltaisen))
              endif
                 if(dbg_sen) write(1111,*)'nsencour, deltai(nsencour),dltaisen',nsencour, deltai(nsencour),dltaisen
            endif

            ! ** si la duree de vie est raccourcie, on peut "faire mourir" plusieurs jours de production
            ! *- PB - 10/03/2005 - Petite modificatoin, on compare nsencour a dernier_n et non plus a n.
            if(dbg_sen) write(1111,*)'durvie(nsencour)',durvie(nsencour),'nsencour',nsencour,'dernier_n',dernier_n
            if (somsen > durvie(nsencour) .and. nsencour < dernier_n) goto 20

            somsenreste = somsen
            if(dbg_sen) write(1111,*)'on a finit la senecence du jour avec somsenreste',somsenreste
          else
            dltamsen = 0.
! Modif Loic et Bruno fevrier 2013 : fraction remobilisable du flux de senescence (vers restemp)
! Loic sept 2020 : ajout code acti reserve
            if (P_code_acti_reserve == 1) dltaremobsen = 0.

            if (P_codlainet == 2) dltaisen = 0.
          endif
40        continue
        endif
      else
        dltamsen = 0.
! Modif Loic et Bruno fevrier 2013 : fraction remobilisable du flux de senescence (vers restemp)
! Loic sept 2020 : ajout code acti reserve
        if (P_code_acti_reserve == 1) dltaremobsen = 0.

        if (P_codlainet == 2) dltaisen = 0.
      endif

30    continue

      ! **      msresjaune   = matiere senescente residuelle
      ! *-      msneojaune   = matiere senescente neoformee (limitee au vegetatif)
      ! *-      mafeuiljaune = matiere senescente cumulee
      ! *- Hyp: msneojaune   = mafeuiljaune
      msresjaune = msresjaune + dltamsres
     if(dbg_sen) write(1111,*)'fin mafeuiljaune,dltamsen,dltaisen',mafeuiljaune,dltamsen,dltaisen

! Modif Loic et Bruno fevrier 2013
! PB&Inaki - 08/03/2005 : On enleve les feuilles tombees de la matiere jaune
!      mafeuiljaune = mafeuiljaune + dltamsen - dltamstombe
!      msneojaune = mafeuiljaune

! Mise a jour du pool restemp !
      if (P_code_acti_reserve ==1) restemp = restemp + dltaremobsen

! Ben voila encore une nouveaute ....
! NB le 26/06/06
!      nsen = ndebsen

!dr 14/10/2011 ecriture de 2 varaibles locales pour pb inde AgMIP
!write(618,*)n,durage,senstress
!write(*,*)'fin senescen'
return
end subroutine senescen
end module senescen_m
 
