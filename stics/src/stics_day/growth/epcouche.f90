! *- sb le 26/03/07
!    cette fonction renvoie l'epaisseur de la couche iz :
!        * int(debut)-debut+1,   si iz = int(debut)
!        * fin-int(fin),         si iz = int(fin)
!        * 1 sinon
! 07/02/08
!  epcouche correspond a l'epaisseur de la mini couche colonisee par les racines
!  lorsqu'il s'agit de la premiere mini couche (apres la profondeur de semis) ou de la
! derniere minicouche (a hauteur de zrac) : epcouche peut etre < 1

module epcouche_m
use iso_fortran_env, only: real32
implicit none
private
public :: epcouche
contains
real(real32) function epcouche(iz, debut, fin)

  implicit none

!: Arguments
  integer, intent(IN) :: iz  
  real(real32),    intent(IN) :: debut  
  real(real32),    intent(IN) :: fin  
      
      if (iz == int(debut)) then
         epcouche = int(debut) - debut + 1.
      else if (iz == int(fin)) then
         epcouche = fin - int(fin)
      else
         epcouche = 1.
      endif
      
return
end function epcouche 
end module epcouche_m
