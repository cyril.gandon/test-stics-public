! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This module calculates root growth.
! - Stics book paragraphe 5, 5.1, 5.2, page 85-90
!
!! In the model, roots only act as water and mineral nitrogen absorbers, and are described by their front depth and density profile.
!! The root growth begins at germination (for sown plants) or at planting (for  transplanted crops, possibly after a latency phase), and it stops at a
!! given stage of development, depending on the species (stoprac which can be either LAX, FLO or MAT).
!    - Root front growth (subroutine 'croissanceFrontRacinaire' in this module):
!!   A first calculation gives the depth of the root front (zrac) beginning at the sowing depth (profsem) for sown crops and at an initial value for
!!   transplanted crops (profsem + zracplantule) or perennial crops (zrac0). The root front growth stops when it reaches the depth of soil or an obstacle
!!   that can be physical or chemical (the obstacle depth is defined by the parameter obstarac) or when the phenological stopping stage has been reached.
!!   For indeterminate crops, when trophic competition prevents vegetative growth, the root front growth is stopped (except before the IAMF stage, when root growth
!!   is given priority).
!!   A first calculation of the front growth rate (deltaz in cm.d-1) is proportional to temperature with a coefficient depending on the variety (croirac).
!!   This value is then multiplied by the water and bulk density stress indices. The thermal function relies on crop or soil temperature according to
!!   the root growth dependence on the collar or apex temperature. If the driving temperature is that of the crop, the cardinal temperatures (tcmin and tcmax)
!!   are the same as those used for the thermal function of the leaf growth rate. If the driving temperature is that of the soil at level zrac (1cm),
!!   the minimum temperature is the base temperature for germination (tgmin) but the maximum temperature does not change.
!!   The water and bulk density stress index is calculated as the product of 3 variables, depending on soil dryness (humirac),
!!   water logging (izrac), and bulk density (efda). The efda variable constitutes a constraint to penetration in the case of compacted soils,
!!   or more rarely a slowing of root penetration linked to a lack of soil cohesiveness. Root penetration is not constrained between the bulk density
!!   thresholds dacohes and daseuilbas. Above a bulk density threshold daseuilhaut the effect of bulk density (DA) on root penetration is constant
!!   and corresponds to the sensitivity of the plant to the penetration constraint; it is equal to contrdamax. daseuilbas and daseuilhaut values are
!!   1.4 and 2.0 respectively. The dacohes value is poorly understood and we only provide an order of magnitude. The bulk density is the effective one,
!!   taking into account fine earth and pebbles.
!    - Growth in root density:
!!   The root density profile is calculated according to two possible options. The 'standard profile' option makes it possible to calculate the root profile
!!   that is effective with respect to absorption. The 'true density' option allows the actual root density profile to be estimated, which is more relevant
!!   in order to simulate low-density crops, for which root density is never optimal, or in order to take into consideration the effects of constraints imposed
!!   by the soil on root distribution. Whatever the chosen option, roots only play a role as absorbers of water and mineral nitrogen.  It is possible to estimate
!!   the root mass with the second option and to account for a direct link between shoot and root growing rates. However an indirect link exists in all
!!   calculations through temperature, which affects both levels.
!         - Standard profile (subroutine 'profilracinaire' in this module): this option enables calculation of the root profile which is efficient in terms of absorption.
!!      It is defined by the maximum current depth, zrac, and a prescribed efficient root density profile, lracz (Z). This profile is calculated dynamically
!!      as a function of zrac and takes a sigmoidal form depending on the zlabour, zprlim and zpente parameters. These parameters define the form of the
!!      reference root profile and are of considerable importance in terms of their interrelationships, but they do not define the final shape of the root system.
!!      In this respect, it is the differences between zpente and zlabour, and particularly between zprlim and zpente which are determinant.
!!      zlabour corresponds to the depth of the tilled layer, where it is assumed that root proliferation is not limited with respect to water and mineral
!!      absorption: root density is optimum at this level (lvopt). zpente is the depth at which root uptake efficiency is reduced by half, and zprlim is the
!!      depth of the root front to which this reference profile can be attributed.
!!     The value used for the optimum root density threshold, LVOPTG, is 0.5 cm cm-3 soil (Brisson, 1998c).  In this way, it is possible to represent a
!!      root system for various species exhibiting fasciculate or pivotal type root systems.
!         - True density (see the module 'densirac.f90')
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------*
module croira_m
use stdlib_ascii, only: to_lower
use phenology_utils
use Stics
USE F_humirac_m, only: F_humirac
USE math_utils, only: from_linear
use messages
use messages_data
use densirac_m, only: densirac
use densirac2_m, only: densirac2
use plant_utils
implicit none
private
public :: densiteRacinaire, croissanceFrontRacinaire
contains
subroutine densiteRacinaire(logger,n, nbCouches, nbCoum, anox, hur, humin, dacouche, P_daseuilbas, P_daseuilhaut, P_dacohes, &
             P_lvopt, P_coderacine, P_contrdamax, P_sensanox, P_zlabour, P_zpente, P_zprlim, P_profsem, P_codazorac, P_minazorac,&
             P_maxazorac, P_minefnra, P_codtrophrac, P_coefracoupe, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens,     &
             P_lvmax,  P_draclong, P_sensrsec, P_stlevamf, P_vlaimax, longsperacf, longsperacg, debsenracf, debsenracg,     &
!             P_codedyntalle, P_propracfmax, P_zramif, P_longsperac, P_msresiduel, sioncoupe, codeinstal, nger, nlev, nrec,  &
             P_codedyntalle, P_propracfmax, P_longsperac, P_msresiduel, sioncoupe, codeinstal, nger, nlev, nrec,            &
             nit, amm,profsol, lai, tauxcouv, drlsenmortalle, densite, repracmin, repracmax, kreprac, idzrac,     &
             precrac, lai_veille, dltams, nstoprac, somtemprac, msrac_veille, zrac, znonli, deltaz, maxdayg, dtj, zracmax,  &
             racnoy, flrac, lracz, msrac, cumflrac, efdensite_rac, difrac, cumlracz, poussracmoy, cumlraczmaxi, dltamsrac,  &
             nsencourpreracf, nsencourpreracg, ndebsenracf, ndebsenracg, ndecalf, ndecalg, maxdayf, lracsentotf, lracsentotg,    &
             rltotf, rltotg, rlf_veille,rlg_veille, rlf, rlg, drlf, drlg, lracsenzf, lracsenzg, drlsenf, drlseng, somtempracvie, &
!            efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj, P_codemortalracine, dltmsrac_plante, dltaremobil,   &
             efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj, dltaremobil,   &
             P_code_diff_root, P_code_acti_reserve, inn, turfac, P_code_stress_root, P_codemonocot, P_kdisrac, P_codedisrac, &
             P_alloperirac, P_pgrainmaxi, P_humirac, hucc, masecnp, P_codemortalracine, QNrac, P_parazorac,               &
             P_code_rootdeposition, urac, msrac0, QNrac0, P_code_ISOP)

  type(logger_), intent(in) :: logger
  integer,  intent(IN)    :: n
  integer,  intent(IN)    :: nbCouches      ! number of soil layers containing roots
  integer,  intent(IN)    :: nbCoum         ! maximum number of soil layers = nbCouchesSol
  real,     intent(IN)    :: anox(nbCouches)
  real,     intent(IN)    :: hur(nbCouches)
  real,     intent(IN)    :: humin(nbCouches)
  real,     intent(IN)    :: dacouche(nbCouches)
  real,     intent(IN)    :: P_daseuilbas   ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1
  real,     intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1
  real,     intent(IN)    :: P_dacohes      ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1
  real,     intent(IN)    :: P_lvopt        ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1
  integer,  intent(IN)    :: P_coderacine   ! // PARAMETER // Choice of estimation module of growth root in volume: standard profile (1) or by actual density (2) // code 1/2 // PARPLT // 0
  real,     intent(IN)    :: P_contrdamax   ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1
  real,     intent(IN)    :: P_sensanox     ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1
  real,     intent(IN)    :: P_zlabour      ! // PARAMETER // Depth of ploughing  // cm // PARPLT // 1
  real,     intent(IN)    :: P_zpente       ! // PARAMETER // Depth where the root density is 50 percent of the surface root density for the reference profile // cm  // PARPLT // 1
  real,     intent(IN)    :: P_zprlim       ! // PARAMETER // Maximum depth of the root profile for the reference profile // cm  // PARPLT // 1
  real,     intent(IN)    :: lai            ! // INPUT // Leaf area index (table) // m2 leaf.m-2 soil
  real,     intent(IN)    :: tauxcouv       ! // INPUT // Cover rate // SD
  integer,  intent(IN)    :: nrec
  integer,  intent(IN)    :: codeinstal
  integer,  intent(IN)    :: nger
  integer,  intent(IN)    :: nlev
  real,     intent(IN)    :: P_profsem      ! // PARAMETER // Sowing depth // cm // PARTEC // 1
  integer,  intent(IN)    :: P_codazorac    ! // PARAMETER // activation of the nitrogen influence on root partitionning within the soil profile  // code 1/2 // PARPLT // 0
  real,     intent(IN)    :: P_minazorac    ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // kg N ha-1 mm-1 // PARPLT // 1
  real,     intent(IN)    :: P_maxazorac    ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning  // kg N ha-1 mm-1 // PARPLT // 1
  real,     intent(IN)    :: P_minefnra     ! // PARAMETER // parameter of the effect of soil nitrogen on root soil partitioning // SD // PARPLT // 1
  integer,  intent(IN)    :: P_codtrophrac  ! // PARAMETER // trophic effect on root partitioning within the soil // code 1/2/3 // PARPLT // 0
  real,     intent(IN)    :: P_coefracoupe  ! // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARAMV6/PLT // 1
  real,     intent(IN)    :: P_sensrsec     ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
  real,     intent(IN)    :: P_stlevamf     ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1
  real,     intent(IN)    :: P_stamflax     ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1
  real,     intent(IN)    :: P_lvfront      ! // PARAMETER // Root density at the root front // cm root.cm-3 soil // PARPLT // 1
  real,     intent(IN)    :: P_laicomp      ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1
  real,     intent(IN)    :: P_adens        ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1
  real,     intent(IN)    :: P_bdens        ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real,     intent(IN)    :: P_draclong     ! // PARAMETER // Maximum rate of root length production // cm root plant-1 degree.days-1 // PARPLT // 1
  real,     intent(IN)    :: P_vlaimax      ! // PARAMETER // ULAI  at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
  integer,  intent(IN)    :: P_codedyntalle ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  real,     intent(IN)    :: nit(nbCouches)
  real,     intent(IN)    :: amm(nbCouches)
  integer,  intent(IN)    :: profsol
  real,     intent(IN)    :: idzrac
  real,     intent(IN)    :: precrac(nbCouches)
  logical,  intent(IN)    :: sioncoupe
  real,     intent(IN)    :: drlsenmortalle ! // OUTPUT // Root biomass corresponding to dead tillers // t ha-1.j-1
  real,     intent(IN)    :: densite        ! // OUTPUT // Actual sowing density // plants.m-2
  real,     intent(IN)    :: P_msresiduel   ! // PARAMETER // Residual dry matter after a cut // t ha-1 // PARTEC // 1
  real,     intent(IN)    :: lai_veille     ! lai(n-1)
  real,     intent(IN)    :: dltams         ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,     intent(IN)    :: repracmin
  real,     intent(IN)    :: repracmax
  real,     intent(IN)    :: kreprac
  real,     intent(IN)    :: P_longsperac
 ! 11/06/2013 la variable efda devient une variable de sortie journaliere pour Simtraces
  real,     intent(INOUT)   :: efda          ! // OUTPUT // efda defines the effect of soil compaction through bulk density // 0-1
  real,     intent(INOUT)   :: efnrac_mean   ! // OUTPUT // effect of mineral nitrogen, which contributes to the root distribution in the layers with high mineral nitrogen content. // 0-1
  real,     intent(INOUT)   :: humirac_mean  ! // OUTPUT // soil dryness // 0-1
  real,     intent(INOUT)   :: humirac_z(nbcouches)
  real,     intent(INOUT)   :: efnrac_z(nbcouches)
  real,     intent(OUT)     :: rlj

  integer,  intent(INOUT) :: nstoprac
  real,     intent(INOUT) :: somtemprac
  real,     intent(INOUT) :: msrac_veille   ! msrac(n-1)
  real,     intent(INOUT) :: msrac          ! msrac(n)
  real,     intent(INOUT) :: zrac           ! // OUTPUT // Depth reached by root system // cm
  real,     intent(INOUT) :: znonli
  real,     intent(INOUT) :: deltaz         ! // OUTPUT // Deepening of the root front  // cm day-1
  real,     intent(INOUT) :: zracmax
  real,     intent(INOUT) :: racnoy
  real,     intent(INOUT) :: flrac(nbCouches)
  real,     intent(INOUT) :: lracz(nbCouches)

  real,     intent(OUT)   :: cumflrac
  real,     intent(OUT)   :: efdensite_rac
  real,     intent(OUT)   :: difrac
  real,     intent(OUT)   :: cumlracz       ! // OUTPUT // Sum of the effective root length  // cm root.cm-2 soil
  real,     intent(OUT)   :: poussracmoy    ! // OUTPUT // "average effect of soil constraints on the root length profile (option 'true density')" // 0-1
  real,     intent(OUT)   :: cumlraczmaxi
! DR 06/05/2015 je rajoute un code pour tester la mortalite des racines
!  integer,  intent(IN)    :: P_codemortalracine ! // PARAMETER // masec servant a calculer les racines mortes a la coupe : masec (1), masectot (2) // code 1/2 //PARAMv6 // 1

!   Modifs Loic et Bruno avril 2014
!   Longueur et masse des nouvelles racines emises au jour n (dltarltot et dltamsrac)
  real,    intent(INOUT) :: P_lvmax         ! // INPUT  // Maximum root length density encountered in the top soil (option profil-type) // cm.cm-3
  real,    intent(INOUT)    :: rlf_veille(nbCouches)    ! length of fine roots in layer iz on day n-1
  real,    intent(INOUT)    :: rlg_veille(nbCouches)    ! length of coarse roots in layer iz on day n-1
  real,    intent(IN)    :: debsenracf               ! Lifespan of fine roots  (degrees C.days)
  real,    intent(IN)    :: debsenracg               ! Lifespan of coarse roots  (degrees C.days)
  real,    intent(IN)    :: longsperacf              ! Specific root length of fine roots  (cm/g DM)
  real,    intent(IN)    :: longsperacg              ! Specific root length of coarse roots  (cm/g DM)
  real,    intent(IN)    :: P_propracfmax            !
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!  real,    intent(IN)    :: P_zramif
  integer, intent(IN)    :: maxdayf
  integer, intent(IN)    :: maxdayg

  real,    intent(INOUT) :: dtj(maxdayg)         ! 1 to n    // OUTPUT // Daily efficient temperature for the root growth  // degree C.d
  real,    intent(INOUT) :: dltamsrac       ! // OUTPUT // Biomass of new roots emitted on day n // t ha-1 d-1
  real,    intent(INOUT) :: rlf(nbCouches)           ! length of fine roots in layer iz on day n
  real,    intent(INOUT) :: rlg(nbCouches)           ! length of coarse roots in layer iz on day n
  real,    intent(INOUT) :: drlf(nbCouches,maxdayf)     ! length of fine roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlg(nbCouches,maxdayg)     ! length of coarse roots emitted in layer iz on day n
  real,    intent(INOUT) :: drlsenf(nbCouches)       ! length of dead fine roots in layer iz on day n
  real,    intent(INOUT) :: drlseng(nbCouches)       ! length of dead coarse roots in layer iz on day n
  integer, intent(INOUT) :: ndebsenracf              ! first day when fine roots start to die
  integer, intent(INOUT) :: ndebsenracg              ! first day when coarse roots start to die
  integer, intent(INOUT) :: nsencourpreracf          ! last day at which fine root mortality is calculated
  integer, intent(INOUT) :: nsencourpreracg          ! last day at which coarse root mortality is calculated
  integer, intent(INOUT) :: ndecalf                  ! number of days remaining in the previous simulation without mortality of fine roots
  integer, intent(INOUT) :: ndecalg                  ! number of days remaining in the previous simulation without mortality of coarse roots
  real,    intent(INOUT) :: lracsenzf(nbCouches)     ! length of fine roots which die in layer iz on day n
  real,    intent(INOUT) :: lracsenzg(nbCouches)     ! length of coarse roots which die in layer iz on day n
  real,    intent(INOUT) :: somtempracvie            ! cumulative thermal time during successive runs (used to calculate initiation of root mortality)

  real,    intent(OUT)   :: lracsentotf      !// OUTPUT // Total length of fine senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: lracsentotg      !// OUTPUT // Total length of coarse senescent roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotf           !// OUTPUT // Total length of fine roots // cm root.cm-2 soil
  real,    intent(OUT)   :: rltotg           !// OUTPUT // Total length of coarse roots // cm root.cm-2 soil
!  real,    intent(OUT)   :: dltmsrac_plante  !// OUTPUT // pour sorties ArchiSTICS: biomasse journaliere allouee aux racines en g / m2 sol / plante
  real,    intent(IN)    :: dltaremobil
  integer, intent(IN)    :: P_code_diff_root
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: inn
  real,    intent(IN)    :: turfac
  integer, intent(IN)    :: P_code_stress_root
! Florent Juin 2018
  real,    intent(IN)    :: P_kdisrac
  integer, intent(IN)    :: P_codedisrac
  real,    intent(IN)    :: P_alloperirac
  real,    intent(IN)    :: P_pgrainmaxi
  integer, intent(IN)    :: P_codemonocot
  integer, intent(IN)    :: P_humirac
  real,    intent(IN)    :: hucc(nbCouches)
  real,    intent(IN)    :: masecnp
  integer, intent(IN)    :: P_codemortalracine ! // PARAMETER // masec servant a calculer les racines mortes a la coupe : masec (1), masectot (2) // code 1/2 //PARAMv6 // 1
  real,    intent(INOUT) :: QNrac
  real,    intent(IN)    :: P_parazorac
  integer, intent(IN)    :: P_code_rootdeposition
  ! PL, 7/10/2020 
  real,    intent(OUT)   :: urac   ! // OUTPUT // daily relative development unit for root growth  // 1-3
! Ajout Loic juin 2021
  real,    intent(OUT)   :: msrac0
  real,    intent(OUT)   :: QNrac0
  integer, intent(IN)    :: P_code_ISOP

  ! variables locales
  real :: dltarltotf = 0.   ! length of fine roots emitted over the root profile on day n   cm.cm-2 sol
  real :: dltarltotg = 0.   ! length of coarse roots emitted over the root profile on day n  cm.cm-2 sol
  real :: dltasentotf = 0.  ! length of fine roots which die on day n over the root profile  cm.cm-2 sol
  real :: dltasentotg = 0.  ! length of coarse roots which die on day n over the root profile  cm.cm-2 sol
  real :: msracf = 0.       ! Biomass of living fine roots               // t ha-1
  real :: msracg = 0.       ! Biomass of living coarse roots             // t ha-1
  real :: Cracpm = 380.

! CALCUL DU PROFIL DE LONGUEUR ET DE MASSE RACINAIRE
! **************************************************
! rl(iz)        longueur de racines vivantes presentes au jour n   dans la couche iz     cm.cm-3 sol
! rl_veille(iz) longueur de racines vivantes presentes au jour n-1 dans la couche iz     cm.cm-3 sol
! drl(n,iz)     longueur de racines vivantes emises    le jour n   dans la couche iz     cm.cm-3 sol
! drliz         longueur de racines vivantes emises    le jour n   dans la couche iz     cm.cm-3 sol
! rltot         longueur de racines vivantes presentes au jour n   sur le profil de sol  cm.cm-2 sol
! sumrl         longueur de racines vivantes presentes au jour n-1 sur le profil de sol  cm.cm-2 sol
! dltasentot    longueur de racines mortes             le jour n   sur le profil de sol  cm.cm-2 sol
! drlsen        longueur de racines mortes             le jour n   dans la couche iz     cm.cm-3 sol
! dltarltot     longueur de racines vivantes emises    le jour n   sur le profil de sol  cm.cm-2 sol
! dltamsrac     biomasse de racines vivantes emises    le jour n   sur le profil de sol  t.ha-1
! lracsentot    longueurs de racines mortes            cumulees    sur le profil de sol  cm.cm-2 sol

!  bilan par couche           rl(iz) = rl_veille(iz) +   drliz   - drlsen
!  bilan sur le profil        rltot  =    sumrl      + dltarltot - dltasentot

! II. CALCUL DE LA LONGUEUR RACINAIRE A PARTIR DE LA LEVEE
! ********************************************************
      if (P_coderacine == 1) then
        call profilRacinaire(logger, nbCouches, anox, hur, humin, tauxcouv, P_lvopt, P_zlabour, P_zpente, P_zprlim, P_lvmax,&
                             codeinstal, nger, P_profsem, lai, zrac, znonli, cumlracz, difrac, cumflrac, racnoy, flrac,    &
                             lracz, cumlraczmaxi, rlg, rltotg, humirac_mean, humirac_z, P_code_diff_root, P_humirac, hucc, &
                             profsol, P_code_ISOP)
      else
        if (nger > 0 .and. P_codedisrac == 2) &
          call densirac(logger, n, nbCouches, nbCoum, dacouche, hur, humin, anox, nstoprac, P_codazorac, P_minazorac, P_maxazorac,&
                     P_minefnra, P_codtrophrac, P_coefracoupe, P_daseuilbas, P_daseuilhaut, P_dacohes, P_contrdamax, P_sensrsec,&
                     P_sensanox, P_stlevamf, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens, P_draclong, P_vlaimax,         &
                     P_longsperac, P_codedyntalle, drlsenmortalle, P_profsem, P_msresiduel, P_lvopt, densite,          &
                     lai_veille, maxdayf, maxdayg, nit, amm, nrec, nger, nlev, codeinstal, poussracmoy, zrac, somtemprac, dtj,  &
                     deltaz, precrac, idzrac, efdensite_rac, dltams, repracmin, repracmax , kreprac, sioncoupe,       &
!	  			     debsenracf, debsenracg, P_propracfmax, P_zramif, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy,   &
                     debsenracf, debsenracg, P_propracfmax, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy,             &
                     ndebsenracf, ndebsenracg, ndecalf, ndecalg, nsencourpreracf, nsencourpreracg, rlf, rlg, drlf, drlg,        &
                     lracsenzf, lracsenzg, lracsentotf, lracsentotg, rltotf, rltotg, rlf_veille, rlg_veille, dltarltotf,        &
                     dltarltotg, dltasentotf, dltasentotg, drlsenf, drlseng, somtempracvie, efda, efnrac_mean, humirac_mean,    &
                     humirac_z, efnrac_z, rlj, dltaremobil, lai, P_code_acti_reserve, inn, turfac, P_code_stress_root, P_humirac,&
                     hucc, P_codemortalracine, masecnp, urac)

         if (nger > 0 .and. P_codedisrac == 1) &
!         if (nlev > 0 .and. P_codedisrac == 1) &
         call densirac2(logger, n, nbCouches, nbCoum, dacouche, hur, humin, anox, nstoprac, P_codazorac, P_minazorac, P_maxazorac, &
                     P_minefnra, P_codtrophrac, P_coefracoupe, P_daseuilbas, P_daseuilhaut, P_dacohes, P_contrdamax, P_sensrsec,&
                     P_sensanox, P_stlevamf, P_stamflax, P_lvfront, P_laicomp, P_adens, P_bdens, P_draclong, P_vlaimax,   &
                     P_longsperac, P_codedyntalle, drlsenmortalle, P_profsem, P_msresiduel, P_lvopt, profsol, densite,    &
                     lai_veille, maxdayf, maxdayg, nit, amm, nrec, nger, nlev, codeinstal, poussracmoy, zrac, somtemprac, dtj,   &
                     idzrac, efdensite_rac, dltams, repracmin, repracmax , kreprac, sioncoupe, debsenracf, debsenracg, &
!                     P_propracfmax, P_zramif, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf, ndebsenracg,  &
                     P_propracfmax, lracz, cumlracz, cumflrac, flrac, cumlraczmaxi, racnoy, ndebsenracf, ndebsenracg,            &
                     ndecalf, ndecalg, nsencourpreracf, nsencourpreracg, rlf, rlg, drlf, drlg, lracsenzf, lracsenzg,             &
                     lracsentotf, lracsentotg, rltotf, rltotg, rlf_veille, rlg_veille, dltarltotf, dltarltotg, dltasentotf,      &
                     dltasentotg, drlsenf, drlseng, somtempracvie, efda, efnrac_mean, humirac_mean, humirac_z, efnrac_z, rlj,    &
                     dltaremobil, lai, P_code_acti_reserve, inn, turfac, P_code_stress_root, P_codemonocot, P_kdisrac,           &
                     P_alloperirac, P_pgrainmaxi, P_humirac, hucc, P_codemortalracine, masecnp, urac)
      endif
! Modifs Loic et Bruno fevrier 2014 : actualisation des racines vivantes

      msracf = rltotf / longsperacf * 100.    ! t/ha = g/dm2 = cm racine cm-2 sol / cm racine g-1 *100
      msracg = rltotg / longsperacg * 100.    ! longsperac en cm g-1, rltot en cm racine cm-2 sol
      msrac = msracf + msracg

! Loic Fevrier 2021 : initialisation de la quantit� d'azote dans les racines quand on demarre la simulation
! ensuite la quantit� d'azote est transmise d'une usm a l'autre pour l'enchainement des perennes
!      if (P_coderacine == 2 .and. codeinstal == 1 .and. n == nger .and. P_code_rootdeposition == 1) then
      if (P_coderacine == 2 .and. codeinstal == 1 .and. n == nger .and. P_code_rootdeposition == 1 .and. QNrac == 0) then
          QNrac = (Cracpm/P_parazorac) * msrac
          ! Ajout loic juin 2021 : on enregistre msrac0 et QNrac0
          msrac0 = msrac
          QNrac0 = QNrac
      endif

! Modif Loic et Bruno avril 2013 : la croissance des racines s'arrete a la recolte (lorsque lai devient nul)
!                                  les mises a jour sont faites dans densirac
      if (P_coderacine == 1) then
        ! PL: 4/10/2022, fix for not using longsperac under that condition
        ! msrac is calculated elsewhere at harvest day, for standard root profile.
         !msrac = rltotg / P_longsperac * 100.
         msrac = 0.
         dltamsrac = msrac - msrac_veille
         dltasentotf = 0.
         dltasentotg = 0.
      else
         dltamsrac = (dltarltotf / longsperacf + dltarltotg / longsperacg) * 100.
      endif

! Modif Loic fevrier 2017 : les organes morts, QNrac et QCrac sont mis a jour dans rootdeposition
      msrac_veille = msrac

! calcul de zracmax = profondeur maximale atteinte par le systeme racinaire
      if (zrac > zracmax) zracmax = zrac
      if (zrac > profsol) zrac = profsol
return

end subroutine densiteRacinaire


! ********************************************************************************************************************************

subroutine croissanceFrontRacinaire(logger, n, nh, nbCouches, hur, hucc, tcult, tsol, humin, dacouche, P_codesimul, P_epc,&
                 P_obstarac, P_daseuilbas, P_daseuilhaut, P_dacohes, P_zrac0, P_coderacine, P_codeplante, P_codeperenne,   &
                 P_codehypo, P_codegermin, P_zracplantule, P_stoprac, P_codetemprac, P_tcmin, P_tcmax, P_tgmin, P_croirac, &
                 P_contrdamax, P_codeindetermin, P_sensanox, nplt, nrec, codeinstal, nger, nsen, nlax, nflo, nmat, nlev, namf, &
                 izrac, P_profsem, P_codcueille, deltai, lai, sioncoupe, P_sensrsec, P_codedyntalle, P_tcxstop, dltams,    &
                 rlf0, rlg0, nhe, nstoprac, zrac, zracmax, znonli, rlf_veille, rlg_veille, deltaz, dtjn, cumlracz,         &
                 poussracmoy, lracsenzf, lracsenzg, tempeff, efda, humirac_mean, profsol, P_humirac, P_code_acti_reserve)
  type(logger_), intent(in) :: logger
  integer,           intent(IN)    :: n  
  integer,           intent(IN)    :: nh  
  integer,           intent(IN)    :: nbCouches  
  real,              intent(IN)    :: hur(nbCouches)  
  real,              intent(IN)    :: hucc(nbCouches)  
  real,              intent(IN)    :: tcult   ! // OUTPUT // Crop surface temperature (daily average) // degree C
  real,              intent(IN)    :: tsol(nbCouches)  
  real,              intent(IN)    :: humin(nbCouches)  
  real,              intent(IN)    :: dacouche(nbCouches)  
  character(len=*),  intent(IN)    :: P_codesimul  ! // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0 
  integer,           intent(IN)    :: P_epc(nh)  ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,              intent(IN)    :: P_obstarac  ! // PARAMETER // Soil depth which will block the root growth  // cm // PARSOL // 1 
  real,              intent(IN)    :: P_daseuilbas  ! // PARAMETER // Threshold of bulk density of soil below that the root growth is not limited // g cm-3 // PARAM // 1 
  real,              intent(IN)    :: P_daseuilhaut  ! // PARAMETER // Threshold of bulk density of soil below that the root growth  no more possible // g cm-3 // PARAM // 1 
  real,              intent(IN)    :: P_dacohes  ! // PARAMETER // bulk density under which root growth is reduced due to a lack of cohesion d // g cm-3 // PARAM // 1 
  real,              intent(IN)    :: P_zrac0  ! // PARAMETER // initial depth of root front  // cm // INIT // 1 
  integer,           intent(IN)    :: P_coderacine  ! // PARAMETER // Choice of estimation module of growth root in volume: standard profile (1) or by actual density (2) // code 1/2 // PARPLT // 0 
  character(len=3),  intent(IN)    :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0 
  integer,           intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0 
  integer,           intent(IN)    :: P_codehypo  ! // PARAMETER // option of simulation of a  phase of hypocotyl growth (1) or planting of plantlets (2) // code 1/2 // PARPLT // 0 
  integer,           intent(IN)    :: P_codegermin  ! // PARAMETER // option of simulation of a germination phase or a delay at the beginning of the crop (1) or  direct starting (2) // code 1/2 // PARPLT // 0 
  real,              intent(IN)    :: P_zracplantule  ! // PARAMETER // depth of the initial root front of the plantlet  // cm // PARPLT // 1 
  character(*),  intent(IN)    :: P_stoprac  ! // PARAMETER // stage when root growth stops (LAX or SEN) // * // PARPLT // 0 
  integer,           intent(IN)    :: P_codetemprac  ! // PARAMETER // option calculation mode of heat time for the root: with crop temperature (1)  or with soil temperature (2) // code 1/2 // PARPLT // 0 
  real,              intent(IN)    :: P_tcmin  ! // PARAMETER // Minimum temperature of growth // degree C // PARPLT // 1
  real,              intent(IN)    :: P_tcmax  ! // PARAMETER // Maximum temperature of growth // degree C // PARPLT // 1
  real,              intent(IN)    :: P_tgmin  ! // PARAMETER // Minimum threshold temperature used in emergence stage // degree C // PARPLT // 1
  real,              intent(IN)    :: P_croirac  ! // PARAMETER // Growth rate of the root front  // cm degree.days-1 // PARPLT // 1 
  real,              intent(IN)    :: P_contrdamax  ! // PARAMETER // maximal root growth reduction due to soil strenghtness (high bulk density) // SD // PARPLT // 1 
  integer,           intent(IN)    :: P_codeindetermin  ! // PARAMETER // option of  simulation of the leaf growth and fruit growth : indeterminate (2) or determinate (1) // code 1/2 // PARPLT // 0 
  real,              intent(IN)    :: P_sensanox  ! // PARAMETER // anoxia sensitivity (0=insensitive) // SD // PARPLT // 1 
  integer,           intent(IN)    :: nplt  
  integer,           intent(IN)    :: nrec  
  integer,           intent(IN)    :: codeinstal  
  integer,           intent(IN)    :: nger  
  integer,           intent(IN)    :: nsen
  integer,           intent(IN)    :: nlax  
  integer,           intent(IN)    :: nflo  
  integer,           intent(IN)    :: nmat  
  integer,           intent(IN)    :: nlev  
  integer,           intent(IN)    :: namf  
  real,              intent(IN)    :: izrac   ! // OUTPUT // Index of excess water stress on roots  // 0-1
  real,              intent(IN)    :: P_profsem  ! // PARAMETER // Sowing depth // cm // PARTEC // 1 
  integer,           intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0 
  real,              intent(IN)    :: deltai   ! // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real,              intent(IN)    :: lai   ! // OUTPUT // Leaf area index (table) // m2 leaves  m-2 soil
  logical,           intent(IN)    :: sioncoupe  
  real,              intent(IN)    :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1 
  integer,           intent(IN)    :: P_codedyntalle  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0 
  real,              intent(IN)    :: P_tcxstop  ! // PARAMETER // threshold temperature beyond which the foliar growth stops // degree C // PARPLT // 1
  real,              intent(IN)    :: dltams   ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1

  integer,           intent(INOUT) :: nhe  
  integer,           intent(INOUT) :: nstoprac  
  real,              intent(INOUT) :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,              intent(INOUT) :: znonli     ! dans le doute, INOUT  
  real,              intent(INOUT) :: rlf_veille(nbCouches) ! nger-1 ou n-1
  real,              intent(INOUT) :: rlg_veille(nbCouches) ! nger-1 ou n-1
  real,              intent(INOUT) :: deltaz   ! // OUTPUT // Deepening of the root front  // cm day-1
  real,              intent(INOUT) :: dtjn     !  // OUTPUT // Efficient temperature for root growth on day n // degree C.j-1
  real,              intent(OUT)   :: cumlracz   ! // OUTPUT // Sum of the effective root lengths  // cm root.cm -2 soil
  real,              intent(OUT)   :: poussracmoy   ! // OUTPUT // "Average index of the effect of soil constraints on the rooting profile (option  true density )" // 0-1
  real,              intent(INOUT) :: lracsenzf(nbCouches)
  real,              intent(INOUT) :: lracsenzg(nbCouches)
  real,              intent(INOUT) :: tempeff   ! // OUTPUT // Efficient temperature for growth // degree C
 ! 11/06/2013 la variable efda devient une variable de sortie journaliere pour Simtraces
  real,              intent(INOUT) :: efda      ! // OUTPUT // efda defines the effect of soil compaction through bulk density // 0-1
  real,              intent(INOUT) :: humirac_mean ! // OUTPUT // soil dryness // 0-1
  real,              intent(IN)    :: rlf0(nbCouches)
  real,              intent(IN)    :: rlg0(nbCouches)
  real,              intent(INOUT) :: zracmax          ! maximal rooting depth reached
  integer,           intent(IN)    :: profsol
  integer,           intent(IN)    :: P_humirac
  integer,           intent(IN)    :: P_code_acti_reserve

!: Variables locales
  integer :: i  
  integer :: intzracbas  
  integer :: intzrachaut  
  integer :: iz  
  integer :: izmax
  real    :: humsol  
  real    :: hn  
  real    :: hx
  real    :: daz  
  character(:), allocatable :: lc_stoprac
!: I. CALCUL DU FRONT RACINAIRE
!******************************
! en journalier, initialisation de la croissance racinaire au semis
! en decadaire,  initialisation de la croissance racinaire a la levee
!
! cas d'une prairie : si la prairie est installee codeinstal = 1
! si on est a la deuxieme coupe, on ne passe pas dans croira
! domi - 19/09/97: pour toutes les plantes "plantees" on ne passe pas dans croira

!: initialisations
      if (codeinstal == 1 .and. n == 1) then
         znonli = P_zrac0
         zrac = znonli
      endif

      if (P_coderacine == 2 .and. codeinstal == 1 .and. n == nger) then
         nhe = 0
         do i = 1, nh
            izmax = P_epc(i)
            do iz = 1, izmax
              rlf_veille(nhe+iz) = rlf0(nhe+iz)
              rlg_veille(nhe+iz) = rlg0(nhe+iz)
            ! Loic avril 2019 : je remets ces lignes de codes de la v9 mises a jour pour la vX
            ! car des utilisateurs initialisent zrac a une profondeur sans mettre de densite racinaire en face...
            ! if ((nhe+iz) < P_profsem) rl_veille(nhe+iz) = 0.
            ! if ((nhe+iz) > P_obstarac) rl_veille(nhe+iz) = 0.
            ! if (rl_veille(iz+nhe) > 0.) zrac = iz + nhe
              if ((nhe+iz) < P_profsem) then
                rlf_veille(nhe+iz) = 0.
                rlg_veille(nhe+iz) = 0.
              endif
              if ((nhe+iz) > P_obstarac) then
                rlf_veille(nhe+iz) = 0.
                rlg_veille(nhe+iz) = 0.
              endif
              ! Loic Dec 2020 : correction bug initialisation racines
              ! Loic Jan 2021 : je l'active uniquement pour les plantes de la v10 en attendant que les
              ! fichiers d'initialisation de la vigne soient refais correctement
              if (P_code_acti_reserve == 1) then
                if ((nhe+iz) > P_zrac0) then
                  rlf_veille(nhe+iz) = 0.
                  rlg_veille(nhe+iz) = 0.
                endif
              endif
!              if (rlf_veille(iz+nhe) > 0 .or. rlg_veille(iz+nhe) > 0.) then
              if (P_code_acti_reserve == 2) then
                if (rlf_veille(nhe+iz) > 0 .or. rlg_veille(nhe+iz) > 0.) then
                  zrac = iz + nhe
                  call EnvoyerMsgHistorique(logger, MESSAGE_10)
                endif
              endif
            ! Fin ajout Loic
            end do
            nhe = nhe+izmax
         end do

 !: Tests sur l'initialisation racinaire
         if (P_profsem > izmax) then
           call exit_error(logger, MESSAGE_23)
         endif
        !if (zrac /= P_zrac0) call EnvoyerMsgHistorique(logger, MESSAGE_24)
        if (abs(zrac-P_zrac0).gt.1.0E-8) call EnvoyerMsgHistorique(logger, MESSAGE_24)
         znonli = zrac
      endif

      !: Si le sol est nu
      if (n < nplt .and. P_codeplante == CODE_BARESOIL) then
        zrac = 0.
        znonli = 0.
        deltaz = 0.
        return
      endif

!: INITIALISATIONS AVANT LE STADE NLEV (LEVEE):
!-    Dans le cas des cultures annuelles, trois cas:
!- a) Pour les cultures qui se plantent et admettent un temps de latence avant le demarrage de la croissance:
!-    initialisation du zrac a P_zracplantule+P_profsem  a partir de la plantation;
!- b) Pour les cultures qui se sement:
!-    initialisation du zrac a P_profsem a partir du semis
!- c) Pour les cultures qui se plantent et demarrent directement:
!-    initialisation du zrac a P_zracplantule+P_profsem  a partir de la levee qui est aussi la plantation
!-    attention: P_zracplantule est la longueur de  la radicelle, et non la profondeur du front racinaire

      if (nger == 0 .and. codeinstal == 0) then
! Modif Simon juin 2017 pour etre plus general
!        if (P_codeperenne == 1 .and. n >= nplt) then
         if (n >= nplt) then
            if (P_codehypo == 2 .and. P_codegermin == 1) then
               znonli = P_zracplantule + P_profsem
               zrac = znonli
               return
            else
               znonli = P_profsem
               zrac = znonli
               ! Modif Loic juillet 2018 : bug sol nu
               if (P_profsem == 999) zrac = 0
               return
            endif
         else
            znonli = 0.
            zrac = znonli
            return
         endif
      endif

      if (n == nger .and. codeinstal == 0) then
        if (P_codehypo == 2) then
          znonli = P_zracplantule+P_profsem
          zrac = znonli
        else
          !: Initialisation de la croissance racinaire a la germination
          znonli = P_profsem
          zrac = znonli
        endif
      endif

      lc_stoprac = to_lower(P_stoprac)
      if (lc_stoprac == sen) nstoprac = nsen
      if (lc_stoprac == lax) nstoprac = nlax
      if (lc_stoprac == flo) nstoprac = nflo
      if (lc_stoprac == mat) nstoprac = nmat
      if (lc_stoprac == rec) nstoprac = nrec

      if (P_codedyntalle == 1 .and. sioncoupe) deltaz = 0.

      if (n >= nger .and. nger > 0 .and. zrac > 0.) then
         if (P_codetemprac == 1) then
            dtjn = max(tcult - P_tcmin, 0.)
            dtjn = min(dtjn, P_tcmax - P_tcmin)

! NB le 06/03/2008 introduction de l'effet negatif des temperatures elevees sur les fonctions racinaires qui utilisent "dtj"
            if (P_codedyntalle == 1 .and. tcult > P_tcmax .and. P_tcxstop < 100.) then
                dtjn = (P_tcmax-P_tcmin)/(-P_tcxstop+P_tcmax)*(tcult-P_tcxstop)
                dtjn = max(dtjn,0.)
            endif
         else
            dtjn = max(tsol(int(zrac)) - P_tgmin, 0.)
            dtjn = min(dtjn ,P_tcmax - P_tgmin)

! NB le 06/03/2008 introduction de l'effet negatif des temperatures elevees sur les fonctions racinaires qui utilisent "dtj"
            if(P_codedyntalle == 1 .and. tsol(int(zrac)) > P_tcmax .and. P_tcxstop < 100.) then
                dtjn = (P_tcmax-P_tcmin)/(-P_tcxstop+P_tcmax)*(tsol(int(zrac))-P_tcxstop)
                dtjn = max(dtjn,0.)
            endif
! Modif Loic = Pour les perennes quand le LAI est nul on considere que la plante est en dormance
!              il n'y a donc plus de cumul de degres jour pour les racines
            if (P_codeperenne == 2 .and. lai == 0 .and. P_code_acti_reserve == 1) dtjn = 0.
         endif
      endif

      if (P_codedyntalle == 1) then
        if (P_tcxstop >= 100.) then
          if(tcult > P_tcmax) then
            tempeff = max(0.,(P_tcmax-P_tcmin))
          else
            tempeff = max(0.,(tcult-P_tcmin))
          endif
        else
          if(tcult > P_tcmax) then
            tempeff = (P_tcmax-P_tcmin) / (-P_tcxstop+P_tcmax)*(tcult-P_tcxstop)
            tempeff = max(0.,tempeff)
          else
            tempeff = max(0.,(tcult-P_tcmin))
          endif
        endif
      endif

! ** croissance racinaire de la germination jusqu'a la floraison
      if (n >= nger .and. (nstoprac == 0.or.n == nstoprac) .and. nger > 0 .and. zrac > 0.) then
        intzracbas = int(zrac-1)
        intzrachaut = int(zrac+1)
        if (intzracbas < 1)     intzracbas = 1
        if (intzracbas > 1000)  intzracbas = 1000
        if (intzrachaut < 1)    intzrachaut = 1
        if (intzrachaut > 1000) intzrachaut = 1000
        humsol = (hur(int(zrac))+hur(intzracbas)+hur(intzrachaut))/3.
        hn = (humin(int(zrac))+humin(intzracbas) + humin(intzrachaut))/3.
        hx = (hucc(int(zrac))+hucc(intzracbas) + hucc(intzrachaut))/3.


! ** fonction humidite en tout ou rien de part et d'autre du PF 4.2 - le front racinaire est davantage ralenti au stade radicelle (avant la levee)
        if (nlev == 0) then
! DR 11/06/2013 on sort la variable de sortie de humirac apres levee
! Florent et Bruno juillet 2018 on remplace hn par hx dans la fonction F_humirac
!           deltaz = dtjn * P_croirac * F_humirac(humsol,hn,hx,P_sensrsec)
            humirac_mean = F_humirac(humsol,hn,hx,P_sensrsec,2)
        else
! Florent et Bruno juillet 2018 on remplace hn par hx dans la fonction F_humirac
!           deltaz = dtjn * P_croirac * F_humirac(humsol,hn,hn,P_sensrsec)
            humirac_mean = F_humirac(humsol,hn,hx,P_sensrsec,P_humirac)
        endif
        deltaz = dtjn * P_croirac * humirac_mean

!       humirac_mean = F_humirac(humsol,hn,hn,P_sensrsec)

! ** Ajout d'une contrainte a la penetration  racinaire: efda
! -------------------------------------------------------
! *- Fonction Jones et al. 1991  + these de B. Rebiere, adaptee avec la densite apparente
! *- P_daseuilbas et P_daseuilhaut : seuils min et max de densite apparente
! *- P_contrdamax : taux maximal de reduction de vitesse de croissance racinaire
        daz = dacouche(int(zrac))
        efda = from_linear(daz,P_daseuilbas,P_daseuilhaut,1.,P_contrdamax)
        if (daz < P_dacohes) efda = daz/P_dacohes
        deltaz = deltaz * efda

! ** ajout pour les indeterminees
        if (P_codeindetermin == 2 .and. namf > 0 .and. deltai <= 0.) deltaz = 0.

! Ajout Loic sptembre 2016 : on arrete la croissance du front racinaire si le lai est nul
        if (nlev > 0 .and. lai == 0 .and. P_code_acti_reserve == 1) deltaz = 0.
        znonli = znonli + (deltaz * (1. - (1. - izrac) * P_sensanox * 0.8))

! NB le 22/11/07 extension de l'arret de pousse des racines quand pas de croissance
        if (P_codedyntalle == 1 .and. nlev > 0 .and. dltams <= 0.) deltaz = 0.

! **NB - le 07/06 - stress joue apres calcul de znonli
        deltaz = deltaz*(1.-(1.-izrac)*P_sensanox)
        zrac = zrac+deltaz

! ** limitation de l'enracinement par la profondeur du sol
        if (zrac > P_obstarac) then
          zrac = P_obstarac
          deltaz = 0.
        endif
! calcul de zracmax = profondeur maximale atteinte par le systeme racinaire
        if (zrac > profsol) zrac = profsol
        if (zrac > zracmax) zracmax = zrac
      endif

! Modifs Bruno et Loic juin 2013 : apres la recolte, on remet zrac a 0 sauf quand cueillette ou perenne

      if (nrec > 0 .and. n /= nrec .and. P_codesimul == CODE_CULTURE .and. P_codcueille == 1) then
        znonli = 0.
        deltaz = 0.
        cumlracz = 0.
        poussracmoy = 0.
        lracsenzf(:) = 0.
        lracsenzg(:) = 0.
        dtjn = 0.
        return
      endif

      if (nrec > 0 .and. P_codesimul == CODE_FEUILLE) then
         if (lai <= 0.) then
            zrac = 0.
            znonli = 0.
            deltaz = 0.
            cumlracz = 0.
            lracsenzf(:) = 0.
            lracsenzg(:) = 0.
            return
         endif
      endif

return
end subroutine croissancefrontracinaire

! *********************************************************************************************************************

subroutine profilracinaire(logger,nbCouches, anox, hur, humin, tauxcouv, P_lvopt, P_zlabour, P_zpente, P_zprlim, P_lvmax,&
                           codeinstal, nger, P_profsem, lai, zrac, znonli, cumlracz, difrac, cumflrac, racnoy, flrac,   &
                           lracz, cumlraczmaxi, rlg, rltotg, humirac_mean, humirac_z, P_code_diff_root, P_humirac, hucc,&
                           profsol,P_code_ISOP)
  type(logger_), intent(in) :: logger
  integer,           intent(IN)    :: nbCouches  
  real,              intent(IN)    :: anox(nbCouches)  
  real,              intent(IN)    :: hur(nbCouches)  
  real,              intent(IN)    :: humin(nbCouches)  
  real,              intent(IN)    :: tauxcouv   ! // OUTPUT // Cover rate // SD
  real,              intent(IN)    :: P_lvopt  ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1 
  real,              intent(IN)    :: P_zlabour  ! // PARAMETER // Depth of ploughing  // cm // PARPLT // 1 
  real,              intent(IN)    :: P_zpente  ! // PARAMETER // Depth where the root density is 50 percent of the surface root density for the reference profile // cm  // PARPLT // 1 
  real,              intent(IN)    :: P_zprlim  ! // PARAMETER // Maximum depth of the root profile for the reference profile // cm  // PARPLT // 1 
  integer,           intent(IN)    :: codeinstal  
  integer,           intent(IN)    :: nger  
  real,              intent(IN)    :: P_profsem ! // PARAMETER // Sowing depth // cm // PARTEC // 1
  real,              intent(IN)    :: lai   ! // OUTPUT // Leaf area index (table) // m2 leaf m-2 soil
  real,              intent(INOUT) :: P_lvmax   ! Maximum root length density
  real,              intent(INOUT) :: zrac      ! // OUTPUT // Depth reached by root system // cm
  real,              intent(INOUT) :: znonli    ! dans le doute, INOUT
  real,              intent(OUT)   :: cumlracz  ! // OUTPUT // Daily growth of the effective root length // cm root.cm-2 soil.d-1
  real,              intent(OUT)   :: difrac  
  real,              intent(OUT)   :: cumflrac  
  real,              intent(INOUT) :: racnoy  
  real,              intent(INOUT) :: flrac(nbCouches)  
  real,              intent(INOUT) :: lracz(nbCouches)  
  real,              intent(OUT)   :: cumlraczmaxi  
  real,              intent(INOUT) :: humirac_mean ! // OUTPUT // soil dryness // 0-1
  real,              intent(INOUT) :: humirac_z(nbCouches)

  real,              intent(INOUT) :: rlg(nbCouches)   ! densite racinaire globale pour option profil-type
  real,              intent(INOUT) :: rltotg           ! longueur racinaire totale pour option profil-type
  integer,           intent(IN)    :: P_code_diff_root
  integer,           intent(IN)    :: P_humirac
  real,              intent(IN)    :: hucc(nbCouches)
  integer,           intent(IN)    :: profsol
  integer,           intent(IN)    :: P_code_ISOP

!: Variables locales
  integer :: iz  
  real    :: zdemi  
  real    :: S
  real    :: coef

  ! on ne considere que des grosses racines
      if (nger > 0 .or. codeinstal == 1) then
       if (P_code_diff_root == 2) P_lvmax = P_lvopt
        ! P_forme du profil racinaire
          S = -log(1e-2)/(P_zlabour-P_zpente)           ! Forme du profil racinaire
          difrac = P_zprlim - P_zpente
          cumlracz = 0.                                 ! cumlracz =  cumul de racines fonctionnelles
          cumflrac = 0.                                 ! cumflrac =  cumul de racines (utilise dans routine excesdeau)
          zdemi = max((znonli - difrac),(log(4.)/S))
          racnoy = 0.

          humirac_mean = 0.
          do iz = 1, nbCouches
            flrac(iz) = 1./(1.+exp(-S*(float(iz)-zdemi)))
            ! Ajout Loic Juin 2021 : correction bug
            ! Quand P_zlabour est plus grand que profsol on force la valeur de flrac(iz) a 1
            ! car le calcul du profil racinaire type n'est pas prevu pour ce genre de cas...
            ! Je mets un code_ISOP en attendant de statuer sur la  genericite de cette modification
            if (P_code_ISOP == 1 .and. P_zlabour >= profsol) flrac(iz) = 1
            if (float(iz) > zrac) then
              lracz(iz) = 0.
              EXIT
            endif
     !DR 23/07/2013 j'ajoute le humirac par couche pour Simtraces
     ! Florent et Bruno juillet 2018 on remplace humin par hucc dans la fonction F_humirac
    !       humirac_z(iz) = F_humirac(hur(iz),humin(iz),humin(iz),0.)
            humirac_z(iz) = F_humirac(hur(iz),humin(iz),hucc(iz),0.,P_humirac)

          ! NB - le 08/05/02 - pas de racine au dessus de la profondeur de semis
            if (float(iz) < P_profsem) flrac(iz) = 0.
     !      coef = flrac(iz) * F_humirac(hur(iz),humin(iz),humin(iz),0.)
            coef = flrac(iz) * humirac_z(iz)
            lracz(iz) = coef * P_lvopt
            rlg(iz) =   coef * P_lvmax
            rltotg = rltotg + rlg(iz)
            ! dr 13/06/2013 j'ajoute humirac_mean en sortie
     !      humirac_mean = humirac_mean + F_humirac(hur(iz),humin(iz),humin(iz),0.)
            humirac_mean = humirac_mean + humirac_z(iz)
            !DR 23/07/2013 j'ajoute le humirac par couche pour Simtraces
     !      humirac_z(iz) = F_humirac(hur(iz),humin(iz),humin(iz),0.)
            cumlracz = cumlracz + lracz(iz)
            cumflrac = cumflrac + flrac(iz)
            cumlraczmaxi = cumlracz
            racnoy = racnoy + flrac(iz) * anox(iz)
          end do
          humirac_mean = humirac_mean/nbCouches

        ! test sur cumlracz
          if ((lai > 0. .or. tauxcouv > 0.) .and. cumlracz <= 0.) call EnvoyerMsgHistorique(logger, MESSAGE_20)
        endif
  return
  end subroutine profilracinaire
end module croira_m
