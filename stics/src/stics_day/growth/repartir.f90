! *----------------------------------------------* c
! * repartition de la biomasse entre les organes * c
! * programmation N. Brisson le 14/11/2000       * c
! * modif le 18/04 NB                            * c
! *----------------------------------------------* c
! There are models for which allocation of assimilates is critical to the operation of the model (e.g. SUCROS described by Van Ittersum et al., 2003).
!- Stics book paragraphe 3,5, page 68-71
!
! In STICS this module was added at a late stage, mainly to help dimensioning the reserve pool. For annual plants with determinate growth, the partitioning
!! calculations simply allow the dimensioning of envelopes of harvested organs which may play a trophic role and ensure an input of information for the
!! senescence module. For perennial plants or those with indeterminate growth, those calculations enable the dimensioning of a compartment for reserves which
!! are recycled in the carbon balance. The calculation of root biomass is not directly connected to that of the above-ground biomass.
! - Organs and compartments identified: the reasons for identifying an organ or a compartment are either its internal trophic role within the plant or an
!!   external role by participation in the nitrogen balance of the system (such as falling leaves and the recycling of roots). The reserve compartment is not
!!   located in a specific organ: it is just a certain quantity of carbon available for the plant growth.
! - Dimensioning of organs:
!!   Green leaves: The biomass of green leaves is calculated without accounting for potential reserves that may be stored in the leaves and remobilized later on,
!!   which are accounted for in the restemp non-located reserve pool. The mafeuilverte variable is deducted from the LAI, based on the
!!   maximum specific leaf area variable (slamax). We assume that the difference between the actual sla and slamax corresponds to remobilized leaf carbon
!!   Yellow leaves: The biomass of yellow leaves (mafeuiljaune) is calculated in the senescence module.  The proportion of leaves in the senescent biomass
!!   on a given day (dltamsen) is determined using the pfeuilverte ratio (proportion of green leaves in the non-senescent biomass) on the day of production
!!   of this senescent biomass. Some of these yellow leaves may fall to the ground depending on the abscission  parameter (between 0 and 1). The daily falling
!!   quantity (dltamstombe) is recycled in the nitrogen balance; its cumulative value is mafeuiltombe.
!!   Stems: this concerns only the structural component of stems (matigestruc). The non-structural component, if significant, can be included in the reserve
!!   compartment (e.g. for cereals) or in the harvested part (sugar cane).  The matigestruc variable is calculated as a constant proportion (tigefeuille of the
!!   total mass of foliage. For monocotyledonous plants, the stem is secondary and the matigestruc variable is only incremented from the time when accumulated
!!   biomass so allows.  It is thus assumed that the first organs to emerge are the leaves. For dicotyledonous plants, it is assumed that the tigefeuille
!!   proportionality is always respected.  Consequently, if the accumulated biomass and the foliage biomass (calculated from the LAI and SLA) are incompatible
!!   with this proportionality, then the SLA (or LAI if the SLA arises from fixed limits) is recalculated. The matigestruc variable cannot diminish,
!!   except in the case of cutting fodder crops.
!!   Harvested organs:
!   - Fruits and grains: the calculation of the number and mass of fruits (indeterminate plants) or seeds (determinate plants) is achieved in modules
!!       fruit.f90 and grain.f90.
!   - Envelops of harvested organs (pods, raches, etc.): the mass corresponding to the envelope is assumed to depend solely upon the number of organs.
!!       In any case, it cannot exceed the residual biomass (masecveg - mafeuil - matigestruc). The envfruit parameter corresponds to the proportion of
!!       membrane related to the maximum weight of the fruit. If the sea parameter is not zero, then this biomass is transformed into an equivalent
!!       leaf surface area, photosynthetically active from the IDRP stage to the IDEBDES stage.
!
!!   Reserves (restemp) are calculated as the difference between the total biomass and the accumulated biomass of leaves, stems and harvested organs.
!!   For perennial plants, at the beginning of the cropping season, the reserves (carbon) can be initialised at a non-zero value (restemp0), so as to
!!   represent the role played by root reserves at the resumption of growth. Yet it is assumed that a limit exists to the size of the reserve compartment,
!!   parametrized at the plant level by resplmax. If this limlit is reached a 'sink on source' effect is simulated. The use of reserves concerns perennial plants
!!   or indeterminate plants.  As for determinate annuals, the use of reserves for grain filling is not simulated as such, but taken globally into account
!!   when calculating the ercarb variable (index of progressive harvest).
!-----------------------------------------------------------------------------
module repartir_m
use messages
use messages_data
use Stics
use plant_utils
implicit none
private
public :: repartir
contains
subroutine repartir(logger,n, nrec, P_codcueille, P_codeperenne, nlev, nlax, P_nbcueille, tustress, P_slamin, P_slamax,&
                    P_codlainet, P_codemonocot, P_codesimul, dltaisen, P_envfruit, chargefruit, ndrp, &
                    ndebdes, P_sea, ntaille, P_codetaille, dltams, lai_veille, restemp, masecveg,  &
                    pdsfruittot, tursla, sla, mafeuilverte, mafeuil, mafeuilp, lai, deltai, maenfruit, eai,        &
                    mareserve, deltares, mabois, masec, msresjaune, mafeuiljaune, msneojaune,          &
                    matigestruc, pfeuil, pfeuilverte, pfeuiljaune, ptigestruc, penfruit, preserve, masecnp,        &
                    maperenne, QNveg, QNvegstruc, QNrestemp, matigestrucp, P_codelaitr, P_hautmax, P_hautbase,     &
                    P_khaut, hauteur, laisen, nstopres, dltamsen, dltaremobsen, remobilj, P_inilai ,P_adilmax,     &
                    QNplantenp, QNfeuille, QNperenne, P_code_acti_reserve, codeinstal, dltags, msresgel,           &
                    P_codefauche, ratioTF,fstressgel,P_restemp0,P_codeplante,numcult,P_codeinitprec, densite)
  type(logger_), intent(in) :: logger
!! merge trunk 23/11/2020
! DR 06/05/2020 j' ajoute la densit� car si on a pas leve ou tout gele on a le message de 'high senescence tous les jours !
  integer, intent(IN)    :: n  
  integer, intent(IN)    :: nrec  
  integer, intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0 
  integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0 
  integer, intent(IN)    :: P_codefauche  ! // PARAMETER // option of cut modes for forage crops: yes (1), no (2) // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: nlev  
  integer, intent(IN)    :: nlax  
  integer, intent(IN)    :: P_nbcueille  ! // PARAMETER // number of fruit harvestings // code 1/2 // PARTEC // 0 
  real,    intent(IN)    :: tustress   ! // OUTPUT // Stress index active on leaf growth (= minimum(turfac,innlai))  // 0-1
  real,    intent(IN)    :: P_slamin  ! // PARAMETER // minimal SLA of green leaves // cm2 g-1 // PARPLT // 1 
  real,    intent(IN)    :: P_slamax  ! // PARAMETER // maximal SLA of green leaves // cm2 g-1 // PARPLT // 1 
  integer, intent(IN)    :: P_codlainet  ! // PARAMETER //option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codemonocot  ! // PARAMETER // option plant monocot(1) or dicot(2) // code 1/2 // PARPLT // 0 
  character(len=*), intent(IN) :: P_codesimul  ! // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0 
  real,    intent(INOUT) :: dltaisen   ! // OUTPUT // Daily increase of the senescent leaf index // m2.m-2 sol.j-1
  !real,    intent(IN)    :: P_tigefeuil  ! // PARAMETER // stem (structural part)/leaf proportion // SD // PARPLT // 1 
  real,    intent(IN)    :: P_envfruit  ! // PARAMETER // proportion envelop/P_pgrainmaxi in weight  // SD // PARPLT // 1 
  real,    intent(IN)    :: chargefruit   ! // OUTPUT // Amount of filling fruits per m-2 // nb fruits.m-2
  integer, intent(IN)    :: ndrp  
  integer, intent(IN)    :: ndebdes  
  real,    intent(IN)    :: P_sea  ! // PARAMETER // specifique surface of fruit envelops // cm2 g-1 // PARPLT // 1 
  integer, intent(IN)    :: ntaille  
  integer, intent(IN)    :: P_codetaille  ! // PARAMETER // option of pruning // code 1/2 // PARTEC // 0 
  real,    intent(INOUT) :: dltams   ! // OUTPUT // Growth rate of the plant  // t ha-1.j-1
  real,    intent(IN)    :: lai_veille              ! n-1

  real,    intent(INOUT) :: restemp   ! // OUTPUT // C crop reserve, during the cropping season, or during the intercrop period (for perenial crops) // t ha-1
  real,    intent(INOUT) :: masecveg   ! // OUTPUT // Vegetative dry matter // t.ha-1
  real,    intent(INOUT) :: pdsfruittot  
  real,    intent(INOUT) :: tursla  
  real,    intent(INOUT) :: sla   ! // OUTPUT // Specific surface area // cm2 g-1
  real,    intent(INOUT) :: mafeuilverte   ! // OUTPUT // Dry matter of green leaves // t.ha-1
  real,    intent(INOUT) :: mafeuil   ! // OUTPUT // Dry matter of leaves // t.ha-1
  real,    intent(INOUT) :: mafeuilp  
  real,    intent(INOUT) :: matigestruc   ! // OUTPUT // Dry matter of stems (only structural parts) // t.ha-1
  real,    intent(INOUT) :: lai             ! n    // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  real,    intent(INOUT) :: deltai   ! // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
  real,    intent(INOUT) :: maenfruit   ! // OUTPUT // Dry matter of harvested organ envelopes // t.ha-1
  real,    intent(INOUT) :: eai  
  real,    intent(INOUT) :: mareserve  
  real,    intent(INOUT) :: deltares  
  real,    intent(INOUT) :: mabois   ! // OUTPUT // Prunning dry weight // t.ha-1
  real,    intent(INOUT) :: masec   ! // OUTPUT // Aboveground dry matter  // t.ha-1
  real,    intent(INOUT) :: msresjaune   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
  real,    intent(INOUT) :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
  real,    intent(INOUT) :: msneojaune   ! // OUTPUT // Newly-formed senescent dry matter  // t.ha-1
  real,    intent(INOUT) :: pfeuil   ! // OUTPUT // Proportion of leaves in total biomass // 0-1
  real,    intent(INOUT) :: pfeuilverte   ! // OUTPUT // Proportion of green leaves in total non-senescent biomass // 0-1
  real,    intent(INOUT) :: pfeuiljaune   ! // OUTPUT // Proportion of yellow leaves in total biomass // 0-1
  real,    intent(INOUT) :: ptigestruc   ! // OUTPUT // Proportion of structural stems in total biomass // 0-1
  real,    intent(INOUT) :: penfruit   ! // OUTPUT // Proportion of fruit envelopes in total biomass // 0-1
  real,    intent(INOUT) :: preserve   ! // OUTPUT // Proportion of reserve in the total biomass // 0-1
  real,    intent(INOUT) :: masecnp    ! // OUTPUT // Biomass of non perennial organs // t ha-1
! Modifs Loic juin 2013
  real,    intent(INOUT) :: maperenne
  real,    intent(INOUT) :: QNveg
  real,    intent(INOUT) :: QNvegstruc
  real,    intent(INOUT) :: QNrestemp
! Ajout Loic Mai 2016
  real,    intent(INOUT) :: matigestrucp
  integer, intent(IN)    :: P_codelaitr
  real,    intent(IN)    :: P_hautmax
  real,    intent(IN)    :: P_hautbase
  real,    intent(IN)    :: P_Khaut
  real,    intent(INOUT) :: hauteur
  real,    intent(IN)    :: laisen
  integer, intent(IN)    :: nstopres
  real,    intent(IN)    :: dltamsen
  real,    intent(IN)    :: dltaremobsen
  real,    intent(IN)    :: remobilj
  real,    intent(IN)    :: P_inilai
  real,    intent(IN)    :: P_adilmax
  real,    intent(INOUT) :: QNplantenp
  real,    intent(INOUT) :: QNfeuille
  real,    intent(IN)    :: QNperenne
  integer, intent(IN)    :: P_code_acti_reserve
  integer, intent(IN)    :: codeinstal
  real,    intent(IN)    :: dltags
  real,    intent(IN)    :: msresgel
  real,    intent(IN)    :: ratioTF
  real,    intent(IN)    :: fstressgel
! DR 05/07/2019 je remets en coherence avce la vigne
  real,    intent(INOUT) :: P_restemp0  ! // PARAMETER // initial reserve biomass // t ha-1 // INIT // 1
  character(len=3),    intent(IN) :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0
  integer, intent(IN)    :: numcult
  integer, intent(IN)    :: P_codeinitprec
!! merge trunk 23/11/2020
  real,    intent(IN)    :: densite



!: Variables locales
  real :: mareservep  !  
  real :: ptigestrucveg  !
  real :: laiajour  
  real :: dltafv
  real :: dltatige
  real :: chargefruitp
  real :: dltamaenfruit
  real :: matigestruc1
  real :: Cfeupc


  !  write(*,*)'repartir deb',n,masecnp
 ! write(2222,*)*)'repartir deb : n,deltai, lai,masecveg,masecnp', n,deltai, lai, masecveg,masecnp
  Cfeupc = 42.
! ** parametres
! *-    slavert est la surface specifique du feuillage "sans stress"
! *-    slavertmin est le minimum de slavert apres application des stress
! *-    tigefeuille est le rapport tigestruc/feuilles totales
! *-    P_envfruit correspond a la proportion enveloppe/P_pgrainmaxi
! *-    photores est la photoperiode seuil en jour decroissant
! *-      qui declenche la mise en reserve

      ! ** apres la recolte (nrec+1)
      ! ** en cas de moisson,on shunte ce module apres recolte
      ! if( dltaisen.gt.4)then
      !    write(*,*)'dltaisen',dltaisen
      ! endif

     if (n >= nrec .and. nrec > 0 .and. P_codcueille == 1 .and. P_codeperenne == 1) then
          restemp = 0.
          return
     else
         if (P_nbcueille == 1 .and. n >= nrec .and. nrec > 0) pdsfruittot = 0.
         masecveg = masecnp - pdsfruittot / 100.
         ! write(2222,*)*)'masecveg,masec,pdsfruittot',masecveg,masecnp,pdsfruittot
! Modif Loic Mai 2016: Conservation de la biomasse de feuilles et de tiges pour le calcul de l'azote structural.
! DR 23/06/2020 a voir avec Loic demain .
         mafeuilp = mafeuil
         matigestrucp = matigestruc
         chargefruitp = chargefruit

       ! restemp est initialise au depart quelle que soit la culture
       ! 05/07/2019 pb d'initialisation de masecveg et reperenne / v9.0
          if (P_code_acti_reserve == 2) then
            ! ** conservation de la reserve du jour precedent
            mareservep = mareserve
          ! ** initialisation de la reserve perenne
            if (n == nlev .and. numcult == 1) then
              if (P_codeperenne == 2) then
                restemp = P_restemp0
              else
                restemp = 0.
              endif
            endif
          endif

         !  les feuilles vertes : calcul d'un stress moyen pour module slavert
         tursla = (tursla+tustress)/2.
         sla = max(tursla*P_slamax,P_slamin)

!          Modif loic Aout 2016: je propose de travailler sur le deltai et le slamax
!          Le calcul pour les dicotyledones posait de nombreux problemes et je ne comprends pas pourquoi on donne la
!          priorite aux tiges par rapport aux feuilles. Je propose de le supprimer.
!          Les calculs se font a partir de dltams et dltai pour la biomasse structurale feuilles vertes.
!          On met ensuite a jour la biomasse structurale des feuilles, des tiges puis de l'enveloppe des fruits. Le complement correspond
!          aux reserves temporaires.
         if (P_code_acti_reserve == 1) then
            dltams = dltams + remobilj - dltags
            lai =  lai - deltai
            deltares = 0.
            dltafv = 0.
            dltatige = 0.
            dltafv = deltai / P_slamax * 100.
            if (dltafv > dltams) dltafv = dltams
            if (dltafv > 0.) then
               if ((deltai / dltafv * 100.) > P_slamax) then
                  deltai = P_slamax * dltafv / 100.
               else
                  deltai = deltai
               endif
            endif
            mafeuilverte = mafeuilverte + dltafv - dltamsen - dltaremobsen
            mafeuil = mafeuilverte + mafeuiljaune
            if (dltafv < dltams) then
              dltatige = max(ratioTF * (mafeuil - mafeuilp),0.)
              if (dltatige > (dltams - dltafv)) dltatige = dltams - dltafv
              matigestruc = matigestruc + dltatige
              deltares = dltams - dltafv - dltatige
              if (deltares < 0.) deltares = 0.
            endif

            if (chargefruit == chargefruitp) then
               dltamaenfruit = P_envfruit * chargefruit
            else
               dltamaenfruit = P_envfruit * (chargefruit - chargefruitp)
            endif
            if (dltamaenfruit > deltares) dltamaenfruit = deltares
            maenfruit = maenfruit + dltamaenfruit
            deltares = deltares - dltamaenfruit

            if (ndrp > 0 .and. ndebdes == 0) then
               eai = P_sea * maenfruit/100.
            else
               eai = 0.
            endif
            if (maenfruit <= 0.) eai = 0.
            if (nstopres > 0.) deltares = 0.
            restemp  = restemp + deltares
            lai =  lai + deltai
            if (n == nlev.and.codeinstal == 0.and.P_inilai > 0.and.msresgel == 0.and.fstressgel > 0.) then
               lai = P_inilai
               deltai = P_inilai
               mafeuilverte = lai / P_slamax * 100.
               mafeuil = mafeuilverte
               dltafv = mafeuilverte
               matigestruc = ratioTF * mafeuilverte
               masecnp = mafeuilverte + matigestruc
               dltams = masecnp
               if (QNperenne == 0.) then
                 QNplantenp = masecnp * P_adilmax * 10.
                 QNvegstruc = QNplantenp
                 QNfeuille = QNplantenp
               endif
            endif
          ! write(4444,*)'repartir hauteur a',hauteur
          ! DR 23/01/2019 c'est pas recalcule dans trunk
            if (P_codelaitr == 1) hauteur = (P_hautmax - P_hautbase) * (1. - exp(-P_khaut * (lai+laisen))) + P_hautbase
          ! write(4444,*)'repartir hauteur b',hauteur
         endif
          ! write(2222,*)*)'avant laiajour',laiajour,dltaisen,lai
         if (P_code_acti_reserve == 2) then
            if (P_codlainet == 2) then
               laiajour = lai-dltaisen
            else
               laiajour = lai
            endif
! DR commentaire de Inaki tres en colere ! je mets un test :-)
! 05/07/2019 Vous allez voir ...
! Loic sept 2020 mareservep est calcul� plus haut j'enl�ve ce calcul pour revenir � la v9
!            mareservep = restemp
!            if(P_codeplante.ne.CODE_VINE) mareservep = restemp

   ! write(2222,*)*)'laiajour',laiajour,lai
            mafeuilverte = min(laiajour/P_slamax * 100., masecveg)

            ! if(mafeuilverte.lt.0)then
            !   write(*,*)'coucou 290'
            ! endif
            mafeuil = mafeuilverte + mafeuiljaune

          ! ** test sur mafeuil >masecveg NB le 22/04
            if (masecveg < mafeuil) then
               mafeuilverte = masecveg - mafeuiljaune
               ! if(mafeuilverte.lt.0)then
               !      write(*,*)'coucou 297'
               ! endif
               mafeuil = masecveg
            endif
!! merge trunk 23/11/2020
! DR 06/05/2020 j' ajoute la densit� dans le test car si on a pas leve ou tout gele on a le message de 'high senescence tous les jours !
          !if (mafeuilverte <= 0 .and. nlax == 0) call EnvoyerMsgHistorique(logger, MESSAGE_403)
          if (mafeuilverte <= 0 .and. nlax == 0 .and. densite .gt. 0) call EnvoyerMsgHistorique(logger, MESSAGE_403)

            if (P_codemonocot == 1) then       ! cas des monocotyledones : priorite aux feuilles
            ! dr 22/06/2020 ca se passe la le cgt de matigestruc a la coupe
            !   matigestruc = matigestruc + max(P_tigefeuil * (mafeuil - mafeuilp),0.)
              matigestruc = matigestruc + max(ratioTF * (mafeuil - mafeuilp),0.)
              matigestruc = min(matigestruc, masecveg-mafeuil)
! Modif Loic avril 2019 : je desactive ce calcul qui n'est pas dans la v9
!               if (matigestruc < matigestrucp) matigestruc = matigestrucp
            else                               ! cas des dicotyledones : priorite aux tiges
             !  matigestruc1 = P_tigefeuil * mafeuil
              matigestruc1 = ratioTF * mafeuil
             ! write(2222,*)*)'avant test : matigestruc1 masecveg,mafeuil',matigestruc1,masecveg,mafeuil
             ! 22/01/2019 test sla dan sle trunk on avait ca
              if (matigestruc1 > (masecveg - mafeuil)) then
                  ! mafeuil = masecveg /(P_tigefeuil+1.)
                  mafeuil = masecveg /(ratioTF+1.)
                  mafeuilverte = mafeuil - mafeuiljaune
                  if (mafeuilverte > 0.) sla = laiajour/mafeuilverte*100.
                  if (sla < P_slamin) then
                     mafeuilverte = laiajour/P_slamin*100.
                     mafeuil = mafeuilverte + mafeuiljaune
                     sla = P_slamin
                  endif
                 ! write(2222,*)*)'avant test sla',sla,P_slamax
                  if (P_codesimul == CODE_CULTURE .and. sla > P_slamax) then
                     sla = P_slamax
                     lai = sla * (mafeuil - mafeuiljaune)/100.
!                deltai = lai - lai_veille
                ! DR et SB 27/02/2017 il y a un pb en cas de phase de decroissance du lai , le deltai calcule ici devient negatif
                ! et du coup dans le calcul du lai brut on fait lai(n)=lai(n-1) + deltai - deltaisen, on ajoute la scenesence !! , on ajoute les 4 lignes qui suivent
                     deltai = lai - lai_veille + dltaisen
                     if(deltai.lt.0.) then
                        deltai = 0.
                        dltaisen = lai_veille - lai
!                        write(2222,*)n,'repartir si deltai<0,laiveille,lai,dltaisen',lai_veille,lai,dltaisen
                     endif
!                 write(2222,*)n,'repartir sla lai laiveille',sla,lai,lai_veille,mafeuil,mafeuiljaune,'deltai',deltai,dltaisen
                ! DR 23/01/2019 n'est pas present dans trunk
                     if (P_codelaitr == 1) hauteur = (P_hautmax - P_hautbase) * (1. - exp(-P_khaut * (lai+laisen))) + P_hautbase
                  endif
               endif
               ! matigestruc = matigestruc + max(P_tigefeuil * (mafeuil-mafeuilp),0.)
               matigestruc = matigestruc + max(ratioTF * (mafeuil-mafeuilp),0.)
! Modif Loic Mai 2016: pour eviter que la somme des composants de la biomasse aerienne (matigestruc + mafeuil + restemp) soit plus grande que masecveg
!           matigestruc = min(matigestruc, masecveg-mafeuil-restemp)
               matigestruc = min(matigestruc, masecveg-mafeuil)
! Modif Loic avril 2019 : je desactive ce calcul qui n'est pas dans la v9
!               if (matigestruc < matigestrucp) matigestruc = matigestrucp
            endif
          ! ** NB - le 12/04 - pour eviter plantade liee au GEL
            if (masecveg > 0.) then
               ptigestrucveg = matigestruc / masecveg
            else
               ptigestrucveg = 0.
            endif

          ! les enveloppes des organes recoltes sont un % du nombre de fruits a condition qu'il y ait assez de biomasse!
            maenfruit = P_envfruit * chargefruit
            maenfruit = min(maenfruit, 0.99*(masecveg - mafeuil - matigestruc))
            maenfruit = max(maenfruit,0.)
            if ((masecveg-mafeuil-matigestruc-maenfruit) < 0.) call EnvoyerMsgHistorique(logger, MESSAGE_404)

          ! estimation d'une surface photosynthetique pour le fruit entre les stades DRP et DEBDES
            if (ndrp > 0 .and. ndebdes == 0) then
               eai = P_sea * maenfruit/100.
            else
               eai = 0.
            endif
            if (maenfruit <= 0.) eai = 0.

          ! ** les reserves correspondent au complement
            mareserve = masecveg - mafeuil - matigestruc - maenfruit
            if (mareserve < 0.) mareserve = 0.
          ! ** pour les perennes les reserves migrent vers des organes de stockage (racines ou bois)
            deltares = mareserve - mareservep
            ! Loic oct 2020 : ajout code_acti_reserve
            if (P_code_acti_reserve == 1 .and. nstopres > 0.) deltares = 0.
            restemp  = max(restemp + deltares,0.)
         endif

         if (n == ntaille .and. P_codetaille == 2 .and. P_codeplante/=CODE_VINE) &
            call repartir_taille(mafeuil, mabois, lai, masec, masecnp, maperenne, restemp, QNveg, QNvegstruc, &
                                 QNrestemp, msresjaune, mafeuiljaune, msneojaune, matigestruc)
! DR 05/07/
         if (n == ntaille .and. P_codetaille == 2 .and. P_codeplante==CODE_VINE) &
            call repartir_taille_vigne(mafeuil,P_codeperenne,P_codeinitprec,        & ! IN
                                 mabois,lai,P_restemp0,masec,msresjaune, & ! INOUT
                                 mafeuiljaune,msneojaune,matigestruc)

      endif

      if (masecnp <= 0.) then
        mafeuil = 0.
        mafeuilverte = 0.
        mafeuiljaune = 0.
        matigestruc = 0.
        masecveg = 0.
        maenfruit = 0.
        mareserve = 0.
        pfeuil = 0.
        pfeuilverte = 0.
        pfeuiljaune = 0.
        ptigestruc = 0.
        penfruit = 0.
        preserve = 0.
! 31/08/2017 TODO Loic le deplace dans plantdeath
! DR 28/02/2017 quand on a recolte (sauf les cultures fauche) on a plus de fruit et plus de eai
        if (nrec > 0 .and. n > nrec) then
            if (P_codcueille == 1 .and. P_codefauche /= 1) eai = 0.
        endif
! fin DR 28/02/2017
    else
        pfeuil = mafeuil / masecnp
        if (dltams > 0.) then
          if (P_code_acti_reserve == 2) then
             pfeuilverte = (deltai/P_slamax*1e2) / dltams
             pfeuilverte = min(1.0,pfeuilverte)
          else
             pfeuilverte = dltafv / dltams
             pfeuilverte = min(1.,pfeuilverte)
          endif
        else
          pfeuilverte = 0.
        endif

        pfeuiljaune = mafeuiljaune / masecnp
        ptigestruc = matigestruc / masecnp
        penfruit = maenfruit / masecnp
        preserve = restemp / masecnp
      endif
 ! write(2222,*)*)'repartir fin lai n-1 et n', n, deltai, lai
 !   write(*,*)'repartir fin',n,masecnp

return
end subroutine repartir

! -----------------------------------------------------------------------------------------

subroutine repartir_taille(mafeuil, mabois, lai, masec, masecnp, maperenne, restemp, QNveg, QNvegstruc,  &
                           QNrestemp, msresjaune ,mafeuiljaune, msneojaune, matigestruc)


    real,    intent(IN)    :: mafeuil   ! // OUTPUT // Dry matter of leaves // t.ha-1
!    integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
!    integer, intent(IN)    :: P_codeinitprec  ! // PARAMETER // reinitializing initial status in case of chaining simulations : yes (1), no (2) // code 1/2 // PARAM // 0

    real,    intent(INOUT) :: mabois   ! // OUTPUT // Prunning dry weight // t.ha-1
    real,    intent(INOUT) :: lai   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
!    real,    intent(INOUT) :: P_restemp0  ! // PARAMETER // initial reserve biomass // t ha-1 // INIT // 1
    real,    intent(INOUT) :: masec   ! // OUTPUT // Total plant dry matter  // t.ha-1
    real,    intent(INOUT) :: masecnp
    real,    intent(INOUT) :: maperenne
    real,    intent(INOUT) :: restemp
    real,    intent(INOUT) :: QNveg
    real,    intent(INOUT) :: QNvegstruc
    real,    intent(INOUT) :: QNrestemp
    real,    intent(OUT)   :: msresjaune   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
    real,    intent(OUT)   :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
    real,    intent(OUT)   :: msneojaune   ! // OUTPUT // Newly-formed senescent dry matter  // t.ha-1
    real,    intent(OUT)   :: matigestruc   ! // OUTPUT // Dry matter of stems (only structural parts) // t.ha-1

    mabois = matigestruc + mafeuil

    lai = 0.
    masecnp = 0.
    msresjaune = 0.
    mafeuiljaune = 0.
    msneojaune = 0.
    matigestruc = 0.
    masec = maperenne
    restemp = 0.
    QNveg = 0.
    QNvegstruc = 0.
    QNrestemp = 0.

end subroutine

! DR 05/07/2019 je fais un repartir_taille especial vigne car nos amis laonnois nous ont ben modifi� celui qui avait initialement
! ete implement� pour la vigne ...
subroutine repartir_taille_vigne(mafeuil,P_codeperenne,P_codeinitprec,        & ! IN
                           mabois,lai,P_restemp0,masec,msresjaune, & ! INOUT
                           mafeuiljaune,msneojaune,matigestruc)


    real,    intent(IN)    :: mafeuil   ! // OUTPUT // Dry matter of leaves // t.ha-1
    integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0
    integer, intent(IN)    :: P_codeinitprec  ! // PARAMETER // reinitializing initial status in case of chaining simulations : yes (1), no (2) // code 1/2 // PARAM // 0

    real,    intent(INOUT) :: mabois   ! // OUTPUT // Prunning dry weight // t.ha-1
    real,    intent(INOUT) :: lai   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
    real,    intent(INOUT) :: P_restemp0  ! // PARAMETER // initial reserve biomass // t ha-1 // INIT // 1
    real,    intent(INOUT) :: masec   ! // OUTPUT // Aboveground dry matter  // t.ha-1
    real,    intent(OUT)   :: msresjaune   ! // OUTPUT // Senescent residual dry matter  // t.ha-1
    real,    intent(OUT)   :: mafeuiljaune   ! // OUTPUT // Dry matter of yellow leaves // t.ha-1
    real,    intent(OUT)   :: msneojaune   ! // OUTPUT // Newly-formed senescent dry matter  // t.ha-1
    real,    intent(OUT)   :: matigestruc   ! // OUTPUT // Dry matter of stems (only structural parts) // t.ha-1

    mabois = matigestruc + mafeuil

    ! *- PB&Inaki - 08/03/2005 - on enleve plus les feuilles tombees de mabois... absurde !!
    !- mabois = matigestruc + mafeuil - mafeuiltombe

    ! *- PB & inaki - 08/12/2004
    ! *- a la taille, il faut reinitialiser les variables plantes
    lai = 0.0

    if (P_codeperenne == 2 .and. P_codeinitprec == 2) then
      ! 07/09/06 DR et IGC on rajoute un then et on garde reserveN
      P_restemp0 = masec - mabois
      !-- P_QNplante0 = QNplante(1,n)+QNplante(2,n)
    endif

    ! *- INAKI - 08/03/2005 - on change le mode de calcul du masec a la taille.
    !-- masec = masec - mabois
    masec = 0.
    msresjaune = 0.
    mafeuiljaune = 0.
    msneojaune = 0.
    matigestruc = 0.
    !-- sometemp = 0.

end subroutine
end module repartir_m
