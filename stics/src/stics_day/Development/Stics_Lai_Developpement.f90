!*-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 3.1.1, page 40-44
! - In STICS, leaf area growth is driven by phasic development, temperature and stresses. An empirical plant density-dependent function represents inter-plant
!! competition. For indeterminate plants, trophic competition is taken into account through a trophic stress index, while for determinate plants a maximal
!! expansion rate threshold is calculated to avoid unrealistic leaf expansion.
!! The calculation of leaf growth rate (deltai in m2m-2 d-1) is broken down: a first calculation of the LAI growth rate (in m2 plant-1 degree-day-1) describes
!! a logistic curve, related to the ILEV, IAMF and ILAX phenological stages. This value is then multiplied by the effective crop temperature, the plant density
!! combined with a density factor, supposed to stand for inter-plant competition, that is characteristic for the variety, and the water and nitrogen stress indices.
!!
!! The phasic development function is comparable to that of the model PUTU (Singels and Jagger, 1991), i.e. a logistic function with dlaimaxbrut as asymptote
!! and pentlaimax as the slope at the inflection point. It is driven by a normalized leaf development unit (ULAI) equal to 1 at ILEV and 3 at ILAX.
!! At the end of the juvenile stage (IAMF), it is equal to vlaimax when the inflection of the dynamics (point of maximal rate) also occurs.
!! Between the stages ILEV, IAMF and ILAX, the model performs linear interpolation based on development units (upvt) which include all the environmental effects
!! on phasic development. As the ILAX stage approaches, it is possible to introduce a gradual decline in growth rate using the udlaimax parameter
!! - the ULAI value beyond which there is a decline in the leaf growth rate. If udlaimax=3 it has no effect and the leaf stops growing at once at ILAX.
!!
!! The thermal function relies on crop temperature and cardinal temperatures (tcmin and tcmax) which differ from the ones used for the phasic development.
!! The extreme threshold tcxstop is the same as for development.
!!
!! The density function is active solely after a given LAI threshold occurs (laicomp parameter) if the plant density (densite in plant m-2 possibly decreased
!! by early frost) is greater than the bdens threshold, below which plant leaf area is assumed independent of density.  Beyond this density value, leaf area
!! per plant decreases exponentially.  The adens parameter represents the ability of a plant to withstand increasing densities.  It depends on the species
!! and may depend on the variety.  For branching or tillering plants, adens represents the plants branching or tillering ability (e. g. wheat or pea).
!! For single-stem plants, adens represents competition between plant leaves within a given stand (e.g. maize or sunflower).
!!
!! Water and nitrogen affect leaf growth as limiting factors, i.e. stress indices whose values vary between 0 and 1. Water (turfac) and nitrogen deficits (innlai)
!! are assumed to interact, justifying the use of the more severe of the two stresses. Meanwhile at the whole plant level the water-logging stress index is assumed
!! to act independently
!
! -	Features of determinate crops
!!   Failure to account for trophic aspects in the calculation of leaf growth may cause problems when the radiation intercepted by the crop is insufficient to
!!   ensure leaf expansion (e.g. for crops under a tree canopy or crops growing in winter).  Consequently, from the IAMF stage, we have introduced a trophic effect
!!  to calculate the definitive LAI growth rate in the form of a maximum threshold for leaf expansion (deltaimaxi in m2m-2d-1) using the notion of the maximum
!!   leaf expansion allowed per unit of biomass accumulated in the plant (sbvmax in cm2 g-1) and the daily biomass accumulation (dltams in t.ha-1day-1 possibly
!!   complemented by remobilized reserve remobilj). sbvmax is calculated using the slamax and tigefeuil parameters.
!!
! -	Features of indeterminate crops
!!   It has been possible to test the robustness of the above formalisation on a variety of crops, including crops where there is an overlap between the
!!   vegetative phase and the reproductive phase (soybean and flax for example).  However, when trophic competition between leaves and fruits is a driving force
!!   for the production and management of the crop (for example tomato, sugarbeet), this formalisation is unsuitable.  We therefore calculate the deltai variable
!!   so as to take trophic monitoring into consideration in the case of crops described as 'indeterminate', by introducing a trophic stress index (splai).
!!   As a consequence, the LAI can decrease markedly during the theoretical growth phase if the crop is experiencing severe stresses during the harvested
!!   organs filling phase.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!TODO: renommer cette routine plus explicitement
module laidev_m
use messages
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
use ResidusApportSurfaceSol_m, only: ResidusApportSurfaceSol
use ResiduParamDec_m, only: ResiduParamDec
use recalcullevee_m, only: recalcullevee
use Developpement, only: develop
use f_densite_equiv_m, only: f_densite_equiv
use calai_m, only: calai
use rognage_m, only: rognage
use effeuill_m, only: effeuill
use TauxRecouvrement_m, only: TauxRecouvrement
use plant_utils
implicit none
private
public :: laidev
contains
subroutine laidev(logger,sc,p,pg,itk,t,soil,c)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),             intent(INOUT) :: sc  
  type(Plante_),                    intent(INOUT) :: p(sc%P_nbplantes)  
  type(ITK_),                       intent(INOUT) :: itk(sc%P_nbplantes)  
  type(Parametres_Generaux_),       intent(INOUT) :: pg  
  type(Stics_Transit_),             intent(INOUT) :: t  
  type(Sol_),                       intent(INOUT) :: soil  
  type(Climat_),                    intent(INOUT) :: c  


  ! variables locales
  integer :: i, ens, ires
  real :: Cfeupc  
  real :: CNrogne  
  real :: Crogne ! Bruno-ajout variables locales C et N rogne
  real :: Nrogne
  real :: Wr

!DR 03122020 merge trunk
    ! pour alleger l'ecriture
    !integer ::  AS, AO, AOAS
    !AS = sc%AS
    !AO = sc%AO
    !AOAS = sc%AOAS

    do i = 1, sc%P_nbplantes
      if (sc%P_codesimul == CODE_FEUILLE) then
          call recalcullevee(sc%n,p(i)%nlev,p(i)%nlevobs,p(i)%nlaxobs,p(i)%lai(1:2,1:sc%n+1), &
                             sc%tauxcouv(1:sc%n+1),p(i)%P_codelaitr,p(i)%is_dominant)
      endif

      if (p(i)%P_codeplante /= CODE_BARESOIL) then
        sc%ens = AS

! ::: Developpement de la plante
        call develop(logger,sc,p(i),itk(i),soil,c,t,pg)

        ! si les 2 plantes sont arrivees a levee on recalcule la densite equivalente
        if(sc%P_nbplantes.gt.1 .and. i==2)then
           ! DR et ML 21/04/2016 le calcul de la densite equivalente est refait tous les jours a partir de le levee des 2 plantes
           ! de facon a prendre en compte les inversions de dominances et les modifications de densite ( gel )

          if((p(1)%nlev>0 .and. p(2)%nlev>0) )then
              if (p(1)%nplt > p(2)%nplt) call EnvoyerMsgHistorique(logger, MESSAGE_25)
              call F_densite_equiv(i,p(1)%densite,p(2)%densite,p(i)%is_dominant,p(1)%P_bdens(itk(1)%P_variete),&
              p(2)%P_bdens(itk(2)%P_variete),p(1)%densiteequiv,p(2)%densiteequiv)

! DR et ML 02/03/2017 garde pour memoire
            !  call F_densite_equiv(i,p(1)%densite,p(2)%densite,p(i)%is_dominant,p(1)%P_bdens ,p(2)%P_bdens,&
            !  p(1)%densiteequiv,p(2)%densiteequiv,     &
            !  p(1)%lai(AOAS,sc%n-1), p(2)%lai(AOAS,sc%n-1), p(1)%deltai(AOAS,sc%n-1), p(2)%deltai(AOAS,sc%n-1))
              ! le jour de la levee la desnite de la plante 2 est celle qu'on vient de recalculer (densite equivalente)
              p(2)%densitelev = p(2)%densite
          endif
        endif
        ! si on a une seule plante on reaffecte la densite equivalente au cas ou il y aurait eu une modif sur la densite courante (gel ou autre )
        if(sc%P_nbplantes.eq.1)p(1)%densiteequiv=p(1)%densite

      ! on decompose les valeurs AOAS calculees dans develop en AO et AS
        p(i)%rdtint(AO,p(i)%nbrecolte-1) = p(i)%rdtint(AOAS,p(i)%nbrecolte-1) * p(i)%surf(AO)
        p(i)%rdtint(AS,p(i)%nbrecolte-1) = p(i)%rdtint(AOAS,p(i)%nbrecolte-1) * p(i)%surf(AS)

        p(i)%nfruit(AO,p(i)%P_nboite) = p(i)%nfruit(AOAS,p(i)%P_nboite) * p(i)%surf(AO)
        p(i)%nfruit(AS,p(i)%P_nboite) = p(i)%nfruit(AOAS,p(i)%P_nboite) * p(i)%surf(AS)

        p(i)%pdsfruit(AO,p(i)%P_nboite) = p(i)%pdsfruit(AOAS,p(i)%P_nboite) * p(i)%surf(AO)
        p(i)%pdsfruit(AS,p(i)%P_nboite) = p(i)%pdsfruit(AOAS,p(i)%P_nboite) * p(i)%surf(AS)

        if (itk(i)%P_coderecolteassoc == 1) then
          if (p(i)%nrec /= 0 .and. ALL(p(:)%P_codeperenne == 1)) p(:)%nrec = p(i)%nrec
        endif


        if (sc%P_codesimul == CODE_CULTURE) then
          if (p(i)%P_codelaitr == 1) then
            do ens = AS,AO
              if (p(i)%surf(ens) > 0.) then

                sc%ens = ens

              ! Calcul du LAI

                call calai(sc,pg,p(i),itk(i),c)  !DR 19/07/2012 t n'est pas utile car codedlaimin a ete supprime

              ! Rognage & Effeuillage, seulement si lai > 0
              ! !ATTENTION! on travaille sur masec(n-1) car a ce moment de la boucle journaliere masec(n) n'a pas encore ete calcule...
                if (p(i)%lai(ens,sc%n) > 0.) then
                  if (itk(i)%P_codrognage == 2) then
                    call rognage(logger,itk(i)%P_codcalrogne, p(i)%hauteur(ens), p(i)%largeur, itk(i)%P_hautrogne, &
                                 itk(i)%P_margerogne, itk(i)%P_largrogne, p(i)%dfol, p(i)%P_hautbase(itk(i)%P_variete), &
                                 itk(i)%P_interrang, p(i)%sla(ens), p(i)%P_tigefeuil(itk(i)%P_variete), itk(i)%P_biorognem, &
                                 sc%n, p(i)%nrogne, p(i)%lairogne(ens), p(i)%biorogne(ens), p(i)%lai(ens,sc%n),   &
                                 p(i)%masecnp(ens,sc%n-1), p(i)%varrapforme, p(i)%P_forme, p(i)%biorognecum(ens), &
                                 p(i)%lairognecum(ens))

                    p(i)%lai(AOAS,sc%n) = p(i)%lai(AS,sc%n) * p(i)%surf(AS) + p(i)%lai(AO,sc%n) * p(i)%surf(AO)
                    p(i)%masecnp(AOAS,sc%n-1) = p(i)%masecnp(AS,sc%n-1) * p(i)%surf(AS)      &
                                                 + p(i)%masecnp(AO,sc%n-1) * p(i)%surf(AO)

                    if (sc%n == p(i)%nrogne .and. p(i)%biorogne(ens) > 0.) then
                      soil%itrav1 = 1
                      soil%itrav2 = 1
                      ires = 2
                      Cfeupc  =  42.
                      CNrogne = Cfeupc / p(i)%CNplante(ens)
		      
! Bruno 06/2012 - ajout quantites de C et N tombees au sol apres rognage
                      Crogne=p(i)%biorogne(ens)*Cfeupc*10.
                      Nrogne=Crogne/CNrogne
                      p(i)%QCrogne = p(i)%QCrogne + Crogne
                      p(i)%QNrogne = p(i)%QNrogne + Nrogne

                      ires = 2
                      call ResidusApportSurfaceSol(p(i)%biorogne(ens), Cfeupc, -1.e-10, pg%P_CNresmin(ires),     &
                                        pg%P_CNresmax(ires), pg%P_Qmulchdec(ires), CNrogne, sc%Cnondec(ires), sc%Nnondec(ires), &
                                        sc%Cres(1,ires), sc%Nres(1,ires), sc%QCapp, sc%QNapp, sc%QCresorg, sc%QNresorg, &
! DR 30/04/2019 je rajoute les 2 varaibles manquantes dans l'appel � la fonction
!                                        pg%P_CroCo(ires), sc%Chum(1), sc%Nhum(1), sc%Chumi, sc%Nhumi, soil%CsurNsol)
                                        pg%P_CroCo(ires), sc%Chum(1), sc%Nhum(1), sc%Chumi, sc%Nhumi, soil%CsurNsol,  &
                                        sc%Chuma, sc%Nhuma)
!subroutine ResidusApportSurfaceSol(qresidu, Crespc, eauresidu, CNresmin, CNresmax, Qmulchdec, CNresid, &
!                                    Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo, &
!                                    Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)

                      Wr = 0.
                      if (CNrogne > 0.) Wr = 1./CNrogne
                      call ResiduParamDec(&
                        logger, pg%P_awb(ires), pg%P_bwb(ires), pg%P_cwb(ires), pg%P_akres(ires), pg%P_bkres(ires),&
                                         pg%P_ahres(ires), pg%P_bhres(ires), Wr, pg%P_CNresmin(ires),  &
                                         pg%P_CNresmax(ires), sc%Wb(ires), sc%kres(ires), sc%hres(ires))
                    endif
                  endif

                  if (itk(i)%P_codeffeuil /= 1) then
                    call effeuill(logger, itk(i)%P_codcaleffeuil,itk(i)%P_laidebeff,p(i)%deltai(ens,sc%n),    & ! IN
                                  itk(i)%P_effeuil,p(i)%sla(ens),sc%n,p(i)%neffeuil,itk(i)%P_laieffeuil,     &
                                  p(i)%P_codetransrad,p(i)%P_hautmax(itk(i)%P_variete),p(i)%P_khaut(itk(i)%P_variete), &
                                  p(i)%dfol,itk(i)%P_largrogne,itk(i)%P_codhauteff,p(i)%largeur,  &
                                  p(i)%lai(ens,sc%n),p(i)%masecnp(ens,sc%n-1),p(i)%P_hautbase(itk(i)%P_variete), & ! INOUT
                                  p(i)%varrapforme,p(i)%bioeffcum(ens),p(i)%laieffcum(ens))

                  ! Cumuls AO/AS
                    p(i)%lai(AOAS,sc%n) = p(i)%lai(AS,sc%n) * p(i)%surf(AS) + p(i)%lai(AO,sc%n) * p(i)%surf(AO)
                    p(i)%masecnp(AOAS,sc%n-1) = p(i)%masecnp(AS,sc%n-1) * p(i)%surf(AS)      &
                                                 + p(i)%masecnp(AO,sc%n-1) * p(i)%surf(AO)

                  endif
                endif
              endif
            end do
          else
            call TauxRecouvrement(sc,pg,p(i),itk(i))
          endif
        endif
      endif     ! fin test P_codeplante /= CODE_BARESOIL

      if (sc%P_codesimul == CODE_FEUILLE) then
!: Recalcul de la levee en fonction de la courbe de LAI observee
        if (p(i)%nlev > 0) then
          if (sc%P_nbplantes > 1 .and. p(i)%lai(AO,sc%n) <= 0.) then ! est-ce que ca ne devrait pas etre if (i>1...) a la place de if (P_usm%P_nbplantes>1...) ?
            p(i)%lai(AO,sc%n) = 0.001                                ! La plante principale n'a pas de partie a l'ombre, donc pas de lai.
          endif
          if (p(i)%lai(AS,sc%n) <= 0.) then
            p(i)%lai(AS,sc%n) = 0.001
          endif
        else
          if (p(i)%lai(AS,sc%n) > 0. .or. p(i)%lai(AO,sc%n) > 0.) then
            p(i)%nlevobs = sc%n
          endif
        endif
      endif
    end do

return
end
end module laidev_m
