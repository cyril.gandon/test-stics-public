! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 10.3.4, page 194
!
!! This module calculates an equivalent plant density for the understorey crop which accounts for the presence of the dominant crop.
!! This equivalent plant density is calculated from the densities at emergence of the two associated crops (and not anymore from the density at sowing).
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! DR le 28/08/2017 je le commente pour memoire dans le trunk , actif dans la branche microclimat
!subroutine F_densite_equiv(ipl,densitep1,densitep2,is_dominant,P_bdensp1 ,P_bdensp2,densiteeqvp1,densiteeqvp2,  &
!                      &     lai_p1,lai_p2,deltai_p1,deltai_p2)
module F_densite_equiv_m
implicit none
private
public :: F_densite_equiv
contains
subroutine F_densite_equiv(ipl,densitep1,densitep2,is_dominant,P_bdensp1 ,P_bdensp2,densiteeqvp1,densiteeqvp2)
    implicit none

  real      :: P_bdensp1  ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real      :: P_bdensp2  ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1
  real      :: densiteeqvp1  ! calcul de la densite equivalente de la plante 1 (dominante)
  real      :: densiteeqvp2  ! calcul de la densite equivalente de la plante 1 (dominee, celle qui est affectee  )
!DR 12/09/2012 devenu inutiles
!  real      :: dassoinit
  logical   :: is_dominant
  integer   :: ipl
  real      :: densitep1
  real      :: densitep2
! DR le 28/08/2017 je le commente pour memoire dans le trunk , actif dans la branche microclimat
!  real      :: lai_p1,lai_p2
!  real      :: deltai_p1,deltai_p2

    ! DR 07/09/2012 je recualcule les densites en cas de cAS et quand les plantes ont leve
        if (ipl > 1 .and. .not. is_dominant) then
        !  dassoinit = densitep2 ! est inutile avce les structures
        ! densites pour le calcul de la competition
        !  densitep2 = densitep2 + densitep1 / P_bdensp1 * P_bdensp2
        ! DR 12/09/2012 on conserve la desnite equivalente de la plante 2 dans dassoiniteqv
        ! la densite de la plante 2 reste elle la densite au semis reduite par les manque a la levee ou le gel
          densiteeqvp1 = densitep1
          densiteeqvp2 = densitep2 + densitep1 / P_bdensp1 * P_bdensp2


! DR le 28/08/2017 je le commente pour memoire dans le trunk , actif dans la branche microclimat
!! DR et ML 02/03/2017 on applique une densite equivalenet aux 2 plantes si elles ont quasiment la meme hauteur
!         if(abs(lai_p1-lai_p2) .lt. max(deltai_p1, deltai_p2))then
!! DR 09/02/2017 on fait un test pour les CAS de recalcul de densite equivalente pour ne pas trop contraindre la plante dessous
!                densiteeqvp1 = densitep1 + densitep2 / P_bdensp1 * P_bdensp2
!                 write(2222,*)'recalcul de densiteeqvp1'
!         endif

        endif
return
end subroutine F_densite_equiv
end module F_densite_equiv_m