! Module of development
!! Description :
!! calculation of Phenological stages and leaf development
!
module Developpement
use messages
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE Besoins_en_froid
use develop2_m, only: develop2
implicit none
private
public :: develop
contains

! Module of plant development
!
!! Description : (routine qui prend des structures en arguments)
! DR 23/07/2012 sta est inutilise
subroutine develop(logger,sc,p,itk,soil,c,t,g)

!-----------------------------------------------------------------------
!    group = Numero du groupe (de precocite)
!    si =-1 on n'a pas assez de sommes de temperature pour
!             aller au bout du cycle
!------------------
! DR et ML 18/03/08 :
! on avait supprime debour et introduit les calculs de debourrement dans
! develop apres reflexion ca pose des pbs car le basculement du calcul horaire au calcul
! journalier dependait de la levee qui etait calcule apres le test de basculement
! d'ou des somtemps horaires le jour de la levee qui entrainait une scenescence
! immediate : INAKI on t'aime
!-----------------------------------------------------------------------

type(logger_), intent(in) :: logger
type(Stics_Communs_) :: sc  
type(Parametres_Generaux_) :: g  
type(Plante_) :: p  
type(ITK_) :: itk  
type(Sol_) :: soil  
type(Climat_) :: c  
type(Stics_Transit_) :: t  
integer :: ens  

ens = sc%ens

call develop2(&
  logger, c%phoi, c%phoi_veille, c%tmax(sc%n), c%tmin(sc%n), c%tmin(sc%n+1), c%trr(sc%n), g%P_codeh2oact, g%P_codeinitprec,  &
              g%P_codeinnact, g%codeulaivernal, g%P_psihucc, g%P_psihumin, itk%P_codcueille, itk%P_codefauche, itk%P_densitesem, &
              itk%P_profsem, itk%P_variete, p%P_ampfroid, p%P_belong, p%P_celong, p%P_codebfroid, p%P_codedormance ,p%P_codegdh, &
              p%P_codegdhdeb, p%P_codegermin, p%P_codehypo, p%P_codeperenne, p%P_codephot, p%P_codeplante, p%P_coderetflo,       &
              p%P_codetemp, p%coeflev, minval(p%cu), p%cu(sc%n-1), p%densite, p%densiteger, p%densitelev, p%P_elmax, p%innlai(0),&
              p%P_julvernal, p%P_jvc(itk%P_variete), p%P_jvcmini, p%P_nbjgerlim, p%ndrpobs, p%P_nlevlim1, p%P_nlevlim2,          &
              p%nlevobs, p%nplt, p%onarretesomcourdrp, p%P_phobase(itk%P_variete), p%P_phosat(itk%P_variete), p%P_potgermi,      &
              p%P_propjgermin, p%P_q10, p%P_sensiphot(itk%P_variete), p%P_sensrsec, p%P_codestrphot, p%P_phobasesen,             &
              p%somelong, p%somger, p%P_stdordebour(itk%P_variete), p%P_stpltger, p%P_stressdev, p%P_tcxstop, p%P_tdmax,         &
              p%P_tdmaxdeb, p%P_tdmin, p%P_tdmindeb, p%P_tfroid, p%P_tgmin, p%turfac(0), p%P_vigueurbat, soil%P_codefente,       &
              soil%P_mulchbat, soil%P_pluiebat, sc%P_culturean, nbCouchesSol, sc%dacouche, sc%hucc, sc%humin, sc%hur, sc%jjul,&
              sc%n, sc%nbjanrec, sc%nbjsemis, sc%numcult, sc%tairveille, sc%tcult, sc%tsol, sc%xmlch1, t%P_codepluiepoquet,      &
              itk%P_codetempfauche, t%humectation, t%nbjhumec, t%pluiesemis, p%somtemphumec, p%P_codeindetermin,          &
              p%P_codelaitr, p%P_codlainet, p%P_dureefruit(itk%P_variete), p%namfobs, itk%P_nbcueille, p%nfloobs, p%nlanobs,     &
              p%nlaxobs, p%nmatobs, p%nrecobs, p%nsenobs, p%P_stdrpnou(itk%P_variete), p%upobs(sc%n), p%P_codemontaison,         &
              p%sioncoupe, p%caljvc, p%cu(sc%n), p%etatvernal, p%namf, p%ndebdorm, p%ndrp, p%nfindorm,                           &
              p%nflo, p%nger, p%nlan, p%nlev, p%nrec, p%nrecbutoir, p%pdsfruitfrais, p%rfpi, p%rfvi, p%somcour, p%somcourdrp,    &
              p%somcourfauche, p%somcourutp, p%somtemp, p%stpltlev, p%tdevelop(sc%n), p%udevair, p%udevcult, p%upvt(sc%n),       &
              p%utp(sc%n), p%zrac, sc%maxwth, p%group, p%ndebdes, p%nfruit(0,p%P_nboite), p%nlax, p%nmat, p%nnou, p%nsen,        &
              p%P_stamflax(itk%P_variete), p%P_stdrpdes(itk%P_variete), p%P_stdrpmat(itk%P_variete), p%stdrpsen,                 &
              p%P_stflodrp(itk%P_variete), p%P_stlaxsen(itk%P_variete), p%P_stlevamf(itk%P_variete), p%P_stlevdrp(itk%P_variete),&
              p%stlevflo, p%stmatrec, p%P_stsenlan(itk%P_variete), p%upvtutil, itk%P_codrecolte, p%h2orec(0), itk%P_sucrerec,    &
              itk%P_CNgrainrec, itk%P_huilerec, p%sucre(0), p%huile(0), p%teaugrain(0), p%P_h2ofrvert, itk%P_codeaumin,          &
              itk%P_h2ograinmin, itk%P_h2ograinmax, p%P_deshydbase(itk%P_variete), p%CNgrain(0), itk%P_cadencerec, p%jdepuisrec, &
              p%pdsfruit(0,p%P_nboite), p%nbrecolte, p%nrecint(p%nbrecolte), p%rdtint(0,p%nbrecolte), p%teauint(0,p%nbrecolte),  &
              p%nbfrint(0,p%nbrecolte), t%onestan2, p%somcourmont, p%nmontaison, p%stpltger, p%P_idebdorm, sc%P_iwater,   &
              p%P_tdoptdeb, p%P_code_WangEngel, p%P_ifindorm, p%P_code_acti_reserve, p%codeinstal, p%ndes, p%P_codephot_part)

end subroutine develop

end module Developpement
