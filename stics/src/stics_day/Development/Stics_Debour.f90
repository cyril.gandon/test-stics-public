

!! ****************************************************************
! programme specifique vigne Inaki pour le calcul de la date
!! de debourrement avec des sommes de temperatures horaires
!! 20/09/2004
! ****************************************************************
module debour_m
USE climate_utils, only: get_hourly_values
use messages
use messages_data
use Divers_develop, only: calcul_GDH, calcul_GDH2

implicit none
private
public :: debour
contains
subroutine debour(logger, P_codegdhdeb,P_codetemp,tmax,tmin,tmin_demain,P_tdmindeb,P_tdmaxdeb,rfvi,rfpi, &
                  upvt,udevair,udevcult,P_tdoptdeb,P_code_WangEngel)
  type(logger_), intent(in) :: logger
  integer, intent(IN)               :: P_codegdhdeb     ! 	  // PARAMETER // option of calculation of the bud break date in hourly or daily growing degrees  // code 1/2 // PARPLT // 0
  integer, intent(IN)               :: P_codetemp       ! 	  // PARAMETER // option calculation mode of heat time for the plant : with air temperature (1)  or crop temperature (2) // code 1/2 // PARPLT // 0
  real,    intent(IN)               :: tmax             ! T Max  	  // OUTPUT // Maximum active temperature of air // degree C
  real,    intent(IN)               :: tmin             ! T Min du jour  	  // OUTPUT // Minimum active temperature of air // degree C
  real,    intent(IN)               :: tmin_demain      ! T Min du lendemain  
  real,    intent(IN)               :: P_tdmindeb       !  // PARAMETER // minimal thermal threshold for hourly calculation of phasic duration between dormancy and bud breaks // degree C // PARPLT // 1
  real,    intent(IN)               :: P_tdmaxdeb       !  // PARAMETER // maximal thermal threshold for hourly calculation of phasic duration between dormancy and bud breaks // degree C // PARPLT // 1
  real,    intent(IN)               :: rfvi             !  // OUTPUT // Slowing effect of the vernalization on plant development // 0-1
  real,    intent(IN)               :: rfpi             !  // OUTPUT // Slowing effect of the photoperiod on plant development  // 0-1
! OUT
  real,    intent(OUT)              :: upvt             !  // OUTPUT // Daily development unit  // degree.days
  real,    intent(OUT)              :: udevair          !  // OUTPUT // Effective temperature for the development, computed with TAIR // degree.days
  real,    intent(OUT)              :: udevcult         !  // OUTPUT // Effective temperature for the development, computed with TCULT // degree.days
  real,    intent(IN)               :: P_tdoptdeb       !  // PARAMETER // optimal temperature for calculation of phasic duration between dormancy and bud breaks // degree C // PARPLT // 1
  integer, intent(IN)               :: P_code_WangEngel ! // PARAMETER // option to activate Wang et Engel (1998) effect of temperature on development units for emergence :yes (1), no(2) // code 1/2 //PARAMv6 // 1

! VARIABLES LOCALES
  real,dimension(24) :: thor
  real :: tmoy

  ! Unites horaires
  if (P_codegdhdeb == 2) then                   ! option debourrement = f(temperature horaire)
    ! Pour l'instant seules temperatures air autorisees
    if (P_codetemp == 2) then                   ! option temperature air
      call EnvoyerMsgHistorique(logger, MESSAGE_49)
      ! Pour inaki j'enleve la condition car il veut utiliser debour pour les tp de l'air
      ! et les temperature de culture pour les stades
      !stop
    endif

    ! Reconstitution des temperatures horaires (pour l'instant que temperatures air)
    thor = get_hourly_values(tmin, tmax, tmin_demain)
    ! Calcul des gdh
    udevair = calcul_GDH(thor,P_tdmindeb,P_tdmaxdeb)
    udevcult = udevair
! 18/10/2016 Loic et Dr on ajoute notre test sur code_gdh_Wang et on essaie de voir avec Inaki
!  else
  endif
  if (P_code_WangEngel == 1) then                                      ! option debourrement = f(temperature journaliere, modele Wang & Engel)
! DR 09022016 A verifier qu'il n'y a pas d'impact pour la vigne par exemple
    tmoy = (tmin+tmax)/2.
    udevair = calcul_GDH2(tmoy,P_tdmindeb,P_tdmaxdeb,P_tdoptdeb)
    udevcult = udevair
  endif

  if (P_codetemp == 1) then                  ! pourquoi ce test ?
    upvt = udevcult * rfpi * rfvi
  endif

!  write(*,*) tmoy,udevair,rfpi,rfvi,upvt
!  call sleep(5)

return
end subroutine debour
end module debour_m
