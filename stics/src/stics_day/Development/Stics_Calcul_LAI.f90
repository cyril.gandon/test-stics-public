!    *****************************
!     calcul de l'indice foliaire
!           N.Brisson, R. Roche
!    *****************************
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! calculation of the LAI
! - Stics book paragraphe 3.1.1, page 40-44
!
!! In STICS, leaf area growth is driven by phasic development, temperature and stresses. An empirical plant density-dependent function represents inter-plant
!! competition. For indeterminate plants, trophic competition is taken into account through a trophic stress index, while for determinate plants a maximal
!! expansion rate threshold is calculated to avoid unrealistic leaf expansion.
!! The calculation of leaf growth rate (deltai in m2m-2 d-1) is broken down: a first calculation of the LAI growth rate (in m2 plant-1 degree-day-1) describes
!! a logistic curve, related to the ILEV, IAMF and ILAX phenological stages. This value is then multiplied by the effective crop temperature, the plant density
!! combined with a density factor, supposed to stand for inter-plant competition, that is characteristic for the variety, and the water and nitrogen stress indices.
!!
!! The phasic development function is comparable to that of the model PUTU (Singels and Jagger, 1991), i.e. a logistic function with dlaimaxbrut as asymptote
!! and pentlaimax as the slope at the inflection point. It is driven by a normalized leaf development unit (ULAI) equal to 1 at ILEV and 3 at ILAX.
!! At the end of the juvenile stage (IAMF), it is equal to vlaimax when the inflection of the dynamics (point of maximal rate) also occurs.
!! Between the stages ILEV, IAMF and ILAX, the model performs linear interpolation based on development units (upvt) which include all the environmental effects
!! on phasic development. As the ILAX stage approaches, it is possible to introduce a gradual decline in growth rate using the udlaimax parameter
!!- the ULAI value beyond which there is a decline in the leaf growth rate. If udlaimax=3 it has no effect and the leaf stops growing at once at ILAX.
!!
!! The thermal function relies on crop temperature and cardinal temperatures (tcmin and tcmax) which differ from the ones used for the phasic development.
!! The extreme threshold tcxstop is the same as for development.
!!
!! The density function is active solely after a given LAI threshold occurs (laicomp parameter) if the plant density (densite in plant m-2 possibly decreased
!! by early frost) is greater than the bdens threshold, below which plant leaf area is assumed independent of density.  Beyond this density value, leaf area
!! per plant decreases exponentially.  The adens parameter represents the ability of a plant to withstand increasing densities.  It depends on the species
!! and may depend on the variety.  For branching or tillering plants, adens represents the plants branching or tillering ability (e. g. wheat or pea).
!! For single-stem plants, adens represents competition between plant leaves within a given stand (e.g. maize or sunflower).
!!
!! Water and nitrogen affect leaf growth as limiting factors, i.e. stress indices whose values vary between 0 and 1. Water (turfac) and nitrogen deficits (innlai)
!! are assumed to interact, justifying the use of the more severe of the two stresses. Meanwhile at the whole plant level the water-logging stress index is assumed
!! to act independently
!!
!!
!!
! -    Features of determinate crops
!!   Failure to account for trophic aspects in the calculation of leaf growth may cause problems when the radiation intercepted by the crop is insufficient to
!!   ensure leaf expansion (e.g. for crops under a tree canopy or crops growing in winter).  Consequently, from the IAMF stage, we have introduced a trophic effect
!!  to calculate the definitive LAI growth rate in the form of a maximum threshold for leaf expansion (deltaimaxi in m2m-2d-1) using the notion of the maximum
!!   leaf expansion allowed per unit of biomass accumulated in the plant (sbvmax in cm2 g-1) and the daily biomass accumulation (dltams in t.ha-1day-1 possibly
!!   complemented by remobilized reserve remobilj). sbvmax is calculated using the slamax and tigefeuil parameters.
!!
! -    Features of indeterminate crops
!!   It has been possible to test the robustness of the above formalisation on a variety of crops, including crops where there is an overlap between the
!!   vegetative phase and the reproductive phase (soybean and flax for example).  However, when trophic competition between leaves and fruits is a driving force
!!   for the production and management of the crop (for example tomato, sugarbeet), this formalisation is unsuitable.  We therefore calculate the deltai variable
!!   so as to take trophic monitoring into consideration in the case of crops described as 'indeterminate', by introducing a trophic stress index (splai).
!!   As a consequence, the LAI can decrease markedly during the theoretical growth phase if the crop is experiencing severe stresses during the harvested
!!   organs filling phase.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module calai_m
USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
use CalculNombreDeFeuilles_m, only: CalculNombreDeFeuilles
implicit none
private
public :: calai
contains
subroutine calai(sc,pg,p,itk,c)
    type(Stics_Communs_),           intent(INOUT) :: sc  
    type(Parametres_Generaux_),     intent(INOUT) :: pg  
    type(Plante_),                  intent(INOUT) :: p  
    type(ITK_),                     intent(INOUT) :: itk  
    type(Climat_),                  intent(INOUT) :: c

!: variables locales
    integer :: ens  !  
    integer ::  n  

    ens = sc%ens
    n = sc%n

    call calai_(pg%P_codeinnact, pg%P_codeh2oact, p%P_codehypo, p%P_codegermin, itk%P_codcueille, p%P_codlainet,             &
                pg%codeulaivernal, p%P_codeindetermin, pg%P_codeinitprec, p%P_codeperenne, p%P_codetemp, p%turfac(ens),      &
                p%innlai(ens), p%P_laiplantule, p%P_phyllotherme, p%udevair, p%udevcult, p%P_dlaimaxbrut(itk%P_variete),     &
                p%P_udlaimax, sc%n, sc%numcult, sc%nbjmax, p%nplt, p%nlev, p%namf, p%nlax, p%ndrp, p%nrec, p%upvt(n),        &
                p%upvtutil, p%P_vlaimax, p%P_laicomp, p%somcour, p%somcourutp, p%P_stlevamf(itk%P_variete),                  &
                p%P_stamflax(itk%P_variete), p%densite, p%P_adens(itk%P_variete), p%P_bdens(itk%P_variete), p%P_tcxstop,     &
                sc%tcult, p%P_tcmin, p%P_tcmax, p%P_pentlaimax, p%P_dlaimin, p%exolai, p%sourcepuits(ens), p%P_splaimin,     &
                p%P_splaimax, p%P_slamin, p%P_slamax(itk%P_variete), p%P_tigefeuil(itk%P_variete), p%P_tustressmin,          &
                p%fstressgel, p%dltamsen(ens), p%sla(ens), p%remobilj(ens), p%dltams(ens,n-1), sc%tustress, p%efdensite,     &
                p%nstopfeuille, p%deltai(ens,n), p%splai(ens), p%fpv(ens,n), p%P_stlaxsen(itk%P_variete),  p%tempeff,        &
                p%lai(ens,:), p%somfeuille, p%nbfeuille, p%nsen, p%nlan, p%P_dlaimax(itk%P_variete), p%reajust, p%ulai,      &
                sc%vmax, p%dltaisenat(ens), p%laisen(ens,:), p%dltaisen(ens), p%P_stsenlan(itk%P_variete), p%densiteequiv,   &
                p%P_phobase(itk%P_variete), c%phoi, p%P_code_acti_reserve, p%codeinstal, p%ndes, p%ratioTF,                  &
                p%P_codephot_part, p%rfpi)

!: cumuls AO/AS
    p%splai(AOAS) = p%splai(AS) * p%surf(AS) + p%splai(AO) * p%surf(AO)
    p%fpv(AOAS,n) = p%fpv(AS,n) * p%surf(AS) + p%fpv(AO,n) * p%surf(AO)

    if (sc%P_codesimul == CODE_CULTURE) then
        if (p%P_codelaitr == 1) then
          p%lai(AOAS,n) = p%lai(AS,n) * p%surf(AS) + p%lai(AO,n) * p%surf(AO)
        endif
        p%deltai(AOAS,n) = p%deltai(AS,n) * p%surf(AS) + p%deltai(AO,n) * p%surf(AO)
        p%laisen(AOAS,n) = p%laisen(AOAS,n-1) + (p%laisen(AO,n) - p%laisen(AO,n-1)) * p%surf(AO)         &
                                                    + (p%laisen(AS,n) - p%laisen(AS,n-1)) * p%surf(AS)
    else !! CODE_FEUILLE
        p%lai(AOAS,n)    = p%lai(AS,n)
        p%laisen(AOAS,n) = p%laisen(AS,n)
        p%deltai(AOAS,n) = max(p%lai(AS,n) - p%lai(AS,n-1),0.0)
    endif
    p%dltaisen(AOAS) = p%dltaisen(AS) * p%surf(AS) + p%dltaisen(AO) * p%surf(AO)

 return
end subroutine calai

! ---------------------------------------------------------------------------------------------------------

subroutine calai_(P_codeinnact, P_codeh2oact, P_codehypo, P_codegermin, P_codcueille, P_codlainet, codeulaivernal,& !IN
                  P_codeindetermin, P_codeinitprec, P_codeperenne, P_codetemp, turfac, innlai,                         &
                  P_laiplantule, P_phyllotherme, udevair, udevcult, P_dlaimaxbrut, P_udlaimax, n, numcult, nbjmax,     &
                  nplt, nlev, namf, nlax, ndrp, nrec, upvt, upvtutil, P_vlaimax, P_laicomp, somcour, somcourutp,       &
                  P_stlevamf, P_stamflax, densite, P_adens, P_bdens, P_tcxstop, tcult, P_tcmin, P_tcmax, P_pentlaimax, &
                  P_dlaimin, exolai, sourcepuits, P_splaimin, P_splaimax, P_slamin, P_slamax, P_tigefeuil, P_tustressmin, &
                  fstressgel, dltamsen, sla, remobilj, dltams,                                                         & !IN
                  tustress, efdensite, nstopfeuille, deltai, splai, fpv, P_stlaxsen, tempeff,                          & !OUT
                  lai, somfeuille, nbfeuille, nsen, nlan, P_dlaimax, reajust, ulai, vmax, dltaisenat, laisen,          & !INOUT
                  dltaisen, P_stsenlan, densiteequiv, P_phobase, phoi,              &
                  P_code_acti_reserve, codeinstal, ndes, ratioTF,P_codephot_part,rfpi)

integer, intent(IN)    :: nbjmax ! la taille des tableaux journaliers  
integer, intent(IN)    :: P_codeinnact  ! // PARAMETER // code activating  nitrogen stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0 
integer, intent(IN)    :: P_codeh2oact  ! // PARAMETER // code to activate  water stress effect on the crop: yes (1), no (2) // code 1/2 // PARAM // 0 
integer, intent(IN)    :: P_codehypo  ! // PARAMETER // option of simulation of a  phase of hypocotyl growth (1) or planting of plantlets (2) // code 1/2 // PARPLT // 0 
integer, intent(IN)    :: P_codegermin  ! // PARAMETER // option of simulation of a germination phase or a delay at the beginning of the crop (1) or  direct starting (2) // code 1/2 // PARPLT // 0 
integer, intent(IN)    :: P_codcueille  ! // PARAMETER // way how to harvest // code 1/2 // PARTEC // 0 
integer, intent(IN)    :: P_codlainet  ! // PARAMETER // option of calculation of the LAI (1 : direct LAInet; 2 : LAInet = gross LAI - senescent LAI) // code 1/2 // PARPLT // 0
integer, intent(IN)    :: codeulaivernal  
integer, intent(IN)    :: P_codeindetermin  ! // PARAMETER // option of  simulation of the leaf growth and fruit growth : indeterminate (2) or determinate (1) // code 1/2 // PARPLT // 0 
integer, intent(IN)    :: P_codeinitprec  ! // PARAMETER // reinitializing initial status in case of chaining simulations : yes (1), no (2) // code 1/2 // PARAM // 0 
integer, intent(IN)    :: P_codeperenne  ! // PARAMETER // option defining the annual (1) or perenial (2) character of the plant // code 1/2 // PARPLT // 0 
integer, intent(IN)    :: P_codetemp  ! // PARAMETER // option calculation mode of heat time for the plant : with air temperature (1)  or crop temperature (2) // code 1/2 // PARPLT // 0 
real,    intent(IN)    :: turfac   ! // OUTPUT // Index of turgescence water stress  // 0-1
real,    intent(IN)    :: innlai   ! // OUTPUT // Index of nitrogen stress active on leaf growth // P_innmin to 1
real,    intent(IN)    :: P_laiplantule  ! // PARAMETER // Plantlet Leaf index at the plantation // m2 leaf  m-2 soil // PARPLT // 1 
real,    intent(IN)    :: P_phyllotherme  ! // PARAMETER // thermal duration between the apparition of two successive leaves on the main stem // degree C day // PARPLT // 1
real,    intent(IN)    :: udevair   ! // OUTPUT // Effective temperature for the development, computed with TAIR // degree.days
real,    intent(IN)    :: udevcult   ! // OUTPUT // Effective temperature for the development, computed with TCULT // degree.days
real,    intent(IN)    :: P_dlaimaxbrut  ! // PARAMETER // Maximum rate of the setting up of LAI // m2 leaf plant-1 degree d-1 // PARPLT // 1 
real,    intent(IN)    :: P_udlaimax  ! // PARAMETER // ulai from which the rate of leaf growth decreases  // SD // PARPLT // 1 
integer, intent(IN)    :: n  
integer, intent(IN)    :: numcult  
integer, intent(IN)    :: nplt  
integer, intent(IN)    :: nlev  
integer, intent(IN)    :: namf  
integer, intent(IN)    :: nlax  
integer, intent(IN)    :: ndrp  
integer, intent(IN)    :: nrec  
real,    intent(IN)    :: upvt   ! // OUTPUT // Daily development unit  // degree.days
real,    intent(IN)    :: upvtutil  
real,    intent(IN)    :: P_vlaimax  ! // PARAMETER // ULAI  at inflection point of the function DELTAI=f(ULAI) // SD // PARPLT // 1
real,    intent(IN)    :: P_laicomp  ! // PARAMETER // LAI from which starts competition inbetween plants // m2 m-2 // PARPLT // 1 
real,    intent(IN)    :: somcour   ! // OUTPUT // Cumulated units of development between two stages // degree.days
real,    intent(IN)    :: somcourutp  
real,    intent(IN)    :: P_stlevamf  ! // PARAMETER // Sum of development units between the stages LEV and AMF // degree.days // PARPLT // 1 
real,    intent(IN)    :: P_stamflax  ! // PARAMETER // Sum of development units between the stages AMF and LAX // degree.days // PARPLT // 1 
real,    intent(IN)    :: densite   ! // OUTPUT // Actual sowing density // plants.m-2
real,    intent(IN)    :: P_adens  ! // PARAMETER // Interplant competition parameter // SD // PARPLT // 1 
real,    intent(IN)    :: P_bdens  ! // PARAMETER // minimal density from which interplant competition starts // plants m-2 // PARPLT // 1 
real,    intent(IN)    :: P_tcxstop  ! // PARAMETER // threshold temperature beyond which the foliar growth stops // degree C // PARPLT // 1
real,    intent(IN)    :: tcult   ! // OUTPUT // Crop surface temperature (daily average) // degree C
real,    intent(IN)    :: P_tcmin  ! // PARAMETER // Minimum temperature of growth // degree C // PARPLT // 1
real,    intent(IN)    :: P_tcmax  ! // PARAMETER // Maximum temperature of growth // degree C // PARPLT // 1
real,    intent(IN)    :: P_pentlaimax  ! // PARAMETER // parameter of the logistic curve of LAI growth  // SD // PARPLT // 1 
real,    intent(IN)    :: P_dlaimin  ! // PARAMETER // accelerating parameter for the lai growth rate // SD // PARAMV6/PLT // 1 
real,    intent(IN)    :: exolai   ! // OUTPUT // Index for excess water active on growth in biomass // 0-1
real,    intent(IN)    :: sourcepuits   ! // OUTPUT // Pool/sink ratio // 0-1
real,    intent(IN)    :: P_splaimin  ! // PARAMETER // Minimal value of ratio sources/sinks for the leaf growth  // between 0 and 1 // PARPLT // 1 
real,    intent(IN)    :: P_splaimax  ! // PARAMETER // maximal sources/sinks value allowing the trophic stress calculation for leaf growing // SD // PARPLT // 1 
real,    intent(IN)    :: P_slamin  ! // PARAMETER // minimal SLA of green leaves // cm2 g-1 // PARPLT // 1 
real,    intent(IN)    :: P_slamax  ! // PARAMETER // maximal SLA of green leaves // cm2 g-1 // PARPLT // 1 
real,    intent(IN)    :: P_tigefeuil  ! // PARAMETER // stem (structural part)/leaf proportion // SD // PARPLT // 1 
real,    intent(IN)    :: P_tustressmin  ! // PARAMETER //  threshold stress (min(turfac,inns)) under which there is an effect on the LAI (supplementary senescence by ratio at the natural senescence) // SD // PARPLT // 1 
real,    intent(IN)    :: fstressgel   ! // OUTPUT // Frost index on the LAI // 0-1
real,    intent(IN)    :: dltamsen   ! // OUTPUT // Senescence rate // t ha-1 j-1
real,    intent(IN)    :: sla   ! // OUTPUT // Specific surface area // cm2 g-1
real,    intent(IN)    :: remobilj   ! // OUTPUT // Amount of biomass remobilized on a daily basis for the fruits  // g.m-2 j-1
real,    intent(IN)    :: dltams                ! (n-1)    // OUTPUT // Growth rate of the plant  // t ha-1.j-1
real,    intent(IN)    :: densiteequiv  !densite equivalente calculee chaque jour

real,    intent(OUT)   :: tustress   ! // OUTPUT // Stress index active on leaf growth (= minimum(turfac,innlai))  // 0-1
real,    intent(OUT)   :: efdensite  
integer, intent(OUT)   :: nstopfeuille  
real,    intent(OUT)   :: deltai                ! (n)    --(0:nbjmax)    // OUTPUT // Daily increase of the green leaf index // m2 leafs.m-2 soil
real,    intent(OUT)   :: splai   ! // OUTPUT // Pool/sink ratio applied to leaves // 0-1
real,    intent(OUT)   :: fpv   ! // OUTPUT // Sink strength of developing leaves // g.j-1.m-2
real,    intent(OUT)   :: P_stlaxsen  ! // PARAMETER // Sum of development units between the stages LAX and SEN // degree.days // PARPLT // 1 
real,    intent(OUT)   :: tempeff   ! // OUTPUT // Efficient temperature for growth // degree C

real,    intent(INOUT) :: lai(0:nbjmax)            ! (n), (n-1), (nsen), (nlan)    // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
real,    intent(INOUT) :: somfeuille  
integer, intent(INOUT) :: nbfeuille   ! // OUTPUT // Number of leaves on main stem // SD
integer, intent(INOUT) :: nsen  
integer, intent(INOUT) :: nlan  
real,    intent(INOUT) :: P_dlaimax  ! // PARAMETER // Maximum rate of the setting up of LAI // m2 leaf plant-1 degree d-1 // PARPLT // 1 
real,    intent(INOUT) :: reajust  
real,    intent(INOUT) :: ulai(0:nbjmax)        ! (n), (n-1)    // OUTPUT // Daily relative development unit for LAI // 0-3
real,    intent(INOUT) :: vmax  
real,    intent(INOUT) :: dltaisenat  
real,    intent(INOUT) :: laisen(0:nbjmax)        ! (n), (n-1)    // OUTPUT // Leaf area index of senescent leaves // m2 leaf  m-2 soil
real,    intent(INOUT) :: dltaisen   ! // OUTPUT // Daily increase of the senescent leaf index // m2.m-2 soil.d-1
real,    intent(INOUT) :: P_stsenlan  ! // PARAMETER // Sum of development units between the stages SEN et LAN // degree.days // PARPLT // 1 
real,    intent(IN)    :: P_phobase
real,    intent(IN)    :: phoi
integer, intent(IN)    :: P_code_acti_reserve
integer, intent(IN)    :: codeinstal
integer, intent(IN)    :: ndes
real,    intent(INOUT) :: ratioTF
! Loic Janvier 2021 :
integer, intent(IN)    :: P_codephot_part ! // PARAMETER // option for simulation of the effect of decreasing photoperiod on biomass allocation // code 1/2 // PARPLT // 0
real,    intent(IN)    :: rfpi

!: VARIABLES LOCALES
real :: deltaimaxi  
real :: sbvmin  
real :: sbvmax  

!DR 19/06/2019 j'ajoute un flag pour faire des ecritures de debug
 logical :: dbg_lai
 dbg_lai=.FALSE.


!: 1. CALCUL DU FACTEUR STRESS EAU ET N
!--------------------------------------
    if (P_codeinnact == 1 .and. P_codeh2oact == 1) tustress = min(turfac,innlai)
    if (P_codeinnact == 2 .and. P_codeh2oact == 1) tustress = min(turfac,1.0)
    if (P_codeinnact == 1 .and. P_codeh2oact == 2) tustress = min(1.0,innlai)
    if (P_codeinnact == 2 .and. P_codeh2oact == 2) tustress = 1.
    if(dbg_lai) write(1111,*)'tustress turfac innlai',n, tustress, P_codeinnact, P_codeh2oact, turfac, innlai

!: 2. INITIALISATIONS AVANT LE STADE NLEV (LEVEE):
!-------------------------------------------------
!: Dans le cas des cultures annuelles (P_codeperenne=1), trois cas:
!
!- a) pour les cultures qui se plantent (P_codehypo=2) et admettent un
!-    temps de latence avant le demarrage de la croissance (P_codegermin=1):
!-    initialisation du lai a P_laiplantule a partir de la plantation;
!-    et calcul de QNplantenp par courbe de dilution
!-
!- b) pour les cultures qui se sement (P_codehypo=1):
!-    initialisation du lai a 0 a partir du semis
!-
!- c) pour les cultures qui se plantent (P_codehypo=2) et demarrent directement (P_codegermin=2):
!-    initialisation du lai a P_laiplantule a partir de la levee qui est aussi la plantation
!-------------------------------------------------
    if (nlev == 0) then
! Modif Simon juin 2017 pour etre plus general
!       if (P_codeperenne == 1 .and. n >= nplt) then
        if (codeinstal == 0 .and. n >= nplt) then
           if (P_codehypo == 2 .and. P_codegermin == 1) then ! a)
              lai(n) = P_laiplantule
           else ! b)
              lai(n) = 0.
           endif
        else
           lai(n) = 0.
           somfeuille = 0.
        endif
        return
    endif

    ! le jour de la levee, si plantation par plantule, on force le lai du jour precedent
    if (n == nlev .and. P_codehypo == 2 .and. namf == 0) lai(n-1) = P_laiplantule

    !: a la recolte (nrec+1) : En cas de moisson, on enleve les feuilles
    if (nrec > 0 .and. n > nrec) then
        if (P_codcueille == 1) then
           lai(n) = 0.
           somfeuille = 0.
           return
        endif
    endif
! Ajout Loic Mars 2017 : on zappe la routine de calcul du lai apres la mort de la plante
    if (n >= ndes) return

    !: Calcul du nombre de feuilles
    if (P_codetemp == 2) then
       call CalculNombreDeFeuilles(P_phyllotherme,nlax,udevcult,somfeuille,nbfeuille)
    else
       call CalculNombreDeFeuilles(P_phyllotherme,nlax,udevair,somfeuille,nbfeuille)
    endif

    !: si option LAI brut+ senescence
    if (P_codlainet == 2) P_dlaimax = P_dlaimaxbrut

    !: 2a) calcul de reajust : distance de deveeloppement pour reajuster
    if (n == nlev) reajust = 0.
    if (n == namf) reajust = 0.
    if (n == nlax) reajust = 0.
    if (n == nsen) reajust = 0.
    if (n == nlan) reajust = 0.
    reajust = reajust + upvt - upvtutil

!: 3. CALCUL DE ULAI
!-------------------
    !: 3a) calcul de ulai entre lev et amf
    !- domi - 29/03 la vernalisation ne joue pas sur ulai si codeulaivernal = 1
    if (nlev > 0 .and. namf == 0) then
       if (codeulaivernal == 1) then
          ulai(n) = 1 + (P_vlaimax - 1) * (somcour + reajust) / (P_stlevamf + reajust)
    if(dbg_lai)write(1111,*)'ulai 1',n,nlev,namf,P_vlaimax,somcour,reajust,P_stlevamf,ulai(n)
       else
          if (somcourutp <= P_stlevamf) then
             ulai(n) = 1 + (P_vlaimax - 1) * (somcourutp + reajust) / (P_stlevamf + reajust)
    if(dbg_lai)write(1111,*)'ulai 2',n,nlev,namf,P_vlaimax,somcour,reajust,P_stlevamf,ulai(n)
          else
             ulai(n) = ulai(n-1)
    if(dbg_lai)write(1111,*)'ulai 3',n,nlev,namf,P_vlaimax,somcour,reajust,P_stlevamf,ulai(n)
        endif
      endif
    endif

    !: 3b) calcul de ulai entre amf et lax
    if (namf > 0 .and. nlax == 0) then
       if (codeulaivernal == 1) then
          ulai(n) = P_vlaimax + (3. - P_vlaimax) * (somcour + reajust) / (P_stamflax + reajust)
    if(dbg_lai)write(1111,*)'ulai 4',n,nlev,namf,P_vlaimax,somcour,reajust,P_stlevamf,ulai(n)
       else
          if (somcourutp <= P_stamflax) then
             ulai(n) = P_vlaimax + (3. - P_vlaimax) * (somcourutp + reajust) / (P_stamflax + reajust)
    if(dbg_lai)write(1111,*)'ulai 5',n,nlev,namf,P_vlaimax,somcour,reajust,P_stlevamf,ulai(n)
          else
             ulai(n) = ulai(n-1)
    if(dbg_lai)write(1111,*)'ulai 6',n,nlev,namf,P_vlaimax,somcour,reajust,P_stlevamf,ulai(n)
          endif
       endif
    endif

    !: 3c) ulai maximal atteint a drp pour les indeterminees
    if (P_codeindetermin == 2 .and. ndrp > 0) then
       ulai(n) = P_udlaimax
    else
      if (nlax > 0) ulai(n) = P_udlaimax
    endif
    if(dbg_lai)write(1111,*)'ulai',n,P_stlevamf,ulai(n)

!: 4. CALCUL du DELTAI BRUT
!--------------------------

    !: 4a) calcul du facteur densite efdensite actif a partir de P_laicomp
    !- calcul de l'effet densite sur la mise en place du LAI pour les stics-plante
    efdensite = 1.
    if (ulai(n) > 1.) then
       if (lai(n-1) < P_laicomp) then
          efdensite = 1.
       else
          !if ( densite == 0. ) then
          if (abs(densite).lt.1.0E-8) then
             efdensite = 0.
          else
! DR 12/09/2012 on prend la densite equivalente pour calculer l'effet densite et uniquement pour ca !
             efdensite = min(1.,(exp(P_adens * (log(densiteequiv / P_bdens)))))
          endif
       endif
    else
      !if (densite == 0) then
      if (abs(densite).lt.1.0E-8) then
          efdensite = 0.
      else
! DR 12/09/2012 on prend la densite equivalente pour calculer l'effet densite et uniquement pour ca !
          efdensite = min(1.,(exp(P_adens * log(densiteequiv / P_bdens))))
      endif
    endif

    !if (ulai(n) == 1.0) efdensite = 1
    if (abs(ulai(n)-1.0).lt.1.0E-8) efdensite = 1


    ! ** 4b) temperature efficace de croissance: limitation par P_tcmax
    ! NB le 13/06/06
    ! introduction d'une temperature maximale d'arret de croissance
    ! foliaire P_Tcxstop a l'occasion etude Franeoise sur fourrages mediterraneens
    if (P_tcxstop >= 100.) then
       if (tcult > P_tcmax) then
          tempeff = max(0.,(P_tcmax - P_tcmin))
       else
          tempeff = max(0.,(tcult - P_tcmin))
       endif
    else
       if (tcult > P_tcmax) then
          tempeff = (P_tcmax - P_tcmin) / (-P_tcxstop + P_tcmax) * (tcult - P_tcxstop)
          tempeff = max(0.,tempeff)
       else
          tempeff = max(0.,(tcult - P_tcmin))
       endif
    endif

    !: 4c) calcul du deltai
    !- quelle que soit l'option LAI choisie, LAI net ou LAI brut :
    !-  * la croissance s'arrete a LAX pour les determinees
    !-  * la croissance s'arrete a SEN pour les indeterminees
    if (P_codeindetermin == 1) nstopfeuille = nlax
    if (P_codeindetermin == 2) nstopfeuille = nsen
    if (P_codlainet == 2) nstopfeuille = nlax
    if (nstopfeuille == 0) then
    ! DR 14/03/2019 on avait oubli� de refaire le test sur les reels
    ! if (ulai(n) <= P_udlaimax .or. P_udlaimax == 3.) then
      if (ulai(n) <= P_udlaimax .or. abs(P_udlaimax-3.0).lt.1.0E-8) then
          deltai = P_dlaimax * efdensite * densite * (P_dlaimin + (1. - P_dlaimin)  &
                   / (1. + exp(P_pentlaimax * (P_vlaimax - ulai(n)))))
            if(dbg_lai) write(1111,*)'1, P_dlaimax,P_dlaimin, efdensite, densite, P_pentlaimax, P_vlaimax, ulai(n)',n, deltai, &
            & P_dlaimax,P_dlaimin, efdensite, densite, P_pentlaimax, P_vlaimax, ulai(n)
          vmax = deltai
       else
          deltai = vmax * (1. - (ulai(n) - P_udlaimax) / (3. - P_udlaimax))**2
         if(dbg_lai) write(1111,*)'2',deltai,vmax,ulai(n),P_udlaimax
       endif
       if (n > nlev .or. (P_codeperenne == 2 .and. P_codeinitprec == 2 .and. numcult > 1)) then
! DR 22/09/2015  je decoupe le calcul de deltai en 2 parties
        deltai = deltai * tempeff
        deltai = deltai * tustress * exolai
          if(dbg_lai) write(1111,*)'2,deltai,  tempeff , tustress , exolai',deltai,  tempeff , tustress , exolai
      endif
      if (P_codeindetermin == 2) then  ! plante indeterminee
         if (P_code_acti_reserve == 2) then
            splai = (sourcepuits - P_splaimin) / (P_splaimax - P_splaimin)
            splai = max(splai,0.)
            splai = min(splai,1.)
            if(dbg_lai) write(1111,*) 'splai line 417',splai,P_splaimax ,P_splaimin, sourcepuits
         endif
         if (P_code_acti_reserve == 1) then
            if (ndrp >= n) then
               splai = (sourcepuits - P_splaimin) / (P_splaimax - P_splaimin)
               splai = max(splai,0.)
               splai = min(splai,1.)
            else
               splai = 1.
            endif
         endif
         ! DR 13/08/2019 pour la vigne on a instancier ratiotF , ca doit poser pb
         ratioTF = P_tigefeuil
! Ajout Loic Septembre 2017: Modification du ratioTF & deltai potentiel pour la luzerne
! deltai diminue car effet photoperiode sur division cellulaire & elongation --> diminue la force puits
! des organes vegetatifs + croissance potentielle quand la photoperiode est decroissante
! Loic Janvier 2021 : remplacement par l'option codephot_part
!        if (P_code_acti_reserve == 1.and.P_codephot == 1.and.P_codeplante == CODE_FODDER) then
        if (P_codephot_part == 1) then
           if (phoi < P_phobase) then
             deltai = deltai * rfpi
             ratioTF = P_tigefeuil * rfpi
           else
             deltai = deltai * rfpi
             ratioTF = P_tigefeuil
           endif
        endif
        ! NB le 21/04 recalcul d'un sbv approprie
        if(dbg_lai) write(1111,*) 'deltai line 440',deltai
        if(dbg_lai) write(1111,*) 'ratioTF', ratioTF
        sbvmin = P_slamin / (1.0 + ratioTF)
        fpv = deltai * splai * 1.e4 / sbvmin
        deltai = deltai * splai
              if(dbg_lai) write(1111,*) 'deltai splai',deltai,splai
      else              ! plante determinee
        ratioTF = P_tigefeuil
        sbvmin = P_slamin / (1.0 + P_tigefeuil)
        fpv = deltai * 1e4 / sbvmin
        sbvmax = P_slamax / (1.0 + P_tigefeuil)
        ! ajout des remobilisations possible pour faire redemarrer le lai en cas de couvert completement sec
        deltaimaxi = (dltams + remobilj) * sbvmax / 100.
        ! limitation si le rayonnement est vraiment insuffisant apres AMF (cas des cultures associees)
            if(dbg_lai) write(1111,*)'deltai avant 3bis',deltai,dltams, remobilj
         !DR 21/01/2019 je desactive pour tester effet degradation lin
        if (namf > 0) deltai = min(deltai,deltaimaxi)
             if(dbg_lai) write(1111,*)'3 bis deltai,  deltaimaxi',deltai,  deltaimaxi
      endif
    else
      deltai = 0.
      if(dbg_lai) write(1111,*) 'deltai =0',deltai
    endif

!: 5. PHASE DE PLATEAU  (si option lainet et culture determinee)
!---------------------------------------------------------------
    if (P_codlainet == 1 .and. P_codeindetermin == 1 .and. nlax > 0 .and. nsen == 0) deltai = 0.

!: 6. Calcul de la senescence rapide par ajustement a la recolte entre sen et lan  pour les options 1 et 2
!---------------------------------------------------------------
    if (P_codlainet <= 1) then
       lai(n) =  lai(n-1) + deltai - dltaisenat
           if(dbg_lai) write(1111,*)'4 lai(n),lai(n-1) , deltai , dltaisenat',lai(n),lai(n-1) , deltai , dltaisenat
       if (n == nsen) lai(n) = lai(n-1)
      !if (nsen > 0 .and. nlan == 0.) then
       if (nsen > 0 .and. abs(nlan).lt.1.0E-8) then
          lai(n) = lai(nsen) * (1 - ((somcour+reajust) / (P_stsenlan+reajust)))
          dltaisenat = lai(n-1) - lai(n)
          if (lai(n) <= 0. .and. nlan == 0)  nlan = n
          if (n == nlan) lai(nlan) = 0.
       endif
    else
      !: option LAI brut la senescence (dltaisen) est calcule dans le spg senescen
      lai(n) = lai(n-1) + deltai - dltaisen
      if(dbg_lai) write(1111,*) 'calcul lai', lai(n-1), deltai, dltaisen, lai(n)
    endif
    if (lai(n) < 0.) lai(n) = 0.
    if (lai(n) <= 1e-4 .and. nstopfeuille > 0) lai(n) = 0.

    if(dbg_lai) write(1111,*)'apres phase plateau P_codlainet,deltai,dltaisen,lai(n)',P_codlainet,deltai,dltaisen,lai(n-1),lai(n)


!: 7. senescence foliaire due a des stress  pour l'option LAInet
!---------------------------------------------------------------
    if (P_codlainet == 1 .and. lai(n) > 0.) then
      !: NB le 08/04 ajout du stress gel
       if (tustress < P_tustressmin .or. tcult < P_tcmin .or. fstressgel < 1.) then
          dltaisen = dltamsen / 100. * sla
          lai(n) = lai(n) - dltaisen
          dltaisen = dltaisen + dltaisenat
          lai(n) = max(lai(n),0.)
              if(dbg_lai) write(1111,*)'5',dltamsen,dltaisen,sla,lai(n)

       else
          dltaisen = dltaisenat
       endif
    endif

    laisen(n) = laisen(n-1) + dltaisen

    !: NB le 22/04: calcul du stade lan pour P_codlainet = 2
    if (lai(n) <= 0 .and. nlan == 0 .and. nlax > 0) then
       if (nsen == 0) then
          nsen = n
          P_stlaxsen = somcour
       endif
       nlan = n
       P_stsenlan = somcour
       lai(n) = 0.
       dltaisen = 0.
       dltaisenat = 0.
    endif
            if(dbg_lai) write(1111,*)'fin calai',n, lai(n),deltai,dltaisen,nlan,nlax
return
end subroutine calai_
end module calai_m
