! ***
! *- Recalcul des dates de levee et LAX en fonction de la courbe de LAI observee
! *-
module recalcullevee_m
USE Plante, ONLY: AO, AS
implicit none
private
public :: recalcullevee
contains

subroutine recalcullevee(n,nlev,nlevobs,nlaxobs,lai,tauxcouv,P_codelaitr,is_dominant)
! DR 03122020 merge trunk
implicit none

  integer, intent(IN)    :: n  
  integer, intent(IN)    :: nlev  
  integer, intent(OUT)   :: nlevobs  
  integer, intent(OUT)   :: nlaxobs  
  real,    intent(INOUT) :: lai(2,n+1)   ! // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  real,    intent(OUT)   :: tauxcouv(n+1)   ! // OUTPUT // Cover rate // SD
  integer, intent(IN)    :: P_codelaitr  ! // PARAMETER // choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0 
  logical, intent(IN)    :: is_dominant  

!  integer :: AS=1  !
!  integer ::  AO=2

! le stade LEV est observe (dans le fichier technique) et par consequent il est impose dans Stics_LAI_devloppement
! cependant le LAI force (on est dans le cas P_codesimul='feuille') est nul; on impose donc la date de levee observee (lue dans le fichier technique)
! ML le 29/05/09
 
    if (nlev > 0) then
      if (lai(AS,n) <= 0. .and. lai(AS,n-1) <= 0.)then
        lai(AS,n) = 0.001
      endif

      if (.not.is_dominant) then
        if (lai(AO,n) <= 0. .and. lai(AO,n-1) <= 0.)then
          lai(AO,n) = 0.001
        endif
      endif

    else

! le LAI force est non nul a cette date, et pourtant la levee n'a pas encore eu lieu 
! (elle n'est pas observee, elle n'est pas calculee par Stics_Levee): on impose donc la date de levee correspondant au jour du 1er LAI >0
! du fichier des LAI journaliers forces
! ML le 29/05/09
    
      if (lai(AS,n) > 0. .or. lai(AO,n) > 0.) nlevobs = n
    endif

! le LAI force (du fichier des LAI journaliers forces) est maximal a cette date: on impose donc cette date comme la date du stade LAX
! ML le 29/05/09

    if (lai(AO,n) > lai(AO,n-1) .and. lai(AO,n) > lai(AO,n+1)) nlaxobs = n

    if (lai(AS,n) > lai(AS,n-1) .and. lai(AS,n) > lai(AS,n+1)) then
        nlaxobs = n
    endif

! si le LAI a ete modifie, et qu'on est en taux de couverture, il faut reaffecter la valeur a la variable tauxcouv
! ML le 29/05/09

    if (P_codelaitr == 2) tauxcouv(n) = lai(AS,n)


return
end
end module recalcullevee_m
