! Death of the plant and return of plant residues to soil
module plant_death_m
use messages
use Rhizomedeposition_m, only: Rhizomedeposition
use ResidusApportSurfaceSol_m, only: ResidusApportSurfaceSol
use ResiduParamDec_m, only: ResiduParamDec
use Rootdeposition_m, only: Rootdeposition
use plant_utils
private
public :: plant_death
contains
 subroutine plant_death(logger,irac, irhiz, nbRes, nrec, P_coderacine, codeperenne, codeplante, proprac, y0msrac, msrac_veille, &
                       masecnpveille, QNplantenpveille, qressuite, QNressuite, QCressuite, qressuite_tot, QNressuite_tot, &
                       QCressuite_tot, CNresmin, CNresmax, &
                       Qmulchdec, Cres, Nres, Cnondec, Nnondec, QCapp, QNapp, QCresorg, QNresorg, awb, bwb, cwb, CroCo, &
                       akres, bkres, ahres, bhres, Wb, kres, hres, msracmort, QCracmort, QNracmort, &
                       lai, hauteur, msrac, zrac, QCrac, QNrac, maperenne, QNperenne, masecnp, QNplantenp, masec, QNplante, &
                       mstot, QNtot, mafrais, pdsfruitfrais, mafraisfeuille, mafraistige, mafraisres, mafraisrec, longsperacf, &
                       longsperacg, drlsenf, drlseng, rlf_veille, rlg_veille, rlf, rlg, rltotf, rltotg, &
                       dltaperennesen, dltaQNperennesen, maperennemort, QCperennemort, QNperennemort, resperenne,  &
                       QNresperenne, matigestruc, mafeuil, mafeuilverte, mafeuiljaune, restemp, QNtige, QNfeuille, &
                       QNrestemp, QNvegstruc, QNveg, laisen, nh, epc, msracmortf, msracmortg, Ndfa, offrenod, Qfix, &
                       dltarestemp, Chum1, Nhum1, Chumi, Nhumi, CsurNsol, eai, P_code_rootdeposition, demande, demanderac, &
                       demandeper, demandetot, absoaer, absorac, absoper, absotot, Chuma, Nhuma, tauxcouv, P_codemsfinal)
  type(logger_), intent(in) :: logger
  integer, intent(IN) :: irac
  integer, intent(IN) :: irhiz             ! mean radius of the rhizome (supposed to be spherical)
  integer, intent(IN) :: nbRes
  integer, intent(IN) :: nrec
  integer, intent(IN) :: P_coderacine        ! Option for root growth root: standard profile (1) or true density (2) // code 1/2 // PARPLT
  integer, intent(IN) :: codeperenne
  !character(len=3)    :: codeplante
  character(len=3), intent(INOUT) :: codeplante
  real, intent(INOUT)    :: CroCo(nbRes)  ! parameter of organic residues decomposition // SD // PARAM // 1
  real, intent(IN)    :: awb(nbRes)    ! parameter  of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real, intent(IN)    :: bwb(nbRes)    ! parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real, intent(IN)    :: cwb(nbRes)    ! Minimum ratio C/N of microbial biomass in the relationship: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real, intent(IN)    :: akres(nbRes)  ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real, intent(IN)    :: bkres(nbRes)  ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real, intent(IN)    :: ahres(nbRes)  ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real, intent(IN)    :: bhres(nbRes)  ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real, intent(IN)    :: proprac           ! Slope of the relationship (root mass vs shoot mass) at harvest  // g.g-1 // PARAM // 0.20
  real, intent(IN)    :: y0msrac           ! minimal amount of root mass at harvest (when aerial biomass is nil) // t.ha-1 // PARAM // 0.7
  real, intent(INOUT) :: msrac_veille      ! root biomass at day n-1 // t.ha-1
  real, intent(IN)    :: masecnpveille(0:2)
  real, intent(INOUT) :: QNplantenpveille(0:2)   ! Amount of nitrogen accumulated in the plant at day n-1  // kgN.ha-1
  real, intent(IN)    :: CNresmin(nbRes) ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real, intent(IN)    :: CNresmax(nbRes) ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real, intent(IN)    :: Qmulchdec(nbRes)! maximal amount of decomposing mulch // t.ha-1 // PARAM // 1
  real, intent(INOUT) :: qressuite         ! quantity of residues from the previous crop // kg.ha-1
  real, intent(INOUT) :: QNressuite        ! quantity of N in residues from the previous crop // kg.ha-1
  real, intent(INOUT) :: QCressuite        ! quantity of C in residues from the previous crop // kg.ha-1
  real, intent(INOUT) :: qressuite_tot
  real, intent(INOUT) :: QNressuite_tot      ! quantity of N in residues from the previous crop // kg.ha-1
  real, intent(INOUT) :: QCressuite_tot     ! quantity of C in residues from the previous crop // kg.ha-1
  real, intent(INOUT) :: Cres(irac,nbRes)
  real, intent(INOUT) :: Nres(irac,nbRes)
  real, intent(INOUT) :: Cnondec(nbRes)       ! undecomposable C stock in residues on the surface // kg.ha-1
  real, intent(INOUT) :: Nnondec(nbRes)       ! undecomposable N stock in residues on the surface // kg.ha-1

  real, intent(INOUT) :: QCapp             ! cumulative amount of organic C added to soil // kg.ha-1
  real, intent(INOUT) :: QNapp             ! cumulative amount of organic N added to soil // kg.ha-1
  real, intent(INOUT) :: QCresorg
  real, intent(INOUT) :: QNresorg
  real, intent(INOUT) :: msracmort
  real, intent(INOUT) :: QCracmort         ! cumulative amount of C in dead roots added to soil //  kg.ha-1
  real, intent(INOUT) :: QNracmort         ! cumulative amount of N in dead roots added to soil //  kg.ha-1

  real, intent(INOUT) :: Wb(nbRes)
  real, intent(INOUT) :: kres(nbRes)
  real, intent(INOUT) :: hres(nbRes)

  real, intent(INOUT) :: lai(0:2)
  real, intent(INOUT) :: hauteur(0:2)
  real, intent(INOUT) :: zrac
  real, intent(INOUT) :: msrac
  real, intent(INOUT) :: QCrac             ! amount of C in living roots //  kg.ha-1
  real, intent(INOUT) :: QNrac             ! amount of N in living roots //  kg.ha-1
  real, intent(INOUT) :: rlf_veille(irac)
  real, intent(INOUT) :: rlg_veille(irac)
  real, intent(INOUT) :: rlf(irac)
  real, intent(INOUT) :: rlg(irac)
  real, intent(INOUT) :: maperenne(0:2)
  real, intent(INOUT) :: QNperenne(0:2)

  real, intent(INOUT) :: masecnp(0:2)
  real, intent(INOUT) :: QNplantenp(0:2)
  real, intent(INOUT) :: masec(0:2)
  real, intent(INOUT) :: QNplante
  real, intent(INOUT) :: mstot
  real, intent(INOUT) :: QNtot
  real, intent(INOUT) :: mafrais(0:2)
  real, intent(INOUT) :: pdsfruitfrais(0:2)
  real, intent(INOUT) :: mafraisfeuille(0:2)
  real, intent(INOUT) :: mafraistige(0:2)
  real, intent(INOUT) :: mafraisres(0:2)
  real, intent(INOUT) :: mafraisrec(0:2)

  real, intent(IN)    :: longsperacf
  real, intent(IN)    :: longsperacg
  real, intent(INOUT) :: drlsenf(irac)
  real, intent(INOUT) :: drlseng(irac)
  real, intent(INOUT) :: rltotf
  real, intent(INOUT) :: rltotg

  real, intent(INOUT) :: dltaperennesen(0:2)
  real, intent(INOUT) :: dltaQNperennesen(0:2)
  real, intent(INOUT) :: maperennemort(0:2)
  real, intent(INOUT) :: QCperennemort(0:2)
  real, intent(INOUT) :: QNperennemort(0:2)

  real, intent(INOUT) :: resperenne(0:2)
  real, intent(INOUT) :: QNresperenne(0:2)
  real, intent(INOUT) :: matigestruc(0:2)
  real, intent(INOUT) :: mafeuil(0:2)
  real, intent(INOUT) :: mafeuilverte(0:2)
  real, intent(INOUT) :: mafeuiljaune(0:2)
  real, intent(INOUT) :: restemp(0:2)
  real, intent(INOUT) :: QNtige
  real, intent(INOUT) :: QNfeuille
  real, intent(INOUT) :: QNrestemp
  real, intent(INOUT) :: QNvegstruc
  real, intent(INOUT) :: QNveg
  real, intent(INOUT) :: laisen(0:2)

  integer, intent(IN) :: nh
  integer, intent(IN) :: epc(nh)
  real, intent(INOUT) :: msracmortf(nh)
  real, intent(INOUT) :: msracmortg(nh)

  real, intent(INOUT) :: Ndfa
  real, intent(INOUT) :: offrenod(0:2)
  real, intent(INOUT) :: Qfix(0:2)
  real, intent(INOUT) :: dltarestemp(0:2)
  real, intent(INOUT) :: Chum1
  real, intent(INOUT) :: Nhum1
  real, intent(INOUT) :: Chumi
  real, intent(INOUT) :: Nhumi
  real, intent(INOUT) :: CsurNsol
  real, intent(INOUT) :: eai(0:2)
  integer, intent(IN) :: P_code_rootdeposition
  real, intent(INOUT) :: demande(0:2)
  real, intent(INOUT) :: demanderac(0:2)
  real, intent(INOUT) :: demandeper(0:2)
  real, intent(INOUT) :: demandetot
  real, intent(INOUT) :: absoaer(0:2)
  real, intent(INOUT) :: absorac(0:2)
  real, intent(INOUT) :: absoper(0:2)
  real, intent(INOUT) :: absotot(0:2)
  real, intent(INOUT) :: Chuma
  real, intent(INOUT) :: Nhuma
  real, intent(INOUT) :: tauxcouv
  integer, intent(IN) :: P_codemsfinal

  ! variables locales
  real  CCaer, CCper, CCrac
  real  Wr
  real  dltasentotf, dltasentotg
  real  msaer, QNaer, QCaer, CsurNaer, msper
  integer ires, i, iz, izmax, nhe, nbresid

  msaer = 0.
  msrac = 0.
  msper = 0.
    nbresid = (nbRes-1)/2
    CCaer = 420.   ! teneur en carbone des residus aeriens = 42% MS
    CCper = 440.   ! teneur en carbone des organes perennes = 44% MS
    CCrac = 380.   ! teneur en carbone des racines = 38% MS

! 1. Restitution des parties aeriennes (sauf si une recolte a eu lieu avant)
! ************************************
    if (nrec == 0 .or. codeplante == CODE_FODDER) then
          msaer = masecnpveille(0)
          QNaer = QNplantenpveille(0)
          QCaer = msaer * CCaer
          CsurNaer = 9999.
          if (QNaer > 1.e-6) CsurNaer = QCaer / QNaer
! residus aeriens
          qressuite = msaer
          QCressuite = QCaer
          QNressuite = QNaer
          qressuite_tot = qressuite_tot + msaer
          QCressuite_tot = QCressuite_tot + QCaer
          QNressuite_tot = QNressuite_tot + QNaer
! Decomposition des parties aeriennes mortes
          ires = 1
          call ResidusApportSurfaceSol(msaer, CCaer/10., -1.e-10, CNresmin(ires),CNresmax(ires), qmulchdec(ires), &
                                 CsurNaer, Cnondec(ires), Nnondec(ires), Cres(1,ires), Nres(1,ires), QCapp, QNapp, &
                                 QCresorg, QNresorg, CroCo(ires), Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)

          Wr = 0.
          if (CsurNaer > 0.) Wr = 1./CsurNaer
          call ResiduParamDec(&
            logger, awb(ires), bwb(ires), cwb(ires), akres(ires), bkres(ires), ahres(ires), bhres(ires),     &
                               Wr, CNresmin(ires), CNresmax(ires), Wb(ires), kres(ires), hres(ires))
     endif

! 2. Restitution des racines
! 2. Restitution des racines (sauf si une recolte a eu lieu avant)
! **************************
! Ajout Bruno juillet 2018 : supprimer le cas ou la restitution a ete faite a la recolte (dans apports)
  if (nrec == 0 .or. codeplante == CODE_FODDER) then
     msrac = msrac_veille
     if (P_coderacine == 1) then
         msrac = masecnpveille(0) * proprac + y0msrac ! profil type
     endif
! Racines qui deviennent mortes
     do iz = 1, irac
          drlsenf(iz) = rlf_veille(iz)
          drlseng(iz) = rlg_veille(iz)
     enddo
! Decomposition des racines mortes
     ires = nbRes
     call Rootdeposition(&
       logger, irac, drlsenf(1:irac), drlseng(1:irac), longsperacf, longsperacg, awb(ires), bwb(ires),  &
                         cwb(ires), akres(ires), bkres(ires), ahres(ires), bhres(ires), CNresmin(ires),           &
                         CNresmax(ires), QCrac, QNrac, Wb(ires), kres(ires), hres(ires), Cres(1:irac,ires),       &
                         Nres(1:irac,ires), msrac, msracmort, QCracmort, QNracmort, P_code_rootdeposition)
! Mise a jour des pools
     nhe = 0
     do i = 1, nh
        izmax = epc(i)
        do iz = 1, izmax
            msracmortf(i) = msracmortf(i) + drlsenf(nhe+iz)/longsperacf*100.
            msracmortg(i) = msracmortg(i) + drlseng(nhe+iz)/longsperacg*100.
        end do
        nhe = nhe + izmax
     end do
  endif

! 3. Restitution des organes perennes (si c'est une perenne)
! ***********************************
! Organes perennes qui deviennent morts
     if(codeperenne == 2) then
         msper = maperenne(0)
         dltaperennesen(:) = maperenne(:)
         dltaQNperennesen(:) = QNperenne(:)

! Decomposition des organes perennes morts
         ires = nbRes
         call Rhizomedeposition(&
           logger,irhiz, dltaperennesen(ires), dltaQNperennesen(ires), awb(ires), bwb(ires), cwb(ires), akres(ires), &
                                bkres(ires), ahres(ires), bhres(ires), CNresmin(ires), CNresmax(ires), Wb(ires),       &
                                kres(ires), hres(ires), Cres(1:irhiz,ires), Nres(1:irhiz,ires))
         dltarestemp = 0.
     endif

! 4. Mise a jour des pools
! ************************
! residus aeriens

! ensemble des residus (aeriens + racines mortes + rhizomes mmorts)
! bruno aout 2017
        qressuite_tot = msaer + msrac + msper
! Loic sept 2020 : ajout codemsfinal, pas de mise a zero des variables si on veut les conserver dans le fichier de sortie
! organes aeriens vegetatifs
        if (P_codemsfinal == 2) then
            masecnp(:) = 0.
            restemp(:) = 0.
            matigestruc(:) = 0.
            mafeuilverte(:) = 0.
            mafeuiljaune(:) = 0.
            mafeuil(:) = 0.
            QNplantenp(:) = 0.
            QNplantenpveille(:) = 0.
            QNveg = 0.
            QNvegstruc = 0.
            QNrestemp = 0.
            QNfeuille = 0.
            QNtige = 0.
        endif
        dltarestemp(:) = 0.
! racines
        msrac = 0.
        msrac_veille = 0.  ! faut-il imposer cela ?
        zrac = 0.
        QCrac = 0.
        QNrac = 0.
        dltasentotf = 0.
        dltasentotg = 0.
        do iz = 1, irac
          dltasentotf = dltasentotf + drlsenf(iz)
          dltasentotg = dltasentotg + drlseng(iz)
          drlsenf(iz) = 0.
          drlseng(iz) = 0.
          rlf_veille(iz) = 0.
          rlg_veille(iz) = 0.
          rlf(iz) = 0.
          rlg(iz) = 0.
        enddo
        rltotf = 0.
        rltotg = 0.
! Organes perennes
        maperennemort(:) = maperennemort(:) + dltaperennesen(:)
        QCperennemort(:) = QCperennemort(:) + dltaperennesen(:) * CCper
        QNperennemort(:) = QNperennemort(:) + dltaQNperennesen(:)
        maperenne(:) = 0.
        QNperenne(:) = 0.
        resperenne(:) = 0.
        QNresperenne(:) = 0.
! Loic sept 2020 : ajout codemsfinal, pas de mise a zero des variables si on veut les conserver dans le fichier de sortie
        if (P_codemsfinal == 2) then
            ! plante entiere (hors racines)
            masec(:) = 0.
            QNplante = 0.
            ! plante totale
            mstot = 0.
            QNtot = 0.
            mafrais(:) = 0.
            ! lai
            lai(:) = 0.
            tauxcouv = 0.
            laisen(:) = 0.
            hauteur(:) = 0.
            eai(:) = 0.
            ! Fruits
            pdsfruitfrais(:) = 0.
            mafraisfeuille(:) = 0.
            mafraistige(:) = 0.
            mafraisres(:) = 0.
            mafraisrec(:) = 0.
            ! Fixation symbiotique
            offrenod(:) = 0.
            Qfix(:) = 0.
            Ndfa = 0.
        endif

! Demande en N et flux de N
        demande(:) = 0.
        demanderac(:) = 0.
        demandeper(:) = 0.
        demandetot = 0.
        absoaer(:) = 0.
        absorac(:) = 0.
        absoper(:) = 0.
        absotot(:) = 0.
! Ajout Bruno novembre 2018 : la plante est morte
!        estVivante = .FALSE.

return
end subroutine plant_death
end module plant_death_m