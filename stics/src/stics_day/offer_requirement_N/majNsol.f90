! *------------------------------------------------------------------------------------------------------
! This module updates the nitrate and ammonium contents in soil due to root uptake over the rooting depth
! *------------------------------------------------------------------------------------------------------
module majNsol_m
implicit none
private
public :: majNsol
contains
subroutine majNsol(irac,absz,nit,amm)

  implicit none
  
  integer, intent(IN)    :: irac
  real,    intent(IN)    :: absz(irac)
  real,    intent(INOUT) :: nit(irac)
  real,    intent(INOUT) :: amm(irac)

!: Variables locales
  integer :: iz
  real    :: absamm
  real    :: absnit
  real    :: prop  
 
      do iz = 1,irac
        if (nit(iz) <= 0 .and. amm(iz) <= 0.) CYCLE
        !  l'absorption de NH4 et NO3 est calculee au prorata de leur quantite
        prop   = absz(iz) / (nit(iz) + amm(iz))
        absnit = prop * nit(iz)
        absamm = prop * amm(iz)
        nit(iz) =  nit(iz) - absnit
        amm(iz) =  amm(iz) - absamm
      end do

return 
end subroutine majNsol 
end module majNsol_m
