! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 8.6.2, 8.6.3., 8.6.4, page 98-100
!
!! The soil supply is the maximum amount of mineral N that the soil can deliver to the surface of roots, for a given status of soil and plant root system.
!! It is calculated for each elementary layer (1 cm thick) from the surface to the maximum rooting depth (zrac, in cm). It does not account for possible
!! nitrate upflow by capillary rise (this would require a knowledge of the nitrate concentration in the soil below the rooting depth).
!! The soil N supply in each soil layer (fluxsol, in kg N ha-1 day-1) is determined by the transport of mineral N from a given soil location to the nearest root
!! by convection and diffusion.
!!
!! The potential uptake rate in each soil layer is fluxrax (kg N ha-1 day-1). It is proportional to the effective root density which is limited by the
!! threshold lvopt above which uptake is no longer limited by root density.
!!
!! The mineral N available for root uptake in each layer (offrN) is equal to the smallest of the three terms: soil supply, uptake capacity and available mineral N.
!! The integration of offrN over the whole profile yields cumoffrN.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
module offreN_m
implicit none
private
public :: offreN
contains
subroutine offreN(zrac,nit,amm,hur,humin,hucc,flrac,P_lvopt,P_difN,epz,P_Vmax1,P_Kmabs1,P_Vmax2,P_Kmabs2, &
                  cumoffrN,flurac,flusol,offrN)


real, intent(IN)    :: zrac      ! // OUTPUT // Depth reached by root system // cm
real, intent(IN)    :: nit(int(zrac))  
real, intent(IN)    :: amm(int(zrac))  
real, intent(IN)    :: hur(int(zrac))  
real, intent(IN)    :: humin(int(zrac))  
real, intent(IN)    :: hucc(int(zrac))  
real, intent(IN)    :: flrac(int(zrac))  
real, intent(IN)    :: P_lvopt  ! // PARAMETER // Optimum root density // cm root.cm-3 soil // PARAM // 1 
real, intent(IN)    :: P_difN  ! // PARAMETER // coefficient de diffusion apparente du nitrate dans le sol humide // cm2 jour-1 // PARAM // 1 
real, intent(IN)    :: epz(int(zrac))  
real, intent(IN)    :: P_Vmax1  ! // PARAMETER // Rate of slow nitrogen absorption (high affinity system) // micromole cm-1 h-1 // PARPLT // 1 
real, intent(IN)    :: P_Kmabs1  ! // PARAMETER // Constant of nitrogen uptake by roots for the high affinity system // micromole. cm root-1 // PARPLT // 1 
real, intent(IN)    :: P_Vmax2  ! // PARAMETER // Rate of rapid nitrogen absorption (high affinity system) // micromole cm-1 h-1 // PARPLT // 1 
real, intent(IN)    :: P_Kmabs2  ! // PARAMETER // Constant of nitrogen uptake by roots for the low affinity system // micromole. cm root-1 // PARPLT // 1 

real, intent(INOUT) :: cumoffrN  
real, intent(INOUT) :: flurac      ! // OUTPUT // Nitrogen absorption flow associated with the limiting absorption capacity of the plant // kgN ha-1 j-1
real, intent(INOUT) :: flusol      ! // OUTPUT // Nitrogen absorption flux associated with limiting transfer soil  --> root  // kgN ha-1 j-1
real, intent(INOUT) :: offrN(int(zrac))  
  
!: Variables locales
  integer :: iz  !  
  integer :: izrac
  real    :: concN  !  
  real    :: conv  !  
  real    :: diff  !  
  real    :: fluxrac  !  
  real    :: fluxsol  !  
  real    :: rh  !  
  real    :: Vabs  
  real    :: lrac  

      izrac = int(zrac)
      cumoffrN = 0.
      flurac   = 0.
      flusol   = 0.
      offrN(:) = 0.

      ! *-----------------------------------------------------------------* c
      ! * 1. Offre du sol (flux d'arrivee a la racine, flux d'absorption) * c
      ! *-----------------------------------------------------------------* c
      ! * les calculs sont faits a partir de la surface du sol mais comme il 
      ! *  n'y a pas de racines de la surface a la profondeur de semis , cette
      ! *  tranche de sol est inoperante sur l'absorption d'azote
      !
      do iz = 1,izrac
        ! concentration en azote (kg.ha-1.mm-1)
        concN = (nit(iz) + amm(iz)) / hur(iz)
        ! longueur racinaire efficace
        lrac = flrac(iz) * P_lvopt
        ! ** flux de nitrate sol --> racine (kg.ha-1.jour-1)
        ! *- 1) flux diffusif             
        rh = (hur(iz) - humin(iz))
        rh = rh / (hucc(iz) - humin(iz))
        if (rh < 0.) rh = 0.
        diff = P_difN * rh

      ! *********************************************************************
      ! ** DR et Bruno Mary le 051208
      !  version modifiee (equation 8.35 du bouquin)
        diff = diff * 7.09 * (nit(iz) + amm(iz)) * sqrt(lrac)
      !  ancienne version
      !--diff = diff * 70.9 * concN * sqrt(lrac)
      ! *********************************************************************
      ! unites :  P_difN: cm2.jour-1 ; lrac: cm.cm-3 ; concN: kg.ha-1.mm-1

        !: 2) flux convectif 
        conv = epz(iz) * concN    ! cumul epz => epzC => epz(0)

        ! unites :  epz(iz): mm.jour-1 ; concN: kg.ha-1.mm-1

        !: 3) flux total              
        fluxsol = conv + diff


        ! Ce flux peut etre limite par le stock N disponible                       
        fluxsol = min(fluxsol,(nit(iz)+amm(iz)))

        !: Flux "actif" d'absorption racinaire
        !- passage de kg.ha-1.mm-1 a micromole.l-1
        concN = concN / 140. * 1e6         
  
        !: Hypothese : meme cinetique MM pour NH4 et NO3      
        ! 04/01/2019 DR je remets l'ancienne formule separee en 2 lignes car les 2 formulations ne sont pas egales !
        ! 13/02/2019 je mets la nouvelle pour test differnece
         Vabs = (P_Vmax1 / (P_Kmabs1 + concN)) + (P_Vmax2 / (P_Kmabs2 + concN))
        !Vabs = P_Vmax1 / (P_Kmabs1 + concN)
        !Vabs = Vabs + P_Vmax2 / (P_Kmabs2 + concN)

        fluxrac = Vabs * concN * lrac
        !: Passage de micromole.cm-2.h-1 a kg.ha-1.j-1     
        fluxrac = fluxrac * 33.6

        !: L'absorption est limitee par le plus petit des 2 flux                   
        offrN(iz) =  min(fluxsol, fluxrac)
        ! absorption cumulee sur toutes les couches de sol
        cumoffrN = cumoffrN + offrN(iz)

        ! flusol = composante du flux d'absorption limite par le transfert dans le sol, cumule sur le profil
        ! flurac = composante du flux d'absorption limite par la capacite de la plante, cumule sur le profil
        ! Modif Bruno juin 2013 pour mieux comprendre : flux d'absorption = min(demandetot, flurac, flusol)
        if (fluxsol  <  fluxrac) then
          flusol = flusol + fluxsol
        else
          flurac = flurac + fluxrac
        endif
      end do
   return
   end subroutine offreN
end module offreN_m
 
