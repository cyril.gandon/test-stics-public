! *----------------------------------------------------------------------------* c
! * Repartition de l'azote entre structure et reserve dans les organes aeriens * c
! * programmation L. Strullu le 11/08/2016                                     * c
! *----------------------------------------------------------------------------* c
! L'azote structural est calcule a l'aide du C/N des tiges et des feuilles : parazotmorte et parazofmorte.
! Le C/N des organes aeriens varie en fonction de l'etat de nutrition azote des organes aeriens grace a
! la prise en compte de l'INN. On calcule en premier lieu l'azote structural accumule dans la plante du
! a l'accumulation de biomasse structurale dans les feuilles et les tiges. Si l'absorption d'azote est limitante,
! l'azote est alloue preferentiellement aux feuilles. La difference entre l'azote accumule dans la partie vegetative
! et l'azote structural correspond a l'azote de reserve (QNrestemp) contenu dans les organes aeriens.
!
!-----------------------------------------------------------------------------
module repartirN_m
use plant_utils
implicit none
private
public :: repartirN
contains
subroutine repartirN(n, nrec, P_parazofmorte, P_parazotmorte, inn, mafeuil, mafeuilp, matigestruc, matigestrucp, absoaer, &
                     dltaremobilN, QNgrain, QNvegstruc, QNrestemp, QNveg, QNplantenp, QNfeuille, QNtige, P_codeplante)

  implicit none

    integer, intent(IN)    :: n
    integer, intent(IN)    :: nrec
    real,    intent(IN)    :: P_Parazofmorte
    real,    intent(IN)    :: P_Parazotmorte
    real,    intent(IN)    :: inn
    real,    intent(IN)    :: mafeuil
    real,    intent(IN)    :: mafeuilp
    real,    intent(IN)    :: matigestruc
    real,    intent(IN)    :: matigestrucp
    real,    intent(IN)    :: absoaer
    real,    intent(IN)    :: dltaremobilN
    character(len=3), intent(IN) :: P_codeplante
    real,    intent(INOUT) :: QNgrain
    real,    intent(INOUT) :: QNvegstruc
    real,    intent(INOUT) :: QNrestemp
    real,    intent(INOUT) :: QNveg
    real,    intent(INOUT) :: QNplantenp ! Nitrogen content in aerial non perennial organs kgN ha-1
    real,    intent(INOUT) :: QNfeuille  ! Strutural nitrogen content in leaves kgN ha-1
    real,    intent(INOUT) :: QNtige     ! Structural nitrogen content in stems kgN ha-1

!: Variables locales
    real :: CsurNfeuil      ! ratio C/N of leaves
    real :: CsurNtige       ! ratio C/N of stems
    real :: dltamafeuil     ! structural biomass of leaves products on d day
    real :: dltamatigestruc ! structural biomass of stems products on d day
    real :: dltaQNvegstruc  ! structural nitrogen allocated to structural abovegroud biomass on d day
    real :: dltaQNfeuille
    real :: dltaQNtige
    real :: Cshoot        ! Carbon concentration in leaves and stems (g/kg)

     Cshoot = 440.

         if (nrec > 0 .and. n >nrec .and. P_codeplante /=CODE_FODDER) then
           QNplantenp = 0.
           QNveg = 0.
           QNgrain = 0.
           QNvegstruc = 0.
           QNrestemp = 0.
           QNfeuille = 0.
           QNtige = 0.
           return
         endif

         if (inn > 0.) then   ! que se passe t il si inn = 0 ?
 ! Modif Bruno avril 2017 pour eviter que QNrestemp soit negatif (luzerne en automne)
    ! QNrestemp = QNveg - QNvegstruc doit etre positif
    ! QNplantenp - QNgrain - QNvegstruc - dltaQNvegstruc doit etre positif
    ! donc dltaQNvegstruc < QNplantenp - QNgrain - QNvegstruc
           QNveg = QNplantenp - QNgrain
           CsurNfeuil = P_Parazofmorte  / inn
           CsurNtige  = P_Parazotmorte  / inn
           dltamafeuil = mafeuil - mafeuilp
           if (dltamafeuil < 0.) dltamafeuil = 0.
           dltamatigestruc = matigestruc - matigestrucp
           if (dltamatigestruc < 0.) dltamatigestruc = 0.

           dltaQNfeuille = Cshoot * dltamafeuil / CsurNfeuil
           dltaQNtige = Cshoot * dltamatigestruc / CsurNtige
           dltaQNvegstruc = dltaQNfeuille + dltaQNtige
           if (dltaQNvegstruc > absoaer + dltaremobilN)  dltaQNvegstruc = absoaer + dltaremobilN
           if (dltaQNvegstruc > QNplantenp - QNgrain - QNvegstruc)  dltaQNvegstruc = QNplantenp - QNgrain - QNvegstruc
           dltaQNfeuille = amin1(dltaQNvegstruc,dltaQNfeuille)
           dltaQNtige = amax1(0.,dltaQNvegstruc - dltaQNfeuille)

           QNfeuille = QNfeuille + dltaQNfeuille
           QNtige = QNtige + dltaQNtige
           QNvegstruc = QNvegstruc + dltaQNvegstruc
           QNrestemp = QNveg - QNvegstruc


    !       CsurNfeuil = P_Parazofmorte  / inn
    !       CsurNtige  = P_Parazotmorte  / inn
    !       dltamafeuil = mafeuil - mafeuilp
    !       dltamatigestruc = matigestruc - matigestrucp
    !       if (dltamafeuil < 0.) dltamafeuil = 0.
    !       if (dltamatigestruc < 0.)  dltamatigestruc = 0.
    !       dltaQNfeuille = 440. * dltamafeuil / CsurNfeuil
    !       dltaQNtige = 440. * dltamatigestruc / CsurNtige
    !       dltaQNvegstruc = dltaQNfeuille + dltaQNtige
    !       if (dltaQNvegstruc > (absoaer + dltaremobilN)) then
    !          dltaQNvegstruc = absoaer + dltaremobilN
    !          dltaQNfeuille = min(dltaQNvegstruc,dltaQNfeuille)
    !          dltaQNtige = max(0.,dltaQNvegstruc - dltaQNfeuille)
    !       endif
    !       QNfeuille = QNfeuille + dltaQNfeuille
    !       QNtige = QNtige + dltaQNtige
    !       QNvegstruc = QNvegstruc + dltaQNvegstruc
    !       QNveg = QNplantenp - QNgrain
    !       QNrestemp = QNveg - QNvegstruc
         endif

end subroutine
end module repartirN_m
 
