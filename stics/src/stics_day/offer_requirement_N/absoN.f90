! DR le 06/01/06 on a decompose offren en absoN et majNsol
! on le fait sur la partie a l'ombre et au soleil
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This module calculates the actual N uptake.
! - Stics book paragraphe 8.6.4, page 100
!
!! The mineral N available for root uptake in each layer (offrN) is equal to the smallest of the three terms: soil supply, uptake capacity and available mineral N.
!! The integration of offrN over the whole profile yields cumoffrN. In each layer, the ratio of crop demand (demandetot) to soil offer (offrN) is calculated (prop).
!!
!! If prop >= 1, the soil N offer is the factor limiting N uptake. In this case the N uptake in each layer is equal to the N offer (offrN).
!! If prop < 1, the N demand is the factor limiting N uptake; in this case, the actual N uptake in each layer is smaller than the N offer and
!! proportional to it.
!!
!! The actual N uptake in each soil layer (absz) and the total uptake over the root profile (abso) can be written as functions of the prop variable.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
module absoN_m
implicit none
private
public :: absoN
contains
subroutine absoN(irac, offrenod, cumoffrN, surf, offrN, nit, amm, demande, demandeper, demanderac, P_codelegume,     &
                 P_codesymbiose, P_PropresPN, dNdWcrit, dltams, dltarespstruc, fixreel, dltaremobilN, absz, profextN, &
                 QNplantenp, QNperenne, QNrac, QNresperenne, Ndfa, absoaer, absoper, absorac, abso, absotot,   &
                 QNabsoaer, QNabsoper, QNabsorac, QNabso, QNabsotot, Qfix, Qfixtot, P_code_ISOP, P_code_pct_legume, &
                 P_pct_legume)

  integer, intent(IN)    :: irac             ! depth reached by root system                 // cm
  real,    intent(INOUT) :: offrenod         ! available N due to biological N fixation     // kg.ha-1
  real,    intent(IN)    :: cumoffrN         ! available mineral N for uptake in all layers // kg.ha-1
  real,    intent(IN)    :: surf             !  // OUTPUT //Fraction of surface in the shade             // 0-1
  real,    intent(IN)    :: offrN(irac)      ! available mineral N for uptake in each layer // kg.ha-1.cm-1
  real,    intent(IN)    :: nit(irac)        ! available NO3-N in each layer                // kg.ha-1.cm-1
  real,    intent(IN)    :: amm(irac)        ! available NH4-N in each layer                // kg.ha-1.cm-1
  real,    intent(IN)    :: demande          ! // OUTPUT // Daily N requirement of aerials               // kg.ha-1.d-1
  real,    intent(IN)    :: demandeper       ! Daily N requirement of perennial organs      // kg.ha-1.d-1
  real,    intent(IN)    :: demanderac       ! Daily N requirement of roots                 // kg.ha-1.d-1
  integer, intent(IN)    :: P_codelegume
  integer, intent(IN)    :: P_codesymbiose
  real,    intent(IN)    :: P_PropresPN
  real,    intent(IN)    :: dNdWcrit
  real,    intent(IN)    :: dltams
  real,    intent(IN)    :: dltarespstruc
  real,    intent(IN)    :: fixreel

  real,    intent(INOUT) :: dltaremobilN     ! N transfer from perennial to aerial organs   // kg.ha-1.d-1
  real,    intent(INOUT) :: absz(irac)       ! Daily N uptake in each soil layer           // kg.ha-1.d-1
  real,    intent(INOUT) :: profextN         ! Average depth of N uptake (1 order moment)  // cm
  real,    intent(INOUT) :: QNplantenp       ! Amount of N in aerial parts                 // kg.ha-1
  real,    intent(INOUT) :: QNperenne        ! Amount of N in perennial organs             // kg.ha-1
  real,    intent(INOUT) :: QNrac            ! Amount of N in roots                        // kg.ha-1
  real,    intent(INOUT) :: QNresperenne
  real,    intent(INOUT) :: Ndfa

  real,    intent(INOUT) :: absoaer
  real,    intent(INOUT) :: absoper
  real,    intent(INOUT) :: absorac
  real,    intent(INOUT) :: abso             ! Nitrogen absorption rate by plant           // kg.ha-1.d-1
  real,    intent(INOUT) :: absotot
  real,    intent(INOUT) :: QNabsoaer
  real,    intent(INOUT) :: QNabsoper
  real,    intent(INOUT) :: QNabsorac
  real,    intent(INOUT) :: QNabso           ! Cumulative N absorption                     // kg.ha-1
  real,    intent(INOUT) :: QNabsotot
  real,    intent(INOUT) :: Qfix
  real,    intent(INOUT) :: Qfixtot
  ! Ajout Loic Mais 2021 : specificite ISOP
  integer, intent(IN)    :: P_code_ISOP
  integer, intent(IN)    :: P_code_pct_legume
  real,    intent(IN)    :: P_pct_legume

! Variables locales
  integer :: iz
  real    :: prop
  real    :: cumoffre
  real    :: absotot1
  real    :: absotot2
  real    :: dltarespstrucN
  real    :: dltaresperN
  real    :: dNdW
  real    :: demandetot

  !DR 09/08/2019 j'ajoute un flag pour faire des ecritures de debug
  logical :: dbg_N
  dbg_N=.FALSE.


      absz(:) = 0.
      abso = 0.
      absoaer = 0.
      absorac = 0.
      absoper = 0.
      absotot = 0.
      offrenod = 0.

! Demande totale en azote de la plante (ponderee par surface)
      demandetot = (demande + demanderac + demandeper) !* surf
! Calcul de l'offre effective des nodules
      if (P_codelegume == 2) then
         if(P_codesymbiose == 1) then
            dNdW = dNdWcrit
            offrenod = dNdW * dltams * 10. * surf             ! il faut ponderer dltams par surf ?
         else
            offrenod = amin1(demandetot, fixreel * surf)     ! il faut ponderer fixreel par surf
         endif
         demandetot = demandetot - offrenod
         Qfix = Qfix + offrenod                   ! fixation cumulee entre 2 fauches
         Qfixtot = Qfixtot + offrenod             ! fixation cumulee sur toute l'usm
      endif

! Loic mai 2021 : ajout de la fixation symbiotique des prairies avec legumineuses pour ISOP
! Une partie de l'azote est fourni par les legumineuses en fonction de leur part dans la prairie
! en considerant que la presence de legumineuses reduit la demande en azote
      if (P_code_ISOP == 1.and.P_code_pct_legume == 1) then
         offrenod = dNdWcrit * dltams * 10. * surf
         offrenod = offrenod * P_pct_legume
         demandetot = demandetot - offrenod
         Qfix = Qfix + offrenod                   ! fixation cumulee entre 2 fauches
         Qfixtot = Qfixtot + offrenod             ! fixation cumulee sur toute l'usm
      endif

! *-------------------------------------------------
! * Confrontation offre/demande et absorption reelle
! *-------------------------------------------------
      cumoffre = cumoffrN * surf       ! l'offre pour chaque partie de la plante (AS, AO) est proportionnelle a sa surface
      prop = 0.
      if(cumoffre > 0.) prop = demandetot / cumoffre
      if(prop < 0.) prop = 0. ! si l'offre des nodules couvre tous les besoins de la plante
      if(prop < 1.) then      ! absorption regulee par la demande
         do iz = 1,irac
            absz(iz) = offrN(iz) * prop * surf
         end do
      else                    ! absorption regulee par l'offre
         do iz = 1,irac
            absz(iz) = offrN(iz) * surf
         end do
      endif
      ! DR 25/02/08 calcul de la profondeur moyenne d'extraction d'azote
      profextN = 0.
 ! Bruno abso est en fait abso(n), mis a 0 au depart
      do iz = 1,irac
         if (nit(iz) <= 0 .and. amm(iz) <= 0.) CYCLE
         abso = abso + absz(iz)
         profextN = profextN + (absz(iz) * iz)
      end do
      if (abso > 0.) profextN = profextN / abso

      if(dbg_N) write(4444,*)'absoN',abso,irac

 ! Allocation du flux d'absorption entre p. aeriennes et racines
 ! Modifs Loic Avril 2016: on ajoute une allocation vers les organes perennes, prioritaire p/r
 ! aux racines, prioritaires p/r aux organes aeriens. Les pools sont mis a jour plus bas.

      absotot = abso + offrenod
      absoper = amin1(demandeper,absotot)
      absotot1 = absotot - absoper
      if (absotot1 > 0.) then
         absorac = amin1(demanderac,absotot1)
      else
         absorac = 0.
      endif
      absotot2 = absotot1 - absorac
      if (absotot2 > 0.) then
         absoaer = amin1(demande,absotot2)
      else
         absoaer = 0.
      endif

 ! Modifs Bruno et Loic juillet 2013 : calcul des flux d'absorption
      absotot = absoaer + absorac + absoper
      abso = absotot - offrenod
      QNabsoaer = QNabsoaer + absoaer
      QNabsorac = QNabsorac + absorac
      QNabsoper = QNabsoper + absoper
      QNabsotot = QNabsotot + absotot
      QNabso = QNabso + abso

 ! Mise a jour de N plante
      QNplantenp = QNplantenp + absoaer
      QNperenne = QNperenne + absoper
      QNrac = QNrac + absorac

! Actualisation des reserves N des perennes
      if (dltarespstruc > 0.) then
          dltarespstrucN = absoper * (1. - P_propresPN)
          dltaresperN = absoper * P_propresPN
      else
          dltarespstrucN = 0.
          dltaresperN = absoper
      endif
      QNresperenne = QNresperenne + dltaresperN

      if(dbg_N) write(4444,*) 'QNplantenp',QNplantenp, 'absoaer',absoaer,'QNresperenne', QNresperenne,'QNperenne', QNperenne
      !if(debug_N) write(4444,*) 'QNrac',QNrac

  ! Ajout Loic Juin 2016: calcul du taux de fixation pour les legumineuses
      if (absotot + dltaremobilN > 0.) then
         Ndfa = offrenod / (absotot + dltaremobilN)
      else
         Ndfa = 0.
      endif

return
end subroutine absoN
end module absoN_m
 
