! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This module calculates the crop N demand.
! - Stics book paragraphe 8.6.1, page 157-159
!
!! If NMAX is the maximal crop nitrogen content and written as a function of plant biomass (W that can be slightly different from masec), the daily N demand
!! (demande, in kg N ha-1 day-1) is the product of the crop growth rate (dltams, in t ha-1 day-1) and the derivative of NMAX relative to W.
!! In STICS the expression of NMAX varies as a function of 2 criteria: the density of the canopy and the presence of storage organs; the first one defining
!! the parameters of the NMAX=f(W) curves (according to Lemaire and Gastal, 1997, or Justes et al., 1997) and the second one defining W.
! - The dilution curves
!!
!!   Two curves define the critical response function: one for low biomass corresponding to isolated plants and one for high biomass with dense canopies.
!!   Similarly, two curves can be derived to characterize the N demand of these two populations. These 4 curves can be described by similar power functions.
!!   In addition to the prescribed parameters adil and bdil, the other parameters are obtained using the following assumptions:
!   -   There is a value of metabolic-N concentration (nmeta) corresponding to the plantlet nitrogen content that is composed of functional organs only.
!!       This value is a function of species metabolism: 6.47% for C3 crops (e.g. wheat) and 4.8% for C4 crops (e.g. maize) (Justes et al., 1997; Lemaire and Gastal, 1997).
!   -   It is possible to define an arbitrary biomass for this plantlet status (masecmeta = 0.04 t ha-1; Justes et al., 1997).
!   -   It is possible to define experimentally the biomass value at the intersection of the two curves that depends on the form of the canopy (masecdil) and
!!       at this point the reserve N content is Nres.
!!   The curvature of the maximal curve is the same than that of the critical curve for dense canopy: bdilmax=bdil
! - The presence of storage organs
!!
!!   The N demand due to vegetative organs is assumed to follow the maximal dilution curve, whereas the demand associated with the "fruit"
!!   (either grains or storage organs) depends on the nitrogen status of the crop through the variable absodrp. The biomass (W) used to calculate the N demand
!!   from the maximal dilution curve can be reduced using the parameters inngrain1 and inngrain2.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
module bNpl_m
implicit none
private
public :: bNpl
contains
 subroutine bNpl(nbrecolte, masecnp, rdtint, surf, dltams, P_codeplisoleN, P_masecNmax, P_masecmeta, adilmaxI,  &
                  bdilmaxI, adilI, bdilI, P_adilmax, P_bdilmax, P_adil, P_bdil, dltags, dltamsN, P_inngrain1,    &
                  P_inngrain2, dltaremobilN, masecdil, masecpartiel, inns, innlai, abso, dNdWcrit, deltabso, absodrp, &
           ! DR 19/02/2019 substitution de parazorac en tant que code par un vrai code P_code_rootdeposition
           !       dltarestemp, P_Parazorac, INN, P_INNmin, dltamsrac, nstopres, nlax, dltarestempN, P_Parazoper, &
                  dltarestemp, P_code_rootdeposition, P_Parazorac,                                               &
                  INN, P_INNmin, dltamsrac, nstopres, nlax, dltarestempN, P_Parazoper,                           &
                  P_code_acti_reserve, fstressgel, demande, demandeper, demanderac, QNperenne, QNresperenne, QNplantenp,&
                  maperenne)

  implicit none

  integer, intent(IN)    :: nbrecolte
  real,    intent(IN)    :: masecnp      ! // OUTPUT // Aboveground dry matter  // t.ha-1
  real,    intent(IN)    :: rdtint(nbrecolte-1)
  real,    intent(IN)    :: surf         ! // OUTPUT // Fraction of surface in the shade // 0-1
  real,    intent(IN)    :: dltams       !// OUTPUT // Growth rate of the plant  // t ha-1 d-1
  integer, intent(IN)    :: P_codeplisoleN  ! // PARAMETER //code for N requirement calculations at the beginning of the cycle: dense plant population (1), isolated plants (2, new formalisation) // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: P_masecNmax  ! // PARAMETER //Aerial biomass  on and after there is nitrogen dilution (critical and maximal curves) // t ha-1 // PARPLT // 1
  real,    intent(IN)    :: P_masecmeta  ! // PARAMETER //biomass of the plantlet supposed to be composed of metabolic nitrogen // t ha-1 // PARPLT // 1
  real,    intent(IN)    :: adilmaxI  
  real,    intent(IN)    :: bdilmaxI  
  real,    intent(IN)    :: adilI  
  real,    intent(IN)    :: bdilI  
  real,    intent(IN)    :: P_adilmax    ! // PARAMETER //Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // N% MS // PARPLT // 1
  real,    intent(IN)    :: P_bdilmax    ! // PARAMETER //Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // SD // PARPLT // 1
  real,    intent(IN)    :: P_bdil       ! // PARAMETER //parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // SD // PARPLT // 1
  real,    intent(IN)    :: P_adil       ! // PARAMETER //Parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // N% MS // PARPLT // 1
  real,    intent(IN)    :: dltags       ! // OUTPUT // Growth rate of the grains  // t ha-1 d-1
  real,    intent(IN)    :: dltamsN  
  real,    intent(IN)    :: P_inngrain1  ! // PARAMETER //INN minimal for net absorption of nitrogen during grain filling  // SD // PARPLT // 1
  real,    intent(INOUT) :: P_inngrain2  ! // PARAMETER //INN minimal for null net absorption of nitrogen during grain filling  // SD // PARPLT // 1
  real,    intent(INOUT) :: dltaremobilN
  real,    intent(INOUT) :: masecdil
  real,    intent(INOUT) :: masecpartiel
  real,    intent(INOUT) :: inns         ! // OUTPUT // Index of nitrogen stress active on growth in biomass // P_innmin to 1
  real,    intent(INOUT) :: innlai       ! // OUTPUT // Index of nitrogen stress active on leaf growth // P_innmin to 1

  real,    intent(IN)    :: dltarestemp
  integer, intent(IN)    :: P_code_rootdeposition
  real,    intent(IN)    :: P_Parazorac
  real,    intent(IN)    :: INN
  real,    intent(IN)    :: P_INNmin
  real,    intent(IN)    :: dltamsrac    ! // OUTPUT // daily gross increase in root biomass (due to new roots emission) // t.ha-1.d-1
  real,    intent(INOUT) :: abso         ! Daily N absorption rate by plant  // kg ha-1
  real,    intent(INOUT) :: dNdWcrit  
  real,    intent(INOUT) :: deltabso  
  real,    intent(INOUT) :: absodrp  
  integer, intent(IN)    :: nstopres
  integer, intent(IN)    :: nlax
  real,    intent(IN)    :: dltarestempN
  real,    intent(IN)    :: P_Parazoper
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: fstressgel

  real,    intent(INOUT) :: demande      ! // OUTPUT // Daily nitrogen need of the plant   // kgN.ha-1.j-1
  real,    intent(INOUT) :: demandeper   ! N demand of perennial organs
  real,    intent(INOUT) :: demanderac
  real,    intent(INOUT) :: QNperenne
  real,    intent(INOUT) :: QNresperenne
  real,    intent(INOUT) :: QNplantenp
  ! 08/08/2019 DR la masec total pour la vigne
  real,    intent(IN)    :: maperenne
!: Variables locales
  integer :: irecolte  
  real    :: dNdW
  real    :: CCrac
  real    :: CNrac
  real    :: INNrac
  real    :: CsurNrac_end
  real    :: CCper
  real    :: CNper
  real    :: INNper
  real    :: CsurNper
  real    :: Nremobil ! Azote remobilise en surplus par rapport aux besoins de la plante

    !DR 09/08/2019 j'ajoute un flag pour faire des ecritures de debug
  logical :: dbg_N
  dbg_N=.FALSE.


! DR 08/08/2019 oui mais pour la vig,ne on faisiat pas comme ca donc je remet un test ...
! pour les plantes autres que vigne
! La courbe de dilution critique s'applique a la biomasse aerienne
! Loic sept 2020 : je mets code_acti_reserve qui est plus g�n�ral
      if(P_code_acti_reserve.eq.1)then
         masecdil = masecnp
      else
         masecdil = maperenne + masecnp
      endif
      ! ** en cas de cueillette
      do irecolte = 1, nbrecolte-1
         masecdil = masecdil + rdtint(irecolte) / 100.
      end do
            if(dbg_N) write(4444,*)'masecdol',masecdil
      if (masecdil <=  0.) then
        inns    = 1.
        innlai  = 1.
        demande = 0.
        abso    = 0.
        return
      endif

      if(surf < 1.) then
        masecpartiel = masecpartiel + dltams
      else
        masecpartiel = masecdil
      endif
  !------------------------------------------------------------------------------------------------------
  ! 1. Courbe de dilution critique et demande en azote de la plante (suit la courbe de dilution maximale)
  !
  !  W correspond a masecdil est en t/ha et demande est en kg N/ha/jour
  !  demande = dNmax/dt = dNmax/dW * dW/dt
  !
  ! 1a. plantes en peuplement dense
  ! -------------------------------
  ! courbe de dilution critique : N% = adil W**(-bdil)            si W > P_masecNmax
  !                               N% = adil masecNmax**(-bdil)    si W < P_masecNmax
  !
  ! courbe de dilution maxi : N% = adilmax W**(-bdilmax)          si W > P_masecNmax
  !                           N% = adilmax masecNmax**(-bdilmax)  si W < P_masecNmax
  !
  !  besoin cumule en N :  N = adilmax W**(1-bdilmax)             si W > P_masecNmax
  !                        N = adilmax W masecNmax**(-bdilmax)    si W < P_masecNmax
  !
  !  besoin instantane  : dN/dt = dN/dW * dW/dt
  !
  !          avec         dN/dW = adilmax (1-bdilmax) W**(-bdilmax)  si W > P_masecNmax
  !                       dN/dW = adilmax masecNmax**(-bdilmax)      si W < P_masecNmax
  !
  !  1b. plantes isolees
  !  -------------------
  !  NB introduction d'une dilution N pour les plantes isolees de 0 a masecNmax
  !
  ! courbe de dilution critique : N% = adil W**(-bdil)               si W > masecNmax
  !                               N% = adili W**(-bdili)             si masecmeta < W < masecNmax
  !                               N% = adili masecmeta**(-bdili)     si W < masecmeta
  !
  !  courbe de dilution maxi : N% = adilmax W**(-bdil)               si W > masecNmax
  !                            N% = adilmaxi W**(-bdilmaxi)          si masecmeta < W < masecNmax
  !                            N% = adilmaxi masecmeta**(-bdilmaxi)  si W < masecmeta
  !
  !  besoin cumule en N :  N = adilmax W**(1-bdil)                   si W > masecNmax
  !                        N = adilmaxi W**(1-bdilmaxi)              si masecmeta < W < masecNmax
  !                        N = adilmaxi W masecmeta**(-bdilmaxi)     si W < masecmeta
  !
  !  besoin instantane  : dN/dt = dN/dW * dW/dt
  !
  !          avec         dN/dW = adilmax (1-bdil) W**(-bdil)           si W > masecNmax
  !                       dN/dW = adilmaxi (1-bdilmaxi) W**(-bdilmaxi)  si masecmeta < W < masecNmax
  !                       dN/dW = adilmaxi masecmeta**(-bdilmaxi)       si W < masecmeta
  !------------------------------------------------------------------------------------------------------

      if(dbg_N) write(4444,*)'masecpartiel',masecpartiel
      if (P_codeplisoleN == 2) then  ! Plante isolee
         if (masecdil <= P_masecNmax) then
            if(masecdil <= P_masecmeta) masecdil = P_masecmeta
            dNdW = adilmaxI * (1-bdilmaxI) * masecdil**(-bdilmaxI)
            dNdWcrit = adilI * (1-bdilI) * masecdil**(-bdilI)
         else
            dNdW = P_adilmax * (1-P_bdil) * masecdil**(-P_bdil)
            dNdWcrit = P_adil * (1-P_bdil) * masecdil**(-P_bdil)
         endif
      else
         if (masecdil <= P_masecNmax) then
            dNdW = P_adilmax * P_masecNmax**(-P_bdilmax)
            dNdWcrit = P_adil * P_masecNmax**(-P_bdil)
         else
            dNdW = P_adilmax * (1-P_bdilmax) * masecdil**(-P_bdilmax)
            dNdWcrit = P_adil * (1-P_bdil) * masecdil**(-P_bdil)
         endif
      endif

      !: l'absorption d'azote diminue apres drp : on applique la courbe a  MSveg + absodrp*MSgrain
      if (dltags <= 0.) then
         deltabso = dltamsN
         absodrp = 1.
      else
        ! absodrp depend du statut azote de la culture
         !if(P_inngrain1== P_inngrain2) P_inngrain2 = P_inngrain1 + 0.01
         if(abs(P_inngrain1-P_inngrain2).lt.1.0E-8) P_inngrain2 = P_inngrain1 + 0.01
         if(inns <= P_inngrain1) absodrp = 1.
         if(inns > P_inngrain1 .and. inns <= P_inngrain2) absodrp = (inns - P_inngrain2) / (P_inngrain1 - P_inngrain2)

         if(inns > P_inngrain2) absodrp=0.
         if(dltams >= dltags) then
            deltabso = (dltamsN - dltags) + (absodrp * dltags)
         else
            deltabso = absodrp * dltamsN
         endif
      endif

      demande = dNdW * deltabso * 10.
      if(dbg_N) write(4444,*)'demande en fonction de dltabso', demande
! Correction bug Loic Novembre 2017: je borne la remobilisation d'azote pour ne pas perdre de l'azote!
! cas des perennnes avec remobilisation des reserves azotees
! 09/09/2019 DR on a un pb de diff de calul de la demande du moment ou on a des remobilisations car on ne rentre plus la
! pour la vigne sans code_acti_reserve
      if (P_code_acti_reserve == 1) then
        if (dltaremobilN > demande) then
            Nremobil = dltaremobilN - demande ! azote remobilise en surplus
            dltaremobilN = demande            ! mise a jour de la quantite remobilisee
            QNperenne = QNperenne + Nremobil  ! mise a jours des pools de reserves & QNplantenp
            QNresperenne = QNresperenne + Nremobil
            QNplantenp = QNplantenp - Nremobil
        endif
      endif
! Loic sept 2020 : ce calcul est fait pour la v9 ou la v10
      demande = demande - dltaremobilN

      if(dbg_N) write(4444,*)' on a retranche dltaremobilN quand code_actireserve est activ� ', dltaremobilN, 'demande', demande
! Fin correction bug

! DR 08/08/2019 pour la vigne on avait ce test je le remets
! bon finalement ca ne fait qu'un kilo de plus � la fin ...
       if(P_code_acti_reserve.eq.2) demande = max(demande, 0.01)
            if(dbg_N) write(4444,*)'demande bornage', demande

 ! Modifs Bruno et Loic juin 2013
 ! Ajout de la demande en N des parties racinaires pour avoir la demande totale
 ! on peut annuler ce terme (et revenir a la version precedente en prenant parazorac tres eleve : ex 9999)
 ! dr 13/02/2019 introduction de la modif propos�e par loic
     !if(P_parazorac == 999)then
     ! DR 19/02/2019 substitution de parazorac en tant que code par un vrai code P_code_rootdeposition
    !  if (abs(P_parazorac - 999).lt.1.0E-8) then
     if (P_code_rootdeposition == 2) then
         CNrac = 0
     else
         INNrac = amax1(INN,P_INNmin)
         CsurNrac_end = P_Parazorac/INNrac
         CCrac = 380.
         CNrac = CCrac/CsurNrac_end            ! g N/kg MS
     endif
     demanderac = dltamsrac * CNrac
     demanderac = amax1(demanderac, 0.)
     if (masecnp == 0.) demanderac = 0. ! arret de demanderac a la recolte

 ! Modifs Loic Avril 2016: j'ajoute une demande en azote des organes perennes tant qu'il y a formation de nouvelles
 ! feuilles cad nlax == 0. et tant qu'il y a de la mise en reserve (dltarestemp > 0.)
      if (P_code_acti_reserve == 1) then
        demandeper = 0.
        if (nlax == 0 .and. dltarestemp > 0.) then
            INNper = amax1(INN, P_INNmin)
            CsurNper = P_Parazoper/INNper
            CCper = 450.
            CNper = CCper/CsurNper
            demandeper = dltarestemp * CNper
            demandeper = amax1(demandeper, 0.)
        endif
        if (masecnp == 0.) demandeper = 0. ! arret de demandeper a la recolte

! On annule la demande totale lorsqu'il y a mise en reserve d'azote vers les organes perennes ou a cause du gel (nstopres ou fstressgel)
        if (dltarestempN > 0. .or. nstopres > 0 .or. fstressgel <= 0.) then
            demande = 0.
            demanderac = 0.
            demandeper = 0.
        endif
      endif
return
end subroutine bNpl
end module bNpl_m
