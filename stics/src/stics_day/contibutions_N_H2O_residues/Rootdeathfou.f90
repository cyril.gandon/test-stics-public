! ----------------------------------------------------------------------------------------------
!  Simulates the root death and the addidition of dead roots to soil
!-----------------------------------------------------------------------------------------------
module Rootdeathfou_m
use messages
use ResiduParamDec_m, only: ResiduParamDec
implicit none
private
public :: Rootdeathfou
contains
 subroutine Rootdeathfou(logger,ihum, P_coderacine, awb, bwb, cwb, akres, bkres, ahres, bhres, CNresmin, &
                      CNresmax, Wb, kres, hres, Cres, Nres, msrac, msracmort, QCracmort, QNracmort,    &
                      msrac_veille,            &
                      QNplantenp, QNgrain, magrain, P_proprac, P_y0msrac, masecnp)
  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: ihum          ! = int(profhum)
  integer, intent(IN)    :: P_coderacine  ! // PARAMETER // Option for simulation of root growth
  real,    intent(IN)    :: awb           ! root decomposition parameter: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb           ! root decomposition parameter: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb           ! Minimum ratio C/N of microbial biomass  // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres         ! root decomposition parameter: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres         ! root decomposition parameter: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres         ! root decomposition parameter: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres         ! root decomposition parameter: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmin      ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax      ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: Cres(1:ihum)
  real,    intent(INOUT) :: Nres(1:ihum)
  real,    intent(INOUT) :: msrac         ! biomasse racinaire vivante (t.ha-1)
  real,    intent(INOUT) :: msracmort     ! dead root biomass (t.ha-1)
  real,    intent(INOUT) :: QCracmort
  real,    intent(INOUT) :: QNracmort
  real,    intent(INOUT) :: msrac_veille  ! biomasse racinaire vivante (t.ha-1)
! Ajouts Loic Avril 2019 : correction bug pour profil type et calcul de la biomasse de racines de la betterave
  real,    intent(IN)          :: QNplantenp    ! // INPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1
  real,    intent(IN)          :: QNgrain       ! // INPUT // Amount of nitrogen in harvested organs (grains / fruits) // kg ha-1
  real,    intent(IN)          :: magrain       ! // INPUT // Harvested organs dry matter //
  real,    intent(IN)          :: P_proprac     ! // PARAMETER // Slope of the relationship (root mass vs shoot mass) at harvest  // g.g-1 // PARAM // 0.20
  real,    intent(IN)          :: P_y0msrac     ! // PARAMETER // minimal amount of root mass at harvest (when aerial biomass is nil) // t.ha-1 // PARAM // 0.7
  real,    intent(IN)          :: masecnp       ! // INPUT // Aboveground dry matter  // t.ha-1

!: Variables locales
  integer :: iz
  real    :: Cracpm, NsurCrac
  real    :: msraciz, Craciz, Nraciz
  real    :: MSveget, QNveget, CNveget

        Cracpm = 380.  ! teneur en C des racines - definie aussi dans croira

! Modification du code pour retrouver les simulations de la v9 : Loic Oct 2019

! 1 : Calcul de msrac pour l'option profil type
!     *****************************************
        if (P_coderacine == 1) then
           msrac = masecnp * P_proprac + P_y0msrac
           msracmort = 0.
        else
           msrac = msrac_veille
        endif

! 2 : Calcul de msracmort et de QNracmort
!     ***********************************
        MSveget = masecnp - (magrain/100.) + msrac
        QNveget = QNplantenp - QNgrain
        if (abs(MSveget).lt.1.0E-8) then
            CNveget= 0.
        else
            CNveget = QNveget/(MSveget+msrac)
        endif

        NsurCrac = (msrac * CNveget)/(msrac * Cracpm)

        msraciz = msracmort/ihum
        Craciz = QCracmort/ihum
        Nraciz = QNracmort/ihum
        do iz = 1,ihum
           if(iz <= size(Cres)) then
             Cres(iz)= Cres(iz) + Craciz
           end if

           if(iz <= size(Nres)) then
             Nres(iz)= Nres(iz) + Nraciz
           end if
           QCracmort = QCracmort + Craciz
           QNracmort = QNracmort + Nraciz
        end do


        call ResiduParamDec(logger, awb, bwb, cwb, akres, bkres, ahres, bhres, NsurCrac, CNresmin, CNresmax, Wb, kres, hres)

   return
 end subroutine Rootdeathfou
 
end module Rootdeathfou_m
