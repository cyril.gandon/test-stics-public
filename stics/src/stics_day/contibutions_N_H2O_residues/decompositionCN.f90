! Calculation of SOM decomposition and C-N dynamics
!***************************************************
module decompositionCN_m
implicit none
private
public :: decompositionCN
contains
 subroutine decompositionCN(iz, nbR, kr, kb, yr, hr, Wb, Cres, Nres, Cbio, Nbio, dCres, dNres, dCbio, dNbio, dChum, dNhum, &
                         dCrest, dNrest, dCbiot, dNbiot, dChumrest, dNhumrest, dNmint, CsurN)

    implicit none

    integer, intent(IN) :: iz
    integer, intent(IN) :: nbR
    real,    intent(IN) :: kr(nbR)       ! // PARAMETER // decay rate of organic residue ir // d-1 /
    real,    intent(IN) :: kb(nbR)        ! // PARAMETER // decay rate of microbial biomass // d-1 /
    real,    intent(IN) :: yr(nbR)       ! // PARAMETER // C assimilation yield of residue by microbial biomass // g.g-1 /
    real,    intent(IN) :: hr(nbR)       ! // PARAMETER // humification rate associated with microbial biomass decay // d-1 /
    real,    intent(IN) :: Wb(nbR)       ! // N:C ratio of the microbial biomass //
    real,    intent(IN) :: Cres(nbR)     ! Amount of C in residue ir// kg.ha-1
    real,    intent(IN) :: Nres(nbR)     ! Amount of N in residue ir// kg.ha-1
    real,    intent(IN) :: Cbio(nbR)     ! Amount of C in zymogeneous (newly formed) biomass // kg.ha-1
    real,    intent(IN) :: Nbio(nbR)     ! Amount of N in zymogeneous (newly formed) biomass // kg.ha-1
    real, intent(INOUT) :: dCres(nbR)    ! daily variation of C in residue // kg.ha-1.d-1
    real, intent(INOUT) :: dNres(nbR)    ! daily variation of N in residue // kg.ha-1.d-1
    real, intent(INOUT) :: dCbio(nbR)    ! daily variation of C in newly formed biomass // kg.ha-1.d-1
    real, intent(INOUT) :: dNbio(nbR)    ! daily variation of N in newly formed biomass // kg.ha-1.d-1
    real, intent(INOUT) :: dChum(nbR)    ! daily variation of C in newly formed humus // kg.ha-1.d-1
    real, intent(INOUT) :: dNhum(nbR)    ! daily variation of N in newly formed humus // kg.ha-1.d-1
    real, intent(INOUT) :: dCrest
    real, intent(INOUT) :: dNrest
    real, intent(INOUT) :: dCbiot
    real, intent(INOUT) :: dNbiot
    real, intent(INOUT) :: dChumrest
    real, intent(INOUT) :: dNhumrest
    real, intent(INOUT) :: dNmint        ! net variation in mineral N (N release if positive, N immobilisation if negative) // kg.ha-1.d-1
    real, intent(IN)    :: CsurN

    integer :: ir

        dCrest = 0.
        dNrest = 0.
        dCbiot = 0.
        dNbiot = 0.
        dChumrest = 0.
        dNhumrest = 0.
        dNmint = 0.

        do ir = 1,nbR
! on ne calcule pas Cres(iz,ir) lorsque ir <= nbR et iz>1  car le mulch est seulement dans la couche 1
           dCres(ir) = 0.
           dNres(ir) = 0.
           dCbio(ir) = 0.
           dNbio(ir) = 0.
           dChum(ir) = 0.
           if (ir > 10 .or. iz == 1) then
               if (Cres(ir) + Cbio(ir) <= 1.e-10) CYCLE

! ** Variation des stocks C et N des pools residu, biomasse, humus & Nmineral
               dCres(ir) = -kr(ir) * Cres(ir)
               if (Cres(ir) + dCres(ir) < 0.) dCres(ir) = -Cres(ir)
               dNres(ir) = -kr(ir) * Nres(ir)
               if (Nres(ir) + dNres(ir) < 0.) dNres(ir) = -Nres(ir)
               dCbio(ir) = -kb(ir) * Cbio(ir) - yr(ir) * dCres(ir)
               if (Cbio(ir) + dCbio(ir) < 0.) dCbio(ir) = -Cbio(ir)
               dNbio(ir) = -kb(ir) * Nbio(ir) - yr(ir) * Wb(ir) * dCres(ir)
               if (Nbio(ir) + dNbio(ir) < 0.) dNbio(ir) = -Nbio(ir)
               dChum(ir) = kb(ir) * hr(ir) * Cbio(ir)
               dNhum(ir) = dChum(ir) / CsurN

! * Cumul des variations de C et N pour tous les residus
               dCrest = dCrest + dCres(ir)
               dNrest = dNrest + dNres(ir)
               dCbiot = dCbiot + dCbio(ir)
               dNbiot = dNbiot + dNbio(ir)
               dChumrest = dChumrest + dChum(ir)
               dNhumrest = dNhumrest + dNhum(ir)
               dNmint = dNmint -(dNres(ir) + dNbio(ir) + dNhum(ir))
 !           if(ir < 11) CO2mul = - (dCres(ir) + dCbio(ir) + dChumres(ir)) ! calcul de la mineralisation du mulch (une fraction du QCO2res)
           endif
        end do        ! fin de boucle sur le type de residus
  return
end subroutine decompositionCN
end module decompositionCN_m
 
 
