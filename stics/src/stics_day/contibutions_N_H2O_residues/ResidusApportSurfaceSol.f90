
! **********************************************************************
! * Apport de Matiere Organique a la surface du sol                    *
! **********************************************************************
! -------------------- variables d'entree -------------------------
!
!   ires       type de residu apporte (1 a nbResidus)
!   qresidu    quantite de MF de residu apporte              (t/ha)
!   Crespc    teneur en C du residu                         (% MF)
!   CNresid   rapport C/N du residu apporte                  (g/g)
!   eauresidu  teneur en eau du residu organique             (% MF)
!   P_qmulchdec  quantite maximale de mulch decomposable      (t/ha)
! --------------------- variables de sortie -----------------------
!
!   Cres      stock C du residu ires                        (kg/ha)
!   Nres      stock N du residu ires                        (kg/ha)
!   Cnondec     stock C non decomposable                      (kg/ha)
!   Nnondec     stock N non decomposable                      (kg/ha)
! ------------------------------------------------------------------
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 6.3.3, page 104
!
!! This module updates the C and N stock (as well as water) at surface when applying organic residues
!! Plant residues are assumed to remain at the soil surface until being buried by the following soil tillage, forming a mulch
!! Roots which are assumed to be located between the surface and the PROFHUMS depth.
!! Those residues may have mulch properties.
!!
!! The N inputs from organic residues arrive onto the soil either under mineral form (mainly as NH4+) or under organic form. The mineral fraction enters
!! the soil mineral pool and is submitted to NH3 volatilization, nitrification, leaching and plant uptake. The organic fraction decomposes more or less rapidly
!! and mineralizes its C and N according to the decomposition module (see mineral.f90). The module is generic and can simulate most types of organic residues.
!! 10 categories are considered:
! -  1) mature crop residues (straw, roots),
! -  2) catch crop residues (young plants),
! -  3) farmyard manures,
! -  4) composts,
! -  5) sewage sludges,
! -  6) distillery vinasses,
! -  7) animal horn,
! -  8,9,10) others.
!!
!! The characteristics of each organic residue are defined in the technical file: category, depth of incorporation in soil, amount of fresh matter added,
!! carbon content, C/N ratio, water content and mineral N content
!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
module ResidusApportSurfaceSol_m
use iso_fortran_env, only: real32
implicit none
private
public :: ResidusApportSurfaceSol
contains
subroutine ResidusApportSurfaceSol(qresidu, Crespc, eauresidu, CNresmin, CNresmax, Qmulchdec, CNresid, &
                                    Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo, &
                                    Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)

  real(real32),    intent(IN)    :: qresidu    ! Fresh matter (FM) added from residue ires  t.ha-1
  real(real32),    intent(IN)    :: Crespc     ! C content of residue ires     // %FM
  real(real32),    intent(IN)    :: eauresidu  ! Water content of residue ires // %FM
  real(real32),    intent(IN)    :: CNresmin   ! minimum observed value of ratio C/N of residue ires // g g-1
  real(real32),    intent(IN)    :: CNresmax   ! maximum observed value of ratio C/N of residue ires // g g-1
  real(real32),    intent(IN)    :: Qmulchdec  ! maximal amount of decomposing mulch of residue ires // t.ha-1
  real(real32),    intent(INOUT) :: CNresid    ! C:N ratio of the added residue
  real(real32),    intent(INOUT) :: Cnondec    ! undecomposable C stock of the residue ires at soil surface //  t.ha-1
  real(real32),    intent(INOUT) :: Nnondec    ! undecomposable N stock of the residue ires at soil surface // kg.ha-1
  real(real32),    intent(INOUT) :: Cres1      ! Amount of C in pool Cres at depth iz=1 and residue ires
  real(real32),    intent(INOUT) :: Nres1      ! Amount of N in pool Cres at depth iz=1 and residue ires
  real(real32),    intent(INOUT) :: QCapp      ! cumulative amount of organic C added to soil // kg.ha-1
  real(real32),    intent(INOUT) :: QNapp      ! cumulative amount of organic N added to soil // kg.ha-1
  real(real32),    intent(INOUT) :: QCresorg   ! cumulative amount of exogenous C added to soil // kg.ha-1
  real(real32),    intent(INOUT) :: QNresorg   ! cumulative amount of exogenous N added to soil // kg.ha-1
  real(real32),    intent(INOUT) :: CroCo      ! decomposition parameter of residue ires
  real(real32),    intent(INOUT) :: Chum1      ! Amount of C in active humus pool at depth iz=1 // kg.ha-1
  real(real32),    intent(INOUT) :: Nhum1      ! Amount of N in active humus pool at depth iz=1 // kg.ha-1
  real(real32),    intent(INOUT) :: Chumi      ! Amount of C in inert humus pool // kg.ha-1
  real(real32),    intent(INOUT) :: Nhumi      ! Amount of N in inert humus pool // kg.ha-1
  real(real32),    intent(INOUT) :: CsurNsol
  real(real32),    intent(INOUT) :: Chuma
  real(real32),    intent(INOUT) :: Nhuma

!: VARIABLES LOCALES
  real    :: Capp, Napp, NCresmoy, Csup, Nsup, Cmulchdecmax
  real    :: fdec , fine, fact, Cappdec, Nappdec, Cappact, Nappact, Cappine, Nappine
  real    :: aws, bws, Ws, Wh, CresMS

! Apport de C et N par les residus organiques (kg/ha)
! ---------------------------------------------------
!    Recalcul eventuel du rapport C/N du residu
!      1) Si CNresid = 0 on prend la moyenne des valeurs de calibration
        !if(CNresid == 0.) then
        if(abs(CNresid).lt.1.0E-8) then
          NCresmoy  =  1. / abs(CNresmin) + 1. / abs(CNresmax)
          CNresid  =  2. / NCresmoy
!        else
!      2) Si CNresid est en dehors des limites de calibration, on borne
!          CNresid = min(CNresid,CNresmax)
!          CNresid = max(CNresid,CNresmin)
        endif
! Calcul du rapport N/C de la fraction stable du residu organique (en utilisant les decimales de CNresmin et CNresmax)
! 31/08/2017 astuce provisoire de Bruno pour simuler les PROs
        aws = CNresmin - int(CNresmin)
        if(CNresmin < 0.) aws = CNresmin - int(CNresmin + 1.)
        bws = (CNresmax - int(CNresmax)) * 100.
        Ws =  aws + bws / CNresid
        Wh = 1. / CsurNsol

! Modif Bruno 2017 la teneur en C est exprimee par rapport au produit brut (% MF)
! DR 20/02/2020 apres discussion avec Fabien on laisse les memes unites que dans le trunk initilement
! On reste en %MS pour primer l�existant plut�t que la coh�rence (Nminres en %MF)
        Capp = qresidu * Crespc * 10. * (1. - eauresidu/100.)
!        Capp = qresidu * Crespc * 10.
        Napp = Capp / CNresid
!
!   Cumul des apports C et N provenant des apports de MO exogenes (supposees avoir une teneur en eau > 0)
       if(eauresidu >= 0.) then
           QCresorg = QCresorg + Capp
           QNresorg = QNresorg + Napp
        endif
!    Cumul des apports totaux de C et N organiques au sol
        if (Capp <= 0.) return
        QCapp = QCapp + Capp
        QNapp = QNapp + Napp

! 31/08/2017 astuce provisoire de Bruno pour simuler les PROs
! les apports de PRO se repartissent entre fractions decomposable (fdec), C actif (fact) et C stable (fine)
!         astuce code CroCo   exemple 0.6125  signifie Cr0/co = 0.61 et Cso/Co = 0.25
! DR 27/05/2020 on force croco a 1.0 en attendant  de pouvoir traiter la prise en compte des PROs sur une branche
! donc pour le moment tout passe en decomposable
!++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            if(CroCo.eq.0)Croco=1.0
!
            fdec = int(100.*CroCo) / 100.
            fine = 100.*(CroCo-fdec)
            fact = 1. - fdec - fine
            Cappdec = Capp * fdec
            Cappact = Capp * fact
            Cappine = Capp * fine
            Nappact = Cappact * Wh
            Nappine = Cappine * Ws
            ! test de coherence
            Nappdec = Napp - Nappact - Nappine
            if(Nappdec < 0.) then
               Nappdec = 0.
               Nappine = Napp - Nappact
            endif


            CresMS = Crespc / (1. - eauresidu/100.)  ! teneur en C par rapport au produit sec
            Cmulchdecmax = Qmulchdec * CresMS * 10.  ! maximal amount of decomposable C of residue ires

! modif Bruno aout 2017 : l'apport de MO se fait par dessus le mulch existant
            Cnondec = Cnondec + Cappdec
            Nnondec = Nnondec + Nappdec
            Csup = amin1(Cmulchdecmax - Cres1, Cnondec)
            if(Csup > 0.) then
               Cnondec = Cnondec - Csup
               Cres1 = Cres1 + Csup
               Nsup = Csup * Nappdec / Cappdec
               Nnondec = Nnondec - Nsup
               Nres1  = Nres1 + Nsup
            endif

            Chum1 = Chum1 + Cappact
            Nhum1 = Nhum1 + Nappact
            Chumi = Chumi + Cappine
            Nhumi = Nhumi + Nappine
            Chuma = Chuma + Cappact
            Nhuma = Nhuma + Nappact

 ! Ajouter mise a jour du mulch ? fait dans mineral
 !        write(6655,*)chum1,Nhum1
  return
end subroutine ResidusApportSurfaceSol
end module ResidusApportSurfaceSol_m
 