module H2OMixedSoil_m
use iso_fortran_env, only: real32
implicit none
private
public :: H2OMixedSoil
contains
subroutine H2OMixedSoil( itrav, ihum, tmix, &  ! IN
                          hur, sat)               ! INOUT

! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!  Mixing of soil H2O after soil tillage
!  Each tillage operation is assumed to mix water and SMN within the tilled layer defined by
!     the surface and the deeper depth (proftrav)
!  The mixing intensity (tmix) varies from 0 (no mixing) to 1 (complete mixing)
!  It is (temporarily) forced to 0.0
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c

implicit none

  integer, intent(IN)    :: itrav      ! Depth of soil tillage  // cm  !cote inferieure de l'operation de travail j   (cm)
  integer, intent(IN)    :: ihum       ! Humification depth  (max.60 cm) // cm
  real(real32),    intent(IN)    :: tmix       ! mixed rate  (0 = no mixed; 1 = total mixed)
  real(real32),    intent(INOUT) :: hur(ihum)  ! water content in the microporosity // mm
  real(real32),    intent(INOUT) :: sat(ihum)  ! water content in the macroporosity // mm

! local VARIABLES
  integer :: iz      ! layer of 1 cm
  real    :: tot_hur ! water content in the microporosity cumulated on the proftrav layer // mm
  real    :: tot_sat ! water content in the macroporosity cumulated on the proftrav layer // mm


! Calcul des stocks d'eau sur la couche de melange [1,itrav2]
      tot_hur    = 0.
      tot_hur = SUM(hur(1:itrav))
      tot_hur = tot_hur/itrav
! DR 24/04/2018 apres discussion avce Marie je laisse l'homogeneisation de l'eau dans la macroporosite et je demande confirmation a Bruno quand il rentre
      tot_sat    = 0.
      tot_sat = SUM(sat(1:itrav))
      tot_sat = tot_sat/itrav

! Repartition de l'eau dans la couche de melange
      do iz = 1,itrav
          hur(iz) = (1.-tmix)*hur(iz) + tmix* tot_hur
          sat(iz) = (1.-tmix)*hur(iz) + tmix* tot_sat
      end do

return

end subroutine H2OMixedSoil
end module H2OMixedSoil_m
