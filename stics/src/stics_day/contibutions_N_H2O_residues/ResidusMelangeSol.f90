! *---------------------------------------------------------------------------------------------------
! - Stics book paragraphe 8.2, page 147
!  Mixing soil residues due to tillage
!  Each tillage operation is assumed to mix the residues uniformly within the tilled layer defined by
!     an upper depth (itrav1) and a deeper depth (itrav2).
! *---------------------------------------------------------------------------------------------------
module ResidusMelangeSol_m
implicit none
private
public :: ResidusMelangeSol
contains
subroutine ResidusMelangeSol(itrav1, itrav2, nbResidus, ihum, Cres, Nres, Cbio, Nbio, Chum, Nhum, &
                              Cnondec, Nnondec, Cmulchnd, Nmulchnd)
                              ! PL, 12/04/2022: option inutile maintenant
                              !,P_code_depth_mixed_humus)

  integer, intent(IN)    :: itrav1                  ! cote superieure de l'operation de travail j     (cm)
  integer, intent(IN)    :: itrav2                  ! cote inferieure de l'operation de travail j     (cm)
  integer, intent(IN)    :: nbResidus               ! Total number of residues
  integer, intent(IN)    :: ihum
  real,    intent(INOUT) :: Cres(ihum,nbResidus)  ! stock C du pool residu j de la couche i       (kg/ha)
  real,    intent(INOUT) :: Nres(ihum,nbResidus)  ! stock N du pool residu j de la couche i       (kg/ha)
  real,    intent(INOUT) :: Cbio(ihum,nbResidus)  ! stock C du pool biomasse j de la couche i     (kg/ha)
  real,    intent(INOUT) :: Nbio(ihum,nbResidus)  ! stock N du pool biomasse j de la couche i     (kg/ha)
  real,    intent(INOUT) :: Chum(ihum)            ! stock C du pool humus actif de la couche i    (kg/ha)
  real,    intent(INOUT) :: Nhum(ihum)            ! stock N du pool humus actif de la couche i    (kg/ha)
  real,    intent(INOUT) :: Cnondec(nbResidus)      ! undecomposable C stock of residue i on the surface // kg.ha-1
  real,    intent(INOUT) :: Nnondec(nbResidus)      ! undecomposable N stock of residue i on the surface // kg.ha-1
  real,    intent(INOUT) :: Cmulchnd
  real,    intent(INOUT) :: Nmulchnd
  ! PL, 12/04/2022: option inutile maintenant
  !integer, intent(IN)    :: P_code_depth_mixed_humus ! option to mixed the humus on the depth itrav1-itrav2 (yes=1) or on the depth 1-itrav2 (no=2) // 1,2

! VARIABLES LOCALES
  integer :: iz
  integer :: ir  
  integer :: itrav  
  real    :: Cresid(nbResidus)  
  real    :: Nresid(nbResidus)  
  real    :: Cbioma(nbResidus)  
  real    :: Nbioma(nbResidus)  
  real    :: Chumus
  real    :: Nhumus

! On sort du programme si la profondeur de travail du sol est <= 1 cm
      if(itrav2 <= 1) return
! Calcul des pools residu, biomasse et humus sur la couche de melange [itrav1,itrav2]
      Cresid(:) = 0.
      Nresid(:) = 0.
      Cbioma(:) = 0.
      Nbioma(:) = 0.
      Chumus    = 0.
      Nhumus    = 0.
      do ir = 11,nbResidus
          do iz = itrav1,itrav2
             Cresid(ir) = Cresid(ir) + Cres(iz,ir)
             Nresid(ir) = Nresid(ir) + Nres(iz,ir)
             Cbioma(ir) = Cbioma(ir) + Cbio(iz,ir)
             Nbioma(ir) = Nbioma(ir) + Nbio(iz,ir)
          end do
      end do
 ! Modif Bruno avril 2018 : calcul sur l'ensemble de la zone travaillee
 !     Chumus = SUM(Chum(1:itrav2))/itrav2
 !     Nhumus = SUM(Nhum(1:itrav2))/itrav2

! Ajout des residus de surface a la couche de melange (sauf racines) et remise a 0 du mulch de surface
      do ir = 11,nbResidus-1
            Cresid(ir) = Cresid(ir)+ Cres(1,ir-10) + Cnondec(ir-10) ! Amount of mulch-C of residue ir
            Cres(1,ir-10)= 0.
            Cnondec(ir-10) = 0.
            Cbioma(ir) = Cbioma(ir)+ Cbio(1,ir-10)
            Cbio(1,ir-10)= 0.
            Nresid(ir) = Nresid(ir)+ Nres(1,ir-10) + Nnondec(ir-10)
            Nres(1,ir-10)= 0.
            Nnondec(ir-10) = 0.
            Nbioma(ir) = Nbioma(ir)+ Nbio(1,ir-10)
            Nbio(1,ir-10)= 0.
      end do
      Cmulchnd = 0.
      Nmulchnd = 0.

! Repartition des residus (sauf racines), biomasse & humus dans la couche de melange
      itrav = itrav2 - itrav1 + 1
      do iz = itrav1,itrav2
          do ir = 11,nbResidus-1
              Cres(iz,ir) = Cresid(ir) / itrav
              Cbio(iz,ir) = Cbioma(ir) / itrav
              Nres(iz,ir) = Nresid(ir) / itrav
              Nbio(iz,ir) = Nbioma(ir) / itrav
          end do
! Modif Bruno : calcul sur l'ensemble de la zone travaillee
!          Chum(iz) = Chumus / itrav
!          Nhum(iz) = Nhumus / itrav
      end do
      
      ! DR 11/06/2019 on conditionne la profonseur de calcul du N et C de l'humus sur une code 
      ! ancienne formule sur itrav1:itrav2
      ! nouvelle formule sur toute la couche 1:itrav2
     ! write(6655,*)Chum
     ! write(6655,*)Nhum

      ! PL, 12/04/2022: option inutile maintenant
      !if(P_code_depth_mixed_humus.eq.2)then
! Modif Bruno avril 2018 : calcul sur l'ensemble de la zone travaillee
          Chumus = SUM(Chum(1:itrav2))/itrav2
          Nhumus = SUM(Nhum(1:itrav2))/itrav2
          do iz = 1,itrav2
              Chum(iz) = Chumus
              Nhum(iz) = Nhumus
            !  write(6655,*)iz,chum(iz),Nhum(iz)
          end do
 
 !     else
 ! DR 11/06/2019 ancienne formule de calcul de itrav1 � itrav2
 !     ! Repartition de l'humus dans la couche de melange
 !         Chumus = SUM(Chum(itrav1:itrav2))/itrav
 !         Nhumus = SUM(Nhum(itrav1:itrav2))/itrav
 !         do iz = itrav1,itrav2
 !             Chum(iz) = Chumus
 !             Nhum(iz) = Nhumus
 !          !   write(6655,*)iz,chum(iz),Nhum(iz)
 !         end do
 !      endif



 ! Ajouter mise a jour du mulch ?

return
end subroutine ResidusMelangeSol
end module ResidusMelangeSol_m
 
 
