module H2OandNmixedSoil_m
use iso_fortran_env, only: real32
use NMixedSoil_m, only: NMixedSoil
use H2OMixedSoil_m, only: H2OMixedSoil
implicit none
private
public :: H2OandNmixedSoil
contains
subroutine H2OandNmixedSoil(proftrav, ihum, tmix, hur, sat, amm, nit)
!call H2OandNmixedSoil(nint(P_proftrav(is)), nbCouches , P_tmix, hur, sat, amm, nit)
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!  Mixing of soil  mineral N and water in the soil after soil tillage
!  Each tillage operation is assumed to mix water and SMN within the tilled layer defined by
!     the surface and the deeper depth (proftrav)
!  The mixing intensity (tmix) varies from 0 (no mixing) to 1 (complete mixing)
!  It is (temporarily) forced to 0.0
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c



  real(real32),    intent(IN)    :: proftrav
  integer, intent(IN)    :: ihum
  real(real32),    intent(IN)    :: tmix    ! taux de melange  (0 = pas de melange; 1 = melange complet)

  real(real32),    intent(INOUT) :: amm(ihum)  ! stock NH4 du pool biomasse j de la couche i     (kg/ha)
  real(real32),    intent(INOUT) :: nit(ihum)  ! stock NO3 du pool biomasse j de la couche i     (kg/ha)
  real(real32),    intent(INOUT) :: hur(ihum)  ! water content in the layer of 1 cm in the microporosity // mm
  real(real32),    intent(INOUT) :: sat(ihum)  ! water content in the layer of 1 cm in the macroporosity // mm
  integer :: itrav

! DR 05/02/2018 on met la le melange d'azote et d'eau sur la profondeur de travail du sol
! Bruno juin 2017 melange de eau et N mineral dans la couche qui est travaillee
!           call EauNminMelangeSol(proftrav(is), ihum, hur, amm, nit)
! On ne fait rien si la profondeur de travail maxi est <= 1 cm

  itrav=int(proftrav)
  !if( itrav <= 1 .or. tmix == 0.) then
  if( itrav <= 1 .or. tmix.lt.1.0E-8) then
    return
  end if

  call NMixedSoil( itrav, ihum, tmix, nit, amm)
  call H2OMixedSoil( itrav, ihum, tmix, hur, sat)

end subroutine H2OandNmixedSoil
end module H2OandNmixedSoil_m