! *---------------------------------------------------------------------------------------------------------------------------------------------------------
! Addition of leaves falling from the plant onto the soil surface
! - Stics book paragraphe 6.3.3, page 105
!
!! Leaves falling onto the soil (the proportion of senescent leaves falling is ABSCISSIONP) during crop growth are taken into account by the model
!! as this phenomenon can be important (e.g. rapeseed due to winter frost). Their decomposition at the soil surface is simulated by the decomposition module
!! (see ResidusDecomposition.f90 and mineral.f90) as residues of young plants (category 2). The C/N ratio of leaves when they fall off is calculated from
!! the nitrogen nutrition index of the whole crop and relies on a plant parameter (PARAZOFMORTE), as proposed by Dorsainvil (2002).
! *---------------------------------------------------------------------------------------------------------------------------------------------------------
module ApportFeuillesMortes_m
use messages
use ResidusApportSurfaceSol_m
use ResiduParamDec_m, only: ResiduParamDec
use plant_utils
implicit none
private
public :: ApportFeuillesMortes
contains
subroutine ApportFeuillesMortes(logger,P_codeplante, P_codemonocot, P_codedyntalle, P_code_acti_reserve, &
                               P_abscission, P_parazofmorte, fstressgel, inn, mortmasec, surf, CNresmin, &
                               CNresmax, Qmulchdec, QNplantenp, dltamsen, dltamstombe, mafeuiltombe, QCplantetombe, &
                               QNplantetombe, QCapp, QNapp, QCresorg, QNresorg, Cres1, Nres1, Cnondec, Nnondec,  &
                               awb, bwb, cwb, CroCo, akres, bkres, ahres, bhres, Wb, kres, hres, msneojaune, mafeuiljaune, &
                               lai, laisen, masecnp, QNvegstruc, mafeuiltombefauche, msresgel, QNresgel, matigestruc, &
                               QNfeuille, QNtige, mafeuil, Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
  type(logger_), intent(in) :: logger
  character(len=3), intent(IN) :: P_codeplante
  integer, intent(IN)    :: P_codemonocot
  integer, intent(IN)    :: P_codedyntalle ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: P_abscission   ! // PARAMETER // proportion of senescent leaves falling onto the soil // SD // PARPLT // 1
  real,    intent(IN)    :: P_parazofmorte ! // PARAMETER // parameter of proportionality between the C/N of dead leaves and the INN // SD // PARPLT // 1
  real,    intent(IN)    :: fstressgel     ! // OUTPUT // Frost index on the LAI // 0-1
  real,    intent(IN)    :: inn            ! // OUTPUT // Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
  real,    intent(IN)    :: mortmasec      ! // OUTPUT // Dead tiller biomass  // t.ha-1
  real,    intent(IN)    :: surf           ! // OUTPUT // Fraction of surface in the shade // 0-1
  real,    intent(IN)    :: CNresmin       ! // PARAMETER // minimum observed value of ratio C/N of organic residue ires  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax       ! // PARAMETER // maximum observed value of ratio C/N of organic residue ires // g g-1 // PARAM // 1

  real,    intent(IN)    :: Qmulchdec      ! // PARAMETER // maximal amount of decomposing mulch of residue ires// t C.ha-1 // PARAM // 1
  real,    intent(INOUT) :: QNplantenp     ! // OUTPUT // Amount of N taken up by the whole plant // kg.ha-1
  real,    intent(INOUT) :: dltamsen       ! // OUTPUT // Senescence rate // t ha-1 d-1
  real,    intent(INOUT) :: dltamstombe  
  real,    intent(INOUT) :: mafeuiltombe   ! // OUTPUT // Dry matter of fallen leaves // t.ha-1
  real,    intent(INOUT) :: QCplantetombe  ! // OUTPUT // cumulative amount of C in fallen leaves // kg.ha-1
  real,    intent(INOUT) :: QNplantetombe
  real,    intent(INOUT) :: QCapp      ! // OUTPUT // cumulative amount of organic C added to soil // kg.ha-1
  real,    intent(INOUT) :: QNapp      ! // OUTPUT // cumulative amount of organic N added to soil // kg.ha-1
  real,    intent(INOUT) :: QCresorg
  real,    intent(INOUT) :: QNresorg
  real,    intent(INOUT) :: Cres1      ! premiere couche, itrav1 = 1 & itrav2 = 1
  real,    intent(INOUT) :: Nres1      ! premiere couche, itrav1 = 1 & itrav2 = 1
  real,    intent(INOUT) :: Cnondec    ! // OUTPUT // undecomposable C stock of the type 10 residues on the surface // t.ha-1
  real,    intent(INOUT) :: Nnondec    ! // OUTPUT // undecomposable N stock of the type 10 residues on the surface // kg.ha-1

  real,    intent(IN)    :: awb
  real,    intent(IN)    :: bwb
  real,    intent(IN)    :: cwb
  real,    intent(IN)    :: akres
  real,    intent(IN)    :: bkres
  real,    intent(IN)    :: ahres
  real,    intent(IN)    :: bhres
  real,    intent(INOUT)    :: Croco
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres

  real,    intent(INOUT) :: msneojaune
  real,    intent(INOUT) :: mafeuiljaune
  real,    intent(IN)    :: lai
  real,    intent(IN)    :: laisen
  real,    intent(INOUT) :: masecnp
  real,    intent(INOUT) :: QNvegstruc
  real,    intent(INOUT) :: mafeuiltombefauche
  real,    intent(IN)    :: msresgel ! Strutural biomasse of stems after cutting of forages
  real,    intent(IN)    :: QNresgel ! Strutural N content after cutting of forages
  real,    intent(INOUT) :: matigestruc
  real,    intent(INOUT) :: QNfeuille
  real,    intent(INOUT) :: QNtige
  real,    intent(IN)    :: mafeuil
  real,    intent(INOUT) :: Chum1
  real,    intent(INOUT) :: Nhum1
  real,    intent(INOUT) :: Chumi
  real,    intent(INOUT) :: Nhumi
  real,    intent(INOUT) :: CsurNsol
  real,    intent(INOUT) :: Chuma
  real,    intent(INOUT) :: Nhuma

  ! Variables locales
  real :: Cfeupc       ! C content of the residue falling on soil (dead leaves or whole plant if frozen) (%)
  real :: CsurNresid   ! C/N ratio of the residue falling on soil (dead leaves or whole plant if frozen)
  real :: Wr           ! N/C ratio of the residue falling on soil (dead leaves or whole plant if frozen)
  real :: Cplantetombe ! Amount of C in residue falling on soil on day n
  real :: dltaNtombe   ! Amount of N in residue falling on soil on day n

  integer :: ficdbg = 800
  logical :: dbg_feuilmort
  dbg_feuilmort = .FALSE.
  !if(n.ge.446)dbg_shutwall = .TRUE.

! 15/10 DR, LS, PL remise a la bonne valeur de la v9.0 et de la v10.0
  Cfeupc = 42.

  !  Residus morts tombant de la plante a la surface du sol :
  ! *-     soit toute la plante si elle est entierement gelee (fstressgel = 0)
  ! *-     soit la partie des feuilles senescentes qui tombent (P_abscission)
  !     These J.F.Dejoux    (p.75) : CsurNresid = -48.4*inn + 86
  !     These F. Dorsainvil (p.66) : CsurNresid =  13.3/inn    : on prend cette equation

! Modifs Bruno et Loic fevrier 2013

! Loic : on teste sur le code d'activation des reserves (code_acti_reserve) plutot que maperenne
     if(P_code_acti_reserve == 1 .and. P_codemonocot == 1) then
          if(lai == 0. .and. laisen > 0. .and. mafeuiltombe == 0.) then
              dltamstombe = mafeuiljaune * P_abscission
          else
              dltamstombe = 0.
          endif
     else
           dltamstombe = dltamsen * P_abscission
     endif

! Calcul de dltamstombe
! Modif Loic septembre 2016 & Juin 2018 : Lorsqu'il y a un gel lethal, toute la plante meurt --> On appelle la routine plant_death (voir senesecen.f90).
! Pour les plantes a reserves seule la partie aerienne meurt

     if (P_codedyntalle == 1) dltamstombe = dltamstombe + mortmasec * surf
! Loic : on teste sur le code d'activation des reserves (code_acti_reserve) plutot que maperenne
     if (P_code_acti_reserve == 2) then
! modif Bruno a discuter !
!        CsurNresid = P_parazofmorte / inn
! 31/08/2017 TODO  NB : le calcul suivant est a revoir
         if (fstressgel > 0.) then
         CsurNresid = P_parazofmorte / inn
         if(dbg_feuilmort)write(ficdbg,*)'CNresid P_parazofmorte',CsurNresid
         endif
     else
        if (QNfeuille == 0.) QNfeuille = 1.e-8
        CsurNresid = Cfeupc * 10. * mafeuil / QNfeuille
        if(dbg_feuilmort)write(ficdbg,*)'CNresid Cfeupc',CsurNresid
     endif
! Actualisation des pools
     mafeuiltombe = mafeuiltombe + dltamstombe
     mafeuiljaune = mafeuiljaune + dltamsen - dltamstombe
     msneojaune = mafeuiljaune
     masecnp = masecnp - dltamstombe
     mafeuiltombefauche = mafeuiltombefauche + dltamstombe
! Fin modifs

     if (dltamstombe > 0.and.fstressgel > 0) then
!       if(dbg_feuilmort)write(ficdbg,*)'dans apport_feuille_morte CResid',Cres1,Nres1,'dltamstombe',dltamstombe,'CsurNresid',CsurNresid
if(dbg_feuilmort)write(ficdbg,*)'dltamstombe a CsurNresid',dltamstombe, Cfeupc, -1.e-10, CNresmin, CNresmax, Qmulchdec, CsurNresid
if(dbg_feuilmort)write(ficdbg,*)'cnondec a Croco',Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo
if(dbg_feuilmort)write(ficdbg,*)'Chum1 a Nhuma',Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma

        call ResidusApportSurfaceSol(dltamstombe, Cfeupc, -1.e-10, CNresmin, CNresmax, Qmulchdec, CsurNresid, &
                                      Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo, &
                                      Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
if(dbg_feuilmort)write(ficdbg,*)'dltamstombe a CsurNresid',dltamstombe, Cfeupc, -1.e-10, CNresmin, CNresmax, Qmulchdec, CsurNresid
if(dbg_feuilmort)write(ficdbg,*)'cnondec a Croco',Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo
if(dbg_feuilmort)write(ficdbg,*)'Chum1 a Nhuma',Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma
       ! write(711,*)'apres ResidusApportSurfaceSol Cres ', Cres1
       ! write(711,*)'apres ResidusApportSurfaceSol Nres ', Nres1
!       if(dbg_feuilmort)write(ficdbg,*)'apres ResidusApportSurfaceSol', CsurNresid

        Wr = 1./CsurNresid
        call ResiduParamDec(logger, awb,bwb,cwb,akres,bkres,ahres,bhres,Wr,CNresmin,CNresmax,Wb,kres,hres)

        Cplantetombe = dltamstombe * Cfeupc * 10.
        ! Loic Juin 2018 : On ne calcule dltaNtombe que quand il n'a pas encore ete calcule
        dltaNtombe = Cplantetombe * Wr
        QCplantetombe = QCplantetombe + Cplantetombe
        QNplantetombe = QNplantetombe +  dltaNtombe
        QNplantenp = QNplantenp - dltaNtombe
      ! Ajout Loic Mai 2016: ce qui tombe est du structural, mise a jour des pools pour les plantes avec reserves
        if (P_code_acti_reserve == 1) then
          QNvegstruc = QNvegstruc - dltaNtombe
          QNfeuille = QNfeuille - dltaNtombe
        endif
      endif

! Ajout Loic Septembre 2016 : Restitution de la partie aerienne quand il y a un gel lethal
    if (P_code_acti_reserve == 1) then
    if (fstressgel <= 0.and.QNplantenp > 0.) then
      if (P_codeplante == CODE_FODDER .and. QNplantenp /= QNresgel) then
           dltamstombe = masecnp
           dltaNtombe = QNplantenp
           if (P_codeplante == CODE_FODDER) then ! On garde la souche pour les fourrages = structure apres la fauche
              dltamstombe = masecnp - msresgel
              dltaNtombe = QNplantenp - QNresgel
           endif
           Cplantetombe = dltamstombe * Cfeupc * 10.
           QCplantetombe = QCplantetombe + Cplantetombe
           QNplantetombe = QNplantetombe +  dltaNtombe
           CsurNresid = 999.
           if (dltaNtombe > 1.e-8) CsurNresid = Cplantetombe / dltaNtombe

           ! write(711,*)'dans apport_feuille_morte',Cres1
           call ResidusApportSurfaceSol(dltamstombe, Cfeupc, -1.e-10, CNresmin, CNresmax, Qmulchdec, CsurNresid, &
                               Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, CroCo, &
                               Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
           Wr = 0.
           if (CsurNresid > 0.) Wr = 1./CsurNresid
           call ResiduParamDec(logger, awb,bwb,cwb,akres,bkres,ahres,bhres,Wr,CNresmin,CNresmax,Wb,kres,hres)
           QNplantenp = 0.
           QNvegstruc = 0.
           masecnp = 0.
           matigestruc = 0.
           QNtige = 0.
           QNfeuille = 0.
           if (P_codeplante == CODE_FODDER) then
               QNplantenp = QNresgel
               QNvegstruc = QNresgel
               masecnp = msresgel
               matigestruc = msresgel
               QNtige = QNvegstruc
               QNfeuille = 0.
           endif
       endif
    endif
    endif

return
end subroutine ApportFeuillesMortes
end module ApportFeuillesMortes_m
 
