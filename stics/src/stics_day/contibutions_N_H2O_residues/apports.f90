! Addition of water, C and N to soil through:
!   - precipitation and irrigation
!   - mineral fertilizers
!   - organic fertilizers (including animal excreta during grazing)
!   - crop residues at harvest
!   - rhizodeposits (roots and rhizomes)
! -----------------------------------------------------
module apports_m
use messages
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Parametres_Generaux
USE Station
USE ApportResidusCulture_m, only: ApportResidusCulture
use Rhizomedeposition_m, only: Rhizome_radius, Rhizomedeposition
use Rootdeposition_m, only: Rootdeposition
use Rootdeath_m, only: Rootdeath
use eauEntrantSysteme_m, only: eauEntrantSysteme
use fertilisations_m, only: fertilisations
use Rootdeathfou_m, only: Rootdeathfou
implicit none

private
public :: apports

contains
 subroutine apports(logger,sc,pg,t,c,soil,p,itk,sta)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT)    :: pg  
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)   ! Toutes les plantes du systeme, pour somme LAI et ABSO
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)  
  type(Sol_),                 intent(INOUT) :: soil  
  type(Climat_),              intent(INOUT)    :: c  
  type(Stics_Transit_),       intent(INOUT) :: t  
  type(Station_),             intent(INOUT) :: sta

  integer :: n, irac, ihum, irhiz, ires, i, nbRes, imort

  n =sc%n
  nbRes = pg%nbResidus
  ihum = int(soil%P_profhum)

!  1) Apport d'eau par precipitation & irrigation
! -----------------------------------------------
  call eauEntrantSysteme(logger,sc,pg,t,c,p,itk)

!  2) Apport de C et N par les fertilisations (engrais mineraux et apports organiques exogenes)
! ---------------------------------------------------------------------------------------------
  call fertilisations(logger,sc,pg,t,c,soil,p,itk(1),sta)

  imort = 0
  do i=1,sc%P_nbPlantes

! 3) Apport de C et N par rhizodeposition chaque jour pour toutes les plantes (option densite vraie)
! --------------------------------------------------------------------------------------------------------------------------------------------------

     irac = int(p(i)%zracmax)
     if((p(i)%zracmax-irac) > 1.e-3) irac = irac + 1
     irac = min(irac,nbCouchesSol)
     if (p(i)%P_coderacine == 2 .and. n < p(i)%ndes .and. p(i)%nrec == 0) then
          ires = nbRes
          call Rootdeposition(&
            logger,irac, p(i)%drlsenf(1:irac), p(i)%drlseng(1:irac), p(i)%longsperacf, p(i)%longsperacg,        &
                     pg%P_awb(ires), pg%P_bwb(ires), pg%P_cwb(ires), pg%P_akres(ires), pg%P_bkres(ires), pg%P_ahres(ires), &
                     pg%P_bhres(ires), pg%P_CNresmin(ires), pg%P_CNresmax(ires), p(i)%QCrac, p(i)%QNrac, sc%Wb(ires),      &
                     sc%kres(ires),sc%hres(ires), sc%Cres(1:irac,ires),sc%Nres(1:irac,ires), p(i)%msrac(sc%n-1),           &
                     p(i)%msracmort, p(i)%QCracmort, p(i)%QNracmort, p(i)%P_code_rootdeposition)
     endif

! 4) Apport de C et N par mortalite du rhizome chaque jour pour les plantes avec reserve
! --------------------------------------------------------------------------------------

    ! Calcul de la profondeur du rhizome (hypothese sphere de rayon irhiz)
     irhiz = 2*Rhizome_radius(p(i)%maperenne(AOAS),p(i)%densite)
     irhiz = max(irhiz, 10)
     ires = nbRes   ! parametres de decomposition du rhizome = ceux des racines
     if (p(i)%P_code_acti_reserve == 1) then
         call Rhizomedeposition(logger, irhiz, p(i)%dltaperennesen(0), p(i)%dltaQNperennesen(0), pg%P_awb(ires), pg%P_bwb(ires), &
                                pg%P_cwb(ires), pg%P_akres(ires), pg%P_bkres(ires), pg%P_ahres(ires), pg%P_bhres(ires),      &
                                pg%P_CNresmin(ires), pg%P_CNresmax(ires), sc%Wb(ires), sc%kres(ires), sc%hres(ires),         &
                                sc%Cres(1:irhiz,ires), sc%Nres(1:irhiz,ires))
     endif

!  5) Apport de C et N par les residus lors d'une taille
! ------------------------------------------------------

     if (itk(i)%P_codcueille == 2 .and. n == p(i)%ntaille) then
        call ApportResidusCulture(&
          logger, nbRes, itk(i)%P_ressuite, p(i)%masecnp(0,sc%n-1), p(i)%magrain(0,sc%n-1),              &
                    p(i)%QNplantenp(0,sc%n-1), p(i)%QNgrain(0), p(i)%mabois(0), itk(i)%P_Stubblevegratio,                &
                    pg%P_CroCo(1:nbRes), pg%P_awb(1:nbRes), pg%P_bwb(1:nbRes), pg%P_cwb(1:nbRes), pg%P_akres(1:nbRes),   &
                    pg%P_bkres(1:nbRes), pg%P_ahres(1:nbRes), pg%P_bhres(1:nbRes), pg%P_CNresmin(1:nbRes),               &
                    pg%P_CNresmax(1:nbRes), pg%P_Qmulchdec(1:nbRes), sc%Wb(1:nbRes), sc%kres(1:nbRes), sc%hres(1:nbRes), &
                    sc%QCapp, sc%QNapp, sc%QCresorg, sc%QNresorg, p(i)%qressuite, p(i)%QCressuite, p(i)%QNressuite,      &
                    p(i)%qexport, p(i)%QNexport, p(i)%qressuite_tot, p(i)%QCressuite_tot, p(i)%QNressuite_tot,           &
                    sc%Cres(1,1:nbRes), sc%Nres(1,1:nbRes), sc%Cnondec(1:nbRes), sc%Nnondec(1:nbRes), sc%Chum(1),        &
                    sc%Nhum(1), sc%Chumi, sc%Nhumi, soil%CsurNsol, p(i)%matuber, sc%Chuma, sc%Nhuma)
     endif

!  6) Apport de C et N par les residus aeriens de culture le jour de recolte
! --------------------------------------------------------------------------
! DR 19112021 test zrac
     if (itk(i)%P_codcueille == 1 .and. n == p(i)%nrec .and. p(i)%fstressgel > 0.) then
        call ApportResidusCulture(&
          logger,nbRes, itk(i)%P_ressuite, p(i)%masecnp(0,sc%n-1), p(i)%magrain(0,sc%n-1),              &
                    p(i)%QNplantenp(0,sc%n-1), p(i)%QNgrain(0), p(i)%mabois(0), itk(i)%P_Stubblevegratio,                &
                    pg%P_CroCo(1:nbRes), pg%P_awb(1:nbRes), pg%P_bwb(1:nbRes), pg%P_cwb(1:nbRes), pg%P_akres(1:nbRes),   &
                    pg%P_bkres(1:nbRes), pg%P_ahres(1:nbRes), pg%P_bhres(1:nbRes), pg%P_CNresmin(1:nbRes),               &
                    pg%P_CNresmax(1:nbRes), pg%P_Qmulchdec(1:nbRes), sc%Wb(1:nbRes), sc%kres(1:nbRes), sc%hres(1:nbRes), &
                    sc%QCapp, sc%QNapp, sc%QCresorg, sc%QNresorg, p(i)%qressuite, p(i)%QCressuite, p(i)%QNressuite,      &
                    p(i)%qexport, p(i)%QNexport, p(i)%qressuite_tot, p(i)%QCressuite_tot, p(i)%QNressuite_tot,           &
                    sc%Cres(1,1:nbRes), sc%Nres(1,1:nbRes), sc%Cnondec(1:nbRes), sc%Nnondec(1:nbRes), sc%Chum(1),        &
                    sc%Nhum(1), sc%Chumi, sc%Nhumi, soil%CsurNsol, p(i)%matuber, sc%Chuma, sc%Nhuma)

! 7) Ajout Bruno fevrier 2018 mort des racines pour une non perenne le jour de la recolte
! ---------------------------------------------------------------------------------------

        if (p(i)%P_codeperenne == 1) then
           p(i)%ndes = n+1
           ires = nbRes

           call Rootdeath(&
             logger,irac, ihum, p(i)%P_coderacine, p(i)%longsperacf, p(i)%longsperacg, pg%P_awb(ires), pg%P_bwb(ires), &
                          pg%P_cwb(ires), pg%P_akres(ires), pg%P_bkres(ires), pg%P_ahres(ires), pg%P_bhres(ires),            &
                          pg%P_CNresmin(ires), pg%P_CNresmax(ires), sc%Wb(ires), sc%kres(ires), sc%hres(ires),               &
                          sc%Cres(1:nbCouchesSol,iRes), sc%Nres(1:nbCouchesSol,iRes), p(i)%rlf(1:irac), p(i)%rlg(1:irac), &
                          p(i)%msrac(sc%n),  &
                          p(i)%QCrac, p(i)%QNrac, p(i)%msracmort, p(i)%QCracmort, p(i)%QNracmort, p(i)%msrac(sc%n-1), sc%nh, &
                          soil%P_epc(1:sc%nh), p(i)%zrac, p(i)%msracmortf(1:sc%nh), p(i)%msracmortg(1:sc%nh),                  &
                          p(i)%drlsenf(1:irac), p(i)%drlseng(1:irac), p(i)%P_code_rootdeposition,                            &
                          p(i)%QNplantenp(0,sc%n-1), p(i)%QNgrain(0), p(i)%magrain(0,sc%n-1), pg%P_proprac, pg%P_y0msrac,    &
                          p(i)%masecnp(0,sc%n-1), p(i)%matuber, p(i)%P_codeplante, p(i)%P_code_acti_reserve)

           if (pg%P_codemsfinal == 2) then
    ! plante entiere (hors racines)
              p(i)%masec(:,:) = 0.
              p(i)%QNplante = 0.
    ! plante totale
              p(i)%mstot = 0.
              p(i)%QNtot = 0.
              p(i)%mafrais(:) = 0.
           endif
        endif
     endif

! 8) Loic Oct 2020 : mort des racines le jour de la fauche pour retrouver la v9
! -----------------------------------------------------------------------------

     if (p(i)%sioncoupe.and.p(i)%P_code_rootdeposition == 2.and.p(i)%P_code_acti_reserve == 2) then
           ires = nbRes
           irac = int(min(ihum,int(p(i)%zrac)))
           call Rootdeathfou(logger,ihum, p(i)%P_coderacine, pg%P_awb(ires), pg%P_bwb(ires), &
                          pg%P_cwb(ires), pg%P_akres(ires), pg%P_bkres(ires), pg%P_ahres(ires), pg%P_bhres(ires),            &
                          pg%P_CNresmin(ires), pg%P_CNresmax(ires), sc%Wb(ires), sc%kres(ires), sc%hres(ires),               &
                          sc%Cres(1:ihum,iRes), sc%Nres(1:ihum,iRes), p(i)%msrac(sc%n),  &
                          p(i)%msracmort, p(i)%QCracmort, p(i)%QNracmort, p(i)%msrac(sc%n-1),  &
                          p(i)%QNplantenp(0,sc%n-1), p(i)%QNgrain(0), p(i)%magrain(0,sc%n-1), pg%P_proprac, pg%P_y0msrac,    &
                          p(i)%masecnp(0,sc%n-1))
     endif
  enddo


return
end subroutine apports
 
end module apports_m
