module NMixedSoil_m
use iso_fortran_env, only: real32
implicit none
private
public :: NMixedSoil
contains
subroutine NMixedSoil( itrav, ihum, tmix,  &  ! IN
                          nit, amm)               ! INOUT
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
!  Mixing of soil  mineral N after soil tillage
!  Each tillage operation is assumed to mix water and SMN within the tilled layer defined by
!     the surface and the deeper depth (proftrav)
!  The mixing intensity (tmix) varies from 0 (no mixing) to 1 (complete mixing)
!  It is (temporarily) forced to 0.0
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c

implicit none


  integer, intent(IN)    :: itrav      ! Depth of soil tillage  // cm ! cote inferieure de l'operation de travail j   (cm)
  integer, intent(IN)    :: ihum       ! Humification depth  (max.60 cm) // cm
  real(real32),    intent(IN)    :: tmix       ! mixed rate  (0 = no mixed; 1 = total mixed)

  real(real32),    intent(INOUT) :: amm(ihum)  !  NH4 in the layer i     (kg/ha)
  real(real32),    intent(INOUT) :: nit(ihum)  !  NO3 in the layer i     (kg/ha)

! local VARIABLES
  integer :: iz      ! layer of 1 cm
  real    :: tot_nit ! stock NO3 in the proftrav layer     (kg/ha)
  real    :: tot_amm ! stock NH4 in the proftrav layer     (kg/ha)


! Calcul des pools d'azote sur la couche de melange [1,itrav2]
      tot_nit    = 0.
      tot_nit = SUM(nit(1:itrav))
      tot_nit = tot_nit/itrav

      tot_amm    = 0.
      tot_amm = SUM(amm(1:itrav))
      tot_amm = tot_amm/itrav

! Repartition des residus, biomasse & humus dans la couche de melange
      do iz = 1,itrav
          nit(iz) = (1.-tmix)*nit(iz) + tmix*tot_nit
          amm(iz) = (1.-tmix)*amm(iz) + tmix*tot_amm
      end do

return

end subroutine NMixedSoil
end module NMixedSoil_m
