! ----------------------------------------------------------------------------------------------
!  Simulates the root death and the addidition of dead roots to soil
!-----------------------------------------------------------------------------------------------
module Rootdeath_m
use messages
use Stics
use ResiduParamDec_m, only: ResiduParamDec
use plant_utils
implicit none
private
public :: Rootdeath
contains
 subroutine Rootdeath(&
    logger,irac, ihum, P_coderacine, longsperacf, longsperacg, awb, bwb, cwb, akres, bkres, ahres, bhres, CNresmin, &
                      CNresmax, Wb, kres, hres, Cres, Nres, rlf, rlg, msrac, QCrac, QNrac, msracmort, QCracmort, QNracmort,    &
                      msrac_veille, nh, epc, zrac, msracmortf, msracmortg, drlsenf, drlseng, P_code_rootdeposition,            &
                      QNplantenp, QNgrain, magrain, P_proprac, P_y0msrac, masecnp, matuber, P_codeplante, P_code_acti_reserve)
  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: irac          ! Number of layers containing dead roots
  integer, intent(IN)    :: ihum          ! = int(profhum)
  integer, intent(IN)    :: P_coderacine  ! // PARAMETER // Option for simulation of root growth
  real,    intent(IN)    :: longsperacf
  real,    intent(IN)    :: longsperacg
  real,    intent(IN)    :: awb           ! root decomposition parameter: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb           ! root decomposition parameter: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb           ! Minimum ratio C/N of microbial biomass  // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres         ! root decomposition parameter: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres         ! root decomposition parameter: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres         ! root decomposition parameter: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres         ! root decomposition parameter: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmin      ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax      ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: Cres(1:nbCouchesSol)
  real,    intent(INOUT) :: Nres(1:nbCouchesSol)
  real,    intent(INOUT) :: rlf(1:irac)
  real,    intent(INOUT) :: rlg(1:irac)
  real,    intent(INOUT) :: msrac         ! biomasse racinaire vivante (t.ha-1)
  real,    intent(INOUT) :: QCrac         ! amount of C in living roots //  kg.ha-1
  real,    intent(INOUT) :: QNrac         ! amount of N in living roots //  kg.ha-1
  real,    intent(INOUT) :: msracmort     ! dead root biomass (t.ha-1)
  real,    intent(INOUT) :: QCracmort
  real,    intent(INOUT) :: QNracmort
  real,    intent(INOUT) :: msrac_veille  ! biomasse racinaire vivante (t.ha-1)
  integer, intent(IN)    :: nh
  integer, intent(IN)    :: epc(nh)
  real,    intent(INOUT) :: zrac
  real,    intent(INOUT) :: msracmortf(1:nh)
  real,    intent(INOUT) :: msracmortg(1:nh)
  real,    intent(INOUT) :: drlsenf(1:irac)
  real,    intent(INOUT) :: drlseng(1:irac)
  integer, intent(IN)    :: P_code_rootdeposition
! Ajouts Loic Avril 2019 : correction bug pour profil type et calcul de la biomasse de racines de la betterave
  real,    intent(IN)          :: QNplantenp    ! // INPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1
  real,    intent(IN)          :: QNgrain       ! // INPUT // Amount of nitrogen in harvested organs (grains / fruits) // kg ha-1
  real,    intent(IN)          :: magrain       ! // INPUT // Harvested organs dry matter //
  real,    intent(IN)          :: P_proprac     ! // PARAMETER // Slope of the relationship (root mass vs shoot mass) at harvest  // g.g-1 // PARAM // 0.20
  real,    intent(IN)          :: P_y0msrac     ! // PARAMETER // minimal amount of root mass at harvest (when aerial biomass is nil) // t.ha-1 // PARAM // 0.7
  real,    intent(IN)          :: masecnp       ! // INPUT // Aboveground dry matter  // t.ha-1
  real,    intent(IN)          :: matuber       ! // INPUT // Sugarbeet harvested organ dry matter  // t.ha-1
  character(len=3), intent(IN) :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0
  integer, intent(IN)          :: P_code_acti_reserve

!: Variables locales
  integer :: i, iz, izmax, nhe
  real    :: Cracpm, NsurCrac
  real    :: msraciz, Craciz, Nraciz
  real    :: MSveget, QNveget, CNveget

        Cracpm = 380.  ! teneur en C des racines - definie aussi dans croira

! Modification du code pour retrouver les simulations de la v9 : Loic Mai 2019
! 3 cas possible lors de la recolte pour le devenir du C&N des racines :
! **********************************************************************
! 1 : si coderacine = 1  (profil type) :
!     la biomasse et la quantit� d'azote dans les racines sont calculees puis les flux de C&N associes sont
!     distribues de maniere homogene dans la couche 0-ihum
! 2.a : si coderacine = 2 (densite vraie) et code_rootdeposition == 2 (allocation dynamique d'azote aux racines desactivee) :
!     la biomasse et la quantit� d'azote dans les racines sont calculees puis les flux de C&N associes aux racines vivantes a le
!     jour de la recolte et mortes pendant l'USM sont distribues de maniere homogene dans la couche 0-ihum
! 2.b : si coderacine = 2 (densite vraie) et code_rootdeposition == 1 (allocation dynamique d'azote aux racines activee),
!     les racines vivantes le jour de la recolte meurent et les flux de C&N associes sont distribues de maniere
!     heterogene dans la couche 0-irac

! 1 : Calcul de msrac pour l'option profil type
!     *****************************************
        if (P_coderacine == 1) then
            msrac = masecnp * P_proprac + P_y0msrac
            msracmort = 0.
        ! Specificite de la betterave sous la v9 a revoir pour la v10 quand la betterave y sera parametree
            if (index(P_codeplante , CODE_BEET)>0.and.P_code_acti_reserve == 2) then
                msrac = (masecnp - matuber) * P_proprac + P_y0msrac
            endif
            MSveget = masecnp - (magrain/100.)
            QNveget = QNplantenp - QNgrain
            if (abs(MSveget).lt.1.0E-8) then
                CNveget= 0.
            else
            ! modif Florent C. et Fabien Octobre 2019 pour coherence avec v9
            ! CNveget = QNveget/MSveget
                CNveget = QNveget/(MSveget+msrac)
            endif
            QNrac = msrac * CNveget
       endif

! 2 : Calcul du retour des rcaines mortes pour l'option 'densite vraie'
!     ****************************************************************
! 2.a : Calcul du retour de la quantite d'azote dans les racines quand il n'y a pas d'allocation d'azote aux racines
!       ************************************************************************************************************
        if (msrac == 0.) return
        if (P_coderacine == 2 .and. P_code_rootdeposition == 2) then
            msrac = msrac + msracmort ! Cf v9 : les racines mortes sont incorporees au sol a la fin de l'USM
            msracmort = msrac
            MSveget = masecnp - (magrain/100.)
            QNveget = QNplantenp - QNgrain
            CNveget = QNveget/(MSveget+msrac)
            QNrac = msrac * CNveget
        endif

! 2.a et 2.b : Calcul du N/C des racines
!              *************************
        if(QCrac == 0.) QCrac = msrac * Cracpm
        NsurCrac = QNrac/QCrac

        if (P_coderacine == 2 .and. P_code_rootdeposition == 1) then
! 2.b : Calcul du retour des racines vivantes dans chaque horizon de sol
!       ****************************************************************
            do iz = 1,irac
               msraciz = (rlf(iz)/longsperacf + rlg(iz)/longsperacg) * 100.
               Craciz = msraciz * Cracpm
               Nraciz = Craciz * NsurCrac

               if(iz <= size(Cres)) then
                Cres(iz)= Cres(iz) + Craciz
               end if
               if(iz <= size(Nres)) then
                Nres(iz)= Nres(iz) + Nraciz
               end if
               msracmort = msracmort + msraciz
               QCracmort = QCracmort + Craciz
               QNracmort = QNracmort + Nraciz
            end do
            nhe = 0
            do i = 1, nh
                izmax = epc(i)
                do iz = 1, izmax
                    msracmortf(i) = msracmortf(i) + rlf(nhe+iz)/longsperacf*100.
                    msracmortg(i) = msracmortg(i) + rlg(nhe+iz)/longsperacg*100.
                    rlf(nhe+iz) = 0.
                    rlg(nhe+iz) = 0.
                    drlsenf(nhe+iz) = 0.
                    drlseng(nhe+iz) = 0.
                end do
                nhe = nhe + izmax
            end do
        else
! 2.a : Calcul du retour des racines vivantes et des racines mortes dans la couche mineralisante (profhum)
!       **************************************************************************************************
            msraciz = msrac/ihum
            Craciz = QCrac/ihum
            Nraciz = QNrac/ihum
            do iz = 1,ihum
               Cres(iz)= Cres(iz) + Craciz
               Nres(iz)= Nres(iz) + Nraciz
               QCracmort = QCracmort + Craciz
               QNracmort = QNracmort + Nraciz
            end do
        endif

        msrac = 0.
        msrac_veille = 0.
        QCrac = 0.
        QNrac = 0.
        ! c est la que zrac est remis a zero
        zrac = 0.

        call ResiduParamDec(logger, awb, bwb, cwb, akres, bkres, ahres, bhres, NsurCrac, CNresmin, CNresmax, Wb, kres, hres)

   return
 end subroutine Rootdeath
end module Rootdeath_m
 
