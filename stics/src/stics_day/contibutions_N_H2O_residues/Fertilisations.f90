! *-----------------------------------------------------------------------------------------------------------------------------------
! This program manages mineral and organic nitrogen supplies.
! - Stics book paragraphe 6.3, page 100
!
!! The inorganic N pool in soil can be replenished by the addition of synthetic fertilizers (called 'mineral fertilizers'),
!! by organic fertilizers which contain significant amounts of mineral N (for example: pig slurry, distillery vinasse, etc.),
!! by rainfall or irrigation water.
!!
!! The N inputs derived from rain and irrigation are sumed up in the variables AMMSURF (inputs of NH4-N) and
!! PRECIPN (inputs of NO3-N)(see apportsNparPluieEtIrrigation.f90).
!! The N inputs derived from mineral fertilizers (NH4 + NO3)-N (see apportsNparEngraisMineraux.f90) and from the inorganic fraction of
!! organic fertilizers (see apportsOrganiquesEtTravailDuSol.f90) are sumed up in the variable TOTAPN.
! *------------------------------------------------------------------------------------------------------------------------------------
module fertilisations_m
use messages
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Parametres_Generaux
USE Station
use calapnenupvt_m, only: calapnenupvt
use calculautomatiquefertilisation_m, only: calculautomatiquefertilisation
use apportsNparEngraisMineraux_m, only: apportsNparEngraisMineraux
use apportsnparpluieetirrigation_m, only: apportsnparpluieetirrigation
use apportsOrganiquesEtTravailDuSol_m, only: apportsOrganiquesEtTravailDuSol
implicit none
private
public :: fertilisations
contains
subroutine fertilisations(logger,sc,pg,t,c,soil,p,itk,sta)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT)    :: pg  
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)    ! Toutes les plantes du systeme, pour somme des LAI et des ABSO  
  type(ITK_),                 intent(INOUT) :: itk  
  type(Sol_),                 intent(INOUT) :: soil  
  type(Climat_),              intent(IN)    :: c  
  type(Stics_Transit_),       intent(INOUT) :: t  
  type(Station_),             intent(INOUT) :: sta

!: Variables locales
  integer :: nd
  integer :: ii
  integer :: n
  integer :: nbRes
  integer :: ihum
  integer :: engrais_type

  n=sc%n


  nbRes = pg%nbResidus
  ihum = int(soil%P_profhum)

      do nd = 1,min(5, sc%n)
         sc%Vabso5(nd) = 0.
         do ii = 1, sc%P_nbplantes
            sc%Vabso5(nd) = sc%Vabso5(nd) + p(ii)%abso(0,sc%n-nd)
         end do
      end do

      if (itk%P_codedateappn == 1 .and. p(1)%nplt > 0) then
          call calapNenUpvt(sc%n,p(1)%nplt, itk%P_upvttapN(1:300), itk%P_codefracappN, itk%P_doseN(1:300), &
                            itk%P_fracN(1:300), itk%P_Qtot_N, p(1)%nlev, p(1)%upvt(sc%n), sc%jjul,        &
                            sc%anit(sc%n), itk%numeroappN, itk%napN, p(1)%somupvt, itk%P_julapN(1:300))
             ! DR 01/02/2017 possibilite de pouvoir faire des ferti de type differents en somme de temp
          if(sc%anit(sc%n) > 0.) then
              sc%anit_engrais(sc%n) = sc%anit(sc%n)
! Loic Fevrier 2021 : engrais_multiple n'est plus une option
!              if(.not.itk%flag_plusieurs_engrais) then
!                  sc%type_ferti(sc%n) = itk%P_engrais(1)
!              else
              sc%type_ferti(sc%n) = itk%P_engrais(itk%numeroappN-1)
!              endif
          endif
      end if

! 1. calcul automatique de la fertilisation (NH4+NO3)
! ---------------------------------------------------
      if (t%P_codecalferti == 1) then
! DR 10/05/2022 les ferti automatiques ne fonctionnent pas car masec n'est pas connu je remplace par masecnp
! et j'initialise type_ferti du jour
        engrais_type = 8
        itk%P_engrais = engrais_type     ! P_engrais type 8 : donnees de pertes forcees (cf. doc)
         if (sc%n == p(1)%nplt) itk%napN = 0   ! le jour du semis, on initialise a 0 le nombre d'apports N
         call calculAutomatiqueFertilisation(pg%P_voleng(engrais_type), pg%P_deneng(engrais_type), pg%P_orgeng(engrais_type), &
                                            t%P_codetesthumN, p(1)%inns(AS), t%P_ratiolN, sc%precip, pg%P_plNmin,           &
                                            sc%cumoffrN, p(1)%demande(AS), p(1)%masecnp(AS,sc%n), p(1)%P_masecNmax,           &
                                            p(1)%QNplantenp(AS,sc%n), t%P_dosimxN, sc%hur(1), sc%hucc(1), p(1)%P_adilmax,   &
                                            p(1)%P_bdilmax, sc%effN, sc%anit(sc%n), itk%napN)
        sc%type_ferti(sc%n) = 8
      endif

! 2. Apports d'azote (NH4 et NO3) par les pluies et l'irrigation
! --------------------------------------------------------------
      call apportsNparPluieEtIrrigation(c%trr(sc%n), sta%P_concrr, sc%airg(sc%n), itk%P_concirr, itk%P_codlocirrig, sc%irrigN, &
                                       soil%nit(1), soil%nit(itk%P_locirrig), soil%amm(1), sc%precipN, sc%precipjN, sc%irrigjN)

! 3. Apports d'azote (NH4 et NO3) par les engrais (plusieurs types d'engrais et pas forcement le meme jour)
! ---------------------------------------------------------------------------------------------------------
! 3a. Apports d'azote (uree) par les vaches (pissats = engrais_pature)
      if(itk%P_restit(p(1)%numcoupe) == 1 .and. sc%anit_uree(sc%n) > 0.) then
            sc%type_ferti_pissats(sc%n) = t%P_engrais_pature
            call apportsNparEngraisMineraux(pg%P_orgeng(t%P_engrais_pature), pg%P_voleng(t%P_engrais_pature),        &
                                      pg%P_deneng(t%P_engrais_pature), sc%anit_uree(sc%n), sc%n, sc%Vabso5,        &
                                      pg%P_Vabs2, itk%P_codlocferti, itk%P_locferti, pg%P_pHminvol, pg%P_pHmaxvol,   &
                                      soil%pH_soil, pg%P_Xorgmax, soil%P_codedenit, pg%P_engamm(t%P_engrais_pature), &
                                      soil%Nvoleng, soil%Ndenit, soil%Norgeng, soil%QNvoleng, soil%QNorgeng,         &
                                      soil%QNdenit, sc%Nhum(1:10), sc%Nhuma, soil%amm(1:max(1,itk%P_locferti)),      &
                                      soil%nit(1:max(1,itk%P_locferti)), sc%totapN)
            sc%flag_onacoupe = .FALSE.
      endif

! 3b. Apports d'azote (NH4 et NO3) par les engrais mineraux
      if(sc%type_ferti(sc%n) > 0 .and. sc%anit(sc%n) > 0.) then
            call apportsNparEngraisMineraux(pg%P_orgeng(sc%type_ferti(sc%n)), pg%P_voleng(sc%type_ferti(sc%n)),             &
                                      pg%P_deneng(sc%type_ferti(sc%n)), sc%anit(sc%n), sc%n, sc%Vabso5(1:5),          &
                                      pg%P_Vabs2, itk%P_codlocferti, itk%P_locferti, pg%P_pHminvol, pg%P_pHmaxvol,    &
                                      soil%pH_soil, pg%P_Xorgmax, soil%P_codedenit, pg%P_engamm(sc%type_ferti(sc%n)), &
                                      soil%Nvoleng, soil%Ndenit, soil%Norgeng, soil%QNvoleng, soil%QNorgeng,          &
                                      soil%QNdenit, sc%Nhum(1:10), sc%Nhuma, soil%amm(1:max(1,itk%P_locferti)),       &
                                      soil%nit(1:max(1,itk%P_locferti)), sc%totapN)
      endif
! *---------------------------------------------------------------
! 4. Apport d'amendements organiques  et/ou travail du sol
! *-    Consequences: - apport d'azote mineral sous forme NH4
! *-                  - apport de differentes formes de MO
! *-                  - melange de ces MO par le travail du sol
! *-  P_profres  = cote superieure d'incorporation des residus
! *-  P_proftrav = profondeur de travail du sol = cote inferieure
! *---------------------------------------------------------------

      call apportsOrganiquesEtTravailDuSol(logger,sc%n, ihum, nbRes, &
                   t%P_codeNmindec, itk%P_nbjres, itk%P_nbjtrav, &
                   p(1)%numjres(1:itk%P_nbjres), p(1)%numjtrav(1:itk%P_nbjtrav), itk%P_coderes(1:itk%P_nbjres), &
                   itk%P_proftrav(1:itk%P_nbjtrav), itk%P_profres(1:itk%P_nbjres), itk%P_qres(1:itk%P_nbjres),  &
                   itk%P_Crespc(1:itk%P_nbjres), itk%P_Nminres(1:itk%P_nbjres), itk%P_eaures(1:itk%P_nbjres),   &
                   pg%P_CNresmin(1:nbRes), pg%P_CNresmax(1:nbRes), pg%P_awb(1:nbRes), pg%P_bwb(1:nbRes),        &
                   pg%P_cwb(1:nbRes), pg%P_CroCo(1:nbRes), pg%P_akres(1:nbRes), pg%P_bkres(1:nbRes), pg%P_ahres(1:nbRes),  &
                   pg%P_bhres(1:nbRes), pg%P_Qmulchdec(1:nbRes), pg%P_alphapH, pg%P_dpHvolmax, pg%P_pHvols,          &
                   soil%pH_soil, soil%Nvolatorg, sc%totapN, sc%totapNres, itk%P_CsurNres(1:itk%P_nbjres), &
                   itk%nap, sc%airg(sc%n+1), sc%Cres(1:ihum,1:nbRes), sc%Nres(1:ihum,1:nbRes), &
                   sc%Cbio(1:ihum,1:nbRes), sc%Nbio(1:ihum,1:nbRes), sc%Chum(1:ihum), sc%Nhum(1:ihum), sc%Wb(1:nbRes), &
                   sc%kres(1:nbRes), sc%hres(1:nbRes), soil%itrav1, soil%itrav2, sc%Cmulchnd, sc%Nmulchnd, &
                   sc%Cnondec(1:nbRes), sc%Nnondec(1:nbRes), soil%dpH, soil%pHvol, sc%QCapp, sc%QNapp, sc%QCresorg,    &
                   sc%QNresorg, sc%irmulch, sc%numcult, pg%P_codeinitprec, sc%Chumi, sc%Nhumi, soil%CsurNsol, sc%qmulch, &
                   sc%hur(1:ihum),  sc%sat(1:ihum), soil%amm(1:ihum), soil%nit(1:ihum), sc%Chuma, sc%Nhuma) !, &
                   ! PL, 12/04/2022: option inutile maintenant
                   ! t%P_code_depth_mixed_humus)
                     ! DR 07/06/2019 j'ajoute sat pour le melange Eau et n au travail du sol

      if (sc%anit(sc%n) > 0.) then
         t%nbapN(1) = t%nbapN(1) + 1
         t%dateN(1,t%nbapN(1)) = sc%jjul
      endif
      if (sc%P_nbplantes > 1) sc%precipN = sc%precipN + p(1)%stemflow * sta%P_concrr   ! Si on simule + d'1 plante

 return
end subroutine Fertilisations
end module fertilisations_m
