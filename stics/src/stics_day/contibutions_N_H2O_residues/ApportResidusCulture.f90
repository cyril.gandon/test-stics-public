! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 6.3.4, page 105
!
!! The calculation of crop residues returning to the soil for the following crop, in terms of quantity (QRESSUITE) and quality (CSURNRESSUITE),
!! relies on the parameter "ressuite", defining four possible management practices according to the plant fraction remaining in the field and then incorporated
!! into the soil:
! - roots (ressuite='roots') when harvesting, for example, lettuce or textile flax,
! - trimmed woody material (ressuite='prunings') for vineyards'fan club
! - straw and fine roots (ressuite='straw+roots') when harvesting cereal grains or sugar-beet taproots or potatoes,
! - stubble and fine roots (ressuite='stubble+roots') when harvesting cereal grains and straw together or silage maize or cutting meadow,
! - whole crop (ressuite='whole_crop') corresponding to catch crops, green manure or crop volunteers.
!! Plant residues are assumed to remain at the soil surface until being buriedfP by the following soil tillage, except for pure roots which are assumed
!! to be located between the surface and the PROFHUM depth.
!!
!! In all cases the fine root biomass (MSRAC) calculation is required, which is done differently according to the option chosen for root profile.
!! For the standard profile, root biomass is assumed to be a fixed proportion of shoot biomass.
!!
!! The quantities of residues (QRESSUITE, in t DM ha-1) left on the soil at harvest are calculated.
!! The nitrogen content of the returned residues is CsurNressuite
!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
module ApportResidusCulture_m
use messages
use ResidusApportSurfaceSol_m, only: ResidusApportSurfaceSol
use ResiduParamDec_m, only: ResiduParamDec
implicit none
private
public :: ApportResidusCulture
contains
subroutine ApportResidusCulture(&
   logger,nbResidus, ressuite, masecnp, magrain, QNplantenp, QNgrain, mabois, Stubblevegratio,      &
                   CroCo, awb, bwb, cwb, akres, bkres, ahres, bhres, CNresmin, CNresmax, Qmulchdec, Wb, kres, hres,       &
                   QCapp, QNapp, QCresorg, QNresorg, qressuite, QCressuite, QNressuite, qexport, QNexport, qressuite_tot, &
                   QCressuite_tot, QNressuite_tot, Cres1, Nres1, Cnondec, Nnondec, Chum1, Nhum1, Chumi, Nhumi, CsurNsol,  &
                   matuber, Chuma, Nhuma)
  type(logger_), intent(in) :: logger
  integer, intent(IN) :: nbResidus
  character(len=45), intent(IN) :: ressuite  ! Type of crop residue (straw, roots, ...)

  real,    intent(IN) :: masecnp       ! Aboveground dry matter  // t.ha-1
  real,    intent(IN) :: magrain       ! magrain(n)
  real,    intent(IN) :: QNplantenp    ! Amount of N accumulated in the plant  // kg.ha-1
  real,    intent(IN) :: QNgrain       ! Amount of N in harvested organs (grains / fruits) // kg ha-1
  real,    intent(IN) :: mabois        ! Pruning biomass // t.ha-1
  real,    intent(IN) :: Stubblevegratio

  real,    intent(INOUT) :: CroCo(nbResidus)   ! parameter of organic residues decomposition // SD // PARAM // 1
  real,    intent(IN) :: awb(nbResidus)     ! parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN) :: bwb(nbResidus)     ! parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN) :: cwb(nbResidus)     ! Minimum ratio C/N of microbial biomass : CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN) :: akres(nbResidus)   ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN) :: bkres(nbResidus)   ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN) :: ahres(nbResidus)   ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN) :: bhres(nbResidus)   ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN) :: CNresmin(nbResidus) ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN) :: CNresmax(nbResidus) ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(IN) :: Qmulchdec(nbResidus)! maximal amount of decomposing mulch // t.ha-1 // PARAM // 1

  real, intent(INOUT) :: Wb(nbResidus)
  real, intent(INOUT) :: kres(nbResidus)
  real, intent(INOUT) :: hres(nbResidus)
  real,    intent(INOUT) :: QCapp          ! cumulative amount of organic C added to soil // kg.ha-1
  real,    intent(INOUT) :: QNapp          ! cumulative amount of organic N added to soil // kg.ha-1
  real,    intent(INOUT) :: QCresorg
  real,    intent(INOUT) :: QNresorg
  real,    intent(INOUT) :: qressuite     ! quantity of crop residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: QCressuite    ! quantity of N in crop residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: QNressuite    ! quantity of C in crop residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: qexport       !
  real,    intent(INOUT) :: QNexport
  real,    intent(OUT)   :: qressuite_tot  ! amount of harvest residues (aerials + roots) returned to soil // t.ha-1
  real,    intent(OUT)   :: QCressuite_tot ! N in total harvest residues (aerials + roots) returned to soil // kg.ha-1
  real,    intent(INOUT) :: QNressuite_tot
  real,    intent(INOUT) :: Cres1(nbResidus)
  real,    intent(INOUT) :: Nres1(nbResidus)
  real,    intent(INOUT) :: Cnondec(nbResidus)    ! undecomposable C stock of residues on the surface // t.ha-1
  real,    intent(INOUT) :: Nnondec(nbResidus)    ! undecomposable N stock of residues on the surface // kg.ha-1
  real,    intent(INOUT) :: Chum1
  real,    intent(INOUT) :: Nhum1
  real,    intent(INOUT) :: Chumi
  real,    intent(INOUT) :: Nhumi
  real,    intent(INOUT) :: CsurNsol
  real,    intent(IN)    :: matuber
  real,    intent(INOUT) :: Chuma
  real,    intent(INOUT) :: Nhuma

!: Variables locales
  real    :: Cracpc, Nracpc
  real    :: Cressuite, CsurNressuite
  real    :: MSveget
  real    :: QNveget
  real    :: Wr
  integer :: ires = 0


     Cressuite = 42.     ! Hypothese: teneur en carbone des residus de culture = 42% MS
     Cracpc = 38.        ! Hypothese: teneur en carbone des racines = 38% MS
     Nracpc = 1.         ! Hypothese: teneur en azote des racines = 1% MS

! Restitution des parties aeriennes
! *********************************
!        MS et N contenus dans les parties vegetatives au jour n-1
! Loic Novembre 2017 ! Attention ! prise en compte de la specificite de la Betterave !
         if (matuber == 0.) then
            MSveget = masecnp - magrain / 100.
            QNveget = QNplantenp - QNgrain
         else
            MSveget = masecnp - matuber
            QNveget = QNplantenp - QNgrain
         endif

         qressuite = 0.
         QNressuite = 0.
         qexport = 0.
         QNexport = 0.
         CsurNressuite = 999.

 !  1er cas:  restitution de la plante entiere  (plante jeune type engrais vert)
         if (index(ressuite, 'whole_crop') >0) then
            qressuite = masecnp
            QNressuite = QNplantenp
            ires = 2
         endif
 !  2eme cas : restitution des pailles
         if (index(ressuite, 'straw+roots') >0) then
            qressuite = MSveget
            qexport = masecnp - qressuite
            QNressuite = QNveget
            QNexport = QNplantenp - QNressuite
            ires = 1
         endif
!   3eme cas : restitution des bois de taille
         if (index(ressuite, 'prunings') >0) then
            qressuite = mabois
            QNressuite = mabois * 0.5 * 10.
            ires = 8  
         endif
!   4eme cas : restitution des chaumes = 35% de MS vegetative
        if (index(ressuite, 'stubble+roots') >0) then
            qressuite =  0.35 * MSveget
            QNressuite = 0.35 * QNveget
            qexport = masecnp - qressuite
            QNexport = QNplantenp - QNressuite
            ires = 1
        endif
!   5eme cas : restitution des chaumes = P_Stubblevegratio % de MS vegetative
         if (index(ressuite, 'stubbleveg') >0) then
            qressuite =  Stubblevegratio * MSveget
            QNressuite = Stubblevegratio * QNveget
            qexport = masecnp - qressuite
            QNexport = QNplantenp - QNressuite
            ires = 1
         endif

! FF 29/05/2019 ajout du cas ressuite = roots, qui �tait oubli� pour le calcul de QNexport et qexport
!   6eme cas : restitution des racines uniquement
         if (ressuite == 'roots') then
            QNexport = QNplantenp
            qexport = masecnp
         endif

! Restitution
         QCressuite = qressuite * Cressuite * 10.
         CsurNressuite = 999.
         if (QNressuite > 1.e-8) CsurNressuite = qressuite * Cressuite / QNressuite * 10.
         if(ires > 0) then
            call ResidusApportSurfaceSol(qressuite, Cressuite, -1.e-10, CNresmin(ires), CNresmax(ires), qmulchdec(ires), &
                                    CsurNressuite, Cnondec(ires), Nnondec(ires), Cres1(ires), Nres1(ires), QCapp, QNapp, &
                                    QCresorg, QNresorg, CroCo(ires), Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
         end if
          Wr = 0.
          if (CsurNressuite > 0.) Wr = 1./CsurNressuite
          if(ires > 0) then
            call ResiduParamDec(logger, awb(ires), bwb(ires), cwb(ires), akres(ires), bkres(ires), ahres(ires), &
                                 bhres(ires), Wr, CNresmin(ires), CNresmax(ires), Wb(ires), kres(ires), hres(ires))
          end if
          qressuite_tot = qressuite_tot + qressuite
          QNressuite_tot = QNressuite_tot + QNressuite
          QCressuite_tot = QCressuite_tot + QCressuite

return
end subroutine ApportResidusCulture
 
end module ApportResidusCulture_m
