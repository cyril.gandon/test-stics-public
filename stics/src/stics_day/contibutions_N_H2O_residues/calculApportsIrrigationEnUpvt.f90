
! *************************************************************
!! * subroutine de calcul des dates d'apport d'eau en upvt     *
!! *************************************************************

!subroutine calculApportsIrrigationEnUpvt(sc,p,itk)

!USE Stics
!USE Plante
!USE Itineraire_technique
!    implicit none
!    type(Stics_Communs_),  intent(INOUT) :: sc
!    type(Plante_),         intent(INOUT) :: p
!    type(ITK_),            intent(INOUT) :: itk
!
!    if (p%nlev > 0) then
!
!      p%somupvtI = p%somupvtI + p%upvt(sc%n)
!
!      if (itk%P_upvttapI(itk%numeroappI) /= 0 .and. p%somupvtI >= itk%P_upvttapI(itk%numeroappI)) then
!        itk%P_julapI(itk%numeroappI) = sc%jjul
!        sc%airg(sc%n) = itk%P_doseI(itk%numeroappI)
!        itk%numeroappI = itk%numeroappI+1
!        itk%nap = itk%numeroappI-1
!      endif
!
!    endif

!! merge trunk 23/11/2020
! recup de operate mais meme avant car on passe directement les arguments au lieu des structures
module calculApportsIrrigationEnUpvt_m
implicit none
private
public :: calculApportsIrrigationEnUpvt
contains
subroutine calculApportsIrrigationEnUpvt(n,nplt,nlev,nrecbutoir, &
                   airg, P_irrlev, preciptmp, nap, somupvtI, upvt, &
                   jjul, P_upvttapI, numeroappI, P_julapI, P_doseI  )

    implicit none


integer, intent(IN)     :: n, nplt, nlev, nrecbutoir, jjul
integer, intent(INOUT)  :: nap, P_julapI(300), numeroappI
real, intent(IN)        :: P_irrlev, preciptmp, upvt
integer, intent(IN) :: P_upvttapI(300)
real, intent(INOUT)     :: airg, somupvtI, P_doseI(300)

! DR 13/02/2020
! si on est en somme de temperature on peut vouloir (utilisation en mode plan d'experience)
! vouloir irriguer au semis pour favoriser la levee comme en irrig auto
    if (n >= nplt .and. n < nrecbutoir) then
      !if (n == nplt) nap = 0
      ! ** apport minimal au semis pour permettre la germination
        if (n == nplt .and. preciptmp < P_irrlev) then
          airg = P_irrlev - preciptmp
        nap = nap + 1
      endif
    endif

    if (nlev > 0) then
      somupvtI = somupvtI + upvt
      if (P_upvttapI(numeroappI) /= 0 .and. somupvtI >= P_upvttapI(numeroappI)) then
        P_julapI(numeroappI) = jjul
        airg = P_doseI(numeroappI)
        numeroappI = numeroappI+1
        nap = numeroappI-1
      endif
    endif


return
end subroutine calculApportsIrrigationEnUpvt
end module calculApportsIrrigationEnUpvt_m
 
