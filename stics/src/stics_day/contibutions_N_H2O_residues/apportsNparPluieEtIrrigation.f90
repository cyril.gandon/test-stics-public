! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
! N inputs by rainfall and irrigation
!!
! - Stics book paragraphe 6.3.1, page 61
!
!    - The N inputs by rainfall (PRECIPJN, in kg ha-1) are the product of the amount of precipitation (TRR, in mm) and
!! its mean concentration in mineral N (CONCRR, in kg ha-1 mm-1). A concentration of 1 mg L-1 corresponds to 0.01 kg ha-1 mm-1.
!! The N input from rainfall occurs at the soil surface and is assumed to consist in 50% of NH4+ and 50% of NO3-.
!!
!    - The N inputs due to irrigation water (IRRIGJN, in kg ha-1) are also the product of the amounts of water (AIRG, in mm) and
!! its mean concentration, defined in the technical file (CONCIRR, in kg ha-1 mm-1).
!! The N input is located either at soil surface or at depth LOCIRRIG if the option 'localised irrigation' is activated (CODLOCIRRIG = 3).
!! The mineral N in the irrigation water is assumed to be exclusively in the form of NO3- .
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module apportsNparPluieEtIrrigation_m
implicit none
private
public :: apportsNparPluieEtIrrigation
contains
subroutine apportsNparPluieEtIrrigation(trr, P_concrr, airg, P_concirr, P_codlocirrig, irrigN, nit_surf, nit_locirrig, &
                                        amm_surf, precipN, precipjN, irrigjN)
! Bruno remise de pluieN
  implicit none

  real,    intent(IN)    :: trr           ! Daily precipitation  // mm.d-1
  real,    intent(IN)    :: P_concrr      ! Mineral N concentration in the precipitation // kg.ha-1.mm-1  (0.01 corresponds to 1 mg N.l-1)
  real,    intent(IN)    :: airg          ! Daily irrigation    // mm.d-1
  real,    intent(IN)    :: P_concirr     ! Mineral N concentration in irrigation water // kg.ha-1.mm-1
  integer, intent(IN)    :: P_codlocirrig ! Code of irrigation localisation: 1= above the foliage, 2= below the foliage above the soil, 3 = in the soil
  real,    intent(INOUT) :: irrigN        ! cumulative input of mineral N from irrigation      // kg.ha-1
  real,    intent(INOUT) :: nit_surf      ! nitrate-N content in the 0-1 cm layer              // kg.ha-1
  real,    intent(INOUT) :: amm_surf      ! ammonium-N content in the 0-1 cm layer             // kg.ha-1
  real,    intent(INOUT) :: nit_locirrig  ! nitrate-N content at depth P_locirrig              // kg.ha-1
  real,    intent(INOUT) :: precipN       ! cumulative input of mineral N from precipitation   // kg.ha-1
  real,    intent(INOUT) :: precipjN      ! daily input of mineral N from precipitation        // kg.ha-1
  real,    intent(INOUT) :: irrigjN       ! daily input of mineral N from irrigation           // kg.ha-1

  ! daily N input by precipitation
      precipjN = trr * P_concrr
  ! daily N input by irrigation
      irrigjN = airg * P_concirr

  ! cumulative N inputs
      irrigN = irrigN + irrigjN
      precipN = precipN + precipjN

  ! update of nitrate and ammonium in soil due to precipitation (50% NH4 + 50% NO3)
      nit_surf = nit_surf + 0.5 * precipjN
      amm_surf = amm_surf + 0.5 * precipjN

  ! update of nitrate and ammonium in soil due to irrigation (100% NO3)
      if (P_codlocirrig /= 3) then
          nit_surf = nit_surf + irrigjN
      else
     ! DR 09022016 verifier le test if (P_codlocirrig == 3 .and. P_locirrig /= 0) then
     ! voir si nit_locirrig est utilise
          nit_locirrig = nit_locirrig + irrigjN
      endif

return
end subroutine apportsNparPluieEtIrrigation
end module apportsNparPluieEtIrrigation_m
 
