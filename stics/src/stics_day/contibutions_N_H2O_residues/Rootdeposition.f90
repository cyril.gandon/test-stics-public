! ----------------------------------------------------------------------------------------------
!  Simulates the CONTINUOUS addition of rhizodeposits (dead roots) to soil during the crop cycle
!  if 'true density' and 'root deposition' options is activated for roots
!-----------------------------------------------------------------------------------------------
module Rootdeposition_m
use messages
use ResiduParamDec_m, only: ResiduParamDec
implicit none

private
public :: Rootdeposition

contains 
 subroutine Rootdeposition(logger,nbCouches, drlsenf, drlseng, longsperacf, longsperacg, awb, bwb, cwb, &
                           akres, bkres, ahres, bhres, CNresmin, CNresmax, QCrac, QNrac,  &
                           Wb, kres, hres, Cres, Nres, msrac, msracmort, QCracmort, QNracmort, &
                           P_code_rootdeposition)
  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: nbCouches     ! Number of layers containing dead roots
  real,    intent(IN)    :: drlsenf(1:nbCouches)
  real,    intent(IN)    :: drlseng(1:nbCouches)
  real,    intent(IN)    :: longsperacf   ! specific root length of fine roots   // cm g-1
  real,    intent(IN)    :: longsperacg   ! specific root length of coarse roots // cm g-1
  real,    intent(IN)    :: awb           ! parameter of organic residue decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb           ! parameter of organic residue decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb           ! Minimum ratio C/N of microbial biomass  // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres         ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres         ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres         ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres         ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmin      ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax      ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(IN)    :: msrac         ! biomasse racinaire vivante (t.ha-1)

  real,    intent(INOUT) :: QCrac         ! amount of C in living roots //  kg.ha-1
  real,    intent(INOUT) :: QNrac         ! amount of N in living roots //  kg.ha-1
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: Cres(1:nbCouches)
  real,    intent(INOUT) :: Nres(1:nbCouches)
  real,    intent(INOUT) :: msracmort     ! dead root biomass (t.ha-1)
  real,    intent(INOUT) :: QCracmort
  real,    intent(INOUT) :: QNracmort
  integer, intent(IN)    :: P_code_rootdeposition

!: Variables locales
  real    :: Cracpm = 380. ! teneur en C des racines - definie aussi dans croira
  real    :: CsurNrac_j = 1000.
  real    :: dltamsracmort = 0.
  real    :: Capp = 0.
  real    :: Napp = 0.
  real    :: Wr = 0.
  integer :: iz = 1
  real    :: dltaQCracmort ! Cumulative amount of C lost by dead roots over the soil profile // kg N.ha-1.day-1
  real    :: dltaQNracmort ! Cumulative amount of N lost by dead roots over the soil profile // kg N.ha-1.day-1

! No calculation without any layer containing dead roots
if (nbCouches == 0) return

!*** restitution des racines mortes au sol
!    ----------------------------------------------------
!*** Commentaires Loic Mai 2019 ***
! 1. Deux options pour le calcul du retour des racines au sol :
!    - si le code rootdeposition est active (=1) --> demande et allocation d'azote dynamique vers les racines, alors QNrac est calcul journalierement
!      dans absoN et les racines mortes sont restitu�es au jour e jour
!    - si le code rootdeposition est desactive (=2) alors on calcule ici la biomasse de racines mortes cumulees sur l'USM. Les racines mortes pendant
!      l'USM seront restitu�es le jour de la recolte dans la routine Rootdeath

! Modification du code pour retrouver les simulations de la v9 : Loic Mai 2019
! 1 : Calcul du ratio C/N des racines
!     *******************************
   if (QNrac >  1.e-6 .and. QCrac > 1.e-5) CsurNrac_j = QCrac/QNrac ! CsurNrac = valeur de la veille

! 2 : Calcul de la deposition des racines mortes dans chaque couche
!     *************************************************************
   if (P_code_rootdeposition == 1) then
      dltaQCracmort = 0.
      dltaQNracmort = 0.
      do iz = 1,nbCouches
         dltamsracmort = (drlsenf(iz) / longsperacf + drlseng(iz) / longsperacg) * 100.
         Capp = dltamsracmort * Cracpm
         Napp = Capp / CsurNrac_j
         Cres(iz)= Cres(iz) + Capp
         Nres(iz)= Nres(iz) + Napp
         dltaQCracmort = dltaQCracmort + Capp
         dltaQNracmort = dltaQNracmort + Napp
         msracmort = msracmort + dltamsracmort
      end do
      QCrac = msrac * Cracpm    ! msrac a ete mis a jour dans croira (msracmort deduit)
      if (QNrac < dltaQNracmort) dltaQNracmort = QNrac
      QNrac = QNrac - dltaQNracmort
      QCracmort = QCracmort + dltaQCracmort
      QNracmort = QNracmort + dltaQNracmort
      if (CsurNrac_j > 0.) Wr = 1./CsurNrac_j
      call ResiduParamDec(logger, awb, bwb, cwb, akres, bkres, ahres, bhres, Wr, CNresmin, CNresmax, Wb, kres, hres)
   else
! 3 : Calcul de la biomasse de racines mortes cumulee sur l'USM
!     *********************************************************
      do iz = 1,nbCouches
         dltamsracmort = (drlsenf(iz) / longsperacf + drlseng(iz) / longsperacg) * 100.
         msracmort = msracmort + dltamsracmort
      end do
   end if

   return
 end subroutine Rootdeposition
end module Rootdeposition_m
 
