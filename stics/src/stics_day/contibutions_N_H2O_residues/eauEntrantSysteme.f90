! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
! water entering the soil
! - Stics book paragraphe 6.2, page 99
!
!! The quantity of water reaching the soil is attributable to rain or irrigation, after passage through vegetation and losses by surface runoff.
!!
!! Rain which penetrates into the soil is called PRECIP
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module eauEntrantSysteme_m
USE Stics
USE Plante
USE Itineraire_Technique
USE Climat
USE Parametres_Generaux
use messages
use phenology_utils
use affectrrquinoapoquet_m, only: affectrrquinoapoquet
use calculapportsirrigationenupvt_m, only: calculapportsirrigationenupvt
use irrig_m, only: irrig
use plant_utils
implicit none
private
public :: eauEntrantSysteme
contains
subroutine eauEntrantSysteme(logger,sc,pg,t,c,p,itk)
  type(logger_), intent(in) :: logger
!: Arguments
  type(Stics_Communs_),       intent(INOUT) :: sc  

  type(Parametres_Generaux_), intent(IN)    :: pg  

  type(Plante_),              intent(INOUT) :: p (sc%P_nbplantes)  

  type(ITK_),                 intent(INOUT) :: itk (sc%P_nbplantes)  

  type(Climat_),              intent(INOUT) :: c  

  type(Stics_Transit_),       intent(INOUT) :: t  

!: Variables locales
  real :: preciptmp(2)  
  real :: stem  
  real :: precip1(2)  
  real :: precip2(2)  

  integer :: i ! indice plante  
  integer :: ens  
  integer :: n  !  
!  integer ::  AS  !
!  integer ::  AO
!  integer ::  AOAS
  real    :: preciptmp_plante1

      ! pour alleger l'ecriture
      n = sc%n
      !AS = sc%AS
      !AO = sc%AO
      !AOAS = sc%AOAS



! DR 16/11/2015 je gere le demarrage des irrigations auto dates ou stades ici
   if(itk(1)%P_codedate_irrigauto == IRRIG_AUTO_STAGE) then
    if(is_n_this_stage(n,p(1)%nplt,p(1)%nger,p(1)%nlev,p(1)%namf,p(1)%nlax,p(1)%ndrp,p(1)%nflo,p(1)%nsen,&
       p(1)%nrec,p(1)%nmat,p(1)%ndebdorm,p(1)%nfindorm,itk(1)%P_stage_start_irrigauto)) then
      sc%n_datedeb_irrigauto = n
    end if
    if(is_n_this_stage(n,p(1)%nplt,p(1)%nger,p(1)%nlev,p(1)%namf,p(1)%nlax,p(1)%ndrp,p(1)%nflo,p(1)%nsen,&
      p(1)%nrec,p(1)%nmat,p(1)%ndebdorm,p(1)%nfindorm,itk(1)%P_stage_end_irrigauto)) then
      sc%n_datefin_irrigauto = n
    end if
   endif

      if (itk(1)%P_codedate_irrigauto == IRRIG_AUTO_STAGE)then
        if(sc%n_datedeb_irrigauto > sc%n_datefin_irrigauto) then
          call exit_error(logger, MESSAGE_408)
        endif
      endif



      !: Pour la quinoa en poquet traitement particulier des pluies a la levee
      !- attention a n'utiliser que pour une culture pure
      if (p(1)%P_codeplante == CODE_QUINOA .and. t%P_codepluiepoquet == 1) then
         if (p(1)%nplt /= -999 .and. n >= p(1)%nplt .and. p(1)%nplt /= 0) then
           call affectrrquinoapoquet(n,p(1)%nplt,p(1)%nlev,t%P_nbjoursrrversirrig,sc%airg(n),c%trr(n))
         endif
      endif


      preciptmp(:) = 0.
      stem         = 0.
      precip1(:)   = 0.
      precip2(:)   = 0.

      do i = 1, sc%P_nbplantes

        p(i)%stemflow = 0.0
        do ens = AS,AO
          !: Si surf = 0 alors, pas besoin de calculer...
          if (p(i)%surf(ens) > 0.0) then

            ! merge trunk 23/11/2020
          ! DR 09/10/09 si on est en calcul des irrigations avec des sommes upvt
          !  if (itk(i)%P_codedateappH2O == 1 .and. p(i)%nplt > 0) then
          !  call calculApportsIrrigationEnUpvt(sc,p(i),itk(i))
          !   endif

            if (i > 1) then
              preciptmp(ens) = precip2(ens) / p(i)%surf(ens)

              ! DR 13022020 on ajoute une dose a nplt pour favoriser la levee si on est en irr=ug en somme de temp
                        ! DR 09/10/09 si on est en calcul des irrigations avec des sommes upvt
              if (itk(i)%P_codedateappH2O == 1 .and. p(i)%nplt > 0) then
              call calculApportsIrrigationEnUpvt(n,p(i)%nplt,p(i)%nlev,p(i)%nrecbutoir, &
                   sc%airg(n), pg%P_irrlev, preciptmp(ens), itk(i)%nap, p(i)%somupvtI, p(i)%upvt(n), &
                   sc%jjul, itk(i)%P_upvttapI, itk(i)%numeroappI, itk(i)%P_julapI, itk(i)%P_doseI  )
            endif

!: ML - 29/10/12 - ajout de l'argument dureehumec dans call irrig
              call irrig(logger, n,p(i)%nplt,p(i)%nrecbutoir,itk(i)%P_codecalirrig,pg%P_irrlev,     &
                         p(i)%swfac(ens),itk(i)%P_ratiol,p(i)%cumlracz,p(i)%zrac,         &
                         sc%hucc(1:int(p(i)%zrac)),sc%hur(1:int(p(i)%zrac)),            &
                         itk(i)%P_dosimx,itk(i)%P_doseirrigmin,p(i)%exofac,                 &
                         itk(i)%P_codlocirrig,itk(i)%P_effirr,p(i)%lai(ens,n),              &
                         p(i)%P_codeintercept,p(i)%P_stemflowmax,p(i)%P_kstemflow,            &
                         p(i)%parapluie,i,sc%P_nbplantes,p(i)%surfSous,p(i)%P_mouillabil,   &
                         p(i)%P_codeplante,                 &
                         itk(i)%nap,sc%airg(n),preciptmp(ens),stem,precip1,             &
                         p(i)%mouill(ens),p(i)%interpluie(ens),                         &
                         p(i)%irrigprof(itk(i)%P_locirrig),sc%eaunoneffic,p(i)%totpl,   &
                         t%P_codeSWDRH,c%dureehumec,itk(i)%P_codedate_irrigauto ,  &
                         itk(i)%P_datedeb_irrigauto,itk(i)%P_datefin_irrigauto)
            else

              preciptmp(ens) = c%trr(n)


          ! DR 09/10/09 si on est en calcul des irrigations avec des sommes upvt
              if (itk(i)%P_codedateappH2O == 1 .and. p(i)%nplt > 0) then
                call calculApportsIrrigationEnUpvt(n,p(i)%nplt,p(i)%nlev,p(i)%nrecbutoir, &
                   sc%airg(n), pg%P_irrlev, preciptmp(ens), itk(i)%nap, p(i)%somupvtI, p(i)%upvt(n), &
                   sc%jjul, itk(i)%P_upvttapI, itk(i)%numeroappI, itk(i)%P_julapI, itk(i)%P_doseI  )

              endif

!#if DEBUG == 1
!              if (iand(sc%irrig,2) >0) call irrig_debug_write_input(1301,sc,pg,p,itk,c,t,i,ens,preciptmp(ens),stem,precip1)
!#endif

              call irrig(logger, n,p(i)%nplt,p(i)%nrecbutoir,itk(i)%P_codecalirrig,pg%P_irrlev,     & ! IN
                         p(i)%swfac(ens),itk(i)%P_ratiol,p(i)%cumlracz,p(i)%zrac,         &
                         sc%hucc(1:int(p(i)%zrac)),sc%hur(1:int(p(i)%zrac)),            &
                         itk(i)%P_dosimx,itk(i)%P_doseirrigmin,p(i)%exofac,                 &
                         itk(i)%P_codlocirrig,itk(i)%P_effirr,p(i)%lai(ens,n),              &
                         p(i)%P_codeintercept,p(i)%P_stemflowmax,p(i)%P_kstemflow,            &
                         p(i)%parapluie,i,sc%P_nbplantes,p(i)%surfSous,p(i)%P_mouillabil,   &
!      itk(i)%P_locirrig,p(i)%P_codeplante,pg%P_codeSIG,                    & !23/07/2012 on utilise pas plocirrig
                         p(i)%P_codeplante,                                             &
                         itk(i)%nap,sc%airg(n),preciptmp(ens),stem,precip1,             & ! INOUT
                         p(i)%mouill(ens),p(i)%interpluie(ens),                         &
                         p(i)%irrigprof(itk(i)%P_locirrig),sc%eaunoneffic,p(i)%totpl,   &
                         t%P_codeSWDRH,c%dureehumec,itk(i)%P_codedate_irrigauto,        &
                         sc%n_datedeb_irrigauto,sc%n_datefin_irrigauto)

              preciptmp_plante1=preciptmp(ens)
!#if DEBUG == 1
!              if (iand(sc%irrig,8) >0) call irrig_debug_write_output(1303,sc,pg,p,itk,c,t,i,ens,preciptmp(ens),stem,precip1)
!#endif

            endif
            p(i)%stemflow = p(i)%stemflow + (stem * p(i)%surf(ens))
            if (i > 1) then
              p(i)%mouill(ens) = p(i)%mouill(ens) * p(i)%surf(ens)
            else
              precip2(1) = precip1(1)
              precip2(2) = precip1(2)
            endif
          endif
        end do

        p(i)%mouill(AOAS) = p(i)%mouill(AS) * p(i)%surf(AS) &
                             + p(i)%mouill(AO) * p(i)%surf(AO)

        p(i)%interpluie(AOAS) = p(i)%interpluie(AS) + p(i)%interpluie(AO)


      end do

    ! domi - 13/10/2004 - on le sort de la boucle des plantes car airg est deja le cumul de 2 plantes
    ! PB - 06/12/2004 - on le met apres l'appel a irrig en cas de calcul automatique des irrigations.
      sc%totir  = sc%totir + sc%airg(n)

    ! DR 18/09/07 pour benj on conserve les dates d'apport
      do i = 1, sc%P_nbplantes
        if (sc%airg(n) > 0) then
          t%nbapirr(i) = t%nbapirr(i) + 1
          t%dateirr(i,t%nbapirr(i)) = sc%jjul
        endif
      enddo


    ! Reconstitution de precip et des quantites d'eau sur les feuilles
      i = sc%P_nbplantes

      sc%precip = preciptmp(AS) * p(i)%surf(AS) + preciptmp(AO) * p(i)%surf(AO)


      ! DR 10/11/2017 verification de l'existence de la CAS
      if (sc%P_nbplantes.gt.1) then
          ! DR 10/07/2014 correction permettant de prendre en compte les pluies anterieures au semis lorsque qu'on est an CAS
          ! if(p(2)%surf(AS)+p(2)%surf(AO) == 0.)sc%precip=preciptmp_plante1
          if(abs(p(2)%surf(AS)+p(2)%surf(AO)).lt.1.0E-8)sc%precip=preciptmp_plante1
      endif
 
 

      do i = sc%P_nbplantes-1,1,-1
        sc%precip = sc%precip + p(i)%stemflow
      end do


return
end subroutine eauEntrantSysteme
end module eauEntrantSysteme_m
 
