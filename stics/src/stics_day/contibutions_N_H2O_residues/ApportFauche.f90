module ApportFauche_m
use messages
use ResiduParamDec_m, only: ResiduParamDec
use ResidusApportSurfaceSol_m, only: ResidusApportSurfaceSol
implicit none
private
public :: ApportFauche
contains
subroutine ApportFauche(logger, qresidu, QNressuite, CNresmin, CNresmax, Qmulchdec, awb, bwb, cwb, akres, bkres, &
                        ahres, bhres, Wb, kres, hres, CroCo, Cres1, Nres1, Cnondec, Nnondec,  &
                        QCapp, QNapp, QCresorg, QNresorg, QCressuite, CsurNressuite, qressuite_tot, QNressuite_tot, &
                        QCressuite_tot, Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
  type(logger_), intent(in) :: logger
  real,    intent(INOUT) :: qresidu     ! biomass of residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: QNressuite    ! amount of N in residues returned to soil // kg.ha-1
  real,    intent(IN)    :: CNresmin      ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax      ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(IN)    :: Qmulchdec     ! maximal amount of decomposing mulch // t.ha-1 // PARAM // 1
  real,    intent(IN)    :: awb       ! parameter  of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb       ! parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb       ! Minimum ratio C/N of microbial biomass in the relationship: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres     ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres     ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres     ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres     ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: CroCo
  real,    intent(INOUT) :: Cres1
  real,    intent(INOUT) :: Nres1
  real,    intent(INOUT) :: Cnondec   ! undecomposable C stock of the type 10 residues on the surface // t.ha-1
  real,    intent(INOUT) :: Nnondec   ! undecomposable N stock of the type 10 residues on the surface // kg.ha-1
  real,    intent(INOUT) :: QCapp         ! cumulative amount of organic C added to soil // kg.ha-1
  real,    intent(INOUT) :: QNapp         ! cumulative amount of organic N added to soil // kg.ha-1
  real,    intent(INOUT) :: QCresorg
  real,    intent(INOUT) :: QNresorg
  real,    intent(INOUT) :: QCressuite    ! amount of C in residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: CsurNressuite
  real,    intent(INOUT) :: qressuite_tot      ! biomass of total residues returned to soil // t.ha-1
  real,    intent(INOUT) :: QNressuite_tot     ! amount of N in total residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: QCressuite_tot     ! amount of C in total residues returned to soil // kg.ha-1
  real,    intent(INOUT) :: Chum1      ! Amount of C in active humus pool at depth iz=1 // kg.ha-1
  real,    intent(INOUT) :: Nhum1      ! Amount of N in active humus pool at depth iz=1 // kg.ha-1
  real,    intent(INOUT) :: Chumi      ! Amount of C in inert humus pool // kg.ha-1
  real,    intent(INOUT) :: Nhumi      ! Amount of N in inert humus pool // kg.ha-1
  real,    intent(INOUT) :: CsurNsol
  real,    intent(INOUT) :: Chuma
  real,    intent(INOUT) :: Nhuma

!: Variables locales
  real    :: Cressuite
  real    :: Wr

       if(qresidu == 0.) return

       Cressuite = 42.                  ! Hypothese: teneur en C des residus de fauche = 42% MS
       QCressuite = qresidu * Cressuite * 10.
       CsurNressuite = 9999.
       if (QNressuite > 1.e-8) CsurNressuite = QCressuite / QNressuite

       call ResidusApportSurfaceSol(qresidu, Cressuite, -1.e-10, CNresmin, CNresmax, qmulchdec, CsurNressuite,  &
                                     Cnondec, Nnondec, Cres1, Nres1, QCapp, QNapp, QCresorg, QNresorg, &
                                     CroCo, Chum1, Nhum1, Chumi, Nhumi, CsurNsol, Chuma, Nhuma)
       Wr=0.
       if (CsurNressuite > 0.) Wr = 1./CsurNressuite
       call ResiduParamDec(logger, awb, bwb, cwb, akres, bkres, ahres, bhres, Wr, CNresmin, CNresmax, Wb, kres, hres)

       qressuite_tot = qressuite_tot + qresidu
       QNressuite_tot = QNressuite_tot + QNressuite
       QCressuite_tot = QCressuite_tot + QCressuite

return
end subroutine ApportFauche
end module ApportFauche_m
