! Updating plant mulch at soil surface
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module mulch_m
implicit none
private
public :: mulch
contains
subroutine mulch(nbresid, Cres1, Nres1, Cbio1, Nbio1, Cnondec, Nnondec, Cmulchnd, Nmulchnd, &
                 Cmulchdec, Nmulchdec, Cmulch, Nmulch, Cbmulch, Nbmulch, qmulch)
  implicit none

  integer, intent(IN)    :: nbresid      ! number of residues (only at soil surface)  = (nbResidus-1)/2
  real,    intent(IN)    :: Cres1(nbresid)    ! amount of C in residues of the layer 1 // kg.ha-1
  real,    intent(IN)    :: Nres1(nbresid)    ! amount of N in residues of the layer 1 // kg.ha-1
  real,    intent(IN)    :: Cbio1(nbresid)    ! amount of C in biomass of the layer 1 // kg.ha-1
  real,    intent(IN)    :: Nbio1(nbresid)    ! amount of N in biomass of the layer 1 // kg.ha-1
  real,    intent(IN)    :: Cnondec(nbresid)  ! undecomposable C stock of residue ir at soil surface //  t.ha-1
  real,    intent(IN)    :: Nnondec(nbresid)  ! undecomposable N stock of residue ir at soil surface // kg.ha-1
  real,    intent(INOUT) :: Cmulchnd     ! quantity of C in undecomposable plant mulch // kg.ha-1
  real,    intent(INOUT) :: Nmulchnd     ! quantity of N in undecomposable plant mulch // kg.ha-1
  real,    intent(INOUT) :: Cmulchdec    ! quantity of C in undecomposable plant mulch // kg.ha-1
  real,    intent(INOUT) :: Nmulchdec    ! quantity of N in undecomposable plant mulch // kg.ha-1
  real,    intent(INOUT) :: Cmulch       ! quantity of C in total mulch // kg.ha-1
  real,    intent(INOUT) :: Nmulch       ! quantity of N in total mulch // kg.ha-1
  real,    intent(INOUT) :: Cbmulch      ! quantity of C in microbial biomass decomposing mulch // kg.ha-1
  real,    intent(INOUT) :: Nbmulch      ! quantity of N in microbial biomass decomposing mulch // kg.ha-1
  real,    intent(INOUT) :: qmulch       ! total biomass of mulch // t.ha-1

  integer :: ir

      Cmulchdec = 0.
      Nmulchdec = 0.
      Cmulchnd = 0.
      Nmulchnd = 0.
      Cbmulch = 0.
      Nbmulch = 0.
      do ir = 1,nbresid
            Cbmulch = Cbmulch + Cbio1(ir)
            Nbmulch = Nbmulch + Nbio1(ir)
            Cmulchdec = Cmulchdec + Cres1(ir)
            Nmulchdec = Nmulchdec + Nres1(ir)
            Cmulchnd = Cmulchnd + Cnondec(ir)
            Nmulchnd = Nmulchnd + Nnondec(ir)
      end do
      Cmulch = Cmulchdec + Cmulchnd
      Nmulch = Nmulchdec + Nmulchnd
      qmulch = Cmulch / 420.

  return
end subroutine mulch
end module mulch_m
 
 
