! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
! - Stics book paragraphe 6.3.3, page 104-105
!
!! The N inputs from organic residues arrive onto the soil either under mineral form (mainly as NH4+) or under organic form.
!! The mineral fraction enters the soil mineral pool and is submitted to NH3 volatilization, nitrification, leaching and plant uptake.
!! The organic fraction decomposes more or less rapidly and mineralizes its C and N according to the decomposition module (see ResidusDecomposition.f90).
!!
!! The module is generic and can simulate most types of organic residues.
!! 10 categories are considered:
!    - 1) mature crop residues (straw, roots)
!    - 2) catch crop residues (young plants)
!    - 3) farmyard manure
!    - 4) compost
!    - 5) sewage sludge
!    - 6) distillery vinasse
!    - 7) animal horn
!    - 8) vineyard prunings
!    - 9) slurry
!    -10) rhizomes
!!
!! The net mineralization (positive or negative) due to the addition of these residues depends on the category and the C/N ratio of the residue.
!! The characteristics of each organic residue are defined in the technical file: category, depth of incorporation in soil, amount of fresh matter added,
!! carbon content, C/N ratio, water content and mineral N content. .
!! In the case of chained simulations (a 10.2), the characteristics of the crop residues returning to the soil are simulated by the model and
!! are taken into account automatically in the next simulation.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module apportsOrganiquesEtTravailDuSol_m
use messages
use ResidusApportSurfaceSol_m, only: ResidusApportSurfaceSol
use ResidusMelangeSol_m, only: ResidusMelangeSol
use ResiduParamDec_m, only: ResiduParamDec
use ResidusParamDecMelange_m, only: ResidusParamDecMelange
use H2OandNmixedSoil_m, only: H2OandNmixedSoil
use NMixedSoil_m, only: NMixedSoil
implicit none
private
public :: apportsOrganiquesEtTravailDuSol
contains
subroutine apportsOrganiquesEtTravailDuSol(logger, n, ihum, nbRes, codeNmindec, nbjres, nbjtrav, numjres, numjtrav, coderes,&
                     proftrav, profres, qres, Crespc, Nminres, eaures, CNresmin, CNresmax, awb, bwb, cwb, CroCo, &
                     akres, bkres, ahres, bhres, Qmulchdec, alphapH, dpHvolmax, pHvols, pH_soil, Nvolatorg, totapN,  &
                     totapNres, CsurNres, nap, airg, Cres, Nres, Cbio, Nbio, Chum, Nhum, Wb, kres, hres, itrav1, itrav2,&
                     Cmulchnd, Nmulchnd, Cnondec, Nnondec, dpH, pHvol, QCapp, QNapp, QCresorg, QNresorg,   &
                     irmulch, numcult, codeinitprec, Chumi, Nhumi, CsurNsol, qmulch, hur,  sat, amm, nit, Chuma, Nhuma) !, &
                     ! PL, 12/04/2022: option inutile maintenant
                     !P_code_depth_mixed_humus)
                     ! DR 07/06/2019 j'ajoute sat pour le melange Eau et n au travail du sol
  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: n
  integer, intent(IN)    :: ihum               ! int(soil%P_profhum)
  integer, intent(IN)    :: nbRes              ! nbResidus
  integer, intent(IN)    :: codeNmindec        ! option to activate the available N :yes (1), no(2) // code 1/2
  integer, intent(IN)    :: nbjres             ! number of residue additions
  integer, intent(IN)    :: nbjtrav            ! number of tillage operations
  integer, intent(IN)    :: numjres(nbjres)    ! julian day of residue addition
  integer, intent(IN)    :: numjtrav(nbjtrav)  ! julian day of tillage operation
  integer, intent(IN)    :: coderes(nbjres)    ! residue type: 1=crop residues,  2=residues of CI,  3=manure,  4=compost OM,  5=mud SE,  6=vinasse,  7=corn,  8=other // code 1 to 10 // PARTEC // 0
  real,    intent(IN)    :: proftrav(nbjtrav)  ! Depth of soil tillage  // cm // PARTEC // 1
  real,    intent(IN)    :: profres(nbjtrav)   ! minimal depth of organic residue incorporation  // cm // PARTEC // 1
  real,    intent(IN)    :: qres(nbjres)       ! amount of crop residue or organic amendments applied to the soil (fresh weight) // t.ha-1 // PARTEC // 1
  real,    intent(IN)    :: Crespc(nbjres)     ! carbon content of organic residue //  // PARTEC // 1
  real,    intent(IN)    :: Nminres(nbjres)    ! N mineral content of organic residues  // % fresh matter // PARTEC // 1
  real,    intent(IN)    :: eaures(nbjres)     ! Water amount of organic residues  // % fresh matter // PARTEC // 1
  real,    intent(IN)    :: CNresmin(nbRes)  ! minimum value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax(nbRes)  ! maximum value of ratio C/N of organic residues // g g-1 // PARAM // 1
  real,    intent(IN)    :: awb(nbRes)       ! parameter  of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb(nbRes)       ! parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb(nbRes)       ! Minimum ratio C/N of microbial biomass in the relationship: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(INOUT)    :: CroCo(nbRes)     ! parameter of organic residues decomposition  // SD // PARAM // 1
  real,    intent(IN)    :: akres(nbRes)     ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres(nbRes)     ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres(nbRes)     ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres(nbRes)     ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: Qmulchdec(nbRes) ! maximal amount of decomposing mulch // t C.ha-1 // PARAM // 1
  real,    intent(IN)    :: alphapH          ! maximal soil pH variation per unit of inorganic N added with slurry // kg-1 ha //PARAM //1
  real,    intent(IN)    :: dpHvolmax        ! maximal pH increase following the application of organic residue such as slurry // SD // PARAM // 1
  real,    intent(IN)    :: pHvols           ! soil pH above which soil pH is not affected by addition of organic residue (such as slurry)  // SD // PARAM // 1
  real,    intent(IN)    :: pH_soil          ! basic soil pH // SD // PARSOL // 1
  real,    intent(INOUT) :: Nvolatorg        ! Nvolatorg = NH4 volatilisable  (these de T. MORVAN 1999)
  real,    intent(INOUT) :: totapN           ! Total amount of mineral N inputs through fertiliser and organic residues  // kg.ha-1
  real,    intent(INOUT) :: totapNres        ! Total amount of organic N inputs through organic products  // kg.ha-1
  real,    intent(INOUT) :: CsurNres(nbjres) ! C/N ratio of residues // g g-1 // PARTEC // 1
  integer, intent(INOUT) :: nap              ! number of water additions
  real,    intent(INOUT) :: airg             ! Daily water addition (irrigation + water added by exogenous residues) // mm
  real,    intent(INOUT) :: Cres(ihum,nbRes)
  real,    intent(INOUT) :: Nres(ihum,nbRes)
  real,    intent(INOUT) :: Cbio(ihum,nbRes)
  real,    intent(INOUT) :: Nbio(ihum,nbRes)
  real,    intent(INOUT) :: Chum(ihum)
  real,    intent(INOUT) :: Nhum(ihum)
  real,    intent(INOUT) :: Wb(nbRes)
  real,    intent(INOUT) :: kres(nbRes)
  real,    intent(INOUT) :: hres(nbRes)
  integer, intent(INOUT) :: itrav1
  integer, intent(INOUT) :: itrav2
  real,    intent(INOUT) :: Cmulchnd
  real,    intent(INOUT) :: Nmulchnd
  real,    intent(INOUT) :: Cnondec(nbRes)     ! undecomposable C stock of the type 10 residues on the surface //  t.ha-1
  real,    intent(INOUT) :: Nnondec(nbRes)     ! undecomposable N stock of the type 5 residues on the surface // kgN.ha-1
  real,    intent(INOUT) :: dpH  
  real,    intent(INOUT) :: pHvol           ! soil surface pH varying after organic residue application (such as slurry) // SD
  real,    intent(INOUT) :: QCapp
  real,    intent(INOUT) :: QNapp
  real,    intent(INOUT) :: QCresorg
  real,    intent(INOUT) :: QNresorg
  integer, intent(INOUT) :: irmulch    ! Utilise dans shutwall : est-ce correct ?

  integer, intent(IN)    :: numcult
  integer, intent(IN)    :: codeinitprec
  real,    intent(INOUT) :: Chumi
  real,    intent(INOUT) :: Nhumi
  real,    intent(INOUT) :: CsurNsol
  real,    intent(INOUT) :: qmulch
  real,    intent(INOUT) :: hur(ihum)
  real,    intent(INOUT) :: amm(ihum)
  real,    intent(INOUT) :: nit(ihum)
  real,    intent(INOUT) :: sat(ihum)
  
  real,    intent(INOUT) :: Chuma
  real,    intent(INOUT) :: Nhuma
! PL, 12/04/2022: option inutile maintenant
! integer, intent(IN)    :: P_code_depth_mixed_humus ! option to mixed the humus on the depth itrav1-itrav2 (yes=1) or on the depth 1-itrav2 (no=2) // 1,2


! DR 03/07/2018 activation du melange H2O N lors du travail du sol : BM pense qu'il vaudra mieux le calculer das le futur en fonction du travail du sol donc on le fixe pour le moment a 1.0


!: Variables locales
  integer :: is
  integer :: ires
  real    :: Qeaures
  real    :: propvolat  !
  real    :: travsol    ! code de travail du sol
  real    :: QNminres   ! Quantite de NH4 contenu dans l'amendement (kg N/ha)
  real    :: dpHvolm    !
  real    :: dpHvol  
  real    :: Wr
  integer :: kkk,rr,itrav_test
  real    :: tmix ! mixed ratio for the mixed of N and H2O in the  Depth of residue incorporation  (0 = pno mixed; 1 = homogeneous mixed)
  logical :: debug_a

  debug_a=.FALSE.

! 1) Apports de MO exogenes au sol  (nbjres = nombre de dates d'apport; numjres = jour de chaque apport)
! ------------------------------------------------------------------------------------------------------
   do is = 1,nbjres
      if (n == numjres(is)) then
     !    type de residu organique apporte
          ires = coderes(is)
 ! si on enchaine, on considere l'apport de residus de type 1 (fichier ITK) seulement pour la 1ere annee
          if(codeinitprec == 2 .and. numcult > 1 .and. (ires == 1 .or. ires == 21)) EXIT
 ! modif Bruno : les apports de MO se font en surface du sol, donc ires < 11 sauf dans le cas d'un apport de racines !
 !        if(ires > 10) ires = ires-10
          if(ires > 10 .and. ires/= nbRes) ires = ires-10
     ! irmulch utilise seulement dans shutwall : est-ce utile ?
          irmulch = ires

 ! Apport d'eau par les residus organiques (1 mm eau = 10 t eau /ha)
 ! -----------------------------------------------------------------
          Qeaures = qres(is) * eaures(is) / 1000.
          if (Qeaures >= 2.) then     ! on ne considere l'apport d'eau que si l'eau apportee est > 2 mm
            airg = airg + Qeaures
            nap  =  nap + 1
          endif

 ! calcul de l'apport de NH4 et du NH4 volatilisable
          QNminres = qres(is)*Nminres(is)*10.
          travsol = 1.
          if (is > 1) then
             if (proftrav(is - 1) > 1. .and. (n - numjtrav(is - 1)) <= 7) travsol = -1.
          end if
        ! Quantite de N volatilisable au jour de l'apport
          propvolat = amin1(1., 0.37 + 0.029 *(100. - eaures(is)) + 0.117 * travsol)
          Nvolatorg = propvolat * QNminres

        ! Calcul de la variation de pH liee a l'apport de NH4 de l'amendement (eq 8.18)
        !    Morvan:    variation maximale = 0.94  pour un apport de 200 kg N/ha (74 m3/ha)
        !    Chantigny: variation maximale = 2.80  pour un apport de 500 kg N/ha (90 m3/ha)
          dpHvolm = amin1(alphapH*QNminres,dpHvolmax)
          dpH = 0.
          if(Nvolatorg > 0. .and. pHvols /= 7.) then
            dpHvol = 0.
            if (pH_soil <= 7.) dpHvol = dpHvolm
            if (pH_soil > 7. .and. pH_soil <= pHvols) dpHvol = dpHvolm * amax1(0.,(pHvols-pH_soil)/(pHvols-7.))
            dpH = dpH + dpHvol / Nvolatorg
            pHvol = pH_soil + dpHvol
          endif

!  actualisation des stocks
    !-    le NH3 volatilise sera deduit du stock NH4 dans le ss-pg volatorg
    !-    le NH4 non volatilisable est mis dans la 2eme couche de sol (1-2 cm)
          amm(1) = amm(1) + Nvolatorg
          amm(2) = amm(2) + QNminres - Nvolatorg
          totapN = totapN + QNminres
          totapNres = totapNres + QNminres
 if(debug_a) write(4455,*)' avant ResidusApportSurfaceSol',n,amm(1),amm(2),Chum(1), Nhum(1)
          call ResidusApportSurfaceSol(qres(is), Crespc(is), eaures(is), CNresmin(ires), CNresmax(ires), qmulchdec(ires), &
                                        CsurNres(is), Cnondec(ires), Nnondec(ires), Cres(1,ires), Nres(1,ires), QCapp,    &
                                        QNapp, QCresorg, QNresorg, CroCo(ires), Chum(1), Nhum(1), Chumi, Nhumi, CsurNsol, &
                                        Chuma, Nhuma)
 if(debug_a) write(4455,*)' apres ResidusApportSurfaceSol',n,amm(1),amm(2),Chum(1), Nhum(1)

          Wr = 0.
          if(CsurNres(is) > 0.) Wr = 1./CsurNres(is)
          call ResiduParamDec(logger, awb(ires), bwb(ires), cwb(ires), akres(ires), bkres(ires), ahres(ires), &
                              bhres(ires), Wr, CNresmin(ires), CNresmax(ires), Wb(ires), kres(ires), hres(ires))
      endif
   end do

! 2) Operations de travail du sol (nbjtrav = nombre de dates; numjtrav = jour de chaque operation)
! ------------------------------------------------------------------------------------------------
    do is = 1,nbjtrav
        if (n == numjtrav(is)) then
           itrav1 = int(profres(is))
           itrav2 = int(proftrav(is))
           itrav_test=22
           if(codeNmindec == 1) itrav1 = 1
              rr=1
      if(debug_a) write(4455,*)'avResidusMelangeSol',n,'iz',(kkk,Cres(kkk,rr),Nres(kkk,rr),Cbio(kkk,rr),  &
                            Nbio(kkk,rr),kkk=1,itrav_test)
      if(debug_a)  write(4455,*)'kkk',(kkk,Chum(kkk), 'Nhum',Nhum(kkk),kkk=1,itrav_test)
      if(debug_a)  write(4455,*)'rr ',(rr, Cnondec(rr), Nnondec(rr),rr=1,nbres),Cmulchnd, Nmulchnd

           call ResidusMelangeSol(itrav1, itrav2, nbRes, ihum, Cres(1:ihum,1:nbRes), Nres(1:ihum,1:nbRes),      &
                                  Cbio(1:ihum,1:nbRes), Nbio(1:ihum,1:nbRes), Chum(1:ihum), Nhum(1:ihum),  &
                                  Cnondec(1:nbRes), Nnondec(1:nbRes), Cmulchnd, Nmulchnd) 
                                  ! PL, 12/04/2022: option inutile maintenant
                                  !, P_code_depth_mixed_humus )

     if(debug_a)  write(4456,*)'apResidusMelangeSol',n,'iz',(kkk,Cres(kkk,rr),Nres(kkk,rr),Cbio(kkk,rr),  &
                            Nbio(kkk,rr),kkk=1,itrav_test)
     if(debug_a)  write(4456,*)'kkk',(kkk,Chum(kkk), Nhum(kkk),kkk=1,itrav_test)
     if(debug_a)  write(4456,*)'rr ',(rr, Cnondec(rr), Nnondec(rr),rr=1,nbres),Cmulchnd, Nmulchnd


     if(debug_a) write(4455,*)'avResidusParamDecMelange',n,('r',rr, kres(rr), hres(rr),rr=1,nbres)

           call ResidusParamDecMelange(itrav2, ihum, nbRes, awb(1:nbRes), bwb(1:nbRes), cwb(1:nbRes),    &
                                        akres(1:nbRes), bkres(1:nbRes), ahres(1:nbRes), bhres(1:nbRes),  &
                                        Cres(1:ihum,1:nbRes), Nres(1:ihum,1:nbRes), CNresmin(1:nbRes), &
                                        CNresmax(1:nbRes), Wb(1:nbRes), kres(1:nbRes), hres(1:nbRes))

     if(debug_a)write(4456,*)'apResidusParamDecMelange',n,('r',rr, kres(rr), hres(rr),rr=1,nbres)

           qmulch = 0.

! Bruno juin 2017 melange de eau et N mineral dans la couche qui est travaillee
           !call EauNminMelangeSol(proftrav(is), ihum, hur, amm, nit)
           ! DR 07/06/2019 pour etre en coherence avce la 9.1 j'appelle le module H2OnadNmixedSoil
           !DR 27/06/2018 je mets tmix=1.0 pour activer le melange et le tester sur SMS
           tmix=1.0
           call H2OandNmixedSoil(proftrav(is), ihum , tmix, hur, sat, amm, nit)

        endif
   end do
return
end subroutine apportsOrganiquesEtTravailDuSol
end module apportsOrganiquesEtTravailDuSol_m
 
