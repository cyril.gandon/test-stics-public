module mineral_tests
   use mineral_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private

   public :: collect

contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_fh", test_get_fh) &
                  ]

   end subroutine collect

   subroutine test_get_fh(error)
      type(error_type), allocatable, intent(out) :: error

      call check(error, get_fh(1., 2., 3, 4., 5., 6., 7., 8., 9.), 0.)
   end subroutine

end module
