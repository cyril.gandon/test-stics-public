! *----------------------------------------------------------------------------------------------------
!! This program updates the decomposition parameters of an organic residue after a new residue addition
! Stics book  page 265-287
! *----------------------------------------------------------------------------------------------------
module ResiduParamDec_m
use messages
use messages_data
implicit none
private
public :: ResiduParamDec
contains
  subroutine ResiduParamDec(logger, awb, bwb, cwb, akres, bkres, ahres, bhres, Wr, CNresmin, CNresmax, Wb, kres, hres)
  type(logger_), intent(in) :: logger
  real,    intent(IN)    :: awb       ! decomposition parameter : CsurNbio = awb+bwb.Wr // SD // PARAM // 1
  real,    intent(IN)    :: bwb       ! decomposition parameter : CsurNbio = awb+bwb.Wr // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb       ! decomposition parameter : CsurNbio = awb+bwb.Wr // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres     ! decomposition parameter : kres = akres+bkres.Wr // d-1 // PARAM // 1
  real,    intent(IN)    :: bkres     ! decomposition parameter : kres = akres+bkres.Wr // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres     ! decomposition parameter : hres = 1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres     ! decomposition parameter : hres = 1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real,    intent(IN)    :: Wr        ! N/C of the residue    g.g-1
  real,    intent(IN)    :: CNresmin  ! minimum value of C/N ratio of residue // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax  ! maximum value of C/N ratio of residue // g g-1 // PARAM // 1
  real,    intent(INOUT) :: Wb        ! N/C ratio of the microbial biomass    g.g-1
  real,    intent(INOUT) :: kres      ! decomposition rate of the organic residue  d-1
  real,    intent(INOUT) :: hres      ! humification rate of the organic residue   g.g-1

  ! VARIABLES LOCALES
  real    :: CsurNbio  
  real    :: NsurCres

       NsurCres = Wr
       if(CNresmin > 0.) NsurCres = min(NsurCres, 1. / CNresmin)
       if(CNresmax > 0.) NsurCres = max(NsurCres, 1. / CNresmax)

       CsurNbio   =  amax1(awb+bwb*NsurCres,cwb)
       CsurNbio   =  amin1(CsurNbio,25.)
       Wb = 0.
       if(CsurNbio > 0.) Wb = 1./CsurNbio

       kres = amax1(0., akres + bkres * NsurCres)
       hres = amax1(0., 1. - ahres /(bhres*NsurCres + 1.))
       hres = amin1(1., hres)

       if (awb==bwb.and.bwb==cwb.and.cwb==akres.and.akres==bkres.and.bkres==ahres.and.ahres==bhres.and.bhres==0.) then
          call EnvoyerMsgHistorique(logger, MESSAGE_2101)
       endif
  return
end subroutine ResiduParamDec
end module ResiduParamDec_m