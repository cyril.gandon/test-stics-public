!*********************************************************************
!     Calcul des apports d'eau
!  version 3.3 18/03/98
! MODIF le 4/6/98 bug interception
! derniere modif 14/11/00
!*********************************************************************
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
! This program manages irrigation.
! - Stics book paragraphe 6.2, page 99
!
!! The amounts of water applied can be entered from an irrigation calendar or calculated by the model.
!!
!! In the latter case, the model automatically calculates water inputs so as to satisfy water requirements at the level of the RATIOL parameter:
!! the model triggers irrigation each time the stomatal stress index (SWFAC is less than RATIOL). Irrigation amounts (AIRG) are then calculated so as
!! to replenish the soil water reserve (HUR) to field capacity (HUCC) down to the rooting front (ZRAC) without exceeding the maximum dose authorised
!! by the irrigation system (DOSIMX). At the time of sowing, whatever the soil reserve status, a fixed value of about 20 mm (IRRLEV parameter) is supplied
!! to the crop if it has not rained, to enable germination.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c

!: ML - 29/10/12 - ajout variable dureehumec dans les arguments
module irrig_m
use messages
USE Plante, ONLY: AO, AS
use plant_utils
use Itineraire_Technique
implicit none
private
public :: irrig
contains
subroutine irrig(logger, n,nplt,nrecbutoir,P_codecalirrig,P_irrlev,swfac,P_ratiol,cumlracz,   &
                 zrac,hucc,hur,P_dosimx,P_doseirrigmin,exofac,P_codlocirrig,P_effirr,   &
                 lai,P_codeintercept,P_stemflowmax,P_kstemflow,parapluie,ipl,         &
                 P_nbplantes, surfSous, P_mouillabil, P_codeplante,     &
                 nap,airg,preciptmp,stem,precipEns,mouill,interpluie,           &
                 irrigprof,eaunoneffic,totpl,codeSWDRH,dureehumec,P_codedate_irrigauto ,n_datedeb_irrigauto,n_datefin_irrigauto)
  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: n  
  integer, intent(IN)    :: nplt  
  integer, intent(IN)    :: nrecbutoir  
  integer, intent(IN)    :: P_codecalirrig  ! // PARAMETER // automatic calculation of irrigation requirements: yes (2), no (1) // code 1/2 // PARTEC // 1 
  real,    intent(IN)    :: P_irrlev  ! // PARAMETER // amount of irrigation applied automatically on the sowing day when the model calculates irrigation, to allow germination // mm // PARAM // 1 
  real,    intent(IN)    :: swfac   ! // OUTPUT // Index of stomatic water stress  // 0-1
  real,    intent(IN)    :: P_ratiol  ! // PARAMETER // Water stress index below which we start an irrigation in automatic mode (0 in manual mode) // between 0 and 1 // PARTEC // 1 
  real,    intent(IN)    :: cumlracz   ! // OUTPUT // Sum of the effective root lengths  // cm root.cm -2 soil
  real,    intent(IN)    :: zrac   ! // OUTPUT // Depth reached by root system // cm
  real,    intent(IN)    :: hucc(int(zrac))  
  real,    intent(IN)    :: hur(int(zrac))  
  real,    intent(IN)    :: P_dosimx  ! // PARAMETER // maximum water amount of irrigation authorised at each time step (mode automatic irrigation) // mm day-1 // PARTEC // 1 
  real,    intent(IN)    :: P_doseirrigmin  ! // PARAMETER // minimal amount of irrigation // mm // PARTEC // 1 
  real,    intent(IN)    :: exofac   ! // OUTPUT // Variable for excess water // 0-1
  integer, intent(IN)    :: P_codlocirrig  ! // PARAMETER // code of irrigation localisation: 1= above the foliage, 2= below the foliage above the soil, 3 = in the soil // code 1/2/3 // PARTEC // 0 
  real,    intent(IN)    :: P_effirr  ! // PARAMETER // irrigation efficiency // SD // PARTEC // 1 
  real,    intent(IN)    :: lai                             ! (n)    // OUTPUT // Leaf area index (table) // m2 leafs  m-2 soil
  integer, intent(IN)    :: P_codeintercept  ! // PARAMETER // option of simulation rainfall interception by leafs: yes (1) or no (2) // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_stemflowmax  ! // PARAMETER // Maximal fraction of rainfall which flows out along the stems  // between 0 and 1 // PARPLT // 1 
  real,    intent(IN)    :: P_kstemflow  ! // PARAMETER // Extinction Coefficient connecting leaf area index to stemflow // * // PARPLT // 1 
  integer, intent(IN)    :: parapluie  
  integer, intent(IN)    :: ipl  
  integer, intent(IN)    :: P_nbplantes  ! // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0 
  real,    intent(IN)    :: surfSous(2)  
  real,    intent(IN)    :: P_mouillabil  ! // PARAMETER // maximum wettability of leaves // mm LAI-1 // PARPLT // 1 
!  integer, intent(IN)    :: P_locirrig  ! // PARAMETER // Depth of water apply (when irrigation is applied in depth of soil) // cm // PARTEC // 1
  character(len=3), intent(IN) :: P_codeplante  ! // PARAMETER // Name code of the plant in 3 letters // * // PARPLT // 0 
  integer, intent(IN)    :: codeSWDRH
  real,    intent(IN)    :: dureehumec

  integer, intent(IN) :: P_codedate_irrigauto ! // PARAMETER // option to activate the beginning and the ending date in case of automatic irrigation  : dates (1),stages (2), no (3) // code 1/2 //PARAMv6 // 1
  integer, intent(IN) :: n_datedeb_irrigauto  ! // PARAMETER // date of beginning automatic irrigations in julian day // code 1/2 //PARAMv6 // 1
  integer, intent(IN) :: n_datefin_irrigauto ! // PARAMETER // date of ending automatic irrigations in julian day // code 1/2 //PARAMv6 // 1

  integer, intent(INOUT) :: nap  
  real,    intent(INOUT) :: airg                            ! (n)    // OUTPUT // Daily irrigation // mm
  real,    intent(INOUT) :: preciptmp  
  real,    intent(INOUT) :: stem  
  real,    intent(INOUT) :: precipEns(2)  
  real,    intent(INOUT) :: mouill  
  real,    intent(INOUT) :: interpluie   ! // OUTPUT // Water intercepted by leaves // mm
  real,    intent(INOUT) :: irrigprof                       ! (P_locirrig)  
  real,    intent(INOUT) :: eaunoneffic  
  real,    intent(INOUT) :: totpl  

!: Variables locales
  real    :: mouillprec  !  
  real    :: mouillmax  
  integer :: iz  

  if (P_codecalirrig == 1) then  !irrigations automatiques

    !TODO : fonction calculAutomatiqueDesIrrigations
    !: Calcul automatique des irrigations entre semis et recolte
    if (n >= nplt .and. n < nrecbutoir) then
      ! PL: 2023/11/08: initialisation deja faite
      !if (n == nplt) nap = 0
      ! PL: 2023/11/08: Desactivation irrigation au semis/plantation
      ! ** apport minimal au semis pour permettre la germination
      !if (n == nplt .and. preciptmp < P_irrlev) then
      !  airg = P_irrlev - preciptmp
      !  nap = nap + 1
      !else

        ! DR 24/02/2015 si on est sans dates faut quand meme qu'on irrig
        if(((P_codedate_irrigauto == IRRIG_AUTO_DATE .or. P_codedate_irrigauto == IRRIG_AUTO_STAGE) .and.&
          (n >= n_datedeb_irrigauto .and. n <= n_datefin_irrigauto)) .or.&
          (P_codedate_irrigauto == IRRIG_AUTO_NO)) then
          if ((swfac >= P_ratiol .and. cumlracz > 0.) .or. preciptmp > 0. .or. cumlracz <= 0.) then
            airg = 0.
          else
            do iz = 1,int(zrac)
               airg = airg + hucc(iz) - hur(iz)
            end do
            airg = min(airg, P_dosimx)
            if (airg < P_doseirrigmin) airg = 0.
            ! pas d'irrigation si exces d'eau
            if (abs(airg).gt.1.0E-8 .and. exofac > 0.0) then
              airg = 0.0
            endif
            ! domi 11/09/07 si on est < dose minimal apportee alors pas d'apport
            if (airg >= P_doseirrigmin) nap = nap + 1
          endif
        endif
      !endif  ! => end apport eau semis/plantation
    endif
  endif

! ** calcul des apports d'eau (preciptmp) et cumul des irrigations
! *- introduction de l'interception de la pluie NB et MD le 27/9/97
! *- P_codlocirrig = 1 : irrigation sur-frondaison
! *- P_codlocirrig = 2 : irrigation sous-frondaison
! *- P_codlocirrig = 3 : irrigation en profondeur
! *- valeur de P_locirrig = profondeur de l'apport en entier (-10, -20)

!: Les precipitations soumises a l'interception comprennent les irrigations pour P_locirrig = 1
if (P_codlocirrig == 1) then
  ! DR et ML 28/08/2014
  ! on ajoute airg a preciptmp qu'une seule fois (des la premiere plante), sinon on comptabilise 2 fois chaque irrigation
  if(ipl.lt.2)then
    preciptmp = preciptmp + (airg * P_effirr)
  endif
endif

!: Interception de la pluie
!- P_codeintercept = 1 pour interception par le feuillage
if (lai > 0. .and. P_codeintercept == 1) then
  !: Calcul du stemflow
  stem = preciptmp * P_stemflowmax * (1.0 - exp(-P_kstemflow * lai))
  !: Effet parapluie pour cultures en rangs; par defaut parapluie = 1 si culture principale
  if (parapluie == 1 .and. ipl < P_nbplantes) then
    precipEns(AS) = preciptmp * surfSous(AS)
    precipEns(AO) = preciptmp * surfSous(AO) - stem
  else
    precipEns(AS) = 0.0
    precipEns(AO) = preciptmp - stem
  endif
  !: Le mouillage (du jour) precedent (non evapore ?)
  mouillprec = mouill
  mouillmax = P_mouillabil * lai

  !: On ne peut pas retenir + que la capacite des feuilles (mouillmax)
  mouill = min(mouillmax, precipEns(AO))

  !: ML - 29/10/12 - prise en compte de la duree d'humectation : si elle est positive on remplit les feuilles d'eau
  if(codeSWDRH.eq.1 .and. dureehumec > 0) mouill = mouillmax

    mouill = mouill + mouillprec
    if (mouill > mouillmax) then
      mouillprec = mouill - mouillmax
      mouill = mouillmax
    else
      mouillprec = 0.
    endif

        !: Les retombees qui ne sont pas retenues par les feuilles ?
        precipEns(AO) = precipEns(AO) - mouill
        interpluie =  mouill - mouillprec

        ! ** reconstitution de la pluie sous la culture
  else
    precipEns(AO) = 0.
    precipEns(AS) = preciptmp
    stem = 0.
  endif

  preciptmp = precipEns(AO) + precipEns(AS) + stem

  if (P_codlocirrig == 2) then
    ! DR et ML 28/08/2014
    ! on ajoute airg a preciptmp qu'une seule fois (des la premiere plante), sinon on comptabilise 2 fois chaque irrigation
    if(ipl.lt.2)then
      preciptmp = preciptmp + (airg * P_effirr)
    endif
  endif

  !: Irrigation en profondeur
  if (P_codlocirrig == 3) then
    irrigprof = airg * P_effirr
  endif

  ! debug NB 10/06/05 soustraction de mouillprec
  ! domi 16/09/05 on le conditionne a P_codeintercept = 1
  !      interpluie =  mouill-mouillprec

  !: Domi - 14/11/00: Calcul des irrigations non efficaces
  eaunoneffic = airg * (1.0 - P_effirr)

  ! Deplacement du test apres le calcul de preciptmp
  if (P_codeplante /= CODE_BARESOIL) then
    if (n == nplt .or. n == nplt+1) then
      totpl = totpl + preciptmp
    endif
    if (n == nplt+1 .and. abs(totpl).lt.1.0E-8) then
      call EnvoyerMsgHistorique(logger, MESSAGE_90)
    endif
  endif


return
end subroutine irrig
end module irrig_m
 
