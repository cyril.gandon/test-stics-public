! *-----------------------------------------------------------------------------------------------------------------------
! fertilization
! - Stics paragraphe 6.3, 6.3.1, 6.3.2, page 100-101
!!
!! The N inputs from mineral fertilizers can be applied either at the soil surface or at a given depth (LOCFERTI)
!! if the option 'localized fertilization' is activated (CODLOCFERTI =2).
!! We consider 8 different types of mineral fertilizers. As a simplification, urea is treated as an ammonium fertilizer
!! since its hydrolysis to ammonium carbonate is a very fast process (e.g. Recous et al, 1988; Hah, 2000).
! Stics paragraphe 6.3, 6.3.1, 6.3.2, page 100-101
! *-----------------------------------------------------------------------------------------------------------------------
module apportsNparEngraisMineraux_m
use perteng_m
implicit none
private
public :: apportsNparEngraisMineraux
contains
subroutine apportsNparEngraisMineraux(P_orgeng, P_voleng, P_deneng, anit, n, Vabso5, P_Vabs2, P_codlocferti, P_locferti, &
                                      P_pHminvol, P_pHmaxvol, P_pH, P_Xorgmax, P_codedenit, P_engamm, Nvoleng, Ndenit,   &
                                      Norgeng, QNvoleng, QNorgeng, QNdenit, Nhum, Nhuma, amm, nit, totapN)

  real,    intent(IN)    :: P_orgeng      !  maximal quantity of mineral fertilizer that can be organized in the soil (fraction for type 8) // kg.ha-1
  real,    intent(IN)    :: P_voleng      !  maximal fraction of mineral fertilizer that can be volatilized
  real,    intent(IN)    :: P_deneng      !  proportion of the mineral fertilizer that can be denitrified (useful if codenit not active)
  real,    intent(IN)    :: anit          !  Amount of fertilizer added on day n  // kg.ha-1.d-1
  integer, intent(IN)    :: n  
  real,    intent(IN)    :: Vabso5(5)     ! Maximal N uptake during the last 5 days
  real,    intent(IN)    :: P_Vabs2       ! N uptake rate for which fertilizer losses are divided by 2 // kg.ha-1.d-1
  integer, intent(IN)    :: P_codlocferti ! code of fertilisation localisation:  1: at soil surface, 2 = in the soil // code 1/2 // PARTEC // 0
  integer, intent(IN)    :: P_locferti    ! Depth of nitrogen injection (when fertiliser is applied at depth) // cm
  real,    intent(IN)    :: P_pHminvol    ! pH above which the fertilizer volatilisation is nil
  real,    intent(IN)    :: P_pHmaxvol    ! pH beyond which the fertilizer volatilisation is maximum
  real,    intent(IN)    :: P_pH          ! pH of mixing soil + organic amendments
  real,    intent(IN)    :: P_Xorgmax     ! maximal amount of immobilised N coming from the mineral fertilizer  // kg.ha-1
  integer, intent(IN)    :: P_codedenit   ! option to allow the calculation of denitrification :yes (1), no(2) // code 1/2
  real,    intent(IN)    :: P_engamm      ! proportion of ammonium in the fertilizer

  ! ici, ces variables de sortie pourraient etre mises en OUT plutot que INOUT
  real,    intent(INOUT) :: Nvoleng      ! Daily volatilisation of NH3-N from fertiliser // kg.ha-1.d-1
  real,    intent(INOUT) :: Ndenit       ! Daily denitrification of N from fertiliser or soil (if option  denitrification  is activated) // kg.ha-1.d-1
  real,    intent(INOUT) :: Norgeng      ! Daily organisation of N from fertiliser // kg.ha-1.d-1
  real,    intent(INOUT) :: QNvoleng     ! Cumulative volatilisation of NH3-N  from fertiliser // kg.ha-1
  real,    intent(INOUT) :: QNorgeng     ! Cumulative organisation of N from fertiliser // kg.ha-1
  real,    intent(INOUT) :: QNdenit      ! Cumulative denitrification of N from fertiliser or soil (if option  denitrification  is activated) // kg.ha-1
  real,    intent(INOUT) :: Nhum(1:10)   ! N immobilised from fertilizers by soil microbial biomass in the 10 first cm  // kg.ha-1
  real,    intent(INOUT) :: Nhuma        ! Amount of active N in the humus pool  // kg.ha-1
  real,    intent(INOUT) :: amm(1:max(1,P_locferti))  ! soil ammonium N  // kg.ha-1
  real,    intent(INOUT) :: nit(1:max(1,P_locferti))  ! soil nitrate N   // kg.ha-1
  real,    intent(INOUT) :: totapN       ! Cumulative amount of mineral N inputs coming from fertilizer and organic residues // kg.ha-1

!: Variables locales
  integer :: iz  
  real    :: appamm
  real    :: appnit
  real    :: Norgamm
  real    :: Norgnit  

      !if (anit == 0.) then
      if (abs(anit).lt.1.0E-8) then
        !  pas d'apport d'engrais mineral
        Nvoleng = 0.
        Norgeng = 0.
        Ndenit = 0.
      else
        ! apport d'engrais mineral (NH4 et NO3)
        appamm = anit * P_engamm
        appnit = anit - appamm

        ! devenir de l'engrais
        call perteng(P_orgeng, P_voleng, P_deneng, anit, n, Vabso5, P_Vabs2, P_codlocferti, P_pHminvol, P_pHmaxvol, &
                     P_pH, P_Xorgmax, P_codedenit, Nvoleng, Ndenit, Norgeng, QNvoleng, QNorgeng, QNdenit)

        ! partition de l'organisation aux depens de NH4 et NO3
        Norgamm = Norgeng * P_engamm
        Norgnit = Norgeng - Norgamm

! on suppose l'azote de l'engrais organise dans la biomasse microbienne du sol (incluse dans humus) sur 0-10 cm
        do iz=1,10
           Nhum(iz)= Nhum(iz)+ Norgeng/10.
        end do
           Nhuma = Nhuma + Norgeng

        ! repartition de l'engrais dans le sol
        if (P_codlocferti == 1) then
        ! a) cas d'un apport en surface
          amm(1) = amm(1) + appamm - Norgamm - Nvoleng
          nit(1) = nit(1) + appnit - Norgnit
          if (P_codedenit == 2) nit(1) = nit(1) - Ndenit

        else ! TODO: rajouter un test sur P_codlocferti (=2 ?) et surtout P_locferti > 0 & < nbCouchesSol
        ! b) cas d'un apport en profondeur
          amm(P_locferti) = amm(P_locferti) + appamm - Norgamm - Nvoleng
          nit(P_locferti) = nit(P_locferti) + appnit - Norgnit
          if (P_codedenit == 2) nit(P_locferti) = nit(P_locferti)- Ndenit
        endif

        ! cumul des entrees d'azote
        totapN = totapN  + anit
      endif

  return
end subroutine apportsNparEngraisMineraux
end module apportsNparEngraisMineraux_m
 
