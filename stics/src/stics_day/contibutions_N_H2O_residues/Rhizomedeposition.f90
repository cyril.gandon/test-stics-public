! -------------------------------------------------------------------------------------------------------
!  Simulates the CONTINUOUS addition of dead rhizomes to soil during the crop cycle (only for perennials)
!--------------------------------------------------------------------------------------------------------
module Rhizomedeposition_m
use messages
use ResiduParamDec_m, only: ResiduParamDec
implicit none
private
public :: Rhizomedeposition, Rhizome_radius
contains
subroutine Rhizomedeposition(logger, irhiz, dltaperennesen, dltaQNperennesen, awb, bwb, cwb, akres, bkres, ahres, bhres,  &
                             CNresmin, CNresmax, Wb, kres, hres, Cres, Nres)
  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: irhiz
  real,    intent(IN)    :: dltaperennesen
  real,    intent(IN)    :: dltaQNperennesen
  real,    intent(IN)    :: awb         ! parameter  of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // SD // PARAM // 1
  real,    intent(IN)    :: bwb         ! parameter of organic residues decomposition: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: cwb         ! Minimum ratio C/N of microbial biomass in the relationship: CsurNbio=awb+bwb/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: akres       ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // day-1 // PARAM // 1
  real,    intent(IN)    :: bkres       ! parameter of organic residues decomposition: kres=akres+bkres/CsurNres // g g-1 // PARAM // 1
  real,    intent(IN)    :: ahres       ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: bhres       ! parameter of organic residues humification: hres=1-ahres*CsurNres/(bhres+CsurNres) // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmin    ! minimum observed value of ratio C/N of organic residues  // g g-1 // PARAM // 1
  real,    intent(IN)    :: CNresmax    ! maximum observed value of ratio C/N of organic residues // g g-1 // PARAM // 1

  real,    intent(INOUT) :: Wb
  real,    intent(INOUT) :: kres
  real,    intent(INOUT) :: hres
  real,    intent(INOUT) :: Cres(1:irhiz)
  real,    intent(INOUT) :: Nres(1:irhiz)

!: Variables locales
  real    :: CCperenne
  real    :: CsurNperenne
  real    :: Capp
  real    :: Napp
  real    :: Wr
  integer :: iz

           CCperenne = 440.
           if (irhiz > 0.) then
              Capp = dltaperennesen * CCperenne / irhiz
              Napp = dltaQNperennesen / irhiz
           endif
           CsurNperenne = 1000.
           if (dltaQNperennesen >  1.e-8) CsurNperenne = Capp / Napp

           do iz=1,irhiz
             Cres(iz) = Cres(iz)+ Capp
             Nres(iz) = Nres(iz)+ Napp
           end do

           Wr = 0.
           if (CsurNperenne > 0.) Wr = 1./CsurNperenne
           call ResiduParamDec(logger, awb, bwb, cwb, akres, bkres, ahres, bhres, Wr, CNresmin, CNresmax, Wb, kres, hres)

   return
 end subroutine Rhizomedeposition
 
!--------------------------------------------------------------------
! - Calculation of the radius of a rhizome assimilated to a sphere
!--------------------------------------------------------------------

 Integer function Rhizome_radius(maperenne, densite)

  real,    intent(IN)    :: maperenne    ! masse de rhizomes (t/ha)
  real,    intent(IN)    :: densite      ! densite de rhizomes = densite de plantes (nb/m2)

!: Variable locale
  real    :: mavol  ! masse volumique du rhizome (g/cm3)

     ! Calcul du rayon du rhizome (suppose spherique) : R (cm)
     ! R = (236.m/nD)^1/3    m = maperenne (t/ha), n = densite (m-2), D = masse volumique (g/cm3)

          mavol = 0.80
          Rhizome_radius = 0
          if(maperenne > 0. .and. densite > 0.) Rhizome_radius = nint((236.* maperenne/densite/mavol)**(0.3333))
   return
end function Rhizome_radius
end module Rhizomedeposition_m
 
