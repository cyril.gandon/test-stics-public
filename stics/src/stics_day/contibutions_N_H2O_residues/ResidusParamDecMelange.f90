! --------------------------------------------------------------------------------------------------------
!! This program updates the decomposition parameters of organic residues after soil tillage
! Stics book  page 265-287
! *-------------------------------------------------------------------------------------------------------
module ResidusParamDecMelange_m
use iso_fortran_env, only: real32
implicit none
private
public :: ResidusParamDecMelange
contains
subroutine ResidusParamDecMelange(itrav2, ihum, nbResidus, awb, bwb, cwb, akres, bkres, ahres, bhres, Cres, Nres,  &
                                   CNresmin, CNresmax, Wb, kres, hres)
  implicit none

  integer, intent(IN)    :: itrav2              ! depth of soil tillage = proftrav (cm)
  integer, intent(IN)    :: ihum
  integer, intent(IN)    :: nbResidus           ! total number of residues
  real(real32),    intent(IN)    :: awb(nbResidus)      ! decomposition parameter : CsurNbio=awb+bwb.Wr // SD // PARAM // 1
  real(real32),    intent(IN)    :: bwb(nbResidus)      ! decomposition parameter : CsurNbio=awb+bwb.Wr // SD // PARAM // 1
  real(real32),    intent(IN)    :: cwb(nbResidus)      ! decomposition parameter : CsurNbio=awb+bwb.Wr // SD // PARAM // 1
  real(real32),    intent(IN)    :: akres(nbResidus)    ! decomposition parameter : kres=akres+bkres.Wr // d-1 // PARAM // 1
  real(real32),    intent(IN)    :: bkres(nbResidus)    ! decomposition parameter : kres=akres+bkres.Wr // d-1 // PARAM // 1
  real(real32),    intent(IN)    :: ahres(nbResidus)    ! decomposition parameter : hres=1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real(real32),    intent(IN)    :: bhres(nbResidus)    ! decomposition parameter : hres=1-ahres/(bhres.Wr+1) // g g-1 // PARAM // 1
  real(real32),    intent(IN)    :: Cres(ihum,nbResidus)
  real(real32),    intent(IN)    :: Nres(ihum,nbResidus)
  real(real32),    intent(IN)    :: CNresmin(nbResidus)  ! minimum observed value of ratio C/N of all residues // g g-1 // PARAM // 1
  real(real32),    intent(IN)    :: CNresmax(nbResidus)  ! maximum observed value of ratio C/N of all residues // g g-1 // PARAM // 1
  real(real32),    intent(INOUT) :: Wb(nbResidus)        ! N/C of the biomass
  real(real32),    intent(INOUT) :: kres(nbResidus)      ! decomposition rate of the organic residues
  real(real32),    intent(INOUT) :: hres(nbResidus)      ! humification rate of the organic residues

  !: VARIABLES LOCALES
  integer :: iz  
  integer :: ir  
  integer :: izmax  
  real    :: Cresid  
  real    :: Nresid  
  real    :: CsurNbio
  real    :: Wr
  real    :: NsurCres 

! On sort du programme si la profondeur de travail du sol est <= 1 cm
      if(itrav2 <= 1) return
! Calcul de Wr (rapport N/C) moyen de chaque residu sur la profondeur izmax
      do ir = 1,nbResidus
          Cresid = 0.
          Nresid = 0.
! Fabien octobre 2019 : on remplace itrav2 par ihum comme dans le trunk
          !izmax = itrav2
          izmax = ihum
          if(ir < 11) izmax = 1
          do iz = 1,izmax
            Cresid = Cresid + Cres(iz,ir)
            Nresid = Nresid + Nres(iz,ir)
          end do
          Wr = 0.
          if (Cresid > 0.) Wr = Nresid/Cresid

! Actualisation des parametres de decomposition de chaque residu en fonction de Wr
          NsurCres = Wr
          if(CNresmin(ir) > 0.) NsurCres = amin1(NsurCres, 1. / CNresmin(ir))
          if(CNresmax(ir) > 0.) NsurCres = amax1(NsurCres, 1. / CNresmax(ir))
   
          CsurNbio   =  amax1(awb(ir)+bwb(ir)*NsurCres,cwb(ir))
          CsurNbio   =  amin1(CsurNbio,25.)
          Wb(ir) = 0.
          if(CsurNbio > 0.) Wb(ir) = 1./CsurNbio

          kres(ir) = amax1(0., akres(ir) + bkres(ir)*NsurCres)
          hres(ir) = amax1(0., 1. - ahres(ir) /(bhres(ir)*NsurCres + 1.))
          hres(ir) = amin1(1., hres(ir))
      end do
return

end subroutine ResidusParamDecMelange
end module ResidusParamDecMelange_m
 
