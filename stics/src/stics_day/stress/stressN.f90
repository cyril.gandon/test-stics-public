!***************************************************************
! calcul de l'absorption d'azote par le principe offre/demande
! demande = courbe de dilution "maximale"
! offre = cumul sur le profil de sol de l'offre elementaire
!         au niveau racinaire estimee comme le minimum entre
!         l'offre physique (migration du nitrate du sol a la
!         racine) et la capacite d'absorption (double MM)
!
! fluxrac : flux d'absorption maximal possible par couche  de sol
! fluxsol : flux de transfert sol --> racine   par couche  de sol
! flurac  : flux cumule d'absorption sur l'ensemble du profil
! flusol  : flux cumule sol --> racine sur l'ensemble du profil
! offrN     = minimum(fluxsol, fluxrac)
! cumoffrN  = cumul de l'offre sur l'ensemble du profil
! masecdil  = matiere seche determinant les besoins (t/ha)
! P_masecNmax = matiere seche a laquelle la dilution d'azote commence
! QNplantenp  = quantite d'azote dans la plante (kg/ha)
! NC        = concentration critique en azote
! CNplante  = concentration d'azote dans la plante
! inn       = indice de satisfaction des besoins azotes
! inns      = inn borne a 1
!****************************************************************
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
!! This module calculates Nitrogen deficiency.
! - Stics book paragraphe 3.4.2, page 60
!
!! The nitrogen status of a crop can be characterized using the concept of 'dilution curves' which relate the N concentration in plant shoots to
!! the dry matter accumulated in them (Lemaire and Salette, 1984; Greenwood et al., 1991). For a given species, a 'critical dilution curve' can be defined,
!! which can be used to make a diagnosis of nitrogen nutrition (Justes et al., 1994; Lemaire et Gastal., 1997): plants below this curve are or have been
!! N deficient, whereas plants above the curve have an optimal growth, i.e. are not limited by nitrogen. The critical dilution curve is the basis for defining
!! a nitrogen nutrition index (inn) which is the ratio of the actual nitrogen concentration (CNplante, in % of dry matter) to the critical concentration (NC)
!! corresponding to the same biomass (masecabso, in t ha-1).
!!
!! However we are aware of an important limitation in the inn dynamics, as for example in the case of the nitrogen reserve available in perennial organs
!! (e.g. grapevine). Consequently we propose an alternative stress variable corresponding to the nitrogen input flux relative to the critical one as
!! proposed by Devienne-Barret et al. (2000). It is a kind of instantaneous inn named inni relying on the daily accumulations of nitrogen (VabsN) and
!! nitrogen dependent biomass (deltabso).
!!
!! All nitrogen stress indices accept INNmin or INNimin as the floor value for respectively the 'INN' and the 'INNI' options.
!! By definition the inns index corresponds to the inn between INNmin and 1.  The innlai and innsenes indices are defined by point [1,1] and by
!! points [INNmin, innturgmin] and [INNmin, innsen], respectively.
!! Such a parameterisation allows the effect of nitrogen deficiency on photosynthesis to be differentiated from that on leaf expansion.
!! In practice it seems that these two functions react very similarly and innturgmin is similar to INNmin, while innsen is greater, indicating that the
!! plants accelerate their senescence later than their growth decrease, just as for water stress. A commonly accepted value for INNmin is 0.3 and
!! INNimin is 0.0.
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
module stressN_m
use plant_utils
implicit none
private
public :: stressN
contains
subroutine stressN(masecdil,P_masecnp0,P_adilmax, & ! IN
                   P_masecNmax,P_bdilmax,masecpartiel,magrain,absodrp,P_codeplisoleN,P_masecmeta,adilI, &
                   bdilI,P_adil,P_bdil,adilmaxI,bdilmaxI,dNdWcrit,deltabso,P_codeINN,P_INNmin,P_INNimin,&
                   P_innturgmin,P_innsen,P_QNpltminINN,absotot,QNplantenp,QNplanteres,CNplante,inn,inni,&
                   inns,innlai,innsenes,P_code_acti_reserve,dltaremobilN,absoaer,absorac,QNplante,      &
                   P_codeplante)

  implicit none

  real,    intent(IN)    :: masecdil  
  real,    intent(IN)    :: P_masecnp0  ! // PARAMETER // initial biomass // t ha-1 // INIT // 1
  real,    intent(IN)    :: P_adilmax  ! // PARAMETER // Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // N% MS // PARPLT // 1 
  real,    intent(IN)    :: P_masecNmax  ! // PARAMETER // Aerial biomass  on and after there is nitrogen dilution (critical and maximal curves) // t ha-1 // PARPLT // 1 
  real,    intent(IN)    :: P_bdilmax  ! // PARAMETER // Parameter of the maximum curve of nitrogen needs [Nplante]=P_adilmax MS^(-P_bdilmax) // SD // PARPLT // 1 
  real,    intent(IN)    :: masecpartiel  
  real,    intent(IN)    :: magrain  
  real,    intent(IN)    :: absodrp  
  integer, intent(IN)    :: P_codeplisoleN  ! // PARAMETER // code for N requirement calculations at the beginning of the cycle: dense plant population (1), isolated plants (2, new formalisation) // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_masecmeta  ! // PARAMETER // biomass of the plantlet supposed to be composed of metabolic nitrogen // t ha-1 // PARPLT // 1
  real,    intent(IN)    :: adilI  
  real,    intent(IN)    :: bdilI  
  real,    intent(IN)    :: P_adil  ! // PARAMETER // Parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // N% MS // PARPLT // 1 
  real,    intent(IN)    :: P_bdil  ! // PARAMETER // parameter of the critical curve of nitrogen needs [Nplante]=P_adil MS^(-P_bdil) // SD // PARPLT // 1 
  real,    intent(IN)    :: adilmaxI  
  real,    intent(IN)    :: bdilmaxI  
  real,    intent(IN)    :: dNdWcrit  
  real,    intent(IN)    :: deltabso  
  integer, intent(IN)    :: P_codeINN  ! // PARAMETER // option to compute INN: cumulated (1), instantaneous (2)  // code 1/2 // PARPLT // 0 
  real,    intent(IN)    :: P_INNmin  ! // PARAMETER // Minimum value of INN authorised for the crop // SD // PARPLT // 1 
  real,    intent(IN)    :: P_INNimin  ! // PARAMETER // INNI (instantaneous INN) corresponding to P_INNmin // SD // PARPLT // 1 
  real,    intent(IN)    :: P_innturgmin  ! // PARAMETER // parameter of the nitrogen stress function active on leaf expansion (INNLAI), that is a bilinear function passing by the point of coordinate (P_innmin, P_innturgmin) // SD // PARPLT // 1 
  real,    intent(IN)    :: P_innsen  ! // PARAMETER // parameter of the nitrogen stress function active on senescence (innnsenes), bilinear function of the INN using the point (P_innmin, P_innsen) // SD // PARPLT // 1 
  real,    intent(IN)    :: P_QNpltminINN  ! // PARAMETER // minimal amount of nitrogen in the plant allowing INN computing // kg ha-1 // PARAM // 1
  real,    intent(IN)    :: absotot

  real,    intent(INOUT) :: QNplantenp      ! // OUTPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1
  real,    intent(INOUT) :: QNplanteres  
  real,    intent(INOUT) :: CNplante      ! // OUTPUT // Nitrogen concentration of entire plant  // %
  real,    intent(INOUT) :: inn      ! // OUTPUT // Nitrogen nutrition index (satisfaction of nitrogen needs ) // 0-1
  real,    intent(INOUT) :: inni  
  real,    intent(INOUT) :: inns      ! // OUTPUT // Index of nitrogen stress active on growth in biomass // P_innmin to 1
  real,    intent(INOUT) :: innlai      ! // OUTPUT // Index of nitrogen stress active on leaf growth // P_innmin to 1
  real,    intent(INOUT) :: innsenes      ! // OUTPUT // Index of nitrogen stress active on leaf death // P_innmin to 1

  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: dltaremobilN
  real,    intent(IN)    :: absoaer
  real,    intent(IN)    :: absorac
  real,    intent(IN)    :: QNplante
  ! Loic Fervier 2021
  character(len=3), intent(IN) :: P_codeplante

!: Variables locales
  real :: masecabso  !  
  real :: NC  !  
  real :: NCmax  !
  real :: dNdWc  !  
  real :: dQNc  

    !DR 09/08/2019 j'ajoute un flag pour faire des ecritures de debug
    logical :: dbg_strN
    dbg_strN=.FALSE.
    
    
      if (masecdil <= 0.) return

 ! Modifs Bruno et Loic fevrier 2013 : lutte pour la modularisation !
 ! *********************************
 ! les calculs suivants sont deplaces dans la routine absoN

      ! *-------------------------------------------------------------------* c
      ! * Calcul des teneurs en azote et des indices de nutrition azotee * c
      ! *-------------------------------------------------------------------* c
      ! variable VabsN necessaire pour INNinstantane NB le 07/04/05
      ! domi 16/09/05 affrenod,Qfix passes en AO/AS
  !    vabsN = abso + offrenod
  !    QNplantenp = QNplante_prec + VabsN

      ! ajout des remobilisations azotees venant des reserves
  !    QNplantenp = QNplantenp + dltaremobilN - dltarestempN

  !    if (P_codelegume == 2) then
  !      Qfix = Qfix + offrenod
  !    endif
! Fin modifs
if(dbg_strN)  write(5555,*)'P_masecnp0',P_masecnp0,'QNplantenp',QNplantenp
      ! ** quantite minimale d'azote = azote contenu dans les grains
      ! --     CNsemence = 2.0
      ! --     QNplantemin = CNsemence*P_pgrainmaxi*densite*10000/1000/100
      ! --     QNplantenp = max(QNplantenp,QNplantemin)

      ! ** pour les cultures plantees (P_masecnp0>0) il faut ajouter QNplanteres   NB le 23/4/98
      QNplanteres = P_masecnp0 * P_adilmax * 10. * P_masecNmax**(-P_bdilmax)
      ! Loic Oct 2020 : on prend l'azote total comme pour la v9
      ! Loic Fevrier 2021 : et bah non et bah si, enfin ca depend pour qui...
      if (P_code_acti_reserve == 1) then
          CNplante = QNplantenp / (masecpartiel * 10.)
      else
        if (P_codeplante == CODE_VINE) then
          CNplante = QNplantenp / (masecpartiel * 10.)
        else
          CNplante = QNplante / (masecpartiel * 10.)
        endif
      endif
      if(dbg_strN)  write(5555,*)'QNplanteres',QNplanteres,'CNplante',CNplante

      ! ** bornage de CNplante pour ecriture             bizarre !
      if (CNplante > P_adilmax) CNplante = P_adilmax

      ! ** concentration critique en azote (NC)
      masecabso = masecdil - (magrain / 100.)  + (absodrp * magrain / 100.)
      if (masecabso <= P_masecNmax) then

        if (P_codeplisoleN == 2) then
          if (masecabso <= P_masecmeta) masecabso = P_masecmeta
          NC = adilI * masecabso**(-bdilI)
          NCmax = adilmaxI * masecabso**(-bdilmaxI)
          dNdWc = 10. * adilI * (1. - bdilI) * masecabso**(-bdilI)
        else
          NC = dNdWcrit
          NCmax = P_adilmax * P_masecNmax**(-P_bdil)
          dNdWc = 10. * P_adil * (1. - P_bdil) * P_masecNmax**(-P_bdil)
        endif
      else
        NC = P_adil * masecabso**(-P_bdil)
        NCmax = P_adilmax * masecabso**(-P_bdil)
        dNdWc = 10. * P_adil* (1. - P_bdil) * masecabso**(-P_bdil)
      endif

      ! ** indice de nutrition azotee et indice de stress azote cumule : INN
      if (NC > 0.) then
        inn = CNplante / NC
        inn = min(inn, NCmax / NC)
        if(dbg_strN)  write(5555,*)'inn',inn
      else
        inn = 1.
      endif
      ! ** indice de stress azote instantane INNi le 07/04/05
      ! calcul de la vitesse d'absorption critique pour l'INNi
      ! Ajout Loic Septembre 2016: on ajoute l'azote remobilise pour les plantes perennes. De plus, on
      ! ne prend en compte que l'azote absorbe alloue aux organes aeriens
      dQNc =  deltabso * dNdWc
      if (dQNc > 0.) then
   !     inni = absotot / dQNc
        inni = (absotot - absorac) / dQNc
      if(dbg_strN)  write(5555,*)'absotot ',absotot, 'absorac',absorac
      if(dbg_strN)  write(5555,*)'1a. inni ',inni,(absotot - absorac),dQNc
      else
        inni = 1.0
      if(dbg_strN)  write(5555,*)'2a. inni ',inni
      endif
      if (P_code_acti_reserve == 1) then
        if (dQNc > 0) then
            inni = (absoaer + dltaremobilN) / dQNc
            if(dbg_strN)  write(5555,*)'1. inni ',inni
        else
            inni = 1.0
             if(dbg_strN)  write(5555,*)'2. inni ',inni
        endif
      endif

      ! ** choix de l'inn actif et calcul de inns qui agit sur RUE   (E. Justes)
      if (P_codeINN == 1) then
        inns = min(1., inn)
         if(dbg_strN)   write(5555,*)'1 inns inn',inns,inn
      else
        inns = min(1., inni)
        inns = (1. - P_INNmin) / (1. - P_INNimin) * inns + ((P_INNimin - P_INNmin) / (P_INNimin - 1.))
         if(dbg_strN)  write(5555,*)'2 inns inni',inns,inni
      endif
      inns = max(P_INNmin, inns)

      !: Calcul de innlai qui agit sur croissance foliaire
      innlai = (P_innturgmin - 1.) / (P_INNmin - 1.) * inns + (1. - (P_innturgmin - 1.) / (P_INNmin - 1.))
      innlai = min(1., innlai)
      innlai = max(P_INNmin, innlai)
         if(dbg_strN)   write(5555,*)'innlai',innlai


      !: Calcul de innsenes qui agit sur senescence foliaire
      innsenes = (P_innsen - 1.) / (P_INNmin - 1.) * inns + (1. - (P_innsen - 1.) / (P_INNmin - 1.))
      innsenes = min(1., innsenes)
      innsenes = max(P_INNmin, innsenes)
      
      if(dbg_strN)  write(5555,*)'calcul innsenes',P_innsen,P_INNmin,inns,P_innsen,'innsenes',innsenes
       
      !: Pour eviter l'instabilite de l'inn au depart
      if (QNplantenp < P_QNpltminINN) then
        inns     = 1.
        innlai   = 1.
        innsenes = 1.
         if(dbg_strN)  write(5555,*)'***',QNplantenp, P_QNpltminINN
      endif

! DR 22/01/2019
! pour comparaison trunk perennes Flax, je desactive les stress 
!      inns=1.0
!      innsenes=1.0
return
end subroutine stressN
end module stressN_m
 
