! = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == =
! DR le 28/10/05 on sort le calucul de l'azote grain et fruit qui etaient dans
!    grain.for et fruit.for
! ** calcul de l'azote dans les grains
! = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == = == =  =
! domi 27/10/05 on deplace ca dans stressN sinon on a pas la valuer de QNplantenp qui est calculee dans stressn
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This module calculates the biochemical composition of the harvested organ
! - Stics book paragraphe 4.3.2, page 48-49
!
!! The quantity of nitrogen in harvested organs, both for determinate and indeterminate species (QNgrain), is an increasing proportion (irazo) of the
!! quantity of nitrogen in the biomass (QNplante): the concept of the harvest index is extended to nitrogen, using the parameter vitirazo.
!!
!! Obviously, as for carbon, the grain/fruit nitrogen filling can be affected by thermal stress which requires a daily calculation. The temperature effect
!! on nitrogen grain filling is assumed to be the same as for carbon. The nitrogen harvest index is assumed to be limited to a value calculated using the
!! carbon parameters (irmax and viticarb).
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c
module Ngrain_m
implicit none
private
public :: Ngrain
contains
subroutine Ngrain(n,ndrp,nrec,P_vitirazo,irazo_veille,nmat,dltags,QNplantenp_veille,    &
                  QNplantenp,pgrain,nbgraingel,magrain,               &
                  irazo,QNgrain,CNgrain,P_code_acti_reserve,P_irazomax)

  integer, intent(IN)    :: n  
  integer, intent(IN)    :: ndrp  
  integer, intent(IN)    :: nrec  
  real,    intent(IN)    :: P_vitirazo  ! // PARAMETER // Rate of increase of the nitrogen harvest index // g grain g plant -1 day-1 // PARPLT // 1 
  real,    intent(IN)    :: irazo_veille  
  integer, intent(IN)    :: nmat  
  real,    intent(IN)    :: dltags      ! // OUTPUT // Growth rate of the grains  // t ha-1.j-1
  real,    intent(IN)    :: QNplantenp_veille
  real,    intent(IN)    :: QNplantenp      ! // OUTPUT // Amount of nitrogen taken up by the plant  // kgN.ha-1
  real,    intent(IN)    :: pgrain  
  real,    intent(IN)    :: nbgraingel  
  !DR 04/10/2021 j'enleve les 2 parametres devenu inutiles depuis la creation de P_irazomax
!  real,    intent(IN)    :: P_irmax  ! // PARAMETER // Maximum harvest index // SD // PARPLT // 1
!  real,    intent(IN)    :: P_vitircarb  ! // PARAMETER // Rate of increase of the carbon harvest index // g grain g plant -1 day-1 // PARPLT // 1
  real,    intent(IN)    :: magrain  
  real,    intent(INOUT) :: irazo      ! // OUTPUT // Nitrogen harvest index  // gN grain gN plant-1
  real,    intent(INOUT) :: QNgrain      ! // OUTPUT // Amount of nitrogen in harvested organs (grains / fruits) // kg ha-1
  real,    intent(INOUT) :: CNgrain      ! // OUTPUT // Nitrogen concentration of grains  // %
  integer, intent(IN)    :: P_code_acti_reserve
  real,    intent(IN)    :: P_irazomax  ! // PARAMETER // Maximum nitrogen harvest index // SD // PARPLT // 1
!: Variables locales
  real :: dltazo
! DR 27/0/ irazomax est devenu un parametre plante
!  real :: irazomax

      if (ndrp == 0 .or. (nrec > 0 .and. n > nrec) .or. magrain == 0) then
        irazo = 0.
! Ajout Loic Mars 2017
! Loic aout 2019 : suite vigne v9 vs vX
        if (P_code_acti_reserve == 1) then
           QNgrain = 0
        endif
        return
      endif

! Modif Bruno fevrier 2018 pour forcer QNgrain a rester stable le jour de recolte (la plante est morte)
!     if (n > ndrp) then
      if (n > ndrp .and. n.ne.nrec) then
        irazo = P_vitirazo * (n - ndrp + 1)
        if (nmat == 0 .or. n == nmat) then
           if (dltags > 0.) then
              dltazo = irazo * QNplantenp - irazo_veille * QNplantenp_veille
           else
              dltazo = 0.
           endif
    ! nb le 14022006 on eleve l'azote des grains geles
           QNgrain = QNgrain + dltazo - ((pgrain * nbgraingel) * CNgrain * 0.1)

! DR 15/12/2021 on a une division par 0 si on a QNplantenp=0...
           if (QNplantenp > 0.) then
                 irazo = QNgrain / QNplantenp
           else
                 irazo = 0.
           endif

    ! Bruno juin 2014 il faut que irazo soit toujours < 1
! DR 27/09/2021 irazomax est devenu un parametre plante
!           irazomax = amin1(1., P_irmax / P_vitircarb * P_vitirazo)
           if ( irazo > P_irazomax) then
              irazo = P_irazomax
              QNgrain  = QNplantenp * irazo
           endif
           if (magrain > 0.) then
               CNgrain =  QNgrain / magrain * 10.
           else
           ! dr 14/12/2021 pour ne pas avoir de CNgrain � NaN
               CNgrain = 0.
           endif
        endif
      endif
return
end subroutine Ngrain
end module Ngrain_m
 
