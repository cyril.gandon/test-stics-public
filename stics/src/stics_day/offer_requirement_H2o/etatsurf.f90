! ************************************************************************ c
! * Etat de surface du sol en fonction de la presence d'un mulch vegetal * c
! * Florent Maraux, E. Scopel et al.,N. Brisson                          * c
! ************************************************************************ c

! ** parametres :
! *- decomposmulch : taux de decomposition
! *- P_ruisolnu : proportion d'eau ruissellee en sol nu
! *- P_qmulchruis0 : quantite de mulch a partir de laquelle
! *-               le ruissellement est nul
! *- P_pminruis : pluie minimale pour ruissellement
! *- P_mouillabilmulch : reserve maxi du mulch en mm/(t.ha)
! *- P_kcouvmlch : coefficient d'extinction du mulch
! *- qmulch0 = quantite de mulch le jour de l'apport
! *- nappmulch = jour de l'apport du mulch

! ** variables :
! *- qmulch = quantite de mulch vegetal en t/ha
! *- ruisselsurf : ruissellement du a l'etat de surface
! *- mouillmulch  : eau stockee dans le mulch
! *- Emulch : evaporation de l'eau du mulch
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! Interception of water by mulch
! - Stics book paragraphe 6.4.3.a, page 109-110
!
!! This module calculates the interception of water by mulch
!! The maximum water reserve of the plant mulch (mouillmulch) is defined as being proportional to its quantity (qmulch), involving the mulch-dependent
!! parameter of wettability (mouillabilmulch) that can range between 0.22 and 0.38 mm t-1 ha (Scopel et al., 1998). The amount of water retained is limited
!! by the incident rainfall minus the surface run-off.
!! The amount of water directly evaporated from the mulch (Emulch) can be calculated in two ways, using either the reference evapotranspiration given in
!! the weather input file or the raw weather variables including wind speed and air humidity:
!       -   In the first case, it is assumed that the water contained in mulch evaporates in the same way as from a grass canopy, according to a resistance/height
!!           compensation phenomenon. This last concept corresponds to the "extinction of energy at the soil level" by the vegetation (as is the case for soil).
!!           If the extin parameter is not active (because the radiation intercepted by the canopy is calculated with the radiation transfer model and not by the
!!           Beer law approach), the value is recalculated and varies depending on the crop geometry and the quality of radiation.
!       -   In the second case the Shuttleworth and Wallace formalisation is applied (see module shutwall.f90), and Emulch evaporates in the same way as
!!           free water located at the soil level and receiving energy. It takes account of the proportion of soil covered by the mulch (couvermulch).
!! In both cases Emulch is limited by the amount of water intercepted by the vegetal mulch, mouillmulch.
! ------------------------------------------------------------------------------------------------------------------------------------------------------------* c
module etatsurf_m
implicit none
private
public :: etatsurf
contains
subroutine etatsurf(stemflowTot, P_pminruis, P_ruisolnu, P_qmulchruis0, P_mouillabilmulch, P_kcouvmlch, P_codebeso, &
                    P_codelaitr, tetp, delta, laiTot, tauxcouv, qmulch, ruisselsurf, precip, mouillmulch,           &
                    intermulch, couvermulch, Emulch, P_penterui )
  implicit none

  real,    intent(IN)    :: stemflowTot     ! somme des stemflow des plantes  
  real,    intent(IN)    :: P_pminruis  ! // PARAMETER // Minimal amount of precipitation to start a drip  // mm day-1 // PARAM // 1 
  real,    intent(IN)    :: P_ruisolnu  ! // PARAMETER // fraction of drip rainfall (by ratio at the total rainfall) on a bare soil  // between 0 and 1 // PARSOL // 1 
  real,    intent(IN)    :: P_qmulchruis0     ! (P_typemulch)   // PARAMETER // Amount of mulch to annul the drip // t ha-1 // PARAM // 1
  real,    intent(IN)    :: P_mouillabilmulch ! (P_typemulch)   // PARAMETER // maximum wettability of crop mulch // mm t-1 ha // PARAM // 1
  real,    intent(IN)    :: P_kcouvmlch       ! (P_typemulch)   // PARAMETER // Extinction Coefficient reliant la quantite de paillis vegetal au taux de couverture du sol // * // PARAM // 1
  integer, intent(IN)    :: P_codebeso        !  => de la plante principale   // PARAMETER // option computing of water needs by the k.ETP (1) approach  or resistive (2) approach // code 1/2 // PARPLT // 0
  integer, intent(IN)    :: P_codelaitr       !  => de la plante principale   // PARAMETER // choice between soil cover or LAI calculation // code 1/2 // PARPLT // 0
  real,    intent(IN)    :: tetp            !    // OUTPUT // Efficient potential evapotranspiration (entered or calculated) // mm day-1
  real,    intent(IN)    :: delta  
  real,    intent(IN)    :: laiTot          ! somme des LAIs des plantes  
  real,    intent(IN)    :: tauxcouv        !    // OUTPUT // Cover rate // SD
  real,    intent(IN)    :: P_penterui      ! // PARAMETER // runoff coefficient taking account for plant mulch // SD // PARSOL // 10
  real,    intent(IN) :: qmulch   ! // Quantity of plant mulch // t.ha-1
  real,    intent(OUT) :: ruisselsurf   ! // OUTPUT // Surface run-off // mm
  real,    intent(INOUT) :: precip   ! // OUTPUT // Daily amount of water (precipitation + irrigation)   // mm day-1
  real,    intent(INOUT) :: mouillmulch  
  real,    intent(OUT) :: intermulch   ! // OUTPUT // Water intercepted by the mulch (vegetal) // mm
  real,    intent(OUT) :: couvermulch   ! // OUTPUT // Cover ratio of mulch  // 0-1
  real,    intent(OUT) :: Emulch   ! // OUTPUT // Direct evaporation of water intercepted by the mulch // mm

!: Variables locales
  real    :: fruis
  real    :: excesmouillmulch  

      ruisselsurf = 0.
! *- le ruissellement ne concerne pas la pluie qui s'ecoule par stemflow
      if (precip - stemflowTot > P_pminruis) then
         if (qmulch <= 0.1) then
             fruis = P_ruisolnu
         else
            fruis = min(P_penterui * (P_qmulchruis0 - qmulch), P_ruisolnu)
         endif
         fruis = max(fruis, 0.)
         if (qmulch <= 0.) fruis = P_ruisolnu
         ruisselsurf = fruis * (precip - stemflowTot - P_pminruis)
      endif
      precip = precip - ruisselsurf

! DR 26/07/2011 le bilan eau est mal equilibre si on a du mulch et si on a fait un travail du sol ensuite
      if(qmulch <= 0.) then
         couvermulch = 0.
         intermulch = 0.
         return
      endif

      ! *- la quantite d'eau stockable dans le mulch est P_mouillabilmulch  (mm/t.ha)
      mouillmulch = precip + mouillmulch
      ! calcul de l'exces d'eau que ne peut retenir le paillage que l'on reaffectera a precip
      excesmouillmulch = max(0., mouillmulch - (P_mouillabilmulch * qmulch))

      mouillmulch = min(mouillmulch, P_mouillabilmulch * qmulch)

      ! intermulch est egal a l'interception le jour n, de la pluie par le mulch.
      intermulch = max(0., precip - excesmouillmulch)

      !: La pluie qui arrive jusqu'au sol et n'est pas retenue par le mulch
      precip = excesmouillmulch

      !: Taux de couverture du mulch
      couvermulch = 1. - exp(-P_kcouvmlch * qmulch)
      !: On fait evaporer l'eau interceptee par le mulch.
      !- On suppose que le pouvoir evaporant de l'air pour une surface d'eau libre
      !  est egal a ETP (Penman dans le cas ou codeso = 1, sinon S&W)

      if (P_codebeso == 1) then
         if (P_codelaitr == 1) then
            Emulch = tetp * exp(-delta * laiTot) * couvermulch
         else
            Emulch = tetp * (1 - tauxcouv) * couvermulch
         endif
         Emulch = min(mouillmulch, Emulch)
         mouillmulch = max(mouillmulch - Emulch, 0.)
      endif

return
end subroutine etatsurf
end module etatsurf_m
 
