! daily loop
! The STICS model is organised into modules (fig. 1.1, page 18),
! each module is composed of sub-modules dealing with specific mechanisms.
! A first set of 3 modules deals with ecophysiology of above-ground plant parts (phenology, shoot growth, yield formation).
! A second set of 4 modules deals with soil functions interacting with roots (root growth, water balance, nitrogen balance, soil transfers)
! The crop management module deals with interactions between the applied techniques and the soil-crop system.
! The microclimate module simulates the combined effects of climate and water balance on the temperature and air humidity within the canopy.
!!
! Each module includes options that can be used to extend the scope with which STICS can be applied to various crop systems.  These options relate to ecophysiology and to crop management, for example:
! -   competition for assimilate between vegetative organs and reserve organs (hereafter referred to as trophic competition);
! -   considering the geometry of the canopy when simulating radiation interception;
! -   the description of the root density profile;
! -   using a resistive approach to estimate the evaporative demand by plants;
! -   the mowing of forage crops;
! -   plant or plastic mulching under vegetation
!!
! some options depend on data availability.  For example, the use of a resistive model is based on availability of additional climatic driving variables: wind and air humidity.
module Stics_Jour_m
USE Stics
USE Plante
USE Root
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE Module_AgMIP
use messages
USE dates_utils, only: is_leap_year, day_number
use Rhizomedeposition_m, only: Rhizome_radius
use ClimatSousAbri_m, only: ClimatSousAbri
use tassesemisrecolte_m, only: tassesemisrecolte
use stressN_m, only: stressN
use cumaoetas_m, only: cumaoetas
use photpd_m, only: photpd
use Ecriture_Rapport_m, only: Ecriture_Rapport
use plant_death_m, only: plant_death
use laidev_m, only: laidev
use croissance_m, only: croissance
use allocation_m, only: allocation
use detassement_m, only: detassement
use decisionsemis_m, only: decisionsemis
use decisionrecolte_m, only: decisionrecolte
use shutwall_m, only: shutwall
use caltcult_m, only: caltcult
use humheure_m, only: humheure
use humcouv_m, only: humcouv
use tempsol_m, only: tempsol
use surface_wetness_duration_relative_humidity_m, only: surface_wetness_duration_relative_humidity
use apports_m, only: apports
use etatsurf_m, only: etatsurf
use volatorg_m, only: volatorg
use mineral_m, only: mineral
use nitrif_N2O_m, only: nitrif_N2O
use denit_N2O_m, only: denit_N2O
use besoinsEnEauDeLaPlante_m, only: besoinsEnEauDeLaPlante
use offrnodu_m, only: offrnodu
use bNpl_m, only: bNpl
use repartirN_m, only: repartirN
use offreN_m, only: offreN
use absoN_m, only: absoN
use lixiv_m, only: lixiv
use transpi_m, only: transpi
use majNsol_m, only: majNsol
use stressEau_m, only: stressEau
use Ngrain_m, only: Ngrain
use excesdeau_m, only: excesdeau
use Stics_Jour_after_m, only: Stics_Jour_after
use daily_output_m
use GestionDesCoupes_m, only: GestionDesCoupes
use dynamictalle_new_m, only: dynamictalle_new
implicit none
private
public :: Stics_Jour
contains
subroutine Stics_Jour(logger,sc,pg,p,r,itk,soil,c,sta,t,ag)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc
  type(Parametres_Generaux_), intent(INOUT) :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil
  type(Climat_),              intent(INOUT) :: c
  type(Station_),             intent(INOUT) :: sta  
  type(Stics_Transit_),       intent(INOUT) :: t

  type(AgMIP_),               intent(INOUT) :: ag
!! variables locales
  integer :: n
  integer :: ihum
  integer :: i
  integer :: iz, izmax, nhel
  integer :: ipl
  integer :: ens
  integer :: irac, irhiz
  real    :: epzTot(nbCouchesSol)  ! est-ce que la taille de epzTot peut varier au cours du temps (tassement/detassement) ?
  real    :: zracMAXI  
  real    :: tmaxveille  
  real    :: troseh(24)
  real    :: tculth(24)
  real    :: humh(24)
  real    ::  cum_absz,cum_nitz
  integer :: nbRes, nbRes2 ! ao, as, aoas
  real    :: QNO3i, QNH4i, QNO3f, QNH4f
  real    :: couvermulch_out
  logical :: should_write_report

      n = sc%n
      nbRes = pg%nbResidus
      nbRes2 = nbRes/2
      ihum = int(soil%P_profhum)

       call EnvoyerMsgEcran(logger, 'n = ' // to_string(n))
       call EnvoyerMsgDebug(logger, 'Stics_Jour: climabri')
  
      if (itk(1)%P_codabri == 2) then
          call ClimatSousAbri(sta%P_aks,sta%P_bks,c%tvent(n),n,sc%nouvre2,sc%nouvre3,itk(1)%P_surfouvre1,   &
                              itk(1)%P_surfouvre2,itk(1)%P_surfouvre3,sta%P_cvent,sta%P_phiv0,              &
                              sc%et,c%tetp,sta%P_coefrnet,c%trgext(n),c%tmoy(n),                            &
                              c%tmoyext(n),c%tmin(n),c%tminext(n),c%tmax(n))
      endif
! Modif Loic 04/09/17 : "La mort de la plante n'est pas une option".
! plant_death est appele si :
! n = ndes (jour de destruction) ou si on est le lendemain de la recolte (ndes est mis a jour la veille dans apports d'ou ndes+1)
   do i=1,sc%P_nbplantes
! Modif Bruno juillet 2018 si la recolte a eu lieu, ndes a ete mis a jour
!       if (n == p(i)%ndes .or. n == p(i)%ndes+1 .and. n>p(i)%nrec .and. p(i)%nrec/=0) then
        if (n == p(i)%ndes) then

        irac = nbCouchesSol
        p(i)%dltams(:,:) = 0.
        irhiz = Rhizome_radius(p(i)%maperenne(0),p(i)%densite)
 !       irac = max(2*irhiz,nint(p(i)%zrac))

        call plant_death(&
          logger, irac, irhiz, nbRes, p(i)%nrec, p(i)%P_coderacine, p(i)%P_codeperenne,p(i)%P_codeplante, pg%P_proprac, &
             pg%P_y0msrac, p(i)%msrac(n-1), p(i)%masecnp(0:2,n-1), p(i)%QNplantenp(0:2,n-1), p(i)%qressuite, p(i)%QNressuite,  &
             p(i)%QCressuite, p(i)%qressuite_tot, p(i)%QNressuite_tot, p(i)%QCressuite_tot, pg%P_CNresmin(1:nbRes),            &
             pg%P_CNresmax(1:nbRes), pg%P_Qmulchdec(1:nbRes), sc%Cres(1:irac,1:nbRes), sc%Nres(1:irac,1:nbRes),                &
             sc%Cnondec(1:nbRes), sc%Nnondec(1:nbRes), sc%QCapp, sc%QNapp, sc%QCresorg, sc%QNresorg,                           &
             pg%P_awb(1:nbRes), pg%P_bwb(1:nbRes), pg%P_cwb(1:nbRes), pg%P_CroCo(1:nbRes), pg%P_akres(1:nbRes),                &
             pg%P_bkres(1:nbRes), pg%P_ahres(1:nbRes), pg%P_bhres(1:nbRes), sc%Wb(1:nbRes), sc%kres(1:nbRes), sc%hres(1:nbRes),&
             p(i)%msracmort, p(i)%QCracmort, p(i)%QNracmort, p(i)%lai(0:2,n), p(i)%hauteur(0:2), p(i)%msrac(n),                &
             p(i)%zrac, p(i)%QCrac, p(i)%QNrac, p(i)%maperenne(0:2), p(i)%QNperenne(0:2), p(i)%masecnp(0:2,n),                 &
             p(i)%QNplantenp(0:2,n), p(i)%masec(0:2,n), p(i)%QNplante, p(i)%mstot, p(i)%QNtot, p(i)%mafrais(0:2),              &
             p(i)%pdsfruitfrais(0:2), p(i)%mafraisfeuille(0:2), p(i)%mafraistige(0:2), p(i)%mafraisres(0:2),                   &
             p(i)%mafraisrec(0:2), p(i)%longsperacf,p(i)%longsperacg, p(i)%drlsenf(1:irac), p(i)%drlseng(1:irac),              &
             p(i)%rlf_veille(1:irac), p(i)%rlg_veille(1:irac), p(i)%rlf(1:irac), p(i)%rlg(1:irac), p(i)%rltotf, p(i)%rltotg,   &
             p(i)%dltaperennesen(0:2), p(i)%dltaQNperennesen(0:2), p(i)%maperennemort(0:2), p(i)%QCperennemort(0:2),           &
             p(i)%QNperennemort(0:2), p(i)%resperenne(0:2), p(i)%QNresperenne(0:2),                                            &
             p(i)%matigestruc(0:2), p(i)%mafeuil(0:2), p(i)%mafeuilverte(0:2), p(i)%mafeuiljaune(0:2), p(i)%restemp(0:2),      &
             p(i)%QNtige, p(i)%QNfeuille, p(i)%QNrestemp, p(i)%QNvegstruc, p(i)%QNveg, p(i)%laisen(0:2,n), sc%nh,              &
             soil%P_epc(1:sc%nh), p(i)%msracmortf(1:sc%nh), p(i)%msracmortg(1:sc%nh), p(i)%Ndfa, p(i)%offrenod(0:2),           &
             p(i)%Qfix(0:2), t%dltarestemp(i,0:2), sc%Chum(1), sc%Nhum(1), sc%Chumi, sc%Nhumi, soil%CsurNsol, p(i)%eai(0:2),   &
             p(i)%P_code_rootdeposition, p(i)%demande(0:2), p(i)%demanderac(0:2), p(i)%demandeper(0:2), p(i)%demandetot,       &
             p(i)%absoaer(0:2), p(i)%absorac(0:2), p(i)%absoper(0:2), p(i)%absotot(0:2), sc%Chuma, sc%Nhuma, sc%tauxcouv(n),   &
             pg%P_codemsfinal)

       endif
   enddo  ! boucle nbplantes

! TEST d'INVERSION DE DOMINANCE
    ! Si la hauteur de la plante dominee depasse celle de la plante dominante, il y a INVERsion de DOMINance
      if (sc%P_nbplantes > 1) then
        if (p(2)%hauteur(0) > p(1)%hauteur(0)) then
          call EnvoyerMsgEcran(logger, 'inversion de dominance')
          p(1:2) = p(2:1:-1)
          itk(1:2) = itk(2:1:-1)
          if (p(2)%nrec > 0) then
             p(2)%surf(as) = 0.
             p(2)%surf(ao) = 0.
             p(1)%lai(as,n-1) = p(1)%lai(0,n-1)
             p(1)%lai(ao,n-1) = 0.
             p(1)%masecnp(as,n-1) = p(1)%masecnp(0,n-1)
             p(1)%masecnp(ao,n-1) = 0.
             p(1)%QNplantenp(as,n-1) = p(1)%QNplantenp(0,n-1)
             p(1)%QNplantenp(ao,n-1) = 0.
             ! Loic juin 2018 maj de variables : c'est le bazard les inversions de dominance!
             ! On ne devrait pas avoir a mettre a jour les variables une par une...
             p(1)%maperenne(as) = p(1)%maperenne(0)
             p(1)%maperenne(ao) = 0
             p(1)%QNperenne(as) = p(1)%QNperenne(0)
             p(1)%QNperenne(ao) = 0
             p(1)%resperenne(as) = p(1)%resperenne(0)
             p(1)%resperenne(ao) = 0
             p(1)%QNresperenne(as) = p(1)%QNresperenne(0)
             p(1)%QNresperenne(ao) = 0
             p(1)%maperennemort(as) = p(1)%maperennemort(0)
             p(1)%maperennemort(ao) = 0
             p(1)%QCperennemort(as) = p(1)%QCperennemort(0)
             p(1)%QCperennemort(ao) = 0
             p(1)%QNperennemort(as) = p(1)%QNperennemort(0)
             p(1)%QNperennemort(ao) = 0
             p(1)%dltaperennesen(as) = p(1)%dltaperennesen(0)
             p(1)%dltaperennesen(ao) = 0
             p(1)%dltaQNperennesen(as) = p(1)%dltaperennesen(0)
             p(1)%dltaQNperennesen(ao) = 0
             p(1)%QCO2resperenne(as) = p(1)%QCO2resperenne(0)
             p(1)%QCO2resperenne(ao) = 0
             p(1)%masecveg(as) = p(1)%masecveg(0)
             p(1)%masecveg(ao) = 0.
             p(1)%mafeuil(as) = p(1)%mafeuil(0)
             p(1)%mafeuil(ao) = 0.
             p(1)%mafeuilp(as) = p(1)%mafeuilp(0)
             p(1)%mafeuilp(ao) = 0.
             p(1)%mafeuilverte(as) = p(1)%mafeuilverte(0)
             p(1)%mafeuilverte(ao) = 0.
             p(1)%mafeuiljaune(as) = p(1)%mafeuiljaune(0)
             p(1)%mafeuiljaune(ao) = 0.
             p(1)%matigestruc(as) = p(1)%matigestruc(0)
             p(1)%matigestruc(ao) = 0.
          else
             !  pour eviter que le lai de la plante dominee qui passe a l'ombre soit nul ,
             !  on lui affecte la valeur du lai au soleil de la veille , sachant qu'il sera ensuite pondere par la surface a l'ombre
             ! (dans croissance.f90 apres l'appel a la fonction repartir)
             p(2)%lai(ao,n-1) = p(2)%lai(0,n-1)
             p(2)%masecnp(ao,n-1) = p(2)%masecnp(0,n-1)

             p(1)%masecnp(as,n-1) = p(1)%masecnp(0,n-1)
             p(1)%lai(as,n-1) = p(1)%lai(0,n-1)
             p(1)%QNplantenp(as,n-1) = p(1)%QNplantenp(0,n-1)

             ! Loic juin 2018 maj de variables : c'est le bazard les inversions de dominance!
             ! On ne devrait pas avoir a mettre a jour les variables une par une...
             p(1)%maperenne(as) = p(1)%maperenne(0)
             p(1)%QNperenne(as) = p(1)%QNperenne(0)
             p(1)%resperenne(as) = p(1)%resperenne(0)
             p(1)%QNresperenne(as) = p(1)%QNresperenne(0)
             p(1)%maperennemort(as) = p(1)%maperennemort(0)
             p(1)%QCperennemort(as) = p(1)%QCperennemort(0)
             p(1)%QNperennemort(as) = p(1)%QNperennemort(0)
             p(1)%dltaperennesen(as) = p(1)%dltaperennesen(0)
             p(1)%dltaQNperennesen(as) = p(1)%dltaperennesen(0)
             p(1)%QCO2resperenne(as) = p(1)%QCO2resperenne(0)

             p(2)%masecveg(ao) = p(2)%masecveg(0)
             p(2)%mafeuil(ao) = p(2)%mafeuil(0)
             p(2)%mafeuilverte(ao) = p(2)%mafeuilverte(0)
             p(2)%mafeuiljaune(ao) = p(2)%mafeuiljaune(0)
             p(2)%matigestruc(ao) = p(2)%matigestruc(0)
             p(2)%pgrain(ao) = p(2)%pgrain(0)
             p(2)%masecveg(as) = p(2)%masecveg(0)
             p(2)%mafeuil(as) = p(2)%mafeuil(0)
             p(2)%mafeuilverte(as) = p(2)%mafeuilverte(0)
             p(2)%mafeuiljaune(as) = p(2)%mafeuiljaune(0)
             p(2)%matigestruc(as) = p(2)%matigestruc(0)
             p(2)%pgrain(as) = p(2)%pgrain(0)
          endif
          p(1)%surf(as) = 1.
          p(1)%surf(ao) = 0.
          p(1)%is_dominant = .TRUE.
          p(2)%is_dominant = .FALSE.
        endif
      endif
! Calcul de la photoperiode
      sc%numdate = sc%jul
      c%phoi_veille = c%phoi
      call photpd(sta%P_latitude,sc%numdate,c%daylen,c%phoi)

!  Bruno novembre 2018 : on saute les routines laidev, croissance et allocation si la plante est morte
!  if(p(1)%P_codeplante /= CODE_BARESOIL .or. p(sc%P_nbplantes)%P_codeplante /= CODE_BARESOIL) then

   if(p(1)%estVivante .or. p(sc%P_nbplantes)%estVivante) then
!: Lai - Developpement
      call EnvoyerMsgDebug(logger, 'Stics_Jour: laidev')
      call laidev(logger,sc,p,pg,itk,t,soil,c)

      call EnvoyerMsgDebug(logger, 'Stics_Jour: croissance')
      call EnvoyerMsgEcran(logger, 'avant croissance')

      call croissance(logger, sc, pg, p, itk, soil, c, sta, t)
      call allocation(logger, sc, pg, p, r, itk, soil, t)

    ! replacing with sum for calculating cumulative lai 
    !      sc%laiTot = 0.
    !      do i = 1, sc%P_nbPlantes
    !        sc%laiTot = sc%laiTot + p(i)%lai(0,n)
    !      end do
      sc%laiTot = sum(p%lai(0,n))
   end if

   call EnvoyerMsgDebug(logger, 'Stics_Jour: detassement')


    ! DR le 06/02/07 decoupage de tassement en 3 sp
    !  1) on fait agir le detassement du au travail du sol
    !  2) on decide de la date semis ou recolte
    !  3) on fait agir le tassement du au passage des engins

      if (itk(1)%P_codeDST == 1) then

!DR 01/02/2011 on remplace napS par P_nbjtrav car pour le detassement c'est du travail du sol
        call detassement(logger,n,sc%NH,nbCouchesSol,sc%sat(1:nbCouchesSol),itk(1)%P_nbjtrav,   &
                         p(1)%numjtrav(1:itk(1)%P_nbjtrav),itk(1)%P_proftrav(1:itk(1)%P_nbjtrav),     &
                         pg%P_proflabour,                                                             &
                         pg%P_proftravmin,itk(1)%P_rugochisel,itk(1)%P_dachisel,soil%P_codefente,     &
                         soil%P_hccf(1:sc%NH),soil%P_hminf(1:sc%NH),sc%beta_sol(1:2),itk(1)%P_rugolabour, &
                         itk(1)%P_dalabour,soil%AZnit(1:sc%NH),sc%AZamm(1:sc%NH),p(1)%P_coderacine, &
                         p(1)%lracf(1:sc%NH), p(1)%lracg(1:sc%NH), p(1)%lrach(1:sc%NH),             &
                         pg%P_lvopt,soil%da_ini(1:2),soil%zesx_ini,soil%q0_ini, pg%nbResidus,       &
                         sc%nhe,sc%HR(1:sc%NH),sc%TS(1:sc%NH),soil%P_epc(1:sc%NH),soil%profsol,     &
                         soil%P_profhum,soil%P_z0solnu,soil%profhum_tass(1:2),soil%P_infil(1:2),soil%ncel,&
                         soil%da(1:sc%NH),soil%P_epd(1:sc%NH),soil%icel(0:nbCouchesSol),         &
                         sc%hur(1:nbCouchesSol),sc%hucc(1:nbCouchesSol),                      &
                         sc%humin(1:nbCouchesSol),sc%tsol(1:nbCouchesSol),soil%izcel(1:sc%NH),&
                         soil%izc(1:sc%NH),soil%nit(1:nbCouchesSol),soil%amm(1:nbCouchesSol), &
                         p(1)%rlf(1:nbCouchesSol), p(1)%rlg(1:nbCouchesSol), p(1)%flrac(1:nbCouchesSol), &
                         sc%Cbio(1:nbCouchesSol,1:pg%nbResidus),sc%Nbio(1:nbCouchesSol,1:pg%nbResidus),&
                         sc%Cres(1:nbCouchesSol,1:pg%nbResidus),sc%Nres(1:nbCouchesSol,1:pg%nbResidus),&
                         sc%Chum(1:nbCouchesSol),sc%Nhum(1:nbCouchesSol),soil%P_zesx,soil%P_q0)
      endif
      call EnvoyerMsgDebug(logger, 'Stics_Jour: decisionsemis')

    !::: Decision semis et recolte
      if (itk(1)%P_codedecisemis == 1 .and. p(1)%P_codeplante /= CODE_BARESOIL.and. n < p(1)%ndes) then
        call decisionsemis(logger, n,sc%P_iwater,sc%NH,nbCouchesSol,pg%P_prophumtasssem,soil%P_hccf(1:sc%NH),       &
                           soil%P_epc(1:sc%NH),sc%hur(1:nbCouchesSol),soil%da(1:sc%NH),                             &
                           sc%sat(1:nbCouchesSol),itk(1)%P_profhumsemoir,itk(1)%P_profsem,                          &
                           sc%hucc(1:nbCouchesSol),sc%humin(1:nbCouchesSol),p(1)%P_sensrsec,                     &
                           itk(1)%P_humirac_decisemis,itk(1)%P_nbjseuiltempref,c%tmin(n:n+itk(1)%P_nbjseuiltempref),   &
                           p(1)%P_tdebgel,c%tmoy(n:n+itk(1)%P_nbjseuiltempref),p(1)%P_tdmin,                           &
                           itk(1)%P_eau_mini_decisemis,itk(1)%P_nbj_pr_apres_semis,sc%esol,itk(1)%P_codecalirrig,      &
                           pg%P_irrlev,sc%airg(n:n+itk(1)%P_nbj_pr_apres_semis),c%trr(n:n+itk(1)%P_nbj_pr_apres_semis),&
                           itk(1)%P_nbjmaxapressemis,p(1)%nbjpourdecisemis,sc%repoussesemis(1),p(1)%nplt,p(1)%iplt)

      endif
      call EnvoyerMsgDebug(logger, 'Stics_Jour: decisionrecolte')

      if (itk(1)%P_codedecirecolte == 1 .and. p(1)%P_codeplante /= CODE_BARESOIL .and. n < p(1)%ndes) then
        call decisionrecolte(n,pg%P_prophumtassrec,soil%P_hccf(1:sc%NH),sc%NH,nbCouchesSol,soil%P_epc(1:sc%NH),&
                             sc%hur(1:nbCouchesSol),soil%da(1:sc%NH),sc%sat(1:nbCouchesSol),                &   ! IN
                             itk(1)%P_profhumrecolteuse,p(1)%nrecbutoir,itk(1)%P_nbjmaxapresrecolte,              &
                             p(1)%nbjpourdecirecolte,sc%repousserecolte(1),p(1)%nrec)                          ! INOUT
      endif

    !::: Tassement semis & recolte
      if (itk(1)%P_codeDSTtass == 1) then
        call EnvoyerMsgDebug(logger, 'Stics_Jour: tassesemisrecolte')

        call tassesemisrecolte(&
          logger,n,sc%NH,nbCouchesSol,p(1)%P_codeplante,p(1)%nplt,p(1)%nrec,itk(1)%P_profhumsemoir,  &
                               pg%P_prophumtasssem,soil%P_hccf(1:sc%NH),itk(1)%P_dasemis,itk(1)%P_profhumrecolteuse,  &
                               pg%P_prophumtassrec,itk(1)%P_darecolte,sc%sat(1:nbCouchesSol),sc%beta_sol,          &
                               itk(1)%P_codeDSTnbcouche,p(1)%P_coderacine,soil%da_ini(1:2),soil%zesx_ini,soil%q0_ini, &
                               pg%nbResidus, sc%Hinit(1:2),                                                         &
                               soil%profsol, soil%P_infil(1:sc%NH), soil%da(1:sc%NH),                               &
                               soil%profhum_tass(1:2), soil%P_obstarac, soil%icel(0:nbCouchesSol), soil%ncel,    &
                               soil%izcel(1:sc%NH), sc%nhe, soil%P_profhum, soil%izc(1:sc%NH),soil%P_epc(1:sc%NH),  &
                               soil%P_epd(1:sc%NH), sc%hur(1:nbCouchesSol), soil%nit(1:nbCouchesSol),         &
                               soil%amm(1:nbCouchesSol), p(1)%flrac(1:nbCouchesSol),                          &
                               p(1)%rlf(1:nbCouchesSol), p(1)%rlg(1:nbCouchesSol),                            &
                               sc%Cbio(1:nbCouchesSol,1:pg%nbResidus), sc%Cres(1:nbCouchesSol,1:pg%nbResidus), &
                               sc%Nres(1:nbCouchesSol,1:pg%nbResidus), sc%Nbio(1:nbCouchesSol,1:pg%nbResidus), &
                               sc%Chum(1:nbCouchesSol), sc%Nhum(1:nbCouchesSol), sc%hucc(1:nbCouchesSol),   &
                               sc%humin(1:nbCouchesSol), sc%tsol(1:nbCouchesSol), soil%P_zesx, soil%P_q0, sc%HR(1:sc%NH))
      endif

      if (t%P_codeSWDRH.eq.1) then
!: ML - 29/10/12 - calcul duree d'humectation
!- appel prealable de shutwall et humheure pour calculer tculthoraire et determiner s'il y a humectation ou non
          call shutwall(logger,sc,pg,c,sta,soil,p,itk)
          if (n > 1) tmaxveille = sc%tcultveille * 2 - c%tmin(n-1)
          if (n == 1) tmaxveille = sc%tcultmax
          call humheure(n,sc%tcultmin,sc%tcultmax,tmaxveille,sc%humidite,c%daylen,c%tmin(n+1),sc%tcult,    &
               sc%trosemax(0:n),c%humimoy,c%tmin(n),t%P_codetrosee,tculth,troseh,humh)
          call surface_wetness_duration_relative_humidity(tculth,troseh,humh, &
               c%dureehumec,c%dureeRH,c%dureeRH1,c%dureeRH2,c%trr(n))
      endif

!::: Apports
      call EnvoyerMsgDebug(logger, 'Stics_Jour: before apports ')
      call EnvoyerMsgEcran(logger, 'Stics_Jour: before apports ')

      do i=1, sc%P_nbPlantes
        p(i)%QNplantetombe(aoas) = p(i)%QNplantetombe(ao) * p(i)%surf(ao) + p(i)%QNplantetombe(as) * p(i)%surf(as)
        p(i)%QCplantetombe(aoas) = p(i)%QCplantetombe(ao) * p(i)%surf(ao) + p(i)%QCplantetombe(as) * p(i)%surf(as)
      enddo

      call apports(logger,sc,pg,t,c,soil,p,itk,sta)
      call EnvoyerMsgEcran(logger, 'Stics_Jour: after apports ')

!::: Etatsurf
! replacing with sum for calculating cumulative stemflow 
!      sc%stemflowTot = 0.
!      do i = 1, sc%P_nbPlantes
!        sc%stemflowTot = sc%stemflowTot + p(i)%stemflow
!      end do
    sc%stemflowTot = sum( p%stemflow )

      ipl = 1
      ens = as
!     Cmulchnd et Cmulchdec en kg C/ha et qmulch en t MS/ha
      sc%qmulch = (sc%Cmulchdec + sc%Cmulchnd)/ 420.

      call etatsurf(sc%stemflowTot,pg%P_pminruis,soil%P_ruisolnu,pg%P_qmulchruis0(sc%irmulch),   &
                    pg%P_mouillabilmulch(sc%irmulch),pg%P_kcouvmlch(sc%irmulch),p(1)%P_codebeso, &
                    p(1)%P_codelaitr,c%tetp(n),sc%delta,sc%laiTot,sc%tauxcouv(n),                &
                    sc%qmulch,sc%ruisselsurf,sc%precip,sc%mouillmulch,sc%intermulch,             &   ! INOUT
                    couvermulch_out,sc%Emulch,soil%P_penterui)
      if(itk(1)%P_codepaillage == 2) then
          !! paillage mulch plastique, do not use couvermulch value
      else
        sc%couvermulch = couvermulch_out
      endif
      call EnvoyerMsgEcran(logger, 'Stics_Jour: after etatsurf')

!  Calcul de la volatilisation liee aux apports de residus organiques
      if (soil%Nvolatorg <= 0.1) then
          sc%FsNH3 = 0.
          ! DR 03/03/2022 on initialise Nvolorg � zero si on a pas de NH4 volatilisable
          soil%Nvolorg=0.
      else
          call EnvoyerMsgDebug(logger, 'volatilisation')
          call EnvoyerMsgDebug(logger, '=== jour volatorg > 0.1 ' // to_string(sc%n) //&
            ' Nvolatorg ' // to_string(soil%Nvolatorg) // ' amm(1) ' // to_string(soil%amm(1)))
          call volatorg(soil%da(1),sc%hur(1:2),soil%P_pH0,sc%tcult,sta%P_NH3ref,sc%ras,sta%P_ra,soil%dpH, &
                        sc%FsNH3,soil%Nvolorg,soil%amm(1),soil%Nvolatorg,soil%QNvolorg,soil%pHvol)
      endif

! Decomposition des MOS et mineralisation C-N
      call EnvoyerMsgDebug(logger, 'Stics_Jour: Mineralisation')
     ! write(8888,*)'avant mineral',n,sc%Qminr
     !      write(712,*)'av mineral', n, sc%Cres(1:nbCouchesSol,1:pg%nbResidus)
     !      write(712,*)'av mineral', n, sc%Nres(1:nbCouchesSol,1:pg%nbResidus)
     !      write(712,*)'av mineral', n, sc%Cbio(1:nbCouchesSol,1:pg%nbResidus)
     !      write(712,*)'av mineral', n, sc%Nbio(1:nbCouchesSol,1:pg%nbResidus)
      call mineral( nbCouchesSol, pg%nbResidus,                                                  &
                   soil%P_profhum, soil%P_argi, soil%P_calc, soil%P_codefente, t%P_codeNmindec,            &  ! codes "activation"
                   pg%P_fhminsat, pg%P_FTEMh, pg%P_FTEMha,             &
                   pg%P_FTEMr, pg%P_FTEMra, sc%FTEMhb, sc%FTEMrb, pg%P_hminm, pg%P_hoptm, &
                   sc%kres(1:pg%nbResidus), pg%P_kbio(1:pg%nbResidus), sc%Wb(1:pg%nbResidus),     &  ! decomposition
                   pg%P_yres(1:pg%nbResidus), sc%hres(1:pg%nbResidus), pg%P_fNCbiomin, pg%P_fredlN,        &
                   pg%P_fredkN, t%P_rapNmindec, t%P_fNmindecmin, pg%P_fredNsup, pg%P_Primingmax,           &  ! mineralisation
                   sc%tsol(1:nbCouchesSol), sc%hucc(1:nbCouchesSol), sc%hur(1:nbCouchesSol),            &
                   sc%dacouche(1:nbCouchesSol), sc%humin(1:nbCouchesSol), sc%sat(1:nbCouchesSol),       &
                   soil%itrav1, soil%itrav2,                                                                     &
                   sc%Cres(1:nbCouchesSol,1:pg%nbResidus),sc%Nres(1:nbCouchesSol,1:pg%nbResidus),          & ! INOUT
                   sc%Cbio(1:nbCouchesSol,1:pg%nbResidus),sc%Nbio(1:nbCouchesSol,1:pg%nbResidus),          &
                   sc%Chum(1:nbCouchesSol),sc%Nhum(1:nbCouchesSol),sc%Chuma,sc%Nhuma,sc%Chumi,sc%Nhumi,    &
                   sc%Chumt, sc%Nhumt, sc%Cr, sc%Nr, sc%Cb, sc%Nb, sc%NCbio,                                     &
                   soil%amm(1:nbCouchesSol),soil%nit(1:nbCouchesSol),sc%CO2hum,sc%CO2res,sc%CO2sol,        &
                   soil%vminr, soil%cumvminh, soil%cumvminr, sc%QCO2sol, sc%QCO2res, sc%QCO2hum, sc%Qminh,       &
                   sc%Qminr, sc%QCprimed,sc%QNprimed,sc%tnhc,sc%tnrc,sc%Cnondec(1:pg%nbResidus), &
                   sc%Nnondec(1:nbRes), &
                   sc%Cmulchnd,sc%Nmulchnd,sc%Cmulchdec,sc%Nmulchdec,sc%Cmulch,sc%Nmulch,sc%Cbmulch,sc%Nbmulch,  &
                   sc%qmulch, sc%cum_immob, sc%QCO2mul, sc%Crprof, sc%Nrprof,                                    &
                   soil%P_pH0, soil%CsurNsol, pg%P_gmin1, pg%P_gmin2, pg%P_gmin3, pg%P_gmin4,   &
                   pg%P_gmin5, pg%P_gmin6, pg%P_gmin7, pg%P_tmin_mineralisation,pg%P_Wh,t%P_code_CsurNsol_dynamic)    ! DR 04/06/2019 ajout d'un nouveau parametre P_tmin_mineralisation
     !      write(712,*)'ap mineral', n, sc%Cres(1:nbCouchesSol,1:pg%nbResidus)
     !      write(712,*)'ap mineral', n, sc%Nres(1:nbCouchesSol,1:pg%nbResidus)
     !      write(712,*)'ap mineral', n, sc%Cbio(1:nbCouchesSol,1:pg%nbResidus)
     !      write(712,*)'ap mineral', n, sc%Nbio(1:nbCouchesSol,1:pg%nbResidus)

!    Nitrification
!    Joel 4/2/2015
        call EnvoyerMsgDebug(logger, 'Stics_Jour: nitrification')
        call nitrif_N2O(nbCouchesSol, soil%P_profhum, soil%P_codefente, soil%P_codenitrif,        &
           pg%P_rationit, pg%P_hoptn, pg%P_hminn, pg%P_fnx, soil%pH_soil, pg%P_pHmaxnit, pg%P_pHminnit,        &
           sc%var_tnitmin(sc%numcult), sc%var_tnitopt(sc%numcult), sc%var_tnitmax(sc%numcult), sc%var_tnitopt2(sc%numcult),  &
           pg%P_vnitmax, pg%P_Kamm, pg%P_nh4_min,                                                                &
           sc%tsol(1:nbCouchesSol), sc%hucc(1:nbCouchesSol), sc%hur(1:nbCouchesSol),                    &
           sc%dacouche(1:nbCouchesSol), sc%humin(1:nbCouchesSol), sc%sat(1:nbCouchesSol),               & ! IN
!           pg%P_code_vnit, pg%P_fnx_soil, pg%P_code_tnit, %P_tnitmin_pw, t%P_tnitopt_pw, t%P_tnitopt2_pw,          &
!           t%P_tnitmax_pw, pg%P_tnitopt_gauss, pg%P_scale_tnitopt, pg%P_code_rationit, pg%P_rationit,       &
           pg%P_code_vnit, pg%P_code_tnit,           &
           pg%P_tnitopt_gauss, pg%P_scale_tnitopt, pg%P_code_rationit,        &
           pg%P_code_hourly_wfps_nit, sc%precip, pg%P_kdesat,                                                    &  ! IN
           soil%amm(1:nbCouchesSol), soil%nit(1:nbCouchesSol),                                             & ! INOUT
           soil%nitrifj, sc%Qnitrif, sc%em_N2Onit, sc%Qem_N2Onit)

!      write(ficgdb,*)'apres nitrif',soil%nit(1:25)


!************
!    Denitrification par modele NOE (conditions de sol)
           call EnvoyerMsgDebug(logger, 'Stics_Jour: denitrification')
!    Joel 4/2/2015
        if (soil%P_codedenit == 1) then
        call denit_N2O(nbCouchesSol, soil%P_profdenit, soil%P_vpotdenit, pg%P_ratiodenit,              &
! DR 12/06/2017 on entre CsurNsol calcule
!                       soil%P_codefente, soil%pH_soil, soil%P_Norg, soil%P_CsurNsol, pg%P_pHminden, pg%P_pHmaxden,               &
                       soil%P_codefente, soil%pH_soil, soil%P_Norg, soil%CsurNsol, pg%P_pHminden, pg%P_pHmaxden,               &
                       !sc%var_TREFdenit1, sc%var_TREFdenit2,                                                             &
                       pg%P_Kd, pg%P_kdesat, pg%P_wfpsc,                              &
                       sc%dacouche(1:nbCouchesSol), sc%hucc(1:nbCouchesSol), sc%humin(1:nbCouchesSol),             &
                       sc%sat(1:nbCouchesSol), sc%hur(1:nbCouchesSol), sc%tsol(1:nbCouchesSol), sc%precip,         & ! IN
                       pg%P_tdenitopt_gauss, pg%P_scale_tdenitopt, pg%P_code_pdenit, pg%P_cmin_pdenit, pg%P_cmax_pdenit,    &
                       pg%P_min_pdenit, pg%P_max_pdenit, pg%P_code_ratiodenit,                              &
                       pg%P_code_hourly_wfps_denit,                                                                         &
                       soil%nit(1:nbCouchesSol), soil%condenit, soil%Ndenit, soil%QNdenit, sc%em_N2Oden, sc%Qem_N2Oden)    ! INOUT
        endif
! Emissions totales de N2O
        sc%em_N2O  = sc%em_N2Onit  + sc%em_N2Oden
        sc%Qem_N2O = sc%Qem_N2Onit + sc%Qem_N2Oden

!::: Calcul des Besoins - Besoin en eau, nodules, besoin en azote

      call EnvoyerMsgDebug(logger, 'Stics_Jour: besoinsEnEauDeLaPlante')
      call EnvoyerMsgEcran(logger, 'Stics_Jour: besoinsEnEauDeLaPlante')
      call besoinsEnEauDeLaPlante(logger,sc,pg,c,sta,soil,p,itk)

      call EnvoyerMsgDebug(logger, 'Stics_Jour: besoins azote')
! Besoins en azote
      do i = 1,sc%P_nbplantes
!       irac = int(p(i)%zrac)
        irac = int(p(i)%zracmax) + 1
        do ens = as, ao
          if (p(i)%surf(ens) > 0.) then
    ! Fixation symmbiotique d'azote
            if (p(i)%P_codelegume == 2 .and. pg%P_codesymbiose == 2) then
              call EnvoyerMsgDebug(logger, 'offrnodu')
              call offrnodu(n, p(i)%nlev, p(i)%zrac, p(i)%dtj(n+p(i)%ndecalg), itk(i)%P_profsem, p(i)%P_profnod, nbCouchesSol, &
                            soil%nit(1:nbCouchesSol), sc%hur(1:nbCouchesSol), p(i)%P_concNnodseuil, p(i)%P_vitno,        &
                            p(i)%P_codefixpot, p(i)%P_fixmax, p(i)%P_fixmaxveg, p(i)%P_fixmaxgr ,p(i)%dltams(ens,n),           &
                            p(i)%dltags(ens), p(i)%P_concNrac100, p(i)%P_concNrac0, pg%P_codefxn, sc%humin(1:nbCouchesSol), &
                            sc%tcultmin, sc%tcultmax, sc%tsol(1:nbCouchesSol), p(i)%P_tempnod1, p(i)%P_tempnod2,            &
                            p(i)%P_tempnod3, p(i)%P_tempnod4, sc%anox(1:nbCouchesSol), p(i)%fixreel(ens), p(i)%somcourno,   &
                            p(i)%P_stlevdno, p(i)%ndno, p(i)%P_stdnofno, p(i)%nfno, p(i)%P_stfnofvino, p(i)%nfvino ,sc%nodn,   &
                            p(i)%propfixpot, p(i)%fixpotfno, p(i)%fixmaxvar(ens), p(i)%fixpot(ens), sc%fxn, sc%fxw, sc%fxt,    &
                            sc%fxa, p(i)%ndrp, p(i)%P_codeplante, p(i)%P_code_acti_reserve)
            endif

            call EnvoyerMsgDebug(logger, 'bNpl ' // to_string(n))
            !DR 08/08/2019 avant d'appeller BNpl pour la vgne j'ai dbesoin de masec total qu'on a pas ...
            call bNpl(p(i)%nbrecolte, p(i)%masecnp(ens,n), p(i)%rdtint(ens,1:max(1,p(i)%nbrecolte-1)), p(i)%surf(ens),  &
                      p(i)%dltams(ens,n), p(i)%P_codeplisoleN, p(i)%P_masecNmax, p(i)%P_masecmeta, t%adilmaxI(i),       &
                      t%bdilmaxI(i), t%adilI(i), t%bdilI(i), p(i)%P_adilmax, p(i)%P_bdilmax, p(i)%P_adil, p(i)%P_bdil,  &
                      p(i)%dltags(ens), t%dltamsN(i,ens), p(i)%P_inngrain1, p(i)%P_inngrain2, p(i)%dltaremobilN(ens),   &
                      p(i)%masecdil(ens), p(i)%masecpartiel(ens), p(i)%inns(ens), p(i)%innlai(ens), p(i)%abso(ens,n),   &
                  ! DR 19/02/2019 substitution de parazorac en tant que code par un vrai code P_code_rootdeposition
                  !    p(i)%dNdWcrit, t%deltabso(i,ens), sc%absodrp, t%dltarestemp(i,ens), p(i)%P_Parazorac, p(i)%INN,   &
                      p(i)%dNdWcrit, t%deltabso(i,ens), sc%absodrp, t%dltarestemp(i,ens),                               &
                      p(i)%P_code_rootdeposition, p(i)%P_Parazorac, p(i)%INN(ens),   &
                      p(i)%P_INNmin, p(i)%dltamsrac, p(i)%nstopres, p(i)%nlax, t%dltarestempN(i,ens), p(i)%P_Parazoper, &
                      p(i)%P_code_acti_reserve, p(i)%fstressgel, p(i)%demande(ens), p(i)%demandeper(ens),               &
                      p(i)%demanderac(ens), p(i)%QNperenne(ens), p(i)%QNresperenne(ens), p(i)%QNplantenp(ens,n), &
                      p(i)%maperenne(ens))


          endif
        end do  ! boucle sur ens

 ! La demande en N des organes racinaires de la partie ombree de la 2eme culture est nulle
        if (i > 1) p(i)%demanderac(ao) = 0.
        p(i)%demande(aoas) = p(i)%demande(ao) * p(i)%surf(ao) + p(i)%demande(as) * p(i)%surf(as)
        p(i)%demanderac(aoas) = p(i)%demanderac(ao) * p(i)%surf(ao) + p(i)%demanderac(as) * p(i)%surf(as)
        p(i)%demandeper(aoas) = p(i)%demandeper(ao) * p(i)%surf(ao) + p(i)%demandeper(as) * p(i)%surf(as)
        p(i)%demandetot = p(i)%demande(aoas) + p(i)%demanderac(aoas) + p(i)%demandeper(aoas)
        p(i)%QNplantenp(aoas,n) = p(i)%QNplantenp(ao,n) + p(i)%QNplantenp(as,n)
 ! ajout Bruno mai 2018 fixreel(aoas) n'etait pas calcule
        p(i)%fixreel(aoas) = p(i)%fixreel(ao) * p(i)%surf(ao) + p(i)%fixreel(as) * p(i)%surf(as)
      end do

!::: Lixivation
      call EnvoyerMsgDebug(logger, 'Stics_Jour: lixivation')
    ! on calcul la somme des epz de toutes les cultures...
      epzTot(:) = 0.
      do iz = 1,sc%nhe
        do i = 1,sc%P_nbplantes
       ! DR et ML 01/09/2014 c'est le cumul des transpirations des 2 plantes  (prenant en compte les surfaces et
       ! calcule dans transpi la veille ) qu'il faut enlever du sol
       ! le bug faisait qu'on n'enlevait pas a HUR les ep transpires la veille (on ne ponderait pas par les surfaces !)
       ! induisant un ecart de bilan pouvant etre fort
       !   epzTot(iz) = epzTot(iz) + p(i)%epz(as,iz) + p(i)%epz(ao,iz)
        p(i)%epz(aoas,iz) = p(i)%epz(ao,iz) * p(i)%surf(ao) + p(i)%epz(as,iz) * p(i)%surf(as)
        epzTot(iz) = epzTot(iz) + p(i)%epz(aoas,iz)
        end do
      end do

! replacing with maxval for calculating zracMAXI 
!      zracMAXI = 0.0
!      do i = 1,sc%P_nbplantes
!          zracMAXI = max(zracMAXI,p(i)%zrac)
!      end do
      zracMAXI = maxval(p%zrac)
! Modif Bruno octobre 2018 remplacement de soil%profsol par nhe pour etre coherent avec la subroutine lixiv
!      call lixiv(sc%n, sc%precip, sc%hurlim, sc%ruisselsurf, sc%humin(1:soil%profsol), sc%nhe, sc%nh,                 &
!                 sc%hucc(1:soil%profsol), sc%hr(1:sc%NH), soil%P_codefente, soil%ncel, soil%icel(0:soil%ncel),        &
!                 soil%da(1:sc%nh), soil%izcel(1:5), soil%P_infil(0:5), soil%P_concseuil, soil%P_humcapil, soil%izc(1:5),   &
!                 soil%hcc(1:sc%nh), soil%P_capiljour, soil%P_epc(1:sc%nh), soil%hmin(1:sc%nh), soil%P_codemacropor,        &
!                 soil%P_profdrain, soil%profsol, pg%P_bformnappe, pg%P_distdrain, pg%P_codhnappe, soil%ldrains,            &
!                 soil%P_codrainage, pg%P_rdrain, soil%P_profimper, soil%P_ksol, soil%profcalc, epzTot(1:sc%nhe),           &
!                 sc%esz(1:sc%nhe), zracMAXI, p(1)%irrigprof(1:sc%nhe), sc%bouchon, sc%hur(1:sc%nhe), sc%sat(1:sc%nhe),     &
!                 sc%anox(1:sc%nhe), soil%nit(1:sc%nhe), sc%pluieruissel, sc%saturation, sc%RU, sc%RsurRU, soil%concno3les, &
!                 soil%hnappe, soil%hmax, soil%hph, soil%hpb, soil%profnappe, soil%qdrain, soil%azlesd, sc%azsup, sc%QLES,  &
!                 sc%DRAT, sc%drain, sc%ruissel, soil%remontee, soil%qlesd, soil%qdraincum, sc%exces(0:5), sc%P_profmesW,   &
!                 sc%SoilAvW, sc%SoilWatM, sc%lessiv, sc%som_HUR, sc%som_sat, soil%P_obstarac, soil%remonteemax)

      call lixiv(logger, sc%n, sc%nhe, sc%nh, soil%ncel, soil%P_codefente, soil%P_codemacropor, soil%P_codrainage, pg%P_codhnappe,&
           soil%profcalc, soil%P_infil(0:sc%nh), soil%P_concseuil, soil%P_humcapil, soil%P_capiljour, soil%P_profdrain,    &
           pg%P_bformnappe, pg%P_distdrain, pg%P_rdrain, soil%P_profimper, soil%P_ksol, soil%profsol, soil%ldrains,        &
           sc%precip, sc%hurlim, sc%ruisselsurf, zracMAXI, soil%icel(0:soil%ncel), soil%P_epc(1:sc%nh), soil%da(1:sc%nh),  &
           sc%hr(1:sc%nh), soil%hcc(1:sc%nh), soil%hmin(1:sc%nh), soil%izcel(1:sc%nh), soil%izc(1:sc%nh), sc%exces(0:sc%nh),&
           sc%hucc(1:sc%nhe), sc%humin(1:sc%nhe), epzTot(1:sc%nhe), sc%esz(1:sc%nhe), p(1)%irrigprof(1:sc%nhe),            &
           sc%hur(1:sc%nhe), sc%sat(1:sc%nhe), sc%anox(1:sc%nhe), soil%nit(1:sc%nhe), sc%bouchon, sc%pluieruissel,         &
           sc%saturation, sc%RU, sc%RsurRU, soil%concno3les, soil%hnappe, soil%hmax, soil%hph, soil%hpb, soil%profnappe,   &
           soil%qdrain, soil%azlesd, sc%azsup, sc%DRAT, sc%QLES, sc%drain, sc%ruissel, soil%remontee, soil%qlesd,          &
           soil%qdraincum, sc%P_profmesW, sc%SoilAvW, sc%SoilWatM, sc%lessiv, sc%som_HUR, sc%som_sat,                      &
           itk(1)%P_profmes,                                                                                               &
           sc%husup_by_horizon(1:5), sc%azsup_by_horizon(1:5), sc%husup_under_profmes, sc%azsup_under_profmes,             &
           sc%husup_by_cm(1:sc%nhe))

      do i = 1,sc%P_nbplantes
        p(i)%RsurRUrac = 0.
        p(i)%RUrac = 0.
        do iz = int(itk(i)%P_profsem),int(p(i)%zrac)
          p(i)%RsurRUrac = p(i)%RsurRUrac + max(sc%hur(iz) + sc%sat(iz) - sc%humin(iz),0.)
          p(i)%RUrac = p(i)%RUrac + sc%hucc(iz) - sc%humin(iz)
        end do

        !if (p(i)%RUrac == 0) then
        if (abs(p(i)%RUrac).lt.1.0E-8) then
          p(i)%RsurRUrac = 1.0
        else
          p(i)%RsurRUrac = p(i)%RsurRUrac / p(i)%RUrac
        endif
      end do

!   Plant transpiration
      call EnvoyerMsgDebug(logger, 'Stics_Jour:  Transpiration')
      do i = 1,sc%P_nbplantes
        do ens = as, ao
          if (p(i)%surf(ens) > 0.) then
            call transpi(sc%n,nbCouchesSol,soil%profsol,sc%hur(1:nbCouchesSol),sc%humin(1:nbCouchesSol), &
                         p(i)%lracz(1:nbCouchesSol),p(i)%surf(ens),p(i)%P_codelaitr,p(i)%lai(ens,n),           &
                         sc%tauxcouv(n),p(i)%nrec,itk(i)%P_codcueille,p(i)%eop(ens),p(i)%exobiom,                 &
                         p(i)%epz(ens,1:nbCouchesSol),p(i)%ep(ens),p(i)%turfac(ens),p(i)%senfac(ens),          &
                         p(i)%swfac(ens),p(i)%profexteau)
          endif
        end do

        p(i)%epz(aoas,:) = p(i)%epz(ao,:) * p(i)%surf(ao) + p(i)%epz(as,:) * p(i)%surf(as)
        p(i)%ep(aoas)     = p(i)%ep(ao) * p(i)%surf(ao) + p(i)%ep(as) * p(i)%surf(as)
        p(i)%turfac(aoas) = min(p(i)%turfac(ao), p(i)%turfac(as))
        p(i)%senfac(aoas) = min(p(i)%senfac(ao), p(i)%senfac(as))
        p(i)%swfac(aoas)  = min(p(i)%swfac(ao), p(i)%swfac(as))
      end do

!::: Calcul des Offres - Offre en azote, absorption N, maj de l'azote du sol
      do i = 1,sc%P_nbplantes
!       irac = int(p(i)%zrac)
        irac = int(p(i)%zracmax) + 1
        if (p(i)%masecdil(as) > 0.) then
            call EnvoyerMsgDebug(logger, 'Stics_Jour: offreN')
            call offreN(p(i)%zrac,soil%nit(1:int(p(i)%zrac)),soil%amm(1:int(p(i)%zrac)),sc%hur(1:int(p(i)%zrac)),   &
                      sc%humin(1:int(p(i)%zrac)),sc%hucc(1:int(p(i)%zrac)),p(i)%flrac(1:int(p(i)%zrac)),          &
                      pg%P_lvopt,pg%P_difN,p(i)%epz(aoas,1:int(p(i)%zrac)),p(i)%P_Vmax1,p(i)%P_Kmabs1,p(i)%P_Vmax2, &
                      p(i)%P_Kmabs2,                                                                              &
                      sc%cumoffrN,p(i)%flurac,p(i)%flusol,sc%offrN(1:int(p(i)%zrac)))
          do ens = as, ao
            if (p(i)%surf(ens) > 0.) then
              call EnvoyerMsgDebug(logger, 'Stics_Jour: absoN')
               call absoN(irac, p(i)%offrenod(ens), sc%cumoffrN, p(i)%surf(ens), sc%offrN(1:irac), soil%nit(1:irac),      &
                           soil%amm(1:irac), p(i)%demande(ens), p(i)%demandeper(ens), p(i)%demanderac(ens), p(i)%P_codelegume,  &
                           pg%P_codesymbiose, p(i)%P_PropresPN, p(i)%dNdWcrit, p(i)%dltams(ens,n), t%dltarespstruc(i,ens),  &
                           p(i)%fixreel(ens), p(i)%dltaremobilN(ens), sc%absz(1:irac), p(i)%profextN, p(i)%QNplantenp(ens,n), &
                           p(i)%QNperenne(ens), p(i)%QNrac, p(i)%QNresperenne(ens), p(i)%Ndfa, p(i)%absoaer(ens), &
                           p(i)%absoper(ens), p(i)%absorac(ens), p(i)%abso(ens,n), p(i)%absotot(ens), p(i)%QNabsoaer(ens), &
                           p(i)%QNabsoper(ens), p(i)%QNabsorac(ens), p(i)%QNabso(ens), p(i)%QNabsotot(ens), p(i)%Qfix(ens), &
                           p(i)%Qfixtot(ens), t%P_code_ISOP, t%P_code_pct_legume, t%P_pct_legume)

                           call EnvoyerMsgDebug(logger, 'Stics_Jour: majNsol')
                if(p(i)%abso(ens,n) > 0.) call majNsol(irac, sc%absz(1:irac), soil%nit(1:irac), soil%amm(1:irac))


!!!!!!! PRESENT DANS LE TRONC: A VOIR !!!!!!!!!!!!!!!!!!!!!!!!!!
! 02/09/2014 DR et ML bug du bilan d'N non boucle pour les CAS
! il faut ponderer l'azote absorbe par les surfaces a l'ombre et au soleil de chaque plante
! sinon on enleve plus d'azote du sol que la plante n'en absorbe
!              sc%absz(1:nbCouchesSol)=sc%absz(1:nbCouchesSol)*p(i)%surf(ens)
!              call majNsol(nbCouchesSol,p(i)%zrac,sc%absz(1:nbCouchesSol),   &
!              soil%nit(1:nbCouchesSol),soil%amm(1:nbCouchesSol))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              cum_absz=sum(sc%absz(1:nbCouchesSol))
              cum_nitz=sum(soil%nit(1:nbCouchesSol))
              endif
            end do

! Mise a jour absorption plante entiere
! les variables suivantes ont deja ete ponderees par la surface dans absoN
          p(i)%absoaer(aoas) = p(i)%absoaer(ao) + p(i)%absoaer(as)
          p(i)%absorac(aoas) = p(i)%absorac(ao) + p(i)%absorac(as)
          p(i)%absoper(aoas) = p(i)%absoper(ao) + p(i)%absoper(as)
          p(i)%abso(aoas,n) = p(i)%abso(ao,n) + p(i)%abso(as,n)
          p(i)%absotot(aoas) = p(i)%absotot(ao) + p(i)%absotot(as)
          p(i)%offrenod(aoas) = p(i)%offrenod(ao) + p(i)%offrenod(as)

          p(i)%QNabsoaer(aoas) = p(i)%QNabsoaer(ao) + p(i)%QNabsoaer(as)
          p(i)%QNabsorac(aoas) = p(i)%QNabsorac(ao) + p(i)%QNabsorac(as)
          p(i)%QNabsoper(aoas) = p(i)%QNabsoper(ao) + p(i)%QNabsoper(as)
          p(i)%QNabso(aoas) = p(i)%QNabso(ao) + p(i)%QNabso(as)
          p(i)%QNabsotot(aoas) = p(i)%QNabsotot(ao) + p(i)%QNabsotot(as)

          p(i)%Qfix(aoas) = p(i)%Qfix(ao) + p(i)%Qfix(as)
          p(i)%Qfixtot(aoas) = p(i)%Qfixtot(ao) + p(i)%Qfixtot(as)
          p(i)%QNplantenp(aoas,n) = p(i)%QNplantenp(ao,n) + p(i)%QNplantenp(as,n)
! Bruno juin 2017  deplace plus bas
          ! Loic Oct 2020 : Pour retrouver la v9 il faut que QNplante soit maj ici
          if (p(i)%P_code_acti_reserve == 2) p(i)%QNplante = p(i)%QNplantenp(aoas,n) + p(i)%QNperenne(aoas)
!          p(i)%QNtot = p(i)%QNplante + p(i)%QNrac
        endif
      end do

!::: Calcul des Stress - hydrique, azote, azote des grains, exces d'eau
      do i = 1,sc%P_nbplantes
        do ens = as, ao
          if (p(i)%surf(ens) > 0.) then
! Stress Eau
            call EnvoyerMsgDebug(logger, 'Stics_Jour: stressEau')
            call stressEau(n,itk(i)%P_profsem,p(i)%zrac,nbCouchesSol,sc%hur(1:nbCouchesSol),                     &
                           sc%humin(1:nbCouchesSol),p(i)%cumlracz,p(i)%surf(ens),p(i)%P_codelaitr,                  &
                           p(i)%lai(ens,n),sc%tauxcouv(n),p(i)%nrec,itk(i)%P_codcueille,p(i)%eop(ens),p(i)%P_rayon,    &
                           p(i)%P_psisto(itk(i)%P_variete),p(i)%P_psiturg(itk(i)%P_variete),                           &
                           p(i)%P_rapsenturg(itk(i)%P_variete),sc%supres,p(i)%P_swfacmin(itk(i)%P_variete),            &
                           t%P_code_ISOP,p(i)%resrac,p(i)%swfac(ens),p(i)%turfac(ens),p(i)%tetstomate,p(i)%teturg,      &
                           p(i)%senfac(ens))

            call EnvoyerMsgDebug(logger, 'Stics_Jour: stressN')

            call stressN(p(i)%masecdil(ens), p(i)%P_masecnp0,p(i)%P_adilmax,p(i)%P_masecNmax,                           &
                         p(i)%P_bdilmax,p(i)%masecpartiel(ens),p(i)%magrain(ens,n),sc%absodrp,p(i)%P_codeplisoleN,      &
                         p(i)%P_masecmeta,t%adilI(i),t%bdilI(i),p(i)%P_adil,p(i)%P_bdil,t%adilmaxI(i),t%bdilmaxI(i),    &
                         p(i)%dNdWcrit,t%deltabso(i,ens),p(i)%P_codeINN,p(i)%P_INNmin,p(i)%P_INNimin,p(i)%P_innturgmin, &
                         p(i)%P_innsen(itk(i)%P_variete),pg%P_QNpltminINN,p(i)%absotot(ens),p(i)%QNplantenp(ens,n),          &
                         p(i)%QNplanteres,p(i)%CNplante(ens),p(i)%inn(ens),p(i)%inni(ens),p(i)%inns(ens),                &
                         p(i)%innlai(ens),p(i)%innsenes(ens),p(i)%P_code_acti_reserve,p(i)%dltaremobilN(ens),           &
                         p(i)%absoaer(ens),p(i)%absorac(ens),p(i)%QNplante,p(i)%P_codeplante)
          endif
      end do

!  Cumuls AO/AS
! 31/08/2017 TODO Loic et BM doivent verifier
        p(i)%CNplante(aoas) = p(i)%CNplante(ao) * p(i)%surf(ao) + p(i)%CNplante(as) * p(i)%surf(as)
        p(i)%QNplantetombe(aoas) = p(i)%QNplantetombe(ao) * p(i)%surf(ao) + p(i)%QNplantetombe(as) * p(i)%surf(as)
        p(i)%QCplantetombe(aoas) = p(i)%QCplantetombe(ao) * p(i)%surf(ao) + p(i)%QCplantetombe(as) * p(i)%surf(as)

! Loic Octobre 2016 : le calcul de l'INN pour les cultures associees est active ssi il y a des cultures associees...
        if (p(i)%surf(ao) > 0.) then
          p(i)%inn(aoas) = min(p(i)%inn(ao), p(i)%inn(as))
          p(i)%inns(aoas) = min(p(i)%inns(ao), p(i)%inns(as))
          p(i)%innsenes(aoas) = min(p(i)%innsenes(ao), p(i)%innsenes(as))
          p(i)%innlai(aoas) = min(p(i)%innlai(ao), p(i)%innlai(as))
          p(i)%inni(aoas) = min(p(i)%inni(ao), p(i)%inni(as))
        endif

        call EnvoyerMsgDebug(logger, 'Stics_Jour: Ngrain')
      call EnvoyerMsgEcran(logger, 'Stics_Jour: Ngrain')

! DR 27/0/ ajout de irazomax qui est devenu un parametre plante
      call Ngrain(n,p(i)%ndrp,p(i)%nrec,p(i)%P_vitirazo(itk(i)%P_variete),p(i)%irazo(aoas,n-1),p(i)%nmat,  &
                    p(i)%dltags(aoas),p(i)%QNplantenp(aoas,n-1),p(i)%QNplantenp(aoas,n),p(i)%pgrain(aoas), &
      !DR 04/10/2021 j'enleve les 2 parametres devenu inutiles depuis la creation de P_irazomax
!                    p(i)%nbgraingel,p(i)%P_irmax,p(i)%P_vitircarb(itk(i)%P_variete),p(i)%magrain(aoas,n),  &
                    p(i)%nbgraingel,p(i)%magrain(aoas,n),  &
      !DR 30/09/2021 QNveg est superfetatoire et fait qu'on perd la valeur des valriables suivante, je vire
!                    p(i)%irazo(aoas,n),p(i)%QNgrain(aoas),p(i)%CNgrain(aoas),p(i)%QNveg,                   &
                    p(i)%irazo(aoas,n),p(i)%QNgrain(aoas),p(i)%CNgrain(aoas),                              &
                    p(i)%P_code_acti_reserve,p(i)%P_irazomax)

! TODO : Ngrain est appele 1 fois par plante, pour les deux parties AO et AS.
! Or, il calcul des variables qui sont separees AO et AS, comme QNgrain, irazo et CNgrain.
! Pour QNgrain, il n'y pas d'incidence dans la version actuelle, car il n'est utilise
! que pour sa partie AS. Mais CNgrain est utilise pour AO et AS, il est meme calcule
! dans eauqual et dans grain. Il faudrait verifier qu'il n'y a pas d'effet de bord.
! Ici, j'affecte en fait la partie commune AOAS, plutot que chacune des 2 composantes.
! TODO : peut etre qu'il vaudrait mieux finalement travailler sur la partie AS et laisser
!        le cumul AO/AS faire son travail ?
!        Parce qu'ici en reaffectant les composantes AO et AS, on risque de perdre des valeurs calculees precedemment.

! Dans le doute, j'affecte les composantes AO et AS aussi des sorties de Ngrain
        p(i)%CNgrain(ao) = p(i)%CNgrain(aoas)
        p(i)%CNgrain(as) = p(i)%CNgrain(aoas)

        p(i)%irazo(ao,n) = p(i)%irazo(aoas,n)
        p(i)%irazo(as,n) = p(i)%irazo(aoas,n)

        p(i)%QNgrain(ao) = p(i)%QNgrain(aoas)
        p(i)%QNgrain(as) = p(i)%QNgrain(aoas)
        ! Ajout Loic juin 2018 : On fait la meme chose pour magrain !
        p(i)%magrain(ao,n) = p(i)%magrain(aoas,n)
        p(i)%magrain(as,n) = p(i)%magrain(aoas,n)

! Ajout Loic Novembre 2016: calcul de QNveg
        p(i)%QNveg = p(i)%QNplantenp(aoas,n) - p(i)%QNgrain(aoas)

! Ajout Loic Aout 2016: mise i jour de la partition de l'azote entre structure et reserve dans les organes aeriens
        if (p(i)%P_code_acti_reserve == 1) then
           do ens = as, ao
              if (p(i)%surf(ens) > 0.) then
                call repartirN(n,p(i)%nrec,p(i)%P_parazofmorte,p(i)%P_parazotmorte,p(i)%inn(ens),p(i)%mafeuil(ens),       &
                              p(i)%mafeuilp(ens),p(i)%matigestruc(ens),p(i)%matigestrucp(ens),p(i)%absoaer(ens),      &
                              p(i)%dltaremobilN(ens),p(i)%QNgrain(ens),p(i)%QNvegstruc,p(i)%QNrestemp,p(i)%QNveg, &
                              p(i)%QNplantenp(ens,n),p(i)%QNfeuille,p(i)%QNtige,p(i)%P_codeplante)
              endif
           enddo
        endif

! Stress Exces d'eau
        call EnvoyerMsgDebug(logger, 'Stics_Jour: excesdeau')
        call excesdeau(n, p(i)%cumflrac, p(i)%rltotf+p(i)%rltotg, p(i)%P_sensanox, p(i)%racnoy(n-1),   &
                       p(i)%racnoy(n), p(i)%exofac, p(i)%izrac, p(i)%idzrac, p(i)%exolai,p(i)%exobiom)
      end do

!::: MicroClimat
    ! le calcul de temperature s'effectue une seule fois pour l'ensemble des systemes plantes
    ! *!* ATTENTION : ICI IL FAUT SOMMER LES EP DES PLANTES
    ! *!* TEMPSOL UNIQUE POUR TOUTES LES CULTURES
! replacing with sum for calculating  sc%eptcult
!      sc%eptcult = 0.0
!      do i = 1, sc%P_nbplantes
!        sc%eptcult = sc%eptcult + p(i)%ep(AS) + p(i)%ep(AO)
!      end do
        sc%eptcult = sum(p%ep(AS)) + sum(p%ep(AO))
      sc%hauteurMAX = 0.
      do i = 1, sc%P_nbplantes
        sc%hauteurMAX = max(sc%hauteurMAX,p(i)%hauteur(as))
        sc%hauteurMAX = max(sc%hauteurMAX,p(i)%hauteur(ao))
      end do
! replacing with sum for calculating sc%EmdTot
!      sc%EmdTot = 0.0
!      do i = 1, sc%P_nbplantes
!        sc%EmdTot = sc%EmdTot + p(i)%Emd
!      end do
      sc%EmdTot = sum(p%Emd)

      block
        real :: couvermulch, albedomulch

        call EnvoyerMsgDebug(logger, 'Stics_Jour: caltcult')
        if(itk(1)%P_codepaillage == 2) then
          !! paillage mulch plastique
          couvermulch = itk(1)%P_couvermulchplastique
          albedomulch = itk(1)%P_albedomulchplastique
        else
          couvermulch = sc%couvermulch
          albedomulch = pg%P_albedomulchresidus(sc%irmulch)
        endif
        call caltcult(itk(1)%P_codabri,sc%EmdTot,sc%hauteurMAX,sc%eptcult,sc%esol,sc%Emulch,soil%P_z0solnu,c%tmax(n), &
                    c%tvent(n),sc%raamin,sc%rnetS,p(1)%P_codebeso,c%phoi,sc%raamax,                                   &
                    sc%tcult,sc%tcultmin,sc%tcultmax,sc%et,c%tutilrnet,sc%numdate,c%daylen,c%difftcult,               &
                    c%nitetcult(n),sc%Ratm,                                                                           &
                    nbCouchesSol,sc%jul,sc%hur(1:nbCouchesSol),sc%humin(1:nbCouchesSol),                     &
                    sc%hucc(1:nbCouchesSol),sc%laiTot,sc%tauxcouv(n),sc%posibsw,soil%P_albedo,                     &
                    couvermulch,albedomulch,sta%P_albveg,sta%P_codernet,sta%P_aangst,                                 &
                    sta%P_bangst,c%tmin(n),c%tmoy(n),sta%P_corecTrosee,c%trg(n),sta%P_latitude,itk(1)%P_codepaillage, &
                    p(1)%P_codelaitr, c%tpm(n),sta%P_codecaltemp,sc%albedolai,sc%rglo,sc%rnet)
      end block
      call EnvoyerMsgDebug(logger, 'Stics_Jour: tempsol')
      sc%tsol(1:soil%profsol) = tempsol(sc%tcultmin,sc%tcultmax,pg%P_diftherm,soil%profsol,          &
                   sc%tsolveille(1:soil%profsol),sc%tcultveille,c%tmin(n))                                  ! sorties

      sc%tsolveille(1:soil%profsol) = sc%tsol(1:soil%profsol)
      sc%tsol(0) = sc%tsol(1)
      sc%tcultveille = sc%tcult
      sc%tairveille = c%tmoy(n)

      if (pg%P_codeh2oact == 0) sc%tcult = c%tmoy(n)

! replacing with sum for calculating sc%epTot
!      sc%epTot = 0.0
!      do i = 1,sc%P_nbplantes
!        sc%epTot = sc%epTot + p(i)%ep(AS) + p(i)%ep(AO)
!      end do
        sc%epTot = sum(p%ep(AS)) + sum(p%ep(AO))
      if (itk(1)%P_codabri /= 2) then
        sc%et = sc%esol + sc%EmdTot + sc%epTot
      endif

      call EnvoyerMsgEcran(logger, 'Stics_Jour: humcouv')
      call humcouv(c%tmoy(n),sc%tcult,c%tpm(n),sta%P_ra,sc%Edirect,0.,sc%epTot,sc%rnet,sta%P_patm,sc%humidite,c%humair)

      if (pg%P_codemicheur == 1) then
         sc%numdate = sc%jul
         call EnvoyerMsgEcran(logger, 'Stics_Jour: photpd')
         call photpd(sta%P_latitude,sc%numdate,c%daylen,c%phoi)

         call EnvoyerMsgEcran(logger, 'Stics_Jour: humheure')
         if (n > 1) tmaxveille = sc%tcultveille * 2 - c%tmin(n-1)
         if (n == 1) tmaxveille = sc%tcultmax

         call humheure(n,sc%tcultmin,sc%tcultmax,tmaxveille,sc%humidite,c%daylen,c%tmin(n+1),sc%tcult,    &
                       sc%trosemax(0:n),c%humimoy,c%tmin(n),t%P_codetrosee,tculth,troseh,humh)
      endif

!::: Cumul AO/AS
      call EnvoyerMsgEcran(logger, 'Stics_Jour: cumAOetAS')
      do i = 1,sc%P_nbplantes
        call cumAOetAS(n,sc%P_codesimul,p(i),itk(i))
! Bruno juin 2017
        p(i)%QNplante = p(i)%QNplantenp(aoas,n) + p(i)%QNperenne(aoas)
        p(i)%QNtot = p(i)%QNplante + p(i)%QNrac
      end do

!::: Fonction d'ecriture des sorties journalieres
    ! Nombre total d'apport (irrig et fertil)
      sc%naptot  = 0
      sc%napNtot = 0
      sc%napNtot = SUM(itk(:)%napN)
      sc%naptot  = SUM(itk(:)%nap)

! DR et FR 31/05/2016 on se demande bien pourquyoi j'ai fait ca , je begaye , faut que je parte a la retraite

! Modif Loic Mars 2017 : je decale le call de GestionDesCoupes apres l'ecriture des variables
! de sorties journalieres sinon le modele fait n'importe quoi!
!      call GestionDesCoupes(sc,pg,c,sta,soil,p,itk,t)

       call Stics_Jour_after(logger,sc,pg,c,sta,soil,p,itk,t)

       call EnvoyerMsgEcran(logger, 'apres stics_jour_after ' // to_string(n))
       call EnvoyerMsgEcran(logger, 'nbgrains ' // to_string(p(1)%nbgrains(0)))
       call EnvoyerMsgEcran(logger, 'chargefruit ' // to_string(p(1)%chargefruit))
       call EnvoyerMsgEcran(logger, 'magrain ' // to_string(p(1)%magrain(0,n)))
       call EnvoyerMsgEcran(logger, 'msracmort ' // to_string(p(1)%msracmort))
       call EnvoyerMsgEcran(logger, 'QNracmort ' // to_string(p(1)%QNracmort))
       

! Bruno juin 2017. Je mets les variables cumulees sur les 2 plantes dans sc% mais ce n'est pas ideal
!  Quantite totale de N absorbe ou fixe par les 2 plantes
      ! For plant first 1
      sc%QNabso2 = p(1)%QNabso(aoas)
      sc%Qfixtot2 = p(1)%Qfixtot(0)
! variation de N total des 2 plantes entre le debut et la fin de simulation
      sc%QNtot2 = p(1)%QNtot
      sc%DQNtot2 = p(1)%QNtot - p(1)%QNtot0
! Quantites totales de N exporte
      sc%qexport2 = p(1)%qexport
      sc%QNexport2 = p(1)%QNexport
      sc%QNfauchetot2 = p(1)%QNfauchetot
! Quantites totales de N et C restitues au sol
      sc%QNressuite2 = p(1)%QNressuite
      sc%QCressuite2 = p(1)%QCressuite
      sc%QNressuite_tot2 = p(1)%QNressuite_tot
      sc%QCressuite_tot2 = p(1)%QCressuite_tot
      sc%QNplantetombe2 = p(1)%QNplantetombe(aoas)
      sc%QCplantetombe2 = p(1)%QCplantetombe(aoas)
      sc%QNrogne2 = p(1)%QNrogne
      sc%QCrogne2 = p(1)%QCrogne
      sc%QNracmort2 = p(1)%QNracmort
      sc%QCracmort2 = p(1)%QCracmort
      sc%QNperennemort2 = p(1)%QNperennemort(aoas)
      sc%QCperennemort2 = p(1)%QCperennemort(aoas)
! transpiration
      sc%cep2 = p(1)%cep

      ! Only if plant number == 2
      if (sc%P_nbplantes > 1) then
        sc%QNabso2 = sc%QNabso2 + p(2)%QNabso(aoas)
        sc%Qfixtot2 = sc%Qfixtot2 + p(2)%Qfixtot(0)
  ! variation de N total des 2 plantes entre le debut et la fin de simulation
        sc%QNtot2 = sc%QNtot2 + p(2)%QNtot
        sc%DQNtot2 = sc%DQNtot2 + p(2)%QNtot - p(2)%QNtot0
  ! Quantites totales de N exporte
        sc%qexport2 = sc%qexport2 + p(2)%qexport
        sc%QNexport2 = sc%QNexport2 + p(2)%QNexport
        sc%QNfauchetot2 = sc%QNfauchetot2 + p(2)%QNfauchetot
  ! Quantites totales de N et C restitues au sol
        sc%QNressuite2 = sc%QNressuite2 + p(2)%QNressuite
        sc%QCressuite2 = sc%QCressuite2 + p(2)%QCressuite
        sc%QNressuite_tot2 = sc%QNressuite_tot2 + p(2)%QNressuite_tot
        sc%QCressuite_tot2 = sc%QCressuite_tot2 + p(2)%QCressuite_tot
        sc%QNplantetombe2 = sc%QNplantetombe2 + p(2)%QNplantetombe(aoas)
        sc%QCplantetombe2 = sc%QCplantetombe2 + p(2)%QCplantetombe(aoas)
        sc%QNrogne2 = sc%QNrogne2 + p(2)%QNrogne
        sc%QCrogne2 = sc%QCrogne2 + p(2)%QCrogne
        sc%QNracmort2 = sc%QNracmort2 + p(2)%QNracmort
        sc%QCracmort2 = sc%QCracmort2 + p(2)%QCracmort
        sc%QNperennemort2 = sc%QNperennemort2 + p(2)%QNperennemort(aoas)
        sc%QCperennemort2 = sc%QCperennemort2 + p(2)%QCperennemort(aoas)
  ! transpiration
        sc%cep2 = sc%cep2 + p(2)%cep
      endif

! Calcul des surplus N
      sc%Nsurplus_min = sc%precipN + sc%irrigN + sc%totapN + sc%Qfixtot2 - sc%QNexport2 ! - sc%QNfauchetot2
      sc%Nsurplus = sc%Nsurplus_min + sc%QNresorg

      sc%SON0 = sc%Nhuma0+ sc%Nhumi0 + sc%Nb0
      sc%SON =  sc%Nhuma + sc%Nhumi + sc%Nb
      sc%SOC0 = sc%Chuma0+ sc%Chumi0 + sc%Cb0
      sc%SOC =  sc%Chuma + sc%Chumi + sc%Cb
      sc%SONtot0 = sc%Nhuma0+ sc%Nhumi0 + sc%Nb0 + sc%Nbmulch0 + sc%Nr0 + sc%Nrprof0 + sc%Nmulch0
      sc%SONtot =  sc%Nhuma + sc%Nhumi + sc%Nb  + sc%Nbmulch  + sc%Nr  + sc%Nrprof  + sc%Nmulch
      sc%SOCtot0 = sc%Chuma0 + sc%Chumi0 + sc%Cb0 + sc%Cbmulch0 + sc%Cr0 + sc%Crprof0 + sc%Cmulch0
      sc%SOCtot =  sc%Chuma  + sc%Chumi  + sc%Cb  + sc%Cbmulch  + sc%Cr  + sc%Crprof  + sc%Cmulch
      sc%SOCinputs = sc%QCresorg + sc%QCressuite_tot2 + sc%QCplantetombe2 + sc%QCrogne2 + sc%QCracmort2 + sc%QCperennemort2
      sc%SOCoutputs = sc%QCO2sol
      sc%SONinputs = sc%QNresorg + sc%QNressuite_tot2 + sc%QNplantetombe2+ sc%QNrogne2 + sc%QNracmort2 + sc%QNperennemort2 &
                   + soil%QNorgeng
      sc%SONoutputs = sc%QNprimed + sc%Qminh + sc%Qminr
      sc%GHG = -(sc%SOC - sc%SOC0)*44./12. + sc%Qem_N2O *44./28.*298.
        QNO3i = 0.
        QNH4i = 0.
        QNO3f = 0.
        QNH4f = 0.
        nhel = 0
        do i = 1,sc%NH   ! 5
          QNO3i = QNO3i + soil%NO3init(i)
          QNO3f = QNO3f + soil%AZnit(i)
          QNH4i = QNH4i + sc%NH4init(i)
          QNH4f = QNH4f + sc%AZamm(i)
          sc%SOCL(i) = 0.
          sc%SONL(i) = 0.
           izmax = int(soil%P_epc(i))
           do iz = 1,izmax
               if(nhel + iz <= ihum) then
                  sc%SOCL(i) = sc%SOCL(i) + sc%Chum(iz+nhel) + sc%Chumi/ihum !+ sum(sc%Cbio(iz+nhel,1:nbRes))
                  sc%SONL(i) = sc%SONL(i) + sc%Nhum(iz+nhel) + sc%Nhumi/ihum !+ sum(sc%Nbio(iz+nhel,1:nbRes))
               endif
           end do
           nhel = nhel + izmax
        end do
! Ajout Bruno fevrier 2018 - ces variables etaient calculees precedemment dans Lixiv
        sc%resmes = SUM(sc%hur(1:soil%profcalc) + sc%sat(1:soil%profcalc))
        sc%azomes = SUM(soil%nit(1:soil%profcalc))
        sc%ammomes = SUM(soil%amm(1:soil%profcalc))
        sc%SMNmes = sc%azomes + sc%ammomes

        sc%SMN = QNO3f + QNH4f
        sc%STN0 = sc%SONtot0 + QNH4i + QNO3i
        sc%STN = sc%SONtot + QNH4f + QNO3f
        soil%QNgaz = soil%QNvoleng + soil%QNvolorg + soil%QNdenit + sc%Qem_N2Onit

! ****************************************************************
! DR 23/09/2016 on met l'appel aux sorties journalieres la plutot que dans stics_jour_after sinon incompatibilite avec Record
      do i= 1, sc%P_nbplantes
      ! DR 13/01/06 pour simplifier les sorties je mets tauxrecouv ou lai(aoas) dans une var temporaire
      ! taux de couverture uniquement pour culture pure, donc p(1).
        if (p(1)%P_codelaitr /= 1) then
          p(1)%lai(AOAS,n) = sc%tauxcouv(n)
        endif

        if (iand(pg%P_flagEcriture, ECRITURE_SORTIESJOUR) > 0) then
          block
            real, allocatable :: values(:)
            values = CorrespondanceVariablesDeSorties(sc%daily_var_names, sc, p(i), soil, c, sta)
            call write_row(p(i)%daily_unit, sc%daily_formats,&
                sc%annee(sc%jjul), sc%nummois, sc%jour, sc%jul, p(i)%P_codeplante, values)
          end block
        endif
	
! DR 07/09/2011 j'joute la possibilite d'ecrire dans un fichier st3 pour avoir le vrai format agmip
! DR 29/08/2012 j'ajoute un code pour garder les sorties Agmip t%P_Config_Output : 1=rien , 2 sorties ecran , 3 sorties agmpi
        if (iand(pg%P_flagEcriture, ECRITURE_AGMIP) > 0) then
          call Ecriture_VariablesSortiesJournalieres_st3(logger, sc,pg,p(i),soil,c,sta,ag)
        endif
      enddo

!**********************************************************************************
! Modif Loic Mars 2017 : appel a GestionDesCoupes place ici
      call EnvoyerMsgEcran(logger, 'Stics_Jour: gestion des coupes')
      call GestionDesCoupes(logger, sc, pg, p, r, itk, soil, c, sta, t)

      ! For plant 1 first 
      sc%QNexport2 = p(1)%QNexport
      sc%QNfauchetot2 = p(1)%QNfauchetot
! Quantites totales de N et C restitues au sol
      sc%QNressuite2 = p(1)%QNressuite
      sc%QCressuite2 = p(1)%QCressuite
      sc%QNressuite_tot2 = p(1)%QNressuite_tot
      sc%QCressuite_tot2 = p(1)%QCressuite_tot
      sc%QNplantetombe2 = p(1)%QNplantetombe(aoas)
      sc%QCplantetombe2 = p(1)%QCplantetombe(aoas)
      sc%QNrogne2 = p(1)%QNrogne 
      sc%QCrogne2 = p(1)%QCrogne
      sc%QNracmort2 = p(1)%QNracmort
      sc%QCracmort2 = p(1)%QCracmort
      sc%QNperennemort2 = p(1)%QNperennemort(aoas)
      sc%QCperennemort2 = p(1)%QCperennemort(aoas)

      ! Only if plant number == 2
      if (sc%P_nbplantes > 1) then
        sc%QNexport2 = sc%QNexport2 + p(2)%QNexport
        sc%QNfauchetot2 = sc%QNfauchetot2 + p(2)%QNfauchetot
  ! Quantites totales de N et C restitues au sol
        sc%QNressuite2 = sc%QNressuite2 + p(2)%QNressuite
        sc%QCressuite2 = sc%QCressuite2 + p(2)%QCressuite
        sc%QNressuite_tot2 = sc%QNressuite_tot2 + p(2)%QNressuite_tot
        sc%QCressuite_tot2 = sc%QCressuite_tot2 + p(2)%QCressuite_tot
        sc%QNplantetombe2 = sc%QNplantetombe2 + p(2)%QNplantetombe(aoas)
        sc%QCplantetombe2 = sc%QCplantetombe2 + p(2)%QCplantetombe(aoas)
        sc%QNrogne2 = sc%QNrogne2 + p(2)%QNrogne
        sc%QCrogne2 = sc%QCrogne2 + p(2)%QCrogne
        sc%QNracmort2 = sc%QNracmort2 + p(2)%QNracmort
        sc%QCracmort2 = sc%QCracmort2 + p(2)%QCracmort
        sc%QNperennemort2 = sc%QNperennemort2 + p(2)%QNperennemort(aoas)
        sc%QCperennemort2 = sc%QCperennemort2 + p(2)%QCperennemort(aoas)
      endif
! Loic Novembre 2017: j'ajoute le Nsurplus ici pour mise a jours pour les fourrages :
      sc%Nsurplus_min = sc%precipN + sc%irrigN + sc%totapN + sc%Qfixtot2 - sc%QNexport2
      sc%Nsurplus = sc%Nsurplus_min + sc%QNresorg
!::: Dynamique des talles
     do i = 1,sc%P_nbplantes
        if (p(i)%P_codedyntalle == 1) then
          block
            real :: current_transpi, max_transpi
            call EnvoyerMsgDebug(logger, 'Stics_Jour: dynamique des talles')
            if (p(i)%P_codetranspitalle == 1) then
              current_transpi = sc%et
              max_transpi = sc%etm
            else
              current_transpi = p(i)%ep(aoas)
              max_transpi = p(i)%eop(aoas)
            end if
            call dynamictalle_new(p(i)%P_SurfApex,p(i)%P_SeuilMorTalle,p(i)%P_SigmaDisTalle,              &
                              p(i)%P_VitReconsPeupl,p(i)%P_SeuilReconsPeupl,p(i)%P_MaxTalle,              &
                              p(i)%densite,current_transpi,max_transpi,p(i)%LAIapex,p(i)%P_SeuilLAIapex,  &
                              p(i)%tempeff,p(i)%mortalle,p(i)%lai(aoas,n)-p(i)%lai(aoas,n-1),             &
                              p(i)%restemp(aoas),p(i)%mortreserve,p(i)%deltai(aoas,n),                    &
                              p(i)%lai(aoas,n),p(i)%densitemax,p(i)%masecnp(0,n),                         &
                              p(i)%mortmasec,p(i)%drlsenmortalle)

            if (p(i)%densite < 1.0) then
              call EnvoyerMsgHistorique(logger, MESSAGE_164,n)
            endif
          end block
        endif
     end do

     call EnvoyerMsgEcran(logger, 'end day ' // to_string(n))
    !::: Ecriture des rapports
      if (iand(pg%P_flagEcriture, ECRITURE_RAPPORTS) > 0) then
        call EnvoyerMsgDebug(logger, 'Stics_Jour: reports')

     do i = 1,sc%P_nbplantes
        if (sc%codeaucun == 1) then
           sc%ecritrap = .FALSE.
           if (sc%codetyperap == 1 .or. sc%codetyperap == 3) then
              do iz = 1, sc%nboccurrap
                sc%daterap(iz) = day_number(sc%date_calend_rap(iz,3),sc%date_calend_rap(iz,2),sc%date_calend_rap(iz,1))
        ! DR 10/03/2014 la gestion des dates calendaire dans le rapport avait ete fait un peu vite , y'a pb  dans le cas ou l'usms est sur 2 ans ,
        ! du coup le jour julien de la date de la deuxieme annee est mauvais
        ! comme a pas acces a l'annee ni a quoi que ce soit a ce stade du programme (dans lecture_variable_rapport) , je mets ici la conversion
! DR 01122020 merge trunk
        ! DR 14/11/2019 je corrige le bug detecte par sebastien . si on est en enchainement climatique et qu'on a mis des dates de sorties du rapport ca bug .

                if(sc%P_culturean.eq.1)then
                   sc%date_calend_rap(i,1)=sc%ansemis
                else
                  ! write(*,*)'== ', sc%daterap(iz)
                   if(sc%daterap(iz).gt.sc%P_iwater)then
                        ! on est en annee 1
                        sc%date_calend_rap(iz,1)=sc%ansemis
                      !  write(*,*) 'annee 1 ', days_count(sc%ansemis),sc%date_calend_rap(iz,1),sc%daterap(iz)
                   else
                   ! DR on est en annee 2
                        sc%date_calend_rap(iz,1)=sc%ansemis+1
                       if(is_leap_year(sc%ansemis))then
                           sc%daterap(iz)= sc%daterap(iz)+366
                       else
                           sc%daterap(iz)= sc%daterap(iz)+365
                       endif
                       ! write(*,*) 'annee 2 ', days_count(sc%ansemis),sc%date_calend_rap(iz,1),sc%daterap(iz)
                   endif
                 endif
          !      if(sc%ansemis.ne.sc%date_calend_rap(iz,1))then
          !        if(isBissextile(sc%ansemis))then
          !           sc%daterap(iz)= sc%daterap(iz)+366
          !        else
          !           sc%daterap(iz)= sc%daterap(iz)+365
          !        endif
          !      endif
                if ((n == sc%daterap(iz)-sc%P_iwater+1) .and. (sc%ecritrap.eqv..FALSE.)) &
                  call Ecriture_Rapport(logger,sc,pg,soil,c,sta,p(i),t, .TRUE.,i)
              end do
            endif
            if ((sc%codetyperap == 2 .or. sc%codetyperap == 3) .and. (sc%ecritrap.eqv..FALSE.)) then
              should_write_report = .false.
              if ((p(i)%nplt == n) .and. sc%rapplt) should_write_report = .true.
              if ((p(i)%nger == n) .and. sc%rapger) should_write_report = .true.
              if ((p(i)%nlev == n) .and. sc%raplev) should_write_report = .true.
              if ((p(i)%namf == n) .and. sc%rapamf) should_write_report = .true.
              if ((p(i)%nlax == n) .and. sc%raplax) should_write_report = .true.
              if ((p(i)%ndrp == n) .and. sc%rapdrp) should_write_report = .true.
              if ((p(i)%nflo == n) .and. sc%rapflo) should_write_report = .true.
              if ((p(i)%nsen == n) .and. sc%rapsen) should_write_report = .true.
              if ((p(i)%nrec == n) .and. sc%raprec) should_write_report = .true.
              if ((p(i)%nmat == n) .and. sc%rapmat) should_write_report = .true.
              if ((p(i)%ndebdorm == n) .and. sc%rapdebdorm) should_write_report = .true.
              if ((p(i)%nfindorm == n) .and. sc%rapfindorm) should_write_report = .true.
            ! dr 12/08/08 pour les perennes le stade debourrement est la levee
              if (p(i)%P_codeperenne == 2 .and. p(i)%nlev == n .and. sc%rapdebdebour) should_write_report = .true.
              
              if(should_write_report) then
                call Ecriture_Rapport(logger,sc,pg,soil,c,sta,p(i),t, .FALSE.,i)
              end if
            endif
          endif
     end do
   endif
end subroutine Stics_Jour
end module Stics_Jour_m
 
