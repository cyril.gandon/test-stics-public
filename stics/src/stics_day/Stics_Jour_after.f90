module Stics_Jour_after_m
   use stics_files
   use Stics
   use Plante
   use Itineraire_Technique
   use Parametres_Generaux
   use Climat
   use Station
   use Sol
   use messages
   use Stics_Calcul_Sorties_m, only: Stics_Calcul_Sorties
   use Stics_Calcul_Sorties_m, only: Stics_Calcul_Sorties_Plante
   implicit none
   private
   public :: Stics_Jour_after
contains
   subroutine Stics_Jour_after(logger, sc, pg, c, sta, soil, p, itk, t)
      type(logger_), intent(in) :: logger
      type(Stics_Communs_), intent(INOUT) :: sc
      type(Parametres_Generaux_), intent(IN)    :: pg
      type(Climat_), intent(INOUT) :: c
      type(Station_), intent(INOUT) :: sta
      type(Plante_), intent(INOUT) :: p(sc%P_nbplantes)
      type(ITK_), intent(INOUT) :: itk(sc%P_nbplantes)
      type(Stics_Transit_), intent(INOUT) :: t
      type(Sol_), intent(INOUT) :: soil

      integer :: ipl, n, profsol_int, kk

      call EnvoyerMsgEcran(logger, 'beginning stics_jour_after ')
      n = sc%n

      ! preparation des variables pour les ecritures. Par defaut, on utilise les valeurs de la plante principale

      call Stics_Calcul_Sorties(logger, sc, c, soil, p(1), itk(1), pg, sta)

      ! preparations des variables plantes pour les ecritures.
      do ipl = 1, sc%P_nbplantes
         call Stics_Calcul_Sorties_Plante(sc, pg, c, p(ipl), itk(ipl), soil, t)
      end do

      do ipl = 1, sc%P_nbplantes
         if (p(1)%P_codelaitr /= 1) p(1)%lai(AOAS, n) = sc%tauxcouv(n)
! DR 23/09/2016 pour Efese prairie on garde 3 variables correspondants aux maxi atteint le jour de la coupe
         p(ipl)%lai_mx_av_cut = p(ipl)%lai(AOAS, sc%n)
         p(ipl)%masec_mx_av_cut = p(ipl)%masecnp(AOAS, sc%n)
         p(ipl)%QNplante_mx_av_cut = p(ipl)%QNplantenp(AOAS, sc%n)

         ! on enregistre certaines valeurs de la plante pour des jours particuliers
         ! ca permet de supprimer certains tableaux temporels qui stockent toute la simulation et de ne conserver que les
         ! valeurs utiles
         if (sc%n == p(ipl)%ntaille - 1) then
            p(ipl)%QNgrain_ntailleveille = p(ipl)%QNgrain(AOAS)
         end if
      end do

      call EnvoyerMsgEcran(logger, 'end stics_jour_after ')
   end subroutine Stics_Jour_after
end module

