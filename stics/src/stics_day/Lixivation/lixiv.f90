!!   lixiv - Routine principale du module lixivation
!!   Module : lixivation
!!   Typologie : scientifique
!!
!!   Simulation du transfert d'eau et de nitrate dans le sol
!!       Processus simules:
!!         - ruissellement
!!         - infiltration dans la macroporosite
!!         - exces d'eau
!!         - ecoulement dans les drains
!!   Introduction de l'epaisseur de dispersion P_epd
module lixiv_m
use messages
USE TransfertsDescendants_m, only: TransfertsDescendants
USE excesEau_m, only: excesEau
USE nappe_m, only: nappe
USE drainageAgricole_m, only: drainageAgricole
USE calculRsurRU_m, only: calculRsurRU
USE calculRU_m, only: calculRU
use soil_utils
implicit none
private
public :: lixiv
contains
subroutine lixiv(&
  logger, n, nhe, nh, ncel, P_codefente, P_codemacropor, P_codrainage, P_codhnappe, profcalc, P_infil, P_concseuil, &
                 P_humcapil, P_capiljour, P_profdrain, P_bformnappe, P_distdrain, P_rdrain, P_profimper, P_ksol, profsol,  &
                 ldrains, precip, hurlim, ruisselsurf, zrac, icel, P_epc, da, hr, hcc, hmin, izcel, izc, exces, hucc, humin, &
                 epz, esz, irrigprof, hur, sat, anox, nit, bouchon, pluieruissel, saturation, RU, RsurRU, concno3les, hnappe,&
                 hmax, hph, hpb, profnappe, qdrain, azlesd, azsup, DRAT, QLES, drain, ruissel, remontee, qlesd, qdraincum,   &
                 P_profmesW, SoilAvW, SoilWatM, lessiv, som_HUR, som_sat,         & ! ajout entrees et des sorties Macsur
                 P_profmes, husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)
!call lixiv(sc%n, sc%nhe, sc%nh, soil%ncel, soil%P_codefente, soil%P_codemacropor, soil%P_codrainage, pg%P_codhnappe, &
!           soil%profcalc, soil%P_infil(0:sc%nh), soil%P_concseuil, soil%P_humcapil, soil%P_capiljour, soil%P_profdrain,    &
!           pg%P_bformnappe, pg%P_distdrain, pg%P_rdrain, soil%P_profimper, soil%P_ksol, soil%profsol, soil%ldrains,        &
!           sc%precip, sc%hurlim, sc%ruisselsurf, zracMAXI, soil%icel(0:soil%ncel), soil%P_epc(1:sc%nh), soil%da(1:sc%nh),  &
!           sc%hr(1:sc%nh), soil%hcc(1:sc%nh), soil%hmin(1:sc%nh), soil%izcel(1:sc%nh), soil%izc(1:sc%nh), sc%exces(0:sc%nh),  &
!           sc%hucc(1:sc%nhe), sc%humin(1:sc%nhe), epzTot(1:sc%nhe), sc%esz(1:sc%nhe), p(1)%irrigprof(1:sc%nhe),            &
!           sc%hur(1:sc%nhe), sc%sat(1:sc%nhe), sc%anox(1:sc%nhe), soil%nit(1:sc%nhe), sc%bouchon, sc%pluieruissel,         &
!           sc%saturation, sc%RU, sc%RsurRU, soil%concno3les, soil%hnappe, soil%hmax, soil%hph, soil%hpb, soil%profnappe,   &
!           soil%qdrain, soil%azlesd, sc%azsup, sc%DRAT, sc%QLES, sc%drain, sc%ruissel, soil%remontee, soil%qlesd,          &
!           soil%qdraincum, sc%P_profmesW, sc%SoilAvW, sc%SoilWatM, sc%lessiv, sc%som_HUR, sc%som_sat, soil%P_obstarac,     &
!           soil%remonteemax)


  type(logger_), intent(in) :: logger
  integer, intent(IN)    :: n              ! simulation day
  integer, intent(IN)    :: nhe            ! number of elementary soil layers
  integer, intent(IN)    :: nh             ! number of macro soil layers
  integer, intent(IN)    :: ncel           ! number of mixing cells
  integer, intent(IN)    :: P_codefente    ! // PARAMETER // option allowing an additional water compartment for the swelling soils: yes (1), no (0) // code 0/1 // PARSOL // 0
  integer, intent(IN)    :: P_codemacropor ! // PARAMETER // Simulation option of water flux in the macroporosity of soils to estimate water excess and drip by  overflowing : yes(1), no (0) // code 1/2 // PARSOL // 0
  integer, intent(IN)    :: P_codrainage   ! // PARAMETER // artificial drainage (yes or no) // code 0/1 // PARSOL // 0
  integer, intent(IN)    :: P_codhnappe    ! // PARAMETER // mode of calculation of watertable level // code 1/2 // PARAM // 0
  integer, intent(IN)    :: profcalc
  real,    intent(IN)    :: P_infil(0:nh)  ! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor = 1)// mm d-1
  real,    intent(IN)    :: P_concseuil    ! // PARAMETER // Minimum concentration of NH4-N in soil // kg ha-1 mm-1 // PARSOL // 1
  real,    intent(IN)    :: P_humcapil     ! // PARAMETER // maximal water content to activate capillary rise // % weight // PARSOL // 1
  real,    intent(IN)    :: P_capiljour    ! // PARAMETER // capillary rise upward water flux // mm d-1 // PARSOL // 1
  integer, intent(IN)    :: P_profdrain    ! // PARAMETER // drain depth // cm // PARSOL // 1
  real,    intent(IN)    :: P_bformnappe   ! // PARAMETER // coefficient of water table shape (artificial drained soil) // SD // PARAM // 1
  real,    intent(IN)    :: P_distdrain    ! // PARAMETER // distance to the drain to calculate watertable height // cm // PARAM // 1
  real,    intent(IN)    :: P_rdrain       ! // PARAMETER // drain radius // cm // PARAM // 1
  real,    intent(IN)    :: P_profimper    ! // PARAMETER // Upper depth of the impermeable layer (from the soil surface). May be greater than the soil depth // cm // PARSOL // 1
  real,    intent(IN)    :: P_ksol         ! // PARAMETER // hydraulic conductivity in the soil above and below the drains // SD // PARSOL // 1
  integer, intent(IN)    :: profsol
  real,    intent(IN)    :: ldrains
  real,    intent(IN)    :: precip         ! // INPUT // Daily amount of water (precipitation + irrigation)   // mm d-1
  real,    intent(IN)    :: hurlim
  real,    intent(IN)    :: ruisselsurf    ! // INPUT // Surface run-off // mm d-1
  real,    intent(IN)    :: zrac           ! // INPUT // Depth reached by root system // cm
  integer, intent(IN)    :: icel(0:ncel)
  integer, intent(IN)    :: P_epc(nh)      ! // PARAMETER // thickness of macro layer i // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,    intent(IN)    :: da(nh)         ! // OUTPUT // Bulk density of macro layer i   // g cm-3
  real,    intent(IN)    :: hr(nh)         ! // OUTPUT // Water content of macro layer i  // % weight
  real,    intent(IN)    :: hcc(nh)        ! water content at field capacity of macro layer i  // % weight
  real,    intent(IN)    :: hmin(nh)       ! water content at permanent wilting point of macro layer i // % weight
  integer, intent(IN)    :: izcel(nh)      !
  integer, intent(IN)    :: izc(nh)        !
  real,    intent(OUT)   :: exces(0:nh)    ! // OUTPUT // Amount of water present in the macroporosity of the layer i  // mm
  real,    intent(IN)    :: hucc(nhe)      ! water content at field capacity of layer i // mm
  real,    intent(IN)    :: humin(nhe)     ! water content at permanent wilting point of layer i // mm
  real,    intent(IN)    :: epz(nhe)  
  real,    intent(IN)    :: esz(nhe)  
  real,    intent(IN)    :: irrigprof(nhe)
  real,    intent(INOUT) :: hur(nhe)
  real,    intent(OUT)   :: sat(nhe)
  real,    intent(OUT)   :: anox(nhe)
  real,    intent(INOUT) :: nit(nhe)

  integer, intent(INOUT) :: bouchon     ! // OUTPUT // Index showing if the shrinkage slots are opened (0) or closed (1)  // 0-1

! variables calculees et utilisees par lixiv et ses sous-fonctions et globalisees (pour les fonctions de sortie lecrap, lecsorti)
  real,    intent(OUT)   :: pluieruissel  
  real,    intent(OUT)   :: saturation  ! // OUTPUT // Amount of water remaining in the soil macroporosity // mm
  real,    intent(OUT)   :: RU          ! // OUTPUT // maximum available water reserve over the entire profile // mm
  real,    intent(OUT)   :: RsurRU      ! // OUTPUT // Fraction of available water reserve (R/RU) over the entire profile // 0 to 1
  real,    intent(OUT)   :: concno3les  ! // OUTPUT // Nitrate concentration in drained water // mg NO3 l-1
  real,    intent(OUT)   :: hnappe      ! // OUTPUT // Height of water table with active effects on the plant // cm
  real,    intent(OUT)   :: hmax        ! // OUTPUT // Maximum height of water table between drains // cm
  real,    intent(OUT)   :: hph         ! // OUTPUT // Maximum depth of perched water table // cm
  real,    intent(OUT)   :: hpb         ! // OUTPUT // Minimum depth of perched water table // cm
  real,    intent(OUT)   :: profnappe   ! // OUTPUT // Depth of water table // cm
  real,    intent(OUT)   :: qdrain      ! // OUTPUT // Flow rate towards drains // mm d-1
  real,    intent(OUT)   :: azlesd      ! // OUTPUT // Daily NO3-N leached into drains // kg.ha-1 d-1
  real,    intent(OUT)   :: azsup
  real,    intent(INOUT) :: DRAT        ! // OUTPUT // Water flux drained at the base of the soil profile integrated over the simulation period out of the soil   // mm d-1
  real,    intent(INOUT) :: QLES        ! // OUTPUT // Cumulative NO3-N leached at the base of the soil profile // kg.ha-1
  real,    intent(OUT)   :: drain       ! // OUTPUT // Water flux drained at the base of the soil profile // mm d-1
  real,    intent(OUT)   :: ruissel     ! // OUTPUT // Daily run-off // mm d-1
  real,    intent(OUT)   :: remontee    ! // OUTPUT // Capillary uptake in the base of the soil profile // mm d-1
  real,    intent(OUT)   :: qlesd       ! // OUTPUT // Cumulative NO3-N leached into drains // kg.ha-1
  real,    intent(INOUT) :: qdraincum   ! // OUTPUT // Cumulative quantity of water evacuated towards drains // mm

! dr 16/12/2013 ajout pour Macsur
  integer, intent(IN)    :: P_profmesW
  real,    intent(INOUT) :: SoilAvW     ! // OUTPUT // Amount of soil available water in the measurement depth // mm
  real,    intent(INOUT) :: SoilWatM    ! // OUTPUT // Amount of soil available water in the observed measurement (macsur) // mm
  real,    intent(INOUT) :: lessiv      ! // OUTPUT // daily NO3-N leached at the base of the soil profile // kg.ha-1
  real,    intent(INOUT) :: som_HUR
  real,    intent(INOUT) :: som_sat
  !real,    intent(IN)    :: P_obstarac
  !real,    intent(INOUT) :: remonteemax ! // OUTPUT // capillary rise from simulated deep soil layer (mm)

!DR 01122020 merge trunk r2382
! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real,    intent(IN)    :: P_profmes
!  integer, intent(IN)    :: iz_base_horizon(1:5)
  real,    intent(OUT)   :: husup_by_horizon(5)
  real,    intent(OUT)   :: azsup_by_horizon(5)
  real,    intent(OUT)   :: husup_under_profmes
  real,    intent(OUT)   :: azsup_under_profmes
 ! DR 25/06/2019 pour le module chloride il faut sortir les veluers de drainage de chaque couche de 1 cm
  real,    intent(OUT)   :: husup_by_cm(nhe)


!! 3- VARIABLES LOCALES
  integer               :: profhoriz  !
  integer               :: ic  !  
  integer               :: ii  !  
  integer               :: iz  
  real                  :: husup  !
  real                  :: de  !
  real                  :: pi  !
  real                  :: delt
  real, dimension(nh)   :: macropor
  real, dimension(1000) :: etn
  real                  :: hnapperch
  real                  :: pluiefente  !
  real                  :: azlesm
!==========================================================

    !! En absence de bouchon, le ruissellement deborde dans les fentes
    !- Des qu'un horizon est bouche, la fente est fermee (bouchon=1)
      if (bouchon /= 1 .and. P_codefente == 1) then
        pluiefente = pluieruissel
      else
        pluiefente = 0. ! par precaution on affecte a zero
      endif

    ! calcul de la macroporosite = f(ds,da,Hcc)
      do ic = 1,nh
        macropor(ic) = get_macroporosite(P_codefente,da(ic),hcc(ic),hmin(ic),P_epc(ic))
      end do


    !! initialisation anoxie et calcul evapotranspiration nette (etn)
      etn(:) = 0. ! Par defaut, on met tout a zero dans le tableau
      do iz = 1,nhe
        anox(iz)= 0.
        etn(iz) = epz(iz) + esz(iz) - irrigprof(iz)
      end do

    !  call TransfertsDescendants(nh, nhe, ncel, nit, hur, etn, hucc, icel, da, hr, hurlim, izcel, P_infil, P_concseuil,  &
    !                             precip, husup, azsup, remontee, P_humcapil, izc, exces, azlesm, P_capiljour, P_epc,     &
    !                             zrac, P_obstarac, remonteemax)
      call TransfertsDescendants(nh, nhe, ncel, P_concseuil, P_humcapil, P_capiljour, precip, hurlim,   &
                           icel(0:ncel), P_infil(0:nh), P_epc(1:nh), da(1:nh), hr(1:nh), izc(1:nh), izcel(1:nh),    &
                           etn(1:nhe), hucc(1:nhe), hur(1:nhe), nit(1:nhe), exces(0:nh), azlesm, azsup, husup,      &
                           !remontee, remonteemax,  P_profmes, iz_base_horizon,
                           remontee, P_profmes,                                                       &
                           husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)
    ! lessivage et drainage  a la base du profil
      QLES = QLES + azsup
      DRAT = DRAT + husup
      drain = husup
      lessiv = azsup

    !- calcul de la concentration cumulee en nitrate des eaux de drainage
      concno3les = concNO3_EauxDrainage(QLES, DRAT)

    ! cas d'exces d'eau : circulation ascendante dans la macroporosite
    ! ----------------------------------------------------------------
    ! call excesEau(nh, nhe, P_codemacropor, P_codefente, pluiefente, zrac, macropor, P_epc, hur, hucc, bouchon, exces)
      call excesEau(logger, nh, nhe, P_codemacropor, P_codefente, pluiefente, zrac, macropor(1:nh), P_epc(1:nh), hur(1:nhe), &
              hucc(1:nhe), bouchon, exces(0:nh))

    ! calcul des hauteurs de nappe
    !-----------------------------
    ! call nappe(macropor, P_epc, hnappe, hnapperch, hmax, P_profdrain, nh, profsol, P_bformnappe, &
    !            exces, nhe, hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)
      call nappe(nh, nhe, macropor(1:nh), P_epc(1:nh), hnappe, hnapperch, hmax, P_profdrain, profsol, P_bformnappe, &
                 exces(0:nh), hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)

    ! cas du drainage agricole
    !-------------------------
      if(P_codrainage.eq.1) then

      ! si impermeable infini
        pi = 3.141592654
        delt = min(((hmax/ldrains)**0.36-hmax/ldrains)/2., pi/(4*log((2*ldrains)/(pi*P_rdrain))))
        if((P_profimper-P_profdrain).ge.(ldrains/2.)) then
          de = delt*ldrains
        else
          if((P_profimper-P_profdrain).le.0.) then
            de = 0.
          else
            de = (ldrains*(P_profimper-P_profdrain)*delt)/(ldrains*delt+(P_profimper-P_profdrain))
          endif
        endif

!       call drainageAgricole(n,nh,P_infil,hur,macropor,ldrains,P_profdrain,profsol,P_ksol,P_concseuil,de,  &
!                             exces,nit,hmax,qdrain,azlesd,qlesd,hnappe)
        call drainageAgricole(logger, n, nh, nhe, P_infil(0:nh), hur(1:nhe), macropor(1:nh), ldrains, P_profdrain, profsol,   &
                      P_ksol, P_concseuil, de, exces(0:nh), nit(1:nhe), hmax, qdrain, azlesd, qlesd, hnappe)

      ! mise a jour de la hauteur de la nappe
      ! call nappe(macropor, P_epc, hnappe, hnapperch, hmax, P_profdrain, nh, profsol, P_bformnappe, &
      !            exces, nhe, hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)
        call nappe(nh, nhe, macropor(1:nh), P_epc(1:nh), hnappe, hnapperch, hmax, P_profdrain, profsol, P_bformnappe, &
                 exces(0:nh), hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)

      ! on cumule les quantites de drainage au fil du temps
        qdraincum = qdraincum + qdrain
      endif


    ! calcul du partage entre ruissellement et alimentation des fentes
      if ( bouchon == 1 .or. P_codefente /= 1 ) then
        ruissel = ruisselsurf + exces(0)
        pluieruissel = 0.
      else
        ruissel = ruisselsurf
        pluieruissel = exces(0)
      endif

    ! on vide le reservoir au dessus du sol s'il y a ruissellement
      ! if (pluieruissel ==0.) exces(0)=0.
      if (abs(pluieruissel).lt.1.0E-8) exces(0)=0.

      profhoriz = nhe
      saturation = 0.
      do ic = nh,1,-1
        do ii = 1,int(P_epc(ic))
          iz = profhoriz-ii+1
          sat(iz) = exces(ic)/P_epc(ic)
        ! recalcul du taux d'anoxie anox(iz) en fonction de la nappe
          if(iz >= Hph .and. iz <= Hpb) anox(iz) = 1.
          if(iz >= profnappe) anox(iz) = 1.
          saturation = saturation+sat(iz)
        end do
        profhoriz = profhoriz-int(P_epc(ic))
      end do

    ! DR 06/01/2017 on ajoute 2 variables pour verifier les bilans d'eau
      som_HUR = SUM(hur(1:profcalc))
      som_sat = SUM(sat(1:profcalc))

! Macsur : Available soil water over the whole profile
      SoilAvW = SUM(max(hur(1:profcalc) + sat(1:profcalc) - humin(1:profcalc),0.))

! Macsur : Amount of soil water over the depth profmesW
      SoilWatM = SUM(hur(1:int(P_profmesW)) + sat(1:int(P_profmesW)))

    ! calcul de RsurRU
!     RU = calculRU(profsol, hucc, humin)
!     RsurRU = calculRsurRU(RU, profsol, hur, sat, humin)
      RU = calculRU(nhe, hucc(1:nhe), humin(1:nhe))
      RsurRU = calculRsurRU(RU, nhe, hur(1:nhe), sat(1:nhe), humin(1:nhe))

end subroutine


function concNO3_EauxDrainage(QLES, DRAT) result(concno3les)
 
  real, intent(IN)  ::  QLES  !  // OUTPUT // Cumulated N-NO3 leached at the base of the soil profile // kgN.ha-1
  real, intent(IN)  ::  DRAT  ! // OUTPUT // Water flux drained at the base of the soil profile integrated over the simulation periodout of the soil   // mm
  real ::  concno3les         ! // OUTPUT // Nitrate concentration in drained water // mg NO3 l-1
  
  if (DRAT > 0.) then
    concno3les = QLES / DRAT * 6200. / 14.
  else
    concno3les = 0.
  endif

end function concNO3_EauxDrainage
end module lixiv_m
