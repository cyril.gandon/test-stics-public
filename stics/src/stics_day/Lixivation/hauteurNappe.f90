! ******************************************************************
!  * Calculs relatifs a la nappe
module nappe_m
implicit none
private
public :: nappe
contains
pure subroutine nappe(nh, nhe, macropor, P_epc, hnappe, hnapperch, hmax, P_profdrain, profsol, P_bformnappe,  &
                 exces, hph, hpb, de, P_distdrain, profnappe, P_codhnappe, ldrains)


  integer, intent(IN)   ::  nh  !  
  integer, intent(IN)   ::  nhe  !  
  real,    intent(IN)   ::  macropor(nh)
  integer, intent(IN)   ::  P_epc(nh)    ! // PARAMETER // thickness of soil layer i // cm
  real,    intent(IN)   ::  exces(0:nh)  ! // OUTPUT // Amount of water  present in the macroporosity of the layer i  // mm
  integer, intent(IN)   ::  P_profdrain  !  // PARAMETER // drain depth // cm // PARSOL // 1 
  integer, intent(IN)   ::  profsol  
  real,    intent(IN)   ::  P_bformnappe !  // PARAMETER // coefficient of water table shape (artificial drained soil) // SD // PARAM // 1
  real,    intent(IN)   ::  de  !  
  real,    intent(IN)   ::  P_distdrain  !  // PARAMETER // distance to the drain to calculate watertable height // cm // PARAM // 1 
  integer, intent(IN)   ::  P_codhnappe  !  // PARAMETER // mode of calculation of watertable level // code 1/2 // PARAM // 0
  real,    intent(IN)   ::  ldrains

!! arguments sortants
  real,    intent(OUT)  ::  hmax        !    // OUTPUT // Maximum height of water table between drains // cm
  real,    intent(OUT)  ::  hnappe      !    // OUTPUT // Height of water table with active effects on the plant // cm
  real,    intent(OUT)  ::  hnapperch   !
  real,    intent(OUT)  ::  hpb         !    // OUTPUT // Minimum depth of perched water table // cm
  real,    intent(OUT)  ::  hph         !    // OUTPUT // Maximum depth of perched water table // cm
  real,    intent(OUT)  ::  profnappe   ! // OUTPUT // Depth of water table // cm
  
! VARIABLES LOCALES
  integer ::  profhoriz  !  
  integer ::  icou  !  
  integer ::  ic  !  
  integer ::  ii  
  real    ::  hmoy  !  
  real    ::  d  !  
  real    ::  x  !  
  real    ::  w  !  
  real    ::  cotedrain  

      profhoriz = nhe 
      hnapperch  = 0.
      hnappe = 0.
      
      do icou = nh,1,-1
        
      !! ??
        hmoy = exces(icou)/macropor(icou)*P_epc(icou)
        
      !! HAUTEUR DE LA NAPPE
        if (hmoy>=P_epc(icou) .and. (icou==nh .or. hnappe>=(profsol-profhoriz))) then
          hnappe = hnappe + hmoy
        endif
        
        if(hmoy<P_epc(icou) .and. hnappe>=(profsol-profhoriz)) then
          hnappe = hnappe + hmoy
        endif
      
      !! ETENDUE DE LA NAPPE PERCHEE
        if(hmoy>=P_epc(icou) .and. hnappe<(profsol-profhoriz)) then
          hnapperch = hnapperch + hmoy
        endif
        
        if(hmoy<P_epc(icou) .and. hnappe<(profsol-profhoriz) .and. hnapperch>=0) then
          hnapperch = hnapperch + hmoy
        endif
      
        profhoriz = profhoriz - int(P_epc(icou))
      end do
      
      ! calcul de la profondeur maximale de la nappe perchee (hph)
      hph = 0.
      if (hnapperch>0.) then
        do ic=1,nh
          if (exces(ic)>0. .and. hph<=0.) then
            do ii = ic,1,-1
              hph = hph + P_epc(ii)
            end do
            hph = hph - (exces(ic)/macropor(ic)*P_epc(ic))
          endif
        end do
      endif
      
      
      ! *- NB - 17/06/2004 - calcul de hpb : pourquoi cumul hph,hnapperch ?
      hpb = hph + hnapperch
      
      ! ** NB - le 27/04
      ! *- calcul de la hauteur a l'interdrain
      ! *- Nb - le 05/06
      cotedrain = profsol-P_profdrain
      if ((hnappe-cotedrain).gt.0.)  then
        hmax = (hnappe-cotedrain)/P_Bformnappe
      else
        hmax = 0.0
      endif
      
      ! NB le 08/06
      ! N Delouvigny
      ! test le 10/07/01 : il peut y avoir de l'eau stagnante 
      ! au dessus du sol (mais on  met  un seuil pour le moment)
      hmax = min(hmax,P_profdrain+5.0)
      
      ! calcul de la profondeur de nappe utilisee pour l'effet sur la plante
      ! P_codhnappe = 1 : on utilise la hauteur moyenne
      ! P_codhnappe = 2 : on calcule une hauteur a une distance P_distdrain du drain
      ! Nb le 05/06
      if (P_codhnappe==1 .or. hmax<=0.) then
        profnappe = P_profdrain - hnappe + cotedrain
      else
        d = de / hmax
        x = 1.0 - (P_distdrain / ldrains)
        w = -d + sqrt((d+1)**2-x**2*(1+2*d))
        profnappe = P_profdrain - w * hmax
      endif     
      
      ! ** NB - le 05/06 - pour eviter anoxie parasite en bas de profil
      if (profnappe == profsol) profnappe = profsol + 1
      
      
      !---------------------------------------------------------------------------
      ! INFO fichiers secondaires
      !---------------------------------------------------------------------------
      !        if(hnappe.ge.0) then
      !      write(2,222)n,hnappe
      !  222      format('n',3x,i3,5x,'hnappe moy (cm)',3x,f6.2)
      !    else
      !      write(2,222)n,0.0
      !    endif
      !    if(hph.ne.0.and.hpb.ne.0) then
      !          write(4,*)'n',n,'    Nappe entre  ',hph,'  cm  et 
      !     s    ',hpb,'  cm de prof'
      !    else
      !      write(4,*)'n',n,'    Pas de nappe perchee'
      !    endif
      !  if(maj.eq.0) then
      !     write(5,51)n,Hmax,hnappe
      !   51   format(/,'n',1x,i3,25x,'Hmax',4x,f6.2,1x,'cm',/,'hnappe avant 
      !     s drainage agricole',11x,f6.2,1x,'cm')
      !  else
      !     write(5,52)n,Hmax,hnappe
      !   52   format(/,'n',1x,i3,25x,'Hmax',4x,f6.2,1x,'cm',/,'hnappe apres 
      !     s drainage agricole',11x,f6.2,1x,'cm')
      !  endif

return 
end 
end module nappe_m
