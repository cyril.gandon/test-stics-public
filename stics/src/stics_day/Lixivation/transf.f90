!   Module : Lixivation
!   Calcul du transfert de l'eau et de l'azote dans le profil
!   Modele reservoir: principe des "cellules de melange" 

!
! INPUTS
!     nhe
!     hucc
!     etn
!     izc
!     hurlim
!     P_infil
!     P_concseuil
!     precip
!     nh
!     P_capiljour
!     P_humcapil
!     hr
!
! OUTPUTS
!     husup
!     azsup
!     remontee
!     hur
!     nit
!
! INPUTS & OUTPUTS
!     exces
!
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! This module deals with downward circulation of soil water
! - Stics book paragraphe 9.2.2, page 169-170
!
!! This module deals with downward circulation of soil water, and is called by the module transfertsDescendants
!! Water transfer in the soil microporosity is calculated per elementary 1 cm layer using a reservoir-type analogy.  Water fills the layers by downward flow,
!! assuming that the upper limit of each basic reservoir corresponds to the layer's field capacity.
!! If the flow is not obstructed, (see macroporosite.f90), the excess water above field capacity is drained downward.  The soil layers affected by evaporation,
!! can dry until they reach the residual soil water content. In deeper layers, the water is only extracted by the plant and therefore always remains above
!! the wilting point.
!!
!! The transfer of nitrate is also described using this reservoir-type analogy, according to the "mixing cells" principle.  Any nitrate arriving by convection
!! with water in the elementary layer mixes with the nitrate already present.  Excess water then leaves with the new concentration of the mixture.
!! This description produces results which are very similar to the convection-dispersion model, the thickness of layers being equal to twice the
!! dispersivity (Mary et al., 1999).
!!
!! A minimum concentration level may exist (concseuil) below which mineral nitrogen cannot be leached. This can be a simple way to simulate ammonia nitrogen
!! without using the simulation of the ammoniacal phase of mineralisation.
!!
!! The amounts of drained water and leached nitrogen, i.e. leaving via the base of the soil profile are not retrievable by another crop.
!! Upwards nitrate movements occur via plant uptake only. Capillary rises provided by humid subsoil can be taken into account. Indeed, if the basal soil layer
!! is dry enough (below the humcapil threshold), capillary rise can occur from the subsoil into the soil, at a constant rate (capiljour) until the basal layer
!! reaches a moist status (above humcapil). As, in the model, these upward transfers take place through the macroporosity (they are considered negative
!! infiltration), they require a zero value of infiltrability at the base of the soil to be active.
!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
! DR 06/09/2012 j'ajoute qq param pour Joel (da et epc)
! DR 01122020 merge trunk R2382
! DR 01122020 ajout des variables de sorties de drainage et lixiaviation sous chacune des couches
module transf_m
implicit none
private
public :: transf
contains
subroutine transf(nh, nhe, hucc, hur, nit, etn, izc, hurlim, exces, P_infil, P_concseuil, precip, husup, azsup, remontee, &
                 ! P_capiljour, P_humcapil, hrtest, da, P_epc, zrac, P_obstarac, remonteemax,  P_profmes, iz_base_horizon,  &
                  P_capiljour, P_humcapil, hrtest, da, P_epc, P_profmes,  &
                  husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)

!  call transf(nh, ncel, huccel(1:nhe), hurcel(1:nhe), nitcel(1:nhe), etncel(1:nhe), izcel(1:nh), hurlimcel, excel(0:nh), &
!              P_infil(0:nh), P_concseuil, precip, husup, azsup, remontee, 0., P_humcapil, hrtest, da(1:nh),      &
!              P_epc(1:nh), zrac, P_obstarac, remonteemax)
!
!  call transf(nh, nhe, hucc(1:nhe), hur(1:nhe), nit(1:nhe), etn(1:nhe), izc(1:nh), hurlim, exces(0:nh), &
!              P_infil(0:nh), P_concseuil, precip, husup, azlesm, remontee, P_capiljour, P_humcapil, hrtest, da(1:nh), &
!              P_epc(1:nh), zrac, P_obstarac, remonteemax)

! DR 09022016 regarder si on garde les modifs de Joel !!!
    integer, intent(IN)    :: nhe
    integer, intent(IN)    :: nh
    real,    intent(IN)    :: hucc(nhe)
    real,    intent(INOUT) :: hur(nhe)
    real,    intent(OUT)   :: nit(nhe)
    real,    intent(IN)    :: etn(nhe)
    integer, intent(IN)    :: izc(nh)
    real,    intent(IN)    :: hurlim
    real,    intent(INOUT) :: exces(0:nh)   ! // OUTPUT // Amount of water  present in the macroporosity of the horizon 1  // mm

    real,    intent(IN)    :: P_infil(0:nh) ! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor=1)// mm day-1 // PARSOL // 1   // OUTPUT // Infiltrability parameter at the base of the horizon 1 // mm day-1
    real,    intent(IN)    :: P_concseuil   ! // PARAMETER // Minimum Concentration of soil to NH4 // kgN ha-1 mm-1 // PARSOL // 1
    real,    intent(IN)    :: precip        ! // OUTPUT // Daily amount of water (precipitation + irrigation)   // mm d-1
    real,    intent(OUT)   :: husup
    real,    intent(OUT)   :: azsup
    real,    intent(OUT)   :: remontee      ! // OUTPUT // Capillary uptake in the base of the soil profile // mm j-1
    real,    intent(IN)    :: P_capiljour   ! // PARAMETER // capillary rise upward water flux // mm j-1 // PARSOL // 1
    real,    intent(IN)    :: P_humcapil    ! // PARAMETER // maximal water content to activate capillary rise // % ponderal // PARSOL // 1
    real,    intent(IN)    :: hrtest        ! // INPUT // Water content by horizon (table)    // % pond.
    real,    intent(IN)    :: da(nh)
    integer, intent(IN)    :: P_epc(nh)     ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
    !real,    intent(IN)    :: zrac
    !real,    intent(IN)    :: P_obstarac
    !real,    intent(INOUT) :: remonteemax   ! // OUTPUT // capillary rise from simulated deep soil layer (mm)
!DR 01122020 merge trunk r2382
! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real,    intent(IN)    :: P_profmes
!  integer, intent(IN)    :: iz_base_horizon(1:5)
  real,    intent(OUT)   :: husup_by_horizon(1:nh)
  real,    intent(OUT)   :: azsup_by_horizon(1:nh)
  real,    intent(OUT)   :: husup_under_profmes
  real,    intent(OUT)   :: azsup_under_profmes
    ! DR 25/06/2019 pour le module chloride il faut sortir les valeurs de drainage de chaque couche de 1 cm
  real,    intent(OUT)   :: husup_by_cm(nhe)




    ! VARIABLES LOCALES
    real     ::  alpha  !
    real     ::  conc0  !
    real     ::  conc1  !
    real     ::  ex  !
    real     ::  eaudrp  !
    real     ::  azlesp  !
    real     ::  eaudrm  !
    real     ::  azlesm
    integer  ::  i  !
    integer  ::  icou  !
    integer  ::  iz
    real     :: hcc(nhe) ! pour / 10 !!!!!
    real     :: capiljour
!DR 01122020 merge trunk r2382
    integer  :: nb_layer

    ! *** ------- debut Joel provisoire --------- ***

    !integer  ::  code_hydro = 2 ! en attendant le code macroporosite
    integer :: profsol ! prof du sol calculee ensuite en sommant les epaisseurs pour le calcul de l'epaisseur des couches elementaires (variable selon appel)
    integer :: n
    integer :: z
    real :: delta
    real :: zc(1000) = 0 ! tableau des profondeurs de chaque couche (centres)
    real :: denscouche(1000) = 0 ! tableau des densites de chaque couche

    profsol = sum(P_epc)
    hcc = hucc

    ! creation d'un vecteur de taille nhe (variable selon appel) contenant le profil de densite
    delta = profsol/(nhe *1.) ! epaisseur des couches selon appel

    z = 0.
    do n = 1,nhe
        zc(n) = real(n) * delta - delta/2
    end do

    do n = 1,nh    !5
        where (zc >= z) ! on remplit par le haut en ecrasant au fur et a mesure
            denscouche = da(n)
        end where
        z = z + P_epc(n)
    end do

    ! *** ------- fin Joel provisoire --------- ***

    ! alpha: proportion d'effet piston  (0 a 1)
    alpha = 0.
      
    ! Transfert d'eau et d'azote d'une couche a l'autre: initialisation      
    husup = precip
    azsup = 0.
    ! Transfert d'eau ascendant = remontees capillaires
    capiljour = 0

!DR 01122020 merge trunk r2382
     ! DR 29/05/2019 j'initialise  � zero les nouvelles varaibles
      husup_by_horizon(:)=0.
      azsup_by_horizon(:)=0.
      husup_under_profmes = 0.
      azsup_under_profmes = 0.




  ! boucle sur les couches elementaires
    ! Modif Loic Mars 2019 : je d�sactive les modifs en attendant une evaluation de ces modifications. Priorite
    ! a l'evaluation de la v10
    !    nhrac = 0
    !    do iz = 1, nhe
    !      i = 1
    !      ! recherche des horizons de transition avec racine
    !      do while(i <= nh .and. nhrac == 0)
    !        if (iz <= izc(i).and.zrac == P_obstarac.and.iz == P_obstarac.and.nhrac == 0) then
    !           nhrac = i
    !        endif
    !        i= i+1
    !      end do
    !    end do
        do iz = 1,nhe
            icou = 0
            i = 1
            ! recherche des horizons de transition s'il y en a
            do while(i <= nh .and. icou == 0)
                if (iz == izc(i)) icou = i
                i= i+1
            end do
            if (hur(iz) > 0.) then
                conc0 = nit(iz) / hur(iz)
            else
                conc0 = 0
            endif
        
            nit(iz) = nit(iz) + azsup
            hur(iz) = hur(iz) + husup - etn(iz)
            if (hur(iz) < hurlim) then
                hur(iz) = hurlim
                husup = 0.
                azsup = 0.
            endif

            ! circulation dans la microporosite
            husup = hur(iz) - hucc(iz)
            ! remontees capillaires
            capiljour = 0
            ! circulation dans la macroporosite
            if (icou /= 0) then
            ! Modif Loic Mars 2019 : je d�sactive les modifs en attendant une evaluation de ces modifications. Priorite
            ! a l'evaluation de la v10
            ! Modifs Loic Mai 2018 : je decale le calcul des remontees capillaires apres le calcul des transferts d'eau descendants
            ! dr et lolo le changement d'unit� de humcapil est fait la hrtest est en volumique , donc on fait le chgt d'unite de humcapil
            !    if(icou == nhrac .and. hrtest <= P_humcapil) then
                if(icou == nh .and. hrtest <= (P_humcapil*da(nh)/10.)) then
                    exces(icou) = exces(icou) + P_capiljour
                    remontee = remontee - P_capiljour
                endif
                husup = max(husup,0.) + exces(icou)
                exces(icou) = max(husup - P_infil(icou),0.)
                ex = husup
                ! l'humidite est bornee au niveau de la discontinuite
                if (exces(icou) > 0.) hur(iz) = hucc(iz)
                husup = min(husup,P_infil(icou))
            ! Modif Loic Mars 2019 : je d�sactive les modifs en attendant une evaluation de ces modifications. Priorite
            ! a l'evaluation de la v10
                ! Modifs Loic Mai 2018 :
                ! les remontees capillaires ont lieu dans la derniere couche ou il y a des racines
                ! if (icou == nhrac .and. hrtest <= P_humcapil) then
                !    exces(icou) = exces(icou) + P_capiljour
                !    capiljour = P_capiljour
                !    remontee = remontee - P_capiljour
                ! endif

            !-----possible de faire remonter de l'eau de la base du profil
            !-----avec une infiltrabilite negative
            !             write(1,*)'Ce qui passe dans la couche inf.',husup,'mm'
            ! NB le 29/06 introduction d'une condition sur l'humidite du sol pour
            ! les remontees capillaires
            !            P_humcapil=10.0
            !        if(icou.eq.nh.and.husup.lt.0.0) then
            !                if(hur(nhe).ge.P_humcapil) husup=0.
            !            remontee=remontee+husup
            !            endif
            !------------------------------------------------------------------------
            endif
            ! Modif Loic Mars 2019 : je d�sactive les modifs en attendant une evaluation de ces modifications. Priorite
            ! a l'evaluation de la v10
            ! ! Modif Loic Juillet 2018 suite : S'il y a une couche de sol simulee en dessous de la derniere couche avec des racines,
            ! ! on fait varier la teneur en eau quand il y a des remontees capillaires
            ! if (nh > nhrac.and.capiljour > 0) then
            !    do ii = 1, int(P_epc(icou+1))
            !       if (hur(iz+ii)>(P_humcapil*da(icou+1)/10).and.capiljour > 0) then
            !          capil = min(capiljour, hur(iz+ii)-(P_humcapil*da(icou+1)/10))
            !          hur(iz+ii) = hur(iz+ii) - capil
            !          capiljour = capiljour - capil
            !          remonteemax = remonteemax + capil
            !       endif
            !    enddo
            ! endif
            ! Fin modifs Loic Juillet 2018

            if (husup <= 0.) then
                husup = 0.
                azsup = 0.
            else
                ! -------- infiltration vers l'horizon iz+1
                eaudrp  = alpha * husup
                hur(iz) = hur(iz) - eaudrp
                azlesp  = eaudrp * conc0
                nit(iz) = nit(iz) - azlesp
                if (hur(iz) > 0) then
                    conc1  = nit(iz) / hur(iz)
                else
                    conc1 = 0
                endif
                ! a la base d'un horizon : correction par l'eau de la macroporosite
                if(icou /= 0) conc1 = nit(iz) / (hur(iz)+ex)
                eaudrm  = (1. - alpha) * husup
                azlesm  = eaudrm * amax1(0.,conc1-P_concseuil)
                hur(iz) = hucc(iz)
                nit(iz) = nit(iz) - azlesm
                azsup   = azlesp + azlesm
            endif
!DR 01122020 merge trunk r2382
      ! DR 29/05/2019 I recorded the value of azsup and hursup for each layer and for profmes
! DR 29/05/2019 je pensais me servir des couches de transition isz mais on les reinitialise � 0 en cas de macroporosit�
! --> donc le calcul est fausse il faut faire autrement
          do nb_layer=1, nh
           !   if (iz == iz_base_horizon(nb_layer))then
              if (iz == izc(nb_layer))then
              husup_by_horizon(nb_layer)=husup
              azsup_by_horizon(nb_layer)=azsup
              !write(1236,*)
              endif
              if(abs(iz-P_profmes) < 1.0D-8)then
                husup_under_profmes=husup
                azsup_under_profmes=azsup
              endif
          enddo
          husup_by_cm(iz) = husup

        end do ! fin de boucle sur les couches
    return
end 
end module transf_m
