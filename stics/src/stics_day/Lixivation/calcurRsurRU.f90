
!   calculRsurRU
!!
!!   Module : lixivation
!!
!! retourne la valeur de R/RU (reel)
!!
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 9.4.1, page 176
!
!! By integrating the difference between hur(iz)-humin(iz) over the soil depth, profsol, and weighted by the difference between hucc(iz)-humin(iz)
!! (corresponding to ru calculated by the function calculRU.f90) gives the soil water status as a proportion of readily available water (calculRsurRU).
!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
!real function calculRsurRU(RU,profsol,hur,sat,humin)
module calculRsurRU_m
implicit none
private
public :: calculRsurRU
contains
real function calculRsurRU(RU, nhe, hur, sat, humin)

  real, intent(IN)  :: RU   ! // OUTPUT // maximum available water reserve over the entire profile // mm
!  real, intent(IN)  :: profsol
  integer, intent(IN) :: nhe
  real, intent(IN)  :: hur(nhe)  ! hur(int(profsol))
  real, intent(IN)  :: humin(nhe)! humin(int(profsol)
  real, intent(IN)  :: sat(nhe)  ! sat(int(profsol))

! Variable(s) locale(s)
  integer :: iz  

    ! TODO : il faut verifier que la taille des tableaux est coherente avec profsol

      calculRsurRU = 0.

      do iz=1,nhe  ! int(profsol)
        calculRsurRU = calculRsurRU + max(hur(iz)+sat(iz)-humin(iz),0.)
      end do

      !if (RU /= 0.) calculRsurRU = calculRsurRU/RU
      if (abs(RU).gt.1.0E-8) calculRsurRU = calculRsurRU/RU

end function
end module calculRsurRU_m
 
