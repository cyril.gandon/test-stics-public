!****f* Lixivation/sommeCouchesParCellule
! NAME
!   sommeCouchesParCellule
! DESCRIPTION
!   Module : lixivation
!   Typologie : utilitaire
!
!   Fonction generique de calcul de sommes de sections d'un tableau.
!
!   Le tableau des couches du sol avec les valeurs a sommer est le parametre <i>tabCouches</i>.
!   Le nombre de cellules de melange est donnee par le parametre <i>nbCellules</i>
!   Pour chaque cellules de melange, le nombre de couches appartenant a la cellule est donnee par le parametre <i>iCellules</i>
!
!   On fait la somme, pour chaque cellule, de la section du tableau <i>tabCouches</i> correspondant au nombre de couches de la cellule.
!   Le resultat pour chaque cellule est stockee et renvoyee dans un tableau de taille <i>nbCellules</i>.
!
! INPUTS
!     tabCouches : tableau de reels - l'ensemble des couches du sol avec
!                  pour chaque couche la valeur a additionner associee (ou nulle)
!     nbCellules : entier - le nombre de cellules de melange
!     iCellules : tableau d'entiers - pour chaque cellule, le nombre de couches appartenant a la cellule
!
!
! RETURN
!     tableau de reels - tableau unidimensionnel de taille <i>nbCellules</i>. Pour chaque cellule, le tableau
!                        renvoi la somme des valeurs  correspondantes aux couches du sol appartenant a la cellule.
!
!***
module sommeCouchesParCellule_m
implicit none
private
public :: sommeCouchesParCellule
contains
function sommeCouchesParCellule(nbCellules, iCellules, tabCouches)

! ARGUMENTS
  integer, intent(in) :: nbCellules  
  integer, intent(in) :: iCellules(0:nbCellules)  
  real,    intent(in) :: tabCouches(iCellules(nbCellules))                           ! (:)  

! TABLEAU DE RETOUR
  real, dimension(nbCellules) :: sommeCouchesParCellule  

! VARIABLES LOCALES
  integer :: ii  

!      print *, 'sommeCouchesParCellule : size(icel)=',size(iCellules)
!      print *, 'sommeCouchesParCellule : icel=',iCellules

    ! TODO : il faudrait peut etre verifier qu'il n'y a pas possibilite de debordement de tableau
    ! il ne doit pas y avoir + de cellules que d'indices dans le tableau iCellules
      if (nbCellules > SIZE(iCellules)) then


      endif

    ! on verifie que la valeur de couche max. est < ou = au nombres de couches (la taille du tableau tabCouches)
      ii = MAXVAL(iCellules) ! MAXVAL(iCellules) = iCellules(nbCellules)

      if (ii <= SIZE(tabCouches)) then
        sommeCouchesParCellule(:) = 0. ! on initialise le tableau a zero
        do ii = 1,nbCellules
          sommeCouchesParCellule(ii) = SUM(tabCouches((iCellules(ii-1)+1):iCellules(ii)))
        end do
      else
        ! Ici on est en situation d'erreur, comment gere-t-on cela ?
        !call EnvoyerMsgHistorique(logger, MESSAGE_580)
        ! write(*,*) 'SUM(iCel)=',ii,' nbCel=',nbCellules
        !write(*,*) 'SIZE(tabCouches)', SIZE(tabCouches)
        sommeCouchesParCellule(:) = -1.
      endif

end function
end module sommeCouchesParCellule_m
 
