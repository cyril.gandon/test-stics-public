
!   calculRU
!!
!!   Module : lixivation
!!
!! retourne la valeur de RU (reel)
!!
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! calculation of the readily available water in the soil
! - Stics book paragraphe 9.4.1, page 176
!
!! This function calculates the readily available water in the soil as the difference between hucc(iz)-humin(iz) over the soil depth (profsol).
!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
!real function calculRU(profsol, hucc, humin)
module calculRU_m
implicit none
private
public :: calculRU
contains
real function calculRU(nhe, hucc, humin)
! Argument(s)
! real, intent(IN)  :: profsol
  integer, intent(IN) :: nhe
  real, intent(IN)  :: hucc(nhe)  ! hucc(int(profsol))
  real, intent(IN)  :: humin(nhe) ! humin(int(profsol))

! Variable(s) locale(s)
  integer :: iz  
  real    :: RU   ! // OUTPUT // maximum available water reserve over the entire profile // mm

    ! TODO : il faut verifier que la taille des tableaux est coherente avec profsol

   !  RU = SUM(hucc(1:int(profsol))) - SUM(humin(1:int(profsol)))
      RU = SUM(hucc(1:nhe)) - SUM(humin(1:nhe))
      RU = max(0., RU)

    !TODO: On pourra supprimer les lignes ci-dessous et garder la formule au-dessus a la place
    ! pour avoir l'exact meme resultat que la version 6.4
      RU = 0.
      do iz = 1,nhe   ! int(profsol)
        RU = RU + hucc(iz)-humin(iz)
      end do

      calculRU = RU

end function calculRU
end module calculRU_m
 
