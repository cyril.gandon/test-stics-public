!****f* Lixivation/TransfersDescendants
! NAME
!   TransfertsDescendants -
! DESCRIPTION
!   Module : Lixivation
!   Typologie : scientifique
!
!
! INPUTS
!     etn
!     hucc
!     ncel
!     icel
!     da
!     hr
!     nh
!     nhe
!     hurlim
!     izcel
!     P_infil
!     P_concseuil
!     precip
!     husup
!     P_humcapil
!     izc
!     P_capiljour
!
! OUTPUTS
!     azlesm
!     azsup
!     remontee
!
! INPUT & OUTPUTS
!     nit
!     hur
!     exces
!***
! ml_com !
! *-----------------------------------------------------------------------------------------------------------------------------------------------------------* c!
! - Stics book paragraphe 9.2, page 169
!
! This module deals with downward circulation of soil water, and call the sub-module transf.f90.
!!
!! The nitrates circulate with water downwards through the soil. The way these transfers are accounted for in STICS relies on the soil compartmental description
!! and on the tipping bucket concept. The description of the soil can involve up to four compartments: microporosity, macroporosity,
!! cracks (the case of swelling clay soils) and pebbles.  However, only the description of microporosity is obligatory, the description of the other compartments
!! being optional. Whatever, soil microporosity is the basis for calculating water and nitrogen transfer values.
!------------------------------------------------------------------------------------------------------------------------------------------------------------* c
! DR 01122020 merge trunk R2382
! DR 01122020 ajout des variables de sorties de drainage et lixiaviation sous chacune des couches
module TransfertsDescendants_m
USE transf_m, only: transf
USE sommeCouchesParCellule_m, only: sommeCouchesParCellule
implicit none
private
public :: TransfertsDescendants
contains
subroutine TransfertsDescendants(nh, nhe, ncel, P_concseuil, P_humcapil, P_capiljour, precip, hurlim,  &
                                 icel, P_infil, P_epc, da, hr, izc, izcel, etn, hucc, hur, nit, exces, azlesm, &
                            !     azsup, husup, remontee, remonteemax, P_profmes, iz_base_horizon,  &
                                 azsup, husup, remontee, P_profmes,                    &
                                 husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)

!call TransfertsDescendants(nh, nhe, ncel, P_concseuil, P_humcapil, P_capiljour, P_obstarac, precip, hurlim, zrac,   &
!                           icel(0:ncel), P_infil(0:nh), P_epc(1:nh), da(1:nh), hr(1:nh), izc(1:nh), izcel(1:nh),    &
!                           etn(1:nhe), hucc(1:nhe), hur(1:nhe), nit(1:nhe), exces(0:nh), azlesm, azsup, husup,      &
!                           remontee, remonteemax)

! On utilise le module Lixivation pour connaitre les interfaces d'appels des routines et fonctions suivantes

  implicit none

  integer, intent(IN)    :: nh
  integer, intent(IN)    :: nhe
  integer, intent(IN)    :: ncel
  real,    intent(IN)    :: P_concseuil  ! // PARAMETER // Minimum Concentration of soil to NH4 // kgN ha-1 mm-1 // PARSOL // 1
  real,    intent(IN)    :: P_humcapil   ! // PARAMETER // maximal water content to activate capillary rise // % ponderal // PARSOL // 1
  real,    intent(IN)    :: P_capiljour  ! // PARAMETER // capillary rise upward water flux // mm j-1 // PARSOL // 1
  !real,    intent(IN)    :: P_obstarac
  real,    intent(IN)    :: precip       ! // OUTPUT // Daily amount of water (precipitation + irrigation)   // mm d-1
  real,    intent(IN)    :: hurlim
  !real,    intent(IN)    :: zrac
  integer, intent(IN)    :: icel(0:ncel)

  real,    intent(IN)    :: P_infil(0:nh)! // PARAMETER // infiltrability parameter at the base of each horizon (if codemacropor=1)// mm day-1 // PARSOL // 1   // OUTPUT // Infiltrability parameter at the base of the horizon 1 // mm day-1
  integer, intent(IN)    :: P_epc(nh)    ! // PARAMETER // thickness of each soil layer // cm   // PARSOL // 1      // OUTPUT // Thickness of the horizon (1 or 2 )// cm
  real,    intent(IN)    :: da(nh)       ! (nh)    // OUTPUT // Bulk density in the different soil layers // g cm-3
  real,    intent(IN)    :: hr(nh)       ! (nh)    // OUTPUT // Water content of each soil layer (table)    // % pond.
  integer, intent(IN)    :: izc(nh)       !
  integer, intent(IN)    :: izcel(nh)    !
  real,    intent(IN)    :: etn(nhe)     ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)
  real,    intent(IN)    :: hucc(nhe)    ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)

  real,    intent(INOUT) :: hur(nhe)     ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)
  real,    intent(INOUT) :: nit(nhe)     ! 1 to icel(ncel), 1 to nhe. Il faut prendre le max, a priori nhe >= icel(ncel)
  real,    intent(INOUT) :: exces(0:nh) ! // OUTPUT // Amount of water present in the macroporosity of the layer i  // mm
  real,    intent(OUT)   :: azlesm  
  real,    intent(OUT)   :: azsup
  real,    intent(OUT)   :: husup
  real,    intent(OUT)   :: remontee    ! // OUTPUT // Capillary uptake in the base of the soil profile // mm d-1
  !real,    intent(INOUT) :: remonteemax ! // INOUT // capillary rise from simulated deep soil layer (mm)

! DR 01122020 merge trunk R2382
  ! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real,    intent(IN)    :: P_profmes
!  integer, intent(IN)    :: iz_base_horizon(1:5)
  real,    intent(OUT)   :: husup_by_horizon(1:5)
  real,    intent(OUT)   :: azsup_by_horizon(1:5)
  real,    intent(OUT)   :: husup_under_profmes
  real,    intent(OUT)   :: azsup_under_profmes

 ! DR 25/06/2019 pour le module chloride il faut sortir les valeurs de drainage de chaque couche de 1 cm
  real,    intent(OUT)   :: husup_by_cm(nhe)



! VARIABLES LOCALES
  integer               ::  ii  !  
  integer               ::  iz
  real                  ::  hrtest
  real                  ::  hurlimcel  
  real                  ::  azot  

  real, dimension(ncel) ::  nitcel  
  real, dimension(ncel) ::  hurcel  
  real, dimension(ncel) ::  etncel  
  real, dimension(ncel) ::  huccel  
  real, dimension(0:nh) ::  excel


    ! on initialise les tableaux locaux a 0
      nitcel = 0.
      hurcel = 0.
      etncel = 0.
      huccel = 0.
      excel = 0.

    ! ---------------------------------------------------------------------
    ! 1) Calcul des stocks d'eau et d'azote par cellule de melange
      nitcel = sommeCouchesParCellule(ncel,icel(0:ncel),nit(1:icel(ncel)))
      hurcel = sommeCouchesParCellule(ncel,icel,hur(1:icel(ncel)))
      etncel = sommeCouchesParCellule(ncel,icel,etn(1:icel(ncel)))
      huccel = sommeCouchesParCellule(ncel,icel,hucc(1:icel(ncel)))

    ! ---------------------------------------------------------------------
    ! 2) Calcul des transferts entre les cellules de melange

    ! *- humidite minimale (mm)
    ! Modif Loic Mars 2019 : je d�sactive les modifs en attendant une evaluation de ces modifications. Priorite
    ! a l'evaluation de la v10
    ! Loic Mai 2018 : pourquoi ce calcul de hrtest ? Il faut qu'il ait la meme unite que P_humcapil !
    ! Je recherche la derniere couche exploree par les racines afin de calculer par la suite les remontees capillaires
     hrtest = hr(nh) * da(nh) / 10.
    !  nhrac = 0
    !  do iz = 1, nhe
    !    i = 1
    !    ! recherche des horizons de transition avec racine
    !    do while(i <= nh .and. nhrac == 0)
    !      if (iz <= izc(i) .and. zrac == P_obstarac .and. iz == P_obstarac .and. nhrac == 0) then
    !         nhrac = i
    !      endif
    !      i= i+1
    !    end do
    !  end do
    !  hrtest = hr(nh)
    !  if (nhrac < nh) then
    !    hrtest = hr(nhrac)
    !  endif
    !! Fin modifs Loic Mai 2018

      hurlimcel=hurlim*nhe/ncel

! modif Bruno octobre 2018 pour avoir les bonnes dimensions de tableaux
!      call transf(ncel, huccel, hurcel, nitcel, etncel, izcel, hurlimcel, excel, P_infil, P_concseuil, precip, husup,  &
!                  azsup, remontee, nh, 0., P_humcapil, hrtest, da, P_epc, zrac, P_obstarac, remonteemax)
!
!      call transf(nhe, hucc, hur, nit, etn, izc, hurlim, exces, P_infil, P_concseuil, precip, husup,  &
!                  azlesm, remontee, nh, P_capiljour, P_humcapil, hrtest, da, P_epc, zrac, P_obstarac, remonteemax)

      call transf(nh, ncel, huccel(1:ncel), hurcel(1:ncel), nitcel(1:ncel), etncel(1:ncel), izcel(1:nh), hurlimcel, excel(0:nh), &
                  P_infil(0:nh), P_concseuil, precip, husup, azsup, remontee, 0., P_humcapil, hrtest, da(1:nh),      &
                 ! P_epc(1:nh), zrac, P_obstarac, remonteemax, P_profmes, iz_base_horizon,   &
                  P_epc(1:nh), P_profmes,    &
                  husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes,  husup_by_cm)

      call transf(nh, nhe, hucc(1:nhe), hur(1:nhe), nit(1:nhe), etn(1:nhe), izc(1:nh), hurlim, exces(0:nh),               &
                  P_infil(0:nh), P_concseuil, precip, husup, azlesm, remontee, P_capiljour, P_humcapil, hrtest, da(1:nh), &
                 ! P_epc(1:nh), zrac, P_obstarac, remonteemax, P_profmes, iz_base_horizon,   &
                  P_epc(1:nh), P_profmes,    &
                  husup_by_horizon, azsup_by_horizon, husup_under_profmes, azsup_under_profmes, husup_by_cm)

    ! ---------------------------------------------------------------------
    ! 4)  Calcul des nouvelles quantites d'azote par couche
      do ii = 1,ncel
        !concel = nitcel(ii)/hurcel(ii)
        azot = 0. ! par defaut et pour eviter les erreurs
        azot = SUM(nit(icel(ii-1)+1:icel(ii)))
      ! ** la quantite d'azote est proportionnelle au cas dispersion 1 cm
        if (azot > 0.) then
          do iz = icel(ii-1)+1,icel(ii)
            nit(iz) = nit(iz)*nitcel(ii)/azot
          end do
        endif
      end do

end subroutine
end module TransfertsDescendants_m
 
