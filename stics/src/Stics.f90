! Module Stics
! - Description : Stics main module. In this module are described the structures
!! Stics_Communs_,Stics_Transit and the initialization subroutine which use it

module Stics
use stdlib_string_type
use soil_profile_settings_m
use daily_output_m

  implicit none

  private
  public Stics_Communs_, Stics_Transit_
  public nb_messages_max, nb_residus_max, nbLayers_max, nbCouchesSol, maxdayf, maxdayg

  integer, public, parameter :: nb_plant_max = 2
  integer, parameter :: nb_messages_max = 1000
  integer, parameter :: nb_residus_max = 22
  integer, parameter :: nbLayers_max = 5
  integer, parameter :: nbCouchesSol = 300     ! Profondeur max du sol (cm)
  integer, parameter :: maxdayf = 1096         ! Taille maximale du tableau drlf (731 pour plantes annuelles, 1096 pour perennes)
  integer, parameter :: maxdayg = 3653         ! Taille maximale des tableaux drlg et dtj (731 pour annuelles, 3653 pour perennes)

  
   !! Le type de simulation ('feuille' pour forcer le lai a partir d'un fichier)
   !! lecture d'un code determinant le mode de simulation
   !! P_codesimul = 'culture' --> mode normal
  character(*), public, parameter :: CODE_CULTURE = 'culture'

  !! P_codesimul = 'feuille' --> mode feuille (1 annee maxi) lai force
  character(*), public, parameter :: CODE_FEUILLE = 'feuille'

type Stics_Communs_
  character(len=7)        :: P_codesimul !   // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0
  integer                 :: codoptim   ! code optimisation : 0 (pas d'opti), 1 (opti plante principale), 2 (opti plante associee)
  integer                 :: P_codesuite  !   // PARAMETER // code for successive P_USM ( 1=yes, 0=no) // SD // P_USM/USMXML // 0
  integer                 :: P_iwater !   // PARAMETER // julian day of the beginning of the simulation // jour julien // P_USM // 1
  integer                 :: P_ifwater !   // PARAMETER // julian day of the end of simulation // julian day // P_USM // 1
  integer                 :: P_nbplantes !  // PARAMETER // number of simulated plants // SD // P_USM/USMXML // 0
  character(len=50)       :: P_ficInit !  // PARAMETER // name of the initialization file  // SD // P_USM // 0
  integer                 :: P_ichsl  !   // PARAMETER // soil numer in the  param.soil  file // SD // P_USM // 1
  character(len=50)       :: P_nomSol !  // PARAMETER // name of soil // SD // P_USM/USMXML // 0
  character(len=50)       :: P_ficStation !  // PARAMETER // name of station file // SD // P_USM // 0
  character(len=50)       :: P_wdata1 !   // PARAMETER // name of the beginning climate file // SD // P_USM // 0
  character(len=50)       :: P_wdata2 !   // PARAMETER // name of the ending climate file // SD // P_USM // 0

  !> Total number of years of the simulation
  integer                 :: nbans
  integer                 :: P_culturean !  // PARAMETER // crop status 1 = over 1 calendar year ,other than 1  = on two calendar years (winter crop in northern hemisphere) // code 0/1 // P_USM/USMXML // 0
  integer                 :: P_ifwater0
  integer                 :: P_culturean0

  real :: ttot, tcroi

  integer           :: jour            !  variable pour le stockage du jour courant de la simulation (01 a 31)
  integer           :: nummois         !  variable pour le stockage du numero de mois courant (01 a 12)
  integer           :: ancours         !  variable pour le stockage de l'annee courante

  integer :: ifwater_courant
  integer :: ifwater0  
  integer :: dernier_n  
  !> Current day of simulation, start at 1
  integer :: n  
  integer :: nbjmax = 731        ! taille des tableaux temporels (731 pour l'instant)
  integer :: jjul  
  integer :: jul  
  integer :: nbjanrec  
  integer :: nstoc  

  !> Current year of the simulation, start at 1
  integer :: numcult  

  integer :: ens ! *** le code d'ensoleillement AO ou AS ***  

  integer :: nbjsemis  
  !> Total number of days of the simulation
  integer :: maxwth  
  integer :: nstoprac  

  integer :: numdate  
  integer :: bouchon      !  // OUTPUT // Index showing if the shrinkage slots are opened (0) or closed (1)  // 0-1
  integer :: nouvdensrac  

  integer :: nappmulch  
  integer :: ires  
  integer :: itrav  

  integer :: ansemis  
  integer :: anrecol
  integer :: annee(731)  
  integer :: NH  

  integer :: codeprofil  

  integer :: nbjrecol  
  integer :: nbjrecol0  
  integer :: NHE  
  integer :: napini(2)  ! DR 10/06/2013 indexe sur les plantes
  integer :: napNini(2)    ! DR 10/06/2013 indexe sur les plantes
  integer :: nbjresini(2) ! DR 17/06/2016
  integer :: faucheannule  

  real :: Ninitf(5)  
  real :: P_Hinitf(5)     ! // PARAMETER // Table of initial gravimetric water content of each soil layer (/fine earth) // % w // INIT // 1
  real :: delta  
  real :: devjourfr  
  real :: esz(nbCouchesSol)
  real :: fdens  
  real :: tustress      !  // OUTPUT // Stress index active on leaf growth (= minimum(turfac,innlai))  // 0-1

  real :: rdif      !  // OUTPUT // Ratio between diffuse radiation and global radiation  // 0-1
  real :: originehaut  
  real :: tairveille      !  // OUTPUT // Mean air temperature the previous day // degree C
  real :: coefbmsres  
  real :: coefaifres  
  real :: coefbifres  
  real :: coefcifres  

  real :: a  
  real :: effN ! TODO: variable locale. Pas d'interet dans la version actuelle de la stocker  
  real :: hi  
  real :: ha  
  real :: hpf  
  real :: rglo  
  real :: hurlim  
  real :: rnetS      !  // OUTPUT // Net radiation in the soil // Mj.m-2
  real :: rnet      !  // OUTPUT // Net radiation  // MJ m-2
  real :: albedolai      !  // OUTPUT // P_Albedo of the crop cobining soil with vegetation // SD
  real :: resmes      !  // OUTPUT // Amount of soil water in the measurement depth // mm
  real :: dacouche(0:nbCouchesSol)  ! 0 a dacouche sinon pb le jour de la recolte dans densirac (si codeculture = feuille)
  real :: ruisselt      !  // OUTPUT // Total quantity of water in run-off (surface + overflow) // mm
  real :: infilj(0:5)  
  real :: exces(0:5)      ! // OUTPUT //  Amount of water  present in the macroporosity of the horizon 1  // mm
  real :: anox(nbCouchesSol)
  real :: pluieruissel  
  real :: sat(nbCouchesSol)
  real :: cpreciptout      !  // OUTPUT // Water supply integrated over the simulation period  // mm
  real :: ruissel      !  // OUTPUT // Daily run-off // mm
  real :: QeauI  
  real :: QeauFS  
  real :: Qeau0  
  real :: doi  
  real :: Edirect       !  // OUTPUT // Water amount evaporated by the soil + intercepted by leaves + intercepted by the mulch  // mm
  real :: humidite      !  // OUTPUT // Moisture in the canopy // 0-1
  real :: mouillmulch  
  real :: Emulch      !  // OUTPUT // Direct evaporation of water intercepted by the mulch // mm
  real :: intermulch      !  // OUTPUT // Water intercepted by the mulch (vegetal) // mm
  real :: cintermulch      !  // OUTPUT // Amount of rain intercepted by the mulch // mm
  real :: ruisselsurf      !  // OUTPUT // Surface run-off // mm
  real :: ras      !  // OUTPUT // Aerodynamic resistance between the soil and the canopy   // s.m-1
  real :: Nvolat  
  real :: eptcult  
  real :: TcultMin  
  real :: TcultMax      !  // OUTPUT // Crop surface temperature (daily maximum) // degree C
  real :: dessecplt  

  real :: eo      !  // OUTPUT // Intermediary variable for the computation of evapotranspiration   // mm
  real :: eos      !  // OUTPUT // Maximum evaporation flux  // mm
  real :: Ratm      !  // OUTPUT // Atmospheric radiation  // Mj.m-2

  real :: hres(nb_residus_max)
  real :: Wb(nb_residus_max)
  real :: kres(nb_residus_max)
  real :: NCbio      !  // OUTPUT // N/C ratio of biomass decomposing organic residues // gN.g-1C
  real :: saturation      !  // OUTPUT // Amount of water remaining in the soil macroporosity // mm
  real :: som_HUR !  // OUTPUT // cumulative water content of the soil microporosity // mm
  real :: som_sat !  // OUTPUT // cumulative amount of water in the soil macroporosity // mm
  real :: qmulch      !  // OUTPUT // Quantity of plant mulch // t.ha-1

  real :: Ninit(5)  
  real :: Hinit(5)  
  real :: HR(5)      !  // OUTPUT // Water content of the horizon 1-5 (table)   in % pond // % pond.
  real :: HR_mm(5)   !  // OUTPUT // Water content of the horizon 1-5 (table)   in mm  // mm
  real :: azomes      !  // OUTPUT // Amount of  mineral nitrogen in the depth of measurement // kgN.ha-1
  real :: ammomes      !  // OUTPUT // Amount of ammonium in the depth of measurement // kgN.ha-1
  real :: FsNH3      !  // OUTPUT // Volatilisation of NH3  // microg.m-2.j-1
  real :: RsurRU      !  // OUTPUT // Fraction of available water reserve (R/RU) over the entire profile // 0 a 1
  real :: DRAT      !  // OUTPUT // Water flux drained at the base of the soil profile integrated over the simulation periodout of the soil   // mm
  real :: QNdrp  
  real :: esol      !  // OUTPUT // Actual soil evaporation flux  // mm day-1
  real :: et      !  // OUTPUT // Daily evapotranspiration (= es + et) // mm day-1
  real :: tnhc      !  // OUTPUT // "Cumulated  normalized   time  for the mineralisation of humus" // days
  real :: tnrc      !  // OUTPUT // "Cumulated  normalized  time for the mineralisation of organic residues" // days
  real :: pluieN  ! // OUTPUT //  "Cumulative amount of mineral N added from precipitation" // kgN.ha-1
  real :: irrigN    !  // OUTPUT // "Cumulative amount of mineral N added by irrigation" // kgN.ha-1
  real :: precip      !  // OUTPUT // Daily amount of water (precipitation + irrigation)   // mm day-1
  real :: precipN  ! "Cumulative amount of mineral N added from precipitation + irrigation)" // kgN.ha-1
  real :: cumoffrN  
  real :: cumoffrN0  
  real :: cumoffrN100  
  real :: azorac0  
  real :: azorac100  
  real :: demandebrute
  real :: absodrp  
  real :: cpluie      ! // OUTPUT //  Cumulative rainfall over the simulation period // mm
  real :: Chumt       ! // OUTPUT //  Total amount of C humus (active + inert fractions) in the soil // kg.ha-1
  real :: Chumt0      !  // OUTPUT // Initial amount of C humus (active + inert fractions) in the soil // kg.ha-1
  real :: Nhuma       ! // OUTPUT //  Amount of active nitrogen in the soil humus pool // kg.ha-1
  real :: Chuma
  real :: Nhuma0
  real :: Chuma0
  real :: SOC       ! Soil organic C (Fine fraction = Chumt + Cb) on day n (kg/ha)
  real :: SOC0      ! Soil organic C (Fine fraction = Chumt + Cb) on day 0 (kg/ha)
  real :: SOCL(5)   ! Soil Organic Carbon in layer i
  real :: SOCtot    ! Total soil organic C on day n (kg/ha)
  real :: SOCtot0   ! Total soil organic C on day 0 (kg/ha)
  real :: SOCinputs
  real :: SOCoutputs
  real :: SONinputs
  real :: SONoutputs
  real :: Nhumi0
  real :: Chumi0
  real :: Nhumi
  real :: Chumi
  real :: Nhumt     !  // OUTPUT // Total quantity of N humus (active + inert fractions) in the soil // kg.ha-1
  real :: Nhumt0
  real :: SON       ! Soil Organic Nitrogen (Fine fraction = Nhumt + Nb) on day n
  real :: SON0
  real :: SONL(5)
  real :: SONtot    ! Total soil organic N
  real :: SONtot0   ! Total soil organic N on day 0
  real :: GHG       ! Greenhouse gas emissions (CO2 + N2O) expressed as CO2eq (kg/ha)
  real :: STN0      ! Total amount of N (mineral + organic) in soil on day 0
  real :: STN       ! Total amount of N (mineral + organic) in soil on day n
  real :: SMN       ! Soil Mineral N (NH4+NO3) in the soil profile on day n
  real :: SMNmes    ! Soil Mineral N (NH4+NO3) over profmes on day n
  real :: SMN0      ! Soil Mineral N (NH4+NO3) in the soil profile on day 0
  real :: QCprimed
  real :: QNprimed
  real :: Cr        !  // OUTPUT // Amount of C in the soil organic residues // kg.ha-1
  real :: Nr        !  // OUTPUT // Amount of N remaining in the decaying organic residues in the soil  // kg.ha-1
  real :: Cb        !  // OUTPUT // amount of C in the microbial biomass decomposing organic residues mixed with soil // kg.ha-1
  real :: Nb        !  // OUTPUT // Amount of N remaining in the biomass decaying organic residues // kg.ha-1
  real :: Nb0
  real :: Nr0
  real :: Cb0
  real :: Cr0
  real :: Cbmulch0
  real :: Nbmulch0

  real :: etm        !  // OUTPUT // Maximum evapotranspiration ( = eop + es)  // mm
  real :: precipamm  
  real :: P_NH4initf(5)    ! // PARAMETER // Amounts of initial mineral N in the 5 soil layers (fine earth) // kg.ha-1 // INIT // 1
  real :: NH4init(5)  

  real :: eaunoneffic  
  real :: toteaunoneffic  
  real :: raamax  
  real :: raamin  
  real :: laiTot                !  LAI total pour un jour de l'ensemble des plantes de la parcelle
  real :: stemflowTot  
  real :: EmdTot  
  real :: epTot  
  real :: hauteurMAX  

  real :: Chum(nbCouchesSol)
  real :: Nhum(nbCouchesSol)
  real :: Cres(nbCouchesSol,nb_residus_max)
  real :: Nres(nbCouchesSol,nb_residus_max)
  real :: Cbio(nbCouchesSol,nb_residus_max)
  real :: Nbio(nbCouchesSol,nb_residus_max)

  real :: xmlch1      !  // OUTPUT // Thickness of mulch created by evaporation from the soil // cm
  real :: xmlch2  
  real :: supres  
  real :: stoc  
  real :: cestout      !  // OUTPUT // Evaporation integrated over the simulation period // mm
  real :: cep2
  real :: QNabso2
  real :: QNressuite2
  real :: QCressuite2
  real :: QNressuite_tot2
  real :: QCressuite_tot2
  real :: qexport2
  real :: QNexport2
  real :: QNfauchetot2
  real :: Qfixtot2
  real :: QNplantetombe2
  real :: QCplantetombe2
  real :: QNrogne2
  real :: QCrogne2
  real :: QNracmort2
  real :: QCracmort2
  real :: QNperennemort2
  real :: QCperennemort2
  real :: QNtot2
  real :: DQNtot2
  real :: Nsurplus
  real :: Nsurplus_min
  real :: fmodk     ! Reduction factor of decomposition rate of organic residue type 1 in layer 1
  real :: fmodB     ! Reduction factor applied to the N/C ratio of microbial biomass decomposing organic residue type 1 in layer 1
  real :: fmodH     ! Reduction factor applied to the humified N during decomposition of organic residue type 1 in layer 1
  real :: fmodP     ! Reduction factor applied to SOC mineralization (priming) during decomposition of organic residue type 1 in layer 1
  real :: fmody     ! Reduction factor applied to C assimilation yield during decomposition of organic residue type 1 in layer 1

  real :: cEdirecttout  ! // OUTPUT //  Total Evaporation (water evaporated by the soil + intercepted by leaves and mulch) integrated over the simulation period  // mm
  real :: cEdirect      ! // OUTPUT //  Total Evaporation (water evaporated by the soil + intercepted by leaves and mulch) integrated over the cropping season // mm

  real :: pfz(nbCouchesSol)
  real :: etz(nbCouchesSol)


  real :: parapluieetz  
  real :: totapN      !  // OUTPUT // Total amount of N inputs from fertiliser and residues // kg.ha-1
  real :: Qminh      !  // OUTPUT // Cumulative mineral N arising from humus // kg.ha-1
  real :: Qminr      !  // OUTPUT // Cumulative mineral N arising from organic residues // kg.ha-1
  real :: QLES      !  // OUTPUT // Cumulative NO3-N leached at the bottom of the soil profile // kg.ha-1
  real :: TS(5)      !  // OUTPUT // Mean soil temperature (mean of the 5 layers) // degree C
  real :: totapNres   !  // OUTPUT // Total amount of N in organic residues inputs  // kg.ha-1
  real :: Qnitrif      !  // OUTPUT // "cumulative nitrification of nitrogen (if option  nitrification  is activated)" // kg.ha-1
  real :: tcult      !  // OUTPUT // Crop surface temperature (daily average) // degree C
  real :: tcultveille  
  real :: tsol(0:nbCouchesSol)
  real :: tsolveille(nbCouchesSol)
  real :: HUR(nbCouchesSol)
  real :: hurmini(nbCouchesSol)
  real :: HUCC(nbCouchesSol)
  real :: HUMIN(nbCouchesSol)
  real :: AZamm(5)      !  // OUTPUT // Amounts of NH4-N in the 5 soil horizons // kg.ha-1
  real :: effamm  
  real :: tauxcouv(0:731)      ! // OUTPUT //  Cover rate // SD
  real :: azsup  

! * pour solnu
  real :: smes02  
  real :: sumes0  
  real :: sumes1  
  real :: sumes2  
  real :: sesj0  
  real :: ses2j0  
  real :: sum2  
  real :: esreste  
  real :: esreste2  

! * pour lixiv
  real :: drain      !  // OUTPUT // Water flux drained at the base of the soil profile // mm j-1
  real :: lessiv      ! // OUTPUT //  daily N-NO3 leached at the base of the soil profile // kgN.ha-1

  real :: fxa      !  // OUTPUT // Anoxic effect on symbiotic uptake // 0-1
  real :: fxn      !  // OUTPUT // Nitrogen effect on symbiotic uptake // 0-1
  real :: fxt      !  // OUTPUT // Temperature effect on symbiotic uptake // 0-1
  real :: fxw      !  // OUTPUT // Water effect on symbiotic uptake // 0-1

  real :: Vabso5(5)  
  real :: vmax  
  real :: cumdltaremobilN  

  real :: cum_immob      !  // OUTPUT // cumulated mineral nitrogen arising from organic residues(immobilization) // kg.ha-1
  real :: QCapp     ! // OUTPUT // cumulative amount of organic C added to soil // kg.ha-1
  real :: QNapp     ! // OUTPUT // cumulative amount of organic N added to soil // kg.ha-1
  real :: QCresorg  ! // OUTPUT // cumulative amount of exogenous C added to soil // kg.ha-1
  real :: QNresorg  ! // OUTPUT // cumulative amount of exogenous N added to soil // kg.ha-1
  real :: cum_immob_positif      ! // OUTPUT // cumulated mineral nitrogen arising from organic residues(immobilization) positive value // kg.ha-1

  logical :: posibsw  
  logical :: posibpe  
  logical :: repoussesemis(2)  
  logical :: repousserecolte(2)  
  logical :: recolte1  
  logical :: P_datefin  ! // PARAMETER // date of the end of the simulation // days // USMXML // 0

  !> name of the P_USM // SD // USMXML // 0
  character(:), allocatable :: P_usm

  character(len=7)  :: wlieu           !  le nom ou code du lieu des donnees climatiques

! ******************** c
! **>> CONSTANTES <<** c
! ******************** c

  integer :: nouvre2
  integer :: nouvre3



! *- apports
  integer :: naptot  
  integer :: napNtot  
  real :: anit(731)      !  // OUTPUT // Daily nitrogen provided  // kg.ha-1 d-1

  real :: anit_engrais(731)      !  // OUTPUT // Daily nitrogen provided by fertiliser // kg.ha-1 d-1
  real :: anit_uree(731)      !  // OUTPUT // Daily nitrogen provided  by pasture (uree) // kg.ha-1 d-1
  integer :: type_ferti(731) !  // OUTPUT // type of fertilizer // SD
  integer :: type_ferti_pissats(731)!  // OUTPUT // type of fertilizer pour les pissats de vache // SD
  real :: airg(731)        !  // OUTPUT // Daily irrigation // mm
  real :: totir           !  // OUTPUT // Total amount of water inputs  // mm


  real :: co2res      ! // OUTPUT // CO2 mass flow from the residues // kgC.ha-1.d-1
  real :: co2hum      ! // OUTPUT //  CO2 mass flow from the soil humus // kgC.ha-1.d-1
  real :: CO2sol      ! // OUTPUT //  CO2 mass flow from the soil // mgCO2.m-2.d-1
  real :: QCO2sol
  real :: QCO2res
  real :: QCO2hum
  real :: QCO2mul

  real :: deltat_an(300)

  real :: deltaT_adaptCC(200)  
  real :: var_trefh(200)  
  real :: var_trefr(200)  
  real :: var_tnitmin(200)  
  real :: var_tnitmax(200)  
  real :: var_tnitopt(200)  

  real :: var_tnitopt2(200)
  real :: var_TREFdenit1(200)  
  real :: var_TREFdenit2(200)  
  real :: var_TREFfhum(200)  
  real :: var_FTEM(200)  
  real :: var_FTEMr(200)  

!DR 19/07/2012 j'allonge le nom du fichier de 25 a 50
  character(len=50) :: fplt_ori(2)
  character(len=3) :: codeplante_ori(2)  
  logical :: plante_ori(2)  
  integer :: iplt_ori(2)  

  real :: Qem_N2O     !  // OUTPUT // cumulative N2O-N emissions (from both sources) // kg.ha-1
  real :: em_N2O      ! // OUTPUT //  daily N2O emission // kgN.ha-1.j-1
  real :: Qem_N2Onit  !  // OUTPUT // cumulative N2O-N emission due to nitrification// kg.ha-1
  real :: em_N2Onit   !  // OUTPUT // daily N2O-N emission due to nitrification // kg.ha-1.d-1
  real :: Qem_N2Oden  !  // OUTPUT // cumulative N2O-N emission due to denitrification// kg.ha-1
  real :: em_N2Oden   !  // OUTPUT // daily N2O-N emission due to denitrification // kgN.ha-1.d-1

  character(len=60) :: sys  
  character(len=14) :: nomplante  


  logical :: onafaitunecoupedelanneedavant

  real :: tempfauche_ancours_ini(2,20)
  real :: irrigjN           ! // OUTPUT //  mineral N added through irrigation // kg ha-1
  real :: precipjN          !  // OUTPUT // mineral N added through rainfall   // kg ha-1
  real :: apport_mini_semis  

  character(len=15) :: nom_variete

  integer :: iwater0
  integer :: ansemis0
  integer :: iwaterapres  
  integer :: ifwaterapres  
  integer :: nbjsemis0  

  integer :: iwater_cultsuiv  
  integer :: ifwater_cultsuiv  

  real :: beta_sol(2)  

  real :: offrN(nbCouchesSol)
  real :: absz(nbCouchesSol)

  real :: nodn      !  // OUTPUT // Nitrogen stress effect on nodosities establishment // 0 ou 1


  ! TODO: reflechir quant a savoir si trosemax reste variable locale
  ! de humheure ou variable globale de Stics_Communs_ ou Climat_ ou autre.
  real    :: trosemax(0:731)  


!: Les variables liees aux ecritures/sorties

  type(soil_profile_settings_) :: soil_profile_settings

  type(string_type), allocatable :: valrap(:)  !  stockage des noms des variables de sortie dans mod_rapport.sti

  ! Variables names of dailies output
  type(string_type), allocatable :: daily_var_names(:)
  type(daily_formats_) :: daily_formats
!  character(len=10) :: nomvarprof(10)
  logical  :: ecritrap  
  ! 14/09/2011 rapport special AgMIP
  real, allocatable :: valsortie_flo(:), valsortie_mat(:) , valsortie_iplt(:)
  ! DR 30/08/2018 pour Agmip LI
  real, allocatable :: valsortie_rec(:)

  !--variable locale de Ecriture_Rapport--real     :: nstt2
  real     :: QH2Of  
  integer :: codeaucun  !
  ! PL, 24/09/2020, for managing report header writing for all simulation cases
  integer :: codeenteterap(2)  
  integer :: codeenteterap_agmip ! dr 11/03/2013 ajout pour pouvoir avoir entete ou pas dans rappeort_agmip
  integer ::  codetyperap  
! DR 27/062013 j'augmente le nombre de dates pour le fichier rapport , je le passe de 20 a 366 dates
  integer :: daterap(366), nboccurrap
  ! dr 11/03/2014 je stocke les dates du rapports puisque je les calcule avant l'appel a ecriture_rapport pour pouvoir recalculer les jours julien
  ! en cas de culture d'hiver
  integer :: date_calend_rap(366,3) ! 1 an , 2 mois, 3 jour
  logical :: raplev  !
  logical :: rapamf  !
  logical :: raplax  !
  logical :: rapflo  !
  logical :: rapdrp  !
  logical :: raprec  !
  logical :: rapsen  !
  logical :: rapfin  
  logical :: rapplt  !
  logical :: rapger  !
  logical :: rapdebdes  !
  logical :: rapdebdebour  !
  logical :: rapmat  
  logical :: rapdebdorm  !
  logical :: rapfindorm  
  logical :: rapdeb
  logical :: start_rap
  logical :: rapcut

  real :: RU      ! maximum available water reserve over the entire profile // mm
  real :: concNO3sol(5)      ! Nitrate concentration in the horizon 4 water // mg NO3 l-1
  real :: FTEMhb  
  real :: FTEMrb  
! From trunk (i.e. V9): Cresdec,  Nresdec, TODO : usefull ?
  real    :: Cresdec(nb_residus_max)   ! // OUTPUT // C in residue (i) decomposing mixed with soil // kg.ha-1
  real    :: Nresdec(nb_residus_max)   ! // OUTPUT // N in residue (i) decomposing mixed with soil // kg.ha-1
  real    :: Cnondec(nb_residus_max)   ! // OUTPUT // undecomposable C in residue i present in the mulch // kg.ha-1
  real    :: Nnondec(nb_residus_max)   ! // OUTPUT // undecomposable N in residue i present in the mulch // kg.ha-1
  real    :: Cmuldec(nb_residus_max)   ! // OUTPUT // C in residue (i) decomposing in mulch at soil surface // kg.ha-1
  real    :: Nmuldec(nb_residus_max)   ! // OUTPUT // N in residue (i) decomposing in mulch at soil surface // kg.ha-1
! From trunk (i.e. V9): Cmulch, Nmulch, TODO : usefull ?
  real    :: Cmulch    !  // OUTPUT // Total C in mulch at soil surface // kg.ha-1
  real    :: Nmulch    !  // OUTPUT // Total N in mulch at soil surface // kg.ha-1
  real    :: Cmulchnd      !  // OUTPUT // total undecomposable C stock in mulch // kg.ha-1
  real    :: Nmulchnd      !  // OUTPUT // total undecomposable N stock in mulch // kg.ha-1
  real    :: Cmulchdec      !  // OUTPUT // total undecomposable C stock in mulch // kg.ha-1
  real    :: Nmulchdec      !  // OUTPUT // total undecomposable N stock in mulch // kg.ha-1
  real    :: Cbmulch      !  // OUTPUT // amount of C in the microbial biomass decomposing organic residues at soil surface (mulch) // kg.ha-1
  real    :: Nbmulch      !  // OUTPUT // amount of N in microbial biomass decomposing mulch // kg.ha-1
  real    :: Cmulch0     !  // OUTPUT // total undecomposable C stock in mulch at time 0 // kg.ha-1
  real    :: Nmulch0     !  // OUTPUT // total undecomposable N stock in mulch at time 0 // kg.ha-1
  real    :: couvermulch      !  // OUTPUT // Cover ratio of mulch  // 0-1

! DR 17/11/2021 j'ajoute les variables qui sotn issus de calcul dans CorrespondanceVariableDeSorties.f90
  real    :: DCbmulch
  real    :: DCmulch
  real    :: DCrprof
!  real    :: CsurNrac_out
!  real    :: CsurNracmort
  real    :: DChumt
  real    :: DNbmulch
  real    :: DNhumt
  real    :: DNmulch
  real    :: DNr
  real    :: DNrprof
  real    :: DSMN
  real    :: DSOC
  real    :: DSOCtot
  real    :: DSON
  real    :: DSONtot
  real    :: DSTN
!  real    :: HI_C
!  real    :: HI_N
!  real    :: Qlesd  ! voir si on garde ca ou qlesd
  real    :: Qmin
  real    :: SOCbalance
  real    :: SoilAvW_by_layers(nbLayers_max)



  ! DR 17/11/2021 fin des rajouts



  real    :: Crprof       ! Amount of C in residues present in depth (below profhum) //  kg.ha-1
  real    :: Nrprof       ! Amount of N in residues present in depth (below profhum) //  kg.ha-1
  real    :: Crprof0      ! Initial amount of C in residues present in depth (below profhum) //  kg.ha-1
  real    :: Nrprof0      ! Initial amount of N in residues present in depth (below profhum) //  kg.ha-1


! DR 03/02/2011 on ajoute des varaibles de sorties sur les residus   !! voir sur quelle profondeur on dimensionne
! DR 04/12/2013 on avait une pb de dimensionnement sur cette varaible qui ne sert plus a rien arghhhhhhhhhhh !!!

  real    :: Crtout      !  // OUTPUT // total amount of C in organic residues over the whole soil profile) // kg.ha-1
  real    :: Nrtout      !  // OUTPUT // total amount of N in organic residues over the whole soil profile) // kg.ha-1
  real    :: Cresiduprofil(nb_residus_max)      !  // OUTPUT // total amount of C in residue (ires) on P_profhum // kg.ha-1
  real    :: Nresiduprofil(nb_residus_max)      !  // OUTPUT // total amount of N in residue (ires) on P_profhum // kgN.ha-1

  !DR 05/06/2019 ajout des Cres et Nres par cm pour tous types de residus confondus
  real    :: C_allresidues(nbCouchesSol)    !  // OUTPUT // total amount of C for all residues by layer // kg.ha-1
  real    :: N_allresidues(nbCouchesSol)    !  // OUTPUT // total amount of N for all residues by layer // kgN.ha-1

  integer :: irmulch  

!DR 16/12/2013 on ajoute les varaibles cumules depuis le semis necessaires a Macsur
  real :: drain_from_plt       ! // OUTPUT // cumulative amount of water drained at the base of the soil profile over the crop period (planting-harvest)  // mm
  real :: leaching_from_plt    ! // OUTPUT // cumulative N-no3 leached at the base of the soil profile over the crop period (planting-harvest) // kg.ha-1
  real :: runoff_from_plt      ! // OUTPUT //  cumulative Total quantity of water in run-off (surface + overflow) over the crop period (planting-harvest) // mm
  real :: Nmineral_from_plt !  // OUTPUT // mineral N from the mineralisation of humus and organic residues cumulated over the crop period (planting-harvest) // kg.ha-1
  real :: Nvolat_from_plt !  // OUTPUT // cumulative amount of N volatilised from fertiliser + organic inputs over the crop period (planting-harvest)// kg.ha-1
  real :: QNdenit_from_plt !  // OUTPUT // Cumulative denitrification of nitrogen from fertiliser or soil (if option  denitrification  is activated) over the crop period (planting-harvest)// kg.ha-1
!DR 13/03/2018 pour wheat4
  real :: cet_from_plt !// Evapotranspiration integrated over the cropping season (from planting or budbreak) // mm
! DR 02/03/2017 pour Macsur_vigne cumul a partir du debourrement des variables autres que plantes
  real :: drain_from_lev       ! // OUTPUT // cumulative amount of water drained at the base of the soil profile over the crop period (emergence or budbreak-harvest)  // mm
  real :: leaching_from_lev    ! // OUTPUT //cumulative N-no3 leached at the base of the soil profile over the crop period (emergence or budbreak-harvest) // kg.ha-1
  real :: runoff_from_lev      ! // OUTPUT // cumulative Total quantity of water in run-off (surface + overflow) over the crop period (emergence or budbreak-harvest) // mm
  real :: Nmineral_from_lev ! // OUTPUT // mineral N from the mineralisation of humus and organic residues cumulated over the crop period (emergence or budbreak-harvest) // kg.ha-1
  real :: Nvolat_from_lev ! // OUTPUT // cumulative amount of N volatilised from fertiliser + organic inputs over the crop period (emergence or budbreak-harvest)// kg.ha-1
  real :: QNdenit_from_lev ! // OUTPUT // Cumulative denitrification of nitrogen from fertiliser or soil (if option  denitrification  is activated) over the crop period (emergence or budbreak-harvest)// kg.ha-1
  real :: cet_from_lev  ! // OUTPUT // Evapotranspiration integrated over the cropping season (from emergence or budbreak) // mm
  real :: cum_et0_from_lev ! // OUTPUT // eos+eop // mm

! dr 16/12/2013 ajout de res_dispo_profmes par plante
  real :: SoilAvW   ! //OUTPUT// variable eau dispo pour la plante sur la profondeur profmes // mm
  real :: SoilWatM
! dr ajout des prof specifiques
  integer :: P_profmesW     ! // PARAMETER // depth of observed Water content  // cm //depths_paramv6.txt // 1
  integer :: P_profmesN     ! // PARAMETER // depth of observed Nitrogen content  // cm //depths_paramv6.txt // 1

! DR 29/05/2019 i add some variables to have the dranaige and the lixiviation under each soil layer and under the profmes value
  real :: husup_by_horizon(5)
  real :: azsup_by_horizon(5)
  real :: husup_under_profmes
  real :: azsup_under_profmes

! DR 25/06/2019 pour le module chloride il faut sortir les valeurs de drainage de chaque couche de 1 cm
  real :: husup_by_cm(nbCouchesSol)

! PB - gestion des ecritures
  integer :: flagEcriture  ! // PARAMETER // option for writing the output files (1 = mod_history.sti, 2=daily outputs,4= report outut, 8=balance outputs,16 = profil outputs,  32= debug outputs, 64 = screen outputs, 128 = agmip outputs) add to have several types of outputs  code 1/2 // PARAM // 1

!! ---------------------------------------
!! PB - les variables necessaires au debug
!  logical :: DEBUG = .FALSE.  

!  integer :: DEVELOP = 0  
!  integer :: CALAI = 0  
!  integer :: BIOMAER = 0  
!  integer :: SENESCEN = 0  
!  integer :: FRUIT = 0  
!  integer :: GRAIN = 0  
!  integer :: EAUQUAL = 0  
!  integer :: CROISSANCEFRONTRACINAIRE = 0  
!  integer :: PROFILRACINAIRE = 0  
!  integer :: DENSITEVRAIERACINAIRE = 0  
!  integer :: CALPSIBASE = 0  
!  integer :: REPARTIR = 0  
!  integer :: IRRIG = 0  
!  integer :: CALAPNENUPVT = 0  
!  integer :: CALCULAUTOMATIQUEIRRIGATION = 0  
!  integer :: APPORTSNPARPLUIEETIRRIGATION = 0  
!  integer :: APPORTSNPARENGRAISMINERAUX = 0  
!  integer :: APPORTSORGANIQUESETTRAVAILDUSOL = 0  
!  integer :: RESIDUS = 0  
!  integer :: ETATSURF = 0  
!  integer :: MINERAL = 0  
!  integer :: KETP = 0  
!  integer :: SHUTWALL = 0  
!  integer :: OFFRNODU = 0  
!  integer :: BNPL = 0  
!  integer :: LIXIV = 0  
!  integer :: TRANSPI = 0  
!  integer :: OFFREN = 0  
!  integer :: ABSON = 0  
!  integer :: MAJNSOL = 0  
!  integer :: STRESSEAU = 0  
!  integer :: STRESSN = 0  
!  integer :: NGRAIN = 0  
!  integer :: EXCESDEAU = 0  
!  integer :: CALTCULT_SJ = 0  
!  integer :: CALTCULT_SHUTWALL = 0  
!  integer :: CALRNET_SHUTWALL = 0  
!  integer :: TEMPSOL = 0  
!  integer :: HUMCOUV_SJ = 0  
!  integer :: HUMHEURE = 0  
!  integer :: SOLNU = 0  
!  integer :: DETASSEMENT = 0  
!  integer :: TASSESEMISRECOLTE = 0  


! DR 05/09/2012 on passe ces 2 varaibles en commun sinon on perd sa valeur
  real  :: surfAO
  real  :: surfAS

! DR 29/04/2013 j'ajoute un compteur pour le calcul force de priestley taylor en cas de donnees manquantes
 integer ::  compt_calcul_taylor
! DR 10/20/2015 ajout des dates pour les irrigation JC
  integer :: n_datedeb_irrigauto
  integer :: n_datefin_irrigauto

  integer :: code_ecrit_nom_usm


!DR 14022016 pour AgMIP ET
  real :: HR_vol_1_10   ! // OUTPUT //water content of the horizon 1-10 cm   //mm
  real :: HR_vol_1_30   ! // OUTPUT //water content of the horizon 1-30 cm   //mm
  real :: HR_vol_31_60  ! // OUTPUT //water content of the horizon 31-60 cm  //mm
  real :: HR_vol_61_90  ! // OUTPUT //water content of the horizon 61-90 cm   //mm
  real :: HR_vol_91_120   ! // OUTPUT //water content of the horizon 91-120 cm   //mm
  real :: HR_vol_121_150   ! // OUTPUT //water content of the horizon 121-150 cm //mm
  real :: HR_vol_151_180   ! // OUTPUT //water content of the horizon 151-180 cm //mm

  real :: hur_10_vol  ! // OUTPUT //water content of the horizon 10 cm //mm

! DR 31/05/2016 ajout du nombre de jours depuis le semis
  integer ::  day_after_sowing ! // OUTPUT //days after sowing //days
  ! dr 05/03/2018 pour AgMIP
  real :: HR_mm_1_30
  real :: HR_mm_31_60
  real :: HR_mm_61_90

  real :: nit_1_30
  real :: nit_31_60
  real :: nit_61_90
  real :: amm_1_30
  real :: amm_31_60
  real :: amm_61_90
  real :: soilN_rootdepth

  character(len=20)  :: info_level , model_name, site_name, experiment_name

  real :: N_mineralisation
  real :: tcult_tairveille
  real :: humidite_percent  !  // OUTPUT // Moisture in the canopy // %
! pour le paturage des vaches
  logical :: flag_onacoupe
  ! DR 07/04/2016 j'ajoute les varaibles pour la pature
  real :: CsurNres_pature
  real :: qres_pature


! DR et FR 20/07/2016 on garde les varaibles ini pour le cas ou la prairie meure
  character(len=3)    :: stade0_ini(2)
  real                :: lai0_ini(2)
  real                :: masec0_ini(2)
  real                :: QNplante0_ini(2)
  real                :: magrain0_ini(2)
  real                :: zrac0_ini(2)
  real                :: resperenne0_ini(2)
  real                :: densinitial_ini(2,5)
  logical             :: onreinitialise_ulai(2)

!! merge trunk 23/11/2020
! DR 02/04/2020 on ajoute les irrigations dans les sorties pour operate
  integer :: n_tot_irrigations
  real    :: q_irrigations(100)
  integer :: date_irrigations(100)

! 15/04/2020 DR j'ajoute 3 varaibles operate
  integer :: day_after_begin_simul
! 15/04/2020 DR Nb jours ou humair_percent > 90% entre amf et lax
  integer :: nb_days_humair_gt_90_percent1
  integer :: nb_days_humair_gt_90_percent2



end type Stics_Communs_


! *********************************** c
! *     variables transitoires      * c
! *********************************** c

type Stics_Transit_


  integer :: P_codlocirrig ! variable d'harmonisation des codlocirrg des differents fichiers techniques de la simulation (cultures associees)     // PARAMETER // code of irrigation localisation: 1= above the foliage, 2= below the foliage above the soil, 3 = in the soil // code 1/2/3 // PARTEC // 0 

!: NB - le 07/06/2004 - calcul de psibase
! TODO: variable locale a calpsibase ? ou dans le commun pour sorties ?
!19/09/2012 va dans plante
!  real :: psibase(2) ! P_nbplantes       // OUTPUT // Predawn leaf water potential potentiel foliaire de base // Mpascal


!: NB le 11/02/05 nouveautes courbes de dilution N
! revise le 11/09/05
  real :: bdilI(2)  
  real :: adilI(2)  
  real :: adilmaxI(2)  
  real :: bdilmaxI(2)  

  real :: deltabso(2,0:2)   ! pour chaque plante, AO/AS  
  real :: dltamsN(2,0:2)    ! pour chaque plante, AO/AS  

  logical :: humectation  
  integer :: nbjhumec  

  real :: pfmax2  

  real :: pluiesemis  

  real :: QH2Oi  
  real :: QNO3i  
  real :: QNH4i  

!  integer :: P_codetempfauche  ! // PARAMETER // option of the reference temperature to compute cutting sum of temperatures : upvt (1), udevair (2) // code 1/2 // PARAMV6 // 0
!  real :: P_coefracoupe(2)            ! 2 plantes     // PARAMETER // coefficient to define the proportion of dying roots after cut (grass) // SD // PARAMV6/PLT // 1

  real :: dltaremobilN(2,0:2)         ! 2 plantes, AO et AS

  real :: dltarestempN(2,0:2)         ! 2 plantes, AO et AS
  real :: dltarestemp(2,0:2)         ! 2 plantes, AO et AS
  real :: dltarespstruc(2,0:2)
  real :: innlax

  real :: spfourrage  
  real :: nspfourrage  

  integer :: P_codepluiepoquet  ! // PARAMETER // option to replace rainfall by irrigation at poquet depth in the case of poquet sowing // code 1/2 // PARAMV6 // 0
  integer :: P_nbjoursrrversirrig  ! // PARAMETER // number of days during which rainfall is replaced by irrigation in the soil after a sowing poquet // jours // PARAMV6 // 1

!  real :: P_SurfApex(2)     ! // PARAMETER // equivalent surface of a transpiring apex // m2 // PARAMV6/PLT // 1
!  real :: P_SeuilMorTalle(2)     ! // PARAMETER // relative transpiring threshold to calculate tiller mortality // mm // PARAMV6/PLT // 1
!  real :: P_SigmaDisTalle(2)     ! // PARAMETER // Coefficient used for the gamma law calculating tiller mortality //  // PARAMV6/PLT // 1
!  real :: P_VitReconsPeupl(2)     ! // PARAMETER // thermal time for the regeneration of the tiller population // nb tillers/degree C/m2 // PARAMV6 // 1
!  real :: P_SeuilReconsPeupl(2)     ! // PARAMETER // tiller density threshold below which the entire population won't be regenerated // nb tillers/m2 // PARAMV6/PLT // 1
!  real :: P_MaxTalle(2)     ! // PARAMETER // maximal density of tillers/m2 // Nb tillers/ // PARAMV6/PLT // 1
  integer :: PerTalle(2)  
  real :: sptalle(2)  
  real :: denstalle(2)  


  integer :: dateirr(2,100)  
  integer :: nbapirr(2)  

  integer :: dateN(2,100)  
  integer :: nbapN(2)  

  integer :: P_code_adapt_MO_CC  ! // PARAMETER // activation code for organic matter adaptation to climate change (1 = yes, 2 = no) // code1/2 // PARAMV6 // 0
  integer :: P_code_adaptCC_miner  ! // PARAMETER // activation code for the impact of climate change on mineralisation, parameter modification P_trefh and P_trefr (1 = yes, 2 = no) // code1/2 // PARAMV6 // 0
  integer :: P_code_adaptCC_nit  ! // PARAMETER // activation code for the impact of climate change on nitrification, parameter modification P_tnitmin, P_tnitmax, P_tnitopt and P_tnitopt2 (1 = yes, 2 = no) // code1/2 // PARAMV6 // 0
  integer :: P_code_adaptCC_denit  ! // PARAMETER // activation code for the impact of climate change on denitrification, parameter modification P_trefdenit1 and P_trefdenit2 (1 = yes, 2 = no) // code1/2 // PARAMV6 // 0
  integer :: P_periode_adapt_CC  ! // PARAMETER // year number to calculate moving temperature average // year // PARAMV6 // 1
  integer :: P_an_debut_serie_histo  ! // PARAMETER // beginning year for the calculation of moving average temperature on period_adapt_CC // year // PARAMV6 // 0
  integer :: P_an_fin_serie_histo  ! // PARAMETER // ending year for the calculation of moving average temperature on period_adapt_CC // year // PARAMV6 // 0
  real :: P_param_tmoy_histo  ! // PARAMETER // mean temperature over the period of adaptation to climate change // degree C // PARAMV6 // 1
!  integer :: P_nbj_pr_apres_semis  ! // PARAMETER // days number to calculate rainfall need to start sowing (is codesemis is activated) // day // PARAMV6 // 1
!  integer :: P_eau_mini_decisemis  ! // PARAMETER // minimum amount of rainfall to start sowing (when codesemis is activated) // mm // PARAMV6 // 1
!  real :: P_humirac_decisemis  ! // PARAMETER // effect of soil moisture for sowing decision ( from 0 to 1 : 0 = no sensitivity to drought; 1 = very sensitive) // SD // PARAMV6 // 1
!  DR 04/03/2019 j'ajoute un code dans param_newform pour desactiver le nouveau calcul de BM pour l'evaluation SMS
!  integer :: P_code_auto_profres(2)   ! // PARAMETER // option to calculate the value of profres with the function profres = proftrav *(1-exp(-resk.(proftrav-resz)) (1 = profres in tec file, 2 = new function) // SD// PARAMV6 //1
!  real  :: P_resk(2)   ! // PARAMETER // option to calculate the value of profres with the function profres = proftrav *(1-exp(-resk.(proftrav-resz)) // SD// PARAMV6 //1
!  real  :: P_resz(2)  ! // PARAMETER // option to calculate the value of profres with the function profres = proftrav *(1-exp(-resk.(proftrav-resz)) // SD// PARAMV6 //1
  integer :: P_code_CsurNsol_dynamic !> // PARAMETER//code to activate the calculation of CsurNsol as Chumt/Nhumt // SD // PARAM// 1

! DR et ML et SYL 15/06/09
! ************************
! introduction de la fin des modifications de Sylvain (nadine et FR)
! dans le cadre du projet PERMED
!    real :: P_swfacmin  ! // PARAMETER // minimul value for drought stress index (turfac, swfac, senfac) // SD // PARAMV6 // 1
!    real :: LAIapex(2)
! DR 19/09/2012 passe dans plante
!    real :: mortreserve(2)   !  // OUTPUT // Reserve biomass corresponding to dead tillers // t.ha-1.d-1
!    real :: P_SeuilLAIapex(2)  ! // PARAMETER // Maximal value of LAI+LAIapex when LAIapex isn't nil // m2/m2 // PARAMV6/PLT // 1

! DR et ML et SYL 15/06/09
! ************************
! on indexe P_codedyntalle sur la plante
!    integer :: P_codetranspitalle  ! // PARAMETER // Choice of the ratio used to calculate tiller mortality: et/etm (1) ou epc2 / eopC (2) // code 1/2 // PARAMV6 // 0
!    integer :: P_codedyntalle(2)  ! // PARAMETER // Activation of the module simulating tiller dynamic: yes (1), no (2) // code 1/2 // PARAMV6/PLT // 0

! DR et ML et SYL 15/06/09
! ************************
! introduction de la fin des modifications de Sylvain (nadine et FR)
! dans le cadre du projet PERMED
! ####
! SYL 05/03/08 1 parametre pour caracteriser la composition de la biomasse residuelle a la coupe
! NB  06/03/08 1 parametre et 2 variables pour le calcul du stade debut montaison
! SYL 26/02/09 1 parametre definissant la quantite de reserve par plante (ou par talle)
! DR et ML et SYL 16/06/09
! on supprime nvernal qui ne sert a rien

!    real :: P_tigefeuilcoupe(2)  ! // PARAMETER // stem (structural part)/leaf proportion the cutting day // SD // PARAMV6/PLT // 1
! DR 19/09/2012 passe dans plante
!    real :: somcourmont(2)   !  // OUTPUT // Cumulatied units of development from the start of vernalisation // degree.days
!    real :: P_resplmax(2)  ! // PARAMETER // maximal reserve biomass // t ha-1 // PARAMV6 // 1

! ####
! DR et ML et SYL 15/06/09 FIN introduction de la fin des modifications de Sylvain


!  integer :: P_codemontaison(2)     ! // PARAMETER // code to stop the reserve limitation from the stem elongation // code 1/2 // PARAMV6 // 0

  integer :: onestan2  

  integer :: P_codecalferti  ! // PARAMETER // automatic calculation of fertilisation requirements: yes (2), no (1) // code 1/2 // PARAMV6 // 0
  integer :: P_codetesthumN  ! // PARAMETER // automatic fertilisation calculations // code 1/2 // PARAMV6 // 0
  real :: P_dosimxN  ! // PARAMETER // maximum amount of fertilisation authorised at each time step (mode automatic fertilisation) // kg N ha-1 // PARAMV6 // 1
  real :: P_ratiolN  ! // PARAMETER // Nitrogen stress index below which we start an fertilisation in automatic mode (0 in manual mode) // between 0 and 1  // PARAMV6 // 1


! ajout param Bruno mai 2012
! Bruno: ajout de 5 nouveaux parametres relatifs a limitation decomposition par N min
  integer :: P_codeNmindec ! // PARAMETER // option to activate the available N :yes (1), no(2) // code 1/2 //PARAMv6 // 1
  real :: P_rapNmindec  ! // PARAMETER //slope of the linear relationship between the fraction of mineral N available for residue decomposition and the amount of C in decomposing residues (0.001)// g.g-1 // PARAMV6 //1
  real :: P_fNmindecmin ! // PARAMETER //minimal fraction of mineral N available for residues decomposition (if codeNmindec is activated) // SD // PARAMV6 //1

! Bruno et Loic : ajout de nouvelles variables initiales plante
! Loic et DR 18/10/2016 ajout de code pour ectiver les nouveaux parametres
!  integer :: P_code_gdh_Wang! // PARAMETER // option to activate Wang et Engel (1998) effect of temperature on development units for emergence :yes (1), no(2) // code 1/2 //PARAMv6 // 1
!  real :: P_maperenne0  ! initial value of biomass of storage organs in perennial crops // t ha-1 /
!  real :: P_QNperenne0  ! initial value of nitrogen amount in storage organs in perennial crops // kg ha-1 /
  real :: P_QNrestemp0  ! initial value of nitrogen amount in vegetative organs of all crops // kg ha-1 /
  real :: P_msrac0      ! initial value of living root biomass // t ha-1 /
  real :: P_QNrac0      ! initial value of nitrogen in living roots // kg ha-1 /
  real :: P_pHminden        ! pH below which the N2O molar fraction is 100%                      3.0
  real :: P_pHmaxden        ! pH beyond which the N2O molar fraction is minimum (<= ratiodenit)  8.0
  real :: P_Kd              ! Affinity constant for NO3 in denitrification (mg N/L)              215
  real :: P_kdesat          ! rate constant of de-saturation (d-1)                               1.0
  real :: P_wfpsc           ! wfps threshold beyond which denitrification occurs                 0.62
  real :: P_Q10den          ! Q10 of the temperature function acting on denitrification          5.0
  real :: P_vnitmax         ! maximum nitrification rate (mg N kg-1 d-1)                         20.
  real :: P_Kamm            ! affinity constant for NH4 in nitrification (mg N/l)                24.
  real :: P_ammmin          ! minimum NH4 concentration found in soil (mg N/kg)                   1.0
! Loic et Bruno fevrier 2014
!  real :: P_rapdia          ! ratio of coarse roots diameter to fine roots diameter
!  real :: P_RTD             ! root tissue density (g/cm3)
!  real :: P_propracfmax     ! proportion of fine roots emitted in the layer 0-1 cm (in length; maximum value over the root profile)
!  real :: P_zramif          ! distance between the root apex and the first root ramification (cm)
!  real :: P_tdoptdeb        ! optimal temperature for calculation of phasic duration between dormancy and bud breaks // degree C
!  real :: P_Gmin1           ! Mineralization parameters of the new model : mineralization rate constant (day-1)
!  real :: P_Gmin2           ! Mineralization parameters of the new model : clay content factor (%-1)
!  real :: P_Gmin3           ! Mineralization parameters of the new model : CaCO3 content factor (%-1)
!  real :: P_Gmin4           ! Mineralization parameters of the new model : pH factor 1 (pH-1)
!  real :: P_Gmin5           ! Mineralization parameters of the new model : pH factor 2 (pH)
!  real :: P_Gmin6           ! Mineralization parameters of the new model : C/N soil factor 1 (nd)
!  real :: P_Gmin7           ! Mineralization parameters of the new model : C/N soil factor 2 (nd)
! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
!  14/06/2017 DR j'active les 3 facons de calculer la partie inerte
!  integer :: P_codeFinert   ! code to activate the 2 methods of the calculation oh the inert fraction of humus
!  integer :: P_codeFunctionFinert   ! code to activate the 2 methods of the clculation oh the inert fraction of humus
!  real :: P_Finert1         ! Mineralization parameters of the new model
!  real :: P_Finert2         ! Mineralization parameters of the new model
  ! A VOIR SI A CONSERVER PAR LA SUITE ?
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!
  real :: P_kdisrac    ! parameter of the daily allocation of root length in the soil profile  Y = exp(-kdisrac(z-zc)2)
  real :: P_zdisrac    ! parameter of the daily allocation of root length in the soil profile  zc = max(0, zrac-zdisrac)
  real :: P_alloperirac     ! taux d'allocation des reserves du perisperme a l'apex racinaire (au stade levee)
  integer :: P_humirac      ! 1 = la fonction F_humirac atteint un plateau (ancien code) / 2 = la fonction n'atteint pas de plateau (identique a la phase germination-levee)

 ! DR 29/08/2012 ajout d'un code pour avoir ou non les sorties aGmip
!   integer :: P_Config_Output ! // PARAMETER // configuration optional Outputs  (1=no,2=screen,3=agmip) // SD // PARAMV6 // 1
! ML 29102012
  integer :: P_codetrosee   ! // PARAMETER // option to choose the way to calculate the hourly dew temperature : linear interpolation (1), sinusoidal interpolation (Debele Bekele et al.,2007)(2) // code 1/2 //PARAMv6 // 1
  integer :: P_codeSWDRH    ! // PARAMETER // option to activate the calculation of surface wetness duration : yes (1), no (2) // code 1/2 //PARAMv6 // 1

! DR 06/05/2015 je rajoute un code pouyr tester la mortalite des racines
! integer :: P_codemortalracine ! // PARAMETER // masec servant a calculer les racines mortes a la coupe  : masec (1), masectot (2) // code 1/2 //PARAMv6 // 1
! DR 05/02/2016 pour regles de semis agmipWheat
!integer :: P_rules_sowing_AgMIP ! // PARAMETER // activation of the semis rules AgMIP wheat3 yes(1) no(2) // code 1/2 //PARAMv6 // 1
!integer :: P_Flag_Agmip_rap  ! // PARAMETER // report specific outputs AgMIP nothing(1) AgMIP(2) Macsur(3) // code 1/2/3 //PARAMv6 // 1
!integer :: P_type_project  ! // PARAMETER // activation des regles de semis AgMIP AgMIP Wheat(1) "AgMIP Wheat Giacomo (HSC)(2) wheat Canopy temp(3) face_maize(4) new wheat3(5) // code 1/2/3/4/5 //PARAMv6 // 1


!integer :: P_option_thinning ! // PARAMETER // enabling of several thinning yes(1),(no) 2 //code 1/2 //PARAMv6 // 1
integer :: P_option_pature ! // PARAMETER // enabling of pasture of grassland yes(1),(no) 2 //code 1/2 //PARAMv6 // 1
!integer :: P_option_engrais_multiple  ! // PARAMETER // enabling of using several kind of fertilizer yes(1),(no) 2 //code 1/2 //PARAMv6 // 1

integer :: P_coderes_pature ! // PARAMETER // residue type: 1=crop residues,  2=residues of CI,  3=manure,  4=compost OM,  5=mud SE,  6=vinasse,  7=corn,  8=other // code 1 to 10 // PARAMv6 // 0

!integer :: P_codejourdes


   real :: P_pertes_restit_ext   ! // PARAMETER // dejections animales non restituees sur les parcelles //  // PARAMv6 // 1
   real :: P_Crespc_pature  ! // PARAMETER // carbon proportion in organic residue //  // PARAMv6 // 1
   real :: P_Nminres_pature    ! // PARAMETER // N mineral content of organic residues  // % fresh matter // PARAMv6 // 1
   real :: P_eaures_pature    ! // PARAMETER // Water amount of organic residues  // % fresh matter // PARAMv6 // 1
   real :: P_coef_calcul_qres   ! // PARAMETER // ?  // ? // PARAMv6 // 1
integer :: P_engrais_pature  ! // PARAMETER // fertilizer type  : 1 =Nitrate.of ammonium ,2=Solution,3=urea,4=Anhydrous ammoniac,5= Sulfate of ammonium,6=phosphate of ammonium,7=Nitrateof calcium,8= fixed efficiency    // * // PARAMv6 // 1
   real :: P_coef_calcul_doseN   ! // PARAMETER // ?  // ? // PARAMv6 // 1


!   integer :: P_codeFunctionFinert   ! code to activate the 2 methods of the clculation oh the inert fraction of humus
! DR 03/07/2018 activation du melange H2O N lors du travail du sol
! integer :: P_codemixN_H2O
! real :: P_tmix

! PL, 12/04/2022: option inutile maintenant
 ! DR 12/06/2019 ajout de l'option to mix the humus on the depth itrav1-itrav2 (yes=1) or on the depth 1-itrav2 (no=2) // 1,2
!  integer :: P_code_depth_mixed_humus

! DR 11/04/2022 on commente le calcul facon 9.1 qui ne sera plus utilis�
!  integer :: P_code_stock_BM
 ! Ajout Loic Avril 2021 : specificites ISOP
  integer :: P_code_ISOP
  integer :: P_code_pct_legume
  real    :: P_pct_legume

end type Stics_Transit_

end module Stics
