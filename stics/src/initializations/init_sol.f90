! *---------------------------------------------------* c
! *                    LE SOL                         * c

! sous_programme de calcul des initialisations sol :
!   calcul de la profondeur du sol
!   calcul pour lixiv des cellules de melange
!   initialisation pour mineralisation
!   prise en compte des P_cailloux dans le calcul pour
!    les caracteristiques du sol : hcc, hmin, da
!    les Humidites et quantites d'azote initiales : hinit, NO3init, NH4init
! DR 23/11/07 adaptation des MO a l'effet du CC : recalcul des parametres MO et res
! DR 14/02/2020 prise en compte de la participation de Nicolas Beaudoin sur les algos concerant
!  la prise en compte des cailloux dans Stics
! *--------------------------------------------------------------------------------
module initialisations_sol_m
USE Stics
USE Parametres_Generaux
USE Sol
USE Climat
USE Station
USE messages
implicit none
private
public :: initialisations_sol
contains
subroutine initialisations_sol(logger,sc,pg,t,soil,st)
! Arguments
    type(logger_), intent(in) :: logger
    type(Stics_Communs_),       intent(INOUT) :: sc  
    type(Stics_Transit_),       intent(IN)    :: t
    type(Parametres_Generaux_), intent(IN)    :: pg  
    type(Sol_),                 intent(INOUT) :: soil  
    type(Station_),             intent(INOUT) :: st

! Variable(s) locale(s)
      integer :: i  !  
      integer :: icou  !  
      integer :: j  !  
      integer :: nepc  !  
      integer :: nhel  !  
      integer :: npro  
      real    :: dax  !  
      real    :: hccx  !  
      real    :: hmincx  

    ! calcul de la profondeur de sol (profsol) et du numero des couches de transition (izc)
      soil%profsol = 0.
      sc%NH = 0
      do i = 1, nbLayers_max  !5   ! on suppose qu'il y a max. 5 couches
        if (soil%P_epc(i) == 0) EXIT ! epaisseur de la couche = zero, on quitte la boucle
        soil%profsol = soil%profsol + soil%P_epc(i)
        soil%izc(i) = soil%profsol
        sc%NH = i
      ! attention si profsol est > profimpermeable !....
        if (soil%P_codemacropor == 1) then
          if(sc%NH < nbLayers_max) then
              if(soil%P_infil(i) <= 0 .and. soil%P_epc(i+1) > 0) then !TODO: A terme, NH pourrait etre variable en fonction de ce qui est lu dans le fichier sol ?
                     call EnvoyerMsgHistorique(logger, SOIL_DEPTH_WARNING)
                     if (soil%P_codrainage == 1) then
                        call exit_error(logger, SOIL_DEPTH_ERROR)
                     endif
              endif
          endif
        endif
      end do


    ! Calculs pour LIXIV
    ! ------------------
    ! ncel  = nombre de cellules de melange
    ! icel  = cote des cellules de melange
    ! izcel = numero des cellules de transition
    ! nhel  = nombre de cellules correspondant a P_epd
      soil%icel(0) = 0
      soil%ncel = 0
      npro = 0
      do i = 1, sc%NH
        nepc = soil%P_epc(i)
        npro = npro + nepc
! **    si P_epd est nul, on prend 1 cm par defaut
        if (soil%P_epd(i) <= 0) then
          call EnvoyerMsgHistorique(logger, MESSAGE_198)
          soil%P_epd(i) = 1
        endif

        nhel = int(nepc / soil%P_epd(i))
        if (nhel > nepc) nhel = nepc
        if (nhel < 1) nhel  = 1
        soil%P_epd(i) = int(nepc / nhel)
        do j = 1,nhel
          soil%ncel = soil%ncel +1
          soil%icel(soil%ncel) = soil%icel(soil%ncel-1) + soil%P_epd(i)
        end do
        if (soil%icel(soil%ncel) /= npro) then
            soil%ncel = soil%ncel + 1
            soil%icel(soil%ncel) = npro
        end if
        soil%izcel(i) = soil%ncel
      end do


    ! si P_codemacropor = 0 on annule les numeros de couche de transition
      if (soil%P_codemacropor == 2) then
        soil%P_codefente = 2
        soil%izc(:) = 0
        soil%izcel(:) = 0
      endif

    ! drainage
      if (soil%P_codrainage == 2) soil%P_profdrain = 0.
      soil%ldrains = soil%P_ecartdrain / 2.

    ! PB - 15/03/2005 - deplaces. On reinitialise en debut de cycle dans tous les cas.
      sc%QLES     = 0.  ! TODO : a deplacer dans initnonsol ?
      sc%DRAT     = 0.  ! TODO : a deplacer dans initnonsol ?
      soil%qlesd    = 0.
      soil%remontee = 0.
      ! Ajout Loic Juillet 2018
      soil%remonteemax = 0.  ! // capillary rise from simulated deep soil layer (true or false)

      if (soil%P_codrainage == 1) then
         if (soil%P_codemacropor == 2) then
           ! mise en place d'un retour code erreur pour la PIC
           call exit_error(logger, SOIL_DRAIN_ERROR)
         endif

! ** domi - 23/07/03 - test uniquement si codehnappe
         if (pg%P_codhnappe == 2) then
            if (pg%P_distdrain > soil%ldrains) then
               call exit_error(logger, SOIL_DRAIN_DISTANCE_ERROR)
            endif
         endif
      endif

      soil%qdraincum = 0.

! ** initialisation pour mineralisation
! Bruno juillet 2017 jhum = profondeur de sol avec MO initiale
!      soil%jhum = (soil%P_profhum - int(soil%P_profhum))*100
!      if(soil%jhum == 0) soil%jhum = int(soil%P_profhum)
      soil%jhum =int(soil%P_profhum)
! fin modif
      if (soil%P_profhum > soil%profsol) then
        call EnvoyerMsgHistorique(logger, MESSAGE_171)
        soil%P_profhum = soil%profsol
      endif
      soil%nitrifj = 0.
      sc%Qminh = 0.
      sc%Qminr = 0.
      soil%Qminrcult =  0.
      soil%cumvminr = 0.
      soil%cumvminh = 0.
      sc%tnhc = 0.
      sc%tnrc = 0.
      sc%Qnitrif = 0.
      sc%Qem_N2O = 0.
      sc%Qem_N2Onit = 0.
      sc%Qem_N2Oden = 0.
 ! variables suivantes deplacees depuis stics_zero
      sc%CO2res = 0.
      sc%CO2hum = 0.
      sc%CO2sol = 0.
      sc%QCO2sol = 0.
      sc%QCO2hum = 0.
      sc%QCO2res = 0.
      sc%QCO2mul = 0.
      sc%QCprimed = 0.
      sc%QNprimed = 0.
      sc%cum_immob = 0.
      sc%cum_immob_positif = 0.


! calcul des parametres sol equivalents lorsque le sol contient des P_cailloux
! --------------------------------------------------------------------------
      do icou = 1, sc%NH   !5

        if (soil%P_epc(icou) <= 0) CYCLE

        dax = pg%P_masvolcx(soil%P_typecailloux(icou))
        hccx = pg%P_hcccx(soil%P_typecailloux(icou))

    ! On suppose que les humidites mini sont proportionnelles entre Cailloux et Terre Fine
        hmincx = soil%P_hminf(icou) * hccx / soil%P_hccf(icou)

    ! DA
        soil%da(icou) = (soil%P_cailloux(icou) * dax + (100. - soil%P_cailloux(icou)) * soil%P_daf(icou)) / 100.

    ! HCC
        soil%hcc(icou) = soil%P_cailloux(icou) * hccx * dax + (100. - soil%P_cailloux(icou)) * soil%P_hccf(icou) * soil%P_daf(icou)
        soil%hcc(icou) = soil%hcc(icou) / soil%da(icou) / 100.

    ! HMIN
        soil%hmin(icou) = soil%P_cailloux(icou) * hmincx * dax +                                            &
                         (100. - soil%P_cailloux(icou)) * soil%P_hminf(icou) * soil%P_daf(icou)
        soil%hmin(icou) = soil%hmin(icou) / soil%da(icou)  / 100.

    ! Humidites et quantites d'azote initiales
        sc%Hinit(icou) = sc%P_Hinitf(icou) * soil%hcc(icou) / soil%P_hccf(icou)
        soil%NO3init(icou) = soil%P_NO3initf(icou) * soil%hcc(icou) * soil%da(icou) / (soil%P_hccf(icou) * soil%P_daf(icou))

        if (soil%P_codenitrif < 2) then
           sc%NH4init(icou) = sc%P_NH4initf(icou) * soil%hcc(icou) * soil%da(icou) / (soil%P_hccf(icou) * soil%P_daf(icou))
        else
           sc%NH4init(icou) = 0.
        endif

      end do

! initialisation de pHvol
      soil%pHvol = soil%P_pH0
      soil%dpH = 0.
      soil%Nvolatorg = 0.
! 22/02/17 initialisation de la variable PH
      soil%pH_soil = soil%P_pH0



! DR 23/11/07 adaptation des MO a l'effet du CC
    if (t%P_code_adaptCC_miner == 1 .or. t%P_code_adaptCC_nit == 1 .or. t%P_code_adaptCC_denit == 1) then

    ! si on veut activer au moins un des effets adaptation MO au CC
      !TODO: call effetadaptMOauCC

    else

    ! si on veut rien activer
      sc%var_trefh(sc%numcult)      = pg%P_trefh
      sc%var_trefr(sc%numcult)      = pg%P_trefr
      sc%var_tnitmin(sc%numcult)    = pg%P_tnitmin
      sc%var_tnitmax(sc%numcult)    = pg%P_tnitmax
      sc%var_tnitopt(sc%numcult)    = pg%P_tnitopt
      sc%var_tnitopt2(sc%numcult)    = pg%P_tnitopt2

    endif
! BM 19/05/2011 deplace ici pour pb d'initialisation plus haut
      sc%FTEMhb = (pg%P_FTEMha-1.) * exp(pg%P_FTEMh * sc%var_TREFh(sc%numcult))
      sc%FTEMrb = (pg%P_FTEMra-1.) * exp(pg%P_FTEMr * sc%var_TREFr(sc%numcult))

! initialisations pour CROIRA
    ! test sur P_obstarac
      if (soil%P_obstarac > soil%profsol) then
        call EnvoyerMsgHistorique(logger, MESSAGE_80)
        soil%P_obstarac = soil%profsol
      endif


    ! initialisation pour l'evaporation sol (SOLNU)
      sc%bouchon   = 1
      sc%sumes0    = 0.
      sc%sumes1    = 0.
      sc%sumes2    = 0.
      sc%esol      = 0.
      sc%hi = soil%hcc(1) * soil%da(1) / 100.

      if (sc%P_codesuite == 0) then
        sc%sum2     = 0.
        sc%smes02   = 0.
        sc%sesj0    = 0.
        sc%ses2j0   = 0.
        sc%esreste  = 0.
        sc%esreste2 = 0.
      endif

    ! Humidite residuelle (Brisson et Perrier, 1991)
      sc%ha     = soil%P_argi / 100. / 15. * soil%da(1)
      sc%hurlim = sc%ha * 10.
      sc%hpf = soil%hmin(1) * soil%da(1) / 100.
      soil%aevap = st%P_aclim / 2. * (0.63 - sc%ha)**(5./3.) * (sc%hi - sc%ha)
    ! 17/06/04 on interdit P_zesx>profsol
      if (soil%P_zesx > soil%profsol) then
        soil%P_zesx = soil%profsol
        call EnvoyerMsgHistorique(logger, MESSAGE_182)
      endif

return
end subroutine initialisations_sol
end module initialisations_sol_m
 
