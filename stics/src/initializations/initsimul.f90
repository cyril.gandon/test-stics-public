! **********************************************
! * routine pour initialiser les variables     *
! * en debut de simulation. (mises a zero,...) *
! appele uniquement si numcult > 1 (plus d'une annee de culture)
!      et si P_codeperenne /= 2 (c'est une annuelle) ou P_codeinitprec /= 2 (reinitialisation)
!
! **********************************************
module initsimul_m
use phenology_utils
USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
implicit none
private
public :: initsimul
contains
subroutine initsimul(sc,pg,p,itk,t)

  type(Stics_Communs_),       intent(INOUT) :: sc  

  type(Parametres_Generaux_), intent(INOUT) :: pg ! TODO : pg en IN, P_codemsfinal a dupliquer  

  type(Plante_),              intent(INOUT) :: p  
  type(ITK_),                 intent(INOUT) :: itk  
  type(Stics_Transit_),       intent(INOUT) :: t  

 ! PB - 21/01/05 - on force P_codemsfinal a qd on enchaine une perenne ! TODO : on modifie un parametre general !!!
!      if (p%P_codeperenne == 2 .and. pg%P_codeinitprec == 2 .and. sc%nbans > 1) pg%P_codemsfinal = 2
      if (p%P_codeperenne == 2) pg%P_codemsfinal = 2

        p%caljvc = 0.
        p%nsencour = 0
        p%nsencourpre = 0
        p%somelong = 0.
        p%somcour = 0.
        p%somcourdrp = 0.
        p%somtemp = 0.
        p%ndebsen = 0

        p%somcourmont = 0.

        p%nplt = itk%P_iplt0 - sc%P_iwater + 1
      ! NB le 13/05/05 initialisation de la pluie pour la battance
        t%pluiesemis   = 0.
      ! NB le 10/09/05 car pb enchainement annees pour la vigne avec reinitialisation
        if (p%P_stade0 /= dor) p%nger = 0
        p%nlev = 0
        p%namf = 0
        p%nlax = 0
        p%ndrp = 0
        p%nsen = 0
        p%nlan = 0
        p%nmat = 0
        p%nrec = 0
        p%nnou = 0
        p%nflo = 0
        p%nstopfeuille = 0
        p%ndebdes = 0
        p%nrecalpfmax = 0

        p%tursla(:) = 1.

        p%mafeuiljaune(:) = 0.

        p%matigestruc(:) = 0.
        p%msresjaune(:) = 0.
        p%msjaune(:) = 0.
        p%pdsfruittot(:) = 0.
        p%mareserve(:) = 0.

        p%msres(:) = itk%P_msresiduel(0)

        p%varrapforme = p%P_rapforme

        p%durvie(:,:) = 0.
        p%dltams(:,:) = 0.
        p%pfeuilverte(:,:) = 0.

        t%humectation = .FALSE.
        t%nbjhumec = 0

      ! DR et ML et SYL 16/06/09
      ! introduction de la montaison pour les prairies
      ! ####
      ! SYL 11/03/08
        if(p%P_codemontaison == 1)then
          t%onestan2 = 1
        endif

return
end subroutine initsimul
end module initsimul_m
 
