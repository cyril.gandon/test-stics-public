!subroutine Stics_Initialisation_Boucle_Annees(sc,p,pg,itk,c,sta,soil,t)   !DR 19/07/2012 c ,sta et soil n'est pas utilise
module Stics_Initialisation_Boucle_Annees_m
use stics_files
use messages
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
USE Parametres_Generaux
USE initsimul_m, only: initsimul
use climate_utils
use calcDeltaTCC_m, only: calcDeltaTCC
implicit none
private
public :: Stics_Initialisation_Boucle_Annees
contains
subroutine Stics_Initialisation_Boucle_Annees(logger,sc,p,pg,itk,c,t)
    type(logger_), intent(in) :: logger
    type(Stics_Communs_),        intent(INOUT) :: sc  

    type(Plante_),               intent(INOUT) :: p(sc%P_nbplantes)  

    type(Parametres_Generaux_),  intent(INOUT) :: pg    ! TODO pg devrait etre en IN, tous les parametres modifies doivent etre dupliques dans sc  

    type(ITK_),                  intent(INOUT) :: itk(sc%P_nbplantes)  

    type(Climat_),               intent(IN) :: c

!    type(Station_),              intent(INOUT) :: sta

!    type(Sol_),                  intent(INOUT) :: soil

    type(Stics_Transit_),        intent(INOUT) :: t  


!integer, intent(IN) :: P_culturean ! Code culture annuelle ou a cheval sur 2 annees
!character(len=12), intent(IN) :: P_codesimul ! Code type simulation
!integer, intent(INOUT) :: nbans ! Nombre d'annees de simulation

! Variables locales
    integer :: i  
    real :: Tm_histo


      ! nbans est calcule par rapport au nombre d'annees climatiques fournies en entrees.
      ! Pour une culture a cheval sur deux annees, il faut enlever une annee de simulation.
      ! PL 2023/10/20: little fix to be sure that sc%nbans is still > 0 
        if (sc%P_culturean /= 1 .and. sc%nbans > 1) sc%nbans = sc%nbans-1

      ! Pour les versions -stress sans simulation de plante, on n'effectue
      ! la simulation que sur une seule annee.
        if (sc%P_codesimul == CODE_FEUILLE) then
          sc%nbans = 1
        end if


      ! initialisation de variables qu'on n'effectue qu'une fois en debut de simulation.
        do i = 1, sc%P_nbplantes
        ! DR et FR 17/02/2015 on conserve le codeperenne de la plante qu'on change quand on fait un semis la premiere annee
          p(i)%codeperenne0 = p(i)%P_codeperenne
          call initsimul(sc,pg,p(i),itk(i),t)
        end do

! ************************* adaptation des Mo au CC ****************************
! DR 26/11/07 on calcule la temperature moyenne annuelle sur la serie clim dispo
! on a toutes les moyennes annuelles (1ier jour serie premiere annee a j-1 annee d'apres)
! dans tmoy_an(1,i)=an, tmoy(2,i)=tmoy annuelle i etant egal a numcult
      ! DR 19/10/09  on enleve le test sur codeoutsti
        if (t%P_code_adaptCC_miner == 1 .or. t%P_code_adaptCC_nit == 1 .or. t%P_code_adaptCC_denit == 1) then

          if (t%P_code_adapt_MO_CC == 1) then
            if (sc%nbans <= t%P_periode_adapt_CC) then
              call EnvoyerMsgHistorique(logger, MESSAGE_452)
            endif
          endif
          sc%deltaT_adaptCC = calcDeltaTCC(sc,t,c)
        endif

! ********************** fin adaptation des Mo au CC ****************************

return
end subroutine Stics_Initialisation_Boucle_Annees
end module Stics_Initialisation_Boucle_Annees_m
 
