! ******************************************
! * Routine d'initialisation de variables  *
! * plante, en debut de cycle.             *
! ******************************************
module initcycle_m
USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE initsimul_m, only: initsimul
implicit none
private
public :: initcycle
contains
subroutine initcycle(sc,pg,p,itk,t)
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT)    :: pg  
  type(Plante_),              intent(INOUT) :: p  
  type(ITK_),                 intent(INOUT) :: itk  
  type(Stics_Transit_),       intent(INOUT) :: t  

    ! changement d'origine si numcult > 1 et enchainement sur des perennes
      if (sc%numcult > 1) then
         if (p%P_codeperenne == 2 .and. pg%P_codeinitprec == 2) then
            p%dltams(:,0) = p%dltams(:,sc%n)
            p%durvie(:,0) = p%durvie(:,sc%n)
            p%lai(:,0) = p%lai(:,sc%n)
            p%masecnp(:,0) = p%masecnp(:,sc%n)
            p%pfeuilverte(:,0) = p%pfeuilverte(:,sc%n)
        ! pour les vernalisantes, quand on enchaine la plante est deja levee. On met nlev = 1
            if (p%P_codebfroid == 2) p%nlev = 1
         else
        ! rajout d'un test sur codeinstal pour que l'enchainement redemarre au stade de debut
        ! = nlev dans notre cas sinon on repart la deuxieme annee avec nlev  = 0
          if (p%codeinstal /= 1) call initsimul(sc,pg,p,itk,t)
         endif
      endif

      p%upobs(:) = 0.
      ! Modifs Bruno et Loic fevrier 2013 msrac et dtj initialises dans initnonsol
      p%upvt(:) = 0.
      p%masecnp(:,1:sc%nbjmax) = 0.
      p%lai(:,1:sc%nbjmax)   = 0.
      p%deltai(:,:) = 0.
      p%magrain(:,1:sc%nbjmax)  = 0.
      p%laisen(:,:)   = 0.
      p%abso(:,:) = 0.
      p%absoaer(:) = 0.
      p%absorac(:) = 0.
      p%absoper(:) = 0.
      p%QNabso(:) = 0.
      p%QNabsoaer(:) = 0.
      p%QNabsorac(:) = 0.
      p%QNabsoper(:) = 0.
      p%demande(:) = 0.
      p%demandeper(:) = 0.
      p%demandetot = 0.
      p%ircarb(:,:) = 0.

      p%QNplantenp(:,1:sc%nbjmax) = 0.
      p%CNgrain(:) = 0.
      p%strphotveille(:) = 1.
      p%dltaisen(:) = 0.
      sc%cumdltaremobilN = 0.
      p%laimax(:) = 0.

! merge trunk 23/11/2020
      ! DR 24/04/2020 pour operate on a detecte que qq variables n'etaient pas reinitilis�es en cas d'enchainement climatique (celles recalcul�es a un stade par exemple)
      ! je les reinitialise ici
      p%H2Orec_percent = 0.
      p%p1000grain = 0.
      p%vitmoy = 0.
      p%pgrain(:)=0.
      p%H2Orec(:)=0.
      p%nst1   = 0
      p%nst2   = 0
      p%str1   = 0.
      p%stu1   = 0.
      p%inn1   = 0.
      p%str2   = 0.
      p%stu2   = 0.
      p%inn2   = 0.

! 26/11/2020 anniversaire a Rachel ! c'est l'initialisation de ins1 � 1 qui fait la modif..
! pour les cultures non sem�es on a deja calcule un inns fonction de inn0 et qnplante0
!      p%swfac(:) = 1.
!      p%turfac(:) = 1.
!      p%inns(:) = 1.

! dr 23/04/2020 avant que ce ne soit calcul� on l'initialise a 1.
      p%inn1moy = 1.
      p%turfac1moy = 1.
      p%swfac1moy = 1.
      p%exofac1moy = 1.
      p%etr_etm1moy = 1.
      p%etm_etr1moy = 1.
      p%inn2moy = 1.
      p%turfac2moy = 1.
      p%swfac2moy = 1.
      p%exofac2moy = 1.
      p%etr_etm2moy = 1.
      p%etm_etr2moy = 1.

! DR 19/11/2021 Ajout des varaibles sorties de correspondance et mise dans sortie
      p%CsurNrac = 0.
      p%CsurNracmort = 0.
      p%HI_C = 0.
      p%HI_N = 0.




return
end subroutine initcycle
end module initcycle_m
 
