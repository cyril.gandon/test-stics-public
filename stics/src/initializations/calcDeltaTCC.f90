module calcDeltaTCC_m
USE Stics
use Climat
use climate_utils
implicit none
contains
pure function calcDeltaTCC(sc,t,c) result(deltaT_adaptCC)


    type(Stics_Communs_), intent(in) :: sc  
    type(Stics_Transit_), intent(in) :: t
    type(Climat_), intent(in) :: c

    real :: deltaT_adaptCC(200)  

    integer :: jj  !  
    integer :: kk  !  
    integer :: deb  !  
    integer :: fin  !  
    integer :: num_an  
    real    :: tot_glisse  
    real :: Tm_histo
    real :: tm_glisse

    if (t%P_code_adapt_MO_CC == 1 .and. sc%nbans > (t%P_an_fin_serie_histo - t%P_an_debut_serie_histo)) then
        Tm_histo = get_mean_temp_for_years(t%P_an_debut_serie_histo,t%P_an_fin_serie_histo,c%tmoy_an)
    else
        Tm_histo = t%P_param_tmoy_histo
    endif
    deltaT_adaptCC(:) = 0.

    do jj = 1,sc%nbans
      deb = t%P_an_fin_serie_histo - t%P_an_debut_serie_histo + 1 + jj
      fin = deb + t%P_periode_adapt_CC - 1
      tot_glisse = 0.0
      do kk = deb,fin
        num_an = kk
        tot_glisse = tot_glisse + c%tmoy_an(2,num_an)
      enddo
      tm_glisse = tot_glisse / (fin-deb+1)
      deltaT_adaptCC(num_an+1) = tm_glisse - Tm_histo
    enddo
end function
end module
 
