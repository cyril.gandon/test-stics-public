!*********************************************************************
!  SUBROUTINE d'initialisation des infos SOL, CULTURE
!     culture : remise a 0 des stades calcules
!               reaffectation des sommes entre stades
!               remise a zero des variables plantes
!         sol :  Cas no1:  P_codeinitprec = 2   (enchainement = lecture dans recup.tmp)
!                Cas no2:  P_codeinitprec = 1   (reinitialisation)


!*********************************************************************

module initnonsol_m
use phenology_utils
use stics_system
use stics_files
USE Stics
USE Parametres_Generaux
USE Plante
USE Root
USE Itineraire_Technique
USE Sol
USE Climat
USE Station
use messages
USE dates_utils, only: is_leap_year
USE Module_AgMIP
USE Snow
USE AJUPRO_m, only: AJUPRO
USE Lecture_DonneesCyclePrecedent_m, only: Lecture_DonneesCyclePrecedent
USE init_prairie_m, only: init_prairie
use plant_utils
implicit none
private
public :: initnonsol
contains
subroutine initnonsol(logger,sc,pg,p,r,itk,soil,sta,t,ag,snow_struct)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(INOUT) :: pg
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)
  type(Sol_),                 intent(INOUT) :: soil  
  type(Station_),             intent(INOUT) :: sta  
  type(Stics_Transit_),       intent(INOUT) :: t  

  type(snow_),                 intent(INOUT)  :: snow_struct

  type(AgMIP_), intent(IN) :: ag


! Variable(s) locale(s)
      logical :: drpavantlax   
      real    :: QEXC  
      integer :: izmax  !  
      integer :: i  !  
      integer :: iz  !  
      integer :: ipl  
      integer :: k  
      real    :: HEXC  !  
      real    :: Qeauinit(5)  
      real    :: CCrac

!      integer :: AOAS  !
!      integer ::  AS  !
!      integer ::  AO
      logical :: AgMIP,Macsur

      real    :: dracmoy
      integer :: nhe
      real    :: propracf

      real :: Corg, Norg, tCorg, tNorg, Msol, QMsol
      integer :: ihum

      real :: HR_dom(5),azo_dom(5),amm_dom(5)
      !real :: resmes_ini,azomes_ini
  
    ! adding local variables for Snow module
!    real :: Sdry ! water in solid state in the snow cover (mm)
!    real :: Swet ! water in liquid state in the snow cover (mm)
!    real :: ps ! density of snow cover (kg m-3) 
!     real  :: Sdepth ! snow cover depth (m)


  logical :: status
  integer :: file_closed
  logical :: file_open
  
! 16/08/2019 ajout DR pour retomber dans 9.1

! DR 13/06/2017 nouvelles variables BM
!      real :: QCorg
! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s, donc plus besoin de la varaibles finert
!      real :: finert
!      real    :: Ch  !
!      real    :: QNorg  !
!      real    :: Nactif !
!      real    :: Cactif  !
 ! Ajout Loic Janvier 2021
      real :: CsurNfeuille
      real :: CsurNtige

! DR 29/08/2019 pour debug
   logical :: dbg_initnonsol
! DR 29/08/2019 en attendant de regarder de pres les diff de stock de BM je mets un flag
! DR 09/06/2020 je
  !  logical :: code_stock_BM

   dbg_initnonsol = .FALSE.
   ! code_stock_BM=.FALSE.
   ! DR 26/08/2019 je dois commiter pour Loic en attendant de faire mieux je
   !   if (p(1)%P_codeplante == CODE_VINE.or. p(2)%P_codeplante == CODE_VINE) code_stock_BM =.FALSE.

  status = .TRUE.

  AgMIP= .FALSE.
  Macsur= .FALSE.
  if(ag%P_type_project.eq.2) AgMIP= .TRUE.
  if(ag%P_type_project.eq.3) Macsur= .TRUE.


!      AOAS = sc%AOAS
!      AO = sc%AO
!      AS = sc%AS

! Ajout Loic et Bruno fevrier 2014
!                       initialisations des variables "reserves perennes" lorsqu'on n'enchaine pas
!                       si on enchaine, ces variables sont ecrasees dans la routine Lecture_DonneesCyclePrecedent
! Loic Aout 2016 : ajout des racines et boucle sur ipl
   do ipl = 1,sc%P_nbplantes
      p(ipl)%maperenne(0) = p(ipl)%P_maperenne0
      p(ipl)%QNperenne(0) = p(ipl)%P_QNperenne0
!      p(ipl)%msrac(:) = t%P_msrac0
!      p(ipl)%QNrac = t%P_QNrac0
      p(ipl)%resperenne(0) = p(ipl)%P_PropresP * p(ipl)%maperenne(0)
      p(ipl)%QNresperenne(0) = p(ipl)%P_PropresPN * p(ipl)%QNperenne(0)
      p(ipl)%resperennestruc = p(ipl)%P_maperenne0 - p(ipl)%resperenne(0)
      p(ipl)%QNresperennestruc = p(ipl)%P_QNperenne0 - p(ipl)%QNresperenne(0)

! calcul des diametres de racines et de leurs longueurs specifiques
    if (p(ipl)%P_code_diff_root == 1) then
      if(p(ipl)%P_rapdia < 1.) p(ipl)%P_rapdia = 1.
      dracmoy = 2./sqrt(3.1416 * p(ipl)%P_RTD * p(ipl)%P_longsperac)
!        cm                         g/cm3          cm/g
      p(ipl)%dracf = dracmoy / ((1.-p(ipl)%P_propracfmax)*p(ipl)%P_rapdia+p(ipl)%P_propracfmax)
      p(ipl)%dracg = p(ipl)%P_rapdia * p(ipl)%dracf
      p(ipl)%longsperacf = 4./(3.1416 * (p(ipl)%dracf**2) * p(ipl)%P_RTD)
!              cm/g                               cm2         g/cm3
      p(ipl)%longsperacg = 4./(3.1416 * (p(ipl)%dracg**2) * p(ipl)%P_RTD)
! duree de vie des racines en degres C.jours
      p(ipl)%debsenracf = p(ipl)%P_debsenrac / sqrt(p(ipl)%P_rapdia)
      p(ipl)%debsenracg = p(ipl)%P_debsenrac * sqrt(p(ipl)%P_rapdia)
    else
      p(ipl)%debsenracf = p(ipl)%P_debsenrac
      p(ipl)%debsenracg = p(ipl)%P_debsenrac
      p(ipl)%longsperacf = p(ipl)%P_longsperac
      p(ipl)%longsperacg = p(ipl)%P_longsperac
      p(ipl)%P_propracfmax = 0.
    endif
! Fin des modifs
! Ajout Loic Juin 2017 : initialisation de msresgel et QNresgel
! Loic sept 2020 : j'utilise le code acti reserve
! Loic janv 2021 ces lignes de code sont inutiles
!    if (p(ipl)%P_codeplante == CODE_FODDER) then
!    if (p(ipl)%P_codeplante == CODE_FODDER.and. p(ipl)%P_code_acti_reserve == 1) then
!      p(ipl)%msresgel0 = p(ipl)%P_masecnp0
!      p(ipl)%QNresgel0 = p(ipl)%P_QNplantenp0
!      p(ipl)%matigestruc0 = p(ipl)%P_masecnp0
!      p(ipl)%QNtige0 = p(ipl)%P_QNplantenp0
!    endif

! Modifs Loic et Bruno fevrier 2014
! Calcul du profil racinaire initial par couche elementaire et par classe de racines
! Ce profil sera utilise dans croira lorsque (P_coderacine == 2 .and. codeinstal == 1 .and. n == nger)
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
        nhe = 0
        do i = 1, sc%nh
          izmax = soil%P_epc(i)
          do iz = 1, izmax
!             if(p(ipl)%P_zrac0 <= p(ipl)%P_zramif) then
!                propracf = 0.
!             else
!                propracf = amax1(0., p(ipl)%P_propracfmax * (1.-iz/(p(ipl)%P_zrac0-p(ipl)%P_zramif)))
!             endif
             propracf = p(ipl)%P_propracfmax
             p(ipl)%rlf0(nhe+iz) = p(ipl)%P_densinitial(i) * propracf
             if ((nhe+iz) < itk(ipl)%P_profsem)   p(ipl)%rlf0(nhe+iz) = 0.
             if ((nhe+iz) > soil%P_obstarac)      p(ipl)%rlf0(nhe+iz) = 0.
             p(ipl)%rlg0(nhe+iz) = p(ipl)%P_densinitial(i) * (1.-propracf)
             if ((nhe+iz) < itk(ipl)%P_profsem)   p(ipl)%rlg0(nhe+iz) = 0.
             if ((nhe+iz) > soil%P_obstarac)      p(ipl)%rlg0(nhe+iz) = 0.
          end do
          nhe = nhe+izmax
        end do
        p(ipl)%ndebsenracf = 0   ! lifespan of fine roots
        p(ipl)%ndebsenracg = 0   ! lifespan of coarse roots
        p(ipl)%nsencourpreracf = 0  ! day when fine root senescence starts
        p(ipl)%nsencourpreracg = 0  ! day when coarse root senescence starts
        p(ipl)%ndecalf = 0          ! number of days remaining in the previous simulation without mortality of fine roots
        p(ipl)%ndecalg = 0          ! number of days remaining in the previous simulation without mortality of coarse roots
        p(ipl)%somtemprac = 0.
        p(ipl)%somtempracvie = 0.
        p(ipl)%dtj(:) = 0.
     end do
! Fin des modifs

call EnvoyerMsgEcran(logger, 'Lecture du fichier recup')
! DR 18/06/08 on integre les modifs enchainement (C)ultures (AS)sociees
! DR 030608 on essai de mettre la lecture de recup avant les initialisations
      if (pg%P_codeinitprec == 2 .and. sc%P_codesuite /= 0) then

      call EnvoyerMsgEcran(logger, 'avant lecture cycle precedent')
      call Lecture_DonneesCyclePrecedent(logger,retrieval_file,sc,pg,p,r,itk,soil)


       ! 09/07/2018 initialisation de la temperature du sol a la temperature de la veille sinon pb de de difference de nitrification entre une usm en rotation (usm solnu+usm mais)
        ! et une usm seule (solnu+mais)
        ! 03/01/2019 la diff entre la version merge et celle non merg�e pour l'usm 2 venait de ca qui n'avait pas ete pris!
        !!! 03/01/2019 a laisser c'etait une correction de bug !!!
! 07/01/2019 DR j'active la correction de bug fu 09/07/2018 du trunk pour comparaison version perennes avant et apres merge
        sc%tsol(1:)=sc%tsolveille(1:)
        !!!!!!!! If snow module is activated and running successive case!!!!!!!!!!!!!
        if (pg%P_codesnow==1) then     
        !print *, "before calling readSnowSta...."   
           call readSnowStateVariables(snow_struct%in%Sdepth,snow_struct%in%Sdry, &
           snow_struct%in%Swet,snow_struct%in%ps)
         endif
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      endif

! domi - 04/02/04
! On rajoute des tests pour la compatibilite entre initialisations et stade de demarrage
      do ipl = 1,sc%P_nbplantes
        if (p(ipl)%P_stade0 == snu) then
          !if (p(ipl)%P_lai0 /= 0 .or. p(ipl)%P_masecnp0 /= 0 .or. p(ipl)%P_zrac0 /= 0 .or.  &
           !   p(ipl)%P_magrain0 /= 0 .or. p(ipl)%P_QNplantenp0 /= 0) then
          if (abs(p(ipl)%P_lai0).gt.1.0E-8 .or. abs(p(ipl)%P_masecnp0).gt.1.0E-8 .or. &
              abs(p(ipl)%P_zrac0).gt.1.0E-8 .or. abs(p(ipl)%P_magrain0).gt.1.0E-8 .or. &
              abs(p(ipl)%P_QNplantenp0).gt.1.0E-8) then
            call EnvoyerMsgHistorique(logger, '(//)')
            call EnvoyerMsgHistorique(logger, MESSAGE_341)
            p(ipl)%P_lai0 = 0.
            p(ipl)%P_masecnp0 = 0.
            p(ipl)%P_zrac0 = 0.
            p(ipl)%P_magrain0 = 0.
            p(ipl)%P_QNplantenp0 = 0.
          endif
        endif

        if (p(ipl)%P_stade0 == plt) then
        call EnvoyerMsgHistorique(logger, '(//)')
        !if (p(ipl)%P_lai0 /= 0 .or. p(ipl)%P_masecnp0 /= 0 .or. p(ipl)%P_zrac0 /= 0 .or. &
        !       p(ipl)%P_magrain0 /= 0 .or. p(ipl)%P_QNplantenp0 /= 0) call EnvoyerMsgHistorique(logger, MESSAGE_342)
          if (abs(p(ipl)%P_lai0).gt.1.0E-8 .or. abs(p(ipl)%P_masecnp0).gt.1.0E-8 .or. &
              abs(p(ipl)%P_zrac0).gt.1.0E-8 .or. abs(p(ipl)%P_magrain0).gt.1.0E-8 .or. &
              abs(p(ipl)%P_QNplantenp0).gt.1.0E-8) then
            call EnvoyerMsgHistorique(logger, MESSAGE_342)
          endif
        endif
      end do

! initialisations pour FERTIL
      soil%QNdenit = 0.
      soil%QNvolorg = 0.
      soil%QNvoleng = 0.
      soil%QNorgeng = 0.
      sc%totapN = 0.
      sc%pluieN = 0.
      sc%precipN = 0.
      sc%irrigN = 0.
      sc%totapNres = 0.

! -------------------------------------------------------------
!   LES TECHNIQUES CULTURALES
! -------------------------------------------------------------
      do ipl = 1, sc%P_nbplantes
         p(ipl)%Qfix(:) = 0.
         p(ipl)%Qfixtot(:) = 0.
         p(ipl)%Ndfa = 0.

        t%nbapN(ipl) = 0
        t%nbapirr(ipl) = 0
        t%dateN(ipl,:) = 0
        t%dateirr(ipl,:) = 0

        do i = 1, itk(ipl)%P_nbjtrav
          if (itk(ipl)%P_proftrav(i) > soil%profsol) then
            call exit_error(logger, MESSAGE_82)
          endif
        end do

    ! evaporation sol
        p(ipl)%parapluie = 1
      end do

      sc%cintermulch = 0.
      sc%nouvre2 = itk(1)%P_julouvre2 - sc%P_iwater + 1
      sc%nouvre3 = itk(1)%P_julouvre3 - sc%P_iwater + 1

      do ipl = 1, sc%P_nbplantes
        if (itk(ipl)%P_codcueille == 1) itk(ipl)%P_nbcueille = 1
        if (itk(ipl)%P_nbcueille == 2 .and. p(ipl)%P_codeindetermin == 1) then
          call exit_error(logger, MESSAGE_333)
        endif
        p(ipl)%nbrecolte = 1

        do k=1,itk(ipl)%P_nb_eclair
            p(ipl)%neclair(k) = itk(ipl)%P_juleclair(k) - sc%P_iwater + 1
        enddo
        if (itk(ipl)%P_codeclaircie == 2 .and. p(ipl)%P_codeindetermin == 1) then
          call exit_error(logger, MESSAGE_334)
        endif

        p(ipl)%biorognecum(:) = 0.
        p(ipl)%lairognecum(:) = 0.
        p(ipl)%laieffcum(:) = 0.
        p(ipl)%bioeffcum(:) = 0.
        p(ipl)%nrogne = itk(ipl)%P_julrogne   - sc%P_iwater + 1
        p(ipl)%neffeuil = itk(ipl)%P_juleffeuil - sc%P_iwater + 1
        p(ipl)%ntaille = itk(ipl)%P_jultaille  - sc%P_iwater + 1
      end do

! ----------------------------------------------------------
! LES DATES
! ----------------------------------------------------------
     do ipl = 1, sc%P_nbplantes
        p(ipl)%nplt = p(ipl)%iplt - sc%P_iwater + 1
        p(ipl)%nger = 0
        p(ipl)%nlev = 0
        p(ipl)%namf = 0
        p(ipl)%nlax = 0
        p(ipl)%ndrp = 0
        p(ipl)%nsen = 0
        p(ipl)%nlan = 0
        p(ipl)%nmat = 0
        p(ipl)%nrec = 0
        p(ipl)%naer(:) = 0
        p(ipl)%nnou = 0
        p(ipl)%nflo = 0
        p(ipl)%nstopfeuille = 0
        p(ipl)%nstopres = 0
        p(ipl)%ndebdes = 0
        p(ipl)%nrecalpfmax = 0
 ! Initialisation de la date de destruction
 !       if (itk(ipl)%P_juldes /= 999) then
        if (itk(ipl)%P_codejourdes == 1) then
           p(ipl)%ndes = itk(ipl)%P_juldes - sc%P_iwater + 1
        else
           p(ipl)%ndes = 999
        endif
      ! cas des vernalisantes (herbacees)
        if (p(ipl)%P_codeperenne == 2) then
          if (p(ipl)%P_codebfroid == 1) p(ipl)%P_ifindorm = 1
          if (p(ipl)%P_codebfroid == 2 .and. sc%P_culturean == 1) p(ipl)%P_ifindorm = 1
        endif

      ! cas de dormance Richardson
        if (p(ipl)%P_codebfroid == 3 .and. p(ipl)%P_codedormance == 2) then
          p(ipl)%ndebdorm = 0
          p(ipl)%nfindorm = 0
          p(ipl)%P_idebdorm = 0
          p(ipl)%P_ifindorm = 0
        endif

      ! cas dormance Bidabe
        if (p(ipl)%P_codebfroid == 3 .and. p(ipl)%P_codedormance == 3) then
          p(ipl)%ndebdorm = p(ipl)%P_idebdorm - sc%P_iwater + 1
          p(ipl)%nfindorm = 0
          p(ipl)%P_ifindorm = 0
        endif

      ! cas forcage dormance
        if (p(ipl)%P_codebfroid == 3 .and. p(ipl)%P_codedormance == 1) then
          if (p(ipl)%P_ifindorm < sc%P_iwater) then
            call exit_error(logger, MESSAGE_335)
          endif
          p(ipl)%nfindorm = p(ipl)%P_ifindorm - sc%P_iwater + 1
          p(ipl)%nplt = p(ipl)%nfindorm
          p(ipl)%iplt = p(ipl)%P_ifindorm
          p(ipl)%P_idebdorm = 0
        endif
        p(ipl)%nrecbutoir = itk(ipl)%P_irecbutoir - sc%P_iwater + 1
        p(ipl)%nlevobs = 999
        p(ipl)%namfobs = 999
        p(ipl)%nlaxobs = 999
        p(ipl)%ndrpobs = 999
        p(ipl)%nsenobs = 999
        p(ipl)%nlanobs = 999
        p(ipl)%nmatobs = 999
        p(ipl)%nrecobs = 999
        p(ipl)%nfloobs = 999
        p(ipl)%ndebdesobs = 999


        if (sc%numcult == 1 .and. p(ipl)%P_codetemp == 2) then
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = p(ipl)%P_stlevamf(itk(ipl)%P_variete) * p(ipl)%P_coeflevamf
          p(ipl)%P_stamflax(itk(ipl)%P_variete) = p(ipl)%P_stamflax(itk(ipl)%P_variete) * p(ipl)%P_coefamflax
          p(ipl)%P_stlaxsen(itk(ipl)%P_variete) = p(ipl)%P_stlaxsen(itk(ipl)%P_variete) * p(ipl)%P_coeflaxsen
          p(ipl)%P_stsenlan(itk(ipl)%P_variete) = p(ipl)%P_stsenlan(itk(ipl)%P_variete) * p(ipl)%P_coefsenlan
          p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = p(ipl)%P_stlevdrp(itk(ipl)%P_variete) * p(ipl)%P_coeflevdrp
          p(ipl)%P_stdrpmat(itk(ipl)%P_variete) = p(ipl)%P_stdrpmat(itk(ipl)%P_variete) * p(ipl)%P_coefdrpmat
          p(ipl)%P_dureefruit(itk(ipl)%P_variete) = p(ipl)%P_dureefruit(itk(ipl)%P_variete) * p(ipl)%P_coefdrpmat
          p(ipl)%P_stflodrp(itk(ipl)%P_variete) = p(ipl)%P_stflodrp(itk(ipl)%P_variete) * p(ipl)%P_coefflodrp
          p(ipl)%P_stdordebour = p(ipl)%P_stdordebour * p(ipl)%P_coeflevamf
          p(ipl)%P_stdrpdes(itk(ipl)%P_variete) = p(ipl)%P_stdrpdes(itk(ipl)%P_variete) * p(ipl)%P_coefdrpmat
        endif

      ! affectation des dates de stades observes
      ! PB - 15/03/2005 - en enchainement de perennes on ne force pas les stades
        if (pg%P_codeinitprec == 2 .and. p(ipl)%P_codeperenne == 2) then
            itk(ipl)%P_codestade = 0
            call EnvoyerMsgHistorique(logger, '')
            call EnvoyerMsgHistorique(logger, MESSAGE_443)
            call EnvoyerMsgHistorique(logger, '')
        endif


        if (itk(ipl)%P_codestade == 1) then
          if (itk(ipl)%P_ilev /= 999) p(ipl)%nlevobs = itk(ipl)%P_ilev - sc%P_iwater + 1
          if (itk(ipl)%P_iamf /= 999) p(ipl)%namfobs = itk(ipl)%P_iamf - sc%P_iwater + 1
          if (itk(ipl)%P_ilax /= 999) p(ipl)%nlaxobs = itk(ipl)%P_ilax - sc%P_iwater + 1
          if (itk(ipl)%P_isen /= 999) p(ipl)%nsenobs = itk(ipl)%P_isen - sc%P_iwater + 1
          if (itk(ipl)%P_ilan /= 999) p(ipl)%nlanobs = itk(ipl)%P_ilan - sc%P_iwater + 1
          if (itk(ipl)%P_idrp /= 999) p(ipl)%ndrpobs = itk(ipl)%P_idrp - sc%P_iwater + 1
          if (itk(ipl)%P_imat /= 999) p(ipl)%nmatobs = itk(ipl)%P_imat - sc%P_iwater + 1
          if (itk(ipl)%P_irec /= 999) p(ipl)%nrecobs = itk(ipl)%P_irec - sc%P_iwater + 1
          if (itk(ipl)%P_iflo /= 999) p(ipl)%nfloobs = itk(ipl)%P_iflo - sc%P_iwater + 1

        ! DR et ML le 29/01/08 (lendemain de l'anniversaire de notre president cheri)
        ! sachant qu'on a des problemes quand on force DRP trop precocement sans forcer FLO
        ! c'est-a-dire avant que stlevflo soit accompli, alors on force FLO automatiquement
          if (itk(ipl)%P_idrp /= 999 .and. itk(ipl)%P_iflo == 999) p(ipl)%nfloobs = p(ipl)%ndrpobs
        endif
     end do

    ! initialisation climatique
      sc%posibsw = .true.
      sc%posibpe = .true.
      sc%Emulch = 0.
      sc%mouillmulch = 0.

! ---------------------------------------------------------
! LA PLANTE
! ---------------------------------------------------------

      sc%albedolai     = 0.

      do ipl = 1, sc%P_nbplantes


! 25/04/2012 DR et IGC if faut tester si on a active l'eclaicissage
        if (sc%numcult > 1 .and. itk(ipl)%P_codeclaircie == 2) p(ipl)%P_nbinflo = p(ipl)%P_nbinflo(itk(ipl)%P_variete) &
                                                                                  + itk(ipl)%P_nbinfloecl(1)

        p(ipl)%durvieI = p(ipl)%P_ratiodurvieI * p(ipl)%P_durvieF(itk(ipl)%P_variete)
        p(ipl)%cumdevfr = 0.
        p(ipl)%jdepuisrec = 0
        p(ipl)%nbj0remp = 0
        p(ipl)%nbfruit = 0.
        p(ipl)%compretarddrp = 0
        p(ipl)%QNplanteres = 0.
        p(ipl)%masectot = 0.
! DR 170106 prairie
        p(ipl)%QNplanteCtot = 0.
        p(ipl)%rendementsec = 0.
! MS et N exportes pour les cultures fauchees
        p(ipl)%mafauche = 0.
        p(ipl)%mafauchetot = 0.
        p(ipl)%QNfauche = 0.
        p(ipl)%QNfauchetot = 0.
        p(ipl)%QNexport = 0.

        p(ipl)%dltaisen(:) = 0.
        p(ipl)%dltaisenat(:) = 0.
        p(ipl)%dltamsen(:) = 0.
        p(ipl)%sla(:) = p(ipl)%P_slamax(itk(ipl)%P_variete)
        p(ipl)%deltares(:) = 0.
        p(ipl)%masecveg(:) = 0.
        p(ipl)%mafeuil(:) = 0.

        p(ipl)%maenfruit(:) = 0.
        p(ipl)%nbgrains(:) = 0.
! DR 14/04/2016
        p(ipl)%msneojaune(:) = 0.
        p(ipl)%somsenreste(:) = 0.
        p(ipl)%hauteur(:) = 0.
        ! 12/02/2015 DR et VG for the vine we start the "hauteur" to hautbase
        if(p(ipl)%P_codeplante==CODE_VINE)then
              p(ipl)%hauteur    =  p(ipl)%P_hautbase(itk(ipl)%P_variete)
        endif
        p(ipl)%pgraingel(:)   = 0.
        p(ipl)%fpft(:)        = 0.
        p(ipl)%mafeuiltombe(:)= 0.
        p(ipl)%teaugrain(:) = p(ipl)%P_h2ofrvert
        p(ipl)%cu(:) = 0.
        p(ipl)%pfeuil(:,:) = 0.

        p(ipl)%durvie(:,:) = 0.
        p(ipl)%irazo(:,:) = 0.
        p(ipl)%nfruit(:,:) = 0
        p(ipl)%nfruitv(:,:) = 0
        p(ipl)%pdsfruit(:,:) = 0.
    ! Ajout Loic Aout 2016: on initialise mafou = ancien mafruit pour les fourages
        p(ipl)%mafou = 0.
    ! Ajout Loic Septembre 2016
        p(ipl)%QNfeuille = 0.
        p(ipl)%QNtige = 0.
        p(ipl)%msresgel(AOAS) = 0.
        p(ipl)%QNresgel(AOAS) = 0.

        p(ipl)%masecneo(:) = 0.
        p(ipl)%masecneov(:) = 0.
        p(ipl)%somfeuille = 0.

        if (sc%numcult == 1) then
           if (p(ipl)%P_codeindetermin == 1) then
              p(ipl)%stdrpmatini = p(ipl)%P_stdrpmat(itk(ipl)%P_variete)
           else
             p(ipl)%P_stdrpmat(itk(ipl)%P_variete) = p(ipl)%P_dureefruit(itk(ipl)%P_variete)
             p(ipl)%stdrpmatini = p(ipl)%P_dureefruit(itk(ipl)%P_variete)
           endif
        else
           p(ipl)%P_stdrpmat(itk(ipl)%P_variete) = p(ipl)%stdrpmatini
        endif
        p(ipl)%dltags(:) = 0.
        if (p(ipl)%P_codedyntalle == 1) p(ipl)%densitemax = p(ipl)%P_MaxTalle
        p(ipl)%mortreserve = 0.

      ! A VOIR : copie depuis trunk !
      ! DR et ML et SYL 15/06/09
      ! ************************
      ! introduction de la fin des modifications de Sylvain (nadine et FR)
      ! dans le cadre du projet PERMED
      ! DR et ML et SYL 16/06/09
      ! on ajoute la condition sur P_codedyntalle
      ! ####
      ! SYL 06/09/07 initialisation de densitemax
        if (p(ipl)%P_codedyntalle == 1) then
          p(ipl)%densitemax = p(ipl)%P_MaxTalle
        endif
      ! ####
      ! DR et ML et SYL 15/06/09 FIN introduction de la fin des modifications de Sylvain
       !!!!!!!!!!!!!!


      end do



      soil%itrav1   = 0
      soil%itrav2   = 0

    ! code pour action de la vernalisation dans le calcul de ulai
      pg%codeulaivernal = 1

     do ipl = 1, sc%P_nbplantes
        p(ipl)%cinterpluie = 0.

      ! recalcul des parcours (temporaire)
        p(ipl)%stlevflo = p(ipl)%P_stlevdrp(itk(ipl)%P_variete) - p(ipl)%P_stflodrp(itk(ipl)%P_variete)
        if (sc%numcult > 1) then
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = p(ipl)%stlevamf0
          p(ipl)%P_stamflax(itk(ipl)%P_variete) = p(ipl)%stamflax0
          p(ipl)%P_stlaxsen(itk(ipl)%P_variete) = p(ipl)%stlaxsen0
          p(ipl)%P_stsenlan(itk(ipl)%P_variete) = p(ipl)%stsenlan0
          p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = p(ipl)%stlevdrp0
          p(ipl)%P_stflodrp(itk(ipl)%P_variete) = p(ipl)%stflodrp0
          p(ipl)%P_stdrpdes(itk(ipl)%P_variete) = p(ipl)%stdrpdes0
        ! DR et ML 05/05/08 on le reinitialise aussi sinon pb calcul de la date
        ! de floraison apres mort de la plante
          p(ipl)%stlevflo = p(ipl)%stlevdrp0 - p(ipl)%stflodrp0
        endif

    ! affectation des sommes
        p(ipl)%stlevamf0 = p(ipl)%P_stlevamf(itk(ipl)%P_variete)
        p(ipl)%stamflax0 = p(ipl)%P_stamflax(itk(ipl)%P_variete)
        p(ipl)%stlaxsen0 = p(ipl)%P_stlaxsen(itk(ipl)%P_variete)
        p(ipl)%stsenlan0 = p(ipl)%P_stsenlan(itk(ipl)%P_variete)
        p(ipl)%stlevdrp0 = p(ipl)%P_stlevdrp(itk(ipl)%P_variete)
        p(ipl)%stflodrp0 = p(ipl)%P_stflodrp(itk(ipl)%P_variete)
        p(ipl)%stdrpdes0 = p(ipl)%P_stdrpdes(itk(ipl)%P_variete)
        if (sc%numcult > 1) then
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = p(ipl)%stlevamf0
          p(ipl)%P_stamflax(itk(ipl)%P_variete) = p(ipl)%stamflax0
          p(ipl)%P_stlaxsen(itk(ipl)%P_variete) = p(ipl)%stlaxsen0
          p(ipl)%P_stsenlan(itk(ipl)%P_variete) = p(ipl)%stsenlan0
          p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = p(ipl)%stlevdrp0
          p(ipl)%P_stflodrp(itk(ipl)%P_variete) = p(ipl)%stflodrp0
          p(ipl)%P_stdrpdes(itk(ipl)%P_variete) = p(ipl)%stdrpdes0
        endif

        p(ipl)%cumrg = 0.
        p(ipl)%cumraint = 0.

        p(ipl)%somcourdrp = 0.
        p(ipl)%somcourno = 0.

        p(ipl)%surf(AS) = 1.
        p(ipl)%surf(AO) = 0.

        p(ipl)%mafeuilverte(:) = 0.
        p(ipl)%pfeuiljaune(:) = 0.
        p(ipl)%ptigestruc(:) = 0.
        p(ipl)%penfruit(:) = 0.
        p(ipl)%preserve(:) = 0.
        p(ipl)%mafeuiljaune(:) = 0.
        p(ipl)%matigestruc(:) = 0.
        p(ipl)%mareserve(:) = 0.
        p(ipl)%pfeuilverte(:,0) = 0.
        p(ipl)%mafeuilp(:) = 0.
        p(ipl)%matigestrucp(:) = 0.

        p(ipl)%tursla(:) = 1.
        if (sc%numcult == 1) then
           itk(ipl)%P_orientrang = itk(ipl)%P_orientrang / 180. * 3.1416
           p(ipl)%P_envfruit = p(ipl)%P_envfruit * p(ipl)%P_pgrainmaxi(itk(ipl)%P_variete) * 1.e-2
        endif

        p(ipl)%coeflev = 1.

        p(ipl)%densite = itk(ipl)%P_densitesem
        p(ipl)%densitelev = p(ipl)%densite
        p(ipl)%densiteger = p(ipl)%densite
        p(ipl)%densiteequiv = p(ipl)%densite

        p(ipl)%pfmax = p(ipl)%P_pgrainmaxi(itk(ipl)%P_variete)
        p(ipl)%mouill(:) = 0.
        p(ipl)%sourcepuits(:) = 1.
        p(ipl)%splai(:) = 1.

      ! test sur les parametres sourcepuits
        ! if (p(ipl)%P_splaimin == p(ipl)%P_splaimax .or. p(ipl)%P_spfrmin == p(ipl)%P_spfrmax) then
        if (P(ipl)%P_codeindetermin.eq.2.and.(abs(p(ipl)%P_splaimin-p(ipl)%P_splaimax).lt.1.0E-8 .or. &
            abs(p(ipl)%P_spfrmin-p(ipl)%P_spfrmax).lt.1.0E-8)) then
          call exit_error(logger, MESSAGE_337)
        endif

    ! offrnodu
        p(ipl)%ndno = 0
        p(ipl)%nfno = 0
        p(ipl)%nfvino = 0
        p(ipl)%fixpotfno = 0.
      end do

      sc%anit(:) = 0.
      sc%anit_uree(:) = 0.
      ! DR 19/12/2016 on initialise anit_engrais
      sc%anit_engrais(:) = 0.
      sc%airg(:) = 0.
      sc%tauxcouv(:) = 0.

      if (sc%numcult == 1) then
        pg%P_coefb = pg%P_coefb / 100. ! TODO : on modifie un parametre general qui normalement ne doit pas etre modifie, a dupliquer !
        ! Modif Loic Mars 2019 : je reactive le changement d'unite
        ! 19/06/2019 apres discussion avec Loic on met ca en coherence avec la 9.1 et on ne modifie pas un parametre (le chgt d'unite est fait dans transf)
        ! soil%P_humcapil = soil%P_humcapil / 10.
      endif

    ! initialisations stress
      if (sc%P_nbplantes > 1 .and. ipl > 1) then
        ! pour CAS, et on suppose que l'on passe en ipl = 2 APRES ipl = 1
        sc%delta = max(p(2)%P_extin(itk(2)%P_variete) - 0.2, 0.)
      else
        ! en culture pure
        sc%delta = max(p(1)%P_extin(itk(1)%P_variete) - 0.2, 0.)
      endif
! DR 22/06/2018 si on est en rotation on ne veut pas le remettre a zero
!      sc%xmlch1 = 0.
      sc%xmlch2 = 0.
! DR 22/06/2018 si on est en rotation on ne veut pas le remettre a zero
!      sc%supres = 0.
      sc%stoc = 0.
      sc%nstoc = 0

    ! variables cumulees
      sc%cestout         = 0.
  ! DR 16/09/2016 j'ajoute 2 varaibles
      sc%cEdirect = 0.
      sc%cEdirecttout = 0.

      sc%cpreciptout     = 0.
      sc%cpluie          = 0.
      sc%toteaunoneffic  = 0.
      sc%totir           = 0.
! DR 26/01/2016 ces varaibles avaient ete cree pour macsur , elles etaient reiunitialisees implicitement par les usms en rotation
! pour Agmip en annees enchainees c'est plus bon
      sc%drain_from_plt = 0.
      sc%leaching_from_plt = 0.
      sc%runoff_from_plt = 0.
      sc%Nmineral_from_plt = 0.
      sc%Nvolat_from_plt = 0.
      sc%QNdenit_from_plt = 0.

!DR 13/03/2018 pour wheat4
      sc%cet_from_plt = 0.

! DR 02/03/2017 pour Macsur_vigne cumul a partir du debourrement des variables autres que plantes
      sc%drain_from_lev = 0.
      sc%leaching_from_lev = 0.
      sc%runoff_from_lev = 0.
      sc%Nmineral_from_lev = 0.
      sc%Nvolat_from_lev = 0.
      sc%QNdenit_from_lev = 0.
      sc%cet_from_lev = 0.
      sc%cum_et0_from_lev= 0.
      sc%etz(:) = 0.
      sc%anox(:) = 0.

      !PL, 7/10/2020, init urac 
      p(:)%urac = 1.

      do ipl = 1, sc%P_nbplantes
        p(ipl)%swfac(:) = 1.
        p(ipl)%turfac(:) = 1.
        p(ipl)%senfac(:) = 1.
        p(ipl)%totpl = 0.
        p(ipl)%nst1 = 0
        p(ipl)%nst2 = 0
        p(ipl)%str1 = 0
        p(ipl)%str2 = 0
        p(ipl)%stu1 = 0
        p(ipl)%stu2 = 0
        p(ipl)%inn1 = 0.
        p(ipl)%inn2 = 0.
        p(ipl)%exofac1 = 0.
        p(ipl)%exofac2 = 0.
        p(ipl)%diftemp1 = 0.
        p(ipl)%diftemp2 = 0.

        p(ipl)%etr_etm1 = 0.
        p(ipl)%etm_etr1 = 0.
        p(ipl)%etr_etm2 = 0.
        p(ipl)%etm_etr2 = 0.

        p(ipl)%gel1 = 1.
        p(ipl)%gel2 = 1.
        p(ipl)%gel3 = 1.
        p(ipl)%mortplante = 0
        p(ipl)%mortplanteN = 0

        p(ipl)%fstressgel = 1.
        p(ipl)%fgelflo = 1.
        p(ipl)%fgellev = 1.
        p(ipl)%gelee = .FALSE.
        p(ipl)%nbjgel = 0
        p(ipl)%znonli = p(ipl)%zrac


      ! variables cumulees
        p(ipl)%cet       = 0.
        p(ipl)%ces       = 0.
        p(ipl)%cep       = 0.
        p(ipl)%cetm      = 0.
        p(ipl)%cprecip   = 0.
        p(ipl)%somger    = 0.
        p(ipl)%cdemande  = 0.
        p(ipl)%crg       = 0.
        p(ipl)%ctmoy     = 0.
        p(ipl)%ctcult    = 0.
   ! DR 29/12/2014 ajout de cumul de tcult max pour faire moyenne pour Giacomo Agmip Canopy temp
        p(ipl)%ctcultmax = 0.
        p(ipl)%cetp   = 0.
        p(ipl)%cum_et0 = 0.
        p(ipl)%qressuite = 0.
        p(ipl)%qressuite_tot = 0.
        p(ipl)%QNressuite_tot = 0.
        p(ipl)%QCressuite_tot = 0.
        p(ipl)%Nexporte = 0.    ! // OUTPUT // total amount of exported N // kg.ha-1
        p(ipl)%Nrecycle = 0.    ! // OUTPUT // total amount of recycled N (unexported N at harvest + N from the fallen leaves) // kg.ha-1
        p(ipl)%MSexporte = 0.    ! // OUTPUT // total amount of exported biomass // t.ha-1
        p(ipl)%MSrecycle = 0.    ! // OUTPUT // total of recycled biomass (unexported DM at harvest + DM from the fallen leaves) // t.ha-1
        p(ipl)%somudevair = 0.
        p(ipl)%somudevcult = 0.
        p(ipl)%somupvtsem = 0.
        p(ipl)%p1000grain = 0.
        p(ipl)%grain_dry_weight_mg = 0.

		

    ! DR 05/04/2006 initialisation pour repousse semis
        sc%repoussesemis(ipl) = .FALSE.
        sc%repousserecolte(ipl) = .TRUE.
        p(ipl)%nbjpourdecisemis = 0
        p(ipl)%nbjpourdecirecolte = 0
    ! DR 05/03/08 on conserve la date de semis qu'on change si on est en decisionsemis
        p(ipl)%iplt = itk(ipl)%P_iplt0

    ! DR 10/06/2013 en cas d'apport de residus avec beaucoup d'eau on a incremente le nombre d'apports , faut le remettre a 0
        itk(ipl)%nap = sc%napini(ipl)
    ! DR 10/06/2013 je fais pareil pour les apports d'azote
        itk(ipl)%napN = sc%napNini(ipl)
    ! DR 17/06/2016 j'essaie  de faire pareil pour nbjres
        itk(ipl)%P_nbjres = sc%nbjresini(ipl)

    ! DR 20/04/06 initialisation des cumuls intercoupe
        p(ipl)%cescoupe = 0.
        p(ipl)%cepcoupe = 0.
        p(ipl)%cetmcoupe = 0.
        p(ipl)%cetcoupe = 0.
        p(ipl)%cprecipcoupe = 0.

    ! pour l'enchainement des annees si on calcule les irrigations on doit
    ! remettre a zero les irrigations de la culture precedente
        if (itk(ipl)%P_codecalirrig == 1)  itk(ipl)%nap = 0
        if (t%P_codecalferti == 1)  itk(ipl)%napN = 0

        p(ipl)%lracz(:) = 0.
        p(ipl)%lracsenzf(:) = 0.
        p(ipl)%lracsenzg(:) = 0.
        p(ipl)%drlsenf(:) = 0.
        p(ipl)%drlseng(:) = 0.
        p(ipl)%precrac(:) = 0.
        p(ipl)%irrigprof(:) = 0.
        p(ipl)%racnoy(:) = 0.
        p(ipl)%izrac = 1.
        p(ipl)%idzrac = 1.
        p(ipl)%exolai = 1.
        p(ipl)%exobiom = 1.

        p(ipl)%efda = 1.
        p(ipl)%efnrac_mean = 1.
        p(ipl)%humirac_mean = 1.
        p(ipl)%group = 0
        p(ipl)%somupvt = 0.
        p(ipl)%somupvtI = 0.
        p(ipl)%somtemp = 0.

! --------------------------------------------------------------
!  INITIALISATIONS
! --------------------------------------------------------------

    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! PL, 29/11/2018: INVALIDATION POUR l'instant, A REVOIR 
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! initialisation des stades pour les perennes
    ! DR et Lolo 08/11/2018 voir le test sur la branche perenne pour voir si ca passe pour la vigne
    ! DR voir avec inaki si c'est coherent de faire ca
    !    if  (p(ipl)%P_codeperenne == 2 .and. (p(ipl)%P_stade0 == snu .or. p(ipl)%P_stade0 == plt)) then
    !      call EnvoyerMsgHistorique(logger, MESSAGE_338)
    !      p(ipl)%P_stade0 = dor
    !    endif	
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
        if (p(ipl)%P_stade0 == plt) p(ipl)%P_stade0 = snu
        if (p(ipl)%P_stade0 /= snu) then
          p(ipl)%codeinstal = 1
        else
          p(ipl)%codeinstal = 0
        endif




    ! ajout de dor dans la liste
        if (p(ipl)%P_stade0 /= snu .and. p(ipl)%P_stade0 /= lev  .and.  &
            p(ipl)%P_stade0 /= amf .and. p(ipl)%P_stade0 /= lax  .and.  &
            p(ipl)%P_stade0 /= sen .and. p(ipl)%P_stade0 /= drp  .and.  &
            p(ipl)%P_stade0 /= dor) then
          call exit_error(logger, MESSAGE_83 // p(ipl)%P_stade0)
        endif

    ! etat trophique plante
        !if (p(ipl)%codeinstal == 1 .and. p(ipl)%P_zrac0 == 0.) then
        if (p(ipl)%codeinstal == 1 .and. abs(p(ipl)%P_zrac0).lt.1.0E-8) then
          call EnvoyerMsg(logger, 'P_zrac0 = ' // to_string(p(ipl)%P_zrac0))
          call exit_error(logger, MESSAGE_84)
        endif
        if (p(ipl)%codeinstal == 1 .and. p(ipl)%P_coderacine == 2 .and.               &
            p(ipl)%P_densinitial(1) <= 0 .and. p(ipl)%P_densinitial(2) <= 0 .and.       &
            p(ipl)%P_densinitial(3) <= 0 .and. p(ipl)%P_densinitial(4) <= 0 .and.       &
            p(ipl)%P_densinitial(5) <= 0.) then
            call exit_error(logger, MESSAGE_196)
        endif

! Modif bug Loic Octobre 2016 : Decale apres la mise a jour de codeinstal sinon tout est mis a 0...
        if (p(ipl)%codeinstal <= 0 .and. p(ipl)%P_codeperenne == 1) then
          p(ipl)%rlf(:) = 0.
          p(ipl)%rlg(:) = 0.
          p(ipl)%rlf_veille(:) = 0.
          p(ipl)%rlg_veille(:) = 0.
          !print *, "taille tableaux racines :", size(r(ipl)%drlf)
          r(ipl)%drlf(:,:) = 0.
          r(ipl)%drlg(:,:) = 0.
        endif

        if (p(ipl)%codeinstal == 1) then  ! 1ere usm pas en sol nu
        ! test si drp avant lax
          if (p(ipl)%P_stlevdrp(itk(ipl)%P_variete) <                                                        &
                       (p(ipl)%P_stlevamf(itk(ipl)%P_variete) + p(ipl)%P_stamflax(itk(ipl)%P_variete))) then
            drpavantlax = .true.
          else
            drpavantlax = .false.
          endif
          p(ipl)%nplt = 1
          p(ipl)%nlev = 1
          if (p(ipl)%P_stade0 == dor) p(ipl)%nlev = 0
          p(ipl)%nger = 1
!          if (P_stade0 == dor) nger = 0
          p(ipl)%zrac = max(p(ipl)%P_zrac0,itk(ipl)%P_profsem)

          p(ipl)%fixpot(:) = 0.
          p(ipl)%fixreel(:) = 0.
          p(ipl)%offrenod(:) = 0.
          p(ipl)%fixmaxvar(:) = 0.
          p(ipl)%zracmax   = 0.

! DR 05/01/2016 on teste la prise en compte des reserves
!!!!!!!!!!!!!!!!!!!!!!  bien voir ce que ca fait sur les autres plantes !!!!!!!!!!!!!!!!
!!!!  ACHTUNG !!!!
! 21/03/2016 pour la prairie cette modif pose pb, on la restreint a vigne et pommier
! doit etre regle par les modifs qui suivent fevrier 2013
!          p(ipl)%P_masec0=p(ipl)%P_masec0 + p(ipl)%P_resperenne0
!          p(ipl)%masec(:,0) = p(ipl)%P_masec0
!          if(p(ipl)%P_codeplante==CODE_VINE.or.p(ipl)%P_codeplante==CODE_APPLE)then
!              p(ipl)%P_masec0=p(ipl)%P_masec0 + p(ipl)%P_resperenne0
!          endif
!! *** a enlever
!  DR le 26/06/2019 ce test etait active dans la 9.1 ...je le reactive
! Loic fevrier 2021 je re re re active et je change masecnp par masec suite a modif fichiers ini
          if(p(ipl)%P_codeplante==CODE_VINE.or.p(ipl)%P_codeplante==CODE_APPLE)then
!              p(ipl)%P_masecnp0 = p(ipl)%P_masecnp0 + p(ipl)%P_restemp0
              p(ipl)%P_masec0 = p(ipl)%P_masec0 + p(ipl)%P_restemp0
          endif
!! *** a enlever

          p(ipl)%masecnp(:,0) = p(ipl)%P_masecnp0
          p(ipl)%magrain(:,0) = p(ipl)%P_magrain0

          if (p(ipl)%P_codelaitr == 1) then
            p(ipl)%lai(AS,0) = p(ipl)%P_lai0
            p(ipl)%lai(AO,0) = p(ipl)%P_lai0
            p(ipl)%lai(AOAS,0) = p(ipl)%P_lai0
          else
            sc%tauxcouv(0) = p(1)%P_lai0 ! TODO : a mettre ailleurs que dans une boule plante
          endif

! MISE EN ATTENTE ----------------------------
! DEFINIR SI UTILE, PAS FAIT AILLEURS !
! En cas d'enchainement d'annees climatiques
!DR 14/04/2016 pour les pariries on stocke des varaibles plantes supplementaires dans le recup.tmp
! on reaffecte
!        if (pg%P_codeinitprec == 2 .and. sc%P_codesuite /= 0)then
!               p(ipl)%mafeuiltombe(:)=p(ipl)%mafeuiltombe0(:)
!               p(ipl)%msneojaune(:) =p(ipl)%msneojaune0(:)
!               p(ipl)%masecneo(:) =p(ipl)%masecneo0(:)
!               p(ipl)%mafeuiljaune(:)=p(ipl)%mafeuiljaune0(:)
!               p(ipl)%dltamsen(:) = p(ipl)%dltamsen0(:)
!        endif
! ------------------------------------------------------------------------------------



! dr 09/08/2019 je le deplace la car on s'en ser apres

! DR 08/08/2019 pour la vigne on va faire des initilaisations particulieres car on ne peut pas mettre les QNplante0 dans les pools du param_mewform , sinon inaki va peter un cable :-)
! Loic Oct 2020 : je mets code_acti_reserve plus generique et je mets � jour les reserves
! Loic Fervier 2021 : j'initialise restemp a restemp0
!          if( p(ipl)%P_codeplante.eq. CODE_VINE)then
          if (p(ipl)%P_code_acti_reserve .eq. 2 .and. sc%P_codesuite .eq. 0) then
          !   p(ipl)%restemp = p(ipl)%P_masecnp0
             p(ipl)%restemp = p(ipl)%P_restemp0
!             t%P_QNperenne0 = p(ipl)%P_QNplantenp0
             p(ipl)%P_QNperenne0 = p(ipl)%P_QNplante0
             p(ipl)%P_QNplantenp0 = 0.0
          endif

          !DR et LS le 10/05/2022 si on enchaine 2 prairies on recupere le masecnp0 du recup
           if(sc%P_codesuite.eq.1)then
             p(ipl)%P_masec0 = p(ipl)%P_masecnp0
           endif

! Modifs Bruno et Loic fevrier 2013
          p(ipl)%dltamsrac = 0.
          p(ipl)%maperenne(0) = p(ipl)%P_maperenne0
          p(ipl)%maperenne(AS) = p(ipl)%P_maperenne0
          p(ipl)%maperenne(AO) = p(ipl)%P_maperenne0
          p(ipl)%QNperenne(0) = p(ipl)%P_QNperenne0
          p(ipl)%QNperenne(AS) = p(ipl)%P_QNperenne0
          p(ipl)%QNperenne(AO) = p(ipl)%P_QNperenne0
          p(ipl)%resperenne(0) = p(ipl)%resperenne(0)
          p(ipl)%resperenne(AS) = p(ipl)%resperenne(0)
          p(ipl)%resperenne(AO) = p(ipl)%resperenne(0)
          p(ipl)%QNresperenne(0) = p(ipl)%QNresperenne(0)
          p(ipl)%QNresperenne(AS) = p(ipl)%QNresperenne(0)
          p(ipl)%QNresperenne(AO) = p(ipl)%QNresperenne(0)
          p(ipl)%restemp(0) = p(ipl)%P_restemp0
          p(ipl)%restemp(AS) = p(ipl)%P_restemp0
          p(ipl)%restemp(AO) = p(ipl)%P_restemp0
          ! Modif Loic juin 2021
          p(ipl)%QNrestemp = p(ipl)%QNrestemp0
          p(ipl)%msrac(:) = p(ipl)%msrac0
          p(ipl)%QNrac    = p(ipl)%QNrac0


! masec = biomasse totale plante (hors racines), donc masec0 inclut maperenne0
! QNplante = quantite totale d'azote plante (hors racines), inclut QNperenne

! DR 08/08/2019 a voir avce Loic ca doit poser pb pour les CAS , on affecte masec a la plante entiere mais aussi � l'ombre et au soleil !! (idem pour magrain)
! Loic fevrier 2021 : on a besoin du code_acti_reserve avec la modification des fichiers ini
!          p(ipl)%masec(:,sc%n) = p(ipl)%P_masecnp0 + t%P_maperenne0
!          p(ipl)%QNplante = p(ipl)%P_QNplantenp0 + t%P_QNperenne0
!          p(ipl)%mstot = p(ipl)%P_masecnp0    + t%P_maperenne0 + t%P_msrac0
!          p(ipl)%QNtot = p(ipl)%P_QNplantenp0 + t%P_QNperenne0 + t%P_QNrac0
!          p(ipl)%QNtot0 = p(ipl)%QNtot
!          p(ipl)%masecnp(AOAS,sc%n) = p(ipl)%P_masecnp0
!          p(ipl)%masecnp(AS,sc%n) = p(ipl)%P_masecnp0
!          p(ipl)%masecnp(AO,sc%n) = p(ipl)%P_masecnp0
!          p(ipl)%QNplantenp(AOAS,sc%n) = p(ipl)%P_QNplantenp0
!          p(ipl)%QNplantenp(AS,sc%n) = p(ipl)%P_QNplantenp0
!          p(ipl)%QNplantenp(AO,sc%n) = p(ipl)%P_QNplantenp0
!          p(ipl)%magrain(:,sc%n) = p(ipl)%P_magrain0

          if (p(ipl)%P_code_acti_reserve == 1) then
            p(ipl)%masec(:,sc%n) = p(ipl)%P_masecnp0 + p(ipl)%P_maperenne0
            p(ipl)%QNplante = p(ipl)%P_QNplantenp0 + p(ipl)%P_QNperenne0
            p(ipl)%mstot = p(ipl)%P_masecnp0    + p(ipl)%P_maperenne0 + p(ipl)%msrac0
            p(ipl)%QNtot = p(ipl)%P_QNplantenp0 + p(ipl)%P_QNperenne0 + p(ipl)%QNrac0
            p(ipl)%QNtot0 = p(ipl)%QNtot
            p(ipl)%masecnp(AOAS,sc%n) = p(ipl)%P_masecnp0
            p(ipl)%masecnp(AS,sc%n) = p(ipl)%P_masecnp0
            p(ipl)%masecnp(AO,sc%n) = p(ipl)%P_masecnp0
            p(ipl)%QNplantenp(AOAS,sc%n) = p(ipl)%P_QNplantenp0
            p(ipl)%QNplantenp(AS,sc%n) = p(ipl)%P_QNplantenp0
            p(ipl)%QNplantenp(AO,sc%n) = p(ipl)%P_QNplantenp0
            p(ipl)%magrain(:,sc%n) = p(ipl)%P_magrain0
            p(ipl)%masecveg(:) = p(ipl)%P_masecnp0 - p(ipl)%P_magrain0
          else
            p(ipl)%masec(:,sc%n) = p(ipl)%P_masec0
            p(ipl)%QNplante = p(ipl)%P_QNplantenp0 + p(ipl)%P_QNperenne0
            p(ipl)%mstot = p(ipl)%P_masec0 + t%P_msrac0
            p(ipl)%QNtot = p(ipl)%P_QNplantenp0 + p(ipl)%P_QNperenne0 + t%P_QNrac0
            p(ipl)%QNtot0 = p(ipl)%QNtot
            p(ipl)%masecnp(AOAS,sc%n) = p(ipl)%P_masec0
            p(ipl)%masecnp(AS,sc%n) = p(ipl)%P_masec0
            p(ipl)%masecnp(AO,sc%n) = p(ipl)%P_masec0
            p(ipl)%P_masecnp0 = p(ipl)%P_masec0
            p(ipl)%QNplantenp(AOAS,sc%n) = p(ipl)%P_QNplantenp0
            p(ipl)%QNplantenp(AS,sc%n) = p(ipl)%P_QNplantenp0
            p(ipl)%QNplantenp(AO,sc%n) = p(ipl)%P_QNplantenp0
            p(ipl)%magrain(:,sc%n) = p(ipl)%P_magrain0
            p(ipl)%masecveg(:) = p(ipl)%P_masec0 - p(ipl)%P_magrain0
            ! Ajout Loic juin 2021 : pour les fourrages il faut initialiser QNplantenp a la valeur de QNplante
            if (p(ipl)%P_codeplante == CODE_FODDER) then
                p(ipl)%QNplantenp(AOAS,sc%n) = p(ipl)%QNplante
                p(ipl)%QNplantenp(AS,sc%n) = p(ipl)%QNplante
                p(ipl)%QNplantenp(AO,sc%n) = p(ipl)%QNplante
                p(ipl)%QNperenne(0) = 0.
                p(ipl)%QNperenne(AS) = 0.
                p(ipl)%QNperenne(AO) = 0.
             endif
          endif

          p(ipl)%maperennemort(AOAS) = 0.
          p(ipl)%maperennemort(AO) = 0.
          p(ipl)%maperennemort(AS) = 0.
          p(ipl)%QCperennemort(AOAS) = 0.
          p(ipl)%QCperennemort(AO) = 0.
          p(ipl)%QCperennemort(AS) = 0.
          p(ipl)%QNperennemort(AOAS) = 0.
          p(ipl)%QNperennemort(AO) = 0.
          p(ipl)%QNperennemort(AS) = 0.
          p(ipl)%dltaperennesen(AOAS) = 0.
          p(ipl)%dltaperennesen(AO) = 0.
          p(ipl)%dltaperennesen(AS) = 0.
          p(ipl)%dltaQNperennesen(AOAS) = 0.
          p(ipl)%dltaQNperennesen(AO) = 0.
          p(ipl)%dltaQNperennesen(AS) = 0.
          p(ipl)%QCO2resperenne(AOAS) = 0.
          p(ipl)%QCO2resperenne(AO) = 0.
          p(ipl)%QCO2resperenne(AS) = 0.
          p(ipl)%msracmort   = 0.
          p(ipl)%msracmortf(:) = 0.
          p(ipl)%msracmortg(:) = 0.
          p(ipl)%QCracmort   = 0.
          p(ipl)%QNracmort   = 0.
          p(ipl)%QNveg       = p(ipl)%P_QNplantenp0
          p(ipl)%QNvegstruc  = p(ipl)%P_QNplantenp0 - p(ipl)%QNrestemp0
          p(ipl)%QNfeuille   = p(ipl)%QNfeuille0
          p(ipl)%QNtige      = p(ipl)%QNtige0
          p(ipl)%msresgel(AOAS) = p(ipl)%msresgel0
          p(ipl)%msresgel(AO) = p(ipl)%msresgel0
          p(ipl)%msresgel(AS) = p(ipl)%msresgel0
          p(ipl)%QNresgel(AOAS) = p(ipl)%QNresgel0
          p(ipl)%QNresgel(AO) = p(ipl)%QNresgel0
          p(ipl)%QNresgel(AS) = p(ipl)%QNresgel0

! Ajouts Loic Mai 2016

          p(ipl)%mafeuilp(:)  = p(ipl)%mafeuil0
          p(ipl)%matigestrucp(:) = p(ipl)%matigestruc0
          p(ipl)%mafeuil(:)   = p(ipl)%mafeuil0
          p(ipl)%matigestruc(:) = p(ipl)%matigestruc0
          p(ipl)%mafeuilverte(:) = p(ipl)%mafeuilverte0
          p(ipl)%mafeuiljaune(:) = p(ipl)%mafeuil0 - p(ipl)%mafeuilverte0
          p(ipl)%nstopres = 0


! Ajout Loic Juillet 2016: nouvelle variable lue pour les plantes perennes fixatrices d'azote
          p(ipl)%somcourno    = p(ipl)%somcourno0
          p(ipl)%ndno         = int(p(ipl)%ndno0)
          p(ipl)%nfno         = int(p(ipl)%nfno0)
          if(p(ipl)%nfno > 0.) p(ipl)%propfixpot = 1.
! Fin modifs Loic et Bruno fevrier 2013
! Ajout Florent C. et Fabien, Octobre 2019
          p(ipl)%Qfix(AOAS) = p(ipl)%Qfix0
          p(ipl)%Qfix(AO) = p(ipl)%Qfix0
          p(ipl)%Qfix(AS) = p(ipl)%Qfix0

          p(ipl)%QNplantetombe(:) = 0.
          p(ipl)%QCplantetombe(:) = 0.
!
          p(ipl)%QCrogne = 0.
          p(ipl)%QNrogne = 0.
          p(ipl)%QCplante = 0.

          p(ipl)%cumdltares(:) = 0.

          if (p(ipl)%P_masecnp0 > 0.) then
! Loic Oct 2020 : il faut faire le test sur QNplante quand code_acti_reserve == 2
!            if (p(ipl)%P_QNplantenp0 <= 0.) then
            if (p(ipl)%P_code_acti_reserve == 1) then
              if (p(ipl)%P_QNplantenp0 <= 0.) then
                  call EnvoyerMsgHistorique(logger, MESSAGE_85)
                  p(ipl)%P_QNplantenp0 = p(ipl)%P_adil * p(ipl)%P_masecnp0 * 10.
                  p(ipl)%QNplantenp(AS,0) = p(ipl)%P_QNplantenp0
                  p(ipl)%QNplantenp(AO,0) = 0.
                  p(ipl)%QNplantenp(0,0) = p(ipl)%QNplantenp(AO,0) * p(ipl)%surf(AO) + p(ipl)%QNplantenp(AS,0) * p(ipl)%surf(AS)
               endif
            else
              if (p(ipl)%QNplante <= 0.) then
                  call EnvoyerMsgHistorique(logger, MESSAGE_85)
! Loic fevrier 2021 je change masecnp par masec suite a modif fichiers ini
!                  p(ipl)%P_QNplantenp0 = p(ipl)%P_adil * p(ipl)%P_masecnp0 * 10.
                  p(ipl)%P_QNplantenp0 = p(ipl)%P_adil * p(ipl)%P_masec0 * 10.
                  p(ipl)%QNplantenp(AS,0) = p(ipl)%P_QNplantenp0
                  p(ipl)%QNplantenp(AO,0) = 0.
                  p(ipl)%QNplantenp(0,0) = p(ipl)%QNplantenp(AO,0) * p(ipl)%surf(AO) + p(ipl)%QNplantenp(AS,0) * p(ipl)%surf(AS)
               endif
            endif
            p(ipl)%INN0(:) = p(ipl)%QNplantenp(:,0) / (p(ipl)%P_masecnp0 *10.*(p(ipl)%P_adil*p(ipl)%P_masecnp0**(-p(ipl)%P_bdil)))
            ! Loic Fevrier 2021
            if (p(ipl)%P_code_acti_reserve .eq. 2 .and. sc%P_codesuite .eq. 0) then
! Loic fevrier 2021 je change masecnp par masec suite a modif fichiers ini
!                p(ipl)%INN0(:) = p(ipl)%QNplante / (p(ipl)%P_masecnp0 *10.*(p(ipl)%P_adil*p(ipl)%P_masecnp0**(-p(ipl)%P_bdil)))
                p(ipl)%INN0(:) = p(ipl)%QNplante / (p(ipl)%P_masec0 *10.*(p(ipl)%P_adil*p(ipl)%P_masec0**(-p(ipl)%P_bdil)))
            endif
            p(ipl)%INN(:) = p(ipl)%INN0(:)
            p(ipl)%INNS(:) = max(p(ipl)%P_INNmin,p(ipl)%INN(:))
            p(ipl)%innlai(:) = max(p(ipl)%P_INNmin,p(ipl)%inn(:))
            p(ipl)%innsenes(:) = max(p(ipl)%P_INNmin,p(ipl)%inn(:))
            p(ipl)%innlai(AOAS) = min(p(ipl)%innlai(AO),p(ipl)%innlai(AS))
          else
            p(ipl)%inns(:) = 1.
            p(ipl)%inn(:) = 1.
            p(ipl)%innlai(:) = 1.
            p(ipl)%innsenes(:) = 1.
          endif

! Ajouts Loic Janvier 2021 : problemes d'initialisation du modele quand on demarre la simulation avec une plante en place
          if (p(ipl)%P_code_acti_reserve == 1) then
            if (p(ipl)%P_lai0 > 0 .and. p(ipl)%mafeuilverte0 == 0) then
                p(ipl)%mafeuilverte0 = p(ipl)%P_lai0 / p(ipl)%P_slamax(itk(ipl)%P_variete) * 100
                p(ipl)%mafeuilverte(:) = p(ipl)%mafeuilverte0
                p(ipl)%mafeuil0 = p(ipl)%mafeuilverte0
                p(ipl)%mafeuil(:) = p(ipl)%mafeuil0
                p(ipl)%mafeuilp(:)  = p(ipl)%mafeuil(:)
                p(ipl)%restemp(0) = p(ipl)%P_propres * p(ipl)%P_masecnp0
                p(ipl)%P_restemp0 = p(ipl)%restemp(0)
                p(ipl)%restemp(AS) = p(ipl)%P_restemp0
                p(ipl)%restemp(AO) = p(ipl)%P_restemp0
                if (p(ipl)%P_masecnp0-p(ipl)%restemp(0)-p(ipl)%mafeuil0 < 0) then
                    p(ipl)%restemp(0) = p(ipl)%P_masecnp0 - p(ipl)%mafeuil0
                endif
                p(ipl)%matigestruc0 = p(ipl)%P_masecnp0 - p(ipl)%mafeuil0 - p(ipl)%restemp(0)
                p(ipl)%matigestruc(:) = p(ipl)%matigestruc0
                p(ipl)%matigestrucp(:) = p(ipl)%matigestruc(:)
                p(ipl)%msresgel(:) = p(ipl)%matigestruc(:)
                CsurNfeuille = p(ipl)%P_parazofmorte / p(ipl)%INN(AOAS)
                p(ipl)%QNfeuille = (440 * p(ipl)%mafeuil0) / CsurNfeuille
                CsurNtige = p(ipl)%P_parazotmorte / p(ipl)%INN(AOAS)
                p(ipl)%QNtige = (440 * p(ipl)%matigestruc0) / CsurNtige
                if (p(ipl)%QNtige > (p(ipl)%P_QNplantenp0-p(ipl)%QNfeuille)) then
                    p(ipl)%QNtige = p(ipl)%P_QNplantenp0 - p(ipl)%QNfeuille
                endif
                p(ipl)%QNrestemp = p(ipl)%P_QNplantenp0 - p(ipl)%QNfeuille - p(ipl)%QNtige
                p(ipl)%QNvegstruc  = p(ipl)%P_QNplantenp0 - p(ipl)%QNrestemp
              endif
          endif
! Fin ajouts Loic Janvier 2021
        else   ! si codeinstal =0 (sol nu)
          p(ipl)%P_QNplantenp0 = 0.
          p(ipl)%lai(:,0) = 0.
          p(ipl)%masecnp(:,0) = 0.
          p(ipl)%QNplantenp(:,0) = 0.
          p(ipl)%QNgrain(:) = 0.
          p(ipl)%inns (:) = 1.
          p(ipl)%innsenes(:) = 1.
          p(ipl)%innlai(:) = 1.
          p(ipl)%inn(:) = 1.

          p(ipl)%QCplantetombe(:) = 0.
          p(ipl)%QNplantetombe(:) = 0.

! Modifs Bruno et Loic fevrier 2013
          CCrac = 380.    !g C/kg MS
          p(ipl)%QCrac = p(ipl)%msrac(0)*CCrac
          p(ipl)%somcourfauche = 0.
        !DR et FR 13/02/2015
          p(ipl)%reste_apres_derniere_coupe = 0.
          p(ipl)%dltamsen(:) = 0.
          p(ipl)%msneojaune(:) = 0.
        endif

      ! enregistrement dans le fichier history
        call EnvoyerMsgHistorique(logger, '')
        call EnvoyerMsgHistorique(logger, '*********************************************')
        call EnvoyerMsgHistorique(logger, MESSAGE_86)
        call EnvoyerMsgHistorique(logger, 'codeinstal : ',p(ipl)%codeinstal)
        call EnvoyerMsgHistorique(logger, 'masec(0) : ',p(ipl)%masec(:,0))
        call EnvoyerMsgHistorique(logger, 'lai(0) : ',p(ipl)%lai(:,0))
        call EnvoyerMsgHistorique(logger, 'P_QNplantenp0 : ',p(ipl)%QNplantenp(:,0))
        call EnvoyerMsgHistorique(logger, 'P_zrac0 : ',p(ipl)%zrac)
        call EnvoyerMsgHistorique(logger, 'P_stade0 : ',p(ipl)%P_stade0)
        call EnvoyerMsgHistorique(logger, 'INN0 : ',p(ipl)%INN0(:))

        if (p(ipl)%P_stade0 == amf) then
          p(ipl)%namf = 1
          p(ipl)%ulai(1) = p(ipl)%P_vlaimax
          p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = p(ipl)%P_stlevdrp(itk(ipl)%P_variete) - p(ipl)%P_stlevamf(itk(ipl)%P_variete)
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = 0.0
        endif

        if (p(ipl)%P_stade0 == lax) then
          p(ipl)%namf = 1
          p(ipl)%nlax = 1
          p(ipl)%ulai(1) = 3.0
          if (drpavantlax) then
            p(ipl)%nflo = 1
            p(ipl)%ndrp = 1
            p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = 0.
          else
            p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = p(ipl)%P_stlevdrp(itk(ipl)%P_variete) &
                                              - p(ipl)%P_stlevamf(itk(ipl)%P_variete) &
                                              - p(ipl)%P_stamflax(itk(ipl)%P_variete)
          endif
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stamflax(itk(ipl)%P_variete) = 0.
        endif

        if (p(ipl)%P_stade0 == sen) then
          p(ipl)%namf = 1
          p(ipl)%nlax = 1
          p(ipl)%nsen = 1
          p(ipl)%ndrp = 1
          p(ipl)%nflo = 1
          p(ipl)%P_nbjgrain(itk(ipl)%P_variete) = 1
          p(ipl)%ulai(1) = 3.
          p(ipl)%P_stdrpmat(itk(ipl)%P_variete) = p(ipl)%P_stlevdrp(itk(ipl)%P_variete) + p(ipl)%P_stdrpmat(itk(ipl)%P_variete) &
                                            - p(ipl)%P_stlevamf(itk(ipl)%P_variete) - p(ipl)%P_stamflax(itk(ipl)%P_variete) &
                                            - p(ipl)%P_stlaxsen(itk(ipl)%P_variete)
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stamflax(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stlaxsen(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stdrpdes(itk(ipl)%P_variete) = 0.
          if (p(ipl)%magrain(AS,0) <= 0 .or. p(ipl)%magrain(AO,0) <= 0) then
            call exit_error(logger, MESSAGE_87)
          endif
        endif

        if (p(ipl)%P_stade0 == drp) then
          p(ipl)%namf = 1
          p(ipl)%nflo = 1
          p(ipl)%ndrp = 1
        ! domi 25/04/2002 compatibilite unix (TODO: ???)
          p(ipl)%ulai(1) = 3.0
          p(ipl)%P_nbjgrain(itk(ipl)%P_variete) = 1
          if (.not.drpavantlax) then
            p(ipl)%P_stlaxsen(itk(ipl)%P_variete) = p(ipl)%P_stlevamf(itk(ipl)%P_variete) + p(ipl)%P_stamflax(itk(ipl)%P_variete) &
                                              + p(ipl)%P_stlaxsen(itk(ipl)%P_variete) - p(ipl)%P_stlevdrp(itk(ipl)%P_variete)
            p(ipl)%P_stamflax(itk(ipl)%P_variete) = 0.
          else
            p(ipl)%P_stamflax(itk(ipl)%P_variete) = - p(ipl)%P_stlevdrp(itk(ipl)%P_variete) +                                     &
                                            p(ipl)%P_stlevamf(itk(ipl)%P_variete) + p(ipl)%P_stamflax(itk(ipl)%P_variete)
          endif
          p(ipl)%P_stlevamf(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stlevdrp(itk(ipl)%P_variete) = 0.
          p(ipl)%P_stdrpdes(itk(ipl)%P_variete) = 0.
        endif

    ! initialisation des parametres des courbes de dilution si option "plante isolee"
    ! NB le 11/09/05

        if (p(ipl)%P_codeplisoleN == 2) then
          t%bdilI(ipl) = (p(ipl)%P_bdil * log(p(ipl)%P_masecNmax) &
                       - (log(p(ipl)%P_adil / p(ipl)%P_Nmeta))) / log(p(ipl)%P_masecNmax /  p(ipl)%P_masecmeta) 

          t%adilI(ipl) = p(ipl)%P_Nmeta / ( p(ipl)%P_masecmeta**(-t%bdilI(ipl)))

! DR 21/04/2011 P_Nres est renomme P_Nreserve car on garde Nres pour la quantite d'azote des residus au meme titre que Cres
          p(ipl)%P_adilmax =  p(ipl)%P_adil + p(ipl)%P_Nreserve / (p(ipl)%P_masecNmax**(-p(ipl)%P_bdil))

          t%bdilmaxI(ipl) = log(p(ipl)%P_adilmax * p(ipl)%P_masecNmax**(-p(ipl)%P_bdil) / p(ipl)%P_Nmeta)
          t%bdilmaxI(ipl) = - t%bdilmaxI(ipl) / log(p(ipl)%P_masecNmax /  p(ipl)%P_masecmeta)

          t%adilmaxI(ipl) = p(ipl)%P_Nmeta / ( p(ipl)%P_masecmeta**(-t%bdilmaxI(ipl)))
        endif
!! merge trunk 23/11/2020
! 15/04/2020 DR j'ajoute 1 variable operate
        p(ipl)%day_after_emergence = 0

! DR 11/02/2020 maise a zero des variables pour operate
        p(ipl)%flo_moins_150 =0
        p(ipl)%flo_plus_150 =0


        p(ipl)%nb_days_frost_amf_120=0
        p(ipl)%sum_udev =0.
        p(ipl)%som_temp_seuil =0.
        p(ipl)%tsol_sum_ger_lev_0_dpthsow =0.
        p(ipl)%tsol_sum_plt_ger_0_dpthsow = 0.
        p(ipl)%tsol_mean_ger_lev_0_dpthsow =0.
        p(ipl)%tsol_mean_plt_ger_0_dpthsow = 0.
        p(ipl)%tsol_min_ger_lev_0_dpthsow = 100.0
        p(ipl)%tsol_min_plt_ger_0_dpthsow = 100.
        p(ipl)%tsol_min_0_profsem = 100.
        p(ipl)%tsol_mean_0_profsem = 0.
      end do ! Fin boucle nb plantes


!! merge trunk 23/11/2020
! DR 02/04/2020 pour operate j'initialise le nb total d'irrigations
      sc%n_tot_irrigations=0
      sc%q_irrigations(:)=0.
      sc%date_irrigations(:)=0
      sc%nb_days_humair_gt_90_percent1=0
      sc%nb_days_humair_gt_90_percent2=0
      sc%day_after_begin_simul = 0




! *----------------------------------------------
!            Initialisations sol   ??? pourquoi c'est ici et pas dans init_sol ???
! *----------------------------------------------
      sc%ruisselt = 0.
      sc%exces(:) = 0.

    ! Cas no 2:   P_codeinitprec = 1   (reinitialisation)
      if (pg%P_codeinitprec == 1 .or. sc%P_codesuite == 0) then
        sc%Cr = 0.
        sc%Nr = 0.
        sc%Cb = 0.
        sc%Nb = 0.
        sc%Nb0 = 0.
        sc%Nr0 = 0.
        sc%Cb0 = 0.
        sc%Cr0 = 0.
        sc%Cbmulch0 = 0.
        sc%Nbmulch0 = 0.
        sc%Crprof = 0.
        sc%Nrprof = 0.
        sc%Crprof0 = 0.
        sc%Nrprof0 = 0.

!DR 01/06/2017 nouvelles variables de sorties de la mineralisation
        sc%Crprof = 0.
        sc%Nrprof = 0.


        sc%Chum(1:nbCouchesSol) = 0.
        sc%Nhum(1:nbCouchesSol) = 0.
        sc%Cres(1:nbCouchesSol,1:pg%nbResidus) = 0.
        sc%Nres(1:nbCouchesSol,1:pg%nbResidus) = 0.
        sc%Cbio(1:nbCouchesSol,1:pg%nbResidus) = 0.
        sc%Nbio(1:nbCouchesSol,1:pg%nbResidus) = 0.

! dr et ml 22/09/2011 pb de qressuite et qmulch non reinitialises a l'enchainement
        sc%qmulch = 0.
        sc%hres(:) = 0.
        sc%Wb(:) = 0.
        sc%kres(:) = 0.
        sc%NCbio = 0.
        sc%Nnondec(:) = 0.
        sc%Cnondec(:) = 0.
        sc%Nmulchnd = 0.
        sc%Cmulchnd = 0.
        sc%Nmulchdec = 0.
        sc%Cmulch0 = 0.
        sc%Nmulch0 = 0.
        sc%Cbmulch = 0.
        sc%Nbmulch = 0.
        sc%Crprof = 0.
        sc%Nrprof = 0.
        sc%Cmulchdec = 0.
        sc%Cbio(:,:) = 0.
        sc%Nbio(:,:) = 0.
        sc%Qminh = 0.
        sc%Qminr = 0.


! DR 16/08/2019 calcul des stocks selon BM% perenne
!    if(t%P_code_stock_BM.eq.2)then
! Calcul des Stocks initiaux C et N par couche selon BM perenne
!Bruno avril 2018
!==================================================================
!       ihum = int(soil%P_profhum)
        ihum = soil%jhum
        sc%nhe = 0
        do i = 1, sc%NH
           sc%SOCL(i) = 0.
           sc%SONL(i) = 0.
           Msol = 0.
           izmax = soil%P_epc(i)
           do iz = 1,izmax
               if(sc%nhe + iz <= ihum) then
             ! teneur en N organique sur chaque cm (en g/kg)
                   tNorg = soil%P_Norg * 10.
             ! teneur en C organique sur chaque cm (en g/kg)
                   tCorg = tNorg * soil%CsurNsol
             ! Masse de terre fine (t/ha)
                   Msol = soil%P_daf(i) * (1. - soil%P_cailloux(i) / 100.) * 100.
             ! stock de N organique par couche elementaire (en kg/ha/cm)
                   Norg =  tNorg * Msol
                   sc%Nhum(sc%nhe + iz) = Norg
             ! stock de C organique par couche elementaire (en kg/ha/cm)
                   Corg =  Norg * soil%CsurNsol
                   sc%Chum(sc%nhe + iz) = Corg
             ! Cumul de masse de sol, de C et N organiques
                   QMsol = QMsol + Msol
                   sc%SOCL(i) = sc%SOCL(i) + Corg
                   sc%SONL(i) = sc%SONL(i) + Norg
               endif
           end do
           sc%nhe = sc%nhe + izmax  ! nb de couches elementaires ( = profsol ?)
        end do

        do i = 1, sc%nh
            sc%SOC0 = sc%SOC0 + sc%SOCL(i)
            sc%SON0 = sc%SON0 + sc%SONL(i)
        end do
! Calcul de la fraction stable de MOH
        tCorg = sc%SOC0/QMsol

        ! DR 23/08/2019 je mets un code dans param_newform pour pouvoir avoir le finert lu dans param_gen comme dans le trunk
     !   if(t%P_Finert1 > 0.) pg%P_finert =  t%P_Finert1 * exp(-t%P_Finert2 * sc%SOC0)  ! Fonction AMG avec original
     !   if(t%P_Finert1 < 0.) pg%P_finert = -t%P_Finert1 * exp(-t%P_Finert2 * tCorg)  ! Fonction AMG avec teneur en Corg (g/kg)
! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
!      if(t%P_codeFinert.eq.1)then
!          finert = soil%P_finert                         ! finert lu dans sol
!      endif
!      if(t%P_codeFinert.eq.2 .and. t%P_codeFunctionFinert.eq.1)then
!          finert = t%P_finert1 * exp(-t%P_finert2*sc%SOC0)    ! finert1 et finert2 lus dans param_newform
!          endif
!      if(t%P_codeFinert.eq.2 .and. t%P_codeFunctionFinert.eq.2)then
!          finert = t%P_finert1 * exp(-t%P_finert2* tCorg)   ! finert1 et finert2 lus dans param_newform
!      endif


! Stock de N organique actif par couche elementaire (en kg/ha/cm)
        do i =1, ihum
            sc%Nhum(i) = sc%Nhum(i) * (1. - soil%P_finert)
            sc%Chum(i) = sc%Chum(i) * (1. - soil%P_finert)
        end do
! Stock de N et C inerte (Nhumi, en kg/ha) sur tout le profil
        sc%Nhumi = sc%SON0 * soil%P_finert
        sc%Chumi = sc%SOC0 * soil%P_finert
      ! stock de N et C actif (Nhuma, en kg/ha) sur tout le profil
        sc%Nhuma = sc%SON0 * (1. - soil%P_finert)
        sc%Chuma = sc%SOC0 * (1. - soil%P_finert)
      ! stock de N et C total (kg/ha) sur tout le profil
        sc%Nhumt = sc%SON0
        sc%Chumt = sc%SOC0
! DR 11/04/2022 on commente le calcul facon 9.1 qui ne sera plus utilis�
!    else
!    ! calcul des stocs N selon version 9.1
!    !==================================================================
!! Stocks C et N
!        ihum = int(soil%P_profhum)
!      ! stock de N organique sur chaque cm (en kg/Ha)
!        QNorg =  soil%P_Norg * soil%P_daf(1) * (1. - soil%P_cailloux(1) / 100.) * 1000.
!    ! DR 12/06/2017 j'ai repris ca de bruno a voir s ca conbcerne la nouvelle mineralisation
!      ! teneur en C organique sur chaque cm (en g/kg)
!        Corg = soil%P_Norg * soil%CsurNsol * 10.
!      ! stock de C organique sur la profondeur profhum (en t/ha)
!        QCorg = QNorg * soil%CsurNsol * ihum /1000.
!
!      ! stock de N et C actif sur chaque cm (en kg/ha)
!      ! DR 12/06/2017 calcule plus bas autrement
!      !  Cactif = QNorg * (1. - pg%P_finert) / pg%P_Wh ! Bruno : on remplace Wh par CN ratio du sol
!      ! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
!      ! 16/08/2019 je force car dasn 9.1 le code etait en attente avec P_codeFunctionFinert
!       ! DR 17/09/2019 anniv de BL !! je l'ai mis proprement dans le param_newform donc j'enleve le forcage
!!      if(t%P_codeFinert.eq.1)then
!!          finert = soil%P_finert                         ! finert lu dans sol
!!      endif
!!      if(t%P_codeFinert.eq.2 .and. t%P_codeFunctionFinert.eq.1)then
!!          finert = t%P_finert1 * exp(-t%P_finert2*Corg)    ! finert1 et finert2 lus dans param_newform
!!          endif
!!      if(t%P_codeFinert.eq.2 .and. t%P_codeFunctionFinert.eq.2)then
!!          finert = t%P_finert1 * exp(-t%P_finert2*QCorg)   ! finert1 et finert2 lus dans param_newform
!!      endif
!       ! Nactif comme avant a part le switche sur finert
!        Nactif = QNorg * (1. - soil%P_finert )
!        ! dans tous les calculs suivant de C ca revient au meme que l'ancienne version car CsurNsol de SMS = 0 donc CsurNsol=1./pg%P_Wh
!        ! Cactif = QNorg * (1. - pg%P_finert) / pg%P_Wh ! Bruno : on remplace Wh par CN ratio du sol
!         Cactif = Nactif * soil%CsurNsol
!
!        sc%Nhum(1:ihum) = Nactif
!        sc%Chum(1:ihum) = Cactif
!        Ch = sum(sc%Chum(1:ihum))
!
!      ! stock de N actif  (Nhum, en kg/ha) sur tout le profil
!      !  sc%Nhum = Ch * pg%P_Wh
!
!      ! stock de N et C inerte (Nhumi, en kg/ha) sur tout le profil
!        sc%Nhumi = QNorg * soil%P_finert * ihum
!      ! sc%Chumi = sc%Nhumi / pg%P_Wh ! idem que pour Cactif
!        sc%Chumi = sc%Nhumi * soil%CsurNsol
!      ! stock de N et C actif (Nhuma, en kg/ha) sur tout le profil
!        sc%Nhuma = QNorg * (1-soil%P_finert )* ihum
!       !sc%Chuma = sc%Nhuma / pg%P_Wh ! idem que pour Cactif
!        sc%Chuma = sc%Nhuma * soil%CsurNsol
!
!! ** stock de N (kg/ha) et C total (Chumt, en kg/ha) sur tout le profil
!        sc%Nhumt = sc%Nhuma + sc%Nhumi
!        sc%Chumt = sc%Chuma + sc%Chumi   ! modif Bruno on reste en kg/ha
!!27/01/2016
!   endif
! DR 28/05/2020 je sors cette partie du flag stock_BM car ca sert dans les 2 cas .
     ! memorisation des stocks initiaux de MO du sol: C et N total, actif, inerte
        sc%Chumt0 = sc%Chumt
        sc%Nhumt0 = sc%Nhumt
        sc%Chuma0 = sc%Chuma
        sc%Nhuma0 = sc%Nhuma
        sc%Chumi0 = sc%Chumi
        sc%Nhumi0 = sc%Nhumi
    ! initialisation de la production de CO2
        sc%QCO2sol = 0. ! TODO: a mettre dans initsol !
        sc%QCapp = 0.
        sc%QNapp = 0.
        sc%QCresorg = 0.
        sc%QNresorg = 0.
      endif
!  write(5588,*)'dans initnonsol apres modif fin iwater',sc%P_iwater,'ifwater',sc%P_ifwater,sc%ifwater_courant,sc%maxwth

! Initialisation des profils d'eau et d'azote
! *******************************************
      do i = 1, sc%NH
        if (sc%Hinit(i) <= 0.) sc%Hinit(i) = soil%hcc(i)   ! TODO remplacer la boucle par une instruction WHERE sur tableau
      end do

 ! 1er cas : profil homogene dans chaque couche
      sc%nhe = 0
      sc%SMN0 = 0.
      do i = 1, sc%nh
        izmax = soil%P_epc(i)
        do iz = 1,izmax
           sc%HUCC(sc%nhe+iz) = soil%HCC(i)  * soil%da(i) /10.
           sc%HUMIN(sc%nhe+iz) = soil%HMIN(i) * soil%da(i) /10.
           sc%dacouche(sc%nhe+iz) = soil%da(i)

! on n'initialise hur, nit et amm que pour la premiere culture et quand on n'enchaine pas
! DR 22/06/2018 y'avait une differnce detectee par CB entre une usm solnu+mais en rotation et une usm mais sur la periode totale
! hur avait ete lu dans recup.tmp et il est recalule ici a partir de hinit
! donc on ne garde pas la precision de la lecture de HUR de fin d usm precedente (chaque hur de 1cm est different, on recalcul a partior de l'integration des valeurs initiales
! ca parait rien mais ca peut faire des differences notables dues a l'effet sur la germination-leveee
!DR 21/12/2018 on remets l'ancien test pour comparer avant merge
          if (pg%P_codeinitprec == 1 .or. sc%numcult == 1) then
!          if (pg%P_codeinitprec == 1 .or. sc%numcult == 1.and.sc%P_codesuite.ne.1) then
            sc%hur(sc%nhe+iz)   = sc%Hinit(i)* soil%da(i) /10.
            soil%nit(sc%nhe+iz) = soil%NO3init(i) / soil%P_epc(i)


            soil%amm(sc%nhe+iz) = sc%NH4init(i) / soil%P_epc(i)
            HR_dom(i)=HR_dom(i)+sc%hur(sc%nhe+iz)
            azo_dom(i)=azo_dom(i)+soil%nit(sc%nhe+iz)
            amm_dom(i)=amm_dom(i)+soil%amm(sc%nhe+iz)
          endif
          sc%hurmini(sc%nhe+iz) = sc%hucc(sc%nhe+iz)
        end do
        ! nb de couches elementaires ( = profsol ?)
        sc%nhe = sc%nhe + izmax  
      end do
! 2eme cas: profil non homogene lisse par une fonction spline
      if (sc%NH > 1) then
        if (pg%P_iniprofil == 1 .and. sc%numcult == 1 .and. (sc%P_codesuite == 0 .or. pg%P_codeinitprec == 1)) then
        ! Qeauinit = quantite d'eau dans chaque horizon (en mm)
          do i = 1,sc%NH
            Qeauinit(i) = sc%Hinit(i) * soil%da(i) / 10. * soil%P_epc(i)
          end do

          call AJUPRO(sc%nh,soil%P_epc(1:sc%nh),soil%NO3init(1:sc%nh),sc%nhe,soil%nit(1:nbCouchesSol),QEXC)

        ! pas d'ajustement eau si la teneur en eau du 1er horizon est egale a la capacite au champ
          if (sc%Hinit(1) < soil%hcc(1)) then
            call AJUPRO(sc%NH,soil%P_epc(1:sc%nh),Qeauinit(1:sc%nh),sc%nhe,sc%hur(1:nbCouchesSol),HEXC)
        ! NB 25/05/05 ajout pour eviter pb lissage profils : UTILE ?
            do iz = 1,sc%nhe
              if (sc%hur(iz) <= 0.) then
                call exit_error(logger, MESSAGE_520)
              endif
            end do
          endif
        endif
      endif

      if (soil%P_humcapil >= sc%hucc(sc%nhe)) call EnvoyerMsgHistorique(logger, MESSAGE_336)

! *---------------------------------------------* c
! **         Fin des initialisations sol       ** c
! *********************************************** c

    ! la profondeur de sol doit etre inferieure a nbCouchesSol
       if (soil%profsol > nbCouchesSol) then
        call EnvoyerMsg(logger, 'soil depth = ' // to_string(soil%profsol))
        call exit_error(logger, MESSAGE_180 // to_string(nbCouchesSol))
      endif

      sc%recolte1 = .TRUE.
      sc%P_datefin = .FALSE.
      sc%irmulch=1

! Modif Bruno avril 2017
      soil%profcalc = 0
      do ipl = 1, sc%P_nbplantes
        ! le calcul des reserves ne peut depasser la profondeur du sol
        if (itk(ipl)%P_profmes > soil%profsol) then
          call EnvoyerMsgHistorique(logger, MESSAGE_88,soil%profsol)
          itk(ipl)%P_profmes = soil%profsol
        endif

        if (pg%P_codeprofmes == 1) then
          soil%profcalc = max(soil%profcalc,int(itk(ipl)%P_profmes))
          soil%profcalc = min(soil%profcalc, soil%profsol)   ! profcalc ne peut pas depasser profsol
        else
          soil%profcalc = soil%profsol
        endif
! fin modif Bruno

    ! DR et FR 23/06/2015 on nettoie initnonsol en isolant les initialisations prairies dans une subroutine independante
    ! DR et FR  24/06/2015 on conditionne l'appel sur l'initialisation des fauches aux cultures fauchees
        if(p(ipl)%P_codeplante == CODE_FODDER) call init_prairie(logger,sc,p,itk)
      ! initialisation des phyllothermes
        if (p(ipl)%P_nbfeuilplant <= 0 .or. p(ipl)%codeinstal /= 1) then
          p(ipl)%nbfeuille = 0
        else
          p(ipl)%nbfeuille = p(ipl)%P_nbfeuilplant
        endif
        p(ipl)%somfeuille = 0.

      ! si on est en taux de recouvrement on a P_codebeso = 1 et pas d'interception de la pluie
        if (p(ipl)%P_codelaitr == 2) then
          if (p(ipl)%P_codebeso /= 1) then
            call EnvoyerMsgHistorique(logger, MESSAGE_191)
            call EnvoyerMsgHistorique(logger, MESSAGE_193)
            p(ipl)%P_codebeso = 1
          endif
          if (p(ipl)%P_codeintercept == 1) then
            call EnvoyerMsgHistorique(logger, MESSAGE_191)
            call EnvoyerMsgHistorique(logger, MESSAGE_192)
            p(ipl)%P_codeintercept = 2
          endif
          if (itk(ipl)%P_codefauche /= 2) then
            call EnvoyerMsgHistorique(logger, MESSAGE_191)
            call EnvoyerMsgHistorique(logger, MESSAGE_194)
            itk(ipl)%P_codefauche = 2
          endif

          if (sta%P_codecaltemp /= 1) then
            call EnvoyerMsgHistorique(logger, MESSAGE_191)
            call EnvoyerMsgHistorique(logger, MESSAGE_195)
            sta%P_codecaltemp = 1
          endif

          p(ipl)%P_codlainet = 1
          p(ipl)%P_codetransrad = 1
        endif
        ! Ajout Loic Avril 2017 : on impose le transfert radiatif quand on est en cultures associees
        if (sc%P_nbplantes > 1) then
           p(ipl)%P_codetransrad = 2
           call EnvoyerMsgHistorique(logger, MESSAGE_385)
        endif
        p(ipl)%fapar(:) = 0.

        if (p(ipl)%P_tcxstop < 100 .and. p(ipl)%P_tdmax >= p(ipl)%P_tcxstop) then
          call exit_error(logger, MESSAGE_446)
        endif

        if (pg%P_codeinitprec == 1 .or. sc%numcult == 1) then
          p(ipl)%fauchediff = .FALSE.
      ! DR et FR 23/06/2015 si on est en reset (codeinitprtec=1) et qu'on simule une serie climatique on doir repartir comme une annee independante
          p(ipl)%msresjaune(:) = 0.
          p(ipl)%msneojaune(:) = 0.
          p(ipl)%dltamstombe(:) = 0.
          p(ipl)%dltaisen(:) = 0.
        endif

      ! DR 24/03/2020 on initialise a lai0 et plus tard on affecte a chaque coupe le lairesiduel
         P(ipl)%lai_to_sen = P(ipl)%P_lai0

      end do
! DR 29/04/2013 j'ajoute un compteur pour le calcul force de priestley taylor en cas de donnees manquantes
      sc%compt_calcul_taylor=0

    ! DR 31/08/07 pour qu'il n'y ai pas de problemes dans les enchainements
    ! une fois recup.tmp lu on l'efface comme ca si il n'est pas genere dans cette simul
    ! il ne peut pas etre reutilise plus tard
    call erase_path(join_path(logger%output_path, retrieval_file))
      

!write(444,*)'hur dans initsol',sc%hur

if(dbg_initnonsol) write(4444,*)'Nhum ', sc%Nhum(1:5)



return
end subroutine initnonsol
end module initnonsol_m
 
