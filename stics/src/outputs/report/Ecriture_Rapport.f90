! ***************************************************** c
! * ecriture d'un rapport final en fin de simulation  * c
! * + eventuellement a 2 autres dates                 * c
! ***************************************************** c
! Writing a final report at the end of simulation (file mod_rapport.sti)
module Ecriture_Rapport_m
use stdlib_strings, only: to_string
use stdlib_string_type
use stics_files
use messages
USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
USE Station
USE Sol
use CorrespondanceVariablesDeSorties_m
use forced_parameters_m
use call_num_version_m, only: call_num_version
implicit none
private
public :: Ecriture_Rapport
contains
subroutine Ecriture_Rapport(logger,sc,pg,soil,c,sta,p,t,date_force, ipl)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(inout) :: sc  
  type(Parametres_Generaux_), intent(in) :: pg  
  type(Sol_),                 intent(in) :: soil  
  type(Climat_),              intent(in) :: c  
  type(Station_),             intent(in) :: sta  
  type(Plante_),              intent(inout) :: p  
  type(Stics_Transit_),       intent(in) :: t
  integer, intent(in) :: ipl
  logical, intent(in) :: date_force

! Variables locales
      integer           :: ancours  !  
      integer           :: ifin  !  
      integer           :: k  !  
      integer           :: i  
      real              :: nstt1  !  
      real              :: QNF  !  
      real              :: rdt  
      real              :: nstt2
      character         :: sep  
      character(len=10) :: commentcelia  
      integer           :: kk
      character(len = 20 ) :: tag
      character(len=50) :: codeversion
      character(len=40) :: usm_name_formatted
      type(forced_parameter_), allocatable :: forced_parameters(:)
      integer :: rap
      real, allocatable :: valsortierap(:)

      rap = p%report_unit
      call call_num_version(codeversion)
      usm_name_formatted = sc%P_usm

    ! DR 29/12/09 il ne faut ecrire l'entete que le premier jour de simulation
    ! PL, 24/09/2020 fix for writing header only once even for successive simulations 
    ! and associated crops
    if (p%header_report_exist .eqv. .false. .and. sc%P_codesuite == 0) then

        write(rap,223)

        do i = 1, size(sc%valrap)
           write(rap,221) trim(char(sc%valrap(i)))
        end do

        write(rap,'(1x)')

        ! for inactivating header writing for tha next calls 
        sc%codeenteterap(ipl) = 0
        p%header_report_exist = .true.
      endif


      if (pg%P_codeseprapport == 2) then
         sep = pg%P_separateurrapport
      else
         sep = ' '
      endif

    ! actualisation des numeros de jour
      if (p%P_codeplante == CODE_BARESOIL) then
     ! DR 02/12/2021 je ne comprends pas pourquoi on fait ca , c'est trompeur et en sol nu y'a pas de stade physio
     !   p%nlax = sc%n
        p%nst1 = 1
        p%nst2 = 1
      endif

      nstt1 = max(p%nst1,1)
      nstt2 = max(p%nst2,1)

    ! calcul du rendement, des quantites H20 et N sur l'ensemble du profil
    ! domi - 5.0 9/11/00 - rdt calcule meme avant recolte
      rdt      = p%magrain(AOAS,sc%n)/100.
      sc%QH2Of = 0.
      QNF      = 0.

      QNF = sum(soil%AZnit(1:sc%nh))
      sc%QH2Of = sum(sc%hur(1:soil%profsol))

    ! date courante: jour (ifin), annee (ancours)
      ifin = sc%n + sc%P_iwater - 1 ! TODO : remplacer par sc%jul ?
      ancours = sc%annee(ifin)
      if (ifin > sc%nbjsemis) ifin = ifin - sc%nbjsemis

      valsortierap = CorrespondanceVariablesDeSorties(sc%valrap, sc, p, soil, c, sta)

    ! Ecriture des noms des stades
      commentcelia = 'date'

! DR 07/01/2022 si c'est une date on le garde comme une date
     if(.not. date_force)then
      if (sc%n == p%nplt.and.sc%rapplt) commentcelia = 'semis'
      if (sc%codetyperap == 2 .or. sc%codetyperap == 3) then
        if (sc%n == p%nger) commentcelia = 'ger'
        if (sc%n == p%nlev) commentcelia = 'lev'
        if (sc%n == p%ndrp) commentcelia = 'drp'
        if (sc%n == p%nlax) commentcelia = 'lax'
        if (sc%n == p%nflo) commentcelia = 'flo'
        if (sc%n == p%nsen) commentcelia = 'sen'
        if (sc%n == p%namf) commentcelia = 'amf'
        if (sc%n == p%nmat) commentcelia = 'mat'
        ! DR 22/07/2013 j'ajoute le stade start pour Nicolas
        if (sc%n == 1.and.sc%rapdeb.and.sc%start_rap) commentcelia = 'start'
      endif
      if (sc%n == p%nrec) then
        if (sc%recolte1) then
          commentcelia = 'recphy'
          sc%recolte1 = .FALSE.
        else
          commentcelia = 'rectec'
        endif
      endif
      if (sc%n == p%nrec .and. p%nrec == p%nrecbutoir) commentcelia = 'recbut'
    ! DR 22/07/2013 je remplace 'fin' par 'end'
      if (sc%n == sc%maxwth .and. sc%P_datefin) commentcelia = 'end'
endif
      if (pg%P_codesensibilite /= 1) then
! **************** cas courant pas analyse de sensibilite *************
! DR pour benj ecriture speciale pour l'instant je mets un P_codesensibilite = 3
        if (pg%P_codesensibilite == 3) then
          if (sc%codoptim == 0) then
! Bruno juin 2017 ajoute la duree de cycle (n) dans le rapport
            write(rap,889) usm_name_formatted,sep, adjustl(sc%wlieu),sep, sc%ansemis,sep, sc%P_iwater,sep, ancours,sep,     &
                           ifin,sep, sc%n,sep, sc%P_ichsl,sep, p%group,sep, p%P_codeplante,sep, commentcelia,sep, &
                           codeversion, sep, (valsortierap(k),sep,k = 1,size(valsortierap)),                       &
                           float(t%nbapN(ipl)),sep, (float(t%dateN(ipl,k)),sep,k = 1,5),                      &
                           float(t%nbapirr(ipl)), (sep,float(t%dateirr(ipl,k)),k = 1,t%nbapirr(ipl))
          else


            ! Getting names, values and number of parameters from the param.sti file
            forced_parameters = get_forced_parameters(logger, forced_parameter_path)

            write(rap,889) usm_name_formatted,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,       &
                           ifin,sep ,sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,  &
                           codeversion,sep,                                                                  &
                           (forced_parameters(k)%value,sep,k = 1,size(forced_parameters)),&
                           (valsortierap(k),sep,k = 1,size(valsortierap)),              &
                           float(t%nbapN(ipl)),sep,                                                           &
                           (float(t%dateN(ipl,k)),sep,k = 1,5),float(t%nbapirr(ipl)),                   &
                           (sep,float(t%dateirr(ipl,k)),k = 1,t%nbapirr(ipl))
          endif
        else
          if (pg%exponent_form) then ! ecriture scientifique
 !           write(rap,889) usm_name_formattedsep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,       &
 !                               ifin,sep,sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
 !                               codeversion,sep,&
 !                               (valsortierap(k),sep,k = 1,size(valsortierap))
           write(rap,889) usm_name_formatted,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,       &
                                ifin,sep,sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
                                codeversion,&
                                (sep,valsortierap(k),k = 1,size(valsortierap))

          else
 !           write(rap,888) usm_name_formattedsep,sc%P_wdata1(1:index(sc%P_wdata1,'.')-1),sep,sc%ansemis,sep,    &
 !                               sc%P_iwater,sep,ancours,sep,       &
 !                               ifin,sep, sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
 !                               codeversion,sep,&
 !                               (valsortierap(k),sep,k = 1,size(valsortierap))
            write(rap,888) usm_name_formatted,sep,sc%P_wdata1(1:index(sc%P_wdata1,'.')-1),sep,sc%ansemis,sep,    &
                                sc%P_iwater,sep,ancours,sep,       &
                                ifin,sep, sc%n,sep, sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,    &
                                codeversion,&
                                (sep,valsortierap(k),k = 1,size(valsortierap))

      if(p%nflo==sc%n )then
         do kk=1,size(valsortierap)
             sc%valsortie_flo(kk)= valsortierap(kk)
         enddo
      endif
      if(p%nmat==sc%n .or. (p%nrec==sc%n))then
       call EnvoyerMsgEcran(logger,  '  n   mat  rec')
       call EnvoyerMsgEcran(logger, to_string(sc%n) // '   ' // to_string(p%nmat) // '   ' // to_string(p%nrec))
         do kk=1,size(valsortierap)
             sc%valsortie_mat(kk)= valsortierap(kk)
         enddo
      endif
! DR 08/12/2013 pour macsur , j'ajoute les valeurs au semis
      if(p%nplt==sc%n )then
         do kk=1,size(valsortierap)
             sc%valsortie_iplt(kk)= valsortierap(kk)
         enddo
      endif

! pour LI j'ajoute les valeurs a la recolte qui peuvent etre diff si la recolte n'est pas faite a maturite
      if(p%nrec==sc%n)then
      call EnvoyerMsgEcran(logger,  '  n   mat  rec')
      call EnvoyerMsgEcran(logger, to_string(sc%n) // '   ' // to_string(p%nmat) // '   ' // to_string(p%nrec))
         do kk=1,size(valsortierap)
             sc%valsortie_rec(kk)= valsortierap(kk)
         enddo
         !write(*,*)'sc%valsortie_rec(6) nrec',sc%valsortie_rec(6)
      endif


          endif
        endif
      else
        
        ! Getting names, values and number of parameters from the param.sti file
        forced_parameters = get_forced_parameters(logger, forced_parameter_path)


        if (pg%exponent_form) then
!          write(rap,889) usm_name_formattedsep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
!                              ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
!                              codeversion,sep,&
!                              (valpar(k),sep,k = 1,nbpar),(valsortierap(k),sep,k = 1,size(valsortierap))
          write(rap,889) usm_name_formatted,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
                              ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
                              codeversion,&
                              (sep,forced_parameters(k)%value,k = 1,size(forced_parameters)),&
                              (sep,valsortierap(k),k = 1,size(valsortierap))
        else
 !         write(rap,888) usm_name_formattedsep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
 !                             ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
 !                             codeversion,sep,&
 !                             (valpar(k),sep,k = 1,nbpar),(valsortierap(k),sep,k = 1,size(valsortierap))
          write(rap,888) usm_name_formatted,sep,adjustl(sc%wlieu),sep,sc%ansemis,sep,sc%P_iwater,sep,ancours,sep,         &
                              ifin,sep,sc%P_ichsl,sep,p%group,sep,p%P_codeplante,sep,commentcelia,sep,      &
                              codeversion,&
                              (sep,forced_parameters(k)%value,k = 1,size(forced_parameters)),&
                              (sep,valsortierap(k),k = 1,size(valsortierap))
        endif
      endif


221   format(';',A,$)
223   format('P_usm;wlieu;ansemis;P_iwater;ancours;ifin;nbdays;P_ichsl;group;P_codeplante;stade;nomversion',$)
!! merge trunk 23/11/2020 Pour Sam on ajoute de la precision aux sorties
!888   format(2(a40,a1),7(i5,a1),a3,a1,a10,a1,a30,a1,300(f10.3,a1))
!889   format(2(a40,a1),7(i5,a1),a3,a1,a10,a1,a30,a1,300(e16.7,a1))

888   format((a40),(a1,a40),7(a1,i5),a1,a3,a1,a10,a1,a30,300(a1,f10.3))
889   format((a40),(a1,a40),7(a1,i5),a1,a3,a1,a10,a1,a30,300(a1,e16.7))

      sc%ecritrap = .TRUE.
      commentcelia = '   '
end subroutine Ecriture_Rapport
end module Ecriture_Rapport_m
 
