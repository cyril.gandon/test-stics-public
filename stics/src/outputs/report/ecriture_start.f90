module ecriture_start_m
USE Stics
USE Sol
USE calculRsurRU_m, only: calculRsurRU
USE calculRU_m, only: calculRU

implicit none
private
public :: ecriture_start
contains
subroutine ecriture_start(sc,soil)
  type(Stics_Communs_),       intent(INOUT) :: sc
  type(Sol_),                 intent(INOUT) :: soil

! DR je calcule les variables pour le jour start jusqu'a profcalc
      sc%resmes = SUM(sc%hur(1:soil%profcalc) + sc%sat(1:soil%profcalc))
      sc%azomes = SUM(soil%nit(1:soil%profcalc))
      sc%ammomes = SUM(soil%amm(1:soil%profcalc))
      ! TODO : not in stics communs ?
      !sc%SMNmes = sc%azomes + sc%ammomes

    ! calcul de RsurRU
 !    sc%RU = calculRU(soil%profsol, sc%hucc, sc%humin)
 !    sc%RsurRU = calculRsurRU(sc%RU, soil%profsol, sc%hur, sc%sat, sc%humin)
      sc%RU = calculRU(sc%nhe, sc%hucc, sc%humin)
      sc%RsurRU = calculRsurRU(sc%RU, sc%nhe, sc%hur, sc%sat, sc%humin)

end subroutine ecriture_start
end module ecriture_start_m