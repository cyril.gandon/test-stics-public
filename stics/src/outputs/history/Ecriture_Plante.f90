! write to the file history.sti
!!
!! plant parameters
module Plante_Ecriture_m
use phenology_utils
use messages
USE Plante
USE Stics
USE Station
USE Itineraire_Technique
USE Parametres_Generaux
use messages
use plant_utils
implicit none
private
public :: Plante_Ecriture
contains
subroutine Plante_Ecriture(logger,p,sc,sta,itk,pg)
    type(logger_), intent(in) :: logger
    type(Plante_),              intent(INOUT) :: p  
    type(Stics_Communs_),       intent(INOUT) :: sc  
    type(Station_),             intent(INOUT) :: sta  
    type(ITK_),                 intent(INOUT) :: itk  
    type(Parametres_Generaux_), intent(in) :: pg

! Ajout Simon 2017 : une plante annuelle demarre sans C et N dans les reserves et dans les racines
      if (p%P_codeperenne == 1.and.p%P_codeplante /= CODE_FODDER) then
        if (p%P_maperenne0 /= 0. .or. p%P_QNperenne0 /= 0.) then
            call EnvoyerMsgHistorique(logger, MESSAGE_103)
        endif
      endif
! Fin Ajout

!: Tests de coherence et message dans history.sti

    ! le profil type
      if (p%P_coderacine /= 2) then
        if (p%P_zpente >= p%P_zprlim .or. p%P_zlabour >= p%P_zpente .or. p%P_zlabour >= p%P_zprlim) then
          call exit_error(logger, MESSAGE_117)
        endif
      endif

      if (sc%P_nbplantes > 1 .and. p%P_coderacine /= 2) then
        call exit_error(logger, MESSAGE_377)
      endif

      if (p%P_coderacine == 2 .and. p%P_draclong < p%P_dlaimax(itk%P_variete)*1e5) then
        call EnvoyerMsgHistorique(logger, MESSAGE_119)
      endif

    ! en CAS, P_codebeso obligatoirement egal a 2
      if (sc%P_nbplantes > 1 .and. p%P_codebeso /= 2) then
        p%P_codebeso = 2
        call EnvoyerMsgHistorique(logger, MESSAGE_438)
      endif

    ! on ne peut pas avoir 2 stades consecutifs nuls
      if (p%P_stlevamf(itk%P_variete) <= 0. .and. p%P_stamflax(itk%P_variete) <= 0.) then
        call exit_error(logger, MESSAGE_5110)
      endif
!
      if (p%P_stamflax(itk%P_variete) <= 0.0 .and. p%P_stlaxsen(itk%P_variete) <= 0.0) then
        call exit_error(logger, MESSAGE_5111)
      endif
!
      if (p%P_codlainet.eq.1 .and.   &
      (p%P_stlaxsen(itk%P_variete) <= 0.0 .and. p%P_stsenlan(itk%P_variete) <= 0.0)) then
        call exit_error(logger, MESSAGE_5112)
      endif

      if (p%P_sensrsec >= 1.) then
        p%P_sensrsec = 1
        call EnvoyerMsgHistorique(logger, MESSAGE_5113)
      endif

      if (abs(p%P_nbgrmax(itk%P_variete)) <= 1.0D-8 .and. p%P_codeindetermin == 1 &
          .and. abs(p%P_nbgrmax(itk%P_variete)+999) > 1.0D-8) then
        call exit_error(logger, MESSAGE_370)
      endif

      if (p%P_stoprac /= sen .and. p%P_stoprac /= lax  .and. p%P_stoprac /= flo .and. p%P_stoprac /= rec) then
        call exit_error(logger, MESSAGE_371)
      endif

      if (p%P_codeperenne == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_372)
      endif

    ! DR 28/08/07
      if (p%P_codeperenne == 1 .and. p%P_julvernal > p%iplt) then
        p%P_julvernal = p%iplt
        call EnvoyerMsgHistorique(logger, MESSAGE_448)
      endif


! ** ecriture des parametres actifs dans le mouchard HISTORY.STI
      call EnvoyerMsgHistorique(logger, '   ')
      call EnvoyerMsgHistorique(logger, MESSAGE_121)
      call EnvoyerMsgHistorique(logger, MESSAGE_5005)
      call EnvoyerMsgHistorique(logger, 'P_fplt ', p%P_fplt)
! plant name and group
      call EnvoyerMsgHistorique(logger, 'P_codeplante ',p%P_codeplante)
 ! DR 21/03/2014 je verifie que tous les params y soient et j'explicite les messages suivant les codes
      if(p%P_codemonocot == 1) call EnvoyerMsgHistorique(logger, MESSAGE_5195,p%P_codemonocot)
      if(p%P_codemonocot == 2) call EnvoyerMsgHistorique(logger, MESSAGE_5196,p%P_codemonocot)
! effect of atmospheric CO2 concentration
      if (sta%P_codeclichange == 2) then
        call EnvoyerMsgHistorique(logger, 'P_alphaCO2',p%P_alphaCO2)
      ! test sur alpha co2
        if (p%P_alphaco2 >= 2.) then
          call exit_error(logger, MESSAGE_361)
        endif
      endif
! phasic development
      call EnvoyerMsgHistorique(logger, 'P_tdmin ',p%P_tdmin)
      call EnvoyerMsgHistorique(logger, 'P_tdmax ',p%P_tdmax)
      call EnvoyerMsgHistorique(logger, 'P_codetemp ',p%P_codetemp)
      if (p%P_codetemp == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_122)
        if (p%P_codegdh == 1) call EnvoyerMsgHistorique(logger, MESSAGE_373)
        if (p%P_codegdh == 2) call EnvoyerMsgHistorique(logger, MESSAGE_374)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_123)
        call EnvoyerMsgHistorique(logger, 'P_coeflevamf ',p%P_coeflevamf)
        call EnvoyerMsgHistorique(logger, 'P_coefamflax ',p%P_coefamflax)
        call EnvoyerMsgHistorique(logger, 'P_coeflaxsen ',p%P_coeflaxsen)
        call EnvoyerMsgHistorique(logger, 'P_coefsenlan ',p%P_coefsenlan)
        call EnvoyerMsgHistorique(logger, 'P_coeflevdrp ',p%P_coeflevdrp)
        call EnvoyerMsgHistorique(logger, 'P_coefdrpmat ',p%P_coefdrpmat)
        call EnvoyerMsgHistorique(logger, 'P_coefflodrp ',p%P_coefflodrp)
      endif
!
      call EnvoyerMsgHistorique(logger, 'P_codephot ',p%P_codephot)
      if (p%P_codephot == 1) then
        call EnvoyerMsgHistorique(logger, 'P_codephot_part ',p%P_codephot_part)
      endif
!
      if (p%P_coderetflo == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_124)
        call EnvoyerMsgHistorique(logger, 'P_stressdev ',p%P_stressdev)
      endif
      if (p%P_codebfroid == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5123)
        call EnvoyerMsgHistorique(logger, 'P_jvcmini ',p%P_jvcmini)
        if (p%P_codeperenne == 2) then
          call EnvoyerMsgHistorique(logger, 'P_julvernal ',p%P_julvernal)
        endif
        call EnvoyerMsgHistorique(logger, 'P_tfroid ',p%P_tfroid)
        call EnvoyerMsgHistorique(logger, 'P_ampfroid ',p%P_ampfroid)
      endif
      if (p%P_codebfroid == 3) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5124)
        call EnvoyerMsgHistorique(logger, 'P_tdmindeb ',p%P_tdmindeb)
        call EnvoyerMsgHistorique(logger, 'P_tdmaxdeb ',p%P_tdmaxdeb)
        if (p%P_codedormance == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5125)
          call EnvoyerMsgHistorique(logger, 'P_ifindorm ',p%P_ifindorm)
        endif
        if (p%P_codedormance == 2) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5126)
        endif
        if (p%P_codedormance == 3) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5127)
          call EnvoyerMsgHistorique(logger, 'P_idebdorm ',p%P_idebdorm)
          call EnvoyerMsgHistorique(logger, 'P_q10',p%P_q10)
        endif
        if(p%P_codegdhdeb == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5197,p%P_codegdhdeb)
          call EnvoyerMsgHistorique(logger, 'P_code_WangEngel ',p%P_code_WangEngel)
          call EnvoyerMsgHistorique(logger, 'P_tdoptdeb ',p%P_tdoptdeb)
        endif
        if(p%P_codegdhdeb == 2) call EnvoyerMsgHistorique(logger, MESSAGE_5198,p%P_codegdhdeb)
!      ! ml et dr - 23/03/2004
        if (p%P_codeperenne == 1) then
          call exit_error(logger, MESSAGE_130)
        endif
      endif
      call EnvoyerMsgHistorique(logger, 'P_tgmin ',p%P_tgmin)
      call EnvoyerMsgHistorique(logger, 'P_codeperenne ',p%P_codeperenne)
! emergence and starting
      if (p%P_codeperenne == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5128)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5132)
        call EnvoyerMsgHistorique(logger, 'P_restemp0',p%P_restemp0)
      endif
      if (p%P_codegermin == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5129)
        call EnvoyerMsgHistorique(logger, 'P_stpltger ',p%P_stpltger)
      endif
      if (p%P_codehypo == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5130)
        call EnvoyerMsgHistorique(logger, 'P_belong ',p%P_belong)
        call EnvoyerMsgHistorique(logger, 'P_celong ',p%P_celong)
        call EnvoyerMsgHistorique(logger, 'P_elmax ',p%P_elmax)
        call EnvoyerMsgHistorique(logger, 'P_nlevlim1 ',p%P_nlevlim1)
        call EnvoyerMsgHistorique(logger, 'P_nlevlim2 ',p%P_nlevlim2)
        call EnvoyerMsgHistorique(logger, 'P_vigueurbat' ,p%P_vigueurbat)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5131)
        call EnvoyerMsgHistorique(logger, 'P_laiplantule ',p%P_laiplantule)
        call EnvoyerMsgHistorique(logger, 'P_nbfeuilplant ',p%P_nbfeuilplant)
        call EnvoyerMsgHistorique(logger, 'P_masecplantule ',p%P_masecplantule)
        call EnvoyerMsgHistorique(logger, 'P_zracplantule ',p%P_zracplantule)
!        ! ml et dr - 23/03/04
        if (p%P_stade0 == lev) then
          call exit_error(logger, MESSAGE_376)
        endif
      endif
!     ml et dr - 23/03/04
      if (p%P_codebfroid == 1 .and. p%P_stade0 == dor) then
        call exit_error(logger, MESSAGE_131)
      endif

! leaves
      call EnvoyerMsgHistorique(logger, 'P_phyllotherme ',p%P_phyllotherme)
      call EnvoyerMsgHistorique(logger, 'P_laicomp ',p%P_laicomp)
      call EnvoyerMsgHistorique(logger, 'P_tcmin ',p%P_tcmin)
      call EnvoyerMsgHistorique(logger, 'P_tcmax ',p%P_tcmax)
      call EnvoyerMsgHistorique(logger, 'P_tcxstop ',p%P_tcxstop)
      if (p%P_codelaitr == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5133)
        call EnvoyerMsgHistorique(logger, 'P_vlaimax ',p%P_vlaimax)
        call EnvoyerMsgHistorique(logger, 'P_pentlaimax ',p%P_pentlaimax)
        call EnvoyerMsgHistorique(logger, 'P_udlaimax ',p%P_udlaimax)
        call EnvoyerMsgHistorique(logger, 'P_ratiodurvieI ',p%P_ratiodurvieI)
        call EnvoyerMsgHistorique(logger, 'P_ratiosen ',p%P_ratiosen)
        call EnvoyerMsgHistorique(logger, 'P_abscission ',p% P_abscission)
        call EnvoyerMsgHistorique(logger, 'P_parazofmorte ',p%P_parazofmorte)
        call EnvoyerMsgHistorique(logger, 'P_innturgmin ',p%P_innturgmin)
        call EnvoyerMsgHistorique(logger, 'P_dlaimin ',p%P_dlaimin)
        if (p%P_codlainet == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5134)
          call EnvoyerMsgHistorique(logger, 'P_tustressmin ',p%P_tustressmin)
        else
          call EnvoyerMsgHistorique(logger, MESSAGE_5135)
          call EnvoyerMsgHistorique(logger, 'P_durviesupmax ',p%P_durviesupmax)
        endif
        if (p%P_codestrphot == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5136)
          call EnvoyerMsgHistorique(logger, 'P_phobasesen ',p%P_phobasesen)
          call EnvoyerMsgHistorique(logger, 'P_dltamsmaxsen ',p%P_dltamsmaxsen)
          call EnvoyerMsgHistorique(logger, 'P_dltamsminsen ',p%P_dltamsminsen)
          call EnvoyerMsgHistorique(logger, 'P_alphaphot ',p%P_alphaphot)
        endif
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5122)
        if (sc%P_nbplantes > 1) then
          call exit_error(logger, MESSAGE_5000)
        endif
        call EnvoyerMsgHistorique(logger, 'P_tauxrecouvmax ',p%P_tauxrecouvmax)
        call EnvoyerMsgHistorique(logger, 'P_tauxrecouvkmax ',p%P_tauxrecouvkmax)
        call EnvoyerMsgHistorique(logger, 'P_pentrecouv ',p%P_pentrecouv)
        call EnvoyerMsgHistorique(logger, 'P_infrecouv',p%P_infrecouv)
      endif
! radiation interception
      if (p%P_codetransrad == 2 .and. itk%P_codetradtec == 2) then
        call exit_error(logger, MESSAGE_126)
      endif
      call EnvoyerMsgHistorique(logger, 'p%P_codetransrad', p%P_codetransrad)
      call EnvoyerMsgHistorique(logger, 'p%P_codebeso', p%P_codebeso)
      if (p%P_codetransrad == 2) then
        call EnvoyerMsgHistorique(logger, 'P_adfol ',p%P_adfol)
        call EnvoyerMsgHistorique(logger, 'P_dfolbas ',p%P_dfolbas)
        call EnvoyerMsgHistorique(logger, 'P_dfolhaut ',p%P_dfolhaut)
        call EnvoyerMsgHistorique(logger, 'P_forme ',p%P_forme)
        call EnvoyerMsgHistorique(logger, 'P_rapforme ',p%P_rapforme)
      endif
        call EnvoyerMsgHistorique(logger, MESSAGE_125)
! shoot biomass growth
      call EnvoyerMsgHistorique(logger, 'P_temax ',p%P_temax)
      call EnvoyerMsgHistorique(logger, 'P_teoptbis ',p%P_teoptbis)
      call EnvoyerMsgHistorique(logger, 'P_efcroijuv ',p%P_efcroijuv)
      call EnvoyerMsgHistorique(logger, 'P_efcroiveg ',p%P_efcroiveg)
      call EnvoyerMsgHistorique(logger, 'P_efcroirepro ',p%P_efcroirepro)
      if (p%P_codeindetermin == 2 .or. p%P_codeperenne == 2) then
        call EnvoyerMsgHistorique(logger, 'P_remobres ',p%P_remobres)
      endif
      call EnvoyerMsgHistorique(logger, 'P_coefmshaut ',p%P_coefmshaut)
! partitioning of biomass in organs
      call EnvoyerMsgHistorique(logger, 'slamin ',p%P_slamin)
      call EnvoyerMsgHistorique(logger, 'P_envfruit ',p%P_envfruit)
      call EnvoyerMsgHistorique(logger, 'P_sea ',p%P_sea)
      call EnvoyerMsgHistorique(logger, 'P_code_acti_reserve',p%P_code_acti_reserve)
      if(p%P_code_acti_reserve==1)then
         call EnvoyerMsgHistorique(logger, 'P_propresP',p%P_propresP)
         call EnvoyerMsgHistorique(logger, 'P_propresPN',p%P_propresPN)
         call EnvoyerMsgHistorique(logger, 'P_efremobil',p%P_efremobil)
         call EnvoyerMsgHistorique(logger, 'P_tauxmortresp',p%P_tauxmortresp)
         call EnvoyerMsgHistorique(logger, 'P_parazoper',p%P_parazoper)
         call EnvoyerMsgHistorique(logger, 'P_paraTmorte',p%P_parazoTmorte)
         call EnvoyerMsgHistorique(logger, 'P_inilai',p%P_inilai)
      endif
      if(p%P_codedyntalle==1)then
        call EnvoyerMsgHistorique(logger, 'P_codedyntalle',p%P_codedyntalle)
        call EnvoyerMsgHistorique(logger, 'Activation of the module simulating tiller dynamics ')
        call EnvoyerMsgHistorique(logger, 'P_codetranspitalle',p%P_codetranspitalle)
        call EnvoyerMsgHistorique(logger, 'P_SurfApex',p%P_SurfApex)
        call EnvoyerMsgHistorique(logger, 'P_SeuilMorTalle',p%P_SeuilMorTalle)
        call EnvoyerMsgHistorique(logger, 'P_SigmaDisTalle',p%P_SigmaDisTalle)
        call EnvoyerMsgHistorique(logger, 'P_VitReconsPeupl',p%P_VitReconsPeupl)
        call EnvoyerMsgHistorique(logger, 'P_SeuilReconsPeupl',p%P_SeuilReconsPeupl)
        call EnvoyerMsgHistorique(logger, 'P_MaxTalle',p%P_MaxTalle)
        call EnvoyerMsgHistorique(logger, 'P_tigefeuilcoupe',p%P_tigefeuilcoupe)
      endif
! yield formation
      if (p%P_codeindetermin == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5137)
        call EnvoyerMsgHistorique(logger, 'P_cgrain ',p%P_cgrain)
        call EnvoyerMsgHistorique(logger, 'P_cgrainv0 ',p%P_cgrainv0)
        call EnvoyerMsgHistorique(logger, 'P_irazomax ',p%P_irazomax)
        if (p%P_codeir == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5138)
          call EnvoyerMsgHistorique(logger, 'P_irmax ',p%P_irmax)
        else
           call EnvoyerMsgHistorique(logger, MESSAGE_5139)
        endif
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5140)
        call EnvoyerMsgHistorique(logger, 'P_nboite ',p%P_nboite)
        call EnvoyerMsgHistorique(logger, 'P_allocfrmax ',p%P_allocfrmax)
        call EnvoyerMsgHistorique(logger, 'P_afpf ',p%P_afpf)
        call EnvoyerMsgHistorique(logger, 'P_bfpf ',p%P_bfpf)
        call EnvoyerMsgHistorique(logger, 'P_cfpf ',p%P_cfpf)
        call EnvoyerMsgHistorique(logger, 'P_dfpf ',p%P_dfpf)
        call EnvoyerMsgHistorique(logger, 'P_spfrmin ',p%P_spfrmin)
        call EnvoyerMsgHistorique(logger, 'P_spfrmax ',p%P_spfrmax)
        call EnvoyerMsgHistorique(logger, 'P_splaimin ',p%P_splaimin)
        call EnvoyerMsgHistorique(logger, 'P_splaimax ',p%P_splaimax)
      endif
      if (p%P_codetremp == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5143)
        call EnvoyerMsgHistorique(logger, 'P_tminremp ',p%P_tminremp)
        call EnvoyerMsgHistorique(logger, 'P_tmaxremp ',p%P_tmaxremp)
      endif
! roots
      call EnvoyerMsgHistorique(logger, 'P_sensanox ',p%P_sensanox)
      call EnvoyerMsgHistorique(logger, 'P_stoprac ',p%P_stoprac)
      call EnvoyerMsgHistorique(logger, 'P_sensrsec ',p%P_sensrsec)
      call EnvoyerMsgHistorique(logger, 'P_contrdamax ',p%P_contrdamax)
      call EnvoyerMsgHistorique(logger, 'P_rayon ',p%P_rayon)
      if (p%P_codetemprac == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_128)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_129)
      endif
      if (p%P_coderacine == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5144)
        call EnvoyerMsgHistorique(logger, 'P_zlabour ',p%P_zlabour)
        call EnvoyerMsgHistorique(logger, 'P_zpente ',p%P_zpente)
        call EnvoyerMsgHistorique(logger, 'P_zprlim ',p%P_zprlim)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5145)
        call EnvoyerMsgHistorique(logger, 'P_draclong ',p%P_draclong)
        call EnvoyerMsgHistorique(logger, 'P_debsenrac ',p%P_debsenrac)
        call EnvoyerMsgHistorique(logger, 'P_lvfront ',p%P_lvfront)
        call EnvoyerMsgHistorique(logger, 'P_longsperac ',p%P_longsperac)
        if (p%P_codazorac == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5146)
          call EnvoyerMsgHistorique(logger, 'P_minefnra ',p%P_minefnra)
          call EnvoyerMsgHistorique(logger, 'P_minazorac ',p%P_minazorac)
          call EnvoyerMsgHistorique(logger, 'P_maxazorac',p%P_maxazorac)
        endif
      endif
        if (p%P_codtrophrac == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5147)
          call EnvoyerMsgHistorique(logger, 'P_repracpermax ',p%P_repracpermax)
          call EnvoyerMsgHistorique(logger, 'P_repracpermin ',p%P_repracpermin)
          call EnvoyerMsgHistorique(logger, 'P_krepracperm',p%P_krepracperm)
        endif
        if (p%P_codtrophrac == 2) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5148)
          call EnvoyerMsgHistorique(logger, 'P_repracseumax ',p%P_repracseumax)
          call EnvoyerMsgHistorique(logger, 'P_repracseumin ',p%P_repracseumin)
          call EnvoyerMsgHistorique(logger, 'P_krepracseu',p%P_krepracseu)
        endif
        ! DR 19/02/2019 substitution de parazorac en tant que code par un vrai code P_code_rootdeposition
        if(p%P_code_rootdeposition == 1)then
          call EnvoyerMsgHistorique(logger, 'P_parazorac',p%P_parazorac)
        endif
        if (p%P_code_diff_root == 1) then
          call EnvoyerMsgHistorique(logger, 'P_lvmax',p%P_lvmax)
          call EnvoyerMsgHistorique(logger, 'P_rapdia',p%P_rapdia)
          call EnvoyerMsgHistorique(logger, 'P_RTD',p%P_RTD)
          call EnvoyerMsgHistorique(logger, 'P_propracfmax',p%P_propracfmax)
! Lo�c Janvier 2021 : zramif n'est pas utilise je le supprime
!          call EnvoyerMsgHistorique(logger, 'P_zramif',p%P_zramif)
          call EnvoyerMsgHistorique(logger, 'P_code_stress_root',p%P_code_stress_root)
        endif
! frost
      call EnvoyerMsgHistorique(logger, 'P_tletale ',p%P_tletale)
      call EnvoyerMsgHistorique(logger, 'P_tdebgel ',p%P_tdebgel)
      if (p%P_codgellev == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5149)
        call EnvoyerMsgHistorique(logger, 'P_nbfgellev ',p%P_nbfgellev)
        call EnvoyerMsgHistorique(logger, 'P_tgellev90 ',p%P_tgellev90)
      endif
      if (p%P_codgeljuv == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5150)
        call EnvoyerMsgHistorique(logger, 'P_tgeljuv90 ',p%P_tgeljuv90)
      endif
      if (p%P_codgelveg == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5151)
        call EnvoyerMsgHistorique(logger, 'P_tgelveg90 ',p%P_tgelveg90)
      endif
      if (p%P_codgelflo == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5152)
        call EnvoyerMsgHistorique(logger, 'P_tgelflo10 ',p%P_tgelflo10)
        call EnvoyerMsgHistorique(logger, 'P_tgelflo90 ',p%P_tgelflo90)
      endif
! water
      call EnvoyerMsgHistorique(logger, 'P_h2ofeuilverte ',p%P_h2ofeuilverte)
      call EnvoyerMsgHistorique(logger, 'P_h2ofeuiljaune ',p%P_h2ofeuiljaune)
      call EnvoyerMsgHistorique(logger, 'P_h2otigestruc ',p%P_h2otigestruc)
      call EnvoyerMsgHistorique(logger, 'P_h2oreserve ',p%P_h2oreserve)
      call EnvoyerMsgHistorique(logger, 'P_h2ofrvert ',p%P_h2ofrvert)
      call EnvoyerMsgHistorique(logger, 'P_tempdeshyd ',p%P_tempdeshyd)
      if (p%P_codebeso == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5153)
        call EnvoyerMsgHistorique(logger, 'P_kmax ',p%P_kmax)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5154)
        call EnvoyerMsgHistorique(logger, 'P_rsmin ',p%P_rsmin)
      endif
      if (p%P_codeintercept == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5155)
        call EnvoyerMsgHistorique(logger, 'P_mouillabil ',p%P_mouillabil)
        call EnvoyerMsgHistorique(logger, 'P_stemflowmax ',p%P_stemflowmax)
        call EnvoyerMsgHistorique(logger, 'P_kstemflow ',p%P_kstemflow)
      endif
! Nitrogen
!    ! modif Bruno
      call EnvoyerMsgHistorique(logger, 'P_Vmax1 ',p% P_Vmax1)
      call EnvoyerMsgHistorique(logger, 'P_Kmabs1 ',p%P_Kmabs1)
!    ! fin modif
      call EnvoyerMsgHistorique(logger, 'P_Vmax2 ',p%P_Vmax2)
      call EnvoyerMsgHistorique(logger, 'P_Kmabs2 ',p%P_Kmabs2)
      call EnvoyerMsgHistorique(logger, 'P_adil ',p%P_adil)
      call EnvoyerMsgHistorique(logger, 'P_bdil ',p%P_bdil)
      call EnvoyerMsgHistorique(logger, 'P_masecNmax ',p%P_masecNmax)
      call EnvoyerMsgHistorique(logger, 'P_INNmin ',p%P_Innmin)
      call EnvoyerMsgHistorique(logger, 'P_INNimin ',p%P_Innimin)
      call EnvoyerMsgHistorique(logger, 'P_inngrain1 ',p%P_inngrain1)
      call EnvoyerMsgHistorique(logger, 'P_inngrain2 ',p%P_inngrain2)
      call EnvoyerMsgHistorique(logger, 'P_bdilmax ',p%P_bdilmax)
      if (p%P_codeplisoleN == 1) then
        call EnvoyerMsgHistorique(logger, MESSAGE_5156)
        call EnvoyerMsgHistorique(logger, 'P_adilmax ',p%P_adilmax)
      else
        call EnvoyerMsgHistorique(logger, MESSAGE_5157)
        call EnvoyerMsgHistorique(logger, 'P_Nmeta ',p%P_Nmeta)
        call EnvoyerMsgHistorique(logger, 'P_masecmeta ',p%P_masecmeta)
        call EnvoyerMsgHistorique(logger, 'P_Nreserve ',p%P_Nreserve)
      endif
      if (p%P_codelegume == 2) then
        call EnvoyerMsgHistorique(logger, MESSAGE_132)
        if (pg%P_codesymbiose == 2) then
          call EnvoyerMsgHistorique(logger, 'P_stlevdno ',p%P_stlevdno)
          call EnvoyerMsgHistorique(logger, 'P_stdnofno ',p%P_stdnofno)
          call EnvoyerMsgHistorique(logger, 'P_stfnofvino ',p%P_stfnofvino)
          call EnvoyerMsgHistorique(logger, 'P_vitno ',p%P_vitno)
          call EnvoyerMsgHistorique(logger, 'P_profnod ',p%P_profnod)
          call EnvoyerMsgHistorique(logger, 'P_concNnodseuil ',p%P_concNnodseuil)
          call EnvoyerMsgHistorique(logger, 'P_concNrac0 ',p%P_concNrac0)
          call EnvoyerMsgHistorique(logger, 'P_concNrac100 ',p%P_concNrac100)
          call EnvoyerMsgHistorique(logger, 'P_tempnod1 ',p%P_tempnod1)
          call EnvoyerMsgHistorique(logger, 'P_tempnod2 ',p%P_tempnod2)
          call EnvoyerMsgHistorique(logger, 'P_tempnod3 ',p%P_tempnod3)
          call EnvoyerMsgHistorique(logger, 'P_tempnod4 ',p%P_tempnod4)
          if (p%P_codefixpot == 1 ) then
           call EnvoyerMsgHistorique(logger, MESSAGE_5158)
           call EnvoyerMsgHistorique(logger, 'P_fixmax ',p%P_fixmax)
          else
           call EnvoyerMsgHistorique(logger, MESSAGE_5159)
           call EnvoyerMsgHistorique(logger, 'P_fixmaxveg ',p%P_fixmaxveg)
           call EnvoyerMsgHistorique(logger, 'P_fixmaxgr ',p%P_fixmaxgr)
          endif
        endif
      endif
! ahah
      if (p%P_codazofruit == 2) call EnvoyerMsgHistorique(logger, MESSAGE_232)
! BBCH
        if(p%P_stadebbchplt .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchplt',p%P_stadebbchplt)
        if(p%P_stadebbchger .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchger',p%P_stadebbchger)
        if(p%P_stadebbchlev .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchlev',p%P_stadebbchlev)
        if(p%P_stadebbchamf .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchamf',p%P_stadebbchamf)
        if(p%P_stadebbchlax .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchlax',p%P_stadebbchlax)
        if(p%P_stadebbchsen .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchsen',p%P_stadebbchsen)
        if(p%P_stadebbchflo .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchflo',p%P_stadebbchflo)
        if(p%P_stadebbchdrp .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchdrp',p%P_stadebbchdrp)
        if(p%P_stadebbchnou .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchnou',p%P_stadebbchnou)
        if(p%P_stadebbchdebdes .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchdebdes',p%P_stadebbchdebdes)
        if(p%P_stadebbchmat .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchmat',p%P_stadebbchmat)
        if(p%P_stadebbchrec .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchrec',p%P_stadebbchrec)
        if(p%P_stadebbchfindorm .ne. '-99') call  EnvoyerMsgHistorique(logger, 'stadebbchfindorm',p%P_stadebbchfindorm)

!cultivar parameters
      call EnvoyerMsgHistorique(logger, 'P_codevar ',p%P_codevar(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_stlevamf ',p%P_stlevamf(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_stamflax ',p%P_stamflax(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_stlevdrp ',p%P_stlevdrp(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_stflodrp ',p%P_stflodrp(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_stdrpdes ',p%P_stdrpdes(itk%P_variete))
      if (p%P_codebfroid /= 1) call EnvoyerMsgHistorique(logger, 'P_jvc ',p%P_jvc(itk%P_variete))
      if (p%P_codebfroid == 3) call EnvoyerMsgHistorique(logger, 'P_stdordebour ',p%P_stdordebour(itk%P_variete))
      if (p%P_codephot == 1) then
        call EnvoyerMsgHistorique(logger, 'P_sensiphot ',p%P_sensiphot(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_phobase ',p%P_phobase(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_phosat ',p%P_phosat(itk%P_variete))
      endif
      ! Leaves
      call EnvoyerMsgHistorique(logger, 'P_adens ',p%P_adens(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_bdens ',p%P_bdens(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_hautbase ',p%P_hautbase(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_hautmax ',p%P_hautmax(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_khaut', p%P_khaut(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_durvieF ',p%P_durvieF(itk%P_variete))
      if (p%P_codlainet == 1) then
        call EnvoyerMsgHistorique(logger, 'P_stlaxsen ',p%P_stlaxsen(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_stsenlan ',p%P_stsenlan(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_dlaimax ',p%P_dlaimax(itk%P_variete))
      else
        call EnvoyerMsgHistorique(logger, 'P_dlaimaxbrut ',p%P_dlaimaxbrut(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_innsen ',p%P_innsen(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_rapsenturg ',p%P_rapsenturg(itk%P_variete))
      endif
      ! Radiation interception
      if (p%P_codetransrad == 1)then
       call EnvoyerMsgHistorique(logger, MESSAGE_5121)
       call EnvoyerMsgHistorique(logger, 'P_extin ',p%P_extin(itk%P_variete))
      endif
      if (p%P_codetransrad == 2) then
        if (p%P_codebeso == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_127, p%P_extin(itk%P_variete))
        endif
        call EnvoyerMsgHistorique(logger, 'P_ktrou ',p%P_ktrou(itk%P_variete))
      endif
      ! Shoot biomass growth
      call EnvoyerMsgHistorique(logger, 'P_temin ',p%P_temin(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_teopt ',p%P_teopt(itk%P_variete))
      ! Biomass partitioning
      call EnvoyerMsgHistorique(logger, 'P_slamax ',p%P_slamax(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_tigefeuil ',p%P_tigefeuil(itk%P_variete))
      ! Yield formation
      call EnvoyerMsgHistorique(logger, 'P_pgrainmaxi ',p%P_pgrainmaxi(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_vitpropsucre ',p%P_vitpropsucre(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_vitprophuile ',p%P_vitprophuile(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_vitirazo ',p%P_vitirazo(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_deshydbase ',p%P_deshydbase(itk%P_variete))
      if (p%P_codeindetermin == 1) then
        call EnvoyerMsgHistorique(logger, 'P_nbjgrain ',p%P_nbjgrain(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_nbgrmin ',p%P_nbgrmin(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_nbgrmax ',p%P_nbgrmax(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_stdrpmat ',p%P_stdrpmat(itk%P_variete))
        if (p%P_codeir == 1) then
          call EnvoyerMsgHistorique(logger, 'P_vitircarb ',p%P_vitircarb(itk%P_variete))
        else
          call EnvoyerMsgHistorique(logger, 'P_vitircarbT ',p%P_vitircarbT(itk%P_variete))
        endif
      else
        call EnvoyerMsgHistorique(logger, 'P_afruitpot ',p%P_afruitpot(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_dureefruit ',p%P_dureefruit(itk%P_variete))
        call EnvoyerMsgHistorique(logger, 'P_stdrpnou ',p%P_stdrpnou(itk%P_variete))
        if (p%P_codcalinflo == 1) then
          call EnvoyerMsgHistorique(logger, MESSAGE_5141)
          call EnvoyerMsgHistorique(logger, 'P_nbinflo ',p%P_nbinflo(itk%P_variete))
        else
          call EnvoyerMsgHistorique(logger, MESSAGE_5142)
          call EnvoyerMsgHistorique(logger, 'P_inflomax ',p%P_inflomax(itk%P_variete))
          call EnvoyerMsgHistorique(logger, 'P_pentinflores ',p%P_pentinflores(itk%P_variete))
        endif
      endif
      ! Roots
      call EnvoyerMsgHistorique(logger, 'P_croirac ',p%P_croirac(itk%P_variete))
      ! Frost
      if(p%P_codgellev.eq.2) call EnvoyerMsgHistorique(logger, 'P_tgellev10 ',p%P_tgellev10)
      if(p%P_codgeljuv.eq.2) call EnvoyerMsgHistorique(logger, 'P_tgeljuv10 ',p%P_tgeljuv10)
      if(p%P_codgelveg.eq.2) call EnvoyerMsgHistorique(logger, 'P_tgelveg10 ',p%P_tgelveg10)
      ! Water stress
      call EnvoyerMsgHistorique(logger, 'P_psisto ',p%P_psisto(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_psiturg ',p%P_psiturg(itk%P_variete))
      call EnvoyerMsgHistorique(logger, 'P_swfacmin',p%P_swfacmin(itk%P_variete))


    ! domi - 03/02/04 - juste un message en attendant que nadine regarde
      if (p%P_codlainet == 2) call EnvoyerMsgHistorique(logger, MESSAGE_375)

    ! domi 03/11/05
    !DR 08/08/2018 c'est ma fete ...
      if (p%P_codelaitr.eq.1.and.(p%P_abscission > 1 .or. p%P_abscission < 0)) then
        call exit_error(logger, MESSAGE_378)
      endif

    ! ML et DR 12/02/08 pb d'incoherence de param
      if (p%P_codebfroid < 3) then
        p%P_codegdhdeb = 1
        call EnvoyerMsgHistorique(logger, MESSAGE_5120)
      endif

return
end subroutine Plante_Ecriture
end module Plante_Ecriture_m
 
