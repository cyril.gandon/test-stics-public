! write to the file history.sti
!!
!! general paramv6 parameters
module Ecriture_Transit_m
USE Stics
use messages
use messages_data
use plant_utils
implicit none
private
public :: Ecriture_Transit
contains
subroutine Ecriture_Transit(logger, t, codeplante1, codeplante2)
    type(logger_), intent(in) :: logger
    type(Stics_Transit_), intent(IN) :: t  
    !integer, intent(IN) :: nbplantes
    character(len=3), intent(IN) :: codeplante1
    character(len=3), optional, intent(IN) :: codeplante2

!TODO: revoir les parametres
    call EnvoyerMsgHistorique(logger, '   ')
    call EnvoyerMsgHistorique(logger, MESSAGE_264)
    call EnvoyerMsgHistorique(logger, '*********************************************')

!  Specificities of cut crops
!   if(codeplante1==CODE_FODDER)then
!      if(t%P_codetempfauche==1)then
!        call EnvoyerMsgHistorique(logger, 'P_codetempfauche in upvt',t%P_codetempfauche)
!      endif
!       if(t%P_codetempfauche==2)then
!        call EnvoyerMsgHistorique(logger, 'P_codetempfauche in udevair',t%P_codetempfauche)
!      endif
!      call EnvoyerMsgHistorique(logger, 'P_coefracoupe(1)',t%P_coefracoupe(1))
!   endif
!      if(codeplante2==CODE_FODDER)then
!      if(t%P_codetempfauche==1)then
!        call EnvoyerMsgHistorique(logger, 'P_codetempfauche in upvt',t%P_codetempfauche)
!      endif
!       if(t%P_codetempfauche==2)then
!        call EnvoyerMsgHistorique(logger, 'P_codetempfauche in udevair',t%P_codetempfauche)
!      endif
!      call EnvoyerMsgHistorique(logger, 'P_coefracoupe(2)',t%P_coefracoupe(2))
!   endif

!  Specificities of Quinoa
   if(codeplante1==CODE_QUINOA)then
      if(t%P_codepluiepoquet==1)then
       call EnvoyerMsgHistorique(logger,&
         'Option to replace rainfall by irrigation at hole depth in the case of hole sowing activated')
       call EnvoyerMsgHistorique(logger, 'P_codepluiepoquet',t%P_codepluiepoquet)
       call EnvoyerMsgHistorique(logger, 'P_nbjoursrrversirrig',t%P_nbjoursrrversirrig)
      endif
   endif
   
   if(present(codeplante2)) then
      if(codeplante2==CODE_QUINOA .and. t%P_codepluiepoquet==1) then
       call EnvoyerMsgHistorique(logger,&
         'Option to replace rainfall by irrigation at hole depth in the case of hole sowing activated')
       call EnvoyerMsgHistorique(logger, 'P_codepluiepoquet',t%P_codepluiepoquet)
       call EnvoyerMsgHistorique(logger, 'P_nbjoursrrversirrig',t%P_nbjoursrrversirrig)
      endif
   endif

!  Activation of the module simulating tiller dynamics
!    call EnvoyerMsgHistorique(logger, 'P_swfacmin',t%P_swfacmin)
!
!    if(t%P_codetranspitalle==1)then
!      call EnvoyerMsgHistorique(logger, 'Activation of the module simulating tiller dynamics ')
!      call EnvoyerMsgHistorique(logger, 'P_codetranspitalle',t%P_codetranspitalle)
!      if(t%P_codedyntalle(1)==1)then
!        call EnvoyerMsgHistorique(logger, 'P_codedyntalle(1)',t%P_codedyntalle(1))
!        call EnvoyerMsgHistorique(logger, 'P_SurfApex(1)',t%P_SurfApex(1))
!        call EnvoyerMsgHistorique(logger, 'P_SeuilMorTalle(1)',t%P_SeuilMorTalle(1))
!        call EnvoyerMsgHistorique(logger, 'P_SigmaDisTalle(1)',t%P_SigmaDisTalle(1))
!        call EnvoyerMsgHistorique(logger, 'P_VitReconsPeupl(1)',t%P_VitReconsPeupl(1))
!        call EnvoyerMsgHistorique(logger, 'P_SeuilReconsPeupl(1)',t%P_SeuilReconsPeupl(1))
!        call EnvoyerMsgHistorique(logger, 'P_MaxTalle(1)',t%P_MaxTalle(1))
!      endif
!      if(nbplantes.gt.1.and.t%P_codedyntalle(2)==1)then
!        call EnvoyerMsgHistorique(logger, 'P_codedyntalle(2)',t%P_codedyntalle(2))
!        call EnvoyerMsgHistorique(logger, 'P_SurfApex(2)',t%P_SurfApex(2))
!        call EnvoyerMsgHistorique(logger, 'P_SeuilMorTalle(2)',t%P_SeuilMorTalle(2))
!        call EnvoyerMsgHistorique(logger, 'P_SigmaDisTalle(2)',t%P_SigmaDisTalle(2))
!        call EnvoyerMsgHistorique(logger, 'P_VitReconsPeupl(2)',t%P_VitReconsPeupl(2))
!        call EnvoyerMsgHistorique(logger, 'P_SeuilReconsPeupl(2)',t%P_SeuilReconsPeupl(2))
!        call EnvoyerMsgHistorique(logger, 'P_MaxTalle(2)',t%P_MaxTalle(2))
!      endif
!    endif

!   Calculation of the stem elongation stage for perenial grasslands, code to stop the reserve limitation from the stem elongation
!    if(t%P_codemontaison(1)==1)then
!       call EnvoyerMsgHistorique(logger, 'code to stop the reserve limitation from the stem elongation for perenial grasslands activated')
!       call EnvoyerMsgHistorique(logger, 'P_codemontaison',t%P_codemontaison(1))
!    endif
!    if(nbplantes.gt.1.and.t%P_codemontaison(2)==1)then
!       call EnvoyerMsgHistorique(logger, 'plant 2,code to stop the reserve limitation from the stem elongation &
!       & for perenial grasslands activated')
!       call EnvoyerMsgHistorique(logger, 'P_codemontaison',t%P_codemontaison(1))
!    endif

!   Calculation of the maximal reserve compartment during reproductive stages
!    call EnvoyerMsgHistorique(logger, 'P_resplmax',t%P_resplmax(1))
!    if(nbplantes.gt.1)then
!           call EnvoyerMsgHistorique(logger, 'P_resplmax plant 2',t%P_resplmax(2))
!    endif


!  Moisture test for sowing decision
!    call EnvoyerMsgHistorique(logger, 'P_nbj_pr_apres_semis',t%P_nbj_pr_apres_semis)
!    call EnvoyerMsgHistorique(logger, 'P_eau_mini_decisemis',t%P_eau_mini_decisemis)
!    call EnvoyerMsgHistorique(logger, 'P_humirac_decisemis',t%P_humirac_decisemis)
! fertilisation driving
    if(t%P_codecalferti==1)then
       call EnvoyerMsgHistorique(logger, 'Automatic calculation of fertilisation requirements activated')
       call EnvoyerMsgHistorique(logger, 'P_ratiolN',t%P_ratiolN)
       call EnvoyerMsgHistorique(logger, 'P_dosimxN',t%P_dosimxN)
       if(t%P_codetesthumN==1)then
           call EnvoyerMsgHistorique(logger, ' option of soil moisture test : minimum rainfall threshold')
       endif
       if(t%P_codetesthumN==2)then
           call EnvoyerMsgHistorique(logger, ' option of soil moisture test : soil moisture threshold')
       endif
     endif

!  Residues decomposition
     if(t%P_codeNmindec==1)then
       call EnvoyerMsgHistorique(logger, 'Limitation of N availability for residues decomposition in soil activated')
       call EnvoyerMsgHistorique(logger, 'P_rapNmindec',t%P_rapNmindec)
       call EnvoyerMsgHistorique(logger, 'P_fNmindecmin',t%P_fNmindecmin)
     endif

! coupling with pathogen models
     if(t%P_codetrosee==1)then
       call EnvoyerMsgHistorique(logger, 'calculation of hourly dew temperature : linear interpolation(actual calculation)')
     endif
     if(t%P_codetrosee==2)then
       call EnvoyerMsgHistorique(logger,&
         'calculation of hourly dew temperature : sinusoidal interpolation (Debele Bekele et al.,2007)')
     endif
     if(t%P_codeSWDRH==1)then
       call EnvoyerMsgHistorique(logger, 'calculation of surface wetness duration activated')
     endif

!  calculation of the root death at cutting date for grasslands
!   if(codeplante1==CODE_FODDER)then
!      if(t%P_codemortalracine==1)then
!         call EnvoyerMsgHistorique(logger, 'dry matter is calculated with masec')
!      endif
!      if(t%P_codemortalracine==2)then
!         call EnvoyerMsgHistorique(logger, 'dry matter is calculated with masectot')
!      endif
!   endif

!  option for several thinning
!   if(t%P_option_thinning==1)then
!       call EnvoyerMsgHistorique(logger, 'activation of several thinning available in the tec file')
!   endif

!  option for several fertilizer type
!   if(t%P_option_engrais_multiple==1)then
!       call EnvoyerMsgHistorique(logger, 'activation of several fertilizer type available in the tec file')
!   endif

!  option for pasture
! DR 10/11/2016 attention il faudra corriger le nom dans le code
   if(t%P_option_pature==1)then
       call EnvoyerMsgHistorique(logger, 'activation of pasture available in the tec file')
       call EnvoyerMsgHistorique(logger, 'P_coderes_pature',t%P_coderes_pature)
       call EnvoyerMsgHistorique(logger, 'P_pertes_restit_ext',t%P_pertes_restit_ext)
       call EnvoyerMsgHistorique(logger, 'P_Crespc_pature',t%P_Crespc_pature)
       call EnvoyerMsgHistorique(logger, 'P_Nminres_pature',t%P_Nminres_pature)
       call EnvoyerMsgHistorique(logger, 'P_eaures_pature',t%P_eaures_pature)
       call EnvoyerMsgHistorique(logger, 'P_coef_calcul_qres',t%P_coef_calcul_qres)
       call EnvoyerMsgHistorique(logger, 'P_engrais_pature',t%P_engrais_pature)
       call EnvoyerMsgHistorique(logger, 'P_coef_calcul_doseN',t%P_coef_calcul_doseN)
   endif


! DR 10/11/2016 on gele le formalisme en attente de verification par joel et Bruno
!    call EnvoyerMsgHistorique(logger, 'P_code_adapt_MO_CC',t%P_code_adapt_MO_CC)
!    call EnvoyerMsgHistorique(logger, 'P_periode_adapt_CC',t%P_periode_adapt_CC)
!    call EnvoyerMsgHistorique(logger, 'P_an_debut_serie_histo',t%P_an_debut_serie_histo)
!    call EnvoyerMsgHistorique(logger, 'P_an_fin_serie_histo',t%P_an_fin_serie_histo)
!    call EnvoyerMsgHistorique(logger, 'P_param_tmoy_histo',t%P_param_tmoy_histo)
!    call EnvoyerMsgHistorique(logger, 'P_code_adaptCC_miner',t%P_code_adaptCC_miner)
!    call EnvoyerMsgHistorique(logger, 'P_code_adaptCC_nit',t%P_code_adaptCC_nit)
!    call EnvoyerMsgHistorique(logger, 'P_code_adaptCC_denit',t%P_code_adaptCC_denit)
!    call EnvoyerMsgHistorique(logger, 'P_TREFdenit1',t%P_TREFdenit1)
!    call EnvoyerMsgHistorique(logger, 'P_TREFdenit2',t%P_TREFdenit2)

!   call EnvoyerMsgHistorique(logger, 'maperenne0 (t/ha)',t%P_maperenne0)
!   call EnvoyerMsgHistorique(logger, 'QNperenne0 (kg N/ha)',t%P_QNperenne0)
!   call EnvoyerMsgHistorique(logger, 'QNrestemp0 (kg N/ha)',t%P_QNrestemp0 )
!   call EnvoyerMsgHistorique(logger, 'msrac0 (t/ha)',t%P_msrac0)
!   call EnvoyerMsgHistorique(logger, 'QNrac0 (kg N/ha)',t%P_QNrac0)
!   if (t%P_code_acti_reserve == 1) then
!     call EnvoyerMsgHistorique(logger, 'PropresP',t%P_PropresP)
!     call EnvoyerMsgHistorique(logger, 'PropresPN',t%P_PropresPN)
!     call EnvoyerMsgHistorique(logger, 'Efremobil',t%P_Efremobil)
!     call EnvoyerMsgHistorique(logger, 'Propres',t%P_Propres)
!     call EnvoyerMsgHistorique(logger, 'tauxmortresP (d-1)',t%P_tauxmortresP)
!     call EnvoyerMsgHistorique(logger, 'Parazoper',t%P_Parazoper)
!     call EnvoyerMsgHistorique(logger, 'ParazoTmorte',t%P_ParazoTmorte)
!     call EnvoyerMsgHistorique(logger, 'Stubblevegratio',t%P_Stubblevegratio)
!     call EnvoyerMsgHistorique(logger, 'inilai',t%P_inilai)
!   endif
!   if (t%P_code_GDH_Wang == 1) then
!      call EnvoyerMsgHistorique(logger, 'tdoptdeb',t%P_tdoptdeb)
!   endif
!   if (t%P_code_hautfauche_dyn == 1) then
!      call EnvoyerMsgHistorique(logger, 'Hautfauche',t%P_Hautfauche)
!   endif
! DR 18/06/2021 le codeFinert est a virer car Les 2 tests de function ne sont pas valid�s
! DR 23/08/2019 je mets les codes comme dans la branche mineralisation p�ur conparaison avec trunk
!   if(t%P_codeFinert == 1) call EnvoyerMsgHistorique(logger, 'finert reading in param_gen.xml')
!   if(t%P_codeFinert == 2.and.t%P_codeFunctionFinert == 1) then
!       call EnvoyerMsgHistorique(logger, 'inert fraction calculated with SOC0 (Fonction AMG with original)')
!       call EnvoyerMsgHistorique(logger, 'Finert1',t%P_Finert1)
!       call EnvoyerMsgHistorique(logger, 'Finert2',t%P_Finert2)
!   endif
!   if(t%P_codeFinert == 2.and.t%P_codeFunctionFinert == 2) then
!       call EnvoyerMsgHistorique(logger, 'inert fraction calculated with tCorg (Fonction AMG with Corg content(g/kg))')
!       call EnvoyerMsgHistorique(logger, 'Finert1',t%P_Finert1)
!       call EnvoyerMsgHistorique(logger, 'Finert2',t%P_Finert2)
!   endif

   call EnvoyerMsgHistorique(logger, 'P_humirac',t%P_humirac)

! PL, 12/04/2022: option inutile maintenant
!   if (t%P_code_depth_mixed_humus == 1)then
!       call EnvoyerMsgHistorique(logger, 'depth of humus mixed : itrav1 to itrav2')
!   else
!       call EnvoyerMsgHistorique(logger, 'depth of humus mixed : 1 to itrav2')
!   endif
! DR 11/04/2022 on commente le calcul facon 9.1 qui ne sera plus utilis�
!   if (t%P_code_stock_BM == 1)then
!       call EnvoyerMsgHistorique(logger, 'initialisation of N stock = v9. ')
!   else
!       call EnvoyerMsgHistorique(logger, 'initialisation of N stock = v perenne.')
!   endif



 ! DR 12/06/2019 ajout de l'option to mix the humus on the depth itrav1-itrav2 (yes=1) or on the depth 1-itrav2 (no=2) // 1,2

return
end
end module Ecriture_Transit_m
 
