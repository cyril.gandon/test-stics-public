! Module Messages
!! Description :
!! management messages to history and debug files
module messages
   use stdlib_strings, only: to_string
   use call_num_version_m
   use stics_system

   implicit none

   ! Standard output is often pre-connected to 6.
   integer, parameter :: DEFAULT_OUTPUT_UNIT = 6

   type :: logger_
      integer :: history_unit = DEFAULT_OUTPUT_UNIT
      integer, private :: debug_unit = DEFAULT_OUTPUT_UNIT
      integer, private :: errors_unit = DEFAULT_OUTPUT_UNIT

      logical, private :: write_histo = .true.
      logical, private :: write_ecran = .true.
      logical, private :: write_debug = .true.

      character(len=:), allocatable :: output_path

   end type

   interface EnvoyerMsgHistorique
      module procedure cc_EnvoyerMsgHistorique, cc_r_EnvoyerMsgHistorique, &
         cc_i_EnvoyerMsgHistorique, cc_c_EnvoyerMsgHistorique, &
         cc_t_r_EnvoyerMsgHistorique
   end interface EnvoyerMsgHistorique

contains

   type(logger_) pure function logger_ctor &
      (write_histo, write_ecran, write_debug, history_unit, debug_unit, errors_unit, output_path) result(logger)
      integer, intent(in) :: history_unit, debug_unit, errors_unit
      logical, intent(in) :: write_histo, write_ecran, write_debug
      character(*), intent(in) :: output_path

      logger%history_unit = history_unit
      logger%debug_unit = debug_unit
      logger%errors_unit = errors_unit
      logger%write_histo = write_histo
      logger%write_ecran = write_ecran
      logger%write_debug = write_debug
      logger%output_path = output_path
   end function

   type(logger_) function open_logger &
      (write_histo, write_ecran, write_debug, errors_file, history_file, debug_file, output_path) result(logger)
      logical, intent(in) :: write_histo, write_ecran, write_debug
      character(*), intent(in) :: errors_file, history_file, debug_file, output_path

      character(:), allocatable :: errors_path, history_path, debug_path
      integer :: debug_unit, debug2_unit, history_unit, errors_unit
      integer :: iostat

      errors_path = join_path(output_path, errors_file)
      open (newunit=errors_unit, file=errors_path, status=unknown, action=write_, iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error(logger, 'Error opening errors_path path=['//errors_path//'], iostat='//to_string(iostat))
      end if
      call write_file_header(errors_unit, "errors")
      if (write_ecran) call EnvoyerMsgEcran(logger, "Opening errors file ["//errors_path//"]")

      if (write_histo) then
         history_path = join_path(output_path, history_file)
         open (newunit=history_unit, file=history_path, status=unknown, action=write_)
         call write_file_header(history_unit, "history")

         if (write_ecran) call EnvoyerMsgEcran(logger, "Opening history file ["//history_path//"]")
      end if

      if (write_debug) then
         debug_path = join_path(output_path, debug_file)
         open (newunit=debug_unit, file=debug_path, status=unknown, action=write_, iostat=iostat)
         call write_file_header(debug_unit, "debug")
         if (write_ecran) call EnvoyerMsgEcran(logger, "Opening debug file ["//debug_path//"]")
      end if

      logger = logger_ctor(write_histo, write_ecran, write_debug, history_unit, debug_unit, errors_unit, output_path)
   end function

   ! writing files footers and/or closing files
   subroutine close_logger(logger)
      type(logger_), intent(in) :: logger

      if (logger%write_histo) then
         call write_file_footer(logger%history_unit, "history")
         close (logger%history_unit)
      end if

      call write_file_footer(logger%errors_unit, "errors")
      close (logger%errors_unit)

      if (logger%write_debug) then
         call write_file_footer(logger%debug_unit, "debug")
         close (logger%debug_unit)
      end if
   end subroutine

   subroutine cc_EnvoyerMsgHistorique(logger, message)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message

      if (.not. logger%write_histo) return

      write (logger%history_unit, *) message

   end subroutine

   subroutine cc_r_EnvoyerMsgHistorique(logger, message, valeur)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      real, intent(in) :: valeur

      if (.not. logger%write_histo) return

      write (logger%history_unit, *) message, valeur

   end subroutine

   subroutine cc_t_r_EnvoyerMsgHistorique(logger, message, valeur)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      real, intent(in) :: valeur(:)

      if (.not. logger%write_histo) return

      write (logger%history_unit, *) message, valeur

   end subroutine

   subroutine cc_i_EnvoyerMsgHistorique(logger, message, valeur)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      integer, intent(in) :: valeur

      if (.not. logger%write_histo) return

      write (logger%history_unit, *) message, valeur

   end subroutine

   subroutine cc_c_EnvoyerMsgHistorique(logger, message, valeur)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      character(len=*), intent(in) :: valeur

      if (.not. logger%write_histo) return

      write (logger%history_unit, *) message, trim(valeur)

   end subroutine

   subroutine EnvoyerMsgEcran(logger, message)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      if (logger%write_ecran) then
         write (*, *) message
      end if
   end subroutine

   subroutine EnvoyerMsgErreur(logger, message)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      write (logger%errors_unit, *) message
   end subroutine

   subroutine EnvoyerMsgDebug(logger, message)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      if (logger%write_debug) then
         write (logger%debug_unit, *) message
      end if
   end subroutine

   subroutine EnvoyerMsg(logger, message)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message
      call EnvoyerMsgErreur(logger, message)
      call EnvoyerMsgEcran(logger, message)
      call EnvoyerMsgHistorique(logger, message)
   end subroutine

   function get_timestamp() result(time_string)
      character(:), allocatable :: time_string
      character(10) :: time
      integer :: value(8)
      character(len=8) :: date

      call date_and_time(DATE=date, TIME=time, VALUES=value)
      ! date heure ...
      time_string = date(1:4)//"/"//date(5:6)//"/"//date(7:8)//"-"//time(1:2)//":"//time(3:4)//":"//time(5:6)
   end function

! writing a file header
   subroutine write_file_header(file_unit, file_kind)
      integer, intent(in) :: file_unit
      character(len=*), intent(in) :: file_kind

      character(len=50) :: codeversion
      character(:), allocatable :: date_and_time

      ! getting date time string
      date_and_time = get_timestamp()
      call call_num_version(codeversion)

      write (file_unit, *) "-----------------------------------------------------------------------------------------"
      write (file_unit, *) "Stics "//trim(file_kind)//" file Creation date: "//date_and_time
      write (file_unit, *) "STICS Version: "//codeversion
      write (file_unit, *) "-----------------------------------------------------------------------------------------"
      write (file_unit, *) ""
      write (file_unit, *) ""
   end subroutine

! writing a file footer
   subroutine write_file_footer(file_unit, file_kind)
      integer, intent(in) :: file_unit
      character(len=*), intent(in) :: file_kind
      character(:), allocatable :: date_and_time

      date_and_time = get_timestamp()

      write (file_unit, *) "-----------------------------------------------------------------------------------------"
      write (file_unit, *) "Stics "//trim(file_kind)//" file end End date: "//date_and_time
      write (file_unit, *) "-----------------------------------------------------------------------------------------"
   end subroutine

   !> Returns a silent logger, for tests purpose
   type(logger_) function get_silent_logger() result(logger)
      logger = logger_ctor( &
               write_histo=.false., &
               write_ecran=.false., &
               write_debug=.false., &
               history_unit=0, &
               debug_unit=0, &
               errors_unit=0, &
               output_path="")

   end function

   !> Send a message in log files and exit 9 
   subroutine exit_error(logger, message)
      type(logger_), intent(in) :: logger
      character(len=*), intent(in) :: message

      call EnvoyerMsg(logger, message)
      call exit(9)
   end subroutine
end module
