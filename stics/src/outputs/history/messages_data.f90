module messages_data
implicit none

character(*), parameter :: TOO_MANY_PLANTS = 'P_nbplantes in USM is greater than the plant max number'
character(*), parameter :: SOIL_FILE_ERROR = 'Error in soil file, missing lines'
character(*), parameter :: MISSING_CLIMATE_VALUE = 'Warning: the model run but one of the climate value of the day is missing:'

character(*), parameter :: IRRIGATION_START_STAGE_ERROR = &
'Error in tec file: irrigation start stage unknown (stage_start_irrigauto) '
character(*), parameter :: IRRIGATION_END_STAGE_ERROR = &
'Error in tec file: irrigation end stage unknown (stage_end_irrigauto)'

character(*), parameter :: OPTION_1_MISSING = &
'!!!!!!! this option for the first choice contains a missing parameter value=-999 :'
character(*), parameter :: OPTION_2_MISSING = &
'!!!!!!! this option for the second choice contains a missing parameter value=-999 :'
character(*), parameter :: OPTION_3_MISSING = &
'!!!!!!! this option for the third choice contains a missing parameter value=-999 :'

character(*), parameter :: OPTION_112 = 'Give a suitable value to sla (between 150 et 600)'
character(*), parameter :: OPTION_116 = 'hautmax < hautbase !'
character(*), parameter :: OPTION_1118 = '!!!!!!! give a suitable values for the parameters tdmin and tdmax'
character(*), parameter :: OPTION_1119 = '!!!!!!! give a suitable values for the parameters tdmindeb and tdmaxdeb'
character(*), parameter :: OPTION_1120 = '!!!!!!! give a suitable values for the parameters tcmin and tcmax'
character(*), parameter :: OPTION_1121 = '!!!!!!! give a suitable values for the parameters temin and temax'
character(*), parameter :: OPTION_1122 = '!!!!!!! give a suitable values for the parameters nlevlim1 et nlevlim2'
character(*), parameter :: OPTION_1123 = '!!!!!!! give a suitable values for the parameters dltamsminsen and dltamsmaxsen'
character(*), parameter :: OPTION_1124 = '!!!!!!! give a suitable values for the parameters tminremp et tmaxremp'
character(*), parameter :: OPTION_1125 = '!!!!!!! give a suitable values for the parameters  minazorac maxazorac'
character(*), parameter :: OPTION_1126 = '!!!!!!! give a suitable values for the parameters repracpermin repracpermax'
character(*), parameter :: OPTION_1127 = '!!!!!!! give a suitable values for the parameters repracseumin repracseumax'
character(*), parameter :: OPTION_1128 = '!!!!!!! give a suitable values for the parameters tgellev10 et tgellev90'
character(*), parameter :: OPTION_1129 = '!!!!!!! give a suitable values for the parameters tgeljuv10 et tgeljuv90'
character(*), parameter :: OPTION_1130 = '!!!!!!! give a suitable values for the parameters tgelveg10 et tgelveg90'
character(*), parameter :: OPTION_1131 = '!!!!!!! give a suitable values for the parameters tgelflo10 et tgelflo90'

character(*), parameter :: SOIL_PROFIL_DATES_PROBLEM = &
'problem with your profile output dates --> they have been recalculated'
character(*), parameter :: SOIL_PROFIL_YEAR_ERROR = &
'A date for the profile output is bad and does not correspond to the simulation year, the file &
&mod_profil.sti will not be complete, correct the date !! '
character(*), parameter :: SOIL_PROFIL_READ_ERROR = 'problem with your profile configuration , correct it !!'

character(*), parameter :: SOIL_DEPTH_WARNING = 'soil depth > impermeable !'
character(*), parameter :: SOIL_DEPTH_ERROR = 'soil depth > impermeable ! &
&if artificial drainage activated (macroporosite must be desactivated and =infil not equal  0)'

character(*), parameter :: SOIL_DRAIN_ERROR = &
'codrainage = 1 implies macroporosite error mentioned above forced the stop program'
character(*), parameter :: SOIL_DRAIN_DISTANCE_ERROR = 'distdrain > ecartdrain/2'

character(*), parameter :: INTERCROP_STATION_ETP_MUST_BE_SW = &
'Error: for mixed crops --> Shuttleworth and Wallace is obligatory for Evapotranspiration model &
& (see codeetp in station file)'

character(*), parameter :: INTERCROP_TRANSRAD_ERROR = &
'Error : the dominant plant must use the radiation transfer. But codetradtec = 2, meaning&
& that the parameters interrang and orientrang have not been given values. To run the simulation, you must set&
& codetradtec to 1 and give at least one value to interrang > 0.'

character(*), parameter :: MISSING_TRANSRAD_PARAMS_ERROR = ' no parameters given for radiation transfer'

character(*), parameter :: MISSING_ETP_SW_PARAMS_ERROR = &
'In the station file you activated the Shuttleworth and Wallace &
&calculation option of the Potential EvapoTranspiration.'&
'Thus, you have to activate the resistive approach for water requirements calculation in your plant file &
&(codebeso should be 2)'

character(*), parameter :: ETP_CODEBESO_1_WARNING = &
'In the station file you have not activated the Shuttleworth and Wallace &
&calculation option of the Potential EvapoTranspiration.'&
'Thus, you have to choose the crop coefficient approach for water requirements calculation in your plant file &
&(codebeso should be 1)'

! dans bilan voir si on le mets dans decisionrecolte !   messages (447) = 'harvest day was delayed of this number of days : '

! General
!---------
!! merge trunk 23/11/2020
character(*), parameter :: MESSAGE_3 = ' '
character(*), parameter :: MESSAGE_4 = '!!!!!!!!!!!!!!!!!!! ---------------- WARNING --------------- !!!!!!!!!!!!!!!!!!! '
character(*), parameter :: MESSAGE_5 = '-------------------------------------------------------------------------------'
character(*), parameter :: MESSAGE_6 = '!!!!!!!!!!!!!!!!!!! ---- WARNING the model stop ---- !!!!!!!!!!!!!!!!!!!!!!!!!!'

! croira
! ------
character(*), parameter :: MESSAGE_20 = 'transpiration nil because root system is in dry soil, day'
character(*), parameter :: MESSAGE_23 = 'profsem > initial rooting depth !'
character(*), parameter :: MESSAGE_24 = &
'inconsistency in initialising rooting front, you must have densinitial no deeper as zrac0 &
&it will be limited to the layer where the roots are presents'
character(*), parameter :: MESSAGE_10 = 'inconsistency in initialising densitnitial and zrac0,  &
&zrac will be limited to the layer where the roots are presents'
! densirac
! --------
character(*), parameter :: MESSAGE_30 = 'the crop cannot begin growing : soil surface too dry'
character(*), parameter :: MESSAGE_31 = 'attention: there are no longer any effective roots, on day :'

! develop
! --------
! ML le 28/05/04
character(*), parameter :: MESSAGE_32 = &
'there is an inconsistency in the initialisations :the simulation begins at the end of &
&vernalisation (stage0=lev) but at a date after julvernal!'
character(*), parameter :: MESSAGE_41 = 'lax observed before amf'
character(*), parameter :: MESSAGE_42 = 'sen observed before lax'
character(*), parameter :: MESSAGE_43 = 'lan observed before sen'
character(*), parameter :: MESSAGE_44 = 'mat observed before drp. ndrp = '
character(*), parameter :: MESSAGE_45 = 'rec observed before mat'
character(*), parameter :: MESSAGE_46 = 'amf observed before emergence'
character(*), parameter :: MESSAGE_47 = 'drp before flo'

! Bruno
character(*), parameter :: MESSAGE_49 = 'if option hourly scale, then air temperature for driving development'
character(*), parameter :: MESSAGE_50 = &
'there is an inconsistency in the initialisations: simulation begins at the start of vernalisation &
&(stage0=dor) but at a date before julvernal!'

! grain
! -----
character(*), parameter :: MESSAGE_51 = 'the plant has not emerged'
! Ajout Florent C. et Fabien octobre 2019
character(*), parameter :: MESSAGE_52 = 'inconsistency in the plant file : you can not use simultaneously &
&"rootdeposition activation" = yes and "root density" = "standard profile" '

! iniclim
! -------
character(*), parameter :: MESSAGE_61 = 'attention: start of simulation after sowing : risk of error in output dates'
!    messages (62) = ' : risk of error in output dates'
character(*), parameter :: MESSAGE_63 = 'incomplete weather file : impossible chaining'
character(*), parameter :: MESSAGE_64 = 'the start of the simulation is earlier than the beginning of the weather file'
character(*), parameter :: MESSAGE_65 = '!! your irrigations :'
character(*), parameter :: MESSAGE_66 = 'are < the start of the simulation :'
character(*), parameter :: MESSAGE_68 = ' correct this date and start again. the pg is interrupted; see history.sti'
character(*), parameter :: MESSAGE_69 = '!! Your nitrogen applications :'
character(*), parameter :: MESSAGE_60 = &
'The simulation runs but the day of incorporation of residues is later than the sowing date.'
! DR 13/08/2012 je rajoute ce message recupere dans message_fr avant son effacement
character(*), parameter :: MESSAGE_260 = &
'The simulation runs but the day of soil cultivation operations is later than the sowing date.'
character(*), parameter :: MESSAGE_71 = 'not the weather variables needed for the resistive model option'
character(*), parameter :: MESSAGE_72 = 'not the reference PET values : calculation of Priestley-Taylor PET'
character(*), parameter :: MESSAGE_73 = 'amf observed and vernalisation impossible so stlevamf=0 and execution error'
!    messages (74) = 'stlevamf=0 and execution error '
character(*), parameter :: MESSAGE_70 = &
'attention: the simulation begins with emergence, vernalisation also begins with emergence and &
&not with germination, as is the case for simulations beginning with sowing; you may be able to reduce jvcmini &
&to take account of this shortage of vernalising day'

character(*), parameter :: MESSAGE_76 = &
'you cannot start at the dor stage (beginning of dormancy) if your crop has no cold requirements'

! inicoupe
! --------
character(*), parameter :: MESSAGE_75 = 'lai < lairesiduel : cut is delayed'

! initial
! -------
character(*), parameter :: MESSAGE_80 = 'obstarac too big : obstarac = soil depth'
character(*), parameter :: MESSAGE_82 = 'proftrav > soil depth'
character(*), parameter :: MESSAGE_83 = 'initial stage not recorded: '
character(*), parameter :: MESSAGE_84 = 'inconsistency in initialisations'
character(*), parameter :: MESSAGE_85 = 'QNplantenp0 = 0 then critical value'
character(*), parameter :: MESSAGE_86 = 'INITIALISATIONS PLANT'
character(*), parameter :: MESSAGE_87 = 'inconsistency between stade0 et magrain0'
character(*), parameter :: MESSAGE_88 = 'Depth of measurement (profmes) > soil depth (profsol). Then profmes = profsol ='
character(*), parameter :: MESSAGE_91 = ' P_lairesiduel >0 et P_msresiduel <= 0'
character(*), parameter :: MESSAGE_179 = 'soil depth = '
character(*), parameter :: MESSAGE_180 = '                     > max number of layers = '
character(*), parameter :: MESSAGE_182 = 'zesx > profsol then zesx=profsol '
character(*), parameter :: MESSAGE_171 = 'profhum > soil depth --> profhum = profsol'
character(*), parameter :: MESSAGE_172 = 'linking [or sequence?]of simulations impossible : wrong starting and finishing dates'
character(*), parameter :: MESSAGE_173 = 'Attention : a previous incomplete weather file defines the end of the current simulation'
character(*), parameter :: MESSAGE_191 = 'Attention you have chosen "rate of ground cover" option'
character(*), parameter :: MESSAGE_192 = 'water interception by foliage is not allowed'
character(*), parameter :: MESSAGE_193 = 'water requirement calculated with crop coefficient'
character(*), parameter :: MESSAGE_194 = 'the crop cannot be mowed'
character(*), parameter :: MESSAGE_195 = 'calculation of crop temperature  is forced to empirical relation'
character(*), parameter :: MESSAGE_196 = 'the perennial plant must have roots (densinitial in usm)'
character(*), parameter :: MESSAGE_198 = 'epd (dispersion thickness, soil file) fixed at 1 cm'
character(*), parameter :: MESSAGE_333 = 'impossible to simulate a staggered harvest with the option chosen (nbceuille=2) '
character(*), parameter :: MESSAGE_334 = 'no fruit removal with determined plants'
character(*), parameter :: MESSAGE_335 = 'end of dormancy forced before the beginning of simulation'
character(*), parameter :: MESSAGE_336 = 'Attention humcapil  > hcc : risk of excess water'
character(*), parameter :: MESSAGE_337 = ' splaimin=splaimax or spfrmin=spfrmax'
character(*), parameter :: MESSAGE_338 = 'stade0 = snu or plt impossible with a perennial plant, put stade0 = dor for before &
&dormancy/vernalisation  and put stade0 = lev for bud burst or beginning of leaf growth'
character(*), parameter :: MESSAGE_339 = 'Attention no nitrogen reserve'
character(*), parameter :: MESSAGE_340 = 'problem codeinitprec=2 and no link [or sequence?]'
character(*), parameter :: MESSAGE_341 = 'Attention : You start with a bare soil therefore your plant initialisations have been&
& set to 0.0 (state of plant at start)'
character(*), parameter :: MESSAGE_342 = &
'Attention if start at plt stage you must initialise the plant by (laiplantule and nbfeuilplant&
& in the parameters in the plant file)'
character(*), parameter :: MESSAGE_520 = 'problem with smoothing of water profile'

! irrig
! -----
character(*), parameter :: MESSAGE_90 = 'ATTENTION: neither rain nor irrigation at sowing'

! leclai
! ------
character(*), parameter :: MESSAGE_100 = '!!! attention !!!'
character(*), parameter :: MESSAGE_101 = 'the observed lai file is not consecutive'

! lecparam
! --------
character(*), parameter :: MESSAGE_104 = 'GENERAL PARAMETERS USEFUL FOR THE SIMULATION'
character(*), parameter :: MESSAGE_105 = 'error in file PARAM.PAR'
character(*), parameter :: MESSAGE_437 = 'STICS Version '

! lecparamv6 DR et ML le 23/03/04
! ----------
character(*), parameter :: MESSAGE_405 = 'error in file PARAMV6.PAR'
character(*), parameter :: MESSAGE_406 = 'I cannot find the file PARAMV6.PAR'
character(*), parameter :: MESSAGE_407 = 'Attention. It seems that your file paramv6_gen.xml does not correspond to the version of&
& stics which you are using.For your information, below is a list of the non-initialised variables. As a &
&precaution the programme has been stopped. If you have questions please contact stics@avignon.inra.fr'
!   character(*), parameter :: MESSAGE_408 = 'when coupling Stics with the pathogen model Mila , the calculation of surface wetness &
!                    &duration is automatically activated'
character(*), parameter :: MESSAGE_408 = &
'Attention the dates of beginning and ending irrigations in case of calendar imposed are bad : begin>end'

! LECPLANT  NB le 08/01/02
! --------
!   messages (110) = 'impossible to have 2 consecutive stages simultaneously'
character(*), parameter :: MESSAGE_438 = 'In CAS simulation mode, the codebeso has to be set to 2. It is forced.'
character(*), parameter :: MESSAGE_113 = 'rsmin strange !'
character(*), parameter :: MESSAGE_114 = 'hautmax strange !'
character(*), parameter :: MESSAGE_115 = 'hautbase strange !'
character(*), parameter :: MESSAGE_116 = 'hautmax < hautbase !'
character(*), parameter :: MESSAGE_117 = 'inconsistency in profile type'
character(*), parameter :: MESSAGE_118 = 'h2ogrmat > h2ograin !'
character(*), parameter :: MESSAGE_119 = 'low draclong : risk of execution problems'
character(*), parameter :: MESSAGE_121 = 'PLANT PARAMETERS NEEDED FOR THE SIMULATION'
character(*), parameter :: MESSAGE_122 = 'driving temperature, air '
character(*), parameter :: MESSAGE_123 = 'driving temperature, crop'
character(*), parameter :: MESSAGE_124 = 'delay water stress before DRP stage '
character(*), parameter :: MESSAGE_125 = 'temin=tcmin and temax=tcmax'
character(*), parameter :: MESSAGE_126 = 'inconsistency between codetransrad and codetradtec'
character(*), parameter :: MESSAGE_127 = 'extin used for Esol'
character(*), parameter :: MESSAGE_128 = 'root driving temperature = crop'
character(*), parameter :: MESSAGE_129 = 'root driving temperature = soil'
character(*), parameter :: MESSAGE_130 = 'impossible to simulate dormancy for annual crops'
character(*), parameter :: MESSAGE_131 = &
'inconsistency between chosen initial stage, stade0=dor, and the fact that this crop has no cold requirement'
character(*), parameter :: MESSAGE_132 = 'legume'
character(*), parameter :: MESSAGE_232 = 'nitrogen effect on nb fruit and grain'
character(*), parameter :: MESSAGE_133 = 'error in plant file. '
character(*), parameter :: MESSAGE_370 = 'number of grains maxi (Nbgrmax) = 0'
character(*), parameter :: MESSAGE_371 = 'invalid stoprac'
character(*), parameter :: MESSAGE_372 = 'iplt inactive as it is a perennial plant'
character(*), parameter :: MESSAGE_373 = 'day temperature'
character(*), parameter :: MESSAGE_374 = 'hour temperature'
character(*), parameter :: MESSAGE_375 = &
'with the option laibrut the residuel lai disappears entirely on the first day of senescence'
character(*), parameter :: MESSAGE_376 = &
'if your crop is planted, it is better to use the initial stages stade0=snu or plt and the initialisation &
&parameters of the seedling(laiplantule and nbfeuilplant in the plant parameter file, and masecplantule and zracplantule)'
character(*), parameter :: MESSAGE_377 = &
'in the simulation mode "mixed cropping" the parameter coderacine of the plants must be 2'
character(*), parameter :: MESSAGE_378 = 'abscission parameter must be between 0 and 1, correct and run again'
character(*), parameter :: MESSAGE_385 = 'For intercrop radiation interception is done with radiation transfer'
! DR 05/01/2022 on met un message et on exit si la coherence entre le parametre P_code_acti_reserve du fichier ini
! et celui du fichier plante qu'on vient de lire ne sont pas respectees
character(*), parameter :: MESSAGE_447 = &
'The parameter value of code_acti_reserve in the plant file and the initialisation file &
&are not consistent. Please fix the values and run again.'

! lecsol   NB le 8/1/02
! ------
character(*), parameter :: MESSAGE_135 = 'SOIL PARAMETERS '
character(*), parameter :: MESSAGE_136 = 'the soil number has not been found !!!'
character(*), parameter :: MESSAGE_138 = 'depth of humification > 60 cm: impossible'
character(*), parameter :: MESSAGE_210 = 'amount of pebbles in wrong units'
character(*), parameter :: MESSAGE_211 = 'profhum = 0 : impossible'
character(*), parameter :: MESSAGE_360 = 'moisture threshold for mineralisation : min=HMIN and opt=HCC'
character(*), parameter :: MESSAGE_361 = 'alphaco2 > 2 : impossible'
character(*), parameter :: MESSAGE_212 = 'attention strange value of hccf (<1), stopped'
character(*), parameter :: MESSAGE_213 = 'attention strange value of hminf (<1) stopped'
character(*), parameter :: MESSAGE_217 = 'attention P_hccf<P_hminf , stopped'
character(*), parameter :: MESSAGE_214 = 'profhum  must be between 0 and profsol. profhum = '
character(*), parameter :: MESSAGE_215 = '(1 active , 2 inactive)'
character(*), parameter :: MESSAGE_216 = &
'you selected "stones" but you did not inform their nature and quantity, check your soil'
! lecstat
! -------
character(*), parameter :: MESSAGE_442 = &
'you selected an "over- 2-years" crop though your simulating period is just over 1 year,&
& correct the USM and run again'

! lectech     le 8/1/02
! -------
character(*), parameter :: MESSAGE_140 = 'name of cutting stage not recorded'
character(*), parameter :: MESSAGE_144 = 'Bare soil therefore parameter ressuite =0'
character(*), parameter :: MESSAGE_146 = 'jultrav before the start of simulation; set it to 0&
(no residues) or after the start of simulation'
character(*), parameter :: MESSAGE_149 = 'jultrav=0 thus qres set to 0'
character(*), parameter :: MESSAGE_150 = &
'your coderes is strange, it must be between 1 and 10 for residues in surface or 21 for roots!'
character(*), parameter :: MESSAGE_1150 = 'it will be forced to the same residue to :'
character(*), parameter :: MESSAGE_151 = 'irrigation depth zero ! change locirrig'
character(*), parameter :: MESSAGE_152 = 'TECHNICAL PARAMETERS NEEDED FOR THE SIMULATION, file: '
character(*), parameter :: MESSAGE_380 = 'thinning from below '
character(*), parameter :: MESSAGE_381 = 'thinning from above'
character(*), parameter :: MESSAGE_382 = 'harvest = all plant harvest'
character(*), parameter :: MESSAGE_383 = 'harvest = fruit picking'
character(*), parameter :: MESSAGE_384 = '1 at end of cycle'
character(*), parameter :: MESSAGE_386 = 'days'
character(*), parameter :: MESSAGE_387 = '    harvest at physiological maturity'
character(*), parameter :: MESSAGE_388 = 'harvest when water content (fruit) >'
character(*), parameter :: MESSAGE_389 = 'harvest when water content (fruit) <'
character(*), parameter :: MESSAGE_390 = 'harvest if sugar content >'
character(*), parameter :: MESSAGE_391 = 'harvest if sugar content>'
character(*), parameter :: MESSAGE_392 = 'harvest if oil content >'
character(*), parameter :: MESSAGE_393 = 'locirrig cannot exceed 50 -> set to 50'
character(*), parameter :: MESSAGE_394 = 'the value of ratiol must be between 0 and 1.0 it will be forced to 1.0'
character(*), parameter :: MESSAGE_395 = 'the value of the ratiolN cannot be negative; it will be forced to 1.0'
character(*), parameter :: MESSAGE_396 = 'the sowing depth value cannot be < 1.0, it will be forced to 1.0'
character(*), parameter :: MESSAGE_511 = 'No protected crops in mixed cropping'
character(*), parameter :: MESSAGE_397 = 'you cannot use a reproductive stage for the decision of forage cut'
character(*), parameter :: MESSAGE_398 = 'Attention !!  flowering imposing is useless because reproductive stages are calculated&
& from the drp stage.In order to impose reproductive phenology you need to impose drp'
character(*), parameter :: MESSAGE_399 = 'the option devoted to the keeping of all the plant variables after harvest (codemsfinal)&
& is not compatible with the picking method of harvest (codcueille)'
character(*), parameter :: MESSAGE_5600 = 'You can not combine plastic mulching and addition of organic residues (P_nbjres)'
character(*), parameter :: MESSAGE_601 = 'You can not combine plastic mulching and addition of previous crop residues (P_ressuite)'
! USM file
character(*), parameter :: MESSAGE_6011 = 'error in reading the file new_travail.usm: usm does not exist!'

! DR 13/08/09
! ******************************************************************
! Introduction des modifs 6.4 a garder pour la version 7.1 Javastics
! DR et ML et BM 23/06/09
character(*), parameter :: MESSAGE_451 = &
'You have added mulch residue type that will be broken down and modify the physical properties&
& of the soil surface'
character(*), parameter :: MESSAGE_452 = 'the simulation period is too short given the parameter periode_adapt_CC'

! lixiv
! -----
character(*), parameter :: MESSAGE_153 = 'inconsistency between the values of da and hcc'
character(*), parameter :: MESSAGE_154 = 'leading to negative macroporosity :'
!dr 02/07/2013 correction des messages macroporosite mauvais
character(*), parameter :: MESSAGE_155 = 'codefente=2 :  macropor=((1-da/2.66)-(hcc/100*da)) * 10 * P_epc'
character(*), parameter :: MESSAGE_256 = 'macropor must be > 2. mm'
character(*), parameter :: MESSAGE_255 = 'codefente=1 :  macropor = 0.5 * (hcc - hmin) / 100. * da * 10. * P_epc'

! princip
! -------
character(*), parameter :: MESSAGE_156 = 'Error reading file of USM !'
! DR 21/06/2017 pour david si le lai est present dans l'usm je force le modele en mode 'feuille' (forcage du lai) et je le dis
character(*), parameter :: MESSAGE_157 = &
'you have a lai file indicated in the usm , the model will force the lai value by the values of this file !'

! lectures
! --------
! domi 12/08/05
character(*), parameter :: MESSAGE_433 = 'for mixed crops -->Shuttleworth and Wallace is obligatory'
character(*), parameter :: MESSAGE_434 = 'check that you have the code  sw in your weather files'

! shutwall
! --------
character(*), parameter :: MESSAGE_159 = 'radiation nil on day '
character(*), parameter :: MESSAGE_160 = 'it is impossible to simulate mixed crops and a mulch'
character(*), parameter :: MESSAGE_161 = 'crop height A > crop height P'
character(*), parameter :: MESSAGE_162 = &
'reference height weather measurements (zr parameter in station file XXX_sta.xml) has to be higher than&
& the maximum height of the crop (hautmax in plant file) when Shuttleworth-Wallace formalism is selected for PET calculation.'

! solnu
! -----
! DR 31/05/2017 ce message ne veut rien dire , je le corrige
!    messages (163) = 'soil evaporation stopped because of the '
character(*), parameter :: MESSAGE_163 = &
'soil evaporation stopped because in the layer 1-zesx there is no water to evaporate this day : '
character(*), parameter :: MESSAGE_164 = 'tillers population died day :'
character(*), parameter :: MESSAGE_169 = 'non existing harvest method: change codcueille value (in technical file)'
character(*), parameter :: MESSAGE_170 = 'proftrav (ITK) > profhum (soil), change one of the 2 values before run again'

! eauplant
! --------
character(*), parameter :: MESSAGE_200 = 'problem calculating h2orec magrain and h2orec =0'

! iniclim
! -------
character(*), parameter :: MESSAGE_201 = &
'your cropping season is more than 1 year the sequence is impossible; correct the date for the end  &
& of simulation and start again'
character(*), parameter :: MESSAGE_202 = 'PET < 0 thresholded at 0 , date '
!    messages (203) = 'the date for the end of simulation and start again'
character(*), parameter :: MESSAGE_204 = 'without wind and humidity: impossible to calculate the weather under shelter'
character(*), parameter :: MESSAGE_205 = &
'without wind and humidity : impossible to calculate Penman PET; change the option to calculate PET'
character(*), parameter :: MESSAGE_206 = 'Priestley-Taylor has been calculated instead'
character(*), parameter :: MESSAGE_207 = 'you want to read PET Penman but it is missing'
character(*), parameter :: MESSAGE_208 = 'Penman has been calculated instead'
character(*), parameter :: MESSAGE_209 = &
'more than 30 PET calculated differently as your choice, change the codetp parameter in general file'
character(*), parameter :: MESSAGE_218 = &
'the vapor pressure is missing and has been calculated with tmin and the parameter P_corecTrosee'

! eauqual
!--------
character(*), parameter :: MESSAGE_311 = 'too much fruit dehydration'
character(*), parameter :: MESSAGE_312 = 'Attention 100% of water in fruit: wrong parameterisation of h2ofrvert or deshydbase'
character(*), parameter :: MESSAGE_313 = 'Fruit biochemistry badly parameterised'
! *- marie 23/03/04
character(*), parameter :: MESSAGE_314 = 'Attention the dehydration of fruit cannot be calculated because stdrpdes has too low&
& a value; if you are not interested in dehydration of fruit, give to stdrpdes the same value as stdrpmat'

! P_effeuil
!--------
character(*), parameter :: MESSAGE_320 = 'trimming too early : review julrogne, hautrogne or biorognem'
character(*), parameter :: MESSAGE_321 = 'thinning rather drastic : there are no leaves left !'

! fruits
! ------
character(*), parameter :: MESSAGE_350 = 'Your development course does not make sense and leads to simulating fruit filling &
&before the start of vegetative growth'
character(*), parameter :: MESSAGE_351 = ' number of days delay for effective drp ) = '

! levee
!------
character(*), parameter :: MESSAGE_400 = 'deficiencies at emergence > density=0'

! recolte
!--------
character(*), parameter :: MESSAGE_401 = 'you need deshydbase < 0'
character(*), parameter :: MESSAGE_402 = 'you need deshydbase > 0'

! repartir
!---------
character(*), parameter :: MESSAGE_403 = 'high senescence : no more green leaves before LAX stage'
character(*), parameter :: MESSAGE_404 = 'attention : fruit skin (enfruit) or ratio tigefeuil too high'


! bfroid
!-------
character(*), parameter :: MESSAGE_431 = 'Date of end of simulation deferred. Vernalisation taking place.'
! domi 29/09/05 dans lecplant
character(*), parameter :: MESSAGE_439 = 'attention : your variety number is more than the number of varieties'
character(*), parameter :: MESSAGE_440 = 'change the variety number and run again'
character(*), parameter :: MESSAGE_448 = &
'for an annual plant P_julvernal can not be earlier than the sowing we put it equal to sowing'

! dominver
! --------
character(*), parameter :: MESSAGE_441 = &
'Attention, change in dominancy, test on the radiative transfer parameters for change in dominancy'

! initnonsol - 16/02/2006
! -----------------------
character(*), parameter :: MESSAGE_443 = 'attention, when chaining the perenial crops (codeinitprec=2 and codeperenne=2) it is&
& not possible to impose stages'
character(*), parameter :: MESSAGE_444 = 'it is not possible to delay the sowing day further on '
character(*), parameter :: MESSAGE_445 = 'sowing day was delayed of this number of days : '
character(*), parameter :: MESSAGE_446 = 'P_tcxstop must be > P_tdmax (if active, if not= 100)'
!    messages (449) = 'P_stdrpmat to P_stdrpdes (codeforcestdrpdes in the paramv6_gen.xml file)'
character(*), parameter :: MESSAGE_450 = 'You are in a sequence of linked simulations for intercrop (CAS) and the dominant crop&
& in the prior usm is not the same as in this usm : you have to change the dominance of crops in the &
&current usm'
! lecture donnees cycle precedent
! DR 14/06/2011 modification mise en conformite param.sol
character(*), parameter :: MESSAGE_501 = 'in the case of a sequence is recovered soil'
character(*), parameter :: MESSAGE_502 = &
'P_typsol P_argi P_Norg P_profhum P_calc P_pH P_concseuil P_albedo P_q0 P_ruisolnu P_obstarac &
& P_pluiebat P_mulchbat P_zesx P_cfes P_z0solnu'
character(*), parameter :: MESSAGE_503 = &
'  P_codecailloux P_codemacropor P_codefente P_codrainage P_coderemontcap P_codenitrif P_codedenit'
character(*), parameter :: MESSAGE_504 = &
'  P_profimper P_ecartdrain P_ksol P_profdrain P_capiljour P_humcapil P_profdenit P_vpotdenit'
character(*), parameter :: MESSAGE_505 = '  P_epc   P_hccf   P_hminf  da  P_cailloux P_infil P_epd  P_typecailloux'
! message mythique a garder     character(*), parameter :: MESSAGE_570 = 'the upper level of the water table reached the soil surface : Noe mobile : +33(0)6154365998'
character(*), parameter :: MESSAGE_570 = 'the upper level of the water table reached the soil surface '
! DR 29/08/2012 j'ajoute ce message d'erreur venant de sommecouche...
character(*), parameter :: MESSAGE_580 = 'The sum of the number of layers per cell is not consistent with the number of layers&
& reported in Table summing'
character(*), parameter :: MESSAGE_600 = 'be sure to have parametrized the parameters for this residue ressuite = '
character(*), parameter :: MESSAGE_5000 = &
'in case of associated crops , the code P_codelaitr in the file plant must be equal to 1="lai option" &
& ,Please change your code and run again. '
character(*), parameter :: MESSAGE_5162 = 'nb_plantes in ficini  /=  nb_plantes in usm : '
character(*), parameter :: MESSAGE_5110 = 'impossible to have 2 consecutive stages simultaneously, amf and lax'
character(*), parameter :: MESSAGE_5111 = 'impossible to have 2 consecutive stages simultaneously, lax and sen'
character(*), parameter :: MESSAGE_5112 = 'impossible to have 2 consecutive stages simultaneously, sen and lan'
character(*), parameter :: MESSAGE_5113 = 'the parameter sensrsec has been limited to 1.0'
character(*), parameter :: MESSAGE_5005 = '*********************************************************'
character(*), parameter :: MESSAGE_5120 = 'no bud break if no dormancy'
character(*), parameter :: MESSAGE_5121 = 'radiation interception = Beers law'
character(*), parameter :: MESSAGE_5122 = 'leaf dynamics = ground cover '
character(*), parameter :: MESSAGE_5123 = 'cold requirements = vernalisation (herbaceous)'
character(*), parameter :: MESSAGE_5124 = 'cold requirements = dormancy (woody)'
character(*), parameter :: MESSAGE_5125 = 'P_codedormance = forcing'
character(*), parameter :: MESSAGE_5126 = 'P_codedormance = Richardson'
character(*), parameter :: MESSAGE_5127 = 'P_codedormance = Bidabe'
character(*), parameter :: MESSAGE_5128 = 'annual'
character(*), parameter :: MESSAGE_5129 = 'germination or latency = yes '
character(*), parameter :: MESSAGE_5130 = 'plantlet growth = hypocotyle growth '
character(*), parameter :: MESSAGE_5131 = 'plantlet growth = planting '
character(*), parameter :: MESSAGE_5132 = 'perennial'
character(*), parameter :: MESSAGE_5133 = 'leaf dynamics = LAI '
character(*), parameter :: MESSAGE_5134 = 'LAI calculation option,  direct LAInet '
character(*), parameter :: MESSAGE_5135 = 'LAI calculation option, LAInet=LAIbrut-senes '
character(*), parameter :: MESSAGE_5136 = 'effect of photoperiod on senescence'
character(*), parameter :: MESSAGE_5137 = 'growing dynamics = determinate growing plant'
character(*), parameter :: MESSAGE_5138 = 'unit Harvest Index = days'
character(*), parameter :: MESSAGE_5139 = 'unit Harvest Index = degree days'
character(*), parameter :: MESSAGE_5140 = 'growing dynamics = indeterminate growing plant'
character(*), parameter :: MESSAGE_5141 = 'number of inflorescences =  prescribed'
character(*), parameter :: MESSAGE_5142 = 'number of inflorescences =  trophic status function'
character(*), parameter :: MESSAGE_5143 = 'thermal stress on filling'
character(*), parameter :: MESSAGE_5144 = 'root density = standard profile'
character(*), parameter :: MESSAGE_5145 = 'root density = true density'
character(*), parameter :: MESSAGE_5146 = 'N effect on root distribution'
character(*), parameter :: MESSAGE_5147 = 'trophic-linked production = continuous link'
character(*), parameter :: MESSAGE_5148 = 'trophic-linked production = threshold'
character(*), parameter :: MESSAGE_5149 = 'plantlet or emergence frost'
character(*), parameter :: MESSAGE_5150 = 'leaf frost at juvenile phase (till AMF)'
character(*), parameter :: MESSAGE_5151 = 'leaf frost at adult phase'
character(*), parameter :: MESSAGE_5152 = 'flower/fruit frost (from FLO)'
character(*), parameter :: MESSAGE_5153 = 'water requirements = crop coefficient'
character(*), parameter :: MESSAGE_5154 = 'water requirements = resistance approach'
character(*), parameter :: MESSAGE_5155 = 'interception of water by foliage'
character(*), parameter :: MESSAGE_5156 = 'calculation nitrogen requirements = dense canopies (initial)'
character(*), parameter :: MESSAGE_5157 = 'calculation nitrogen requirements = isolated plants (new calculation)'
character(*), parameter :: MESSAGE_5158 = 'maximal fixation capacity = constant'
character(*), parameter :: MESSAGE_5159 = 'maximal fixation capacity = f(growth)'
character(*), parameter :: MESSAGE_3045 = ' Successive simulations impossible: wrong dates for start and finish'
character(*), parameter :: MESSAGE_3046 = ' Attention; if your previous weather file was not complete, &
&that may determine the end of the previous simulation !'
character(*), parameter :: MESSAGE_3274 = 'I cannot find the technique file '
character(*), parameter :: MESSAGE_4000 = 'I cannot find the file lai : '
character(*), parameter :: MESSAGE_219 = 'error during the station file reading'
character(*), parameter :: MESSAGE_261 = '******** file station ********'
character(*), parameter :: MESSAGE_510 = 'P_julres P_coderes P_qres  P_Crespc  P_CsurNres P_Nminres  P_eaures'
character(*), parameter :: MESSAGE_379 = 'if P_codefauche=1, then P_codcueille=2'
character(*), parameter :: MESSAGE_4001 = 'erreur dans la lecture du fichier initialisations'
character(*), parameter :: MESSAGE_262 = 'reading param.sti in case of parameter forcing '
character(*), parameter :: MESSAGE_263 = 'this parameter name is not correct : '
character(*), parameter :: MESSAGE_506 = &
'P_typsol          P_argi      P_Norg   P_profhum      P_calc        P_pH  P_concseuil    P_albedo&
&        P_q0  P_ruisolnu  P_obstarac  P_pluiebat P_mulchbat      P_zesx      P_cfes   P_z0solnu&
&  P_CsurNsol  P_finert  P_penterui'
character(*), parameter :: MESSAGE_507 = &
'         P_epc        P_hccf       P_hminf         P_daf      P_cailloux  P_typecailloux  &
&   P_infil         P_epd'
character(*), parameter :: MESSAGE_5170 = 'Nitrogen stress actived '
character(*), parameter :: MESSAGE_5171 = 'Water stress actived'
character(*), parameter :: MESSAGE_5172 = 'Optimum mineralisation in bare soil activated'
character(*), parameter :: MESSAGE_5173 = 'Smoothing of initial profiles activated'
character(*), parameter :: MESSAGE_5174 = 'Depth for mineral N and water stocks calculation = profmes'
character(*), parameter :: MESSAGE_5175 = 'Depth for mineral N and water stocks calculation = profsol'
character(*), parameter :: MESSAGE_5176 = 'Climatic series = reset of the initial conditions'
character(*), parameter :: MESSAGE_5177 = 'Climatic series = succession'
character(*), parameter :: MESSAGE_5178 = 'Biomass and yield conservation after harvest'
character(*), parameter :: MESSAGE_5179 = 'Take account of mulch effect (drying out of soil surface)'
character(*), parameter :: MESSAGE_5180 = 'Fruit load = all fruits (including ripe ones)'
character(*), parameter :: MESSAGE_5181 = 'Fruit load = growing fruits only'
character(*), parameter :: MESSAGE_5182 = 'Hourly microclimate'
character(*), parameter :: MESSAGE_5183 = 'scientific writing in st2 and report'
character(*), parameter :: MESSAGE_5184 = 'Separator spaces in report'
character(*), parameter :: MESSAGE_5185 = 'Separator in report = '
character(*), parameter :: MESSAGE_5186 = 'Activation of model sensitivity analysis'
character(*), parameter :: MESSAGE_5187 = 'this flag use to choose the output files is not ok, change it : P_flagEcriture'
character(*), parameter :: MESSAGE_5188 = 'the output file will be only the report file : P_flagEcriture = '
character(*), parameter :: MESSAGE_5189 = 'remark , you will not obtain the balance file : P_flagEcriture = '
character(*), parameter :: MESSAGE_5190 = 'Mineral nitrogen inhibition = no effect'
character(*), parameter :: MESSAGE_5191 = 'Mineral nitrogen inhibition = nitrogen amount'
character(*), parameter :: MESSAGE_5192 = 'Mineral nitrogen inhibition = nitrogen concentration'
character(*), parameter :: MESSAGE_5193 = 'calculation of groundwater if drainage = average'
character(*), parameter :: MESSAGE_5194 = 'calculation of groundwater if drainage = interdrain localisation'
character(*), parameter :: MESSAGE_5195 = 'monocotyledon '
character(*), parameter :: MESSAGE_5196 = 'dicotyledon '
character(*), parameter :: MESSAGE_5197 = 'post dormancy calculated using daily temperatures'
character(*), parameter :: MESSAGE_5198 = 'post dormancy calculated using hourly temperatures'
character(*), parameter :: MESSAGE_264 = 'Parameters of param_newform'
character(*), parameter :: MESSAGE_25 = 'main crop planting is after associated crop planting'
character(*), parameter :: MESSAGE_1167 = 'Plant frozen with no more reserve'
character(*), parameter :: MESSAGE_2099 = '===== Death of the plant due to lack of nitrogen ===='
character(*), parameter :: MESSAGE_103 = 'Check the new pools for perennials in the param_newform' ! Ajout Simon 2017

character(*), parameter :: MESSAGE_2100 = &
'===== the option pasture in the param_newform is not still available and has been desactivated ==='
character(*), parameter :: MESSAGE_2101 = &
'Warning : You have to fulfill parameters value for your residue in the parm_gen or choose an other one'
character(*), parameter :: MESSAGE_2102 = &
'Warning : When chaining simulations you cannot choose codeMSfinal = 2 in param_gen,you have to put it to 1.&
& You can use variables with _nrec to compare simulations and observations at harvest'
character(*), parameter :: MESSAGE_666 = 'Warning, you have both a deep drainage and a non-zero deep infiltration value.'

! Agmip
character(*), parameter :: MESSAGE_1159 = 'AgMIP Outputs activated'
character(*), parameter :: MESSAGE_1151 = 'Macsur Outputs activated'
character(*), parameter :: MESSAGE_1152 = 'sowing rules wheat3 AgMIP activation'

character(*), parameter :: MESSAGE_1153 = 'AgMIP Outputs activated, pilot AgMIP Wheat'
character(*), parameter :: MESSAGE_1154 = 'AgMIP Outputs activated, pilot AgMIP Wheat Giacomo (HSC)'
character(*), parameter :: MESSAGE_1155 = 'AgMIP Outputs activated, pilot wheat Canopy temp'
character(*), parameter :: MESSAGE_1156 = 'AgMIP Outputs activated, pilot face_maize'
character(*), parameter :: MESSAGE_1157 = 'AgMIP Outputs activated, pilot new wheat3'
character(*), parameter :: MESSAGE_1158 = 'AgMIP Outputs activated, pilot new wheat4'
contains
end module
