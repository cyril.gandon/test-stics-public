module daily_output_tests
   use stdlib_string_type
   use daily_output_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_format", test_get_format) &
                  ]

   end subroutine

   subroutine test_get_format(error)
      type(error_type), allocatable, intent(out) :: error
      type(column_format) :: fmt

      fmt = get_format(string_type('var'), .true.)
      call check(error, fmt%column_size, 16)
      call check(error, fmt%form, 'e')
      call check(error, fmt%precision, 7)

      
      fmt = get_format(string_type('vavarvarvarvarvarvarr'), .true.)
      call check(error, fmt%column_size, 21)
      call check(error, fmt%form, 'e')
      call check(error, fmt%precision, 7)

      fmt = get_format(string_type('var'), .false.)
      call check(error, fmt%column_size, 12)
      call check(error, fmt%form, 'f')
      call check(error, fmt%precision, 5)

      fmt = get_format(string_type('vavarvarvarvarvarvarr'), .false.)
      call check(error, fmt%column_size, 21)
      call check(error, fmt%form, 'f')
      call check(error, fmt%precision, 5)
   end subroutine

end module
