!*************************************************************
!      Gestion des sorties
!      calcul des valeurs moyennes de stress eau, azote et GEL
!      calcul des cumuls de flux et stocks d'eau et d'azote
!      calcul de la charge en fruits
!      enregistrement dans les fichiers: s*.sti et s*.st2
!        - racine.sti         (fichier 26)
!*************************************************************

! Output Management
! - calculation of mean values of stress water, nitrogen and GEL
! - calculation of cumulative stocks and flows of water and nitrogen
! - calculation of the fruit load
module Stics_Calcul_Sorties_m
USE Stics
USE Plante
USE Itineraire_Technique
USE Parametres_Generaux
USE Climat
USE Sol
use messages
USE string_utils, only: bbch_string_to_int
USE Station
USE climate_utils, only: tvar
USE calculRU_m, only: calculRU
use plant_utils
implicit none
private
public :: Stics_Calcul_Sorties_Plante, Stics_Calcul_Sorties
contains
subroutine Stics_Calcul_Sorties_Plante(sc,pg,c,p,itk,soil,t)
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(IN)    :: pg  
  type(Climat_),              intent(INOUT) :: c  
  type(Plante_),              intent(INOUT) :: p  
  type(ITK_),                 intent(INOUT) :: itk  
  type(Stics_Transit_),       intent(INOUT) :: t  
  type(Sol_),                 intent(INOUT) :: soil  

!: Variables locales
      integer n,i,iz,izmax,nstress1  !, AOAS, AS , AO


      n = sc%n
!      AOAS = 0
!      AS = 1
!      AO = 2

! DR 26/07/2016 on stocke le CNplante cde la veille pour les paturages
      p%CNplante_veille = p%CNplante(AOAS)

! ajout Bruno fevrier 2018   l'indice de recolte betterave etait faux
      if (p%P_codeplante .eq. CODE_BEET) then
!           p%ircarb(AOAS,n) = 0.
           p%CNgrain(0) = 0.
!           if(p%masecnp(AOAS,n) > 0.) p%ircarb(AOAS,n) = (p%mafruit + p%restemp(AOAS))/p%masecnp(AOAS,n)
           if (p%matuber > 0.) p%CNgrain(0) =  p%QNgrain(0) / p%matuber / 10.
!           if(n == p%nrec) p%ircarb(AOAS,n) = p%ircarb(AOAS,n-1)
           if(n > p%nrec .and. p%nrec > 0)  then
!               p%ircarb(AOAS,n) = 0.
               p%CNgrain(0) =  0.
            endif
      endif
! calcul de mafruit
      p%mafruit = p%magrain(AOAS,n) / 100.
      if (itk%P_codcueille == 2 .and. p%nbrecolte > 1) p%mafruit = p%magrain(AOAS,n)/100. - p%rdtint(AOAS,p%nbrecolte-1)/100.
! Modif Bruno fevrier 2018 : on sauvegarde les valeurs jusqu'au jour nrec-1 et non nrec
      if (p%nrec == 0 .or. n < p%nrec) then
          p%H2Orec_nrec        = p%H2Orec(AOAS)
          p%mabois_nrec        = p%mabois(AOAS)
          p%chargefruit_nrec   = p%chargefruit
          p%mafrais_nrec       = p%mafrais(AOAS)
          p%pdsfruitfrais_nrec = p%pdsfruitfrais(AOAS)
          p%masecnp_nrec       = p%masecnp(0,sc%n)
          p%magrain_nrec       = p%magrain(0,sc%n)
          p%mafruit_nrec       = p%mafruit
          p%matuber_nrec       = p%matuber
          p%CNgrain_nrec       = p%CNgrain(AOAS)
          p%CNplante_nrec      = p%CNplante(AOAS)
          p%QNplantenp_nrec    = p%QNplantenp(0,sc%n)
          p%QNgrain_nrec       = p%QNgrain(0)
          p%Qfix_nrec          = p%Qfix(0)
      endif
! Loic sept 2020 : pour resoudre les problemes de conservation des variables a la recolte si codemsfinal == 1
      if (pg%P_codemsfinal == 1 .and. itk%P_codcueille == 1) then
        if (n .eq. p%nrec .and. p%nrec > 0) then
          p%magrain(AOAS,n)     = p%magrain(AOAS,p%nrec)
          p%masecnp(AOAS,n)     = p%masecnp(AOAS,p%nrec)
          p%QNplantenp(AOAS,n)  = p%QNplantenp(AOAS,p%nrec)
          p%CNgrain(AOAS)       = p%CNgrain_nrec
          p%mafrais(AOAS)       = p%mafrais_nrec
          p%pdsfruitfrais(AOAS) = p%pdsfruitfrais_nrec
          p%mabois(AOAS)        = p%mabois_nrec
          p%H2Orec(AOAS)        = p%H2Orec_nrec
          p%chargefruit         = p%chargefruit_nrec
          p%msrac(n)            = p%msrac(p%nrec)
          p%irazo(AOAS,n)       = p%irazo(AOAS,p%nrec)
          p%ircarb(AOAS,n)      = p%ircarb(AOAS,p%nrec)
        endif
      endif

! modif Bruno octobre 2018
! les lignes qui suivent sont issues de la routine bilans (lignes 528-563)
 ! Rendement nul si magrain = 0 et si maperenne = 0
      if(p%magrain_nrec <= 0.) then
           if(p%P_code_acti_reserve == 2) then
              p%rendementsec = 0.
 ! Rendement = masec si magrain = 0 et si maperenne >0
           else
              p%rendementsec = p%masecnp_nrec
           endif
 ! Rendement = magrain si magrain > 0
      else
           p%rendementsec = p%magrain_nrec / 100.
      endif

! tests pas geniaux car dependent du nom du fichier : betterave, mais ensilage ou fourrage !!!
      if(p%P_codeplante.eq.CODE_BEET) p%rendementsec = p%matuber_nrec
      if(p%P_codeplante.eq.CODE_MEN) p%rendementsec = p%masec(AOAS,p%nrec)
      if(p%P_codeplante.eq.CODE_FODDER) p%rendementsec = p%mafauchetot
 ! fin deplacement

! 29/04/03 - forcage des variables plantes apres recolte si P_codemsfinal = 1
      if (pg%P_codemsfinal == 1 .and. itk%P_codcueille == 1 .and. n >= p%nrec .and. p%nrec > 0) then
          p%H2Orec(AOAS)        = p%H2Orec_nrec
          p%mabois(AOAS)        = p%mabois_nrec
          p%chargefruit         = p%chargefruit_nrec
          p%mafrais(AOAS)       = p%mafrais_nrec
          p%pdsfruitfrais(AOAS) = p%pdsfruitfrais_nrec
          p%masecnp(AOAS,n)     = p%masecnp_nrec
          p%magrain(AOAS,n)     = p%magrain_nrec
          p%mafruit             = p%mafruit_nrec
          p%matuber             = p%matuber_nrec
          p%CNgrain(AOAS)       = p%CNgrain_nrec
          p%CNplante(AOAS)      = p%CNplante_nrec
          p%QNplantenp(AOAS,n)  = p%QNplantenp_nrec
          p%QNgrain(AOAS)       = p%QNgrain_nrec
          p%Qfix(AOAS)          = p%Qfix_nrec
          p%QNplante            = p%QNplantenp_nrec + p%QNperenne(AOAS)
          p%QNtot               = p%QNplante + p%QNrac
          p%irazo(AOAS,n)       = p%irazo(AOAS,p%nrec)
          p%ircarb(AOAS,n)      = p%ircarb(AOAS,p%nrec)
      endif

    ! cas du sol nu: indicateurs de stress =  - 1  et profondeur racinaire = 0
      if (p%P_codeplante == CODE_BARESOIL) then
        p%swfac(AOAS)  = -1.
        p%turfac(AOAS) = -1.
        p%zrac = 0.
      endif

!: calcul des stress moyens sur les 2 grandes periodes du cycle
!--------------------------------------------------------------
    ! changement du stade fin de periode vegetative si pb drp avant lev
      if (p%nlev == 0 .and. p%ndrp > 0) then
         nstress1 = p%nlax
      else
         nstress1 = p%ndrp
      endif

      if (p%nlev > 0 .and. (nstress1 == 0 .or. n == nstress1)) then
         p%nst1 = p%nst1 + 1
         p%str1 = p%str1 + p%swfac(AOAS)
         p%stu1 = p%stu1 + p%turfac(AOAS)
         p%inn1 = p%inn1 + p%inns(AOAS)
         p%diftemp1 = p%diftemp1 + sc%tcult - c%tmoy(n)
         p%exofac1 = p%exofac1 + p%exofac
! DR 12/04/2011 j'ajoute les stress etr/etm et etm/etr dans le bilan
! calcul de etr/etm
         if(sc%etm > 0.) then
            p%etr_etm1 = p%etr_etm1 + (sc%et / sc%etm)
         else
            p%etr_etm1 = p%etr_etm1 + 1
         endif
! calcul de etm/etr
         if(sc%et > 0.) then
            p%etm_etr1 = p%etm_etr1 + (sc%etm / sc%et)
         else
            if (sc%etm >0.) p%etm_etr1 = p%etm_etr1 + 1.
           endif

         p%inn1moy = p%inn1 / p%nst1
         p%turfac1moy = p%stu1 / p%nst1
         p%swfac1moy = p%str1 / p%nst1
         p%exofac1moy = p%exofac1 / p%nst1
         p%etr_etm1moy = p%etr_etm1 / p%nst1
         p%etm_etr1moy = p%etm_etr1 / p%nst1
      endif

      if (p%ndrp > 0 .and. (p%nmat == 0 .or. n == p%nmat)) then
         p%nst2 = p%nst2 + 1
         p%str2 = p%str2 + p%swfac(AOAS)
         p%stu2 = p%stu2 + p%turfac(AOAS)
         p%inn2 = p%inn2 + p%inns(AOAS)
         p%diftemp2 = p%diftemp2 + sc%tcult - c%tmoy(n)
         p%exofac2 = p%exofac2 + p%exofac

 ! DR 12/04/2011 j'ajoute les stress etr/etm et etm/etr dans le bilan
! calcul de etr/etm
         if(sc%etm > 0.) then
             p%etr_etm2 = p%etr_etm2 + sc%et/sc%etm
         else
             p%etr_etm2 = p%etr_etm2 + 1.
         endif
! calcul de etm/etr
         if(sc%et > 0.) then
             p%etm_etr2 = p%etm_etr2 + sc%etm/sc%et
         else
             if (sc%etm >0.) p%etm_etr2 = p%etm_etr2 + 1.
         endif

         p%inn2moy = p%inn2 / p%nst2
         p%turfac2moy = p%stu2 / p%nst2
         p%swfac2moy = p%str2 / p%nst2
         p%exofac2moy = p%exofac2 / p%nst2
         p%etr_etm2moy = p%etr_etm2 / p%nst2
         p%etm_etr2moy = p%etm_etr2 / p%nst2
      endif

    ! effet du GEL
      if (p%nlev > 0 .and. (p%namf == 0 .or. n == p%namf)) p%gel1 = min(p%gel1, p%fstressgel)
      if (p%namf > 0) p%gel2 = min(p%gel2, p%fstressgel)
      p%gel3 = min(p%gel3, p%fgelflo)

    ! DR 12/08/08 si gel2 = 0. plante completement gelee, on met tous les stades suivants a amf
    ! verifier la genericite du rajout du codeplante
      if (p%P_codeperenne /= 2 .and. p%P_codeplante /= CODE_FODDER .and. (p%gel2 <= 0. .or. p%gel1 <= 0.)) then
         if (p%nrec == 0) then
            p%group = -1
! Modif Loic Juin 2018 :  on ne force plus la recolte quand on a un gel lethal, la plante est detruite dans plante_death.f90
!            p%nrec = n
         endif
         if (p%nlev > 0 .and. p%namf == 0) then
            p%namf = n
            p%P_stlevamf(itk%P_variete) = p%somcour
         endif
         if (p%namf > 0 .and. p%nlax == 0) then
            p%nlax = n
            p%P_stamflax(itk%P_variete) = p%somcour
         endif
         if (p%nlax > 0 .and. p%nsen == 0) then
            p%nsen = n
            p%P_stlaxsen(itk%P_variete) = p%somcour
         endif
         if (p%nsen > 0 .and. p%nlan == 0) then
            p%nlan = n
            p%P_stsenlan(itk%P_variete) = p%somcour
         endif
        if (p%nflo == 0) then
          p%nflo = n
          p%stlevflo = p%somcourdrp
        endif
        if (p%ndrp == 0) then
          p%ndrp = n
          p%P_stlevdrp(itk%P_variete) = p%somcourdrp
        endif
        if (p%ndebdes == 0) then
          p%ndebdes = n
          p%P_stdrpdes(itk%P_variete) = p%somcourdrp
        endif
        if (p%ndrp > 0 .and. p%nmat == 0) then
          p%nmat = n
          p%P_stdrpmat(itk%P_variete) = p%somcourdrp
        endif
      endif

    !: Cumuls sur le cycle de la culture
      if (n >= p%nplt  .and.  (p%nrec == 0 .or. n <= p%nrec .or. itk%P_codcueille == 2)) then
        p%ces = p%ces + sc%esol
        p%cep = p%cep + p%ep(AOAS)
        p%cet = p%cep + p%ces
        p%cprecip = p%cprecip + sc%precip + sc%ruisselsurf
        ! DR 16/09/2016 On avait pas de varaible exprimant le cumul des evaporation totals(mulch+sol), on les ajoute
        sc%cEdirect = sc%cEdirect + sc%Edirect
      ! cas des irrigations en profondeur
        if (itk%P_codlocirrig == 3) p%cprecip = p%cprecip + p%irrigprof(itk%P_locirrig)

        p%cetm = p%cetm + sc%etm
        p%cdemande = p%cdemande + p%demande(AOAS)
        p%cinterpluie = p%cinterpluie + p%interpluie(AOAS)
! dr 26/07/2011 anniversaire de maraige de Marie  on le deplace sinon pas de prise en compte dans bilan avant semis
!        sc%cintermulch = sc%cintermulch + sc%intermulch       ! TODO: cintermulch, pourquoi dans stics_communs et pas dans plante, comme les autres cumuls de cette partie ?
      ! DR 17/03/06 rajout de cumul sur la periode de culture
        p%crg = p%crg + c%trg(n)
        p%ctmoy = p%ctmoy + c%tmoy(n)
        p%ctcult = p%ctcult + sc%tcult
        p%cetp = p%cetp + c%tetp(n)
! DR 29/12/2014 ajout de la somme des tcultmax pour Giacomo
        p%ctcultmax = p%ctcultmax + sc%tcultmax
        p%cum_et0 = p%cum_et0+p%eop(aoas)+sc%eos
      endif

    ! calcul d'une quantite d'azote au stade drp
      if (n == p%ndrp) sc%QNdrp = p%QNplantenp(AOAS,n)
!      if (itk%P_codcueille == 2 .and. p%nbrecolte > 1) then
!         p%mafruit = p%magrain(AOAS,n) / 100.  - p%rdtint(AOAS,p%nbrecolte-1) / 100.
!      else
!         p%mafruit = p%magrain(AOAS,n) / 100.
!      endif

    ! calcul de la charge en fruits
      if (p%P_codeindetermin == 1) p%chargefruit = p%nbgrains(AOAS)
      if (p%P_codeindetermin == 2) then
         p%chargefruit = p%nbfruit
         if (pg%P_codefrmur == 1)  p%chargefruit = p%nbfruit + p%nfruit(AOAS,p%P_nboite)
      endif
      if (p%magrain(AOAS,n) <= 0.) p%chargefruit = 0.
      if (p%P_codeplante == CODE_FODDER) then
! 29/07/2015 DR et FR il faut retrancher de mafruit ce qui est tombe au sol en plus de ce qui est jaune
! Loic Juin 2016: il ne faut pas retirer ce qui est tombe au sol car c'est deja retranche de masecnp!
! Je cree une variable mafou car mafruit existe si on laisse pousser notre luzerne...
! Loic septembre 2020 : je remets ce calcul m�me s'il ne me plait pas du tout... car ca pose pb si la plantes fauch� fait des fruits
! Voir si on peut garder mafou pour les fourrages
        if (p%P_code_acti_reserve == 2) then
!            p%mafruit = p%masec(AOAS,n) - p%msresjaune(AOAS) - p%msneojaune(AOAS)
!            p%mafruit = p%masecnp(AOAS,n) - p%msresjaune(AOAS) - p%msneojaune(AOAS) - p%mafeuiltombe(AOAS)
! 15/10/2020 DR et LS et PL on garde ce calcul sachnat que masec=masecnp si code-acti-reserve=2
            p%mafruit = p%masecnp(AOAS,n) - p%msresjaune(AOAS) - p%msneojaune(AOAS)

        endif
        p%mafou = p%masecnp(AOAS,n) - p%msresjaune(AOAS) - p%msneojaune(AOAS)
        p%msjaune(AOAS) = p%msresjaune(AOAS) + p%msneojaune(AOAS)
      endif
      p%mafruit = max(0.,p%mafruit)
      p%mafou = max(0.,p%mafou)

      if (p%P_codeplante .eq. CODE_BEET .and. p%nrec == 0.) then
         p%matuber = p%mafruit + p%restemp(AOAS)
! Modif Loic Octobre 2017 : Maintenant les variables a la recolte sont enregistrees au debut de la routine
!         if(n<=p%nrec .and. p%nrec>0) p%matuber_rec = p%matuber
!         if (pg%P_codemsfinal == 1 .and. itk%P_codcueille == 1 .and. n > p%nrec .and. p%nrec > 0) p%matuber = p%matuber_rec
      endif

      if (p%nplt /= 0) p%iplts = p%nplt + sc%P_iwater - 1
      if (p%nlev /= 0) p%ilevs = p%nlev + sc%P_iwater - 1
      if (p%namf /= 0) p%iamfs = p%namf + sc%P_iwater - 1
      if (p%nlax /= 0) p%ilaxs = p%nlax + sc%P_iwater - 1
      if (p%ndrp /= 0) p%idrps = p%ndrp + sc%P_iwater - 1
      if (p%nflo /= 0) p%iflos = p%nflo + sc%P_iwater - 1
      if (p%nsen /= 0) p%isens = p%nsen + sc%P_iwater - 1
      if (p%nlan /= 0) p%ilans = p%nlan + sc%P_iwater - 1
      if (p%nmat /= 0) p%imats = p%nmat + sc%P_iwater - 1
      if (p%nrec /= 0) p%irecs = p%nrec + sc%P_iwater - 1
      if (p%ndebdorm /= 0) p%idebdorms = p%ndebdorm + sc%P_iwater - 1
      if (p%nfindorm /= 0) p%ifindorms = p%nfindorm + sc%P_iwater - 1
      if (p%nnou /= 0) p%inous = p%nnou + sc%P_iwater - 1
      if (p%ndebdes /= 0) p%idebdess = p%ndebdes + sc%P_iwater - 1
      if (p%nger /= 0) p%igers = p%nger + sc%P_iwater - 1

! DR 30/11/2020 merge trunk
!DR 13/02/2020 j'ajoute les stades flo-150 et flo+150
      if (p%flo_moins_150 /= 0) p%iflos_minus_150 = p%flo_moins_150 + sc%P_iwater - 1
      if (p%flo_plus_150 /= 0) p%iflos_plus_150 = p%flo_plus_150 + sc%P_iwater - 1

         ! DR 31/05/2016 ajout du nombre de jours depuis le semis
      sc%day_after_sowing = sc%n-(p%iplts-sc%P_iwater)-1

      if (p%nmontaison /= 0) p%imontaisons = p%nmontaison + sc%P_iwater - 1

! DR 03/03/08 pour climator on veut les variables suivantes
      if (n == p%nrec) then
        p%nexporte =  p%QNplantenp_nrec - p%QNressuite
        p%nrecycle =  p%QNressuite + p%QNplantetombe(AOAS)
        p%MSexporte =  p%masecnp_nrec - p%Qressuite
        p%MSrecycle =  p%Qressuite + p%mafeuiltombe(AOAS)
      endif

! DR 30/04/08 poids de 1000 grains pour Julie
      if (p%pgrain(AOAS) <= 0.) then
         p%p1000grain = 0.
      else
         p%p1000grain = p%pgrain(AOAS) * 1000.
      endif

! Modifs Bruno et Loic fevrier 2013 : quantites totales de biomasse et d'azote (plante entiere)
! DR 26/06/2020 plante entiere mais du coup on a pas les valeurs AO et AS et on en a besoin dans jour de coupe
! Loic sept 2020 : je mets masec dans la routine cumAOAS sinon ca fou le bazard
!      p%masec(AOAS,n) = p%masecnp(AOAS,n) + p%maperenne(AOAS)
!     write(*,*)p%masec(AOAS,n)
!      p%masec(AS,n) = p%masecnp(AS,n) + p%maperenne(AS)
!      p%masec(AO,n) = p%masecnp(AO,n) + p%maperenne(AO)
!      p%masec(AOAS,n) = p%masecnp(AS,n)*p%surf(AS)+ p%masecnp(AO,n)*p%surf(AO) + &
!                    p%maperenne(AS)*p%surf(AS)+ p%maperenne(AO)*p%surf(AO)

 !     write(*,*)p%masec(AOAS,n), p%masecnp(AS,n),p%surf(AS),p%maperenne(AS),p%surf(AS)

      p%mstot = p%masec(AOAS,n) + p%msrac(n)

! Ajout Loic Aout 2016: calcul d'un ratio feuille / tige pour la qualite des fourages
      if (p%matigestruc(AOAS)>0.00001) then
         p%ratioFT = p%mafeuilverte(AOAS) / p%matigestruc(AOAS)
      else
         p%ratioFT = 0.
      endif

    ! 08/09/06 DR etIGC on calcule la tempmini moy et l'amplitude moyenne
    !  entre lax et rec (ca donne un indicateur de la qualite du pinardos )
      if (n >= p%nlax .and. p%nlax /= 0 .and. (p%nrec == 0 .or. p%nrec == n)) then
        c%tncultmat = c%tncultmat + sc%tcultmin
        c%amptcultmat = c%amptcultmat + (sc%tcultmax - sc%tcultmin)
        c%dureelaxrec = c%dureelaxrec + 1
        if (sc%tcultmax > p%P_tmaxremp) c%nbjechaudage = c%nbjechaudage + 1    ! nbjechaudage dans comclim, pourquoi pas plutot une variable plante ?
        if (n == p%nrec) then
          c%tncultmat = c%tncultmat / c%dureelaxrec
          c%amptcultmat = c%amptcultmat / c%dureelaxrec
        endif
      endif

      if (n >= p%nplt) then
        p%somudevair = p%somudevair  + p%udevair
        p%somudevcult = p%somudevcult + p%udevcult
        p%somupvtsem  = p%somupvtsem  + p%upvt(n)
      endif

! Modifs Loic et Bruno fevrier 2014
! calcul de lrac (f/g), msrac (f/g), msracmort (f/g) pour les racines fines et grosses
     sc%nhe = 0
     do i = 1, sc%NH
         p%LRACH(i) = 0.
         p%lracf(i) = 0.
         p%lracg(i) = 0.
         p%msracf(i) = 0.
         p%msracg(i) = 0.
         izmax = soil%P_epc(i)
         do iz = 1, izmax
            if (p%P_coderacine == 2) then
                p%lracf(i) = p%lracf(i) + p%rlf(sc%nhe + iz)
                p%lracg(i) = p%lracg(i) + p%rlg(sc%nhe + iz)
                p%msracf(i) = p%msracf(i) + p%rlf(sc%nhe + iz) / p%longsperacf *100.
                p%msracg(i) = p%msracg(i) + p%rlg(sc%nhe + iz) / p%longsperacg *100.
                p%msracmortf(i) = p%msracmortf(i) + p%drlsenf(sc%nhe + iz)/ p%longsperacf *100.
                p%msracmortg(i) = p%msracmortg(i) + p%drlseng(sc%nhe + iz)/ p%longsperacg *100.
            else
                p%LRACH(i) = p%LRACH(i) + p%flrac(sc%nhe + iz) * p%P_lvmax
            endif
         end do
         sc%nhe = sc%nhe + izmax
         if (p%P_coderacine == 2) then
            p%lracf(i) = max(1.e-7, p%lracf(i) / soil%P_epc(i))
            p%lracg(i) = max(1.e-7, p%lracg(i) / soil%P_epc(i))
            p%LRACH(i) = p%lracf(i) + p%lracg(i)
         else
            p%LRACH(i) = max(1.e-7, p%LRACH(i) / soil%P_epc(i))
         endif
     end do
    ! Modif Loic Juin 2016: je modifie le calcul en prenant la variable mafou
    ! Modif Loic Avril 2021: calculs ISOP
    ! p%msrec_fou = p%mafou - itk%P_msresiduel(p%numcoupe)
    ! p%msrec_fou= p%mafruit- itk%P_msresiduel(p%numcoupe)
     if (t%P_code_ISOP == 1) then
     ! Ajout Loic Mars 2021 : ajouts et modifs pour ISOP
        p%msrec_fou = p%mafou - itk%P_msresiduel(p%numcoupe)
        ! Modif Loic Mai 2021 : attention il ne faut pas avoir de valeurs de cumul negatives
        if (p%msrec_fou > 0) then
            p%msrec_fou_tot = p%msrec_fou_coupe + p%msrec_fou
        endif
     else
        p%msrec_fou = p%mafou - itk%P_msresiduel(p%numcoupe)
     endif

     if(p%lai(0,n) > p%laimax(0)) p%laimax(0) = p%lai(0,n)
! DR 19/02/2016 je demarre de creer les variables pour les changements d'unites
     p%masec_kg_ha = p%masec(0,n)*1000.
     p%mafruit_kg_ha = p%mafruit*1000.
     p%mafeuil_kg_ha = p%mafeuil(0)*1000.
     p%matigestruc_kg_ha = p%matigestruc(0)*1000.
! DR 25/03/2016 je continue de sortir les varaibles specifiques de correspondance
     p%H2Orec_percent = p%H2Orec(aoas)*100.
     p%huile_percent = p%huile(aoas)*100.
     p%sucre_percent = p%sucre(aoas)*100.
     p%gel1_percent = (1-p%gel1)*100.
     p%gel2_percent = (1-p%gel2)*100.
     p%gel3_percent = (1-p%gel3)*100.
     p%nbinflo_recal=p%P_nbinflo(itk%P_variete)
     p%ebmax_gr = p%ebmax*100.
     p%fpari_gr = p%fpari*100.
     p%codebbch_output = get_current_bbch_code(sc%n, p)
     p%rltot = p%rltotg+p%rltotf

  ! DR 17/11/2021 ajout des variables issues de CorrespondanceVariablesDeSorties.f90
     p%CsurNrac = p%QCrac/p%QNrac
     p%CsurNracmort = p%QCracmort/p%QNracmort

     p%HI_C = 0.
     if (p%masecnp(0,n) > 0.) p%HI_C = p%mafruit/p%masecnp(0,n)
     if (p%masecnp(0,n) > 0. .and. p%P_codeplante .eq. CODE_BEET)  p%HI_C = (p%mafruit + p%restemp(0))/p%masecnp(0,n)

     p%HI_N = 0.
     if(p%QNplantenp(0,n) > 0.) p%HI_N = p%QNgrain(0)/p%QNplantenp(0,n)

  return
end subroutine Stics_Calcul_Sorties_Plante

! ----------------------------------------------------------------------------------------------------
! partie commune, non plante
subroutine Stics_Calcul_Sorties(logger,sc,c,soil,p,itk,pg,sta)
  type(logger_), intent(in) :: logger
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Climat_),              intent(INOUT) :: c  
  type(Plante_),              intent(INOUT) :: p  
  type(ITK_),                 intent(INOUT) :: itk  
  type(Sol_),                 intent(INOUT) :: soil  
  type(Parametres_Generaux_), intent(IN)    :: pg  
  type(Station_),             intent(INOUT) :: sta

  integer :: i  !  
  integer ::  iz  !  
  integer ::  izmax  !  
  integer ::  n  !  
  integer ::  ir  
  integer :: epaisseur_couche
!! merge trunk 23/11/2020
  integer :: j, k, jul
  integer :: nl1, nl2

      n = sc%n

    ! Cumuls sur l'ensemble de la simulation
      sc%cestout = sc%cestout + sc%esol
      sc%cpreciptout = sc%cpreciptout + sc%precip + sc%ruisselsurf
    ! DR 16/09/2016 On avait pas de varaible exprimant le cumul des evaporation totals(mulch+sol), on les ajoute
      sc%cEdirecttout = sc%cEdirecttout+sc%Edirect
      sc%cintermulch = sc%cintermulch + sc%intermulch       ! TODO: cintermulch, pourquoi dans stics_communs et pas dans plante, comme les autres cumuls de cette partie ?

! DR 16/12/2013 pour Macsur cumul a partir du semis des variables autres que plantes
      if (n >= p%nplt  .and.  (p%nrec == 0 .or. n <= p%nrec .or. itk%P_codcueille == 2)) then
        sc%drain_from_plt = sc%drain_from_plt +sc%drain
        sc%leaching_from_plt = sc%leaching_from_plt + sc%azsup
        sc%runoff_from_plt = sc%runoff_from_plt + sc%ruissel
        sc%Nmineral_from_plt = sc%Nmineral_from_plt + soil%cumvminr + soil%cumvminh
        sc%Nvolat_from_plt = sc%Nvolat_from_plt + soil%Nvoleng + soil%Nvolorg
        sc%QNdenit_from_plt = sc%QNdenit_from_plt + soil%Ndenit
	! from trunk 
        sc%cet_from_plt = sc%cet_from_plt + sc%esol + p%ep(AOAS)
      endif
! DR 02/03/2017 pour Macsur_vigne cumul a partir du debourrement des variables autres que plantes
      if ((n >= p%nlev .and. p%nlev.ne.0)  .and.  (p%nrec == 0 .or. n <= p%nrec )) then
        sc%drain_from_lev = sc%drain_from_lev +sc%drain
        sc%leaching_from_lev = sc%leaching_from_lev + sc%azsup
        sc%runoff_from_lev = sc%runoff_from_lev + sc%ruissel
        sc%Nmineral_from_lev = sc%Nmineral_from_lev + soil%cumvminr + soil%cumvminh
        sc%Nvolat_from_lev = sc%Nvolat_from_lev + soil%Nvoleng + soil%Nvolorg
        sc%QNdenit_from_lev = sc%QNdenit_from_lev + soil%Ndenit
        sc%cet_from_lev = sc%cet_from_lev + sc%esol + p%ep(AOAS)
        sc%cum_et0_from_lev=sc%cum_et0_from_lev+p%eop(AOAS)+sc%eos
      endif

    ! cas des irrigations en profondeur
    ! TODO: ajouter P_codlocirrig dans le Stics Communs. C'est une variable technique mais unique a la simulation,
    !       donc a dupliquer et harmoniser au sein d'une variable dans le tronc commun des variables stics.
    ! En fait, par defaut, on prend les valeurs de la plante principale.
      if (itk%P_codlocirrig == 3) sc%cpreciptout = sc%cpreciptout + p%irrigprof(itk%P_locirrig)

      sc%ruisselt = sc%ruisselt + sc%ruissel

    ! moyenne des donnees eau, azote et temperature par horizon
      sc%nhe = 0
      do i = 1, sc%NH
        sc%HR(i)    = 0.
        soil%AZnit(i) = 0.
        sc%AZamm(i) = 0.
        sc%TS(i)    = 0.
        izmax = soil%P_epc(i)
        do iz = 1, izmax
          sc%HR(i) = sc%HR(i) + sc%HUR(sc%nhe + iz) + sc%sat(sc%nhe + iz)
          soil%AZnit(i) = soil%AZnit(i) + soil%nit(sc%nhe + iz)
          sc%AZamm(i) = sc%AZamm(i) + soil%amm(sc%nhe + iz)
          sc%TS(i) = sc%TS(i) + sc%tsol(sc%nhe + iz)
        end do
        sc%nhe = sc%nhe + izmax

      ! calcul de la concentration en nitrate (mg NO3/litre) de chaque horizon
      ! (attention ce n'est pas une concentration en azote!)
        sc%concNO3sol(i) = 6200./14. * soil%AZnit(i) / sc%HR(i)

! DR 23/11/2021 on veut aussi les teneurs en eau par horizon mais en mm
        sc%HR_mm(i)=sc%HR(i)

        sc%HR(i) = sc%HR(i) / soil%da(i) * 10 / soil%P_epc(i)
        sc%TS(i) = sc%TS(i) / soil%P_epc(i)

      end do

! DR 13/10/2016 je divise les humidites volumiques par 100. d'apres Patrick B.
! DR 30/09/2016 pour AgMIP ET calcul des humdites sur 10 CM pour caler avce des obs
        epaisseur_couche = 10
        sc%HR_vol_1_10  = 0.
        do iz = 1, 10
           sc%HR_vol_1_10 = sc%HR_vol_1_10 + (( sc%HUR(iz) + sc%sat(iz)) * 10. / epaisseur_couche)
         end do
         sc%HR_vol_1_10 = sc%HR_vol_1_10/100.
! DR 14/02/2016 pour AgMIP ET calcul des humdites sur 30 CM
! on reste en humidites volumique !!
        epaisseur_couche = 30
        ! couche 1-30
        sc%HR_vol_1_30  = 0.
        do iz = 1, 30
          sc%HR_vol_1_30 = sc%HR_vol_1_30 + (( sc%HUR( iz) + sc%sat( iz)) * 10 / epaisseur_couche)
         end do
        sc%HR_vol_1_30 = sc%HR_vol_1_30/100.
        ! couche 31-60
        sc%HR_vol_31_60  = 0.
        do iz = 31, 60
          sc%HR_vol_31_60 = sc%HR_vol_31_60 + (( sc%HUR( iz) + sc%sat( iz)) * 10 / epaisseur_couche)
         end do
        sc%HR_vol_31_60 = sc%HR_vol_31_60/100.
        ! couche 61-90
        sc%HR_vol_61_90  = 0.
        do iz = 61, 90
          sc%HR_vol_61_90 = sc%HR_vol_61_90 + (( sc%HUR( iz) + sc%sat( iz)) * 10 / epaisseur_couche)
         end do
         sc%HR_vol_61_90 = sc%HR_vol_61_90/100.
        ! couche 91-120
        sc%HR_vol_91_120  = 0.
        do iz = 91, 120
          sc%HR_vol_91_120 = sc%HR_vol_91_120 + (( sc%HUR( iz) + sc%sat( iz)) * 10 / epaisseur_couche)
         end do
         sc%HR_vol_91_120 = sc%HR_vol_91_120/100.
        ! couche 121-150
        sc%HR_vol_121_150  = 0.
        do iz = 121, 150
          sc%HR_vol_121_150 = sc%HR_vol_121_150 + (( sc%HUR( iz) + sc%sat( iz)) * 10 / epaisseur_couche)
         end do
         sc%HR_vol_121_150 = sc%HR_vol_121_150/100.
        ! couche 151-180
        sc%HR_vol_151_180  = 0.
        do iz = 151, 180
          sc%HR_vol_151_180 = sc%HR_vol_151_180 + (( sc%HUR( iz) + sc%sat( iz)) * 10 / epaisseur_couche)
         end do
         sc%HR_vol_151_180 = sc%HR_vol_151_180/100.

! DR et PL pour Agmip ET ,   on a cree une variable , anciennement 'hur(10)_vol'
         sc%hur_10_vol = sc%hur(10)/10.


! DR 05/03/2018 j'ajoute mes reserves specifiques pour Wheat phase 4 pour verifier si c'est ok par couches
        sc%HR_mm_1_30 = sum( sc%HUR(1:30) + sc%sat(1:30) )
        sc%HR_mm_31_60 = sum( sc%HUR(31:60) + sc%sat( 31:60))
        sc%HR_mm_61_90 = sum( sc%HUR(61:90) + sc%sat( 61:90))
! DR 12/03/2018 j'ajoute mes propres profondeurs pour  Wheat phase 4
        sc%nit_1_30 = sum( soil%nit(1:30) )
        sc%nit_31_60 = sum( soil%nit(31:60) )
        sc%nit_61_90 = sum( soil%nit(61:90) )
        sc%amm_1_30 = sum( soil%amm(1:30) )
        sc%amm_31_60 = sum( soil%amm(31:60) )
        sc%amm_61_90 = sum( soil%amm(61:90) )


        sc%soilN_rootdepth = sum( soil%nit(1:int(p%zracmax)) )


! DR 14/02/2016  Fin AgMIP ET

    !  cumul des pluies
        sc%cpluie = sc%cpluie + c%trr(n)
        sc%toteaunoneffic = sc%toteaunoneffic + sc%eaunoneffic
        c%Ctculttout = c%Ctculttout + sc%tcult
        c%Ctairtout  = c%Ctairtout  + c%tmoy(n)
        c%Crgtout    = c%Crgtout    + c%trg(n)
        c%Cetmtout   = c%Cetmtout   + sc%etm
        c%Ctetptout  = c%Ctetptout  + c%tetp(n)
        c%somdifftculttair = c%somdifftculttair + sc%tcult - sc%tairveille

 ! Modif Bruno fevrier 2018 changement de nom et de definition
 ! Crtout = quantite totale de C des residus presents sur tout le profil de sol
 ! Nrtout = quantite totale de N des residus presents sur tout le profil de sol

        sc%Crtout = SUM (sc%Cres(1:int(soil%P_profhum),1:pg%nbResidus))
        sc%Nrtout = SUM (sc%Nres(1:int(soil%P_profhum),1:pg%nbResidus))

        do ir = 11,pg%nbResidus
          sc%Cresiduprofil(ir-10) = SUM (sc%Cres(1:int(soil%P_profhum),ir))
          sc%Nresiduprofil(ir-10) = SUM (sc%Nres(1:int(soil%P_profhum),ir))
        enddo
!DR 05/06/2019 ajout des Cres et Nres par cm pour tous types de residus confondus
        do iz = 1,int(soil%P_profhum)
          sc%C_allresidues(iz) = SUM (sc%Cres(iz,11:pg%nbResidus))
          sc%N_allresidues(iz) = SUM (sc%Nres(iz,11:pg%nbResidus))
        enddo


        c%humair_percent = c%humair*100.
        sc%humidite_percent = sc%humidite*100.
        sc%N_mineralisation = sc%Qminh+sc%Qminr
        soil%N_volatilisation = soil%QNvolorg+soil%QNvoleng
        sc%tcult_tairveille = sc%tcult-sc%tairveille

        soil%epc_recal(1) = soil%P_epc(1)
        soil%epc_recal(2) = soil%P_epc(2)
        soil%epc_recal(3) = soil%P_epc(3)
        soil%epc_recal(4) = soil%P_epc(4)
        soil%epc_recal(5) = soil%P_epc(5)
        soil%infil_recal(1) = soil%P_infil(1)
        soil%infil_recal(2) = soil%P_infil(2)
        soil%infil_recal(3) = soil%P_infil(3)
        soil%infil_recal(4) = soil%P_infil(4)
        soil%infil_recal(5) = soil%P_infil(5)

        sta%ra_recal = sta%P_ra
        p%et0 = p%eop(AOAS)+sc%eos

! DR 07/09/2018
          if(sc%cum_immob.lt.0)then
              sc%cum_immob_positif=-sc%cum_immob
          else
              sc%cum_immob_positif=sc%cum_immob
          endif

! PL, 27/11/2018, A VOIR: from trunk
! DR 22/04/2016 je rajoute les variables prairies calculees dans le bilan
! les quantites d'N exportes et les dates de fauche
     !*******************************
     ! cas 1 : les cultures fauchees
!     if (itk%P_codefauche == 1) then
!               p%QNexport = p%QNplantefauche
!**********************************************
! Cas 2 : les cultures perennes avec cueillette
!     else
!        if (itk%P_codcueille == 2 .and. p%P_codeperenne == 2) then
!               p%QNexport = p%QNgrain(AOAS)
!        else
    !*******************************
    ! Cas 3 : les cultures annuelles
!               p%QNexport = p%QNplante(AOAS,p%nrec) - p%QNressuite - p%QNrac
!        endif
!     endif

 ! DR 07/06/2017 j'ajoute la variable leai qui est la somme de la surface foliaire pour la photosuynthese des feuilles et des epis
 ! DR 07/06/2017  on cumul ici les composantes de la varaibles leai pour chaque sous-systeme
              p%leai(AO)=p%lai(AO,n)+p%eai(AO)
              p%leai(AS)=p%lai(AS,n)+p%eai(AS)

!! merge trunk 23/11/2020
 ! DR 11/02/2020 ajout des variables pour Operate
              p%tsol_mean_0_profsem = sum(sc%tsol(1:int(itk%P_profsem)))/int(itk%P_profsem)
              p%tsol_min_0_profsem = 100.
              if(n.eq.1)then
                  p%tsol_min_plt_ger_0_dpthsow=100.
                  p%tsol_min_ger_lev_0_dpthsow=100.
              endif
              do k=1,int(itk%P_profsem)
                  p%tsol_min_0_profsem = min(p%tsol_min_0_profsem, sc%tsol(k))
              enddo
           ! entre plt et ger et entre 0 et profondeur de semis on calule la moyenne et le min de tsol
            if ((n >= p%nplt.and.p%nplt.ne.0) .and.  (p%nger == 0 .or. n <= p%nger) )then
              p%tsol_sum_plt_ger_0_dpthsow = &
                 p%tsol_sum_plt_ger_0_dpthsow+sum(sc%tsol(1:int(itk%P_profsem)))
              do k=1,int(itk%P_profsem)

                p%tsol_min_plt_ger_0_dpthsow = min(p%tsol_min_plt_ger_0_dpthsow, sc%tsol(k))
              enddo
              !p%nbj_pltger=p%nbjpltger+1
            endif
            if(n.eq.p%nger) then
               p%tsol_sum_plt_ger_0_dpthsow = &
                 p%tsol_sum_plt_ger_0_dpthsow/itk%P_profsem/(p%nger-p%nplt+1)
            endif
           ! entre ger et lev et entre 0 et profondeur de semis on calule la moyenne et le min de tsol
            if ((n >= p%nger.and.p%nger.ne.0)  .and.  (p%nlev == 0 .or. n <= p%nlev)) then
              p%tsol_sum_ger_lev_0_dpthsow= &
                  p%tsol_sum_ger_lev_0_dpthsow+sum(sc%tsol(1:int(itk%P_profsem)))
              do k=1,int(itk%P_profsem)
                p%tsol_min_ger_lev_0_dpthsow= min(p%tsol_min_ger_lev_0_dpthsow, sc%tsol(k))
              enddo
              !p%nbj_gerlev=p%nbj_gerlev+1
            endif
            if(n.eq.p%nger) then
               p%tsol_mean_plt_ger_0_dpthsow = &
                 p%tsol_sum_plt_ger_0_dpthsow/itk%P_profsem/(p%nger-p%nplt+1)
            endif
            if(n.eq.p%nlev) then
               p%tsol_mean_ger_lev_0_dpthsow = &
                 p%tsol_sum_ger_lev_0_dpthsow/itk%P_profsem/(p%nger-p%nplt+1)
            endif
           ! nb jours de gel entre amf et amf+120 �c en udev
            if (n >= p%namf.and.p%namf.ne.0.and.p%som_temp_seuil <=120. ) then
                p%som_temp_seuil=p%som_temp_seuil+p%udevcult
                if(sc%tcultmin.le.p%P_tdebgel)then
                    p%nb_days_frost_amf_120=p%nb_days_frost_amf_120+1
                endif
            endif

           ! swfac moyen entre flo-150� et flo+150  entre amf et amf+120 �c en udev
           p%tab_swfac(n)=p%swfac(0)
!           p%tab_sum_udev(n)=upvt
           if(n.eq.p%nflo) then
               do j=n,1,-1
                   if(p%sum_udev.le.150.) then
                      p%sum_udev=p%sum_udev+p%upvt(j)
                      p%flo_moins_150 = j
                   endif
               enddo
               p%sum_udev=0.
           endif
           if(n.ge.p%nflo.and.p%nflo.ne.0) then
               p%sum_udev=p%sum_udev+p%upvt(n)
               if(p%sum_udev.le.150.) p%flo_plus_150 = n
           endif

           p%mean_swfac_flo_p_m_150 = &
             sum(p%tab_swfac(p%flo_moins_150:p%flo_plus_150))/(p%flo_plus_150-p%flo_moins_150+1)

! DR 02/04/2020 on ajoute aux sorties les nb d'irrigations , les quantites et les dates
     if (sc%naptot  >  0) then
         if (sc%airg(n) > 0.) then
           jul = k + sc%P_iwater - 1
          !   call modulo_years(jul, sc%annee(sc%P_iwater), jt, ancours)
          !   call get_day_month(jt,ancours,mois,jour,nummois)
         !  write (fb,180) jour,mois,ancours,sc%airg(k)
! 180         format(7x,I2,'-',A3,'-',I4,15x,F7.1)
           sc%n_tot_irrigations= sc%n_tot_irrigations + 1
           sc%q_irrigations(sc%n_tot_irrigations) = sc%airg(n)
           sc%date_irrigations(sc%n_tot_irrigations) = n + sc%P_iwater
!            write(*,*)'irrig', sc%n_tot_irrigations,sc%date_irrigations(sc%n_tot_irrigations), &
!             sc%q_irrigations(sc%n_tot_irrigations)
         endif
     endif
! 15/04/2020 DR j'ajoute 3 varaibles operate
      sc%day_after_begin_simul = sc%n
      if(n >= p%nlev.and.p%nlev.ne.0)p%day_after_emergence = sc%n - p%nlev+1
! 15/04/2020 DR Nb jours ou humair_percent > 90% entre amf et lax
  if ((n >= p%namf.and.p%namf.ne.0) .and.  (p%nlax == 0 .or. n <= p%nlax) )then
    if(c%humair_percent>=90.0)sc%nb_days_humair_gt_90_percent1=sc%nb_days_humair_gt_90_percent1 + 1
    endif
  if ((n >= p%nlax.and.p%nlax.ne.0) .and.  (p%ndrp == 0 .or. n <= p%ndrp) )then
    if(c%humair_percent>=90.0)sc%nb_days_humair_gt_90_percent2=sc%nb_days_humair_gt_90_percent2 + 1
    endif
! fin modif operate


! DR 17/11/2021 j'ajoute ici les calculs sortis de CorrespondanceVariablesDeSorties.f90

    sc%DCbmulch = sc%Cbmulch - sc%Cbmulch0
    sc%DCmulch  = sc%Cmulch - sc%Cmulch0
    sc%DCrprof  = sc%Crprof - sc%Crprof0


    !sc%CsurNrac_out = p%QCrac/p%QNrac
    !sc%CsurNracmort = p%QCracmort/p%QNracmort
    sc%DChumt       = sc%Chumt - sc%Chumt0
    sc%DNbmulch     = sc%Nbmulch - sc%Nbmulch0
    sc%DNhumt       = sc%Nhumt - sc%Nhumt0
    sc%DNmulch      = sc%Nmulch - sc%Nmulch0
    sc%DNr          = sc%Nr - sc%Nr0
    sc%DNrprof      = sc%Nrprof - sc%Nrprof0
    sc%DSMN         = sc%SMN - sc%SMN0
    sc%DSOC         = sc%SOC - sc%SOC0
    sc%DSOCtot      = sc%SOCtot - sc%SOCtot0
    sc%DSON         = sc%SON - sc%SON0
    sc%DSONtot      = sc%SONtot - sc%SONtot0
    sc%DSTN         = sc%STN - sc%STN0


   ! Qlesd  ! on garde ca comme dans la v9.0


    sc%Qmin     = sc%Qminh + sc%Qminr
    sc%SOCbalance     = sc%SOCinputs-sc%QCO2sol

! Bruno octobre 2018 Soil available water par couche de sol
! Modif Florent C. et Fabien Octobre 2019
! On ne s interesse ici qu a l eau disponible pour la plante, donc >humin !
! Pour eviter que la variable SoilAvW soit negative, on borne a humin

    sc%SoilAvW_by_layers(1) = calculRU(soil%P_epc(1), max(sc%humin, sc%hur+sc%sat), sc%humin)

    nl1 = soil%P_epc(1)
    nl2 = nl1 + soil%P_epc(2)
    sc%SoilAvW_by_layers(2) = calculRU(nl2, max(sc%humin, sc%hur+sc%sat), sc%humin) - &
        calculRU(nl1, max(sc%humin, sc%hur+sc%sat), sc%humin)

    nl1 = soil%P_epc(1) + soil%P_epc(2)
    nl2 = nl1 + soil%P_epc(3)
    sc%SoilAvW_by_layers(3) = calculRU(nl2, max(sc%humin, sc%hur+sc%sat), sc%humin) - &
        calculRU(nl1, max(sc%humin, sc%hur+sc%sat), sc%humin)

    nl1 = soil%P_epc(1) + soil%P_epc(2) + soil%P_epc(3)
    nl2 = nl1 + soil%P_epc(4)
    sc%SoilAvW_by_layers(4) = calculRU(nl2, max(sc%humin, sc%hur+sc%sat), sc%humin) - &
         calculRU(nl1, max(sc%humin, sc%hur+sc%sat), sc%humin)

    nl1 = soil%P_epc(1) + soil%P_epc(2) + soil%P_epc(3) + soil%P_epc(4)
    nl2 = nl1 + soil%P_epc(5)
    sc%SoilAvW_by_layers(5) = calculRU(nl2, max(sc%humin, sc%hur+sc%sat), sc%humin) - &
        calculRU(nl1, max(sc%humin, sc%hur+sc%sat), sc%humin)

  ! DR 17/11/2021 fin des rajouts
    soil%concN_W_drained=0.
    if(sc%drain.gt.0.)soil%concN_W_drained = sc%lessiv/sc%drain*6200./14.

end subroutine Stics_Calcul_Sorties

    !> Return the current phenology stage as a BBCH code
    !! Search the last stage that is not zero based on the simulation day
    pure integer function get_current_bbch_code(sim_day, plant) result(bbch_code)
        integer, intent(in)  :: sim_day
        type(Plante_), intent(in)  :: plant

        character(len=3) :: bbch_code_c

        bbch_code_c = '-01'

        if (sim_day >= plant%nplt .and. plant%nplt /= 0) bbch_code_c = plant%P_stadebbchplt
        if (sim_day >= plant%nger .and. plant%nger /= 0) bbch_code_c = plant%P_stadebbchger
        if (sim_day >= plant%nlev .and. plant%nlev /= 0) bbch_code_c = plant%P_stadebbchlev
        if (sim_day >= plant%namf .and. plant%namf /= 0) bbch_code_c = plant%P_stadebbchamf
        if (sim_day >= plant%nlax .and. plant%nlax /= 0) bbch_code_c = plant%P_stadebbchlax
        if (sim_day >= plant%nflo .and. plant%nflo /= 0) bbch_code_c = plant%P_stadebbchflo
        if (sim_day >= plant%ndrp .and. plant%ndrp /= 0) bbch_code_c = plant%P_stadebbchdrp
        if (sim_day >= plant%nsen .and. plant%nsen /= 0) bbch_code_c = plant%P_stadebbchsen
        if (sim_day >= plant%ndebdes .and. plant%ndebdes /= 0) bbch_code_c = plant%P_stadebbchdebdes
        if (sim_day >= plant%nmat .and. plant%nmat /= 0) bbch_code_c = plant%P_stadebbchmat
        if (sim_day == plant%nrec .and. plant%nrec /= 0) bbch_code_c = plant%P_stadebbchrec
        if (sim_day > plant%nrec .and. plant%nrec /= 0) bbch_code_c = '-01'

        bbch_code = bbch_string_to_int(bbch_code_c)
    end function
end module Stics_Calcul_Sorties_m
 
