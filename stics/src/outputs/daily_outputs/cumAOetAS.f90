! sous-programme qui cumule les variables d'etat ombre et soleil
! pour ne faire qu'une sortie pour la culture
! TODO: faire des cumuls au fur et a mesure des calculs et non pas tout a la fois en fin de boucle journaliere.
! subroutine that combines the state variables shade and sun
!! to make only outlet for culture
module cumAOetAS_m
use Stics
USE Plante
USE Itineraire_Technique
implicit none
private
public :: cumAOetAS
contains
subroutine cumAOetAS(n,P_codesimul,p,itk)
  integer,            intent(IN)    :: n  
  character(len=*),   intent(IN)    :: P_codesimul  ! // PARAMETER // simulation code (culture ou feuille=lai force) // SD // P_USM/USMXML // 0 

  type(Plante_),      intent(INOUT) :: p  
  type(ITK_),         intent(IN)    :: itk  

!: Variables locales
  integer :: iz
!  integer :: AO
!  integer :: AS
  real    :: rdtote  


!      AS = 1  ! au soleil
!      AO = 2  ! a l'ombre

! ** variables a ponderer
! *- ici les variables "xxxT" sont des variables "temporaires" de cumul
! *- utilises normalement uniquement dans les fonctions de sorties
! *- (et notamment sortie.for et lecsorti.for)
! *- et les variables "xxx(0)" sont les memes variables mais a portees
! *- plus large que sortie.for et lecsorti.for
! *- typiquement, on les retrouve dans rapport.for et lecrap.for,
! *- ou bilan.for et bilcoupe.for, et parfois ailleurs mais tres rarement.

      p%mouill(0) = p%mouill(AS) * p%surf(AS) + p%mouill(AO) * p%surf(AO)
      p%deltai(0,n) = p%deltai(AS,n) * p%surf(AS) + p%deltai(AO,n) * p%surf(AO)
      p%dltaisen(0) = p%dltaisen(AS) * p%surf(AS) + p%dltaisen(AO) * p%surf(AO)
      p%dltams(0,n) = p%dltams(AS,n) * p%surf(AS) + p%dltams(AO,n) * p%surf(AO)
      p%dltamsen(0) = p%dltamsen(AS) * p%surf(AS) + p%dltamsen(AO) * p%surf(AO)
      p%dltamstombe(0) = p%dltamstombe(AS) * p%surf(AS) + p%dltamstombe(AO) * p%surf(AO)
      p%dltags(0) = p%dltags(AS) * p%surf(AS) + p%dltags(AO) * p%surf(AO)
      p%dltaremobil(0) = p%dltaremobil(AS) * p%surf(AS) + p%dltaremobil(AO) * p%surf(AO)
      p%dltaremobilN(0) = p%dltaremobilN(AS) * p%surf(AS) + p%dltaremobilN(AO) * p%surf(AO)

    ! dr 151107 pour sorties climator
      p%photnet(0,n) = p%photnet(AS,n) * p%surf(AS) + p%photnet(AO,n) * p%surf(AO)
      p%eai(0) = p%eai(AS) * p%surf(AS) + p%eai(AO) * p%surf(AO)
      p%fapar(0) = p%fapar(AS) * p%surf(AS) + p%fapar(AO) * p%surf(AO)
      p%fpft(0) = p%fpft(AS) * p%surf(AS)+ p%fpft(AO) * p%surf(AO)
      p%fpv(0,n) = p%fpv(AS,n) * p%surf(AS)+ p%fpv(AO,n) * p%surf(AO)
      p%nfruitnou(0) = p%nfruitnou(AO) * p%surf(AO) + p%nfruitnou(AS) * p%surf(AS)
      p%pousfruit(0) = p%pousfruit(AO) * p%surf(AO) + p%pousfruit(AS) * p%surf(AS)
      p%pdsfruitfrais(0) = p%pdsfruitfrais(AO) * p%surf(AO) + p%pdsfruitfrais(AS) * p%surf(AS)
      p%raint(0)  = p%raint(AO) * p%surf(AO)+ p%raint(AS) * p%surf(AS)
      p%sourcepuits(0) = p%sourcepuits(AO) * p%surf(AO) + p%sourcepuits(AS) * p%surf(AS)
      p%splai(0) = p%splai(AO) * p%surf(AO) + p%splai(AS) * p%surf(AS)
      p%spfruit(0) = p%spfruit(AO) * p%surf(AO) + p%spfruit(AS) * p%surf(AS)
      p%sla(0) = p%sla(AO) * p%surf(AO) + p%sla(AS) * p%surf(AS)

      p%vitmoy(0) = p%vitmoy(AO) * p%surf(AO) + p%vitmoy(AS) * p%surf(AS)
      p%allocfruit(0) = p%allocfruit(AO) * p%surf(AO) + p%allocfruit(AS) * p%surf(AS)
      p%huile(0) = p%huile(AO) * p%surf(AO) + p%huile(AS) * p%surf(AS)
      p%sucre(0) = p%sucre(AO) * p%surf(AO) + p%sucre(AS) * p%surf(AS)
      p%h2orec(0) = p%h2orec(AO) * p%surf(AO) + p%h2orec(AS) * p%surf(AS)
      p%restemp(0) = p%restemp(AO) * p%surf(AO) + p%restemp(AS) * p%surf(AS)
      p%remobilj(0) = p%remobilj(AO) * p%surf(AO) + p%remobilj(AS) * p%surf(AS)
      p%mabois(0) = p%mabois(AO) * p%surf(AO) + p%mabois(AS) * p%surf(AS)
      p%maenfruit(0) = p%maenfruit(AO) * p%surf(AO) + p%maenfruit(AS) * p%surf(AS)
      p%mafrais(0) = p%mafrais(AO) * p%surf(AO) + p%mafrais(AS) * p%surf(AS)
      p%mafeuil(0) = p%mafeuil(AO) * p%surf(AO) + p%mafeuil(AS) * p%surf(AS)
      p%mafeuiljaune(0) = p%mafeuiljaune(AO) * p%surf(AO) + p%mafeuiljaune(AS) * p%surf(AS)
      p%mafeuilverte(0) = p%mafeuilverte(AO) * p%surf(AO) + p%mafeuilverte(AS) * p%surf(AS)
      p%mafeuiltombe(0) = p%mafeuiltombe(AO) * p%surf(AO) + p%mafeuiltombe(AS) * p%surf(AS)
      p%masecveg(0) = p%masecveg(AO) * p%surf(AO) + p%masecveg(AS) * p%surf(AS)
      p%matigestruc(0) = p%matigestruc(AO) * p%surf(AO) + p%matigestruc(AS) * p%surf(AS)

      p%fixpot(0) = p%fixpot(AO) * p%surf(AO) + p%fixpot(AS) * p%surf(AS)
      p%fixmaxvar(0) = p%fixmaxvar(AO) * p%surf(AO) + p%fixmaxvar(AS) * p%surf(AS)
      p%durvie(0,n) = p%durvie(AO,n) * p%surf(AO) + p%durvie(AS,n) * p%surf(AS)

      p%QNplantetombe(0) = p%QNplantetombe(AO) * p%surf(AO) + p%QNplantetombe(AS) * p%surf(AS)
      p%QCplantetombe(0) = p%QCplantetombe(AO) * p%surf(AO) + p%QCplantetombe(AS) * p%surf(AS)

      p%msresgel(0) = p%msresgel(AO) * p%surf(AO) + p%msresgel(AS) * p%surf(AS)
      p%QNresgel(0) = p%QNresgel(AO) * p%surf(AO) + p%QNresgel(AS) * p%surf(AS)

! Ajout Lolo Juin 2018 : j'ai du indicer les organes perennes sur AO et AS pour pouvoir faire des CAS avec des cultures perennes
      p%maperenne(0) = p%maperenne(AO) * p%surf(AO) + p%maperenne(AS) * p%surf(AS)
      p%QNperenne(0) = p%QNperenne(AO) * p%surf(AO) + p%QNperenne(AS) * p%surf(AS)
      p%resperenne(0) = p%resperenne(AO) * p%surf(AO) + p%resperenne(AS) * p%surf(AS)
      p%QNresperenne(0) = p%QNresperenne(AO) * p%surf(AO) + p%QNresperenne(AS) * p%surf(AS)
      p%dltaperennesen(0) = p%dltaperennesen(AO) * p%surf(AO) + p%dltaperennesen(AS) * p%surf(AS)
      p%dltaQNperennesen(0) = p%dltaQNperennesen(AO) * p%surf(AO) + p%dltaQNperennesen(AS) * p%surf(AS)
      p%maperennemort(0) = p%maperennemort(AO) * p%surf(AO) + p%maperennemort(AS) * p%surf(AS)
      p%QNperennemort(0) = p%QNperennemort(AO) * p%surf(AO) + p%QNperennemort(AS) * p%surf(AS)
      p%QCperennemort(0) = p%QCperennemort(AO) * p%surf(AO) + p%QCperennemort(AS) * p%surf(AS)
      p%QCO2resperenne(0) = p%QCO2resperenne(AO) * p%surf(AO) + p%QCO2resperenne(AS) * p%surf(AS)
      ! Loic sept 2020 : ajout de masec
      p%masec(AS,n) = p%masecnp(AS,n) + p%maperenne(AS)
      p%masec(AO,n) = p%masecnp(AO,n) + p%maperenne(AO)
      p%masec(0,n) = p%masecnp(AS,n)*p%surf(AS) + p%masecnp(AO,n)*p%surf(AO) &
                     + p%maperenne(AS)*p%surf(AS) + p%maperenne(AO)*p%surf(AO)

 ! DR 07/06/2017  on cumule ici les composantes de la varaibles leai pour chaque sous-systeme
      p%leai(0) = p%leai(AS) * p%surf(AS) + p%leai(AO) * p%surf(AO)
      do iz = 1, p%P_nboite
        p%pdsfruit(0,iz) = p%pdsfruit(AO,iz) * p%surf(AO) + p%pdsfruit(AS,iz) * p%surf(AS)
        p%nfruitv(AO,iz) = p%nfruit(AO,iz)
        p%nfruitv(AS,iz) = p%nfruit(AS,iz)
      end do


! *** // variables a cumuler (une seule fois !) //

      if (P_codesimul == CODE_CULTURE) then
        if (p%P_codelaitr == 1) p%lai(0,n) = p%lai(AS,n) * p%surf(AS) + p%lai(AO,n) * p%surf(AO)
! Bruno juin 2017 curieux calcul, je le remplace
!        p%laisen(0,n) = p%laisen(0,n-1)                                   &
!                      + (p%laisen(AO,n) - p%laisen(AO,n-1)) * p%surf(AO)  &
!                      + (p%laisen(AS,n) - p%laisen(AS,n-1)) * p%surf(AS)
        p%laisen(0,n) = p%laisen(AS,n) * p%surf(AS) + p%laisen(AO,n) * p%surf(AO)
! fin modif
      else
        p%lai(0,n) = p%lai(AS,n)
        p%laisen(0,n) = p%laisen(AS,n)
        p%deltai(0,n) = max(p%lai(AS,n) - p%lai(AS,n-1),0.0)
      endif
! Bruno fevrier 2018 curieux calcul, je le remplace
!      p%magrain(0,n) = p%magrain(0,n-1)                                   &
!                     + (p%magrain(AO,n) - p%magrain(AO,n-1)) * p%surf(AO) &
!                     + (p%magrain(AS,n) - p%magrain(AS,n-1)) * p%surf(AS)
      p%magrain(0,n) = p%magrain(AS,n) * p%surf(AS) + p%magrain(AO,n) * p%surf(AO)

!: Cumul des rendements
      if (n == p%nrec .and. p%nbrecolte > 1) then
        p%rdtint(0,p%nbrecolte-1) = p%rdtint(AS,p%nbrecolte-1) * p%surf(AS) + p%rdtint(AO,p%nbrecolte-1) * p%surf(AO)
      endif

      if (p%masecnp(0,n) <= 0.) p%masecnp(0,n) = p%masecnp(0,n-1) + p%dltams(0,n) - p%dltamstombe(0)

    ! Si recolte/cueillette, on enleve la partie recoltee/cueillee du cumul de masec
      if (itk%P_codcueille == 2) then
        if (n == p%nrec .and. p%nrec > 0) then
          if (itk%P_nbcueille == 1) rdtote = p%magrain(0,n-1)/100.
          if (itk%P_nbcueille == 2) rdtote = p%rdtint(0,p%nbrecolte-1)/100.
        endif
      endif

      if (p%masecnp(0,n) > 0.) then
        p%ptigestruc(0)    = p%matigestruc(0)  / p%masecnp(0,n)
        p%penfruit(0)      = p%maenfruit(0)    / p%masecnp(0,n)
        p%preserve(0)      = p%restemp(0)      / p%masecnp(0,n)
        p%pfeuil(0,n)      = p%mafeuil(0)      / p%masecnp(0,n)
        p%pfeuiljaune(0)   = p%mafeuiljaune(0) / p%masecnp(0,n)
        p%pfeuilverte(0,n) = p%mafeuilverte(0) / p%masecnp(0,n)
      else
        p%ptigestruc(0)    = 0.
        p%penfruit(0)      = 0.
        p%preserve(0)      = 0.
        p%pfeuil(0,n)      = 0.
        p%pfeuiljaune(0)   = 0.
        p%pfeuilverte(0,n) = 0.
      endif

      if (n == p%nlev) then
        if (p%P_codelaitr == 1) then
          if (p%P_lai0 > p%lai(0,n)) p%lai(0,n) = p%P_lai0
        endif
      endif

      p%hauteur(0) = p%hauteur(AO) * p%surf(AO) + p%hauteur(AS) * p%surf(AS)
      p%mafraisfeuille(0) = p%mafraisfeuille(AS) * p%surf(AS) + p%mafraisfeuille(AO) * p%surf(AO)
      p%mafraisrec(0)     = p%mafraisrec(AS)     * p%surf(AS) + p%mafraisrec(AO)     * p%surf(AO)
      p%mafraisres(0)     = p%mafraisres(AS)     * p%surf(AS) + p%mafraisres(AO)     * p%surf(AO)
      p%mafraistige(0)    = p%mafraistige(AS)    * p%surf(AS) + p%mafraistige(AO)    * p%surf(AO)

! Lolo tentative resolution bug...
      if (p%lai(AS,n) <= 0. .and. p%lai(AO,n) <= 0.) p%lai(0,n) = 0.

      if (p%masecnp(AO,n) <= 0. .and. p%masecnp(AS,n) <= 0.) then
            p%magrain(0,n) = 0.
            p%masecnp(0,n) = 0.
            p%laisen(0,n) = 0.
            p%msres(0) = 0.
      ! on saute ces mises a zeros, valables que pour la plante associee
            if (.not. p%is_dominant) p%msneojaune(0) = 0.
       endif

! DR 07/06/2017 je rajoute la variable leai surface photosynthique somme lu lai et du eai
      p%leai(0) = p%leai(AS) * p%surf(AS) + p%leai(AO) * p%surf(AO)




    ! variables cumulees

      p%interpluie(0) = p%interpluie(AO) + p%interpluie(AS)
      p%eop(0)        = p%eop(AO) * p%surf(AO)     + p%eop(AS) * p%surf(AS)
      p%ep(0)         = p%ep(AO)  * p%surf(AO)     + p%ep(AS)  * p%surf(AS)
      p%QNgrain(0)    = p%QNgrain(AO) * p%surf(AO) + p%QNgrain(AS) * p%surf(AS)


!: Variables recalculees

      if (.not. p%is_dominant) then
        if (p%magrain(0,n) > 0.) then
          p%CNgrain(0) = p%QNgrain(0) / p%magrain(0,n) * 10.
        else
          p%CNgrain(0) = 0.
        endif

        if (p%masecnp(0,n) > 0.0) then
          p%CNplante(0) = p%QNplantenp(0,n) / (p%masecnp(0,n) * 10.)
        else
          p%CNplante(0) = 0.
        endif

        p%swfac(0)    = min(p%swfac(AO), p%swfac(AS))
        p%inn(0)      = min(p%inn(AO), p%inn(AS))
        p%inns(0)     = min(p%inns(AO), p%inns(AS))
        p%innsenes(0) = min(p%innsenes(AO), p%innsenes(AS))
        p%innlai(0)   = min(p%innlai(AO), p%innlai(AS))
        p%turfac(0)   = min(p%turfac(AO), p%turfac(AS))

        p%senfac(0)   = min(p%senfac(AO), p%senfac(AS))
        p%epsib(0)    = (p%epsib(AO) + p%epsib(AS)) / 2.
        p%irazo(0,n)  = (p%irazo(AO,n) + p%irazo(AS,n)) / 2.
        p%ircarb(0,n) = (p%ircarb(AO,n) + p%ircarb(AS,n)) / 2.
        p%pgrain(0)   = (p%pgrain(AO) + p%pgrain(AS)) / 2.
        p%teaugrain(0) = (p%teaugrain(AO) + p%teaugrain(AS)) / 2.
      else
        p%CNgrain(0)    = p%CNgrain(AS)
        p%CNplante(0)   = p%CNplante(AS)
        p%swfac(0)      = p%swfac(AS)
        p%inn(0)        = p%inn(AS)
        p%inns(0)       = p%inns(AS)
        p%innsenes(0)   = p%innsenes(AS)
        p%innlai(0)     = p%innlai(AS)
        p%turfac(0)     = p%turfac(AS)

        p%senfac(0)     = p%senfac(AS)
        p%epsib(0)      = p%epsib(AS)
        p%irazo(0,n)    = p%irazo(AS,n)
        p%ircarb(0,n)   = p%ircarb(AS,n)
        p%pgrain(0)     = p%pgrain(AS)
        p%teaugrain(0)  = p%teaugrain(AS)
      endif

return
end subroutine cumAOetAS
end module cumAOetAS_m
 
