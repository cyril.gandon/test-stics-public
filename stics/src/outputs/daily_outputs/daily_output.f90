!! Module to write the dailies output variables
!! Variables are in var.mod
module daily_output_m
   use stdlib_strings, only: to_string
   use stdlib_string_type
   use string_utils, only: concat_array_string
   implicit none

   !> Separator
   character(*), parameter :: s = ';'

   type :: column_format
      integer :: column_size, precision
      !> F for decimal form (12340), E for scientific form (123.4E2)
      character(1) :: form
   end type

   !> Format of the line of the daily report
   type :: daily_formats_
      !> Format of the header of the daily report
      character(:), allocatable :: header_formats
      !> Format of rows of values of the daily report
      character(:), allocatable :: row_formats
   end type
contains

   !> Write the header in the dailies output
   subroutine write_header(unit, fmt, daily_var_names)
      integer, intent(in) :: unit
      type(daily_formats_), intent(in) :: fmt
      type(string_type), allocatable, intent(in) :: daily_var_names(:)

      integer :: k

      write (unit, fmt%header_formats) 'ian', s, 'mo', s, 'jo', s, 'jul', s, 'pla', &
         (s, char(daily_var_names(k)), k=1, size(daily_var_names))
   end subroutine

   !> Write a line of values in the dailies output
   subroutine write_row(unit, fmt, year, month, day, julian_day, plant_code, values)
      integer, intent(in) :: unit
      type(daily_formats_), intent(in) :: fmt
      integer, intent(in) :: year, month, day, julian_day
      character(len=3), intent(in) :: plant_code
      real, allocatable, intent(in) :: values(:)

      integer :: k

      write (unit, fmt%row_formats) year, s, month, s, day, s, julian_day, s, plant_code, (s, values(k), k=1, size(values))
   end subroutine

   pure type(daily_formats_) function build_daily_formats(daily_var_names, exponent_form) result(fmt)
      type(string_type), allocatable, intent(in) :: daily_var_names(:)
      logical, intent(in) :: exponent_form

      integer :: k
      type(string_type), allocatable :: header_formats(:), row_formats(:)
      type(column_format), allocatable :: column_formats(:)
      type(column_format) :: cfmt

      column_formats = get_format(daily_var_names, exponent_form)
      allocate (header_formats(size(column_formats)))
      allocate (row_formats(size(column_formats)))

      do k = 1, size(column_formats)
         cfmt = column_formats(k)
         header_formats(k) = ',a1,a'//to_string(cfmt%column_size)
         row_formats(k) = ',a1,'//cfmt%form//to_string(cfmt%column_size)//'.'//to_string(cfmt%precision)
      end do

      fmt%header_formats = '(a4,a1,a2,a1,a2,a1,a3,a1,a3'//concat_array_string(header_formats)//')'
      fmt%row_formats = '(i4,a1,i2,a1,i2,a1,i3,a1,a3'//concat_array_string(row_formats)//')'
   end function

   !> Get the formating of a column by on the length of the header
   elemental type(column_format) function get_format(column_name, exponent_form) result(fmt)
      type(string_type), intent(in) :: column_name
      logical, intent(in) :: exponent_form

      integer :: var_len

      var_len = len(column_name)
      if (exponent_form) then
         fmt%column_size = max(var_len, 16)
         fmt%precision = 7
         ! Exponential form
         fmt%form = 'e'
      else
         fmt%column_size = max(var_len, 12)
         fmt%precision = 5
         ! Decimal form
         fmt%form = 'f'
      end if
   end function
end module
