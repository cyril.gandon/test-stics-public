! Module Bilans
!! Description :
!! writing of the balance file (description of the inputs, phenologicals stages, harvests components, water balance, Nitrogen balance, Carbon balance)
module Bilans
use phenology_utils
use stics_system
use stics_files
use Stics
use Plante
use Itineraire_Technique
use Sol
use Station
use Parametres_Generaux
use dates_utils, only: get_day_month, modulo_years
use messages
use Climat
use bilan_messages
use call_num_version_m, only: call_num_version
use plant_utils
use climate_utils
implicit none

private
public :: ecrireEnteteBilan, ecrireBilanIntercoupe, ecrireBilanFinCultureFauchee, ecrireBilanEauCN, ecrireBilanDefaut

contains

! *********************************************************
!*************************************************************************************************
! subroutine ecrireEnteteBilan
! Description : header part of the file balance
! *********************************************************
subroutine ecrireEnteteBilan(logger, plant_index, sc,pg,p,itk,soil,sta,t,fb)
  type(logger_), intent(in) :: logger
  integer, intent(in) :: plant_index
  type(Stics_Communs_),       intent(IN) :: sc  
  type(Parametres_Generaux_), intent(IN) :: pg  
  type(Plante_),              intent(IN) :: p  
  type(ITK_),                 intent(IN) :: itk  
  type(Sol_),                 intent(IN) :: soil  
  type(Station_),             intent(IN) :: sta  
  type(Stics_Transit_),       intent(IN) :: t
  integer, intent(inout) :: fb

  integer :: iz, i, ifwater1an
  integer :: jour, nummois, ancours, iostat
  character(len=20) :: tag
  character(len=3) :: month_code
  character(:), allocatable :: path, filename
  character(len=50) codeversion
  character(:), allocatable :: method_etp

  path = join_path(logger%output_path, p%bilan_output_filename)
  if (sc%numcult .eq. 1) then
    open (newunit=fb, file=path, status=replace_, action=write_, iostat=iostat)
    if (iostat .ne. 0) then
      call exit_error(logger, "Error opening bilan file: [" // path // "]")
    end if
  end if

! DONNEES d'ENTREES ecrites en debut de fichier bilan

        if (sc%numcult > 1 .or. itk%P_codefauche == 1) write(fb,mes3181) sc%annee(sc%ifwater_courant)

        call call_num_version(codeversion)
        if (itk%P_codefauche == 1) then
           write(fb,mes2001) codeversion, sc%P_codesimul
        else
           write(fb,mes1008) codeversion, sc%P_codesimul
        endif
    ! DR 20/08/2012 en cas d'utilisation du forcage lai  on ecrit le nom du fichier lai
        if (sc%P_codesimul == CODE_FEUILLE) then
          write(fb,mes1013) p%P_flai
        endif

        if(sc%P_nbplantes == 1) then
           write(fb,mes1009)
        else
           write(fb,mes1007)
        endif
        write(fb,mes1010) sc%P_wdata1(1:index(sc%P_wdata1,'.')-1)
        if (sta%P_codaltitude == 2 .and. abs(sta%P_altistation - sta%P_altisimul).gt.1.0E-8 ) then
          write(fb,mes3008) sta%P_altistation
          write(fb,mes3009) sta%P_altisimul
          if (sta%P_codadret == 1) write(fb,mes3110)
          if (sta%P_codadret == 2) write(fb,mes3111)
        endif

        write(fb,mes1011) p%P_ftec

        if (p%P_codeplante /= CODE_BARESOIL) then
           write(fb,mes1012) p%P_fplt
           if (p%P_codeplante == CODE_STN) then
            write(fb,mes1013) p%P_flai
           end if
           write(fb,mes2014) p%P_codevar(itk%P_variete), itk%P_variete
        endif
! j'ajoute le nom des fichiers d'initialisation

        write(fb,mes10111) trim(adjustl(sc%P_ficInit))

        write(fb,mes1014) soil%P_typsol
        do iz = 1, sc%nh
          write(fb,50) real(soil%P_epc(iz)),sc%P_Hinitf(iz),soil%P_NO3initf(iz),sc%P_NH4initf(iz)
50        format(f11.0,f12.1,2f14.1)
        end do

        if (p%P_codeplante == CODE_BARESOIL) write(fb,mes1065) p%rmaxi, 0.

! DR 03/08/2015 je mets un test pour indiquer quand on est dans le cadre d'une rotation
       if(sc%P_codesuite == 1)then
           if(sc%nbans.eq.1)then
               write(fb,mes449)
           else
               if(pg%P_codeinitprec == 2.and. sc%numcult.gt.1 ) write(fb,mes449)
           endif
       endif


    !. Initialisations plante
        if (p%codeinstal == 1) then
          write(fb,mes1020)
          if (p%P_codelaitr == 1) then
            write(fb,47) p%P_stade0,p%lai(AOAS,0),p%masecnp(AOAS,0),p%P_zrac0,     &
                         p%magrain(AOAS,0),p%P_QNplantenp0,p%inn0(AOAS),p%P_restemp0
          else
            write(fb,47) p%P_stade0,sc%tauxcouv(0),p%masecnp(AOAS,0),p%P_zrac0,    &
                         p%magrain(AOAS,0),p%P_QNplantenp0,p%inn0(AOAS),p%P_restemp0
          endif
47        format(5x,a3,f7.2,f7.1,f7.0,f7.1,f7.0,f7.2,f7.1)
        endif

        call get_day_month(sc%P_iwater,sc%ansemis,month_code,jour,nummois)
        write (fb,mes1021) jour,month_code,sc%annee(sc%P_iwater),sc%P_iwater
        ancours = sc%annee(sc%ifwater_courant)
        if (sc%ifwater_courant  >  sc%nbjsemis) then
          ifwater1an = sc%ifwater_courant - sc%nbjsemis
        else
          ifwater1an = sc%ifwater_courant
        endif

        call get_day_month(ifwater1an,ancours,month_code,jour,nummois)
        if (itk%P_codefauche == 1) then
          write (fb,mes2017) jour,month_code,ancours,ifwater1an
        else
          write (fb,mes1022) jour,month_code,ancours,ifwater1an,sc%ifwater_courant
        endif

        if (itk%P_codefauche == 1) then
          if (itk%lecfauche) then
            if (itk%P_codemodfauche == 2) then
              write(fb,mes2018)
            else
              write(fb,mes2019)
              if (itk%P_codetempfauche == 1) then
                write(fb,mes2031)
              else
                write(fb,mes2032)
              endif
            endif
          else
            write(fb,mes2020) itk%P_stadecoupedf
          endif
          write(fb,mes2027) itk%nbcoupe, (itk%P_julfauche(i), i=1,itk%nbcoupe)
        endif

      ! domi 30/09/97 type d'irrigation
        if (itk%P_codecalirrig == 2) then
          write(fb,mes2021)
        else
          write(fb,mes2022) itk%P_ratiol
          if(itk%P_codedate_irrigauto == IRRIG_AUTO_DATE) then
            write(fb,mes20221)itk%P_datedeb_irrigauto,itk%P_datefin_irrigauto
          end if
        endif
      ! domi 30/09/97 type de fertilisation
        if (t%P_codecalferti == 2) then
          write(fb,mes2023)
        else
          write(fb,mes2024) t%P_ratiolN
        endif

        if (soil%P_codrainage == 1) then
          write(fb,mes3011) pg%P_Bformnappe,soil%Ldrains,soil%P_Ksol,   &
                            soil%P_profdrain,p%P_sensanox,pg%P_distdrain
          write(fb,mes3015) soil%P_Infil(sc%NH)
        endif

      method_etp = etp_code_to_string(sta%P_codeetp)
      write(fb,mes448) method_etp
return
end subroutine ecrireEnteteBilan



! subroutine ecrireBilanDefaut
!! Description :
!! second part of the file balance (irrigations and fertilisations, phenologicals stages, harvests components, water balance, Nitrogen balance, Carbon balance)
!!  harvests components, water balance, Nitrogen balance, Carbon balance)
! **********************************************
subroutine ecrireBilanDefaut(sc,pg,soil,p,itk, c)
    integer, parameter :: nb_stades_max = 30
    type(Stics_Communs_),       intent(INOUT) :: sc
    type(Parametres_Generaux_), intent(IN)    :: pg  
    type(Sol_),                 intent(IN)    :: soil  
    type(Plante_),              intent(INOUT) :: p  
    type(ITK_),                 intent(IN)    :: itk
    type(Climat_),              intent(IN)    :: c

    integer :: jour, nummois
    integer :: julrec, dureecycle, irecolte
    character(len=3)  :: mois
    real    ::  stcum, stcumdrp
    real    ::  rdtote, effN
    real    ::  msrec, rendement, pgrainfrais
    real    ::  qresMS, Corg, Norg, Nmin
    real    ::  cellulose
    real    ::  Cplante
    real    ::  nbdegrains

    integer :: nbstades
    character(len=3), dimension(nb_stades_max) :: stades
    real    :: aerialbiom, QNaerialbiom
    integer :: i, nmax
    integer :: fb

    fb = p%bilan_unit
    !!!!!!!!!!!!!!!!!!!!! snow balance informations !!!!!!!!!!!!!!!!!!!!!!!!!!!
    call snowBalance(fb,c,pg%P_codesnow,sc%numcult)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    call bilanIrrigation(sc,1,sc%n,fb,'final',itk%P_codedateappH2O)
! FERTILISATIONS
    call bilanFertilisations(sc,itk,1,sc%n,fb,'final')

! ** APPORTS DE RESIDUS DE CULTURE
    write(fb,mes1029)
! Travail du sol / Apports de residus organiques
!     1) pas de travail du sol ni d'incorporation de residus
    if(itk%P_nbjtrav == 0 .and. itk%P_nbjres == 0) then
         write(fb,mes1030)
    else
!     2)travail du sol
        do i = 1,itk%P_nbjres
             qresMS = itk%P_qres(i) * (1. - itk%P_eaures(i)/100.)
             ! DR 18/05/2022 on corrige Crespc est en %DW  donc c'est qresMS en sec aussi qu'il faut utiliser pour le calcul de Corg
             ! Corg = itk%P_qres(i) * itk%P_Crespc(i) * 10.
             Corg = qresMS * itk%P_Crespc(i) * 10.
             Norg = 0.
             if(itk%P_CsurNres(i) > 1.e-8) Norg = Corg / itk%P_CsurNres(i)
             Nmin = itk%P_qres(i) * itk%P_Nminres(i) * 10.
             if(itk%P_julres(i) < 999 .and. Corg /= 0.) &
!            write(fb,mes1032) itk%P_coderes(i), itk%P_julres(i), itk%P_profres(i), qresMS, Corg, Norg, Nmin
             write(fb,mes1032) itk%P_coderes(i), itk%P_julres(i), qresMS, Corg, Norg, Nmin
        end do
        do i = 1,itk%P_nbjtrav
!            if(itk%P_jultrav(i) < 999) write(fb,mes1031) itk%P_jultrav(i), itk%P_proftrav(i)
             if(itk%P_jultrav(i) < 999) write(fb,mes1031) itk%P_jultrav(i), itk%P_profres(i),itk%P_proftrav(i)
        end do
    endif

    if (p%P_codeplante /= CODE_BARESOIL) then

     ! Operations particulieres
      if(itk%P_codrognage == 2)  write(fb,*) mes410
      if(itk%P_codeffeuil == 2)  write(fb,*) mes411

    ! bilan DEVELOPPEMENT
      write (fb,mes1033)
!      if (p%P_codephot == 1 .and. p%P_phobase(itk%P_variete) < p%P_phosat(itk%P_variete)) write (fb,mes1040)
!      if (p%P_codephot == 1 .and. p%P_phobase(itk%P_variete) > p%P_phosat(itk%P_variete)) write (fb,mes1041)
!      if (p%P_codephot == 1 .and. p%P_jvc(itk%P_variete) > 0)   write (fb,mes1042)
!      if (p%P_codephot == 1 .and. p%P_jvc(itk%P_variete) == 0)  write (fb,mes1043)
!      if (p%P_codephot == 2 .and. p%P_jvc(itk%P_variete) == 0)  write (fb,mes1044)
 if (p%P_codephot == 1) then! les plantes photoperiodiques
             ! on traite les jours courts/longs
 if(p%P_phobase(itk%P_variete) < p%P_phosat(itk%P_variete)) write (fb,mes1040)
 if(p%P_phobase(itk%P_variete) > p%P_phosat(itk%P_variete)) write (fb,mes1041)
 if(p%P_codebfroid ==2)write(fb, mes1042)   ! vernalisation et photoperiode
 if(p%P_codebfroid ==3)write(fb, mes1042b)  ! dormance et photoperiode
 if(p%P_codebfroid ==1)write(fb, mes1043)   ! photoperiode seule
 else ! les plantes non photoperiodes
 if(p%P_codebfroid ==1)write(fb, mes1044)  ! dormance
 if(p%P_codebfroid ==2)write(fb, mes1044c) ! vernalisation
 if(p%P_codebfroid ==3)write(fb, mes1044b) ! dormance
      endif
      ! la temperature
      if (p%P_codetemp == 1) write (fb,mes1045)
      if (p%P_codetemp == 2) write (fb,mes1046)

!
! DR 17/08/06 on rajoute un test sur nlev
! pour que la premiere annee on ne calcule pas de levee (debourrement)
! ces 3 conditions servent a sortir du fb pour ne pas ecrire des stades faux
! la premiere annee d'un enchainement de perenne avec bidabe
! ajout Bruno mais A REVOIR
      if (p%P_codedormance == 3 .and. pg%P_codeinitprec == 2 .and. sc%numcult == 1) p%nlev = 0

      if ( abs(p%rfvi).lt.1.0E-8 .and. p%nlev == 0 ) then
        write(fb,mes1050)
!      return
!Modif Bruno juillet 2018 : ndes ne peut plus etre nul
!      if(p%ndes == 0 .or. p%ndes > 731) return
       if(p%ndes > 731) return
      endif
      if (p%ilevs == 0) then
        write(fb,mes1050)
!      return
!       if(p%ndes == 0 .or. p%ndes > 731) return
        if(p%ndes > 731) return
      endif

    ! parcours de developpement de la culture
      write(fb,mes2076)
      stcum = 0.
      stcumdrp = 0.
      nbstades = 3
!      allocate(stades(nbstades))
      stades(1:nbstades) = (/ddo,fdo,plt/)
      call bilanStadesDeveloppement(fb,sc,p,itk,nbstades,stades,stcum,stcumdrp)
!      deallocate(stades)

    ! stades vegetatifs
       write(fb,mes1053)
     ! DR 19/07/2012 je rajoute le stade germination
    !DR 23/08/2012 pour la vigne je n'ecris pas le stade germination
      if(p%P_codeplante.eq.CODE_VINE)then
         nbstades = 5
!         allocate(stades(nbstades))
         stades(1:nbstades) = (/lev,amf,lax,sen,lan/)
      else
         nbstades = 6
!         allocate(stades(nbstades))
         stades(1:nbstades) = (/ger,lev,amf,lax,sen,lan/)
      endif
      call bilanStadesDeveloppement(fb,sc,p,itk,nbstades,stades,stcum,stcumdrp)

!      deallocate(stades)

    ! stades reproducteurs
      write(fb,mes1054)
      nbstades = 6
!      allocate(stades(nbstades))
      stades(1:nbstades) = (/flo,drp,des,mat,fde,rec/)
      call bilanStadesDeveloppement(fb,sc,p,itk,nbstades,stades,stcum,stcumdrp)
!      deallocate(stades)

    ! duree du cycle
      dureecycle = p%nrec - p%nplt
      if(p%nrec == 0) dureecycle = p%ndes- p%nplt
      write(fb,mes1055) dureecycle

    ! recolte
      call bilanRecolte(sc,p,itk,fb)
      if (p%group == -1) write(fb,mes1056)

! -------------------------------------------------------
!  3. CROISSANCE & ABSORPTION d'AZOTE
! -------------------------------------------------------

    ! ajout NB le 7/4/98 pour fruit
      if (p%P_codeindetermin == 2) then
        p%nbgrains(AOAS) = p%nbfruit + p%nfruit(AOAS,p%P_nboite)

        ! if (p%nbgrains(AOAS) == 0) then
        if (abs(p%nbgrains(AOAS)).lt.1.0E-8) then
          p%pgrain(AOAS) = 0.
          write(fb,mes2091)
        else
          p%pgrain(AOAS) = p%magrain(AOAS,p%nrec) / p%nbgrains(AOAS)
        endif
      endif

      if(itk%P_codcueille == 2) then
        if(itk%P_nbcueille == 1) p%nbrecolte=2
        do irecolte = 1,p%nbrecolte-1
          if(itk%P_nbcueille == 1) rdtote = p%magrain(AOAS,p%nrec)/100.
          if(itk%P_nbcueille == 2) rdtote = p%rdtint(AOAS,irecolte)/100.
          p%masecnp(AOAS,p%nrec) = p%masecnp(AOAS,p%nrec) + rdtote
        end do
      endif


      write(fb,mes1057)

    ! jour de recolte
      julrec = p%nrec + sc%P_iwater - 1 ! tCal1JanAbs
      if (julrec > sc%nbjsemis) then
        call get_day_month(julrec-sc%nbjsemis,sc%annee(julrec),mois,jour,nummois)
      else
        call get_day_month(julrec,sc%annee(julrec),mois,jour,nummois)
      endif
      write(fb,mes1157) jour,mois,sc%annee(julrec)

! les lignes qui suivent sont transferees dans "sortie" (lignes 81-98)
      msrec = amax1(1.- p%h2orec(AOAS),1.e-6)
 ! Rendement nul si magrain = 0 et si maperenne = 0
 !     if(p%magrain_nrec <= 0.) then
 !          if(p%P_code_acti_reserve == 2) then
 !             p%rendementsec = 0.
 ! Rendement = masec si magrain = 0 et si maperenne >0
 !          else
 !             p%rendementsec = p%masecnp_nrec
 !          endif
 ! Rendement = magrain si magrain > 0
 !     else
 !          p%rendementsec = p%magrain_nrec / 100.
 !     endif

      if(p%pgrain(AOAS) <= 0.)then
         pgrainfrais = 0.
      else
         pgrainfrais = p%pgrain(AOAS)/msrec
      endif
      nbdegrains = p%nbgrains(AOAS)

!      if(p%P_codeplante.eq.CODE_MEN) p%rendementsec = p%masec(AOAS,p%nrec)
! Inutile car on ne passe pas la en cas de culture fauchee
!      if(p%P_codeplante.eq.CODE_FODDER) p%rendementsec = p%mafauchetot
       rendement = p%rendementsec / msrec
 ! fin deplacement

! test pas genial car depend du nom du fichier betterave !!!
      if(p%P_codeplante.eq.CODE_BEET)then
      !           p%rendementsec = p%matuber_nrec
            pgrainfrais = rendement / p%densite * 1000.
            nbdegrains = p%P_nbinflo(itk%P_variete) * p%densite
      endif

 ! Modifs Bruno et Loic : cas des perennes que l'on recolte et que l'on taille (ex: miscanthus)
       if(p%nrec == 0 .or. p%nrec > 731) then
           nmax = p%ndes-1                           ! si la culture est detruite
           if(nmax < 0 .or. nmax > 731) nmax =sc%n   ! si la culture n'est pas detruite
       else
           nmax = p%nrec
       endif
       aerialbiom = p%masecnp(AOAS,nmax)
! A revoir
!       if(p%P_codeperenne == 2 .and. itk%P_codcueille == 1) then
!          aerialbiom = p%mabois(AOAS)
!          p%mabois(AOAS)= 0.
!       endif
       write(fb,mes1058) p%masecnp_nrec, p%rendementsec, p%h2orec(AOAS)*100., rendement, p%msneojaune(AOAS)
       write(fb,mes1060) nbdegrains
! Fin modifs
      write(fb,mes1059) p%densite, p%h2orec(AOAS)*100., pgrainfrais

      if(p%P_codeindetermin == 2 .and. p%P_codeplante.ne.CODE_BEET) then
        write(fb,mes1160) p%P_nbinflo(itk%P_variete)
        if(p%nbfruit > 0.) write(fb,mes1161) p%nbfruit, (p%magrain(AOAS,p%nrec) - p%pdsfruit(AOAS,p%P_nboite))/100.
      endif

      if(p%P_codeindetermin == 1) write (fb,mes1159) p%vitmoy(AOAS)
      if(p%P_phyllotherme > 0.) write (fb,mes2092) p%nbfeuille

    ! nombre de jours remplissage bloque par la temperature
      if(p%P_codetremp == 1) write(fb,mes2100) p%nbj0remp
      if(itk%P_codetaille == 2) write(fb,mes1360) p%mabois(AOAS)
    ! Loic Fevrier 2021 : pour avoir les reserves quand code_acti_reserve = 2
      if (p%P_code_acti_reserve == 1) then
        if (p%P_codeperenne == 2)  write(fb,mes1361) p%maperenne(AOAS)
      else
        if (p%P_codeperenne == 2)  write(fb,mes1361) p%restemp(AOAS)
      endif
   ! on prend la derniere valeur non nulle de QNplantenp
     if (p%nrec == 0) then
        QNaerialbiom = p%QNplantenp(AOAS,sc%n)
     else
        do i = p%nrec,1,-1
           QNaerialbiom = p%QNplantenp(AOAS,i)
           if(QNaerialbiom > 0.) EXIT
        end do
     endif
     if(p%QNrac == 0.) then
        write(fb,mes1061) QNaerialbiom, p%QNgrain_nrec, p%QNracmort, p%CNplante_nrec
     else
        write(fb,mes1061) QNaerialbiom, p%QNgrain_nrec, p%QNrac, p%CNplante_nrec
     endif
     if(p%magrain(AOAS,p%nrec) > 0.) write(fb,mes1461) p%CNgrain_nrec, p%CNgrain_nrec*5.70

    ! efficience de l'azote_engrais (CAU/CRU)
       if(sc%totapN > 0.) then
           effN = 1.-(soil%QNvoleng + soil%QNdenit + soil%QNorgeng) / sc%totapN
           write(fb,mes1258) effN
       endif

    ! indice de recolte, sucre et huile
      p%QCplante = 0.
      if (p%masecnp(AOAS,p%nrec) > 0 .and. p%magrain(AOAS,p%nrec) > 0.) then
         write(fb,mes1158) p%magrain(AOAS,p%nrec) / 100. /p%masecnp(AOAS,p%nrec)  ! Harvest index
         write(fb,mes1261) msrec*100., p%CNgrain_nrec * msrec
         if(p%sucre(AOAS) > 0.) write(fb,mes1262) p%sucre(AOAS)*100.
         if(p%huile(AOAS) > 0.) write(fb,mes1263) p%huile(AOAS)*100.
   ! modif Bruno mai 2012 : calcul de la quantite de C dans la plante (kg C/ha)
          cellulose = 1. - p%sucre(AOAS) - p%huile(AOAS)
          Cplante = 0.44*cellulose + 0.40*p%sucre(AOAS) + 0.76*p%huile(AOAS)
          p%QCplante = p%masecnp(AOAS,p%nrec) * Cplante * 1000.
      endif
! ---------------------------------------------------------------
!   4. Bilan eau, stress hydrique, azote et GEL sur la culture
! ---------------------------------------------------------------
      write(fb,mes1063)
      write(fb,mes1064) p%cetm, p%cet, p%ces, p%cep, p%cprecip
      write(fb,mes1065) p%rmaxi, p%zracmax
      write(fb,mes1066) p%swfac1moy, p%turfac1moy, p%inn1moy, p%diftemp1/p%nst1, p%exofac1moy,  &
                        p%etr_etm1moy, p%etm_etr1moy, p%swfac2moy, p%turfac2moy, p%inn2moy,     &
                        p%diftemp2/p%nst2, p%exofac2moy, p%etr_etm2moy, p%etm_etr2moy
! 24/01/2011 DR pb sur le test des seuils je corrige
      if (p%gel1 < 1.or.p%gel2 < 1.) write(fb,mes1166) (1.-p%gel1)*100.,(1.-p%gel2)*100.
      if (p%mortplante == 1)    write(fb,mes1167)
      if (p%gel3 < 1.)          write(fb,mes1168) (1.-p%fgelflo)*100.
      if (p%mortplanteN == 1)   write(fb,mes2097)
      if (pg%P_codeinnact == 2) write(fb,mes1067)
      if (pg%P_codeh2oact == 2) write(fb,mes1068)

    endif ! fin des resultats culture

  call ecrirebilanEauCN(sc,pg,soil,p,itk)

return
end subroutine ecrireBilanDefaut


! balance between two cuts for the cut crops
subroutine ecrireBilanIntercoupe(sc,pg,p,itk)
    integer, parameter :: nb_stades_max = 30
    type(Stics_Communs_),       intent(IN)    :: sc  
    type(Parametres_Generaux_), intent(IN)    :: pg  
    type(Plante_),              intent(INOUT) :: p  
    type(ITK_),                 intent(INOUT) :: itk

    integer           :: jul
    integer           :: ancours
    integer           :: jour
    integer           :: nummois
    integer           :: idebsen
    integer           ::  dureecycle
    character(len=3)  :: mois
    real              :: stcum
    real              ::  stcumdrp
    integer           :: nbstades
    character(len=3), dimension(nb_stades_max) :: stades
    integer :: fb
    
    fb = p%bilan_unit
    call modulo_years(sc%n + sc%P_iwater - 1, sc%annee(sc%P_iwater), jul, ancours)
    call get_day_month(jul,ancours,mois,jour,nummois)

    write (fb,mes2064) p%numcoupe, jour, mois, ancours, jul
! on ecrit si on a repousse la fauche
    if(p%nbrepoussefauche > 0) write(fb,mes2096) p%nbrepoussefauche

    if (abs(itk%P_hautcoupe(p%numcoupe) - 999.).lt.1.0E-8 .and. itk%P_code_hautfauche_dyn == 2) then
        write(fb,mes2069) itk%P_lairesiduel(p%numcoupe),itk%P_msresiduel(p%numcoupe)
    else
!        if (itk%P_hautcoupe(p%numcoupe) == 999) then
!          write(fb,mes2068) t%P_hautfauche*100.
!        else
          write(fb,mes2068) itk%P_hautcoupe(p%numcoupe)*100.
!        endif
    endif
    if (p%fauchediff) write(fb,mes2066) itk%mscoupemini_courant

! IRRIGATIONS
! ecriture du bilan d'irrigation pour la coupe
    call bilanIrrigation(sc,p%nplt,sc%n,fb,'coupe',itk%P_codedateappH2O)

! FERTILISATIONS
! ecriture du bilan des fertilisations pour la coupe

    call bilanFertilisations(sc,itk,p%nplt,sc%n,fb,'coupe')

! Pas de bilan des apports de residus de culture dans l'inter-coupe
    if (p%P_codeplante /= CODE_BARESOIL) then

     ! Operations particulieres
      if(itk%P_codrognage == 2)  write(fb,*) mes410
      if(itk%P_codeffeuil == 2)  write(fb,*) mes411

    ! bilan DEVELOPPEMENT
      write (fb,mes1033)
      if (p%P_codephot == 1 .and. p%P_phobase(itk%P_variete) < p%P_phosat(itk%P_variete)) write (fb,mes1040)
      if (p%P_codephot == 1 .and. p%P_phobase(itk%P_variete) > p%P_phosat(itk%P_variete)) write (fb,mes1041)
      if (p%P_codephot == 1 .and. p%P_jvc(itk%P_variete) > 0.)  write (fb,mes1042)
      ! if (p%P_codephot == 1 .and. p%P_jvc(itk%P_variete) == 0.0)  &
      if (p%P_codephot == 1 .and. abs(p%P_jvc(itk%P_variete)).lt.1.0E-8)  &
        write (fb,mes1043)

      ! if (p%P_codephot == 2 .and. p%P_jvc(itk%P_variete) == 0.0)  &
      if (p%P_codephot == 2 .and. abs(p%P_jvc(itk%P_variete)).lt.1.0E-8)  &
        write (fb,mes1044)

      if (p%P_codetemp == 1) write (fb,mes1045)
      if (p%P_codetemp == 2) write (fb,mes1046)

    ! recalcul des dates de stade dans le calendrier julien si elles sont simulees
      p%ilevs = p%nlev + sc%P_iwater - 1
      p%iamfs = p%namf + sc%P_iwater - 1
      p%ilaxs = p%nlax + sc%P_iwater - 1
      p%idrps = p%ndrp + sc%P_iwater - 1
      p%isens = p%nsen + sc%P_iwater - 1
      idebsen = p%ndebsen + sc%P_iwater - 1
      p%ilans = p%nlan + sc%P_iwater - 1
      p%imats = p%nmat + sc%P_iwater - 1
      p%irecs = p%nrec + sc%P_iwater - 1

    ! parcours de developpement de la culture
      write(fb,mes2076)

    ! premiere coupe
      if (p%numcoupe == 1) then
        stcum = 0.
        stcumdrp = p%stpltlev
        nbstades = 9
!        allocate(stades(nbstades))
        stades(1:nbstades) = (/lev,amf,lax,sen,lan,drp,mat,rec,cut/)
        call bilanStadesDeveloppement(fb,sc,p,itk,nbstades,stades,stcum,stcumdrp)
        dureecycle= sc%n - p%nlev  ! jul - ilevs
    ! les coupes suivantes
      else
      ! redemarrage
        stcum = p%udevlaires(p%numcoupe)
        stcumdrp = p%udevlaires(p%numcoupe)
        nbstades = 9
!        allocate(stades(nbstades))
        stades(1:nbstades) = (/lcu,amf,lax,sen,lan,drp,mat,rec,cut/)
        call bilanStadesDeveloppement(fb,sc,p,itk,nbstades,stades,stcum,stcumdrp)
        dureecycle = sc%n - p%nfauche(p%numcoupe-1)
      endif

      write(fb,mes1055) dureecycle
      if (p%group == -1) write(fb,mes1056)

    ! ** CROISSANCE & ABSORPTION d'AZOTE
      write(fb,mes1057)
!     Aerial biomass (0%), green biomass and senescent biomass
      write(fb,mes2083) p%masecavantfauche, p%masecavantfauche - p%msresjaune(AOAS) - p%msneojaune(AOAS),  &
                        p%msresjaune(AOAS)+p%msneojaune(AOAS)

! Ajout Loic Mars 2017: calcul de msres pour hautfauche
      if (itk%P_code_hautfauche_dyn == 1) then
! Modif Loic Novembre 2020
!         p%msres(AOAS) = t%P_Hautfauche * p%P_coefmshaut
!         itk%P_msresiduel(p%numcoupe) = p%msres(AOAS)
         p%msres(AOAS) = p%P_coefmshaut * (itk%P_hautcoupe(p%numcoupe) - p%P_hautbase(itk%P_variete))
         itk%P_msresiduel(p%numcoupe) = p%msres(AOAS)
         if (p%P_stade0 == snu.and.p%numcoupe == 1.) then
            itk%P_msresiduel(p%numcoupe-1) = 0.
         else
            itk%P_msresiduel(p%numcoupe-1) = itk%P_msresiduel(p%numcoupe)
         endif
      endif
! Residual biomass coming from previous growth & Newly-formed biomass
      write(fb,mes2084) itk%P_msresiduel(p%numcoupe-1), p%masecavantfauche - itk%P_msresiduel(p%numcoupe-1)
      ! Ajout Loic Mars 2017: calcul de msres pour hautfauche
      if (itk%P_code_hautfauche_dyn == 1) then
         itk%P_msresiduel(p%numcoupe) = p%msres(AOAS)
         itk%P_msresiduel(p%numcoupe-1) = itk%P_msresiduel(p%numcoupe)
      endif

! Residual green biomass & Residual biomass for the next cycle
      write(fb,mes2085) p%msres(AOAS), itk%P_msresiduel(p%numcoupe)

! Crop N and N concentration
!      write(fb,mes2087) p%densite
      write(fb,mes2039) p%QNplanteavantfauche, p%QNplanteavantfauche / p%masecavantfauche / 10.
! Taux d'exportation de la biomasse fauchee
      write(fb,mes2042) itk%P_tauxexportfauche(p%numcoupe)*100.

! Harvested_biomass and cut biomass returned to soil
      write(fb,mes2082) p%mafauche, p%qressuite

! N in harvested_biomass and cut biomass returned to soil
      write(fb,mes2098) p%QNfauche, p%QNressuite

! Bilan eau, stress hydrique et azote
      write(fb,mes2088) p%numcoupe-1,p%numcoupe
      write(fb,mes1064) p%cetmcoupe,p%cetcoupe,p%cescoupe, p%cepcoupe,p%cprecipcoupe

      write(fb,mes1065) p%rmaxi,p%zracmax
      write(fb,mes2089) p%str1coupe/p%nst1coupe, p%stu1coupe/p%nst1coupe, p%inn1coupe/p%nst1coupe, p%diftemp1coupe/p%nst1coupe

      if (p%nst2coupe > 0) then
        write(fb,mes2090) p%str2coupe/p%nst2coupe, p%stu2coupe/p%nst2coupe, p%inn2coupe/p%nst2coupe, p%diftemp2coupe/p%nst2coupe
      endif

      write(fb,mes1166) (1.-p%gel1)*100., (1.-p%gel2)*100.
      if (p%mortplante == 1)    write(fb,mes1167)
      if (p%gel3 < 1.)          write(fb,mes1168) (1.-p%fgelflo)*100.
      if (pg%P_codeinnact /= 1) write(fb,mes1067)
      if (pg%P_codeh2oact /= 1) write(fb,mes1068)


      p%QNplanteCtot = p%QNplanteCtot + p%QNplantenp(AOAS,sc%n)

    endif ! fin des resultats culture

return
end subroutine ecrireBilanIntercoupe

subroutine ecrireBilanFinCultureFauchee(sc,p,itk)
    type(Stics_Communs_),       intent(IN)    :: sc
    type(Plante_),              intent(INOUT) :: p  
    type(ITK_),                 intent(IN)    :: itk  

    integer           :: jul, k
    integer           :: ancours, jour, nummois
    character(len=3)  :: mois
    real :: CsurNtot
    integer :: fb
    fb = p%bilan_unit

    write(fb,mes2025)

! IRRIGATIONS
   call bilanIrrigation(sc,1,sc%P_ifwater,fb,'coupe',itk%P_codedateappH2O)

! FERTILISATIONS
   call bilanFertilisations(sc,itk,1,sc%P_ifwater,fb,'coupe')

! Apports des residus
    ! if (itk%P_qres(1) == 0.) then
    if (abs(itk%P_qres(1)).lt.1.0E-8) then
      write(fb,mes2034) ! pas d'apports
    else
      write(fb,mes2035) itk%P_qres(1), itk%P_CsurNres(1), itk%P_jultrav(1), itk%P_proftrav(1)
    endif

    if (p%P_codeplante /= CODE_BARESOIL) then
       write(fb,mes2036)
       do k = 1, p%numcoupe - 1
          jul = p%nfauche(k) + sc%P_iwater - 1
          ancours = sc%annee(jul)
          if (jul  >  sc%nbjsemis) jul = jul - sc%nbjsemis
          call get_day_month(jul,ancours,mois,jour,nummois)
          write(fb,1300) k,jour,mois,ancours
1300      format(12x,i2,10x,I2,'-',A3,'-',I4,2F14.0)
       end do
       p%CNplante(AOAS) = p%QNfauchetot / (p%mafauchetot * 10.)
    ! Loic Juin 2016: il ne faut pas utiliser magrain qui correspond a la biomasse des fruits.
!      p%magrain(AOAS,p%nrec) = p%mafauchetot
       write(fb,mes1057)

! N in crop at harvest and N concentration
!       write(fb,mes2039) p%QNfauchetot, p%CNplante(AOAS)
! Harvested biomass
       write(fb,mes2038) p%mafauchetot
! Taux d'exportation de la biomasse fauchee (coupe 1)
      write(fb,mes2042) itk%P_tauxexportfauche(1)*100.

       write(fb,mes2037)  p%QNfauchetot
       write(fb,mes2040b) p%qressuite, p%QNressuite
       CsurNtot = 999.
       if(p%QNressuite_tot > 1.e-8) CsurNtot = p%QCressuite_tot / p%QNressuite_tot
       write(fb,mes2040) itk%P_ressuite, p%qressuite_tot, p%QNressuite_tot, CsurNtot

! Bilan eau, stress hydrique et azote
       write(fb,mes2041)
       write(fb,mes1064) p%cetm, p%cet, p%ces, p%cep, p%cprecip
       write(fb,mes1065) p%rmaxi,p%zracmax
    endif
return
end subroutine ecrireBilanFinCultureFauchee


!  WATER, MINERAL NITROGEN AND ORGANIC C AND N BALANCE
   subroutine ecrireBilanEauCN(sc,pg,soil,p,itk)
    type(Stics_Communs_),       intent(INOUT) :: sc
    type(Parametres_Generaux_), intent(IN)    :: pg
    type(Sol_),                 intent(IN)    :: soil
    type(Plante_),              intent(INOUT) :: p
    type(ITK_),                 intent(IN)    :: itk

    real    :: QH2Oi  !
    real    :: QH2Of  !
    real    :: QNO3i  !
    real    :: QNH4i  !
    real    :: QNO3f  !
    real    :: QNH4f
    real    :: entree  !
    real    :: sortie
    real    :: Vp
    real    :: Vpannuel
    character(len=45) :: typres

    real    :: QNrest   ! N restitue
    real    :: QCrest   ! C restitue
    real    :: Qmin_end     ! N mineral
    real    :: QCorgeng ! C associe au N organise par l'engrais
    real    :: QNap     ! somme des apports N
    real    :: QCap     ! somme des apports C
    real    :: QNaerienfinal  ! quantite de N dans la plante apres recolte
    real    :: QNplanteinit   ! quantite de N dans la plante entiere au depart
    real    :: QNplantefinal  ! quantite de N dans la plante entiere apres recolte
    integer :: i
    real    :: CsurNtot
    real    :: MSrest
    integer :: fb

    fb = p%bilan_unit
! **************************************************************
!       Temps normalise et Vitesse potentielle de mineralisation
! **************************************************************
        write(fb,mes1069) sc%maxwth
        write(fb,mes1070) sc%var_TREFh(sc%numcult),sc%tnhc,sc%tnrc
      ! vitesse de mineralisation potentielle
        Vp = 0.
        if (sc%tnhc > 0.) Vp = sc%Qminh / sc%tnhc
        Vpannuel = Vp *36525. / sc%Nhumt
        write(fb,mes4070) Vp,Vpannuel
! **********************************************
!               Bilan EAU SOL
! **********************************************
! Bruno avril 2017 : pour avoir un bon bilan il faut considerer les 2 plantes
!                    remplace p%cep par sc%cep2
! Bruno aout 2017 deja calcule dans Stics_jour. Partie a supprimer ?
        QH2Oi = 0.
        QH2Of = 0.
        QNO3i = 0.
        QNH4i = 0.
        QNO3f = 0.
        QNH4f = 0.
        do i = 1,sc%NH    !5
          QH2Oi = QH2Oi + sc%Hinit(i) * soil%da(i) / 10. * soil%P_epc(i)
          QNO3i = QNO3i + soil%NO3init(i)
          QNO3f = QNO3f + soil%AZnit(i)
          QNH4i = QNH4i + sc%NH4init(i)
          QNH4f = QNH4f + sc%AZamm(i)
        end do
! fin
        QH2Of = SUM(sc%Hur(1:soil%profsol)) + SUM(sc%sat(1:soil%profsol))

! Pluies + Irrigations + Initialisations eaux du sol - Remontee
        entree = QH2Oi + sc%cpluie + sc%totir - soil%remontee
        sortie = QH2Of + sc%cestout + sc%cep2 + sc%drat + sc%ruisselt   &
                       + p%cinterpluie + sc%cintermulch + sc%toteaunoneffic + soil%Qdraincum + soil%remonteemax
        write(fb,mes1072)
        write(fb,mes1073) QH2Oi,QH2Of,sc%cpluie, sc%cestout, sc%totir, sc%cep2, -soil%remontee, sc%ruisselt, sc%drat, &
                          soil%Qdraincum, p%cinterpluie, sc%cintermulch, sc%toteaunoneffic, entree, sortie

! **********************************************
!               Bilan AZOTE PLANTE
! **********************************************
!      QNrest = cumul du N restitue au sol pendant la duree de la simulation
!      QNressuite = quantite de N restitue au sol sous forme de residus de culture au dernier jour de simulation

! Quantite finale d'azote dans la plante en fin de simulation
! si (p%codemsfinal == 1)  QNplantenp(AOAS,) n'a plus la valeur reelle en fin de simulation
! on contourne en recalculant avec les 2 plantes
!     QNaerienfinal = p%QNplantenp(AOAS,sc%maxwth)
!     if(pg%P_codemsfinal == 1) QNaerienfinal = p%QNplantenp(1,sc%maxwth)*p%surf(1) + p%QNplantenp(2,sc%maxwth)*p%surf(2)
! 31/08/2017 TODO stocker la variable QNaerienfinal independamment car si on remet les AS AO a zero on la perd
       ! Modif Florent C et Fabien Octobre 2019
       ! lorsque la fauche prevue ne se fait pas, pb avec QNaerienfinal qui vaut 0
       ! on remplace maxwth par n
       !QNaerienfinal = p%QNplantenp(1,sc%maxwth)*p%surf(1) + p%QNplantenp(2,sc%maxwth)*p%surf(2)
       QNaerienfinal = p%QNplantenp(1,sc%n)*p%surf(1) + p%QNplantenp(2,sc%n)*p%surf(2)

       ! Loic Oct 2020 : pb bilan quand code_msfinal = 1
       if (pg%P_codemsfinal == 1 .and. p%nrec > 0) QNaerienfinal = 0.

! Calcul des quantites exportees et residuelles en fonction du type de plante

! QNexport et QNressuite deja calcules pour une plante recoltee
       if(p%nrec == 0 .or. p%nrec > sc%maxwth) then
          p%QNexport = 0.
          if (itk%P_codefauche == 2 .or. p%P_codeplante == CODE_FODDER) p%QNexport = p%QNfauchetot
          if (itk%P_codcueille == 2 .and. p%P_codeperenne == 2 .and. p%P_codeplante /= CODE_FODDER) p%QNexport = p%QNgrain(AOAS)
          if (p%P_codeperenne == 1) p%QNexport = p%QNfauchetot
! ajout Bruno avril 2017
          if(p%P_codeplante.eq.CODE_FODDER) p%rendementsec = p%mafauchetot
       endif
       ! Loic Fevrier 2021 : pour la vigne QNexport n'est jamais calcule !
       if (itk%P_codcueille == 2 .and. p%P_codeperenne == 2 .and. p%P_codeplante == CODE_VINE) p%QNexport = p%QNgrain(AOAS)

! DR 19/02/2019 substitution de parazorac en tant que code par un vrai code P_code_rootdeposition
!       if (p%P_parazorac < 999.) then
       ! Modif Florent C. et Fabien Octobre 2019
       ! dans le cas ou p%P_code_rootdeposition ==1, on cree de l'azote pour le mettre dans les racines a la recolte
       ! c'est donc logique que le bilan ne boucle pas
       !if( p%P_code_rootdeposition ==1) then
       !   QNrest = p%QNplantetombe(0) + p%QNrogne + p%QNressuite_tot + p%QNracmort + p%QNperennemort(AOAS)
       !else
       !   QNrest = p%QNplantetombe(0) + p%QNrogne + p%QNressuite_tot + p%QNperennemort(AOAS)
       !endif
       QNrest = p%QNplantetombe(0) + p%QNrogne + p%QNressuite_tot + p%QNracmort + p%QNperennemort(AOAS)
    ! ajout Bruno avril 2018
    ! Modif Loic sept 2020 : plus besoin de code specifique pour la betterave voir routine ApportResidusCulture
!       if (p%P_codeplante == CODE_BEET) then
!          p%QNexport = p%QNabso(AOAS) - QNrest
!          p%QNgrain(AOAS) = p%QNexport
!     endif

       QNplanteinit = p%P_QNplantenp0 + p%P_QNperenne0 + p%QNrac0
       QNplantefinal = QNaerienfinal  + p%QNperenne(AOAS) + p%QNrac
       entree = QNplanteinit  + p%QNabso(AOAS) + p%Qfixtot(AOAS)
       sortie = QNplantefinal + p%QNexport + QNrest

       write(fb,mes1074) p%P_QNplantenp0, QNaerienfinal ,p%QNrac0, p%QNrac, p%P_QNperenne0, p%QNperenne(AOAS),  &
                         QNplanteinit, QNplantefinal, p%QNabso(AOAS), p%QNexport, p%Qfixtot(AOAS), QNrest,  &
                         entree, sortie

! **************************************************
!               Bilan AZOTE MINERAL SOL
! **************************************************
! Bruno avril 2017 : pour avoir un bon bilan il faut considerer les 2 plantes
!                    remplace p%QNabso(AOAS) par sc%QNabso2

        entree =  QNH4i + QNO3i + sc%precipN + sc%irrigN + sc%totapN + sc%Qminh + sc%Qminr
        sortie =  QNH4f + QNO3f + sc%QNabso2 + sc%Qles + soil%qlesd + soil%QNorgeng   &
                 + soil%QNvoleng + soil%QNvolorg + soil%QNdenit + sc%Qem_N2Onit

         write(fb,mes1076) QNH4i, QNH4f, QNO3i, QNO3f, QNH4i+QNO3i, QNH4f+QNO3f,     &
                           sc%QNabso2, sc%precipN, sc%Qles, sc%irrigN, soil%qlesd,    &
                           sc%totapN, soil%QNorgeng, soil%QNvoleng, sc%Qminh, soil%QNvolorg, &
                           sc%Qminr, soil%QNdenit + sc%Qem_N2Onit, &
                           entree, sortie
! ***********************************************
!             BILANS N et C ORGANIQUES
! ***********************************************
! Bruno avril 2017 : pour avoir un bon bilan avec 2 plantes
!                    remplace p%QNressuite_tot par sc%QNressuite2
!                    remplace QNtombe par sc%QNplantetombe2
!                    remplace p%QNressuite_tot par sc%QNressuite2
!                    remplace p%QNressuite_tot par sc%QNressuite2
!                    remplace p%QNressuite_tot par sc%QNressuite2

        QNrest = sc%QNplantetombe2 + sc%QNrogne2 + sc%QNressuite_tot2 + sc%QNracmort2 + sc%QNperennemort2

! Bruno aout 2017 deja calcule dans Stics_jour. Partie a supprimer ?
        sc%SONtot0 = sc%Nhuma0+ sc%Nhumi0 + sc%Nb0 + sc%Nbmulch0 + sc%Nr0 + sc%Nrprof0 + sc%Nmulch0
        sc%SONtot =  sc%Nhuma + sc%Nhumi + sc%Nb  + sc%Nbmulch  + sc%Nr  + sc%Nrprof  + sc%Nmulch
        sc%STN0 = sc%SONtot0 + QNH4i + QNO3i
        sc%STN = sc%SONtot + QNH4f + QNO3f
! fin
        Qmin_end = sc%Qminh + sc%Qminr - sc%QNprimed
        QNap = sc%QNresorg + QNrest

        entree = sc%Nhuma0 + sc%Nhumi0 + sc%Nb0 + sc%Nbmulch0 + sc%Nr0 + sc%Nrprof0 + sc%Nmulch0 + soil%QNorgeng + QNap
        sortie = sc%Nhuma  + sc%Nhumi  + sc%Nb  + sc%Nbmulch  + sc%Nr  + sc%Nrprof  + sc%Nmulch  + Qmin_end + sc%QNprimed

        write(fb,mes1080)
        write(fb,mes1082) sc%Nhuma0, sc%Nhuma, sc%Nhumi0, sc%Nhumi, sc%Nb0+sc%Nbmulch0, sc%Nb+sc%Nbmulch, sc%Nr0, sc%Nr,   &
                          sc%Nrprof0, sc%Nrprof, sc%Nmulch0, sc%Nmulch, sc%SONtot0, sc%SONtot, soil%QNorgeng, sc%QNresorg, &
                          sc%QNressuite_tot2,&
                          sc%QNplantetombe2, sc%QNprimed, sc%QNrogne2, Qmin_end, sc%QNracmort2, sc%QNperennemort2, entree, sortie

        QCrest = sc%QCplantetombe2 + sc%QCrogne2 + sc%QCressuite_tot2 + sc%QCracmort2 + sc%QCperennemort2
        QCorgeng = 0.
        Qmin_end = sc%QCO2sol - sc%QCprimed
        QCap = sc%QCresorg + QCrest

! Bruno aout 2017 deja calcule dans Stics_jour. Partie a supprimer ?
        sc%SOCtot0 = sc%Chuma0 + sc%Chumi0 + sc%Cb0 + sc%Cbmulch0 + sc%Cr0 + sc%Crprof0 + sc%Cmulch0
        sc%SOCtot =  sc%Chuma  + sc%Chumi  + sc%Cb  + sc%Cbmulch  + sc%Cr  + sc%Crprof  + sc%Cmulch
! fin
        entree = sc%Chuma0 + sc%Chumi0 + sc%Cb0 + sc%Cbmulch0 + sc%Cr0 + sc%Crprof0 + sc%Cmulch0 + QCap
        sortie = sc%Chuma  + sc%Chumi  + sc%Cb  + sc%Cbmulch  + sc%Cr  + sc%Crprof  + sc%Cmulch  + sc%QCO2sol

        write(fb,mes1081)
        write(fb,mes1082) sc%Chuma0, sc%Chuma, sc%Chumi0, sc%Chumi, sc%Cb0+sc%Cbmulch0, sc%Cb+sc%Cbmulch, sc%Cr0, sc%Cr,    &
                          sc%Crprof0, sc%Crprof, sc%Cmulch0, sc%Cmulch, sc%SOCtot0, sc%SOCtot, QCorgeng, sc%QCresorg, &
                          sc%QCressuite_tot2,   &
                          sc%QCplantetombe2, sc%QCprimed, sc%QCrogne2, Qmin_end, sc%QCracmort2, sc%QCperennemort2, entree, sortie

! FLUX CO2 et N2O
        write(fb,mes1083) sc%QCO2sol-sc%QCO2hum, sc%QCO2hum, sc%QCO2sol
        write(fb,mes1084) sc%Qem_N2Onit, sc%Qem_N2Oden, sc%Qem_N2O

! Residus de culture
      if (itk%P_codefauche == 1) then
           typres = "dead roots     "
      else
          if (p%P_codeplante == CODE_BARESOIL) then
              typres = "none           "
          else
              typres = itk%P_ressuite
          endif
      endif
      CsurNtot = 9999.
      if(p%QNressuite > 1.e-8) CsurNtot = sc%QCressuite2 / sc%QNressuite2
      write(fb,mes1062b) p%qressuite, sc%QCressuite2, sc%QNressuite2, CsurNtot
      CsurNtot = 9999.
      ! A voir avec Bruno
!      if(p%QNressuite_tot > 1.e-8) CsurNtot = p%QCressuite_tot / p%QNressuite_tot
!      write(fb,mes1062) typres, p%qressuite_tot, p%QCressuite_tot, p%QNressuite_tot, CsurNtot
      if(QNrest > 1.e-8) CsurNtot = QCrest / QNrest
! Modif Bruno mars 2018 MS total restitue recalcule
      MSrest = QCrest/0.42/1000.
!     write(fb,mes1062) typres, p%qressuite_tot, QCrest, QNrest, CsurNtot
      write(fb,mes1062) typres, MSrest, QCrest, QNrest, CsurNtot
return
end subroutine ecrireBilanEauCN



! *****************************************************
 subroutine bilanSimulationRecolteButoir(fb,sc,pg,soil,p)
    integer, intent(IN) :: fb
    type(Stics_Communs_),       intent(IN)    :: sc  
    type(Parametres_Generaux_), intent(IN)    :: pg  
    type(Sol_),                 intent(IN)    :: soil  
    type(Plante_),              intent(INOUT) :: p  

    real    :: QH2Oi  !  
    real    ::  QH2Of  !  
    real    ::  QNO3i  !  
    real    ::  QNH4i  !  
    real    ::  QNO3f  !  
    real    ::  QNH4f  
    real    :: entree  !  
    real    ::  sortie  
    integer :: i  

    ! quantites d'eau et d'azote sur l'ensemble du profil
        QH2Oi = 0.
        QH2Of = 0.
        QNO3i = 0.
        QNH4i = 0.
        QNO3f = 0.
        QNH4f = 0.
        do i = 1,sc%NH    !5
          QH2Oi = QH2Oi + sc%Hinit(i) * soil%da(i) / 10. * soil%P_epc(i)
          QNO3i = QNO3i + soil%NO3init(i)
          QNO3f = QNO3f + soil%AZnit(i)
          QNH4i = QNH4i + sc%NH4init(i)
          QNH4f = QNH4f + sc%AZamm(i)
        end do
        QH2Of = SUM(sc%Hur(1:soil%profsol))

        write(fb,mes1069) sc%maxwth
        write(fb,mes1070) pg%P_TREFh, sc%tnhc, sc%tnrc

! bilan eau
       entree = sc%cpluie + sc%totir + QH2Oi - soil%remontee

       sortie = sc%cestout + p%cep + sc%drat + sc%ruisselt + QH2Of - sc%et + p%cinterpluie + sc%cintermulch &
              + sc%toteaunoneffic + soil%Qdraincum

        write(fb,mes1072)
        write(fb,mes1073) sc%cpluie, sc%cestout, sc%totir, p%cep, -soil%remontee, sc%ruisselt, sc%drat, soil%Qdraincum,  &
                          p%cinterpluie, sc%cintermulch, sc%toteaunoneffic, QH2Oi, QH2Of, entree, sortie
! bilan azote
        entree = sc%pluieN + sc%irrigN + sc%totapN + p%Qfixtot(AOAS) + sc%Qminh + sc%Qminr &
                           + p%P_QNplantenp0 + QNO3i + QNH4i

        p%QNplanteCtot = p%QNplanteCtot + p%QNplantenp(AOAS,p%nrec)
        sortie = sc%Qles + soil%qlesd + soil%QNorgeng + soil%QNvolorg + soil%QNvoleng + soil%QNdenit &
                         + p%QNplanteCtot + QNO3f + QNH4f

        write(fb,mes1074) sc%pluieN, p%QNplanteCtot - p%QNressuite, sc%irrigN, p%QNressuite + soil%Qminrcult, &
                          sc%totapN, sc%Qles, p%Qfixtot(AOAS), soil%QNorgeng, sc%Qminh, soil%QNvoleng,     &
                          sc%Qminr, soil%QNvolorg, soil%QNdenit, p%P_QNplantenp0, soil%qlesd, QNO3i, QNO3f,   &
                          QNH4i, QNH4f, entree, sortie
return
end subroutine bilanSimulationRecolteButoir

! IRRIGATION
subroutine bilanIrrigation(sc,debut,fin,fb,type,codedateappH2O)
    type(Stics_Communs_), intent(IN)    :: sc  
    integer,              intent(IN)    :: debut    ! debut de periode de calcul des apports irrigation  
    integer,              intent(IN)    :: fin      !   fin de periode de calcul des apports irrigation  
    integer,              intent(IN)    :: fb       ! descripteur du fichier dans lequel ecrire
    character(len=5),     intent(IN)    :: type     ! type de bilan => coupe ou final  

!: Variables locales
    integer :: nApports  
    real    :: qApports  
    integer :: jul  !  
    integer ::  ancours  !  
    integer ::  jour  !  
    integer ::  nummois  
    character(len=3) :: mois  
    integer :: k  
    integer :: codedateappH2O

      write (fb,mes2026)
      qApports = 0. ! quantite des apports  ! =   SUM(airg(debut:fin),msc%ASk=airg/=0.)
      nApports = 0  ! nombre d'apports      ! = COUNT(airg(debut:fin),msc%ASk=airg/=0.)
      if (codedateappH2O.eq.1) write (fb,mes10024)
      if (sc%naptot  >  0) then
!        write(fb,mes1024)
        if (codedateappH2O.eq.1) write (fb,mes10024)
        do k = debut, fin ! debut et fin de periode
          if (sc%airg(k) > 0.) then
            call modulo_years(k + sc%P_iwater - 1, sc%annee(sc%P_iwater), jul, ancours)
            call get_day_month(jul,ancours,mois,jour,nummois)

            write (fb,180) jour,mois,ancours,sc%airg(k)
180         format(7x,I2,'-',A3,'-',I4,15x,F7.0)
            nApports = nApports + 1
            qApports =  qApports + sc%airg(k)
          endif
        end do
      endif
      if (nApports > 0) then
         write (fb,mes1023) nApports
         select case(trim(type))
            case('coupe')
                write (fb,mes2067) qApports        !p%totircoupe(p%numcoupe)
            case('final')
                write (fb,mes1125) qApports
         end select
      else
         write (fb,*) '  No irrigation during the period'
      endif
return
end subroutine bilanIrrigation


! Fertilisations
subroutine bilanFertilisations(sc,itk,debut,fin,fb,type)
    type(Stics_Communs_), intent(IN)    :: sc  
    type(ITK_),           intent(IN)    :: itk  
    integer,              intent(IN)    :: debut    ! debut de periode de calcul des apports irrigation  
    integer,              intent(IN)    :: fin      ! fin de periode de calcul des apports irrigation  
    integer,              intent(IN)    :: fb       ! descripteur du fichier dans lequel ecrire  
    character(len=5),     intent(IN)    :: type     ! type de bilan => coupe ou final

    integer :: nApports  
    real    :: qApports  
    integer :: jul  !  
    integer ::  ancours  !  
    integer ::  jour  !  
    integer ::  nummois  
    character(len=3) :: mois  
    integer :: k  

    qApports = 0. ! quantite des apports N
    nApports = 0  ! nombre d'apports N

    if (sc%napNtot == 0) then
        write (fb,*) '  No mineral fertilisation'
    else
        nApports = COUNT(sc%anit(debut:fin)> 0.) + COUNT(sc%anit_uree(debut:fin)> 0.)
        if (itk%P_codedateappN == 1) write(fb,mes4001)
        if(nApports == 0) then
           write (fb,*) '  No fertilisation during the period'
        else
           write (fb,mes1026) nApports
           write(fb,mes1027)
           do k = debut,fin
              if (sc%anit(k) > 0.) then
                 call modulo_years(k + sc%P_iwater - 1, sc%annee(sc%P_iwater), jul, ancours)
                 call get_day_month(jul,ancours,mois,jour,nummois)

                 write (fb,180) jour, mois, ancours, sc%anit(k), sc%type_ferti(k)
180              format(7x,I2,'-',A3,'-',I4,F12.0,i16)
                 qApports = qApports + sc%anit(k)
              endif
              if(sc%anit_uree(k) > 0.) then
                 call modulo_years(k + sc%P_iwater - 1, sc%annee(sc%P_iwater), jul, ancours)
                 call get_day_month(jul,ancours,mois,jour,nummois)

                 write (fb,180) jour, mois, ancours, sc%anit_uree(k), sc%type_ferti_pissats(k)
                 qApports = qApports + sc%anit_uree(k)
              endif
           end do
        endif
    endif
    if (nApports > 1) then
      select case(trim(type))
        case('coupe')
          write(fb,mes2033) qApports
        case('final')
          write(fb,mes1028) qApports
      end select
    endif

return
end subroutine bilanFertilisations

subroutine bilanStadesDeveloppement(fb,sc,p,itk,nbstades,stades,stcum,stcumdrp)
    integer, intent(IN) :: fb
    type(Stics_Communs_),       intent(IN)    :: sc  
    type(Plante_),              intent(INOUT) :: p  
    type(ITK_),                 intent(IN)    :: itk  
    integer,                    intent(IN)    :: nbstades  
    character(len=3),           intent(IN)    :: stades(nbstades)  
    real,                       intent(INOUT) :: stcum
    real,                       intent(INOUT) :: stcumdrp  

    integer           :: jul, ancours, jour, nummois
    integer           :: i, k
    integer           :: dureecycle
    integer           :: idess
    character(len=3)  :: mois  
    character(len=50) :: car
    real              :: stdrp  
    real              :: stcumger
    real              :: stdrpdes
    logical :: flagdes

    real              :: units_by_stages

! format avec stade bbch

 300 format(5x,A20,a3,3x,I2,'-',A3,'-',I4,2F10.0,2F10.1)
! format sans stade bbch

 301 format(5x,A26,I2,'-',A3,'-',I4,2F10.0,2F10.1)    
        
    stcum = 0.

    do k = 1,nbstades
      SELECT CASE(stades(k))

! semis
        case(plt)
          if(p%codeinstal == 0) then
            i = p%iplt
            if (p%iplt > sc%nbjsemis) i = p%iplt - sc%nbjsemis
            call get_day_month(i,sc%annee(p%iplt),mois,jour,nummois)
            car = 'sowing              '
            write(fb,300) car, p%P_stadebbchplt, jour, mois, sc%annee(p%iplt), stcum, stcum
            if (itk%P_codedecisemis == 1) &
               write(fb,*) mes445,p%nbjpourdecisemis
            else
               write(fb,mes1052) p%P_stade0
            endif

! DR 18/07/2012 je rajoute la date de germination
! germination
        case(ger)
          i = p%igers
          if (p%igers > sc%nbjsemis) i = p%igers - sc%nbjsemis
          call get_day_month(i,sc%annee(p%igers),mois,jour,nummois)
          ! DR 19/07/2012 on cumule depuis le semis donc on cree une nouvelle variable
          stcumger = p%stpltger
          units_by_stages = p%stpltger
          car = 'ger calculated      '
          if (p%iplt /= 0) then
            write(fb,300) car, p%P_stadebbchger, jour, mois, sc%annee(p%igers), units_by_stages, stcumger,  &
                          p%lai(AO,p%nger)*p%surf(AO) + p%lai(AS,p%nger)*p%surf(AS)
          endif

      ! levee = demarrage
        case(lev)
          i = p%ilevs
          if (p%ilevs > sc%nbjsemis) i = p%ilevs - sc%nbjsemis
          call get_day_month(i,sc%annee(p%ilevs),mois,jour,nummois)
          ! DR 10/08/2022 si on est a levee observee et qu'on est codegermin = 2 on a pas de stpltger et pas de stpltlev
          ! donc je mets tout a -0
          ! DR 04/10/2022 j'ajoute le codegermin dans le test
          !DR 05/10/2022
         ! if ( p%P_codehypo == 2 ) then
         !     stcum = 0.0
         !     units_by_stages = 0.0
         ! else
          !01/12/2014 la somme intermediaire entre stade doit etre stgerlev
              stcum = stcum + p%stpltlev 
              units_by_stages = p%stpltlev-p%stpltger
          !endif
          car = 'lev observed        '
          if (p%nlevobs == 999) car = 'lev calculated      '
          if (p%iplt /= 0) then
            write(fb,300) car, p%P_stadebbchlev, jour, mois, sc%annee(p%ilevs), units_by_stages, stcum,    &
                          p%lai(AO,p%nlev)*p%surf(AO) + p%lai(AS,p%nlev)*p%surf(AS)
          endif


! amf
        case(amf)
          i = p%iamfs
          if (p%iamfs > sc%nbjsemis) i = p%iamfs - sc%nbjsemis
          call get_day_month(i,sc%annee(p%iamfs),mois,jour,nummois)
          car = 'amf observed        '
          if (p%namfobs == 999) car = 'amf calculated      '
          units_by_stages = p%P_stlevamf(itk%P_variete)
          if (p%namf /= 0) then
            stcum = stcum + p%P_stlevamf(itk%P_variete)
            write(fb,300) car, p%P_stadebbchamf, jour, mois, sc%annee(p%iamfs), units_by_stages, stcum,    &
                          p%lai(AO,p%namf) * p%surf(AO) + p%lai(AS,p%namf) * p%surf(AS)
          endif

! lax
        case(lax)
          i = p%ilaxs
          if (p%ilaxs > sc%nbjsemis) i = p%ilaxs - sc%nbjsemis
          call get_day_month(i,sc%annee(p%ilaxs),mois,jour,nummois)
          car = 'lax observed        '
          units_by_stages = p%P_stamflax(itk%P_variete)
          if (p%nlaxobs == 999) car = 'lax calculated      '
          if (p%nlax /= 0) then
            stcum = stcum + p%P_stamflax(itk%P_variete)
            write(fb,300) car, p%P_stadebbchlax, jour, mois, sc%annee(p%ilaxs), units_by_stages, stcum,    &
                            p%lai(AO,p%nlax)*p%surf(AO) + p%lai(AS,p%nlax)*p%surf(AS)
          endif

! sen
        case(sen)
          p%isens = max(1,p%isens)
          if (p%P_codlainet == 1) then
            i = p%isens
            if (p%isens > sc%nbjsemis) i = p%isens - sc%nbjsemis
! DR 01/12/2014 je corrige les stades y'a des pbs de recalcule de dates , fallait prendre l'annee du stade
            call get_day_month(i,sc%annee(p%isens),mois,jour,nummois)
            units_by_stages = p%P_stlaxsen(itk%P_variete)
            car = 'sen observed      '
            if (p%nsenobs == 999) car = 'sen calculated      '
            if (p%nsen /= 0) then
              stcum = stcum + p%P_stlaxsen(itk%P_variete)
              write(fb,300) car, p%P_stadebbchsen, jour, mois, sc%annee(p%isens), units_by_stages, stcum,    &
                            p%lai(AO,p%nsen)*p%surf(AO) + p%lai(AS,p%nsen)*p%surf(AS)
            endif
          endif

! lan
        case(lan)
          i = p%ilans
          if (p%ilans > sc%nbjsemis) i = p%ilans - sc%nbjsemis
! DR 01/12/2014 je corrige les stades y'a des pbs de recalcule de dates , fallait prendre l'annee du stade
          call get_day_month(i,sc%annee(p%ilans),mois,jour,nummois)
          units_by_stages = p%P_stsenlan(itk%P_variete)
          car = 'lan observed        '
          if (p%nlanobs == 999) car = 'lan calculated      '
          if (p%nlan /= 0) then
            stcum = stcum + p%P_stsenlan(itk%P_variete)
            write(fb,301) car, jour, mois, sc%annee(p%ilans), units_by_stages, stcum,  &
                          p%lai(AO,p%nlan)*p%surf(AO) + p%lai(AS,p%nlan)*p%surf(AS)
          endif

! flo
        case(flo)
          p%iflos = max(1,p%iflos)
          i = p%iflos
          if (p%iflos > sc%nbjsemis) i = p%iflos - sc%nbjsemis
          call get_day_month(i,sc%annee(p%iflos),mois,jour,nummois)
          if (p%P_codeperenne == 1) then
             if(p%P_codegermin == 1) then
                units_by_stages = p%stlevflo + p%stpltlev
                stcumdrp = stcumdrp + p%stlevflo + p%stpltlev
             else
                units_by_stages = p%stlevflo
                stcumdrp = stcumdrp + p%stlevflo
             endif
          else
             units_by_stages = p%stlevflo
          endif

          car = 'flo observed        '
          if (p%nfloobs == 999) car = 'flo calculated           '
          if (p%P_codeperenne == 1) then
            write(fb,300) car, p%P_stadebbchflo, jour, mois, sc%annee(p%iflos), units_by_stages, stcumdrp
          else
            write(fb,300) car, p%P_stadebbchflo, jour, mois, sc%annee(p%iflos), units_by_stages, stcumdrp,    &
                          p%lai(AO,p%nflo) * p%surf(AO) + p%lai(AS,p%nflo) * p%surf(AS)
          endif

! drp
        case(drp)
          p%idrps = max(1,p%idrps)
          i = p%idrps
          if (p%idrps > sc%nbjsemis) i = p%idrps - sc%nbjsemis
! DR 01/12/2014 je corrige les stades y'a des pbs de recalcule de dates , fallait prendre l'annee du stade
          call get_day_month(i,sc%annee(p%idrps),mois,jour,nummois)
          car = 'drp observed        '
          if (p%ndrpobs == 999) car = 'drp calculated      '
          if (p%ndrp /= 0) then
            if (itk%P_codefauche == 2) then
              stcumdrp = stcumdrp + p%P_stflodrp(itk%P_variete)
              units_by_stages = p%P_stflodrp(itk%P_variete)
            else
              stcumdrp = stcumdrp + p%P_stlevdrp(itk%P_variete)
              units_by_stages = p%P_stlevdrp(itk%P_variete)
            endif
            write(fb,300) car, p%P_stadebbchdrp, jour, mois, sc%annee(p%idrps), units_by_stages, stcumdrp,   &
                          p%lai(AO,p%ndrp)*p%surf(AO) + p%lai(AS,p%ndrp)*p%surf(AS)
          endif
          flagdes=.false.

! des avant mat
        case(des)
          if (p%ndebdes /= 0 .and. p%ndebdes < p%nmat) then
            idess = max(1,p%ndebdes + sc%P_iwater -1)
            i = idess
            if (idess > 0) then
              if (idess > sc%nbjsemis) i = idess - sc%nbjsemis
              call get_day_month(i,sc%annee(idess),mois,jour,nummois)
              stcumdrp = stcumdrp + p%P_stdrpdes(itk%P_variete)
              units_by_stages = p%P_stdrpdes(itk%P_variete)
              car = 'debdes observed              '
              if (p%ndebdesobs == 999) car = 'debdes calculated           '
              write(fb,300) car, p%P_stadebbchdebdes, jour, mois, sc%annee(idess), units_by_stages, stcumdrp,       &
                            p%lai(AO,p%ndebdes) * p%surf(AO) + p%lai(AS,p%ndebdes) * p%surf(AS)
            endif
            flagdes=.true.
          endif

! mat
        case(mat)
          if (p%nmat > 0) then
            p%imats = max(1,p%imats)
            i = p%imats
            if (p%imats > sc%nbjsemis) i = p%imats - sc%nbjsemis
            call get_day_month(i,sc%annee(p%imats),mois,jour,nummois)
            if (flagdes) then
              units_by_stages = p%P_stdrpmat(itk%P_variete)- p%P_stdrpdes(itk%P_variete)
              if(units_by_stages.lt.0) units_by_stages=0.
            else
              units_by_stages=p%P_stdrpmat(itk%P_variete)
            endif
            stcumdrp = stcumdrp + units_by_stages
            car = 'mat observed      '
            if (p%nmatobs == 999) car = 'mat calculated      '
            write(fb,300) car, p%P_stadebbchmat, jour, mois, sc%annee(p%imats), units_by_stages, stcumdrp,    &
                            p%lai(AO,p%nmat) * p%surf(AO) + p%lai(AS,p%nmat) * p%surf(AS)
          endif
! des apres mat
        case(fde)
        if(.not.flagdes) then
          if (p%ndebdes  /= 0 .and. p%ndebdes >= p%nmat .and.p%ndebdes < p%nrec ) then   ! TODO: ndebdes(1) <-//- pkoi (1) ?
          idess = max(1,p%ndebdes + sc%P_iwater - 1)
          i = idess
            if (idess > 0) then
              if (idess > sc%nbjsemis) i = idess - sc%nbjsemis
              call get_day_month(i,sc%annee(idess),mois,jour,nummois)
              if(p%ndebdes .eq. p%nmat) then
                 units_by_stages =0.
              else
                 units_by_stages = p%P_stdrpdes(itk%P_variete)- p%P_stdrpmat(itk%P_variete)
              endif
              stcumdrp = stcumdrp + units_by_stages
              car = 'debdes observed   '
              if (p%ndebdesobs == 999) car = 'debdes calculated   '
              write(fb,300) car, p%P_stadebbchdebdes, jour, mois, sc%annee(idess), units_by_stages, stcumdrp,    &
                          p%lai(AO,p%ndebdes) * p%surf(AO) + p%lai(AS,p%ndebdes) * p%surf(AS)
            endif
          endif
        endif


! Recolte
        case(rec)
          p%irecs = max(1,p%irecs)  ! on veut eviter qu'il soit egal a zero
          i = p%irecs
          if (p%irecs > sc%nbjsemis) i = p%irecs - sc%nbjsemis
          call get_day_month(i,sc%annee(p%irecs),mois,jour,nummois)
          if(p%ndebdes <= p%nmat) then
             units_by_stages=p%stmatrec
          else
             units_by_stages=p%stmatrec-(p%P_stdrpdes(itk%P_variete)- p%P_stdrpmat(itk%P_variete))
          endif
          stcumdrp = stcumdrp + units_by_stages
                                      car = 'rec observed     '
          if (p%nrecobs == 999)       car = 'rec calculated   '
          if (p%nrec == p%nrecbutoir) car = 'rec butoir       '
          if (p%ndes > 0 .and. p%ndes < 732) car = 'crop destruction '
          ! DR et Lolo 19/06/2019 si on a pas de gel lethale et qu'on est apres recolte, le test etait mauvais par rapport � l'affectiation de ndes dans apport
          !if (p%nrec == p%ndes)       car = 'crop harvest '
          if (p%nrec +1  == p%ndes)       car = 'crop harvest '
          ! non !! 19/06/2019 ML et DR : Si on a a la fois recolte puis "destruction" (mortalit� naturelle des parties non exportees de la culture apres la recolte)
          ! on ecrit 2 lignes : une pour la r�colte et une pour la mortalit� (uniquement culture annuelle) !!non
          if (p%nrec /= 0 .or. (p%ndes > 0 .and. p%ndes < 732)) then
            write(fb,300) car, p%P_stadebbchrec, jour, mois, sc%annee(p%irecs), p%stmatrec, stcumdrp,  &
                          p%lai(AO,p%nrec) * p%surf(AO) + p%lai(AS,p%nrec) * p%surf(AS)
          endif

          if (itk%P_codedecirecolte == 1) write(fb,mes447) p%nbjpourdecirecolte
        ! on ajoute le cas ou ***des est apres rec***
          if(.not.flagdes) then
             if (p%ndebdes  /= 0 .and. p%ndebdes >= p%nrec) then
             idess = max(1,p%ndebdes + sc%P_iwater - 1)
             i = idess
             stdrpdes=0.
             if (idess > 0) then
               if (idess > sc%nbjsemis) i = idess - sc%nbjsemis
               call get_day_month(i,sc%annee(idess),mois,jour,nummois)
               units_by_stages = stdrpdes
               car = 'debdes observed '
               if (p%ndebdesobs == 999) car = 'debdes calculated  '
               write(fb,300) car, p%P_stadebbchdebdes, jour, mois, sc%annee(idess), units_by_stages, stcumdrp,    &
                          p%lai(AO,p%ndebdes) * p%surf(AO) + p%lai(AS,p%ndebdes) * p%surf(AS)
            endif
          endif
        endif

! DR 19/06/2019 destruction eventuelle apres recolte
!       case('dea')
!          p%idess = max(1,p%idess)  ! on veut eviter qu'il soit egal a zero
!          i = p%idess
!          if (p%idess > sc%nbjsemis) i = p%idess - sc%nbjsemis
!          call get_day_month(i,sc%annee(p%idess),mois,jour,nummois)
 !           if (p%nrec /= 0 .and. (p%ndes > 0 .and. p%ndes < 732).and. (p%ndes.gt. p%nrec)) then
!              car = 'crop destruction '
!              write(fb,300) car, p%P_stadebbchrec, jour, mois, sc%annee(p%irecs), 0.0, 0.0,  &
!                          p%lai(AO,p%ndes) * p%surf(AO) + p%lai(AS,p%ndes) * p%surf(AS)
!            endif
!          endif



! jour de la coupe
        case(cut)
          jul = sc%n + sc%P_iwater - 1
          ancours = sc%annee(jul)
          dureecycle = jul-p%ilevs
          if (jul  >  sc%nbjsemis) jul = jul - sc%nbjsemis
          call get_day_month(jul,ancours,mois,jour,nummois)
          car = 'cut  day'
          write (fb,301) car, jour, mois, ancours, p%somcour, stcum+p%somcour,  &
                         p%lai(AO,sc%n)*p%surf(AO) + p%lai(AS,sc%n)*p%surf(AS)

! derniere coupe
        case(lcu)
          jul = p%nfauche(p%numcoupe-1) + sc%P_iwater - 1
          ancours = sc%annee(jul)
          if (jul  >  sc%nbjsemis) jul = jul - sc%nbjsemis
          call get_day_month(jul,ancours,mois,jour,nummois)
          car = 'cut    '
          write(fb,301) car, jour, mois, ancours, p%udevlaires(p%numcoupe), stcum,    &
                        itk%P_lairesiduel(p%numcoupe-1), itk%P_msresiduel(p%numcoupe-1)

! entree en dormance
        case(ddo)
          if (p%P_codebfroid == 3) then
            i = p%idebdorms
            if (p%idebdorms > sc%nbjsemis) i = p%idebdorms - sc%nbjsemis +1
            if (sc%P_iwater > p%P_idebdorm) then
              if(p%idebdorms > 0) then
                call get_day_month(i,sc%annee(p%idebdorms),mois,jour,nummois)
                write(fb,mes2094) jour, mois, sc%annee(1)
              end if
            else
               call get_day_month(i,sc%annee(p%ilevs),mois,jour,nummois)
               write(fb,mes2094)jour, mois, sc%annee(p%idebdorms)
            endif
          endif

! fin de dormance
        case(fdo)
          if (p%P_codebfroid == 3) then
            i = p%ifindorms
            if (p%ifindorms > sc%nbjsemis) i = p%ifindorms - sc%nbjsemis +1
            call get_day_month(i,sc%annee(p%ifindorms),mois,jour,nummois)
            !DR 04/10/2022 j'ajoute le cas du forcage de la fin de dormance
            if (p%P_codedormance == 1) write(fb,mes2093a) jour, mois, sc%annee(p%ifindorms)
            if (p%P_codedormance == 2) write(fb,mes2093) jour, mois, sc%annee(p%ifindorms), p%cu(p%nfindorm)
            if (p%P_codedormance == 3) write(fb,mes2095) jour, mois, sc%annee(p%ifindorms), p%cu(p%nfindorm)
          endif

      end select
    end do

return
end subroutine bilanStadesDeveloppement

! harvest components
subroutine bilanRecolte(sc,p,itk,fb)
    type(Stics_Communs_),       intent(IN)    :: sc  
    type(Plante_),              intent(INOUT) :: p  
    type(ITK_),                 intent(IN)    :: itk  
    integer,                    intent(IN)    :: fb  

    integer           :: irecolte  !  
    integer           ::  julrec  !  
    integer           ::  julan  
    integer           :: jour  !  
    integer           :: nummois  
    character(len=3)  :: mois  
    real              :: tenMF  

    ! pour alleger l'ecriture
    !integer ::  AS, AO, AOAS
    !AS = sc%AS
    !AO = sc%AO
    !AOAS = AOAS


    if (itk%P_codcueille == 1) then
      write(fb,mes412)
    else
      if(itk%P_nbcueille == 1) then
        write(fb,mes414)
      else
        write(fb,mes415) itk%P_cadencerec
      endif
    endif

! DR 11/04/06 je mets un message au cas ou mat posterieur a rec
    if (p%nrec < p%nmat) then
       write(fb,mes48)
    endif

! calcul de la date de recolte
! ---------------------------------------------------------------
!  plusieurs criteres pour la recolte  :
!     1- la maturite physiologique (p%P_codrecolte=1)
!     2- la teneur en eau          (p%P_codrecolte=2)
!     3- la teneur en sucre        (p%P_codrecolte=3)
!     4- la teneur en proteine     (p%P_codrecolte=4)
!     5- la teneur en huile        (p%P_codrecolte=5)
! ---------------------------------------------------------------
    if (itk%P_nbcueille == 1) then
      if (itk%P_codrecolte == 1) write(fb,mes417)
      if (itk%P_codrecolte == 2) write(fb,mes418)
      if (itk%P_codrecolte == 3) write(fb,mes419)
      if (itk%P_codrecolte == 4) write(fb,mes420)
      if (itk%P_codrecolte == 5) write(fb,mes421)
    endif

  ! etalement de la recolte
    if (itk%P_nbcueille == 2) then
      write(fb,mes422)
      do irecolte = 1,p%nbrecolte-1
        julrec = p%nrecint(irecolte) + sc%P_iwater - 1
        julan = julrec
        ! PB - teauint & nbfrint ne sont pas calcules pour les 2 composantes AO/AS
        !      dans la version modulaire on calcule directement le cumul dans 'recolte'
        !--p%teauint(AOAS,irecolte) = p%teauint(AS,irecolte) * p%surf(AS) &
        !--                            + p%teauint(AO,irecolte) * p%surf(AO)
        !--p%nbfrint(AOAS,irecolte) = p%nbfrint(AS,irecolte) * p%surf(AS) &
        !--                            + p%nbfrint(AO,irecolte) * p%surf(AO)

        if (julrec > sc%nbjsemis) julrec = julrec - sc%nbjsemis
 ! DR 05/12/2014 pb d'annee pour les bissextiles
 !       call get_day_month(julrec,sc%annee(julan),mois,jour,nummois)
        call get_day_month(julrec,sc%annee(julrec),mois,jour,nummois)

        tenMF = (1. - p%teauint(AOAS,irecolte)) * 100.
 ! DR 05/12/2014 pb d'annee pour les bissextiles
!        write(fb,mes3101) irecolte,jour,mois,sc%annee(julan),   &
        write(fb,mes3101) irecolte,jour,mois,sc%annee(julrec),   &
                          p%rdtint(AOAS,irecolte)/100.,      &
                          p%nbfrint(AOAS,irecolte),tenMF

      ! on reintegre dans pdsfruit et nfruit ce qu'on avait enleve lors des cueillettes
        p%pdsfruit(AOAS,p%P_nboite) = p%pdsfruit(AOAS,p%P_nboite) &
                                     + p%rdtint(AOAS,irecolte)
        p%nfruit(AOAS,p%P_nboite) = p%nfruit(AOAS,p%P_nboite) &
                                   + p%nbfrint(AOAS,irecolte)


        p%nrec = p%nrecint(irecolte)
      end do
    endif

return
end subroutine bilanRecolte

!> SNOW
subroutine snowBalance(fb,c,P_codesnow,numcult)
    integer, intent(IN) :: fb
    type(Climat_),        intent(IN)    :: c  
    integer,              intent(IN)    :: P_codesnow    
    integer,              intent(IN)    :: numcult 
    logical :: snow_lasting_over_3cm = .FALSE.
    logical :: snow_lasting_over_10cm = .FALSE.
    logical :: any_snow_lasting = .FALSE.

    ! snow module call activated
     if (P_codesnow.eq.1) then   
          snow_lasting_over_3cm = c%ndays_sdepth_over_3cm(numcult).gt.snow_cum_days_threshold
          ! print *, lasting_3cm
          snow_lasting_over_10cm = c%ndays_sdepth_over_10cm(numcult).gt.snow_cum_days_threshold
          ! print *, lasting_10cm
          
          any_snow_lasting = snow_lasting_over_3cm.or.snow_lasting_over_10cm

          if (any_snow_lasting) then
              if (numcult.eq.1) then
                  write(fb,*) '--------------------------------------------------------------------------------------'
                  write(fb,*) ' Snow information:'
              endif
              ! for each year 
              write(fb,*) '--------------------------------------------------------------------------------------'
              write(fb,*) ' Year :',numcult
              if (snow_lasting_over_3cm) then
                   write(fb,*) ' Number of days with a snow depth over 3 cm :',c%ndays_sdepth_over_3cm(numcult)
              endif
              if (snow_lasting_over_10cm) then
                   write(fb,*) ' Number of days with a snow depth over 10 cm :',c%ndays_sdepth_over_10cm(numcult)
              endif
              write(fb,*) '--------------------------------------------------------------------------------------'
              write(fb,*) ' '
          endif
      endif 

return
end subroutine snowBalance
end module Bilans
