! ---------------------------------------------------------
!     calculs de fin de simulation
!     + ecriture du fichier recup.tmp
! ---------------------------------------------------------
! Write in the file recup.tmp for the next crop
module Ecriture_DonneesFinDeCycle_m
use stics_system
use Stics
use Parametres_Generaux
use Plante
use Root
use Itineraire_Technique
use Sol
use messages
use plant_utils
implicit none
private
public :: Ecriture_DonneesFinDeCycle
contains
subroutine Ecriture_DonneesFinDeCycle(logger,path,sc, pg, p, r, itk, soil)
  type(logger_), intent(in) :: logger
  character(*), intent(in) :: path
  type(Stics_Communs_),       intent(INOUT) :: sc  
  type(Parametres_Generaux_), intent(IN)    :: pg  
  type(Plante_),              intent(INOUT) :: p(sc%P_nbplantes)
  type(Root_),                intent(INOUT) :: r(sc%P_nbplantes)
  type(ITK_),                 intent(INOUT) :: itk(sc%P_nbplantes)  
  type(Sol_),                 intent(INOUT) :: soil

! les VARIABLES LOCALES
  integer :: ifwateravant  !  
  integer :: i  !  
  integer :: ir  !  
  integer :: idx  !  
  integer :: icou  !  
  integer :: iz  
  integer :: nbresid        ! nombre de types de residus en mulch

  integer :: irac
  integer :: nbcourac
  integer :: ndf
  integer :: ndg
  integer :: nzero

  integer :: recup, iostat
  
  open (newunit=recup, file=path, status=unknown, action=write_, iostat=iostat)
  if (iostat .ne. 0) then
    call exit_error(logger, "Error opening recup.tmp file: [" // path // "]")
  end if

      nbresid = (pg%nbResidus-1)/2


      if (sc%ifwater_courant > sc%nbjsemis) then
        ifwateravant = sc%ifwater_courant - sc%nbjsemis
      else
        ifwateravant = sc%ifwater_courant
      endif
      
      !write(recup,*) sc%nbjanrec, ifwateravant, sc%nhe, sc%n, sc%codeenteterap
      ! PL: 10/4/2019: Ajout du nombre de plantes sc%P_nbplantes courant pour pouvoir lire
      ! correctement en cas d'enchainement dans l'usm suivante !
      write(recup,*) sc%nbjanrec, ifwateravant, sc%nhe, sc%n, sc%codeenteterap, sc%P_nbplantes

! Modif Bruno mars 2018
      write(recup,*) sc%NH
      write(recup,*) (sc%HR(i),i = 1,sc%NH)        !5)
      write(recup,*) (soil%AZnit(i),i = 1,sc%NH)   !5)
      write(recup,*) (sc%AZamm(i),i = 1,sc%NH)     !5)
      write(recup,*) (sc%TS(i),i = 1,sc%NH)        !5)
      write(recup,*) sc%tcultveille, sc%tairveille

      write(recup,*) sc%Chumt,sc%Chuma,sc%Chumi,sc%Nhumt,sc%Nhuma,sc%Nhumi,sc%Cr,sc%Nr,sc%Cb,sc%Nb,sc%Cmulchnd,sc%Nmulchnd, &
                  sc%Cmulchdec,sc%Nmulchdec,sc%Cbmulch,sc%Nbmulch,sc%Crprof,sc%Nrprof

      do ir = 1,pg%nbResidus
         write(recup,*) sc%kres(ir),pg%P_kbio(ir),sc%hres(ir),sc%Wb(ir)
      end do
      do ir = 1,nbResid
         write(recup,*) sc%Cnondec(ir),sc%Nnondec(ir)
      end do

 ! Modif Loic 2016: j'enregistre les residus jusqu'a obstarac car on a maintenant des residus qui
 ! proviennent des racines profondes cad en-dessous de profhum
      irac = int(soil%P_obstarac)
      do ir = 1,pg%nbResidus
            nzero = 0
            do iz = irac,1,-1
               if(sc%Cres(iz,ir).gt.0.) exit
               nzero = nzero + 1
            enddo
            write(recup,*) nzero
            write(recup,*) (sc%Cres(iz,ir),iz=1,irac-nzero)

            nzero = 0
            do iz = irac,1,-1
               if(sc%Nres(iz,ir).gt.0.) exit
               nzero = nzero + 1
            enddo
            write(recup,*) nzero
            write(recup,*) (sc%Nres(iz,ir),iz=1,irac-nzero)

            nzero = 0
            do iz = irac,1,-1
               if(sc%Cbio(iz,ir).gt.0.) exit
               nzero = nzero + 1
            enddo
            write(recup,*) nzero
            write(recup,*) (sc%Cbio(iz,ir),iz=1,irac-nzero)

            nzero = 0
            do iz = irac,1,-1
               if(sc%Nbio(iz,ir).gt.0.) exit
               nzero = nzero + 1
            enddo
            write(recup,*) nzero
            write(recup,*) (sc%Nbio(iz,ir),iz=1,irac-nzero)
      enddo
      nzero = 0
      do iz = irac,1,-1
           if(sc%Chum(iz).gt.0.) exit
           nzero = nzero + 1
      enddo
      write(recup,*) nzero
      write(recup,*) (sc%Chum(iz),iz=1,irac-nzero)

      nzero = 0
      do iz = irac,1,-1
           if(sc%Nhum(iz).gt.0.) exit
           nzero = nzero + 1
      enddo
      write(recup,*) nzero
      write(recup,*) (sc%Nhum(iz),iz=1,irac-nzero)


      do iz = 1,sc%nhe
          write(recup,*) sc%hur(iz), sc%sat(iz), soil%nit(iz), soil%amm(iz), sc%tsol(iz)
      end do

! DR 14/10/2011 mise en conformite param.sol
      write(recup,*) soil%P_numsol,soil%P_typsol,soil%P_argi,soil%P_Norg,soil%P_profhum,soil%P_calc,soil%P_pH0, &
                  soil%P_concseuil,soil%P_albedo,soil%P_q0,soil%P_ruisolnu,soil%P_obstarac,soil%P_pluiebat, &
                  soil%P_mulchbat,soil%P_zesx,soil%P_cfes,soil%P_z0solnu ,soil%CsurNsol, soil%P_penterui

      write(recup,*) soil%P_numsol,soil%P_codecailloux,soil%P_codemacropor,soil%P_codefente,   &
                  soil%P_codrainage,soil%P_coderemontcap,soil%P_codenitrif,soil%P_codedenit

      write(recup,*) soil%P_numsol,soil%P_profimper,soil%P_ecartdrain,soil%P_ksol,soil%P_profdrain,     &
                  soil%P_capiljour,soil%P_humcapil,soil%P_profdenit,soil%P_vpotdenit

      do icou = 1,sc%NH    !5
        write(recup,*) soil%P_numsol,soil%P_epc(icou),soil%P_hccf(icou),soil%P_hminf(icou),  &
                    soil%P_DAF(icou),soil%P_cailloux(icou),soil%P_typecailloux(icou),    &
                    soil%P_infil(icou),soil%P_epd(icou)
      end do

      if (sc%n >= sc%maxwth) then
        do idx = 1, sc%P_nbplantes
          p(idx)%P_QNplantenp0 = p(idx)%QNplantenp(AOAS,sc%n)
!     Loic Septembre 2021: on ecrit tout le temps les variables plantes
!          if (pg%P_codeinitprec == 2) then
 !    le idx qui suit est-il utile ?  il n'est pas lu dans la routine de lecture de recup
            write(recup,*) 'Plante : ',idx
 !    le sc%n qui suit est-il utile ? il n'est pas lu dans la routine de lecture de recup
            write(recup,'(a8,2x,i3)') p(idx)%P_codeplante, sc%n
            write(recup,*) itk(idx)%P_ressuite

            if (p(idx)%P_codebfroid == 3 .and. p(idx)%P_codedormance == 3) then
               p(idx)%P_QNplantenp0 = p(idx)%QNplantenp(AOAS,p(idx)%ntaille-1)    &
                                    - p(idx)%mabois(AS) * 0.5 * 10.               &
                                    - p(idx)%mabois(AO) * 0.5 * 10.               &
                                    - p(idx)%QNgrain_ntailleveille
               if (p(idx)%P_QNplantenp0 <= 0.) p(idx)%P_QNplantenp0 = 0.
            else
               p(idx)%P_QNplantenp0 = p(idx)%QNplantenp(AOAS,sc%n)
            endif

            write(recup,*) p(idx)%lai(AOAS,sc%n), p(idx)%masecnp(AOAS,sc%n), p(idx)%magrain(AOAS,sc%n), &
                        p(idx)%zrac, p(idx)%P_QNplantenp0, p(idx)%restemp(AOAS), p(idx)%resperenne(AOAS), &
                        p(idx)%maperenne(AOAS), p(idx)%QNresperenne(AOAS), p(idx)%QNperenne(AOAS), &
                        p(idx)%resperennestruc,p(idx)%QNresperennestruc, p(idx)%msrac(sc%n), p(idx)%QNrac, p(idx)%QNrestemp, &
                        p(idx)%mafeuil(AOAS), p(idx)%mafeuilverte(AOAS), p(idx)%matigestruc(AOAS),  &
                        p(idx)%somcourno, p(idx)%ndno, p(idx)%nfno, &
                        p(idx)%QNfeuille, p(idx)%QNtige, p(idx)%msresgel(AOAS), p(idx)%QNresgel(AOAS), &
 ! Bruno juin 2017
                        p(idx)%QNabsotot(AS), p(idx)%QNabsotot(AO), p(idx)%Qfixtot(AS), p(idx)%Qfixtot(AO), &
                        ! Ajout Florent C. et Fabien, Octobre 2019
                        p(idx)%Qfix(AOAS)

            write(recup,*) p(idx)%cu(sc%n)
            write(recup,*) (p(idx)%LRACH(i),i = 1,sc%NH)   !5)

! On enregistre le profil seulement jusqu'a zrac (=nbcourac)
!           nbcourac = nint(p(idx)%zrac) + 1
            nbcourac = int(p(idx)%zrac + 0.999999)
            ndf = sc%n + p(idx)%ndecalf
            ndg = sc%n + p(idx)%ndecalg
            p(idx)%ndecalf = ndf - p(idx)%nsencourpreracf
            p(idx)%ndecalg = ndg - p(idx)%nsencourpreracg
! Ajout Bruno novembre 2018
            if(p(idx)%P_codeplante == CODE_BARESOIL) then
              p(idx)%ndecalf = 0
              p(idx)%ndecalg = 0
            endif

! DR 10/01/2022 on supprime l'ecriture de ce fichier de debuggage qui n'est pas utile
          !  open (141,file='decal.txt',position='append')
          !  write(141,*) sc%n, ndf-sc%n, ndf, p(idx)%nsencourpreracf, p(idx)%ndecalf, &
          !                     ndg-sc%n, ndg, p(idx)%nsencourpreracg, p(idx)%ndecalg
! fin ajout
            write(recup,*) p(idx)%ndecalf, p(idx)%ndecalg, p(idx)%ndebsenracf, p(idx)%ndebsenracg, p(idx)%somtempracvie
            do iz = 1,nbcourac
               write(recup,*) p(idx)%rlf(iz), p(idx)%rlg(iz)
            end do

! modif Bruno septembre 2018 : permutation des lignes et colonnes de drlf et drlg
! on recherche le nombre de valeurs nulles (nzero) et on n'enregistre que les valeurs non nulles
            do iz = 1,nbcourac
                nzero = 0
               do i = ndf, p(idx)%nsencourpreracf+1, -1
                   if(r(idx)%drlf(iz,i).ne.0.) exit
                   nzero = nzero + 1
               enddo
                write(recup,*) nzero
!               write(recup,*) (r(idx)%drlf(i,iz), i=p(idx)%nsencourpreracf+1, ndf-nzero)
                write(recup,*) (r(idx)%drlf(iz,i), i=p(idx)%nsencourpreracf+1, ndf-nzero)
            enddo

            do iz = 1,nbcourac
                nzero = 0
               do i = ndg, p(idx)%nsencourpreracg+1, -1
                  if(r(idx)%drlg(iz,i).ne.0.) exit
                  nzero = nzero + 1
               enddo
                write(recup,*) nzero
!               write(recup,*) (r(idx)%drlg(i,iz), i=p(idx)%nsencourpreracg+1, ndg-nzero)
                write(recup,*) (r(idx)%drlg(iz,i), i=p(idx)%nsencourpreracg+1, ndg-nzero)
            enddo

            nzero = 0
           do i = ndg, p(idx)%nsencourpreracg+1, -1
                if(p(idx)%dtj(i).ne.0.) exit
                nzero = nzero + 1
           enddo
            write(recup,*) nzero
            do i = p(idx)%nsencourpreracg+1, ndg - nzero
                write(recup,*) p(idx)%dtj(i)
            enddo

            p(idx)%P_lai0     = p(idx)%lai(AOAS,sc%n-1)
            p(idx)%P_masecnp0 = p(idx)%masecnp(AOAS,sc%n-1)
            p(idx)%P_zrac0    = p(idx)%zrac

            do iz = 1,sc%NH    !5
              p(idx)%P_densinitial(iz) = p(idx)%LRACH(iz)
            enddo

            p(idx)%P_restemp0 = p(idx)%restemp(AOAS)
    ! 07/09/06 IGC seul devant le danger!!! Voici que je vais mettre un petit test
    ! pour laisser le calcul de P_QNplante0 egal a la valeur qui a ete observe dans la
    ! plante le derniere jour avant la taille (car le jour de taille QNplante devient
    ! egal a 0). Le test est base sur la caracteristique de la dormance de ligneuses
    !     if(P_codedormance(idx) == 3)then
    !        P_QNplante0(idx) = QNplanteC(idx,n)-mabois(idx,1)*
    !    s     CNplante(idx,1)*10 -mabois(idx,2)* CNplante(idx,2)*10
    !     endif
    ! DR 31/01/07 IGC seul devant le danger ca merde !
    ! il faut tester aussi sur le P_codebfroid sinon on peut aller dans ce test pour du ble ou de la parierie
    !     if(P_codedormance(idx) == 3)then
    !     if(P_codebfroid(idx) == 3.and.P_codedormance(idx) == 3)then
    !        P_QNplante0(idx) = QNplanteC(idx,ntaille(idx)-1)
    !    s     -mabois(idx,1)*0.5*10 -mabois(idx,2)* 0.5*10
    !    s       - QngrainC(idx,ntaille(idx)-1)
    !     endif
            p(idx)%mafeuil0 = p(idx)%mafeuil(AOAS)
            p(idx)%mafeuilverte0 = p(idx)%mafeuilverte(AOAS)
            p(idx)%matigestruc0 = p(idx)%matigestruc(AOAS)

            p(idx)%P_magrain0 = 0.

        ! DR 16/08/06 recuperer l'etat de la dormance si on est en enchainement de perenne
        !  besoins en froid et besoins en chaud
        !   cu0 = p(idx)%cu(sc%n-1)
        !   cu0 = p(idx)%cu(sc%n)
            p(idx)%cu0 = p(idx)%cu(sc%n)
            p(idx)%somelong0 = p(idx)%somelong

          ! DR et IGC 17/03/08 on garde nfindorm si il se passe l'annee en cours
            if (abs(p(idx)%cu0).lt.1.0E-8) then
              p(idx)%nfindorm0 = p(idx)%nfindorm
            else
              p(idx)%nfindorm0 = 0
            endif
!          endif codeinitprec
        end do
      endif

    close(recup)
end subroutine Ecriture_DonneesFinDeCycle
end module Ecriture_DonneesFinDeCycle_m
 
