module stics_system_tests
   use stics_system
   use testdrive, only: error_type, unittest_type, new_unittest, check

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_assert_path_exist_true", test_assert_path_exist_true) &
                  , new_unittest("test_assert_path_exist_false", test_assert_path_exist_false) &
                  , new_unittest("test_assert_isdir_true", test_assert_isdir_true) &
                  , new_unittest("test_assert_isdir_false", test_assert_isdir_false) &
                  , new_unittest("test_erase_path", test_erase_path) &
                  , new_unittest("test_get_file_stat", test_get_file_stat) &
                  , new_unittest("test_join_path", test_join_path) &
                  ]

   end subroutine

   function get_directory_path() result(full_path)
      character(:), allocatable :: full_path
      character(len=255) :: cwd

      call getcwd(cwd)
      full_path = join_path(trim(cwd),'src','utilities','stics_system')
   end function

   function get_file_path() result(full_path)
      character(:), allocatable :: full_path
      full_path = join_path(get_directory_path(),'stics_system_tests_dummy.txt')
   end function

   subroutine test_assert_path_exist_true(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: file_path

      file_path = get_file_path()
      call check(error, path_exist(file_path), .true.)
   end subroutine

   subroutine test_assert_path_exist_false(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: directory_path

      call check(error, path_exist(''), .false.)
      call check(error, path_exist('nofile.txt'), .false.)

      directory_path = get_directory_path()
      ! gfortran don't make difference between file and directory...
      call check(error, path_exist(directory_path), .true.)
   end subroutine

   subroutine test_assert_isdir_true(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, isdir(get_directory_path()), .true.)
   end subroutine

   subroutine test_assert_isdir_false(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: a_path

      a_path = get_file_path()

      ! proving that this function does not work
      if (is_unix()) then
         call check(error, isdir(a_path), .false.)
      else
         call check(error, isdir(a_path), .true.)
      end if
   end subroutine

   subroutine test_erase_path(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: dummy_path
      integer :: unit, i, count
      dummy_path = "dummy_path.tmp"

      ! Fill a file with some content
      open (newunit=unit, file=dummy_path, status=unknown, action=write_)
      do i = 1, 100
         write (unit, *) i
      end do
      close (unit)

      ! Ensure the file is full of content
      open (newunit=unit, file=dummy_path, status=old, action=read_)
      call check(error, number_of_rows(unit), 100)
      close (unit)

      ! Actual action of the test
      call erase_path(dummy_path)

      open (newunit=unit, file=dummy_path, status=old, action=read_)
      ! Expected to have 1 empty row
      call check(error, number_of_rows(unit), 1)
      close (unit)

      ! Cleanup
      call delete_path(dummy_path)
   end subroutine

   subroutine test_get_file_stat(error)
      type(error_type), allocatable, intent(out) :: error

      character(:), allocatable :: path
      integer, dimension(13) :: buffer
      type(File_Stat_) :: file_stat1, file_stat2
      integer :: unit

      path = get_file_path()

      call stat(trim(path), buffer)
      file_stat1 = parse_stat_buffer(buffer)

      open (newunit=unit, file=path)
      call fstat(unit, buffer)
      file_stat2 = parse_stat_buffer(buffer)

      call check(error, file_stat1%size, 12)
      call check(error, file_stat1%last_change > 0)
      call check(error, file_stat1%last_modification > 0)

      call check(error, file_stat1%size, file_stat2%size)
      call check(error, file_stat2%last_access - file_stat1%last_access >= 0)
      call check(error, file_stat1%last_modification, file_stat2%last_modification)
      call check(error, file_stat1%last_change, file_stat2%last_change)
   end subroutine

   subroutine test_join_path(error)
      type(error_type), allocatable, intent(out) :: error
      character(len=1) :: sep

      sep = filesep()
      call check(error, join_path("left", "right"), "left"//sep//"right")
      call check(error, join_path("left\middle", "right"), "left\middle"//sep//"right")
      call check(error, join_path("path1", "path2", "path3"), "path1"//sep//"path2"//sep//"path3")
      call check(error, join_path("path1", "path2", "path3", "path4"), &
         "path1"//sep//"path2"//sep//"path3"//sep//"path4")
      call check(error, join_path("path1", "path2", "path3", "path4", "path5"), &
         "path1"//sep//"path2"//sep//"path3"//sep//"path4"//sep//"path5")
      call check(error, join_path("path1", "path2", "path3", "path4", "path5", "path6"), &
         "path1"//sep//"path2"//sep//"path3"//sep//"path4"//sep//"path5"//sep//"path6")
   end subroutine
end module
