module stics_system
   implicit none

   integer, parameter :: system_path_length = 255
   integer, parameter :: files_path_length = 255
   integer, parameter :: min_file_unit = 34
   integer, save :: current_free_file_unit = min_file_unit

   ! set it to .true. for displaying messages, file content
   logical, parameter :: files_debug = .false.

   ! file position
   character(len=10), public, parameter :: append = 'append'
   character(len=10), public, parameter :: asis = 'asis'

   ! file actions
   character(len=10), public, parameter :: write_ = 'write'
   character(len=10), public, parameter :: readwrite = 'readwrite'
   character(len=10), public, parameter :: read_ = 'read'

   ! file status
   character(len=10), public, parameter :: old = 'old'
   character(len=10), public, parameter :: unknown = 'unknown'
   character(len=10), public, parameter :: new = 'new'
   character(len=10), public, parameter :: replace_ = 'replace'
   character(len=10), public, parameter :: scratch = 'scratch'

   interface join_path
      module procedure join_path_2, join_path_3, join_path_4, join_path_5, join_path_6
   end interface

   !> Structure for storing informations about a file
   !> Getting informations dynamically through a function using *stat fortran fonctions.
   !> Not all these elements are relevant on all systems. If an element is not relevant, it is returned as 0.
   !> https://gcc.gnu.org/onlinedocs/gcc-4.2.4/gfortran/STAT.html
   type File_Stat_
      !> Device ID
      integer :: device_id
      !> Inode number
      integer :: innode_number
      !> File mode
      integer :: file_mode
      !> Number of links
      integer :: number_of_links
      !> Owner's uid
      integer :: owners_uid
      !> Owner's gid
      integer :: owners_gid
      !> ID of device containing directory entry for file (0 if not available)
      integer :: container_device_id
      !> File size (bytes)
      integer :: size
      !> Last access time
      integer :: last_access
      !> Last modification time
      integer :: last_modification
      !> Last file status change time
      integer :: last_change
      !> Preferred I/O block size (-1 if not available)
      integer :: preferred_io_block_size
      !> Number of blocks allocated (-1 if not available)
      integer :: number_of_block_allocated
   end type
contains

   ! Keeping this logic, unused for the time being, but can be useful
   !  if (is_unix()) then
   !     sys%copy='cp '
   !     sys%move='mv '
   !     sys%disp='cat '
   !     sys%delete_file='rm '
   !     sys%delete_dir='rmdir '
   !     sys%recursive_delete='rm -fR '
   !  else
   !     sys%copy='copy '
   !     sys%move='move /Y'
   !     sys%disp='type '
   !     sys%delete_file='del '
   !    sys%delete_dir='rmdir '
   !    sys%recursive_delete='rmdir /S /Q '
   ! end if

   logical function is_unix()
      character(len=255) :: cwd
      call getcwd(cwd)
      if (index(trim(cwd), '/') .gt. 0) then
         is_unix = .true.
      else
         is_unix = .false.
      end if
   end function

   character(len=1) function filesep()
      if (is_unix()) then
         filesep = '/'
      else
         filesep = '\'
      end if
   end function

   logical function path_exist(path) result(exist)
      character(*), intent(in) :: path

      inquire (file=trim(path), exist=exist)

      if (files_debug) then
         if (exist) then
            write (*, *) path//' exists'
         else
            write (*, *) path//" doesn't exist "
         end if
      end if
   end function

   ! SEE: UNLINK fortran func/subr. ???
   subroutine delete_path(path)
      character(*), intent(in) :: path
      integer :: status
      if (path_exist(path)) then
         status = delete(path)
      end if
   end subroutine

   !> Test if a directory exists. If not, it executes a mkdir command.
   function ensure_directory(a_path) result(status)
      character(*), intent(in) :: a_path
      integer :: status

      if (isdir(a_path) .eqv. .false.) then
         status = execute_command('mkdir '//a_path)
      else
         status = 0
      end if
   end function

   logical function isdir(a_path)
      character(*), intent(in) :: a_path
      character(:), allocatable :: dot_file_path

      ! On Windows and Linux/UNIX/OSX a directory contains a special file named "."
      dot_file_path = trim(a_path)//filesep()//'. '
      inquire (file=dot_file_path, exist=isdir)
   end function

   integer function execute_command(cmd) result(status)
      character(*), intent(in) :: cmd

      call system(cmd, status)
      if (status > 0) then
         print *, 'Error while executing command :', cmd
      end if
   end function

   integer function delete(path) result(status)
      character(*), intent(in) :: path
      character(:), allocatable :: cmd

      if (is_unix()) then
         cmd = 'rm'
      else
         cmd = 'del'
      end if

      cmd = cmd//" "//path
      status = execute_command(cmd)
   end function

   ! checking if a unit is in use
   logical function is_unit_used(unit)
      integer, intent(in) :: unit

      integer :: iostat
      logical :: opened

      inquire (unit=unit, opened=opened, iostat=iostat)

      is_unit_used = opened .or. iostat .ne. 0

   end function

   ! testing if a unit is open
   logical function is_unit_opened(unit) result(opened)
      integer, intent(in) :: unit
      character(len=files_path_length) :: name

      inquire (unit=unit, opened=opened, name=name)
      if (opened .and. files_debug) then
         print *, name, ': File already open!'
      end if
   end function

   integer function number_of_rows(s) result(nrows)
   !! Determine the number or rows in a file
      integer, intent(in)::s
      integer :: ios

      rewind (s)
      nrows = 0
      do
         read (s, *, iostat=ios)
         if (ios /= 0) exit
         nrows = nrows + 1
      end do

      rewind (s)

   end function

   !> Open a file and write empty into it in order to erase it
   subroutine erase_path(path)
      character(*), intent(in) :: path

      integer :: unit

      open (newunit=unit, file=path, status=unknown, action=write_)
      write (unit, *)
      close (unit)
   end subroutine

   ! getting file stats from buffer returned by stat and fstat
   type(File_Stat_) pure function parse_stat_buffer(buffer) result(file_stat)
      integer, dimension(13), intent(in) :: buffer
      file_stat%device_id = buffer(1)
      file_stat%innode_number = buffer(2)
      file_stat%file_mode = buffer(3)
      file_stat%number_of_links = buffer(4)
      file_stat%owners_uid = buffer(5)
      file_stat%owners_gid = buffer(6)
      file_stat%container_device_id = buffer(7)
      file_stat%size = buffer(8)
      file_stat%last_access = buffer(9)
      file_stat%last_modification = buffer(10)
      file_stat%last_change = buffer(11)
      file_stat%preferred_io_block_size = buffer(12)
      file_stat%number_of_block_allocated = buffer(13)
   end function

   subroutine write_file_stat(file_stat)
      type(File_Stat_), intent(in) :: file_stat
      write (*, fmt="('Device ID:',               T30, I19)") file_stat%device_id
      write (*, fmt="('Inode number:',            T30, I19)") file_stat%innode_number
      write (*, fmt="('File mode (octal):',       T30, O19)") file_stat%file_mode
      write (*, fmt="('Number of links:',         T30, I19)") file_stat%number_of_links
      write (*, fmt="('Owner''s uid:',            T30, I19)") file_stat%owners_uid
      write (*, fmt="('Owner''s gid:',            T30, I19)") file_stat%owners_gid
      write (*, fmt="('Device where located:',    T30, I19)") file_stat%container_device_id
      write (*, fmt="('File size:',               T30, I19)") file_stat%size
      write (*, fmt="('Last access time:',        T30, A19)") ctime(file_stat%last_access)
      write (*, fmt="('Last modification time',   T30, A19)") ctime(file_stat%last_modification)
      write (*, fmt="('Last status change time:', T30, A19)") ctime(file_stat%last_change)
      write (*, fmt="('Preferred block size:',    T30, I19)") file_stat%preferred_io_block_size
      write (*, fmt="('No. of blocks allocated:', T30, I19)") file_stat%number_of_block_allocated
   end subroutine

   !> Concatenates two paths into a single path.
   function join_path_2(path1, path2) result(joined)
      character(*), intent(in) :: path1, path2
      character(:), allocatable :: joined

      character(len=1) :: sep
      sep = filesep()
      if (path1 == "") then
         joined = path2
      else
         joined = path1//sep//path2
      end if
   end function

   function join_path_3(path1, path2, path3) result(joined)
      character(*), intent(in) :: path1, path2, path3
      character(:), allocatable :: joined

      character(len=1) :: sep
      sep = filesep()
      joined = path1//sep//path2//sep//path3
   end function

   function join_path_4(path1, path2, path3, path4) result(joined)
      character(*), intent(in) :: path1, path2, path3, path4
      character(:), allocatable :: joined

      character(len=1) :: sep
      sep = filesep()
      joined = path1//sep//path2//sep//path3//sep//path4
   end function

   function join_path_5(path1, path2, path3, path4, path5) result(joined)
      character(*), intent(in) :: path1, path2, path3, path4, path5
      character(:), allocatable :: joined

      character(len=1) :: sep
      sep = filesep()
      joined = path1//sep//path2//sep//path3//sep//path4//sep//path5
   end function

   function join_path_6(path1, path2, path3, path4, path5, path6) result(joined)
      character(*), intent(in) :: path1, path2, path3, path4, path5, path6
      character(:), allocatable :: joined

      character(len=1) :: sep
      sep = filesep()
      joined = path1//sep//path2//sep//path3//sep//path4//sep//path5//sep//path6
   end function
end module
