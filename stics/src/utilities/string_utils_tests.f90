module string_utils_tests
   use stdlib_string_type
   use stdlib_strings, only: to_string
   use iso_fortran_env
   use string_utils
   use test_utils
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private

   public :: collect

contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest('test_bbch_string_to_int', test_bbch_string_to_int) &
                  , new_unittest('test_char_to_digit', test_char_to_digit) &
                  , new_unittest('test_to_string', test_to_string) &
                  , new_unittest('test_to_string_a', test_to_string_a) &
                  , new_unittest('test_count_chars', test_count_chars) &
                  , new_unittest('test_count_words', test_count_words) &
                  , new_unittest('test_split_sentence', test_split_sentence) &
                  , new_unittest('test_concat_array_string', test_concat_array_string) &
                  ]
   end subroutine

   subroutine test_bbch_string_to_int(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, bbch_string_to_int('-99'), -99)
      call check(error, bbch_string_to_int('-50'), -50)
      call check(error, bbch_string_to_int('-10'), -10)

      ! That's how the function is supposed to work, not sure if we should correct this
      call check(error, bbch_string_to_int('1'), 10)

      call check(error, bbch_string_to_int('01'), 1)
      call check(error, bbch_string_to_int('23'), 23)
      call check(error, bbch_string_to_int('50'), 50)
      call check(error, bbch_string_to_int('99'), 99)

   end subroutine

   subroutine test_char_to_digit(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, char_to_digit('0'), 0)
      call check(error, char_to_digit('1'), 1)
      call check(error, char_to_digit('2'), 2)
      call check(error, char_to_digit('3'), 3)
      call check(error, char_to_digit('4'), 4)
      call check(error, char_to_digit('5'), 5)
      call check(error, char_to_digit('6'), 6)
      call check(error, char_to_digit('7'), 7)
      call check(error, char_to_digit('8'), 8)
      call check(error, char_to_digit('9'), 9)
   
      call check(error, char_to_digit('%'), 0)
      call check(error, char_to_digit('-'), 0)
      call check(error, char_to_digit('a'), 0)
   end subroutine

   subroutine test_to_string(error)
      type(error_type), allocatable, intent(out) :: error

      integer(kind=int32) :: i32
      integer(kind=int32), dimension(0) :: empty

      call check(error, to_string(-99), '-99')

      call check(error, to_string(-99), '-99')
      call check(error, to_string(-1), '-1')
      call check(error, to_string(-0), '0')
      call check(error, to_string(0), '0')
      call check(error, to_string(1), '1')
      call check(error, to_string(99), '99')

      call check(error, to_string(huge(i32)), '2147483647')
      call check(error, to_string(-huge(i32)), '-2147483647')

      call check(error, to_string(99.9), '99.9000015')
      call check(error, to_string(-99.9), '-99.9000015')
      call check(error, to_string(0.), '0.00000000')
      call check(error, to_string(1/3.), '0.333333343')
   end subroutine

   subroutine test_to_string_a(error)
      type(error_type), allocatable, intent(out) :: error

      integer(kind=int32) :: i32
      integer(kind=int32), dimension(0) :: empty

      call check(error, to_string_a(empty), '')
      call check(error, to_string_a((/1/)), '1')
      call check(error, to_string_a((/1, 2, 3, 4/)), '1, 2, 3, 4')
      call check(error, to_string_a((/45, 32, 1851, -2/)), '45, 32, 1851, -2')

      call check(error, to_string_a(empty), '')
      call check(error, to_string_a((/1., 2., 3., 4./)), '1.00000000, 2.00000000, 3.00000000, 4.00000000')
      call check(error, to_string_a((/45., 32., 1851., -2./)), '45.0000000, 32.0000000, 1851.00000, -2.00000000')
   end subroutine

   subroutine test_count_chars(error)
      type(error_type), allocatable, intent(out) :: error

      call check(error, count_chars("abcd", "a"), 1)
      call check(error, count_chars("aaa", "a"), 3)
      call check(error, count_chars("aaa", "b"), 0)
      call check(error, count_chars("   ", " "), 3)
   end subroutine

   subroutine test_count_words(error)
      type(error_type), allocatable, intent(out) :: error

      call check(error, count_words(""), 0)
      call check(error, count_words("   "), 0)
      call check(error, count_words("Chum efnrac %rl"), 3)
      call check(error, count_words("                Chum "), 1)
      call check(error, count_words("   Chum    efnrac  "), 2)
      call check(error, count_words(" rlg(n,iz)  "), 1)
   end subroutine

   subroutine test_split_sentence(error)
      type(error_type), allocatable, intent(out) :: error
      type(string_type), allocatable :: expected0(:), expected1(:), expected2(:), expected3(:), expected4(:)

      allocate(expected0(0))
      call check_array_string(error, split_sentence(""), expected0)
      call check_array_string(error, split_sentence("    "), expected0)

      allocate(expected1(3), source=string_type(''))
      expected1 = (/ string_type('Chum'), string_type('efnrac'), string_type('%rl') /)
      call check_array_string(error, split_sentence("Chum efnrac %rl"), expected1)

      allocate(expected2(1), source=string_type(''))
      expected2 = (/ string_type('Chum') /)
      call check_array_string(error, split_sentence("                 Chum"), expected2)

      allocate(expected3(2), source=string_type(''))
      expected3 = (/ string_type('Chum'), string_type('efnrac') /)
      call check_array_string(error, split_sentence("   Chum    efnrac  "), expected3)

      allocate(expected4(2), source=string_type(''))
      expected3 = (/ string_type('Chum'), string_type('rlg(n,iz)') /)
      call check_array_string(error, split_sentence("   Chum    rlg(n,iz)   "), expected3)
   end subroutine

   subroutine test_concat_array_string(error)
      type(error_type), allocatable, intent(out) :: error
      type(string_type), allocatable :: input1(:), input2(:), input3(:)
      allocate(input1(0))
      call check(error, concat_array_string(input1), '')

      input2 = (/ string_type('') /)
      call check(error, concat_array_string(input2), '')

      input3 = (/ string_type(' 12'), string_type('13'), string_type('14'),&
         string_type(''), string_type('   '), string_type('15 ') /)
      call check(error, concat_array_string(input3), ' 121314   15 ')
   end subroutine
end module
