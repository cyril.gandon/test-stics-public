module math_utils
   use iso_fortran_env, only: real32
   implicit none

   real(real32), parameter :: PI = 3.14159265358979323_real32
   real(real32), parameter :: degrees_to_rads_factor = PI/180
contains
   pure real(real32) function degrees_to_rad(degrees)
      real(real32), intent(in) :: degrees
      degrees_to_rad = degrees*degrees_to_rads_factor
   end function

   pure real(real32) function rad_to_degrees(rad)
      real(real32), intent(in) :: rad
      rad_to_degrees = rad/degrees_to_rads_factor
   end function

   !> From a line passing by (x1, y1) and (x2, y2), determine y from x on the line.
   !! If x is not in [x1, x2], y got the value y1 or y2.
   pure real(real32) function from_linear(x, x1, x2, y1, y2) result(y)
      real(real32), intent(in) :: x, x1, y1, x2, y2
      real(real32) :: dx, slope

      if (x <= x1) then
         y = y1
      else if (x >= x2) then
         y = y2
      else
         dx = x2 - x1
         if (abs(dx) .lt. 1.0E-8) then
            ! vertical line, give a fixed very high value for y
            y = 1.e10
         else
            slope = (y2 - y1)/dx
            y = y1 + (slope*(x - x1))
         end if
      end if
   end function

   !> Get count values evenly distribute from "from" to "to"
   pure function uniform_values(from, to, count) result(arr)
      integer, intent(in) :: from, to, count
      integer, allocatable :: arr(:)
      real :: frequency
      integer :: j
      if(from .gt. to .or. count .le. 0) then
         allocate(arr(0))
         return
      end if
      if(count .eq. 1) then
         allocate(arr(1))
         arr(1) = from
         return
      end if
      frequency = (to - from)/(count * 1. - 1.0)
      allocate (arr(count))
      do j = 1, count
         arr(j) = int(from + ((j - 1)*frequency))
      end do
   end function
end module
