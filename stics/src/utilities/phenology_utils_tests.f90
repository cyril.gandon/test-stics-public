module phenology_utils_tests
   use phenology_utils
   use test_utils
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_stage_for_date", test_get_stage_for_date) &
                  , new_unittest("test_is_n_this_stage", test_is_n_this_stage) &
                  , new_unittest("test_is_valid_stage", test_is_valid_stage) &
                  ]

   end subroutine

   subroutine test_get_stage_for_date(error)
      type(error_type), allocatable, intent(out) :: error

      integer :: n, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm
      integer :: i
      integer, allocatable :: day_stages(:)
      type(string_type), allocatable :: expected(:)

      nplt = 2
      nger = 3
      nlev = 4
      namf = 5
      nlax = 6
      ndrp = 7
      nflo = 8
      nsen = 9
      nrec = 10
      nmat = 11
      ndebdorm = 12
      nfindorm = 13

      day_stages = (/nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm/)
      expected = (/string_type(plt), string_type(ger), string_type(lev), string_type(amf), string_type(lax),&
                   string_type(drp), string_type(flo), string_type(sen), string_type(rec), string_type(mat),&
                   string_type(debdorm), string_type(findorm)/)

      do i = 1, size(day_stages)
         call check(error, &
                    get_stage_for_date(day_stages(i), nplt, nger, nlev, namf, nlax, ndrp, &
                    nflo, nsen, nrec, nmat, ndebdorm, nfindorm), &
                    char(expected(i)))
      end do

      call check(error, &
                 get_stage_for_date(-1, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm), &
                 "")
   end subroutine

   subroutine test_is_n_this_stage(error)
      type(error_type), allocatable, intent(out) :: error

      integer :: n, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm
      nplt = 2
      nger = 3
      nlev = 4
      namf = 5
      nlax = 6
      ndrp = 7
      nflo = 8
      nsen = 9
      nrec = 10
      nmat = 11
      ndebdorm = 12
      nfindorm = 13
      call check(error, is_n_this_stage(2, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm, plt),&
       .true.)
      call check(error, is_n_this_stage(3, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm, plt),&
       .false.)

   end subroutine

   subroutine test_is_valid_stage(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, is_valid_stage('plt'), .true.)
      call check(error, is_valid_stage('PLT'), .true.)

      call check(error, is_valid_stage('aaa'), .false.)
   end subroutine
end module
