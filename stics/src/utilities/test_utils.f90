module test_utils
   use stdlib_string_type
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
contains

   subroutine check_array(error, actual, expected)
      real, intent(in):: actual(:), expected(:)
      type(error_type), allocatable, intent(out) :: error
      integer :: index

      call check(error, size(actual), size(expected))

      do index = 1, size(actual)
         call check(error, actual(index), expected(index))
      end do
   end subroutine

   subroutine check_array_i(error, actual, expected)
      integer, intent(in):: actual(:), expected(:)
      type(error_type), allocatable, intent(out) :: error
      integer :: index

      call check(error, size(actual), size(expected))

      do index = 1, size(actual)
         call check(error, actual(index), expected(index))
      end do
   end subroutine

   subroutine check_array_string(error, actual, expected)
      type(string_type), intent(in):: actual(:), expected(:)
      type(error_type), allocatable, intent(out) :: error
      integer :: index

      call check(error, size(actual), size(expected))

      do index = 1, size(actual)
         call check(error, char(actual(index)), char(expected(index)))
      end do
   end subroutine
end module test_utils
