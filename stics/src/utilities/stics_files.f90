module stics_files
   implicit none

   private

   integer, parameter, public :: STANDARD_OUPUT = 6

   !! Input files
   character(*), public, parameter :: usm_path = 'new_travail.usm'
   character(*), public, parameter :: default_station_path = 'station.txt'
   character(*), public, parameter :: general_settings_path = 'tempopar.sti'
   character(*), public, parameter :: new_formalisms_path = 'tempoparv6.sti'
   character(*), public, parameter :: default_soil_path = 'param.sol'
   character(*), public, parameter :: default_init_path = 'ficini.txt'
   character(*), public, parameter :: ouput_variables_path = 'var.mod'
   character(*), public, parameter :: report_variables_path = 'rap.mod'
   character(*), public, parameter :: profil_path = 'prof.mod'
   character(*), public, parameter :: forced_parameter_path = 'param.sti'
   character(*), public, parameter :: depths_paramv6 = 'depths_paramv6.txt'

   !! Output files
   character(*), public, parameter :: swow_file = 'snow_variables.txt'
   character(*), public, parameter :: retrieval_file = 'recup.tmp'
   character(*), public, parameter :: climat_file = 'climat.txt'
   character(*), public, parameter :: history_file = 'modhistory.sti'
   character(*), public, parameter :: errors_file = 'stics_errors.log'
   character(*), public, parameter :: debug_file = 'mod_debug.sti'

   ! option for writing the output files
   ! (1 = mod_history.sti, 2=daily outputs,4= report outut,
   ! 8=balance outputs,16 = profil outputs,
   ! 32= debug outputs, 64 = screen outputs, 128 = agmip outputs)
   ! sum codes to have several types of outputs.
   integer, public, parameter :: ECRITURE_HISTORIQUE = 1
   integer, public, parameter :: ECRITURE_SORTIESJOUR = 2
   integer, public, parameter :: ECRITURE_RAPPORTS = 4
   integer, public, parameter :: ECRITURE_BILAN = 8
   integer, public, parameter :: ECRITURE_PROFIL = 16
   integer, public, parameter :: ECRITURE_DEBUG = 32
  ! 29/08/2012 DR j'ajoute sorties ecran et Agmip et je mets flagecriture en parametre des parametres generaux
   integer, public, parameter :: ECRITURE_ECRAN = 64
   integer, public, parameter :: ECRITURE_AGMIP = 128
end module stics_files
