module dates_utils
   implicit none

   integer, parameter, dimension(13) :: cumulative_days_in_leap_year = (/0,31,60,91,121,152,182,213,244,274,305,335, 366/)
   integer, parameter, dimension(13) :: cumulative_days_in_not_leap_year = (/0,31,59,90,120,151,181,212,243,273,304,334,365/)
   character(3), parameter, dimension(12) :: month_codes = &
      (/'jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'/)


    !> Represents a day with a day number in the month, a month number in the year, and the year
    type day_dmy_
        integer :: day, month, year
    end type

contains

   !> Returns .true. if argument is a leap year, .false. otherwise.
   pure logical function is_leap_year(year)
      integer, intent(in) :: year

      ! Une annee bissextile est (divisible par 4 mais pas par 100) ou (divisible par 400)
      is_leap_year = ((mod(year, 4) == 0) .and. (mod(year, 100) /= 0)) .or. (mod(year, 400) == 0)
   end function is_leap_year

   !> Returns 366 if year is a leap year, 365 otherwise.
   pure integer function days_count(year)
      integer, intent(in) :: year  

      if (is_leap_year(year)) then
         days_count = 366
      else
         days_count = 365
      endif
   end function

   !> Returns the day number. January 1 is day 1. Result is in range [1, 366].
   pure integer function day_number(day,month,year)
       integer, intent(in) :: day, month, year
       integer :: bounded_month
       integer :: cumulative_days(13)

       if (is_leap_year(year)) then 
         cumulative_days = cumulative_days_in_leap_year
       else
         cumulative_days = cumulative_days_in_not_leap_year
       end if

       bounded_month = max(1, month)
       bounded_month = min(12, bounded_month)
       day_number = day + cumulative_days(month)
   end function

      !> Returns the day number. January 1 is day 1. Result is in range [1, 366].
   pure integer function day_number_dmy(day)
       type(day_dmy_), intent(in) :: day
       day_number_dmy = day_number(day%day, day%month, day%year)
   end function

   !> Return the date in the form day/month based on the day number and the year
   pure subroutine get_day_month(day_num,year,month_code,day,month)
      integer, intent(in) :: day_num  
      !! day number in the year [1, 366]

      integer, intent(in) :: year  
   
      character(len=3), intent(out) :: month_code
      !! month code on 3 letter

      integer, intent(out) :: day  
      !! day number in the month [1, 31]
      
      integer, intent(out) :: month    
      !! month [1, 12]

      integer :: month_index, cumulative_days(13)

       if (is_leap_year(year)) then 
         cumulative_days = cumulative_days_in_leap_year
       else
         cumulative_days = cumulative_days_in_not_leap_year
       end if
       
       day = 0
       month_code = ""
       month = 0

       do month_index = 1, 12
         if (day_num - cumulative_days(month_index + 1) <= 0) then
           day = day_num - cumulative_days(month_index)
           month_code = month_codes(month_index)
           month = month_index
           exit
         endif
       end do
   end subroutine
 
   !> Adjust a day number that can be over 365.
   !! Example: day 366 of 2021 is day 1 of 2022.
   pure subroutine modulo_years(day, year, adjusted_day, year_out)
      integer, intent(in) :: day, year
      integer, intent(out) :: adjusted_day
      integer, intent(out), optional :: year_out

      integer :: adjusted_year
      adjusted_year = year
      adjusted_day = day

      do while(adjusted_day > days_count(adjusted_year))
         adjusted_day = adjusted_day - days_count(adjusted_year)
         adjusted_year = adjusted_year + 1
      end do

      if(present(year_out)) then
         year_out = adjusted_year
      end if
   end subroutine

end module
