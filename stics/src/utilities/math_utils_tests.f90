module math_utils_tests
   use math_utils
   use test_utils
   use testdrive, only: error_type, unittest_type, new_unittest, check

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_rad_to_degrees", test_rad_to_degrees) &
                  , new_unittest("test_degrees_to_rad", test_degrees_to_rad) &
                  , new_unittest("test_from_linear", test_from_linear) &
                  , new_unittest("test_uniform_values", test_uniform_values) &
                  ]

   end subroutine

   subroutine test_rad_to_degrees(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, rad_to_degrees(0.), 0.)
      call check(error, rad_to_degrees(PI/2.), 90.)
      call check(error, rad_to_degrees(PI), 180.)
      call check(error, rad_to_degrees(3*PI/2.), 270.)
      call check(error, rad_to_degrees(2*PI), 360.)

   end subroutine

   subroutine test_degrees_to_rad(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, degrees_to_rad(0.), 0.)
      call check(error, degrees_to_rad(90.), PI/2.)
      call check(error, degrees_to_rad(180.), PI)
      call check(error, degrees_to_rad(270.), 3*PI/2.)
      call check(error, degrees_to_rad(360.), 2*PI)

   end subroutine

   subroutine test_from_linear(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, from_linear(1., 0., 2., 0., 2.), 1.)
      call check(error, from_linear(-1., 0., 2., 0., 2.), 0.)
      call check(error, from_linear(3., 0., 2., 0., 2.), 2.)

   end subroutine

   subroutine test_uniform_values(error)
      type(error_type), allocatable, intent(out) :: error
      integer, allocatable :: expected(:)
      integer, allocatable :: empty_integers(:)
      allocate (empty_integers(0))

      call check_array_i(error, uniform_values(20, 10, 10), empty_integers)
      call check_array_i(error, uniform_values(10, 20, 0), empty_integers)

      expected = (/10/)
      call check_array_i(error, uniform_values(10, 20, 1), expected)
      expected = (/10, 20/)
      call check_array_i(error, uniform_values(10, 20, 2), expected)
      expected = (/10, 15, 20/)
      call check_array_i(error, uniform_values(10, 20, 3), expected)
      expected = (/10, 13, 16, 20/)
      call check_array_i(error, uniform_values(10, 20, 4), expected)
      expected = (/10, 12, 15, 17, 20/)
      call check_array_i(error, uniform_values(10, 20, 5), expected)
   end subroutine
end module
