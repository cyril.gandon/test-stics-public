module dates_utils_tests
   use dates_utils, only: is_leap_year, days_count, day_number, get_day_month, modulo_years
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private
   integer, parameter, dimension(39) :: leap_years = (/ 1904, 1908, 1912, 1916, 1920, 1924, 1928, &
    1932, 1936, 1940, 1944, 1948, 1952, 1956, 1960, 1964, 1968, 1972, 1976, 1980, 1984, 1988, 1992, &
    1996, 2000, 2004, 2008, 2016, 2020, 2024, 2028, 2032, 2036, 2040, 2044, 2048, 2052, 2056, 2060 /)
    integer, parameter, dimension(5) :: not_leap_years = (/ 1900, 1901, 1902, 1903, 1905 /)
   public :: collect

contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_is_leap_year", test_is_leap_year), &
                  new_unittest("test_is_not_leap_year", test_is_not_leap_year), &
                  new_unittest("test_days_count_leap_year", test_days_count_leap_year), &
                  new_unittest("test_days_count_not_leap_year", test_days_count_not_leap_year), &
                  new_unittest("test_day_number", test_day_number), &
                  new_unittest("test_get_day_month", test_get_day_month), &
                  new_unittest("test_modulo_years", test_modulo_years) &
                  ]

   end subroutine collect

   subroutine test_is_leap_year(error)
      type(error_type), allocatable, intent(out) :: error

      integer :: year_index, year

      do year_index = 1, size(leap_years)
         year = leap_years(year_index)
         call check(error, is_leap_year(year), .true.)
      end do

   end subroutine test_is_leap_year

   subroutine test_is_not_leap_year(error)
      type(error_type), allocatable, intent(out) :: error

      integer :: year_index, year

      do year_index = 1, size(not_leap_years)
         year = not_leap_years(year_index)
         call check(error, is_leap_year(year), .false.)
      end do

   end subroutine test_is_not_leap_year

   subroutine test_days_count_leap_year(error)
      type(error_type), allocatable, intent(out) :: error

      integer :: year_index, year

      do year_index = 1, size(leap_years)
         year = leap_years(year_index)
         call check(error, days_count(year), 366)
      end do

   end subroutine test_days_count_leap_year

   subroutine test_days_count_not_leap_year(error)
      type(error_type), allocatable, intent(out) :: error

      integer :: year_index, year

      do year_index = 1, size(not_leap_years)
         year = not_leap_years(year_index)
         call check(error, days_count(year), 365)
      end do

   end subroutine test_days_count_not_leap_year

   subroutine test_day_number(error)
      type(error_type), allocatable, intent(out) :: error

      call check(error, day_number(1, 1, 2000), 1)
      call check(error, day_number(1, 1, 2001), 1)

      call check(error, day_number(1, 2, 2000), 32)
      call check(error, day_number(1, 2, 2001), 32)

      call check(error, day_number(29, 2, 2000), 60)
      call check(error, day_number(28, 2, 2001), 59)

      call check(error, day_number(1, 3, 2000), 61)
      call check(error, day_number(1, 3, 2001), 60)

      call check(error, day_number(31, 12, 2000), 366)
      call check(error, day_number(31, 12, 2001), 365)

      call check(error, day_number(10, 11, 2022), 314)

   end subroutine test_day_number


   subroutine test_get_day_month(error)
      type(error_type), allocatable, intent(out) :: error
      integer :: day, month
      character(len=3) :: month_code  

      call get_day_month(1, 2001, month_code, day, month)
      call check(error, day, 1)
      call check(error, month, 1)
      call check(error, month_code, "jan")

      call get_day_month(60, 2000, month_code, day, month)
      call check(error, month_code, "feb")
      call check(error, day, 29)
      call check(error, month, 2)

      call get_day_month(60, 2001, month_code, day, month)
      call check(error, month_code, "mar")
      call check(error, day, 1)
      call check(error, month, 3)

      call get_day_month(365, 2001, month_code, day, month)
      call check(error, month_code, "dec")
      call check(error, day, 31)
      call check(error, month, 12)

      call get_day_month(366, 2000, month_code, day, month)
      call check(error, month_code, "dec")
      call check(error, day, 31)
      call check(error, month, 12)

      ! if out of bound day number, should returns empty values
      call get_day_month(366, 2001, month_code, day, month)
      call check(error, month_code, "")
      call check(error, day, 0)
      call check(error, month, 0)

   end subroutine test_get_day_month

   subroutine test_modulo_years(error)
      type(error_type), allocatable, intent(out) :: error
      integer :: day, year

      call modulo_years(365, 2021, day, year)
      call check(error, day, 365)
      call check(error, year, 2021)

      call modulo_years(366, 2021, day, year)
      call check(error, day, 1)
      call check(error, year, 2022)

      call modulo_years(731, 2021, day, year)
      call check(error, day, 1)
      call check(error, year, 2023)

      call modulo_years(365, 2000, day, year)
      call check(error, day, 365)
      call check(error, year, 2000)

      call modulo_years(366, 2000, day, year)
      call check(error, day, 366)
      call check(error, year, 2000)

      call modulo_years(367, 2000, day, year)
      call check(error, day, 1)
      call check(error, year, 2001)

      call modulo_years(365, 2000, day)
      call check(error, day, 365)
      
      call modulo_years(367, 2000, day)
      call check(error, day, 1)
   
   end subroutine test_modulo_years
end module dates_utils_tests
