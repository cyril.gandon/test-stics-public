module climate_utils_tests
   use math_utils, only: PI, degrees_to_rad
   use astronomy_utils, only: MARCH_EQUINOX, JUNE_SOLSTICE, SEPTEMBER_EQUINOX, DECEMBER_SOLSTICE
   use climate_utils
   use climate_file_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   use test_utils, only: check_array
   implicit none

   real, parameter :: threshold = 1e-3

contains
   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_rgex_france", test_rgex_france) &
                  , new_unittest("test_rgex_south_pole", test_rgex_south_pole) &
                  , new_unittest("test_rgex_north_pole", test_rgex_north_pole) &
                  , new_unittest("test_rgex_equator", test_rgex_equator) &
                  , new_unittest("test_tvar", test_tvar) &
                  , new_unittest("test_temprosee", test_temprosee) &
                  , new_unittest("test_tvar_temprosee_inverse", test_tvar_temprosee_inverse) &
                  , new_unittest("test_get_hourly_values", test_get_hourly_values) &
                  , new_unittest("test_get_dsat", test_get_dsat) &
                  , new_unittest("test_get_means_by_year_one_year", test_get_means_by_year_one_year) &
                  , new_unittest("test_get_means_by_year_five_years", test_get_means_by_year_five_years) &
                  , new_unittest("test_get_mean_temp_for_years", test_get_mean_temp_for_years) &
                  ]

   end subroutine collect

   subroutine test_rgex_south_pole(error)
      type(error_type), allocatable, intent(out) :: error
      real :: latitude

      latitude = degrees_to_rad(-90.)
      call check(error, rgex(latitude, MARCH_EQUINOX), 0.)
      call check(error, rgex(latitude, JUNE_SOLSTICE), 0.)
      call check(error, rgex(latitude, SEPTEMBER_EQUINOX), 4.3353033)
      call check(error, rgex(latitude, DECEMBER_SOLSTICE), 48.5309677)

   end subroutine test_rgex_south_pole

   subroutine test_rgex_north_pole(error)
      type(error_type), allocatable, intent(out) :: error
      real :: latitude

      latitude = degrees_to_rad(90.)
      call check(error, rgex(latitude, MARCH_EQUINOX), 0.)
      call check(error, rgex(latitude, JUNE_SOLSTICE), 45.5241547)
      call check(error, rgex(latitude, SEPTEMBER_EQUINOX), 0.)
      call check(error, rgex(latitude, DECEMBER_SOLSTICE), 0.)

   end subroutine test_rgex_north_pole

   subroutine test_rgex_equator(error)
      type(error_type), allocatable, intent(out) :: error
      real :: latitude

      latitude = degrees_to_rad(0.)
      call check(error, rgex(latitude, MARCH_EQUINOX), 37.9169922)
      call check(error, rgex(latitude, JUNE_SOLSTICE), 33.4507790)
      call check(error, rgex(latitude, SEPTEMBER_EQUINOX), 37.4445763)
      call check(error, rgex(latitude, DECEMBER_SOLSTICE), 35.7040901)

   end subroutine test_rgex_equator

   subroutine test_rgex_france(error)
      type(error_type), allocatable, intent(out) :: error
      real :: latitude

      latitude = degrees_to_rad(45.)

      call check(error, rgex(latitude, MARCH_EQUINOX), 26.8113613)
      call check(error, rgex(latitude, JUNE_SOLSTICE), 42.0047188)
      call check(error, rgex(latitude, SEPTEMBER_EQUINOX), 24.9625359)
      call check(error, rgex(latitude, DECEMBER_SOLSTICE), 10.4905090)

   end subroutine test_rgex_france

   subroutine test_tvar(error)
      type(error_type), allocatable, intent(out) :: error

      call check(error, tvar(0.), 6.10699987)
      call check(error, tvar(20.), 23.3662033)
      call check(error, tvar(35.), 56.2386246)
      call check(error, tvar(50.), 123.312271)
      call check(error, tvar(75.), 381.956787)
      call check(error, tvar(100.), 977.426392)
   end subroutine test_tvar

   subroutine test_temprosee(error)
      type(error_type), allocatable, intent(out) :: error

      call check(error, temprosee(6.10699987), 0.)
      call check(error, temprosee(23.3662033), 20.)
      call check(error, temprosee(56.2386246), 34.9999924)
      call check(error, temprosee(123.312271), 50.0000038)
      call check(error, temprosee(381.956787), 75.)
      call check(error, temprosee(977.426392), 99.9999924)
   end subroutine test_temprosee

   subroutine test_tvar_temprosee_inverse(error)
      type(error_type), allocatable, intent(out) :: error
      integer:: temperature
      do temperature = -50, 200, 10
         call check(error, temprosee(tvar(real(temperature))), real(temperature), thr=1e-4)
      end do
   end subroutine test_tvar_temprosee_inverse

   subroutine test_get_hourly_values(error)
      type(error_type), allocatable, intent(out) :: error
      real, dimension(24):: expected

      expected = (/1.66666663, 3.33333325, 5., 6.66666651, 8.33333302, 10., 11.6666670, 13.3333330, 15., 16.6666660, &
                   18.3333340, 20., 18.3333340, 16.6666660, 15., 13.3333340, 11.6666670, 10., 8.33333302, 6.66666698, 5., &
                   3.33333397, 1.66666603, 0./)
      call check_array(error, get_hourly_values(0., 20., 0.), expected)

      expected = (/75., 50., 25., 0., -25., -50., -75., -100., -125., -150., -175., -200., &
                   -191.666672, -183.333328, -175., -166.666672, -158.333328, -150., -141.666672, -133.333344, &
                   -125., -116.666664, -108.333336, -100./)
      call check_array(error, get_hourly_values(100., -200., -100.), expected)
   end subroutine test_get_hourly_values

   subroutine test_get_dsat(error)
      type(error_type), allocatable, intent(out) :: error

      ! at 25 degrees, vapour pressure of water is around 31.69 mbars
      call check(error, get_dsat(25., 10.), 21.6656246)
      call check(error, get_dsat(25., 31.6), 0.656242371E-1)
      call check(error, get_dsat(25., 40.), 0.)
   end subroutine

   
   function get_test_path(filename) result(full_path)
      character(*), intent(in) :: filename
      character(:), allocatable :: full_path
      character(len=255) :: cwd

      call getcwd(cwd)
      full_path = join_path(trim(cwd),'src','utilities','climate','test_data',filename)
   end function

   subroutine test_get_means_by_year_one_year(error)
      type(error_type), allocatable, intent(out) :: error
      real, allocatable :: means(:, :)
      type(logger_) :: logger
      type(climate_file_row_), allocatable :: climate_rows(:)

      climate_rows = read_climate_file(logger, get_test_path('climat.txt'))
      means = get_means_by_year(climate_rows)
      call check(error, size(means, 1), 2)
      call check(error, size(means, 2), 1)
      call check(error, means(1, 1), 1996.)
      call check(error, means(2, 1), 25.8555, thr=threshold)

   end subroutine

   subroutine test_get_means_by_year_five_years(error)
      type(error_type), allocatable, intent(out) :: error
      real, allocatable :: means(:, :)
      type(logger_) :: logger
      type(climate_file_row_), allocatable :: climate_rows(:)


      climate_rows = read_climate_file(logger, get_test_path('climat_5_years.txt'))
      means = get_means_by_year(climate_rows)
      call check(error, size(means, 1), 2)
      call check(error, size(means, 2), 5)

      call check(error, means(1, 1), 1992.)
      call check(error, means(2, 1), 14.1935, thr=threshold)
      call check(error, means(1, 2), 1993.)
      call check(error, means(2, 2), 14.0856, thr=threshold)   
      call check(error, means(1, 3), 1994.)
      call check(error, means(2, 3), 15.3715, thr=threshold)   
      call check(error, means(1, 4), 1995.)
      call check(error, means(2, 4), 14.7856, thr=threshold)
      call check(error, means(1, 5), 1996.)
      call check(error, means(2, 5), 14.0878, thr=threshold)
   end subroutine

   subroutine test_get_mean_temp_for_years(error)
      type(error_type), allocatable, intent(out) :: error
      real, allocatable :: means(:, :)
      type(logger_) :: logger
      type(climate_file_row_), allocatable :: climate_rows(:)

      climate_rows = read_climate_file(logger, get_test_path('climat_5_years.txt'))
      means = get_means_by_year(climate_rows)

      call check(error, get_mean_temp_for_years(1993, 1993, means), 14.0856, thr=threshold)
      call check(error, get_mean_temp_for_years(1992, 1993, means), 14.1395, thr=threshold)
      call check(error, get_mean_temp_for_years(1991, 1997, means), 14.5048, thr=threshold)
      call check(error, get_mean_temp_for_years(1997, 1991, means), 0., thr=threshold)

   end subroutine
end module climate_utils_tests
