module climate_utils
   use math_utils, only: PI, degrees_to_rad, rad_to_degrees
   use astronomy_utils, only: MARCH_EQUINOX, EARTH_VELOCITY, OBLIQUITY, decangle
   use dates_utils
   use climate_file_m
   implicit none

   !> Average extraterrestrial irradiance in Watts/meter2 (W/m2). This value varies by ±3% as the earth orbits the sun.
   real, parameter :: irradiance = 1370.

   !> Evapotranspiration model used, codes for P_codeetp
   !> Evapotranspiration Forced Penman
   integer, parameter :: ETP_PE = 1
   !> Evapotranspiration Calculated Penman
   integer, parameter :: ETP_PC = 2
   !> Evapotranspiration Shuttleworth & Wallace
   integer, parameter :: ETP_SW = 3
   !> Evapotranspiration Priestley & Taylor
   integer, parameter :: ETP_PT = 4
contains

   !> Calculation of extraterrestrial radiation in MJ/m2/jour.
   !! C. Varlet-Grancher, Raymond Bonhomme, Herve Sinoquet. Crop structure and light microclimate characterization and applications. INRA, 518 p., 1993, 2-7380-0448-2. ⟨hal-02851716⟩
   pure real function rgex(latitude, day)
      real, intent(in) :: latitude  ! P_latitude in radians
      integer, intent(in) :: day    ! jour julien

      real :: z
      real :: x
      real :: y
      real :: solar
      real :: a
      real :: u

      z = decangle(day)

      ! x =  sinus de l'angle de  declinaison du soleil
      x = sin(z)
      y = sin(latitude)

      solar = irradiance*3600*24/PI
      a = -x*y/sqrt((1 - x**2)*(1 - y**2))
      if (a >= 1) then
         rgex = 0.
         return
      end if
      rgex = 0.*x
      if (a < 0.0) then
         rgex = PI
         if (a + 1.0 < 0.0) a = -1.0
         u = sqrt(1 - a**2)/a
         rgex = x*y*(rgex + atan(u) - u)
      end if

!      if (a == 0.0) then
!          if (x == 0.0) then
      if (abs(a) .lt. 1.0E-8) then
         if (abs(x) .lt. 1.0E-8) then
            rgex = sqrt(1 - y**2)
         else
            rgex = sqrt(1 - x**2)
         end if
      end if

      if (a > 0.0) then
         u = sqrt(1 - a**2)/a
         rgex = x*y*(rgex + atan(u) - u)
      end if

      ! 0.033 is a value obtained by observation
      rgex = solar*(1 + (0.033*cos(EARTH_VELOCITY*day)))*rgex*1e-6
   end function rgex

   !> Returns the vapor pressure (millibar) at the temperature (°C)
   !! https://en.wikipedia.org/wiki/Vapour_pressure_of_water
   !! We use a custom formula developed by Alt, Jean from INRA
   !! Source: "Formules empiriques pour la Pression de saturation de la Vapeur d'eau" Alt, Jean (1978)
   pure real function tvar(temperature)
      real, intent(in) :: temperature
      real :: rad_temp
      rad_temp = degrees_to_rad(temperature/3)
      tvar = 6.1070*(1 + SQRT(2.)*SIN(rad_temp))**8.827
   end function tvar

   !> Function for calculating the dew point temperature at the E pressure. This is the invert formula of tvar.
   !! Pressure is in millibar, temperature in °C.
   pure real function temprosee(pressure)
      real, intent(in) :: pressure
      real :: rad_pressure
      rad_pressure = asin(((pressure/6.1070)**(1.0/8.827) - 1.0)/sqrt(2.0))
      temprosee = rad_to_degrees(3*rad_pressure)
   end function temprosee

   !> Calculating the hourly values from a min and a max, smooth through the day
   pure function get_hourly_values(start, middle, end) result(result)
      real, intent(in):: start
      !! Starting value that happens at 00:00

      real, intent(in):: middle
      !! Middle value of the day, happens at 12:00

      real, intent(in):: end
      !! Minimum value of the next day that happens at 00:00 the day after

      real, dimension(24):: result
      !! Resulting array of values hour by hour

      integer :: hour

      do hour = 1, 12
         result(hour) = start + (hour*(middle - start)/12.0)
      end do

      do hour = 13, 24
         result(hour) = middle - ((hour - 12)*(middle - end)/12.0)
      end do
   end

   !> Get the Vapour-pressure deficit (VPD or dsat)
   !! given a temperature in celsius and Vapour pressure in air in mbars
   pure real function get_dsat(temperature, vapour_pressure) result(dsat)
      real, intent(in):: temperature, vapour_pressure

      dsat = max(0., tvar(temperature) - vapour_pressure)
   end

   pure function get_means_by_year(rows) result(mean_by_year)
      type(climate_file_row_), allocatable, intent(in) :: rows(:)
      real, allocatable :: mean_by_year(:, :)

      integer :: current_year, year_index, i, year_count
      real :: cumul, mean
      type(climate_file_row_) :: row

      if(size(rows) == 0) then
         allocate(mean_by_year(2, 0))
         return
      end if

      current_year = rows(1)%year
      year_count = 1
      do i = 1, size(rows)
         row = rows(i)
         if(row%year /= current_year) then
            year_count = year_count + 1
         end if
         current_year = row%year
      end do

      allocate(mean_by_year(2, year_count))

      current_year = rows(1)%year
      year_index = 1
      cumul = 0.
      do i = 1, size(rows)
         row = rows(i)
         if(row%year /= current_year) then
            mean_by_year(1, year_index) = current_year
            mean_by_year(2, year_index) = cumul/(2*days_count(current_year))
            year_index = year_index + 1
            cumul = 0.
         end if
         current_year = row%year
         cumul = cumul + row%ttmax + row%ttmin
      end do

      mean_by_year(1, year_index) = current_year
      mean_by_year(2, year_index) = cumul/(2*days_count(current_year))
   end

   !> Get mean temperature between start year and end year based on yearly means
   pure real function get_mean_temp_for_years(start_year,end_year,means_by_year) result(t)
      integer, intent(in) :: start_year  
      integer, intent(in) :: end_year  
      real, intent(in) :: means_by_year(:, :)

      integer :: i, count
      real :: summ

      summ = 0.
      count = 0
      do i = 1, size(means_by_year, 2)
         if (means_by_year(1,i) >= start_year .and. means_by_year(1,i) <= end_year) then
            summ = summ + means_by_year(2,i)
            count = count + 1
         endif
      enddo

      if(count == 0) then
         t = 0.
      else
         t = summ / count
      end if
   end function

   !> Return the model used for evapotranspiration calculation as text
   pure function etp_code_to_string(code) result(text)
      integer, intent(in) :: code
      character(:), allocatable :: text
      
      select case (code)
      case (ETP_PE)
         text = ' Penman reading'
      case (ETP_PC)
         text = ' Penman calculated'
      case (ETP_SW)
         text = ' Shuttleworth and Wallace'
      case (ETP_PT)
         text = ' Priestley-Taylor calculated'
      case default
         text = ' Unknown etp model for code ['//to_string(code)//']'
      end select
   end function
end module
