module astronomy_utils
   use iso_fortran_env, only: real32
   use math_utils, only: PI
   implicit none
   private


   public :: MARCH_EQUINOX, JUNE_SOLSTICE, SEPTEMBER_EQUINOX, DECEMBER_SOLSTICE, EARTH_VELOCITY, OBLIQUITY
   public :: decangle

   !> March equinox in Julian days. Can occur as early as 19 March or as late as 21 March. https://en.wikipedia.org/wiki/March_equinox
   integer, parameter :: MARCH_EQUINOX = 80

   !> June solstice in Julian days. Occurs annually between 20 and 22 June. https://en.wikipedia.org/wiki/June_solstice
   integer, parameter :: JUNE_SOLSTICE = 172

   !> September equinox in Julian days. May occur anytime from September 21 to 24. https://en.wikipedia.org/wiki/September_equinox
   integer, parameter :: SEPTEMBER_EQUINOX = 264

   !> December solstice in Julian days. typically on 21 December. https://en.wikipedia.org/wiki/December_solstice
   integer, parameter :: DECEMBER_SOLSTICE = 355

   !> Angular Velocity of Earth. 2 * Pi / 365 = 0.0172 rad/day
   real(real32), parameter :: EARTH_VELOCITY = 2*PI/365

   !> Obliquity of the ecliptic, approximately 23.44°, or 0.409143 rads. https://en.wikipedia.org/wiki/Axial_tilt#Earth
   real(real32), parameter :: OBLIQUITY = 0.409143_real32

   !> Sinus of the obliquity = 0.3978
   real(real32), parameter :: sinEpsilon = sin(OBLIQUITY);
contains

   !> Calculates the angle of the declination in radian of the sun at noon of the julian day j.
   !! Precision 1' for the declination.
   !! C. Varlet-Grancher, Raymond Bonhomme, Herve Sinoquet. Crop structure and light microclimate characterization and applications. INRA, 518 p., 1993, 2-7380-0448-2. ⟨hal-02851716⟩
   pure real(real32) function decangle(j)
      !> Julian day
      integer, intent(IN) :: j
      real(real32) :: theta1, theta2, theta

      theta1 = EARTH_VELOCITY*(j - MARCH_EQUINOX)
      theta2 = 0.034_real32*(sin(EARTH_VELOCITY*j) - sin(EARTH_VELOCITY*MARCH_EQUINOX))
      ! Celestial longitude of the sun
      theta = theta1 - theta2
      decangle = asin(sinEpsilon*sin(theta))
   end function decangle

end module astronomy_utils
