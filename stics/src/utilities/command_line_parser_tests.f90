module command_line_parser_tests
   use command_line_parser
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private
   public :: collect

contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_parse_command_line", test_parse_command_line), &
                  new_unittest("test_parse_command_line_arg", test_parse_command_line_arg) &
                  ]

   end subroutine collect

   subroutine test_parse_command_line(error)
      type(error_type), allocatable, intent(out) :: error
      type(command_line_args_) :: args
      args = parse_command_line("")
      call check(error, args%output_path_set, .false.)
      call check(error, args%output_path, "")

      args = parse_command_line("--output-path=./some_dirs")
      call check(error, args%version_set, .false.)
      call check(error, args%output_path_set, .true.)
      call check(error, args%output_path, "./some_dirs")

      args = parse_command_line("version")
      call check(error, args%version_set, .true.)
      args = parse_command_line("-v")
      call check(error, args%version_set, .true.)
      args = parse_command_line("--version=whatever")
      call check(error, args%version_set, .true.)
   end subroutine

   subroutine test_parse_command_line_arg(error)
      type(error_type), allocatable, intent(out) :: error
      type(command_line_arg_) :: arg
      arg = parse_command_line_arg("")
      call check(error, arg%key, "")
      call check(error, arg%value, "")

      arg = parse_command_line_arg("toto=123")
      call check(error, arg%key, "toto")
      call check(error, arg%value, "123")

      arg = parse_command_line_arg("=123")
      call check(error, arg%key, "")
      call check(error, arg%value, "123")

      arg = parse_command_line_arg("toto")
      call check(error, arg%key, "toto")
      call check(error, arg%value, "")

      arg = parse_command_line_arg("toto=123=456")
      call check(error, arg%key, "toto")
      call check(error, arg%value, "123=456")

      arg = parse_command_line_arg("toto=123 456")
      call check(error, arg%key, "toto")
      call check(error, arg%value, "123 456")

      arg = parse_command_line_arg("          toto=123 456                 ")
      call check(error, arg%key, "toto")
      call check(error, arg%value, "123 456")
   end subroutine
end module

