module phenology_utils
   use stdlib_ascii, only: to_lower

   implicit none

   !! Bare soil (Sol nu)
   character(*), parameter :: snu = 'snu'

   !! Sowing or Planting (Semis)
   character(*), parameter :: plt = 'plt'

   !! Germination
   character(*), parameter :: ger = 'ger'

   !! Emergence (Levée)
   character(*), parameter :: lev = 'lev'

    !! End of juvenile phase (Accélération maximale de croissance foliaire)
   character(*), parameter :: amf = 'amf'

   !! Leaf area index is nil
   character(*), parameter :: lan = 'lan'

   !! Maximum leaf area index
   character(*), parameter :: lax = 'lax'

   !! Beginning of fruit filling (Date de remplissage des puits)
   character(*), parameter :: drp = 'drp'

   !! Floraison
   character(*), parameter :: flo = 'flo'

   !! Beginning of net senescence
   character(*), parameter :: sen = 'sen'

   !! Harvest (récolte)
   character(*), parameter :: rec = 'rec'

   !! First cut (Fauches)
   character(*), parameter :: cut = 'cut'

   !! Last cut
   character(*), parameter :: lcu = 'lcu'

   !! Physiological maturity (Maturation)
   character(*), parameter :: mat = 'mat'

   character(*), parameter :: dor = 'dor'

   !! Beginning of dormance (Début de la période de dormance)
   character(*), parameter :: debdorm = 'debdorm'
   character(*), parameter :: ddo = 'ddo'

   !! End of dormance (Fin de la période de dormance)
   character(*), parameter :: findorm = 'findorm'
   character(*), parameter :: fdo = 'fdo'

   character(*), parameter ::fin = 'fin'
   character(*), parameter ::debdes = 'debdes'
   character(*), parameter ::debdebour = 'debdebour'
   character(*), parameter ::start = 'start'

   ! des avant mat
   character(*), parameter :: des = 'des'

   !!  des apres mat ???
   character(*), parameter :: fde = 'fde'

   !> List of valid stage understand by the program
   character(len=3),parameter,dimension(20) :: valid_stages = [&
   snu,plt,ger,lev,amf,lan,lax,drp,flo,sen,rec,cut,lcu,mat,dor,ddo,fdo,fin,des,fde&
   ]

contains

   !> Returns true if the current date `n` and current stage match the given stage
   pure logical function is_n_this_stage( &
      n, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm, stage &
      ) result(res)
      integer, intent(in) :: nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm
      integer, intent(in) :: n
      character(*), intent(in) :: stage

      character(:), allocatable :: current_stage

      res = .false.

      current_stage = get_stage_for_date(n, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm)
      if (len(current_stage) .gt. 0) then
         res = to_lower(stage) .eq. current_stage
      end if
   end function

!> Returns the stage at which this date corresponds. If this date does not correspond to any date, return empty string.
   pure function get_stage_for_date( &
      n, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm &
      ) result(stage)
      integer, intent(in) :: n, nplt, nger, nlev, namf, nlax, ndrp, nflo, nsen, nrec, nmat, ndebdorm, nfindorm
      character(:), allocatable :: stage

      if (n .eq. nplt) then
         stage = plt
      else if (n .eq. nger) then
         stage = ger
      else if (n .eq. nlev) then
         stage = lev
      else if (n .eq. namf) then
         stage = amf
      else if (n .eq. nlax) then
         stage = lax
      else if (n .eq. ndrp) then
         stage = drp
      else if (n .eq. nflo) then
         stage = flo
      else if (n .eq. nsen) then
         stage = sen
      else if (n .eq. nrec) then
         stage = rec
      else if (n .eq. nmat) then
         stage = mat
      else if (n .eq. ndebdorm) then
         stage = debdorm
      else if (n .eq. nfindorm) then
         stage = findorm
      else
         stage = ''
      end if
   end function
   
   !> Return true is the stage is valid, false otherwise
   pure logical function is_valid_stage(stage) result(valid)
      character(len=3), intent(in) :: stage
      integer :: i
      character(len=3) :: lc_stage

      valid = .false.
      lc_stage = to_lower(stage)
      do i=1, size(valid_stages)
         if(valid_stages(i) == lc_stage) then
            valid = .true.
            return
         end if
      end do
   end function
end module
