!======================================================================================!
!======================================================================================!
!======================================================================================!
module Divers_develop
use soil_utils
implicit none
private
public :: calcul_UDev, calcul_GDH, calcul_GDH2, cRFPI, humpotsol
contains
  real function calcul_UDev(temp,P_tdmax,P_tdmin,P_tcxstop)

  real, intent(IN) :: temp  
  real, intent(IN) :: P_tdmax  ! // PARAMETER // Maximum threshold temperature for development // degree C // PARPLT // 1
  real, intent(IN) :: P_tdmin  ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
  real, intent(IN) :: P_tcxstop  ! // PARAMETER // threshold temperature beyond which the foliar growth stops // degree C // PARPLT // 1

  real :: udev ! pour alleger l'ecriture, utilisation variable temporaire udev  

    udev = max(0.,temp - P_tdmin)
    if (P_tcxstop >= 100.0) then
      if (temp > P_tdmax) udev = P_tdmax - P_tdmin
    else
      if (temp > P_tdmax) then
        udev = (P_tdmax - P_tdmin) / (-P_tcxstop + P_tdmax) * (temp - P_tcxstop)
        udev = max(0.,udev)
      endif
    endif

    calcul_UDev = udev ! on affecte la valeur de retour

return
end

!======================================================================================!
!======================================================================================!
!======================================================================================!
! calculating the effect of temperature on development units for emergence or development post emergence
real function calcul_GDH(thor,P_tdmin,P_tdmax)

  real, intent(IN), dimension(24) :: thor    ! Hourly temperature (over 24 hours)
  real, intent(IN)                :: P_tdmin ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
  real, intent(IN)                :: P_tdmax ! // PARAMETER // Maximum threshold temperature for development // degree C // PARPLT // 1

  integer :: ih       ! local
  real :: udh         ! local

    calcul_GDH = 0.
    do ih = 1,24
      udh = thor(ih) - P_tdmin
      if (thor(ih) < P_tdmin) udh = 0.0
      if (thor(ih) > P_tdmax) udh = P_tdmax - P_tdmin
      calcul_GDH = calcul_GDH + udh
    end do

return
end

!======================================================================================!
! Loic et Bruno mai 2014
! Use of Wang et Engel (1998) function for calculating the effect of temperature on development units for emergence
real function calcul_GDH2(tmoy,P_tdmindeb,P_tdmaxdeb,P_tdoptdeb)

  real, intent(IN) :: tmoy    ! mean daily temperature
  real, intent(IN) :: P_tdmindeb ! // PARAMETER // Minimum threshold temperature for development // degree C // PARPLT // 1
  real, intent(IN) :: P_tdmaxdeb ! // PARAMETER // Maximum threshold temperature for development // degree C // PARPLT // 1
  real, intent(IN) :: P_tdoptdeb ! // PARAMETER // Optimum temperature for development // degree C // PARPLT // 1

! variables locales
  real :: alpha
  real :: dTopt
  real :: dTmoy
  real :: dTmax

  dTopt = amax1(0., P_tdoptdeb - P_tdmindeb)
  dTmoy = amax1(0., tmoy - P_tdmindeb)
  dTmax = P_tdmaxdeb - P_tdmindeb

  alpha = log(2.)/log(dTmax/dTopt)
  calcul_GDH2 = (2.*(dTmoy**alpha)*(dTopt**alpha)-dTmoy**(2.*alpha))/(dTopt**(2.*alpha))

return
end

!======================================================================================!
! slowly effect of the photoperiod on plant development
real function cRFPI(P_sensiphot,P_phosat,P_phobase,phoi)

  real, intent(IN) :: P_sensiphot  ! // PARAMETER //  photoperiod sensitivity (1=insensitive) // SD // PARPLT // 1 
  real, intent(IN) :: P_phosat  ! // PARAMETER // saturating photoperiod // hours // PARPLT // 1 
  real, intent(IN) :: P_phobase  ! // PARAMETER // Base photoperiod  // hours // PARPLT // 1 
  real, intent(IN) :: phoi   ! // OUTPUT // Photoperiod // hours

    cRFPI = (1.0 - P_sensiphot) / (P_phosat - P_phobase) * (phoi - P_phosat) + 1.0
    cRFPI = min(cRFPI,1.0)
    cRFPI = max(cRFPI,P_sensiphot)

return
end function cRFPI

!======================================================================================!
! fonction de calcul de l'humidite correspondant a un potentiel du sol
! utile pour la germination
! NB 15/02 2007
real function humpotsol(P_psihucc,P_psihumin,humin,hucc,dacouche,psiref,P_codefente)

!: ARGUMENTS
  real,    intent(IN) :: P_psihucc  ! // PARAMETER // soil potential corresponding to field capacity  // Mpa // PARAM // 1 
  real,    intent(IN) :: P_psihumin  ! // PARAMETER // soil potential corresponding to wilting point // Mpa // PARAM // 1 
  real,    intent(IN) :: hucc  
  real,    intent(IN) :: humin  
  real,    intent(IN) :: dacouche  
  real,    intent(IN) :: psiref  
  integer, intent(IN) :: P_codefente  ! // PARAMETER // option allowing an additional water compartment for the swelling soils: yes (1), no (0) // code 0/1 // PARSOL // 0 

!: Variables locales
  real :: bpsisol  
  real :: psisols  
  real :: wsat  

  ! Calcul des parametres de la courbe de retention
    wsat = get_wsat(P_codefente, dacouche, hucc, humin)
    bpsisol = log(P_psihucc / P_psihumin) / log(humin / hucc)
    psisols = P_psihumin * ((humin / (wsat *10 ))**bpsisol)

  ! Calcul de l'humidite
    humpotsol = wsat * 10. * ((psiref / psisols)**(-1/bpsisol))

return
end function humpotsol
end module Divers_develop
