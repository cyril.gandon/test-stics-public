module string_utils
   use iso_fortran_env
   use stdlib_strings, only: to_string
   use stdlib_string_type
   implicit none

   character, parameter, dimension(9) :: char_digits = (/'1', '2', '3', '4', '5', '6', '7', '8', '9'/)

   !> to_string for arrays
   interface to_string_a
      module procedure to_string_ai, to_string_ar
   end interface

contains
   pure integer function bbch_string_to_int(text) result(result)
      !> Return the first two significant digits as an integer
      !! Use only for bbch code, that ranges from "-99" to "99"
      !! See https://en.wikipedia.org/wiki/BBCH-scale
      character(*), intent(in) :: text
      integer digit1, digit2, digit3

      if (len(text) .ge. 2) then
         digit2 = char_to_digit(text(2:2))
      else
         digit2 = 0
      end if

      if (text(1:1) .eq. '-') then
         ! Third digit should only exist for negative numbers
         if (len(text) .ge. 3) then
            digit3 = char_to_digit(text(3:3))
         else
            digit3 = 0
         end if
         result = -(digit2*10 + digit3)
      else
         ! 1st char is only a digit for positive numbers
         if (len(text) .ge. 1) then
            digit1 = char_to_digit(text(1:1))
         else
            digit1 = 0
         end if
         result = digit1*10 + digit2
      end if
   end function

   !> Return the integer value for a single caracter.
   !> Return 0 if this is not a string representation of a digit.
   integer elemental function char_to_digit(c) result(digit)
      character(1), intent(in) :: c
      integer :: i
      digit = 0
      do i = 1, 9
         if (c .eq. char_digits(i)) then
            digit = i
            return
         end if
      end do
   end function

   pure function to_string_ai(values) result(text)
      integer(kind=int32), dimension(:), intent(in) :: values
      character(:), allocatable :: text
      integer ivalue
      if (size(values) == 0) then
         text = ''
         return
      end if
      text = to_string(values(1))
      if (size(values) == 1) then
         return
      end if
      do ivalue = 2, size(values)
         text = text//", "//to_string(values(ivalue))
      end do
   end function

   pure function to_string_ar(values) result(text)
      real(kind=real32), dimension(:), intent(in) :: values
      character(:), allocatable :: text
      integer ivalue
      if (size(values) == 0) then
         text = ''
         return
      end if
      text = to_string(values(1))
      if (size(values) == 1) then
         return
      end if
      do ivalue = 2, size(values)
         text = text//", "//to_string(values(ivalue))
      end do
   end function

   !> Count letter in string
   integer pure function count_chars(text, letter)
      character(*), intent(in) :: text
      character(1), intent(in) :: letter
      count_chars = count(transfer(text, (/"x"/)) == letter)
   end function

   !> Count words in a sentence blank space separated
   integer function count_words(sentence) result(count)
      character(*), intent(in) :: sentence
      character(len=1) :: letter
      integer :: letter_index
      logical :: last_letter_separator

      count = 0
      last_letter_separator = .true.

      do letter_index = 1, len(sentence)
         letter = sentence(letter_index:letter_index)
         select case (letter)
         case (' ')
            ! separator
            last_letter_separator = .true.
         case default
            if (last_letter_separator) then
               count = count + 1
            end if
            last_letter_separator = .false.
         end select
      end do
   end function

   !> Returns a list of words from a sentence (blank space separated)
   function split_sentence(sentence) result(words)
      character(*), intent(in) :: sentence

      character(len=256), allocatable, dimension(:) :: buffer
      type(string_type), allocatable :: words(:)
      integer :: words_count
      integer :: letter_index, start_word, word_index
      character(len=1) :: letter
      logical :: last_letter_separator

      words_count = count_words(sentence)
      allocate (words(words_count))

      if (words_count .eq. 0) then
         return
      end if

      last_letter_separator = .true.
      start_word = 1
      word_index = 0
      do letter_index = 1, len(sentence)
         letter = sentence(letter_index:letter_index)
         select case (letter)
         case (' ')
            ! separator
            if (last_letter_separator .eqv. .false.) then
               words(word_index) = sentence(start_word:letter_index - 1)
            end if
            last_letter_separator = .true.
         case default
            if (last_letter_separator) then
               start_word = letter_index
               word_index = word_index + 1
            end if
            last_letter_separator = .false.
         end select
      end do

      ! Add the last word to the sentence
      if (last_letter_separator .eqv. .false.) then
         words(size(words)) = sentence(start_word:len(sentence))
      end if
   end function

   pure function concat_array_string(arr) result(out_str)
      type(string_type), intent(in), allocatable :: arr(:)

      character(:), allocatable :: out_str
      integer :: from, to, i, total_len

      total_len = 0
      do i = 1, size(arr)
         total_len = total_len + len(arr(i))
      end do

      allocate (character(total_len) :: out_str)

      from = 1
      do i = 1, size(arr)
         to = from + len(arr(i)) - 1
         out_str(from:to) = char(arr(i))
         from = to + 1
      end do
   end function
end module
