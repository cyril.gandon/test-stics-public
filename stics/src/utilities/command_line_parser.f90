module command_line_parser
   use fortran202x_split

   implicit none
   type command_line_args_
      logical :: version_set
      logical :: help_set
      logical :: output_path_set
      character(len=:), allocatable :: output_path
   end type

   type command_line_arg_
      character(len=:), allocatable :: key
      character(len=:), allocatable :: value
   end type
contains

   !> Parse a string representing the full command line pass to stics executable
   pure type(command_line_args_) function parse_command_line(command_line) result(args)
      character(len=*), intent(in) :: command_line
      character(:), allocatable :: tokens(:)

      integer :: token_index, test
      character(:), allocatable :: token

      args = command_line_args_ctor()

      call split(command_line, ' ', tokens)

      do token_index = 1, size(tokens)
         token = tokens(token_index)
         block
            type(command_line_arg_) :: arg
            arg = parse_command_line_arg(token)
            select case (arg%key)
            case ('--output-path')
               args%output_path_set = .true.
               args%output_path = arg%value
               test = len(arg%value)
            case ('version') ! legacy argument to show version, prefer --version instead
               args%version_set = .true.
            case ('-v')
               args%version_set = .true.
            case ('--version')
               args%version_set = .true.
            case ('-h')
               args%help_set = .true.
            case ('--help')
               args%help_set = .true.
            case default
               ! Unknown arguments in the command line
               ! TODO some error handling
            end select
         end block
      end do
   end function

   !> Parse a string in the form "key=value" into a dedicated derived type
   pure type(command_line_arg_) function parse_command_line_arg(token) result(arg)
      character(len=*), intent(in) :: token

      integer index_equal

      index_equal = index(token, "=")
      if (index_equal .eq. 0) then
         arg%key = adjustl(trim(token))
         arg%value = ""
      else
         arg%key = adjustl(trim(token(1:index_equal - 1)))
         arg%value = adjustl(trim(token(index_equal + 1:len(token))))
      end if
   end function

   pure type(command_line_args_) function command_line_args_ctor() result(args)
      args%version_set = .false.
      args%output_path_set = .false.
      args%output_path = ""
      args%help_set = .false.
   end function
end module
