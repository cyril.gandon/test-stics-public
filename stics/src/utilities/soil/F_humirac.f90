! *----------------------------------------------------------------* c
! *   introduction d'une fonction continue entre pf4.2 et CC pour  * c
! *   contraindre la germination et la levee                       * c
! *----------------------------------------------------------------* c
! Introduction of a continuous function between pf4.2 and CC to Constrain germination and emergence
module F_humirac_m
implicit none
private
public :: F_humirac
contains
real function F_humirac(h, hmin, hmax, P_sensrsec, P_humirac)

  real, intent(IN) :: h  
  real, intent(IN) :: hmin  
  real, intent(IN) :: hmax        ! // OUTPUT // Maximum height of water table between drains // cm
  real, intent(IN) :: P_sensrsec  ! // PARAMETER // root sensitivity to drought (1=insensitive) // SD // PARPLT // 1
  integer, intent(IN) :: P_humirac
  
  real :: x  

      ! if (abs(hmax-hmin).gt.1.0E-8) then
      if (P_humirac == 2) then
        !: 10/06/03 - modif de fonction humirac avant pfp pour 
        !-            aboutir a une absorption nulle pour h = 0
        if (h > hmin) then
          x = (h - hmin) / (hmax - hmin)
          F_humirac = P_sensrsec + (1. - P_sensrsec) * x
        else
          F_humirac = P_sensrsec / hmin * h
        endif
      else
        !: NB - 10/06/03
        ! --if (h < hmin) humirac = P_sensrsec
        if (h >= hmin) then
          F_humirac = 1.0
        else
          F_humirac = P_sensrsec / hmin * h
        endif
      endif

      if (F_humirac > 1.) F_humirac = 1.
      if (F_humirac < 0.) F_humirac = 0.
      if (P_humirac == 0) F_humirac = 1.  ! on desactive l'effet de l'eau si P_humirac = 0

return
end function F_humirac
end module F_humirac_m
