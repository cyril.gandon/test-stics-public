module plant_utils
    implicit none
    !> Plant code for baresoil (sol nu)
    character(*), parameter :: CODE_BARESOIL = 'snu'
     !> Plant code for beet
    character(*), parameter :: CODE_BEET = 'bet'
    !> Plant code for fodder (fourrages)
    character(*), parameter :: CODE_FODDER = 'fou'
    !> Plant code for maize
    character(*), parameter :: CODE_MAIZE = 'mai'
    !> Plant code for apple tree
    character(*), parameter :: CODE_APPLE = 'pom'
    !> Plant code for quinoa
    character(*), parameter :: CODE_QUINOA = 'qui'
    !> Plant code for vine
    character(*), parameter :: CODE_VINE = 'vig'

    !> Plant code for men
    character(*), parameter :: CODE_MEN = 'men'
    !> Plant code for stn
    character(*), parameter :: CODE_STN = 'stn'
    contains
 
 
 end module
 