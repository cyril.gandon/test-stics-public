module vernalization_utils
   implicit none

contains

   !> Return rfvi, the percentage of vernalization available
   real pure elemental function compute_rfvi(current_jvc, min_jvc, max_jvc) result(rfvi)
      real, intent(in) :: current_jvc, min_jvc, max_jvc

      if (max_jvc < min_jvc) then
        !! If it happens, consider a value of zero for the percentage
         rfvi = 0.
      else
         if (current_jvc < min_jvc) then
            rfvi = 0.
         else if (min_jvc <= current_jvc .and. current_jvc < max_jvc) then
            ! Compute percentage of vernalizaztion
            rfvi = (current_jvc - min_jvc)/(max_jvc - min_jvc)
         else !! max_jvc <= current_jvc
            rfvi = 1.
         end if
      end if
   end function
end module
