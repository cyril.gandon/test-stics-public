module soil_utils
   implicit none

   !> Constant solid density of soil in gram per cubic centimeter (10.1.1)
   real, parameter :: rho_s = 2.66
contains

   !> Get the water saturation
   !! (porosite totale ou teneur en eau a saturation ou humidite volumique a saturation)
   !! Eq 9.11 in Stics Red Book (9.4.3 Predawn plant water potential)
   real pure elemental function get_wsat(P_codefente, DAFs, hccfs, hminfs) result(wsat)
      !> Allowing an additional water compartment for the swelling soils
      integer, intent(in) :: P_codefente
      !> Bulk density in the horizon 1 // g cm-3
      real, intent(in) :: DAFs
      !> Gravimetric water content at field capacity (/fine earth) // % w
      real, intent(in) :: hccfs
      !> Gravimetric water content at wilting point (/fine earth) // % w
      real, intent(in) :: hminfs

      if (P_codefente == 1) then
         wsat = (1.5*hccfs - 0.5*hminfs)/10.
      else
         wsat = 1.-DAFs/rho_s
      end if
   end function

   real pure function get_macroporosite(P_codefente, DAFs, hccfs, hminfs, P_epc) result(macroporosite)
      !> Allowing an additional water compartment for the swelling soils
      integer, intent(in) :: P_codefente
      !> Bulk density in the horizon 1 // g cm-3
      real, intent(in) :: DAFs
      !> Gravimetric water content at field capacity (/fine earth) // % w
      real, intent(in) :: hccfs
      !> Gravimetric water content at wilting point (/fine earth) // % w
      real, intent(in) :: hminfs
      !> Thickness of the horizon H   (table) // cm 
      integer, intent(in) :: P_epc

      if (P_codefente == 1) then
         macroporosite = 0.5*(hccfs - hminfs)/100.*DAFs*10.*P_epc
      else
         ! la macroporosite des sols gonflants est proportionnelle a la RU
         macroporosite = ((1.0 - DAFs/rho_s) - (hccfs/100.*DAFs))*10.*P_epc
      end if

   end function
end module
