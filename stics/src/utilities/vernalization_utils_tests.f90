module vernalization_utils_tests
    use vernalization_utils
    use testdrive, only: error_type, unittest_type, new_unittest, check
    use test_utils
    implicit none
 
 contains
 
    subroutine collect(testsuite)
       type(unittest_type), allocatable, intent(out) :: testsuite(:)
 
       testsuite = [ &
                   new_unittest("test_compute_rfvi", test_compute_rfvi) &
                   ]
 
    end subroutine collect
 
    subroutine test_compute_rfvi(error)
       type(error_type), allocatable, intent(out) :: error

       !! max < min
       call check(error, compute_rfvi(5., 10., 5.), 0.)
       call check(error, compute_rfvi(10., 10., 5.), 0.)
       call check(error, compute_rfvi(15., 10., 5.), 0.)

       !! different milestone
       call check(error, compute_rfvi(5., 10., 20.), 0.)
       call check(error, compute_rfvi(10., 10., 20.), 0.)
       call check(error, compute_rfvi(12., 10., 20.), 0.200000003)
       call check(error, compute_rfvi(15., 10., 20.), 0.5)
       call check(error, compute_rfvi(17., 10., 20.), 0.699999988)
       call check(error, compute_rfvi(20., 10., 20.), 1.)
       call check(error, compute_rfvi(25., 10., 20.), 1.)

       !! same milestone
       call check(error, compute_rfvi(5., 10., 10.), 0.)
       call check(error, compute_rfvi(10., 10., 10.), 1.)
       call check(error, compute_rfvi(15., 10., 10.), 1.)
    end subroutine
 
 end module
 