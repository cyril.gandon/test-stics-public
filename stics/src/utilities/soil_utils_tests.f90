module soil_utils_tests
   use soil_utils
   use testdrive, only: error_type, unittest_type, new_unittest, check
   use test_utils
   implicit none

contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_wsat", test_get_wsat) &
                  , new_unittest("test_get_wsat_elemental", test_get_wsat_elemental) &
                  , new_unittest("test_get_macroporosite", test_get_macroporosite) &
                  ]

   end subroutine collect

   subroutine test_get_wsat(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, get_wsat(0, 1.2, 21.70, 12.00), 0.548872173)
      call check(error, get_wsat(1, 1.2, 21.70, 12.00), 2.65500021)
   end subroutine

   subroutine test_get_wsat_elemental(error)
      type(error_type), allocatable, intent(out) :: error
      real, allocatable :: dafs(:), hccfs(:), hminfs(:), wsat(:), expected_wsat(:)

      dafs = (/ 1., 1.1, 1.2, 1.3, 1.4 /)
      hccfs = (/ 21.70, 25., 30., 35., 40. /)
      hminfs = (/ 1., 1.1, 1.2, 1.3, 1.4 /)
      wsat = get_wsat(0, dafs, hccfs, hminfs)
      expected_wsat = (/ 0.624060154, 0.586466193, 0.548872173, 0.511278272, 0.473684251 /)
      call check_array(error, wsat, expected_wsat)

      wsat = get_wsat(1, dafs, hccfs, hminfs)
      expected_wsat = (/ 3.20500040, 3.69500017, 4.44000006, 5.18499994, 5.92999983 /)
      call check_array(error, wsat, expected_wsat)

   end subroutine

   subroutine test_get_macroporosite(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, get_macroporosite(0, 1.2, 21.70, 12.00, 10), 28.8472157)
      call check(error, get_macroporosite(1, 1.2, 21.70, 12.00, 10), 5.82000065)

   end subroutine
end module
