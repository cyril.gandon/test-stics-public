module astronomy_utils_tests
   use astronomy_utils, only: decangle, MARCH_EQUINOX, JUNE_SOLSTICE, SEPTEMBER_EQUINOX, DECEMBER_SOLSTICE
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private

   public :: collect

contains

   !> Collect all exported unit tests
   subroutine collect(testsuite)
      !> Collection of tests
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_decangle", test_decangle) &
                  ]

   end subroutine collect

   subroutine test_decangle(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, decangle(MARCH_EQUINOX), 0.)
      call check(error, decangle(JUNE_SOLSTICE), 0.408793360) ! maximum is 23°, or 0.4 rad
      call check(error, decangle(SEPTEMBER_EQUINOX), -0.0368369929)
      call check(error, decangle(DECEMBER_SOLSTICE), -0.408344418) ! minimum is -23°, or -0.4 rad

   end subroutine test_decangle

end module astronomy_utils_tests
