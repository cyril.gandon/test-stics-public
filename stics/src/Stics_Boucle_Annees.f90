! annual loop
! - initializations
! - Call of the loop DAILY
! - write the output files
module Stics_Boucle_Annees_m
use stdlib_strings, only: to_string
use string_utils, only: to_string_a
use phenology_utils
USE Stics
USE Plante
USE Itineraire_Technique
USE Sol
USE Climat
USE climate_file_m
USE Station
USE Parametres_Generaux
USE Bilans
USE Root
USE Module_AgMIP
use messages
use dates_utils, only: get_day_month, modulo_years
use initialisations_sol_m, only: initialisations_sol
use initialisations_m, only: initialisations
use ecriture_start_m, only: ecriture_start
use Ecriture_Rapport_m, only: Ecriture_Rapport
use Stics_Jour_m, only: Stics_Jour
use Ecriture_DonneesFinDeCycle_m, only: Ecriture_DonneesFinDeCycle
use daily_output_m
use soil_profile_m
use get_soil_values_m

implicit none
private
public :: Stics_Boucle_Annees
contains
subroutine Stics_Boucle_Annees(logger,sc,p,pg,r,itk,c,sta,soil,t,ag)
    type(logger_), intent(in) :: logger
! DR 17/10/2013 on ajoute le module patho pour couplage avce Mila
!USE mod_patho on enleve les appel a Mila geres sur la branche Stics_Mila
    type(Stics_Communs_),        intent(INOUT) :: sc
    type(Parametres_Generaux_),  intent(INOUT) :: pg
    type(Plante_),               intent(INOUT) :: p(sc%P_nbplantes)
    type(Root_),                 intent(INOUT) :: r(sc%P_nbplantes)
    type(ITK_),                  intent(INOUT) :: itk(sc%P_nbplantes)
    type(Sol_),                  intent(INOUT) :: soil
    type(Climat_),               intent(INOUT) :: c
    type(Station_),              intent(INOUT) :: sta
    type(Stics_Transit_),        intent(INOUT) :: t
    type(AgMIP_),                intent(INOUT) :: ag

    integer :: numcult, ipl, n, jour_iwater, nummois_iwater
    integer :: jour_ifwater, nummois_ifwater, ancours, ifin, ifwater1an, date_profil
    integer :: iostat_report, iostat_agmip_report, iostat_dailies
    character(len=3) :: mois_iwater, mois_ifwater  
    character(len=3) :: month_code
    real :: resmes_ini,azomes_ini

    integer :: numdateprof, jour_courant, kk, var_name_index, last_index_climate

    character(:), allocatable :: filename_agmip_report, path_agmip_report, path_dailies
    character(:), allocatable :: path_report
    character(:), allocatable :: error_msg
    character(:), allocatable :: path_sequence_file

    real, allocatable :: soil_values(:)

    last_index_climate=1
    if (iand(pg%P_flagEcriture, ECRITURE_AGMIP) > 0) then
      do ipl= 1, sc%P_nbplantes
        path_agmip_report = join_path(logger%output_path, p(ipl)%agmip_report_filename)
        open (newunit=p(ipl)%agmip_report_unit, file=path_agmip_report, status=unknown, action=write_,&
              position=append, iostat=iostat_agmip_report)
        if (iostat_agmip_report .ne. 0) then
            call exit_error(logger, "Error opening agmip output report file: [" // path_agmip_report // "]")
        end if
      end do
    end if

! writing dailies headers
 if (iand(pg%P_flagEcriture, ECRITURE_SORTIESJOUR) > 0) then
  do ipl = 1, sc%P_nbplantes
    path_dailies = join_path(logger%output_path, p(ipl)%daily_output_filename)    
    open (newunit=p(ipl)%daily_unit, file=path_dailies, status=replace_, action=write_, iostat=iostat_dailies)
    if (iostat_dailies .ne. 0) then
        call exit_error(logger, "Error opening daily ouput file: [" // path_dailies // "]")
    end if
    call write_header(p(ipl)%daily_unit, sc%daily_formats, sc%daily_var_names)
  end do
end if

if (iand(pg%P_flagEcriture, ECRITURE_AGMIP) > 0) then
  do ipl = 1, sc%P_nbplantes
    call Ecriture_VariablesSortiesJournalieres_st3_header(logger, sc, ipl, ag, p(ipl)%agmip_daily_unit, &
      p(ipl)%agmip_daily_output_filename)
  end do
endif

if (iand(pg%P_flagEcriture, ECRITURE_RAPPORTS) > 0) then
  do ipl = 1, sc%P_nbplantes
    path_report = join_path(logger%output_path, p(ipl)%report_filename)
    open (newunit=p(ipl)%report_unit, file=path_report, status=unknown, action=write_,&
          position=append, iostat=iostat_report)
    if (iostat_report .ne. 0) then
      error_msg = "Error opening output report file: " // path_report&
      // " iostat=" // to_string(iostat_report)
        call exit_error(logger, error_msg)
    end if
  end do
end if

call EnvoyerMsgEcran(logger, "Year loop: " // to_string(sc%nbans))

!--------------------- snow ------------------------------
        ! Temporary: zero for snow cumulative vars
        c%ndays_sdepth_over_3cm(:)=0
        c%ndays_sdepth_over_10cm(:)=0
!---------------------------------------------------        
        
    ! La boucle des annees
      do numcult = 1, sc%nbans
        sc%numcult = numcult
        numdateprof = 1

      !: On affiche le numero de la culture courante (pour info uniquement)
      call EnvoyerMsgEcran(logger, "Crop number = " // to_string(numcult))
      call EnvoyerMsgDebug(logger, 'numcult = ' // to_string(numcult))
      call EnvoyerMsgEcran(logger, 'numcult = ' // to_string(numcult))
        do ipl= 1, sc%P_nbplantes
          if (p(ipl)%P_codeplante == CODE_FODDER .and. (p(ipl)%P_stade0 == snu .or. p(ipl)%P_stade0 == plt)) then
            p(ipl)%age_prairie = numcult

          ! DR et FR 23/06/2015 dans le cas des prairies  semees avec reset il ne faut pas repartir a amf
            if (numcult > 1 .and. p(ipl)%codeperenne0==2 .and. pg%P_codeinitprec==2 ) then
              p(ipl)%P_codeperenne = 2
              p(ipl)%P_stade0 = amf
          ! DR - 28/02/08 - Il ne faut pas faire le travail du sol si on est en annee (semis+1)
              itk(ipl)%P_nbjtrav = 0
            else
              p(ipl)%numcoupe=1
              p(ipl)%fauchediff=.FALSE.
            endif
          else
            p(ipl)%age_prairie = -999
          endif

        enddo

    ! Si on enchaine plusieurs annees climatiques => P_codesuite=1
        if (numcult > 1) sc%P_codesuite = 1
    ! Cas d'enchainement de 2 usm differentes : on force P_codeinitprec a 2
        if (sc%P_codesuite == 1 .and. sc%nbans == 1) pg%P_codeinitprec = 2


    ! Initialisation sols, climat et variables du cycle journalier
     call EnvoyerMsgEcran(logger, "Soil Initialisations")
     call initialisations_sol(logger,sc,pg,t,soil,sta)
     
     call EnvoyerMsgEcran(logger, "Initialisations")
     call initialisations(logger,sc,pg,p,r,itk,soil,c,sta,t,ag,last_index_climate)

     if (iand(pg%P_flagEcriture, ECRITURE_PROFIL) > 0 .and. numcult.eq.1) then
       sc%soil_profile_settings = Lecture_Profil(logger, profil_path, sc%P_iwater, sc%annee(sc%P_iwater), &
          sc%annee(sc%P_ifwater), sc%maxwth)
       do ipl = 1, sc%P_nbplantes
          allocate(p(ipl)%tabprof(&
            sc%nbans,&
            size(sc%soil_profile_settings%var_names),&
            size(sc%soil_profile_settings%dates),&
            soil%profsol),&
          source=0.)
       end do
     end if

        do ipl = 1, sc%P_nbplantes
          if(itk(ipl)%P_codedateappN == 1) itk(ipl)%numeroappN = 1
        ! DR 09/10/09 j'ajoute la possibilite de faire des irrigations a des sommes de temp
          if (itk(ipl)%P_codedateappH2O == 1) itk(ipl)%numeroappI = 1
        enddo

! 06/01/2017 on calcule un resmes initial avant tout calcul
    ! calcul de la reserve jusqu'a profcalc
        resmes_ini = SUM(sc%hur(1:soil%profcalc)+sc%sat(1:soil%profcalc))
        azomes_ini = SUM(soil%nit(1:soil%profcalc))
        write(logger%history_unit,19) soil%profcalc, resmes_ini, azomes_ini
  19    format('Initial values over the soil depth (',i3,' cm):',5X,'SWC =',f5.0,' mm',5X,'SMN =',f5.1,' kg N/ha')

     ! writing balance headers !
     if (iand(pg%P_flagEcriture, ECRITURE_BILAN) > 0) then
        do ipl = 1, sc%P_nbplantes
          call ecrireEnteteBilan(logger, ipl, sc,pg,p(ipl),itk(ipl),soil,sta,t, p(ipl)%bilan_unit)
        end do
     end if

  
!:***************************************************************************
!:: Boucle Journaliere                                                     !*

        do n = 1, sc%maxwth
           ! print *,'day = ',n, '       or',n+sc%P_iwater-1
           sc%n = n

         ! on transpose n en jour julien depuis le 1er janvier de l'annee de debut de simulation
           sc%jjul = sc%n + sc%P_iwater - 1
           call modulo_years(sc%jjul,sc%annee(sc%P_iwater), sc%jul)
           sc%numdate = sc%jul
           call get_day_month(sc%jul,sc%annee(sc%jjul),month_code,sc%jour,sc%nummois)

! DR 10/07/02013 j'ajoute l'ecriture du stade start dans le rapport
    !::: Ecriture des rapports
      if (iand(pg%P_flagEcriture, ECRITURE_RAPPORTS) > 0) then
        
      call EnvoyerMsgDebug(logger, 'Stics_Jour: reports')
      call EnvoyerMsgEcran(logger, "Stics_Jour: reports")
      
        do ipl = 1,sc%P_nbplantes
        ! domi 24/09/03 pb dans l'ecriture a des stades
          if (sc%codeaucun == 1) then
          ! domi - 07/10/03 - pour vianney stades+dates
          ! domi - 20/10/03 - je teste pour ne pas ecrire 2 fois le meme jour
            sc%ecritrap = .FALSE.
            if ((sc%codetyperap == 2 .or. sc%codetyperap == 3) .and. (.NOT.sc%ecritrap)) then
              call ecriture_start(sc,soil)
              if ((n == 1) .and. sc%rapdeb)then
                sc%start_rap=.TRUE.

                ! added plant num for getting the report file name in Ecriture_Rapport
                call Ecriture_Rapport(logger,sc,pg,soil,c,sta,p(ipl),t, .FALSE.,ipl)
                sc%ecritrap = .FALSE.
                sc%start_rap=.FALSE.
              endif
            endif
          endif
        enddo
      endif




call EnvoyerMsgEcran(logger, "calling of the daily loop Stics_Jour " // to_string(n))
call EnvoyerMsgEcran(logger, "calling of the daily loop Stics_Jour " // to_string(sc%jjul))

          ! On appelle la routine journaliere de Stics
            call Stics_Jour(logger,sc,pg,p,r,itk,soil,c,sta,t,ag)   
            
            
            if (iand(pg%P_flagEcriture, ECRITURE_PROFIL) > 0) then
                ! stockage des variables par profil
                jour_courant = n + sc%P_iwater - 1
                ! lecture des variables de profil au jour n = 1
                if (n == 1) then
                  ! DR 07/02/2014 j'augmente le nb de jours de sorties pour Simtraces
                  ! pose pb on va tester pluto sur le debut de simul
                  do kk = 1, size(sc%soil_profile_settings%dates)
                      if (jour_courant > sc%soil_profile_settings%dates(kk)) then
                        numdateprof = numdateprof + 1
                      else
                        exit
                      end if
                  end do
                end if
                ! stockage des profils dans le tableau tabprof
                if (numdateprof <= size(sc%soil_profile_settings%dates)) then
                  date_profil = sc%soil_profile_settings%dates(numdateprof)
                else
                  date_profil = -999
                end if
                ! stockage des profils dans le tableau tabprof
                if (jour_courant == date_profil) then
                  do ipl = 1, sc%P_nbplantes
                    do var_name_index = 1, size(sc%soil_profile_settings%var_names)
                      soil_values = get_soil_values(logger, char(sc%soil_profile_settings%var_names(var_name_index)),&
                       sc, p(ipl), soil)
                      p(ipl)%tabprof(numcult, var_name_index, numdateprof, :) = soil_values
                    end do
                  end do
                  numdateprof = numdateprof + 1
                end if
            end if
        end do
!:: Fin de la Boucle Journaliere                                         !*
!:***************************************************************************


call EnvoyerMsgEcran(logger, "end of annual loop iwater " // to_string(sc%P_iwater))
call EnvoyerMsgEcran(logger, "ifwater" // to_string(sc%P_ifwater))


    ! Recalcul des dates de debut, dates de fin de simulation en cas d'enchainement
        sc%n = sc%maxwth

      ! DR - 18/06/08 - j'essaie d'introduire un calcul coherent de P_ifwater et P_iwater en cas d'enchainement
        if (numcult == 1)then
        ! Calcul de P_iwater d'apres l'annee 1
              call get_day_month(sc%P_iwater,sc%ansemis,mois_iwater,jour_iwater,nummois_iwater)
              ancours = sc%annee(sc%ifwater_courant)
        ! Calcul de P_ifwater d'apres l'annee 1
              ifin = sc%ifwater_courant
              if (sc%ifwater_courant > sc%nbjsemis) then
                ifwater1an = sc%ifwater_courant - sc%nbjsemis
              else
                ifwater1an = sc%ifwater_courant
              endif
              call get_day_month(ifwater1an,ancours,mois_ifwater,jour_ifwater,nummois_ifwater)
        endif

 ! Rapport synthetique 1 ligne par annee sauf pour plantes fauchees
        do ipl = 1, sc%P_nbplantes
        
       !write(*,*)'codefauche',itk(i)%P_codefauche
          if(itk(ipl)%P_codefauche == 2) then
            if (iand(pg%P_flagEcriture, ECRITURE_BILAN) > 0) then
              call EnvoyerMsgEcran(logger, "before writing default balance")
              call ecrireBilanDefaut(sc,pg,soil,p(ipl),itk(ipl),c) !,t,c)
            endif
          endif

          sc%P_datefin = .TRUE.
          ! DR 29/12/2021 pour ne pas qu'on ecrive 2 fois le meme jour si on a des dates qui correspondent a la date de fin de simul
            if (sc%rapfin .and. (sc%ecritrap.eqv..FALSE.))then
              call EnvoyerMsgEcran(logger, "Reports writing")
               if (iand(pg%P_flagEcriture, ECRITURE_RAPPORTS) > 0) then
               ! added plant num for getting the report file name in Ecriture_Rapport
                  call Ecriture_Rapport(logger,sc,pg,soil,c,sta,p(ipl),t, .FALSE.,ipl)
                  call EnvoyerMsgEcran(logger, "Standard report writing")
               end if 
               if (iand(pg%P_flagEcriture, ECRITURE_AGMIP) > 0) then
                  call Ecriture_Rapport_AgMIP(sc,pg,soil,sta,p(ipl),ag)
                  call EnvoyerMsgEcran(logger, "Agmip report writing")
               end if
            endif
        end do
        
    ! Calculs de P_iwater et P_ifwater pour annee en cours

          
          call EnvoyerMsgEcran(logger, "numcult nbans culturean iwater " //&
            & to_string_a((/numcult,sc%nbans,sc%P_culturean,sc%P_iwater/)))
          

    ! Recup(eration)
        call EnvoyerMsgEcran(logger, "before Ecriture_DonneesFinDeCycle")
        path_sequence_file = join_path(logger%output_path, retrieval_file)
        call Ecriture_DonneesFinDeCycle(logger, path_sequence_file, sc, pg, p, r, itk, soil)
        call EnvoyerMsgEcran(logger, "after Ecriture_DonneesFinDeCycle")

        call writeSnowStateVariables(join_path(logger%output_path, swow_file),c,sc%P_iwater,sc%ifwater_courant)
      end do ! Fin de la boucle des annees
    call EnvoyerMsgEcran(logger, "end of years loop")
end subroutine Stics_Boucle_Annees
end module Stics_Boucle_Annees_m
 
