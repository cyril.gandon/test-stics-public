! ***** main program of Stics_modulo  *******
! - Programmation: N. Brisson et al
! call readings / initializations and annual loop
!The aims of STICS (Simulateur mulTIdisciplinaire pour les Cultures Standard) are similar to those of a large number of existing models (Whisler et al., 1986).
! It is a crop model with a daily time-step and input variables relating to climate, soil and the crop system.
! Its output variables relate to yield in terms of quantity and quality and to the environment in terms of drainage and nitrate leaching.
! The simulated object is the crop situation for which a physical medium and a crop management schedule can be determined.
! The main simulated processes are crop growth and development as well as the water and nitrogen balances.
! A full description of crop models with their fundamental concepts is available in Brisson et al. (2006).
!
! STICS has been developed since 1996 at INRA (French National Institute for Agronomic Research) in collaboration with other research
! (CIRAD , CEMAGREF , Ecole des Mines de Paris, ESA , LSCE ) or professional (ARVALIS , CETIOM , CTIFL , ITV , ITB , Agrotransferts , etc.) and teaching institutes.
! For more than 20 years STICS has been used and regularly improved thanks to a close link between development and application, involving scientists and technicians from various disciplines.
module SticsMain
   use stdlib_strings, only: to_string
   use command_line_parser
   USE Stics
   USE USM
   USE Plante
   USE Root
   USE Itineraire_Technique
   USE Sol
   USE Climat
   USE Station
   USE Parametres_Generaux
   USE Module_AgMIP
   USE stics_files
   USE stics_system
   use messages
   use messages_data
   use Stics_Zero_m, only: Stics_Zero
   use Stics_Lectures_m, only: Stics_Lectures
   use Initialisation_PrairiePerenne_m, only: Initialisation_PrairiePerenne
   use Stics_Initialisation_Boucle_Annees_m, only: Stics_Initialisation_Boucle_Annees
   use Stics_Boucle_Annees_m, only: Stics_Boucle_Annees
   use Initialisation_PrairiePerenne_m, only: Initialisation_PrairiePerenne
   use call_num_version_m, only: call_num_version
   use soil_profile_m
   use climate_utils
   use climate_file_m
   implicit none

   private
   public :: run

contains
   subroutine run()
      type(Stics_Communs_) :: sc
      type(Plante_), dimension(nb_plant_max) :: p
      type(Root_), dimension(nb_plant_max) :: r
      type(Parametres_Generaux_) :: pg
      type(ITK_), dimension(nb_plant_max) :: itk
      type(Climat_) :: c
      type(Station_) :: sta
      type(Sol_) :: soil
      type(Stics_Transit_) :: t
      type(AgMIP_) :: ag
      type(logger_) :: logger, empty_logger
      integer :: plant_index, output_path_ensure_status
      character(len=100) arg_content
      character(len=50) codeversion
      character(len=255) :: command_line_str
      type(command_line_args_) :: command_line_args
      character(len=255) :: cwd

      call getcwd(cwd)

      call get_command(command_line_str)
      command_line_args = parse_command_line(command_line_str)
      if (command_line_args%version_set) then
         call call_num_version(codeversion)
         print *, 'Modulostics version : ', codeversion
         call exit(0)
      end if

      if(command_line_args%help_set) then
         call show_help()
         call exit(0)
      end if

      if (command_line_args%output_path_set) then
         output_path_ensure_status = ensure_directory(command_line_args%output_path)
         if (output_path_ensure_status .ne. 0) then
            print *, 'Error while creating output path ['//command_line_args%output_path//']'
            call exit(9)
         end if
      end if
      ! Read general parameters first to initialize outputs choice early
      pg = Lecture_Parametres_Generaux(empty_logger, general_settings_path)
      logger = open_logger( &
               write_histo=iand(pg%P_flagEcriture, ECRITURE_HISTORIQUE) > 0 &
               , write_ecran=iand(pg%P_flagEcriture, ECRITURE_ECRAN) > 0 &
               , write_debug=iand(pg%P_flagEcriture, ECRITURE_DEBUG) > 0 &
               , errors_file=errors_file &
               , history_file=history_file &
               , debug_file=debug_file &
               , output_path=command_line_args%output_path &
               )

      call Ecriture_Parametres_Generaux(logger, pg)

      ! Lecture du fichier usm : new_travail.usm (format JavaStics)
      call EnvoyerMsgEcran(logger, "on lit le fichier usm")
      
      ! on zero-ifie les structures plante et itk
      do plant_index = 1, nb_plant_max
         p(plant_index) = plant_ctor()
         itk(plant_index) = ITK_Ctor(plant_index)
      end do

      sc = read_usm_from_file(logger, usm_path, p)

      if (sc%P_nbplantes .gt. nb_plant_max) then
         call exit_error(logger, TOO_MANY_PLANTS//": "//to_string(sc%P_nbplantes)//" > "//to_string(nb_plant_max))
      end if

      call EnvoyerMsgEcran(logger, "before sticslectures")
      sta = station_ctor()

      call Stics_Lectures(logger, sc, pg, p, itk, soil, c, sta, t)

      ! lecture des fichiers parametres de AgMIP
      ag = Stics_Lecture_Param_AgMIP(logger, new_formalisms_path)

      call EnvoyerMsgEcran(logger, "after sticslectures")

      sc%ifwater_courant = sc%P_ifwater

      do plant_index = 1, sc%P_nbplantes
         ! DR 03/03/08 pour la prairie perenne
         ! DR 17/06/08 pour la prairie climator je mets en option a voir si on gardera apres
         ! DR 08/10/09 on remplace codeoutsti par un test sur P_stade0
         if (p(plant_index)%P_codeplante == CODE_FODDER &
             .and. itk(plant_index)%P_codefauche == 1 &
             .and. itk(plant_index)%lecfauche &
             .and. (itk(plant_index)%P_codemodfauche == 3 .or. itk(plant_index)%P_codemodfauche == 2)) then
            ! DR et Fr 11/02/2015 on ne saute les coupes de la premiere annee que si on est en annee de semis
!                     on veut y passer dans tous les cas pour initialiser les sommes ou dates de coupes
!                     on garde les valeurs initiales pour la prairie dans le cas ou elle meurt on va repartir sur les valeurs initiales
            call Initialisation_PrairiePerenne(sc, itk(plant_index), plant_index)
            !endif
         end if
      end do

      call EnvoyerMsgEcran(logger, "parameters are readed")

      call EnvoyerMsgEcran(logger, 'Lecture du fichier climatique')
      c%rows = read_climate_file(logger, climat_file)
      c%tmoy_an = get_means_by_year(c%rows)

      ! Preparation a la boucle des annees : Adaptation des MO au CC
      call Stics_Initialisation_Boucle_Annees(logger, sc, p, pg, itk, c, t)

      call EnvoyerMsgEcran(logger, "calling of the annual loop")
      call Stics_Boucle_Annees(logger, sc, p, pg, r, itk, c, sta, soil, t, ag)

      ! Ecriture du profil
      if (iand(pg%P_flagEcriture, ECRITURE_PROFIL) > 0) then
         call EnvoyerMsgEcran(logger, "Writing profile")
         call write_profil(logger, sc, p)
      end if
      call EnvoyerMsgHistorique(logger, "Program end ... ")

      ! Writing history, debug, error files footer and closing files
      call close_logger(logger)
      call close_plants_files(p)
   end subroutine

   subroutine show_help()
      integer :: i
      character(len=80), parameter :: help_text(*) = [character(len=80) ::      &
      ' --help, -h        print this help and exit                             ', &
      ' --version, -v     print program version information and exit           ', &
      ' --output-path     set the path for output files                        '&
      ]
      do i=1, size(help_text)
         print *, help_text(i)
      end do
   end subroutine
end module
