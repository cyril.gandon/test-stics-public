  program tester
    use, intrinsic :: iso_fortran_env, only : error_unit
    use testdrive, only : run_testsuite, testsuite_type, new_testsuite
    use math_utils_tests, only : collect_math_utils_tests => collect
    use string_utils_tests, only: collect_string_utils_tests => collect
    use command_line_parser_tests, only: collect_command_line_parser_tests => collect
    use dates_utils_tests, only : collect_dates_utils_tests => collect
    use stics_system_tests, only: collect_stics_system_tests => collect
    use read_file_tests, only: collect_read_file_tests => collect
    use astronomy_utils_tests, only: collect_astronomy_utils_tests => collect
    use climate_utils_tests, only: collect_climate_utils_tests => collect
    use usm_tests, only: collect_usm_tests => collect
    use soil_tests, only: collect_soil_tests => collect
    use forced_parameters_tests, only: collect_forced_parameters_tests => collect
    use Lecture_Optimisation_tests, only: collect_Lecture_Optimisation_tests => collect
    use mineral_tests, only: collect_mineral_tests => collect
    use soil_utils_tests, only: collect_soil_utils_tests => collect
    use soil_profile_tests, only: collect_soil_profile_tests => collect
    use phenology_utils_tests, only: collect_phenology_utils_tests => collect
    use Stics_Lectures_tests, only: collect_Stics_Lectures_tests => collect
    use daily_output_tests, only: collect_daily_output_tests => collect
    use vernalization_utils_tests, only: collect_vernalization_utils_tests => collect
    use Lecture_DonneesCyclePrecedent_tests, only: collect_Lecture_DonneesCyclePrecedent_tests => collect

    implicit none
    integer :: stat, is
    type(testsuite_type), allocatable :: testsuites(:)

    testsuites = [ &
      new_testsuite("Math utils", collect_math_utils_tests) &
      , new_testsuite("String utils", collect_string_utils_tests) &
      , new_testsuite("Command Line Parser utils", collect_command_line_parser_tests) &
      , new_testsuite("Dates utils", collect_dates_utils_tests) &
      , new_testsuite("Stics System", collect_stics_system_tests) &
      , new_testsuite("Read File", collect_read_file_tests) &
      , new_testsuite("Astronomy utils", collect_astronomy_utils_tests) &
      , new_testsuite("Climate utils", collect_climate_utils_tests) &
      , new_testsuite("USM", collect_usm_tests) &
      , new_testsuite("Soil", collect_soil_tests) &
      , new_testsuite("Forced Parameters", collect_forced_parameters_tests) &
      , new_testsuite("Lecture Optimisation", collect_Lecture_Optimisation_tests) &
      , new_testsuite("Mineral", collect_mineral_tests) &
      , new_testsuite("Soil utils", collect_soil_utils_tests) &
      , new_testsuite("Soil Profil", collect_soil_profile_tests) &
      , new_testsuite("Phenology utils", collect_phenology_utils_tests) &
      , new_testsuite("Stics Lectures", collect_Stics_Lectures_tests) &
      , new_testsuite("Daily output", collect_daily_output_tests) &
      , new_testsuite("Vernalization utils", collect_vernalization_utils_tests) &
      , new_testsuite("Lecture Donnees Cycle Precedent", collect_Lecture_DonneesCyclePrecedent_tests) &
    ]

    stat = 0
    do is = 1, size(testsuites)
      write(error_unit, '("#", *(1x, a))') "Testing:", testsuites(is)%name
      call run_testsuite(testsuites(is)%collect, error_unit, stat)
    end do
  
    if (stat > 0) then
      write(error_unit, '(i0, 1x, a)') stat, "test(s) failed!"
      error stop
    end if
  
  end program tester
