cmake_minimum_required(VERSION 3.25)

project(Stics)
enable_language(Fortran)
set(CMAKE_BUILD_TYPE "Debug")
set(CMAKE_Fortran_COMPILER gfortran)

# Commented for the moment, before fixing bounds values management
# set(CMAKE_Fortran_FLAGS_DEBUG "-fcheck=bounds")
# Set the base flags
set(CMAKE_Fortran_FLAGS "-ffree-form -std=gnu -fimplicit-none -Wall -Wextra")

# Check the system name
if(NOT APPLE)
    # If the system is not MacOS (e.g., Windows, Linux) we add the static flag:
    set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -static")
endif()

set(dir ${CMAKE_CURRENT_SOURCE_DIR}/build)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${dir}/debug/bin)
set(CMAKE_CACHEFILE_DIR ${dir})

include(FetchContent)

FetchContent_Declare(
  stdlib
  GIT_REPOSITORY https://github.com/fortran-lang/stdlib.git
  GIT_TAG        stdlib-fpm
)
FetchContent_MakeAvailable(stdlib)

# Get all the f90 files
file(GLOB_RECURSE sources src/*.f90 app/*.f90 ${stdlib_SOURCE_DIR}/src/*.f90)

# Exlude test files for now. TODO: remove those files and replace them with an ad hoc unit test framew
list(FILTER sources EXCLUDE REGEX "\\_tests.f90$" )
list(FILTER sources EXCLUDE REGEX "test_stics_files.f90$" )
list(FILTER sources EXCLUDE REGEX "test_utils.f90")

add_executable(Stics ${sources})
