program main
   use stdlib_strings, only: to_string
   use SticsMain, only:run

   real :: start_cpu_time, end_cpu_time

   call cpu_time(start_cpu_time)

   call run()

   call cpu_time(end_cpu_time)
   write (*, *) "The execution was successful."
   write (*, *) "Duration = "//to_string(int((end_cpu_time - start_cpu_time)*1000))//"ms"
end program main
