
module stics_input_m
   use string_builder_m
   use stdlib_string_type
   use fortran202x_split
   use pre_stics_utils
   use stdlib_strings, only: to_string

   implicit none

   !> Represents a single input from the stics program
   type stics_input_

      !> The name of the variable
      type(string_type) :: name

      !> A description of the variable
      type(string_type) :: description

      type(string_type) :: unit
      type(string_type) :: file

      !> Dimension of the input, can be an array if dim > 1
      integer :: dim

      type(string_type) :: type
      type(string_type) :: min
      type(string_type) :: max
      type(string_type) :: codeoptimisation
      type(string_type) :: variete
   end type stics_input_

contains
   type(stics_input_) function from_tokens(tokens) result(ret)
      character(:), allocatable, intent(in) :: tokens(:)
      character(:), allocatable :: dim_str

      integer :: iostat

      ! format is [adens;Interplant competition parameter;SD;PARPLT;1;real;-2;0;1;1]
      ret%name = trim(tokens(1))
      ret%description = trim(tokens(2))
      ret%unit = trim(tokens(3))
      ret%file = trim(tokens(4))

      dim_str = trim(tokens(5))
      read (dim_str, *, iostat=iostat) ret%dim
      if (iostat .ne. 0) then
         call exit_error("Error converting dim=["//dim_str//"] into an integer.")
      end if

      ret%type = trim(tokens(6))
      ret%min = trim(tokens(7))
      ret%max = trim(tokens(8))
      ret%codeoptimisation = trim(tokens(9))
      ret%variete = trim(tokens(10))
   end function

   type(stics_input_) function row_to_stics_input_(row) result(ret)
      type(string_type), intent(in) :: row
      character(:), allocatable :: tokens(:)

      call split(char(row), ';', tokens)
      ret = from_tokens(tokens)

   end function

   type(string_type) function get_container(input_) result(container)
      type(stics_input_), intent(in) :: input_

      select case (char(input_%file))
      case ('PARPLT')
         container = "p(numplt)"
      case ('PARAM')
         container = "pg"
      case ('PARAMV6')
         container = "t"
      case ('PARTEC')
         container = "itk(numplt)"
      case ('STATION')
         container = "sta"
      case ('PARSOL')
         container = "soil"
      case ('INIT')
         if (input_%name == "NH4initf" .or. input_%name == "Hinitf") then
            container = "sc"
         else if (input_%name == "NO3initf") then
            container = "soil"
         else if ( &
            input_%name == "ps0" .or. input_%name == "Sdepth0" .or. input_%name == "Sdry0" .or. input_%name == "Swet0") then
            container = "sta"
         else
            container = "p(numplt)"
         end if
      case ('USM')
         container = "sc"
      case ('USM/USMXML')
         ! do not import
      case ('PARSOL/USM')
         ! do not import
      case ('java')
         ! do not import
      case default
         call exit_error('Skipping variable ['//char(input_%name)//']. File ['//char(input_%file)//'] is unknown.')
      end select
   end function

   function get_generated_lines(input_) result(strings)
      type(stics_input_), intent(in) :: input_
      type(string_builder) :: strings
      type(string_type) :: container, line, var_name
      type(string_type) :: variete, indexing
      integer :: idim, start, end

      if (input_%type == 'character') then
         return
      end if

      if(input_%name == "codeoutscient") then
         ! Unclear what to do with those single var parameter that should be logical instead.
         ! Filter those until a better idea is found.
         return
      end if

      if (input_%name == "codoptim" .or. input_%name == "nbans") then
         var_name = input_%name
      else
         var_name = 'P_'//input_%name
      end if

      if (input_%file == 'PARPLT' .and. input_%variete /= "0") then
         variete = "(itk(numplt)%P_variete)"
      end if

      container = get_container(input_)
      if (container == '') then
         return
      end if

      if (input_%name == 'infil') then
         start = 0
         end = input_%dim - 1
      else
         start = 1
         end = input_%dim
      end if

      do idim = start, end
         if (input_%dim == 1) then
            ! single value, no indexing
            indexing = ''
         else
            ! array, need indexing
            indexing = '('//to_string(idim)//')'
         end if

         if (input_%type == "integer") then
            ! need casting to integer
            line = "        "//container//"%"//var_name//indexing//variete//" = int(variable_value)"
         else
            line = "        "//container//"%"//var_name//indexing//variete//" = variable_value"
         end if
         call strings%push_line("      case ('"//input_%name//indexing//"')")
         call strings%push_line(line)
      end do
   end function

end module stics_input_m
