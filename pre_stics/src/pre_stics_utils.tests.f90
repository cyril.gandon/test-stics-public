module pre_stics_utils_tests
   use stdlib_string_type
   use pre_stics_utils
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private

   public :: collect

contains
   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_extract_parenthesis", test_extract_parenthesis) &
                  ]

   end subroutine collect

   subroutine test_extract_parenthesis(error)
      type(error_type), allocatable, intent(out) :: error
      type(string_type) :: inside
      integer :: idx

      idx = extract_parenthesis(string_type("toto"), inside)
      call check(error, idx, 0)

      idx = extract_parenthesis(string_type("toto()"), inside)
      call check(error, idx, 5)
      call check(error, char(inside), "")

      idx = extract_parenthesis(string_type("toto   (123)"), inside)
      call check(error, idx, 8)
      call check(error, char(inside), "123")
   end subroutine test_extract_parenthesis

end module

