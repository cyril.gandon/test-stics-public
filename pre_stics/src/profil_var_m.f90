
module profil_var_m
   use string_builder_m
   use stdlib_string_type
   use fortran202x_split
   use pre_stics_utils
   use stdlib_strings, only: to_string

   implicit none

   !> Represents a profil output
   type profil_var_
      !> The name of the variable
      type(string_type) :: name

      !> A description of the variable
      type(string_type) :: description

      type(string_type) :: unit

      !> In which structure the data is contained (plant, stics_communs, etc..)
      type(string_type) :: container

   end type

contains
   type(profil_var_) function profil_from_tokens(tokens) result(ret)
      character(:), allocatable, intent(in) :: tokens(:)

      ret%name = trim(tokens(1))
      ret%description = trim(tokens(2))
      ret%unit = trim(tokens(3))
      ret%container = trim(tokens(4))

   end function

   type(profil_var_) function row_to_stics_profil_(row) result(ret)
      type(string_type), intent(in) :: row
      character(:), allocatable :: tokens(:)

      call split(char(row), ';', tokens)
      ret = profil_from_tokens(tokens)
   end function

   function get_profil_lines(input_) result(strings)
      type(profil_var_), intent(in) :: input_
      type(string_builder) :: strings

      call strings%push_line("        case ('"//input_%name//"')")
      call strings%push_line("          ! "//input_%description)
      call strings%push_line("          ! "//input_%unit)

      if(input_%container == "function") then
         call strings%push_line("          soil_values = compute_"//input_%name//"(sc, plant, soil)")
      else
         call strings%push_line("          soil_values = "//input_%container//"%"//input_%name//"(1:soil%profsol)")
      end if
   end function

end module
