
module stics_output_m
   use string_builder_m
   use stdlib_string_type
   use fortran202x_split
   use pre_stics_utils
   use stdlib_strings, only: to_string

   implicit none

   !> Represents a single variable outpout from the stics program
   type stics_output_
      !> The name of the variable
      type(string_type) :: name

      !> A description of the variable
      type(string_type) :: description

      !> In which unit the variable must be (kg, m, kg.ha-1...)
      type(string_type) :: unit

      !> In which derived type the variable is contained: s = station, p = plant, etc..
      type(string_type) :: container

      !> Type of the variable, integer or real
      type(string_type) :: type_

      !> si la valeur est 0 la variable est un scalaire,
      !! si la valeur est > 0 cela indique que la variable est distinguée par couche de sol.
      !!  Ex: pour AZmm la valeur est 5 et on a une déclinaison de cette variable par couche:
      !!       AZmm(1), AZmm(2),..., AZmm(5)
      !! si on a n dans la colonne cela indique que le nom de la variable contient (n) et est une variable temporelle par ex lai(n).
      type(string_type) :: var_index

      !! correspond à une info spécifique pour les variables plantes:
      !!   si on a 1 c'est un scalaire,
      !!   si on a 2 la dimension est 0:2 pour stocker des infos en fonction de l'exposition (soleil, ombre)
      !! l'indice 0 est utilisé pour stocker la valeur globale (somme des valeurs de la variable à l'ombre et au soleil)
       !! Voir 0=AOAS, 1=AO, 2=AS
      integer :: plt_expo_index
   end type stics_output_

contains
   type(stics_output_) function from_tokens(tokens) result(ret)
      character(:), allocatable, intent(in) :: tokens(:)
      character(:), allocatable :: plt_expo_index_str
      integer :: iostat
      ! format is [CNgrain;N concentration in fruits;% dry weight;p;real;0;2]
      ret%name = trim(tokens(1))
      ret%description = trim(tokens(2))
      ret%unit = trim(tokens(3))
      ret%container = trim(tokens(4))
      ret%type_ = trim(tokens(5))
      ret%var_index = trim(tokens(6))

      plt_expo_index_str = trim(tokens(7))
      read (plt_expo_index_str, *, iostat=iostat) ret%plt_expo_index

      if (iostat .ne. 0) then
         call exit_error("Error converting plt_expo_index=["//plt_expo_index_str//"] into an integer.")
      end if
   end function from_tokens

   type(stics_output_) function from_row(row) result(ret)
      type(string_type), intent(in) :: row
      character(:), allocatable :: tokens(:)

      call split(char(row), ';', tokens)
      ret = from_tokens(tokens)

   end function from_row

   subroutine write_stics_output(output_)
      type(stics_output_), intent(in) :: output_

      call write_screen("Name=`"//char(output_%name)//"`" &
                        //", Description=`"//char(output_%description)//"`" &
                        //", Unit=`"//char(output_%unit)//"`" &
                        //", Container=`"//char(output_%container)//"`" &
                        //", Type=`"//char(output_%type_)//"`" &
                        //", VarIndex=`"//char(output_%var_index)//"`" &
                        //", PltExpoIndex=`"//to_string(output_%plt_expo_index)//"`")

   end subroutine write_stics_output

   function get_generated_case(output_) result(strings)
      type(stics_output_), intent(in) :: output_
      type(string_builder) :: strings

      call strings%push_line("    ! "//output_%description)
      call strings%push_line("    ! unit: "//output_%unit)
      select case (char(output_%name))
      case ('tauxcouv(n)')
         call strings%push_line("    case ('tauxcouv(n)')")
         call strings%push_line("       if ( p%P_codelaitr /= 1) then")
         call strings%push_line("         variable_value = sc%tauxcouv(n)")
         call strings%push_line("       else")
         call strings%push_line("         variable_value = p%lai(aoas,n)")
         call strings%push_line("       endif")
         call strings%push_line("      return")
      case ("mafruit")
         call strings%push_line("    case ('mafruit')")
         call strings%push_line("       if ( p%P_codeplante == CODE_BEET) then")
         call strings%push_line("         variable_value = p%matuber")
         call strings%push_line("       else")
         call strings%push_line("         variable_value = p%mafruit")
         call strings%push_line("       endif")
         call strings%push_line("      return")
      case ("CsurNrac")
         call strings%push_line("    case ('CsurNrac')")
         call strings%push_line("      variable_value = 0.")
         call strings%push_line("      if ( p%QNrac > 0. ) variable_value = p%CsurNrac")
         call strings%push_line("      return")
      case ("CsurNracmort")
         call strings%push_line("    case ('CsurNracmort')")
         call strings%push_line("      variable_value = 0.")
         call strings%push_line("      if ( p%QNrac > 0. ) variable_value = p%CsurNracmort")
         call strings%push_line("      return")
      case ("SOCbalance")
         call strings%push_line("    case ('SOCbalance')")
         call strings%push_line("      variable_value = sc%SOCinputs-sc%QCO2sol")
         call strings%push_line("      return")
      case ("DCr")
         call strings%push_line("    case ('DCr')")
         call strings%push_line("      variable_value = sc%Cr - sc%Cr0")
         call strings%push_line("      return")
      case ("SONbalance")
         call strings%push_line("    case ('SONbalance')")
         call strings%push_line("      variable_value = sc%SONinputs-sc%SONoutputs")
         call strings%push_line("      return")
      case default
         call strings%push_line("    case ('"//output_%name//"')")
         call strings%push_line("      variable_value = "//get_roperand(output_))
         call strings%push_line("      return")
      end select
   end function

   type(string_type) function get_roperand(output_) result(roperand)
      type(stics_output_), intent(in) :: output_
      type(string_type) :: out_var, varidx
      integer :: idx

      if (char(output_%var_index) .ne. "0") then
         out_var = output_%name
         if (output_%plt_expo_index > 1) then
            idx = extract_parenthesis(output_%name, varidx)
            if (idx > 0) then
               out_var = char(output_%name, 1, idx - 1)
            end if

            ! double indices
            roperand = output_%container//"%"//out_var//"(aoas,"//output_%var_index//")"
         else
            roperand = output_%container//'%'//output_%name
         end if
      else
         ! indexation sur aoas
         if (output_%plt_expo_index > 1) then
            roperand = output_%container//"%"//output_%name//"(aoas)"
         else
            roperand = output_%container//'%'//output_%name
         end if
      end if

      if (output_%type_ == "integer") then
         roperand = "float("//roperand//")"
      else if (output_%type_ == "logical") then
         ! using merge function, see https://gcc.gnu.org/onlinedocs/gcc-4.4.7/gfortran/MERGE.html
         ! 1 is true, and 0 is false
         roperand = "merge(1.,0.,"//roperand//")"
      end if
   end function get_roperand
end module stics_output_m
