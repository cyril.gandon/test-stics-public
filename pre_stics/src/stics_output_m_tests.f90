module stics_output_m_tests
   use stdlib_string_type
   use stics_output_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none
   private

   public :: collect

contains
   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_roperand", test_get_roperand) &
                  ]

   end subroutine collect

   subroutine test_get_roperand(error)
      type(error_type), allocatable, intent(out) :: error
      type(string_type) :: row
      type(stics_output_) :: parsed

      row = "Cnondec(1);amount of C in the undecomposable mulch made of residues of type 1;kg.ha-1;sc;real;1;0"
      parsed = from_row(row)
      call check(error, char(get_roperand(parsed)), "sc%Cnondec(1)")

      row = "age_prairie;age of the forage crop since sowing;year;p;integer;0;1"
      parsed = from_row(row)
      call check(error, char(get_roperand(parsed)), "float(p%age_prairie)")

      row = "abso(n);N uptake rate by the crop;kg.ha-1.d-1;p;real;n;2"
      parsed = from_row(row)
      call check(error, char(get_roperand(parsed)), "p%abso(aoas,n)")

      row = "nfruit(nboite);number of fruits in last box;SD;p;real;p%P_nboite;2"
      parsed = from_row(row)
      call check(error, char(get_roperand(parsed)), "p%nfruit(aoas,p%P_nboite)")
      
      row = "pdsfruit(4);weight of fruits in box 4;g.m-2;p;real;4;2"
      parsed = from_row(row)
      call check(error, char(get_roperand(parsed)), "p%pdsfruit(aoas,4)")
   end subroutine test_get_roperand

end module stics_output_m_tests
