module pre_stics
   use string_builder_m
   use stdlib_string_type
   use stdlib_strings, only: to_string

   use stics_output_m
   use stics_input_m
   use profil_var_m
   use pre_stics_utils

   implicit none

   character(*), parameter :: list_output_variables = '../doc/outputs.csv'
   character(*), parameter :: template_path = './data/outputs_template.fpp'
   character(*), parameter :: output_path = '../stics/src/outputs/daily_outputs/CorrespondanceVariablesDeSortie.f90'

   character(*), parameter :: list_inputs_variables = '../doc/inputs.csv'
   character(*), parameter :: optim_template_path = './data/Lecture_Optimisation_template.fpp'
   character(*), parameter :: optim_output_path = '../stics/src/inputs/optimisation/Lecture_Optimisation.f90'


   character(*), parameter :: list_profil_variables = '../doc/profil.csv'
   character(*), parameter :: profil_template_path = './data/get_soil_values_template.fpp'
   character(*), parameter :: profil_output_path = '../stics/src/inputs/outputs_management/get_soil_values.f90'

   !! The placeholder symbol to replace in the template
   character(*), parameter :: placeholder_symbol = '[PLACEHOLDER]'

contains

   subroutine create_lecture_optimisation()
      integer :: template_row_index, input_row_index
      type(stringlist_type) :: input_rows, template_rows
      type(string_type) :: template_row, input_row
      type(string_builder) :: output_rows, lines
      type(stics_input_) :: input_

      input_rows = load_file(list_inputs_variables)

      call write_screen("Reading "//list_inputs_variables//": "//to_string(input_rows%len())//" lines read.")

      template_rows = load_file(optim_template_path)

      do template_row_index = 1, template_rows%len()
         template_row = template_rows%get(fidx(template_row_index))
         if (template_row == placeholder_symbol) then
            ! insert content here
            ! start at 2, skipping header row
            do input_row_index = 2, input_rows%len()
               input_row = input_rows%get(fidx(input_row_index))
               input_ = row_to_stics_input_(input_row)
               lines = get_generated_lines(input_)
               call output_rows%push(lines%to_chars())
            end do
         else
            call output_rows%push_line(template_row)
         end if
      end do

      call write_screen("Writing ["//to_string(output_rows%length())//"] into  "//optim_output_path//"...")

      call write_file(optim_output_path, output_rows%to_chars())
   end subroutine

   subroutine create_file_from_outputs()
      integer :: template_row_index, input_row_index
      type(stringlist_type) :: input_rows, template_rows
      type(string_type) :: template_row, input_row
      type(string_builder) :: output_rows, case_rows

      type(stics_output_) :: output_
      real :: start_cpu_time, end_cpu_time

      input_rows = load_file(list_output_variables)

      call write_screen("Reading "//list_output_variables//": "//to_string(input_rows%len())//" lines read.")

      template_rows = load_file(template_path)

      do template_row_index = 1, template_rows%len()
         template_row = template_rows%get(fidx(template_row_index))
         if (template_row == placeholder_symbol) then
            ! insert content here
            ! start at 2, skipping header row
            do input_row_index = 2, input_rows%len()
               input_row = input_rows%get(fidx(input_row_index))
               output_ = from_row(input_row)
               case_rows = get_generated_case(output_)
               call output_rows%push(case_rows%to_chars())
            end do
         else
            call output_rows%push_line(template_row)
         end if
      end do

      call write_screen("Writing ["//to_string(output_rows%length())//"] rows into  "//output_path//"...")

      call write_file(output_path, output_rows%to_chars())

   end subroutine

   
   subroutine create_file_from_profil()
      integer :: template_row_index, input_row_index
      type(stringlist_type) :: input_rows, template_rows
      type(string_type) :: template_row, input_row
      type(string_builder) :: output_rows, case_rows

      type(profil_var_) :: output_
      real :: start_cpu_time, end_cpu_time

      input_rows = load_file(list_profil_variables)

      call write_screen("Reading "//list_profil_variables//": "//to_string(input_rows%len())//" lines read.")

      template_rows = load_file(profil_template_path)

      do template_row_index = 1, template_rows%len()
         template_row = template_rows%get(fidx(template_row_index))
         if (template_row == placeholder_symbol) then
            ! insert content here
            ! start at 2, skipping header row
            do input_row_index = 2, input_rows%len()
               input_row = input_rows%get(fidx(input_row_index))
               output_ = row_to_stics_profil_(input_row)
               case_rows = get_profil_lines(output_)
               call output_rows%push(case_rows%to_chars())
            end do
         else
            call output_rows%push_line(template_row)
         end if
      end do

      call write_screen("Writing into ["//to_string(output_rows%length())//"] "//profil_output_path//"...")

      call write_file(profil_output_path, output_rows%to_chars())

   end subroutine
end module
