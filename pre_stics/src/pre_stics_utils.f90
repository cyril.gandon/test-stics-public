module pre_stics_utils
   use, intrinsic :: iso_fortran_env, only: output_unit
   use stdlib_string_type
   use stdlib_stringlist_type
   use stdlib_strings, only: to_string

   implicit none

contains
   subroutine write_file(path, chars)
      character(*), intent(in) :: path
      character(len=:), allocatable :: chars

      integer :: unit, iostat

      open (newunit=unit, file=path, status='unknown', action='write', iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error('Error opening path for write action: path=['//path//'], iostat='//to_string(iostat))
      end if

      write (unit, '(A)') chars

      close (unit)
   end subroutine

   integer function number_of_rows(unit) result(nrows)
      integer, intent(in)::unit
      integer :: ios

      rewind (unit)
      nrows = 0
      do
         read (unit, *, iostat=ios)
         if (ios /= 0) exit
         nrows = nrows + 1
      end do

      rewind (unit)
   end function

   function load_file(path) result(list)
      character(*), intent(in) :: path

      integer :: unit, iostat_open, iostat_read, row_count, row_index
      type(string_type), dimension(:), allocatable :: rows
      character(5012) :: read_buffer
      type(stringlist_type) :: list

      open (newunit=unit, file=path, status='old', action='read', iostat=iostat_open)
      if (iostat_open .ne. 0) then
         call exit_error('Error in get_file: File ['//path//'] does not exists. iostat='//to_string(iostat_open))
      end if

      row_count = number_of_rows(unit)
      allocate (rows(row_count))
      do row_index = 1, row_count
         read (unit, "(A)", iostat=iostat_read) read_buffer
         rows(row_index) = trim(read_buffer)
      end do
      close (unit)

      list = stringlist_type(rows)
   end function

   integer function extract_parenthesis(str, inside) result(idx)
      type(string_type), intent(in) :: str
      type(string_type), intent(out) :: inside
      integer :: index_start, index_end
      index_start = index(str, '(')
      index_end = index(str, ')')
      if (index_start > 0 .and. index_end > index_start) then
         idx = index_start
         inside = char(str, index_start + 1, index_end - 1)
      else
         idx = 0
      end if
   end function extract_parenthesis

   subroutine write_screen(message)
      character(*), intent(in) :: message

      write (output_unit, *) message
   end subroutine

   subroutine exit_error(error_message)
      character(*), intent(in) :: error_message

      write (output_unit, *) error_message
      call exit(9)
   end subroutine
end module
