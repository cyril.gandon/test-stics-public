program main
   use pre_stics_utils
   use pre_stics
   implicit none

   real :: start_cpu_time, end_cpu_time

   call cpu_time(start_cpu_time)
   call write_screen("Program pre_stics starting...")
   call create_lecture_optimisation()

   call create_file_from_outputs()

   call create_file_from_profil()

   call cpu_time(end_cpu_time)
   call write_screen("Finished in "//to_string(int((end_cpu_time - start_cpu_time)*1000))//"ms")

end program main
