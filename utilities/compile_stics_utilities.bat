ECHO OFF

REM initial dir
SET INIT_DIR=%CD%

REM set version
SET VER=%1
IF "%1"=="" (
     echo "Missing first input argument for model version !"
     exit 1
)

REM set stics dir
SET UTIL_DIR=%2
IF "%2"=="" (
    SET DEBUG_DIR=Debug
    SET UTIL_DIR="."
)

SET DATEVER=%3
IF "%3"=="" (
    SET DATEVER=%DATE%
)

REM Stics dir
echo Stics directory %UTIL_DIR%

REM version file creation
echo Informations about Stics model build version > %UTIL_DIR%\stics_utilities_version.txt
echo Build date: %DATE% (%TIME%) >> %UTIL_DIR%\stics_utilities_version.txt
echo Stics version : %VER% >> %UTIL_DIR%\stics_utilities_version.txt
echo Version date : %DATEVER% >> %UTIL_DIR%\stics_utilities_version.txt

REM ajustJavaStics: moving to Debug folder for compilation
cd %UTIL_DIR%\ajustJavaStics\Debug
IF EXIST ajustJavaStics.exe (
   del /F ajustJavaStics.exe
)

make all


cd %INIT_DIR%
REM maf6versmod: moving to Debug folder for compilation
cd %UTIL_DIR%\maf6versmod\Debug
IF EXIST maf6versmod.exe (
   del /F maf6versmod.exe
)

make all

REM back to initial dir
cd %INIT_DIR%
