#!/bin/bash

# initial dir
INIT_DIR=`pwd`
 
# set version
VER=$1
if [ -z "$1" ];
  then
     echo "Missing first input argument for model version !"
     exit 1
fi

# set util dir
UTIL_DIR="$2"
#DIR=$UTIL_DIR/Debug
if [ -z "$2" ];
  then
     DIR=Debug
     UTIL_DIR=".";
fi



# version file creation
DATE=`date '+%Y/%m/%d'`
TIME=`date '+%H:%M:%S'`

# getting revision date
DATEVER=$3
if [ -z "$3" ];
  then
     DATEVER=$DATE
fi

echo "Informations about Stics model utilities build version" > $UTIL_DIR/stics_utilities_version.txt
echo "Build date: $DATE ($TIME)" >> $UTIL_DIR/stics_utilities_version.txt
echo "Stics version : $VER" >> $UTIL_DIR/stics_utilities_version.txt
echo "Version date : $DATEVER" >> $UTIL_DIR/stics_utilities_version.txt

# link to gfortran version, specific to PIC 147.100.65.20
which gfortran
r=$?
if [ $r=="1" ]
  then
    ln -s /usr/bin/gfortran-4.4 ./gfortran
    curr=`pwd`
    PATH=$PATH:$curr
fi

# checking OS type
OSNAME="Linux"
UNAME_S=`uname -s`
if [ $UNAME_S = "Darwin" ];
	then
	OSNAME="Mac OS"
fi
echo "OS name : $OSNAME" >> $UTIL_DIR/stics_utilities_version.txt


# ajustJavaStics: moving to Debug folder for compilation
cd $UTIL_DIR/ajustJavaStics/Debug

if [ -e ajustJavaStics ];
  then
    rm ajustJavaStics
fi 

if [ -e ajustJavaStics_mac ];
  then
    rm ajustJavaStics_mac
fi

make all

# maf6versmod : moving to Debug folder for compilation
cd $INIT_DIR
cd $UTIL_DIR/maf6versmod/Debug

if [ -e maf6versmod_mac ];
  then
    rm maf6versmod_mac
fi 

if [ -e maf6versmod ];
  then
    rm maf6versmod
fi 

make all


# back to initial dir
cd $INIT_DIR
