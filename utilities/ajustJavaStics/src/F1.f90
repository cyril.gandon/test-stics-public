      function f1(par,npar,stempi)
!     =====================
      dimension par(npar)
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
      common /x/stemp(366)
!
!   model Baret a 1 parametre
!   -------------------------
      if(icomb.eq.00001)then
          f1=cst(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(cst(4)*(stempi-par(1))))
          goto 20
      endif
!
      if(icomb.eq.00010)then
          f1=cst(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(par(1)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.00100)then
          f1=cst(1)*(1/(1+exp(-cst(2)*(stempi-par(1))))&
     &     -exp(cst(4)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.01000)then
          f1=cst(1)*(1/(1+exp(-par(1)*(stempi-cst(3))))&
     &     -exp(cst(4)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.10000)then
          f1=par(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(cst(4)*(stempi-cst(5))))
          goto 20
      endif
  20  continue
      return
      end
