!    programme d'estimation des lai a partir des lai observes
!     et du programme d'ajustement non lineaire haus
! ** DR 12/04/07 cette version est adapt�e aux nouveaux format
! ** de fichiers de javastics
!
common/temps/pas,icodclim
common/lai/jlevee,nobs,nl(366),xinfl(366),nl2(366),xinfl2(366),irec,consec,wdata1,wdata2,ficlai
common/lev/anlevee
common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
integer icod,ian1,ian2,anlevee,jour,mois
real base
integer ansemis
real var
character*30 wdata1,wdata2,ficobs,ficlai
character wlieu*7,codpas*1,codan1,codan2
integer date(3)
character*1 entete(200)
character*1 vars(200)
character*20 mot(20)
real valeur(200)
integer idate(4)
integer icol,collai
logical trouve_lai
real ligne(100)
logical unefois

!	ouverture du fichier test 14=*
open(14,file='test.txt')
!         ouverture de history.sti pour les messages d'erreur
!            open(9,file='history.sti',access='append')
open(9,file='history.sti')
!    ouverture et lecture des parametres dans le fichier param.lai
open(1,file='param.soj')

!	open(199, file='pourquoi.txt')

trouve_lai=.FALSE.
!    lecture du nom du fichier climatique pour l'annee de semis
!            read(1,200)wlieu,codpas,codan1,codan2
!    200 format(a7,a1,1x,2a1)
read(1,*)wdata1
write(14,*)wdata1

read(1,*)wdata2
write(14,*)wdata2
!
!         read(1,'(a12)')wdata2
pas=1
! lecture du fichier des observes
!            read(1,'(a30)')ficobs
read(1,*)ficobs
write(14,'(2x,a30)')ficobs

! lecture du fichier les lai en sortie
!            read(1,'(a30)')ficlai
read(1,*)ficlai
write(14,'(2x,a30)')ficlai

! lecture du jour de levee jo/mo/an
read(1,55)jour,mois,anlevee
     55 format(i2,1x,i2,1x,i4)
	 
jlevee=njan(anlevee,mois,jour,0)

! lecture du code culture sur une annee 1 sur 2 annee 0
read(1,*)icodclim


! lecture de la base de temperature
read(1,*)base

unefois=.TRUE.

! lecture de parametres a ajuster
! 0 fixes
! 1 a interpoler
nbpar=0
do i=1,5
read(1,*)icod(i),var(i)
write(14,*)'parameters',icod(i),var(i)
nbpar=nbpar+icod(i)
enddo
close(1)

!    *** lecture des lai observes
open(2,file=ficobs)

! DR le 12/04/07 on regarde d'abord ou est le lai
read(2,401)(entete(k),k=1,200)
 401    format(200a1)
do ll=1,200
    vars(ll)=''
enddo
im=1
icol=1
do ivar=1,100
!  if(entete(ivar).eq.';'.or.entete(ivar).eq.' ')then
    if(entete(ivar).eq.';'.or.entete(ivar).eq.' ')then
    mot(icol)=vars(1)//vars(2)//vars(3)//vars(4)//vars(5)&
    &//vars(6)//vars(7)//vars(8)//vars(9)//vars(10)//vars(11)&
    &//vars(12)
!			vars=''
    do ll=1,200
      vars(ll)=''
    enddo
    im=1
!			if(mot(icol).eq.'lai(n)')collai=icol
    if(mot(icol).eq.'lai(n)')then
      collai=icol
      icol=icol+1
      trouve_lai=.TRUE.
      goto 179
    endif
    icol=icol+1
!			if(mot(icol).eq.'                                        ') goto 179
  else
    if(entete(ivar).eq.' ')then
      goto 179
    else
      vars(im)=entete(ivar)
      im=im+1
    endif
  endif
enddo
179 backspace(2)

! 25/08/2011 j'essaie d'enlever les ; d'une seule fois 
open(20,file='tempobs2.txt',recl=200)

1405    read(2,401,end=510)(entete(k),k=1,200)
do ivar=1,200
  if(entete(ivar).eq.';')entete(ivar)=' '
enddo
write(20,401)(entete(k),k=1,200)
goto 1405
510 close(20)

!            valeur=''
valeur=0

open(20,file='tempobs2.txt')
open(21,file='tempobs.txt')
!	read(20,*,end=610)(ligne_entete(k),k=1,collai+4)
read(20,*,end=610)

!	write(21,*)(valeur(k),k=1,4),valeur(collai)
2405 read(20,*,end=610)(idate(i),i=1,4),(valeur(k),k=1,collai-4)
if(idate(1).gt.anlevee.and.unefois.eqv..TRUE.)then
!	        write(21,*)anlevee,mois,jour,jlevee,'    -999.99'
!		unefois=.FALSE.
endif
write(21,*)(idate(i),i=1,4),valeur(collai-4)
goto 2405
   610 close(20)
close (21)


open(2,file='tempobs.txt',recl=200)
!	open(2,file='tempobs2.txt',recl=200)

!     == calcul de la premiere annee des obs (annee de semis) et comparaison
!            avec l'annee de la levee
!	read(2,*)
read(2,*,end=30)(date(i),i=1,3)
backspace(2)
ansemis=date(1)
if(((ansemis/4.)*4.).eq.ansemis)then
  nbjansemis=366
else
  nbjansemis=365
endif
 write(14,*)'pas',pas,ansemis,nbjansemis,anlevee
if(anlevee.eq.ansemis)then
  jlevee=njan(anlevee,mois,jour,0)
else
     if(anlevee.eq.ansemis+1)then
            jlevee=njan(anlevee,mois,jour,0)+nbjansemis
     else
!	                if(anlevee.eq.ansemis-1)jlevee=njan(anlevee,mois,jour,0)
!	                write(*,499)ansemis,anlevee
   499 format('error- il y a un pb de concordance de date dans l''ajustement'&
         &    ,' des lai',/,'error- an de debut des obs (semis) =',i4,/&
         &    ,'error- an de levee =',i4)
     write(*,*)"error- You need to add an observation during the first year of the simulation"&
     &" or declare the emergence day during the second day of the simulation and uncheck '2 climate years crop'"
                        stop
     endif
endif
write(14,*)'jlevee ',jlevee

!        == avant levee
20 read(2,*,end=30)(date(i),i=1,3),numjour,vlai
if(date(1).ne.ansemis)numjour=numjour+nbjansemis

!            write(14,*)'numjour avant levee ',numjour,vlai,'jlevee',jlevee
!            if(numjour.lt.jlevee)goto 20
if(numjour.lt.jlevee.and.vlai.lt.0.0)goto 20
i=1
if(numjour.eq.jlevee)then
  write(14,*)'levee obs ',i,numjour,jlevee
  nl(i)=jlevee
  xinfl(i)=vlai
!                 write(14,*)'i',i,numjour,vlai
!                 i=i+1
  backspace(2)
  goto 25
else
                 write(14,*)'emergence forced',i,numjour,jlevee
                 nl(i)=jlevee
                 xinfl(i)=0.
!                 write(14,*)'i',i,numjour,vlai
                 i=i+1
!                 nl(i)=numjour
!                 xinfl(i)=vlai
!                 i=i+1
  backspace(2)
  goto 25
endif
40 write(14,*)'pb avec le fichier des lai observes'
!        == apres la levee
25 read(2,*,end=30)(date(k),k=1,3),numjour,vlai
!            write(14,*)'les obs ',numjour,vlai
if(vlai.ge.0)then
!    modif domi pour le semis une annee et la levee l'annee d'apres
!                 if(numjour.lt.jlevee)numjour=numjour+365
  if(ansemis.ne.date(1))then
         numjour = numjour + nbjansemis
  endif
    nl(i)=numjour
    xinfl(i)=vlai
    write(14,*)'i',i,numjour,vlai
!                 pause
    i=i+1
endif
goto 25
!
30    nobs=i-1
write(14,*)'nobs',nobs
close(2)
write(14,*) 'call of laibym'
write(*,*)"info- pour infos sur l'ajustement regardez le fichier adjust_lai.txt"

call laibym
close(14)
close(9)
close(199)
stop
100    format(i4,i3,i3,1x,i3,1x,f7.2)
end
