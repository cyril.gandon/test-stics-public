      function f2(par,npar,stempi)
!     =====================
      dimension par(npar)
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
      common /x/stemp(366)
!
!   model Baret  a 2 parametres
!   ---------------------------
!
      if(icomb.eq.11000)then
          f2=par(1)*(1/(1+exp(-par(2)*(stempi-cst(3))))&
     &     -exp(cst(1)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.10100)then
          f2=par(1)*(1/(1+exp(-cst(2)*(stempi-par(2))))&
     &     -exp(cst(4)*(stempi-cst(5))))
          goto 20
      endif
      if(icomb.eq.10010)then
          f2=par(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(par(2)*(stempi-cst(5))))
!          write(*,*)'f2',f2
          goto 20
      endif
!
      if(icomb.eq.10001)then
          f2=par(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(cst(4)*(stempi-par(2))))
          goto 20
      endif
!
      if(icomb.eq.01100)then
          f2=cst(1)*(1/(1+exp(-par(1)*(stempi-par(2))))&
     &     -exp(cst(4)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.01010)then              
!          write(*,*)par(1),par(2),cst(1),cst(2),cst(3),cst(4),cst(5)
          f2=cst(1)*(1/(1+exp(-par(1)*(stempi-cst(3))))&
     &     -exp(par(2)*(stempi-cst(5))))  
!          write(*,*)'f2',f2
          goto 20
      endif
!
      if(icomb.eq.01001)then
          f2=cst(1)*(1/(1+exp(-par(1)*(stempi-cst(3))))&
     &     -exp(cst(4)*(stempi-par(2))))
          goto 20
      endif
!
      if(icomb.eq.00110)then
          f2=cst(1)*(1/(1+exp(-cst(2)*(stempi-par(1))))&
     &     -exp(par(2)*(stempi-cst(5))))
          goto 20
      endif
      if(icomb.eq.00101)then
          f2=cst(1)*(1/(1+exp(-cst(2)*(stempi-par(1))))&
     &     -exp(cst(4)*(stempi-par(2))))
          goto 20
      endif
!
      if(icomb.eq.00011)then
          f2=cst(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(par(1)*(stempi-par(2))))
          goto 20
      endif
!
  20  continue
      return
      end
