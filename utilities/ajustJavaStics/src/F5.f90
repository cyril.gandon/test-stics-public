      function f5(par,npar,stempi)
!     =====================
      dimension par(npar)
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
      common /x/stemp(366)
!
!   model Baret a 5 parametres
!   --------------------------
      f5=par(1)*(1/(1+exp(-par(2)*(stempi-par(3))))&
     & -exp(par(4)*(stempi-par(5))))
      return
      end
