      subroutine laibym
!************************************************************************
! ** sous programme d'interpolation des LAI necessaires stics feuille
!   derniere version 22/10/97  possibilite de generer des lai journaliers
!   � partir de fichiers climatos decadaires
!************************************************************************
!
      common/temps/pas,icodclim
      parameter (nobx=366,nworkx=2500)
      common /x/stemp(nobx)
      common/imphau/mecran,mimp,impc
      common/lai/jlevee,nobs,nl(366),xinfl(366),nl2(366)&
     &,xinfl2(366),irec,consec,wdata1,wdata2,ficlai
      common/somcarre/sumc,sdev,idf
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)

	  common/lev/anlevee
      integer anlevee

      real signs(5),yob(nobx),par(5),work(nworkx)
      real base
      external model
      dimension som(800)
      character*30 wdata1,wdata2,ficlai
      character car,station*7
	  character*60 entete
      logical consec,an2,fin  
      integer an
!***** debut du programme ***********************
! ***** ouverture des fichiers
!    annee climatique 1
      open(11,file=wdata1)
	  if(icodclim.ne.1)open(12,file=wdata2)
      open(15,file='adjust_lai.txt')
      ncult=3
      i=1
      car='o'
      consec=.true.
      an2=.true.
      if(icodclim.ne.1)an2=.false.   
!  cycle sur une annee an2 est vraie des le depart      
!      an2=.true.
      nlc=11
      jourini=0
      fin=.false.
! ** test sur les valeurs manquantes
      do 300 il=1,nobs
      if(nl(il).ne.(il+jlevee-1))consec=.false.
  300 continue 
      write(14,*)'dans laibym avant test consec'
! ** le fichier lai a des donnees manquantes
      if(.not.consec)then
!**  calcul des sommes de temperature a partir du jour de semis
!**   pour l'estimation des lai
      somi=0
          do 5 i=1,800
          som(i)=0.
    5     continue
!**  debut de lecture du fichier climatique et calcul des sommes de temp.
      write(14,*)'jlev',jlevee 
!      read(nlc,*) 
   10 continue
      read(nlc,*,end=99)station,AN,mois,nday,Jour &
     & ,Tn,Tx,RG,etp,RR
   30  format(a7,1x,i4,1x,i2,1x,i2,1x,i3,1x,5(f7.1))
!      
      if(jour.lt.(jlevee-1))goto 10 
   20 read(nlc,*,end=99)station,AN,mois,nday,Jour &
     & ,Tn,Tx,RG,etp,RR
      ian=an-1900       
      tm=(tx+tn)/2.
      if(tm.ge.base)then
          diff=tm-base
      else
          diff=0
      end if
      som(jour+jourini)=somi+diff
      somi=som(jour+jourini)
      goto 20
   99 continue 
      write(14,*)'fin de lecture annee',nlc
! test pour savoir si les sommes sont a calculer sur 2 annees
      if(wdata1.ne.wdata2.and.(.not.an2))then
          jourini=jour
! lecture de la deuxieme annee
          close(nlc)
          nlc=12
!          read(nlc,*)
          an2=.true.
          goto 20
      endif
      close(nlc)
      open(29,file=ficlai)
          do 13 i=1,nobs
          jour=nl(i)
          stemp(i)=som(jour)
          yob(i)=xinfl(i)
		write(14,*)jour,stemp(i),yob(i)
   13     continue
      nob=nobs
!
!** particulier a HAUS
!
      nbpar=0
      do 1001 i=1,5
          nbpar=nbpar+icod(i)
!         write(14,*)var(i)
          cst(i)=var(i)
          if(icod(i).eq.1)then
              par(nbpar)=var(i)
!              write(14,*)par(nbpar)
          endif
 1001 continue
      npar=nbpar
! calcul de icomb
      icomb=icod(1)*10000+icod(2)*1000+icod(3)*100+icod(4)*10+icod(5)
      write(14,*)'avant appel a haus',icomb,nbpar,npar
      signs(1)=1.
      if(npar.gt.1) signs(2)=1.
      if(npar.gt.2) signs(3)=1.
      if(npar.gt.3) signs(4)=1.
      if(npar.gt.4) signs(5)=1.
      mecran=6
      mimp=15
      impc=0
      maxit=50
      errc=1.0E-5
      errp=1.0E-5
      nwork=7*npar+2*nob+3*npar**2+nob*npar
      nprob=1
      call haus(nprob,model,nob,yob,npar,par,signs,maxit,errc,errp,work)
      write(14,*)'apres haus'
      close(15)
! ** recuperation des parametres
!      write(14,*)'fin de haus'
! ** ecriture des paramatres dans deslai
!  domi le 08/04/99 test pour empecherle calcul si on a pas assez de sommes de temp
	write(14,*)'avant ecriture dans le fichier de lai interpole'
      if(somi.lt.par(5))then 
	    write(29,*)'Tf non atteint'
	else
		write(29,*)' '
	endif

      numpar=1
      if(icod(1).eq.0)then
         write(29,175)icod(1),cst(1)
      else
         write(29,175)icod(1),par(numpar)
         numpar=numpar+1
      endif
      if(icod(2).eq.0)then
         write(29,176)icod(2),cst(2)
      else
         write(29,176)icod(2),par(numpar)
         numpar=numpar+1
      endif
      if(icod(3).eq.0)then
         write(29,177)icod(3),cst(3)
      else
         write(29,177)icod(3),par(numpar)
         numpar=numpar+1
      endif
      if(icod(4).eq.0)then
         write(29,176)icod(4),cst(4)
      else
         write(29,176)icod(4),par(numpar)
         numpar=numpar+1
      endif
      if(icod(5).eq.0)then
         write(29,177)icod(5),cst(5)
      else
         write(29,177)icod(5),par(numpar)
         numpar=numpar+1
      endif
175   format(i1,1x,f9.4)
176   format(i1,1x,f9.4)
177   format(i1,1x,f15.4)
      sdev=sdev*sdev
      write(29,76)sdev,idf,sumc
   76 format(f10.1,1x,i4,1x,f10.1)
! DR 23/07/08 on ajoute l'annee de levee pour javastics
	write(29,*)anlevee
!
! ** calcul des lai manquants a partir des nouveaux parametres
!  -----------------------------------------------------------
!     j=1
      num=1
      i=jlevee
   82 nl2(num)=i
   14 format(i3,1x,i3,1x,f6.3,1x,f6.1)

      if(nbpar.eq.0)then   
!   cas ou on genere des lai a partir de parametres sans obs      
          xinfl2(num)=cst(1)*(1/(1+exp(-cst(2)*(som(i)-cst(3))))&
     &     -exp(cst(4)*(som(i)-cst(5))))
      endif
!
      if(nbpar.eq.1)then
          xinfl2(num)=f1(par,npar,som(i))
      endif
!
      if(nbpar.eq.2)then
          xinfl2(num)=f2(par,npar,som(i))
      endif
!
      if(nbpar.eq.3)then
          xinfl2(num)=f3(par,npar,som(i))
      endif
!
      if (nbpar.eq.4)then
          xinfl2(num)=f4(par,npar,som(i))
      endif
!
      if(nbpar.eq.5)then
          xinfl2(num)=f5(par,npar,som(i))
      endif
!      write(*,14)num,nl2(num),xinfl2(num),som(i)
      if(xinfl2(num).le.0.)then
          xinfl2(num)=0
          if(fin)then
              irec=i
              write(29,14)num,nl2(num),xinfl2(num),som(i)
              goto 80
          endif
      else
          fin=.true.
      endif
!   
      write(29,14)num,nl2(num),xinfl2(num),som(i)
	 if(som(i).eq.somi)then
		write(14,*)'Tf non atteint'
	    goto 80
	endif
!    write(*,14)num,nl2(num),xinfl2(num),som(i)
      num=num+1
      i=i+1
      goto 82
   80 continue
!      write(14,*)' estimation termin�e '
      close(11)
	close(20)
      close(15)
      close(29)
! ** le fichier des lai n'a pas de donnees manquantes
      else
!      write(14,*)'le fichier lai est consecutif'
      close(11)
      close(15)
      close(2)
      endif
      return
      end

      function nbjourd(ian,idec)
! ** calcul du nombre de jours de la decade *****
      integer njour3(12)
      data njour3/11,8,11,10,11,10,11,11,10,11,10,11/
      if(idec.gt.36)idec=idec-36
! ** test sur les annees bissextiles
      if(((ian/4)*4).eq.ian)njour3(2)=9
      if(mod(idec,3).ne.0)then
          nbjourd=10
      else
          mois=idec/3
          nbjourd=njour3(mois)
      endif
      return
      end
