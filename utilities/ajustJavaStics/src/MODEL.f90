      subroutine model(nprob,par,ye,nob,npar)
      common /x/stemp(366)
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
      real ye(nob),par(npar)
!
      write(*,*)"debug-  appel de model"
      write(*,*)"debug-  ajustement",nbpar," parametres"
!
      if(nbpar.eq.1)then
      do 10 i=1,nob
        write(*,*)"debug-  ",i,'nob',nob,par,npar,stemp(i)
         ye(i)=f1(par,npar,stemp(i))
   10 continue
      endif
!
      if(nbpar.eq.2)then
      do 20 i=1,nob
        write(*,*)"debug-  ",i,'nob',nob,par,npar,stemp(i)
         ye(i)=f2(par,npar,stemp(i))  
   20 continue
      endif
!
      if(nbpar.eq.3)then
      do 30 i=1,nob
        write(*,*)"debug-  ",i,'nob',nob,par,npar,stemp(i)
         ye(i)=f3(par,npar,stemp(i))
   30 continue
      endif
!
      if (nbpar.eq.4)then
      do 40 i=1,nob
        write(*,*)"debug-  ",i,'nob',nob,par,npar,stemp(i)
         ye(i)=f4(par,npar,stemp(i))
   40 continue
      endif
!
      if(nbpar.eq.5)then
      do 50 i=1,nob
        write(*,*)"debug-  ",i,'nob',nob,par,npar,stemp(i)
         ye(i)=f5(par,npar,stemp(i))
   50 continue
      endif
      return
      end
