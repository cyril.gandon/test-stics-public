      function f4(par,npar,stempi)
!     =====================
      dimension par(npar)
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
      common /x/stemp(366)
!
!   model Baret a 4 parametres
!   ---------------------------
      if(icomb.eq.01111)then
          f4=cst(1)*(1/(1+exp(-par(1)*(stempi-par(2))))&
     &     -exp(par(3)*(stempi-par(4))))
          goto 20
      endif
!
      if(icomb.eq.10111)then
          f4=par(1)*(1/(1+exp(-cst(2)*(stempi-par(2))))&
     &     -exp(par(3)*(stempi-par(4))))
          goto 20
      endif
!
      if(icomb.eq.11011)then
          f4=par(1)*(1/(1+exp(-par(2)*(stempi-cst(3))))&
     &     -exp(par(3)*(stempi-par(4))))
          goto 20
      endif
!
      if(icomb.eq.11101)then
          f4=par(1)*(1/(1+exp(-par(2)*(stempi-par(3))))&
     &     -exp(cst(4)*(stempi-par(4))))
          goto 20
      endif
!
      if(icomb.eq.11110)then
          f4=par(1)*(1/(1+exp(-par(2)*(stempi-par(3))))&
     &     -exp(par(4)*(stempi-cst(5))))
          goto 20
      endif
  20  continue
      return
      end
