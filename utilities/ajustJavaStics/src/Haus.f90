      subroutine haus(nprob,model,nob,yob,npar,par,signs,maxit,errc,errp,work)
!
! **********************************************************************
! HAUS : appel simplifie de haus59 - 28 mai 88
!        rapatriee le 18 aout 88 sous MS-DOS.
!
!   nprob        : numero de probleme (pour impression)
!   model        : nom de la subroutine "modele"
!   nob          : nb d'observations
!   yob(nob)     : "y observes"
!   npar         : nb de parametres
!   par(npar)    : parametres
!   signs(npar)  : contraintes de signe sur les parametres
!     si signs(i)=0.,  par(i) peut etre < ou > a 0
!     si signs(i)<>0., par(i) garde le signe de signs(i)
!   maxit        : nb max d'iterations
!   errc         : critere d'arret sur la variation des parametres
!   errp         : critere d'arret sur la variation somme des carres
!   work(nwork)  : tableau de travail
!
!   Le programme principal doit contenir les instructions suivantes :
!
!     PARAMETER(NOB=...,NPAR=...,
!    & NWORK=7*NPAR+2*NOB+3*NPAR*NPAR+NOB*NPAR)
!
!     COMMON /IMPHAU/ MECRAN,MIMP,IMP
! ...... mecran : numero fichier "ecran" (0 ou 6)
!        mimp   : numero fichier "imprimante"
!        imp    : valeur entre 0 et 3,
!                 0 : pas de sorties sur mecran,
!                 3 : sorties mecran maximum (comme mimp)
!
!     EXTERNAL MODEL
! ...... declaration de la subroutine "modele",
!        dont la specification est :
!
!     SUBROUTINE MODEL(NPROB,PAR,YE,NOB,NPAR)
! ...... par(npar) : parametres
!        ye(nob)   : "y calcules"
!
! ...... les "x observes" doivent etre transmis a "model" par common.
!
! **********************************************************************
!
      dimension yob(1),par(1),signs(1),work(1)
      external model

!       
      np2=npar*npar
      i1=1
      i2=i1+npar
      i3=i2+npar
      i4=i3+npar
      i5=i4+npar
      i6=i5+npar
      i7=i6+npar
      i8=i7+nob
      i9=i8+nob
      i10=i9+np2
      i11=i10+np2
      i12=i11+nob*npar
      i13=i12+npar
!
      flam=0.01
      fnu=10.
! ... difz
      do 20 ind=1,i2-1
20    work(ind)=.0001
!      
      write(14,*)'appel de haus59',ind,i2,npar
      call haus59(nprob,model,nob,yob,npar,par,work(i1),signs,errc,errp,&
     & maxit,flam,fnu,work(i2),work(i3),work(i4),work(i5),work(i6),&
     & work(i7),work(i8),work(i9),work(i10),work(i11),work(i12),&
     & work(i13))
!
      return
      end
!
      subroutine haus59(nprob,model,nob,y,np,th,difz,signs,eps1,eps2,mit&
     &,flam,fnu,q,p,e,phi,tb,f,r,a,d,delz,t1,t2)
!
! **********************************************************************
! HAUS59 : ajustement non-lineaire
! version fournie par F. Aries le 29 sept 87,
! modifications au 28 mai 88 :
!     "mise au propre formats",
!     protections e(i)=0 : 150, 231, 7692.
!     sorties : sur mimp : systematiques,
!               sur mecran : selon la valeur de imp=0...3
!     mecran,mimp et imp sont desormais trnsmis a gass60 par argument.
!
! **********************************************************************
!
      common /imphau/ mecran,mimp,impc
      common /somcarre/sumc,sdev,idf
      dimension th(1),difz(1),signs(1),y(1)
      dimension q(1),p(1),e(1),phi(1),tb(1),f(1),r(1),a(1),d(1),delz(1),&
     & t1(1),t2(1)
      dimension delz1(390)
      dimension s(100)
!
! ... underflow multics
      underf=1.0e-18
! ... stockage intermediaire imp
      imp=impc
      impold=imp
!     write(mecran,*)'mimp',mimp,'mecran',mecran,'imp',imp,'impc',impc
!
      npsq=np*np
!     write(*,*)' '
      nscrac=5*np+2*npsq+2*nob+np*nob
!     write(*,*)'np ',np,' nob ',nob,' nscrac ',nscrac
!     write(*,*)nscrac
!     write(*,*)' '
!     write(mimp,6026)char(12)
      write(mimp,6000)nprob
      if(imp.ge.2)write(mecran,6000) nprob
      write(mimp,6013)nob,np,nscrac
      write(mimp,6001)
      if(imp.eq.3) then
        write(mecran,6013)nob,np,nscrac
        write(mecran,6001)
      endif 
!  essai domi      
      call gass60(1,np,th,t1,t2,mecran,mimp,imp)
      write(mimp,6002)
      if(imp.eq.3)  write(mecran,6002)
      call gass60(1,np,difz,t1,t2,mecran,mimp,imp)
      if(min0(np-1,50-np,nob-np,mit-1,999-mit)) 99,15,15
15    if(fnu-1.0) 99,99,16
16    continue
      do 19 i=1,np
      temp=abs(difz(i))
      if(amin1(1.0-temp,abs(th(i)))) 99,99,19
19    continue
      ga=flam
      nit=1
      assign 131 to laos
      assign 225 to iran
      assign 265 to jordan
      if(eps1) 5,10,10
5     eps1=0
10    if(eps2) 40,40,30
40    if(eps1) 60,60,50
60    assign 270 to iran
      goto 70
50    assign 265 to iran
      goto 70
30    if(eps1) 80,80,70
80    assign 270 to jordan
70    ssq=0   
      write(14,*)'appel de model'
      call model(nprob,th,f,nob,np)
      do 90 i=1,nob
      r(i)=y(i)-f(i)
90    ssq=ssq+r(i)*r(i)
      write(mimp,6003) ssq
      if(imp.eq.3)  write(mecran,6003) ssq
!
! *** begin iteration
!
100   ga=ga/fnu
      intcnt=0
!     write(mimp,6004) nit
!     if(imp.ge.2)  write(mecran,6004) nit
101   js=1-nob
      do 130 j=1,np
      temp=th(j)
      p(j)=difz(j)*th(j)
      th(j)=th(j)+p(j)
      q(j)=0
      js=js+nob
      call model(nprob,th,delz1,nob,np)
      do 9120 i=1,nob
      js1=js+i-1
9120  delz(js1)=delz1(i)
      ij=js-1
      do 120 i=1,nob
      ij=ij+1
      delz(ij)=delz(ij)-f(i)
120   q(j)=q(j)+delz(ij)*r(i)
      q(j)=q(j)/p(j)
!
! ... q=xt*r (steepest descent)
!
130   th(j)=temp
      goto laos,(131,414)
131   do 150 i=1,np
      do 151 j=1,i
      sum=0
      kj=nob*(j-1)
      ki=nob*(i-1)
      do 160 k=1,nob
      ki=ki+1
      kj=kj+1
160   sum=sum+delz(ki)*delz(kj)
      temp=sum/(p(i)*p(j))
      ji=j+np*(i-1)
      d(ji)=temp
      ij=i+np*(j-1)
151   d(ij)=temp
150   e(i)=amax1(sqrt(d(ji)),underf)
! !!! protection
666   continue
      do 153 i=1,np
      ij=i-np
      do 153 j=1,i
      ij=ij+np
      a(ij)=d(ij)/(e(i)*e(j))
      ji=j+np*(i-1)
153   a(ji)=a(ij)
!
! ... a=scaled moment matrix
!
      ii=-np
      do 155 i=1,np
      p(i)=q(i)/e(i)
      phi(i)=p(i)
      ii=np+1+ii
155   a(ii)=a(ii)+ga
!
      i=1
      call matin(a,np,p,i,det)
!
! ... p/e=correction vector
!
      step=1.0
      sum1=0.
      sum2=0.
      sum3=0.
      do 231 i=1,np
      sum1=p(i)*phi(i)+sum1
      sum2=p(i)*p(i)+sum2
      sum3=phi(i)*phi(i)+sum3
231   phi(i)=p(i)
      temp=sum1/amax1(sum2*sum3,underf)
! !!! protection
      temp=amin1(temp,1.0)
      temp=57.295*acos(temp)
!     write(mimp,6019) det,temp
!     if(imp.eq.3)  write(mecran,6019) det,temp
170   do 220 i=1,np
      p(i)=phi(i)*step/e(i)
      tb(i)=th(i)+p(i)
220   continue
!     write(mimp,6025)
!     write(mimp,6021) (tb(i),i=1,np)
      if(imp.eq.3) then
!       write(mecran,6025)
!       write(mecran,6021) (tb(i),i=1,np)
      endif
      do 221 i=1,np
      if(signs(i)) 221,221,222
222   if(sign(1.0,th(i))*sign(1.0,tb(i))) 663,221,221
221   continue
      sumb=0
      call model(nprob,tb,f,nob,np)
      do 230 i=1,nob
      r(i)=y(i)-f(i)
230   sumb=sumb+r(i)*r(i)
!     write(mimp,6020) sumb
!     if(imp.eq.3)  write(mecran,6020) sumb
      if(sumb-(1.0+eps1)*ssq) 662,662,663
663   if(amin1(temp-30.0,ga)) 665,665,664
665   step=step/2.0
      intcnt=intcnt+1
      if(intcnt-36) 170,272,272
664   ga=ga*fnu
      intcnt=intcnt+1
      if(intcnt-36) 666,272,272
662   continue
!     write(mimp,6005)
      if(imp.ge.2) then
! ... on stocke imp dans impold et on met imp=3 pour gass60.
!       write(mecran,6005)
        impold=imp
        imp=3
      endif
      do 669 i=1,np
669   th(i)=tb(i)
!     call gass60(1,np,th,t1,t2,mecran,mimp,imp)
      if(imp.ne.impold) then
! ... on restore imp
        imp=impold
      endif
!     write(mimp,6018) ga,sumb
!     if(imp.ge.2)  write(mecran,6018) ga,sumb
      goto iran,(225,270,265)
225   do 240 i=1,np
      if(abs(p(i))-(1.e-20+abs(th(i)))*eps2) 240,240,241
241   goto jordan,(265,270)
240   continue
      write(mimp,6007) eps2
      if(imp.ge.2) write(mecran,6007) eps2
      goto 3000
265   if(abs(sumb-ssq)-eps1*ssq) 266,266,270
266   write(mimp,6008) eps1
      if(imp.ge.2)write(mecran,6008) eps1
      goto 3000
270   ssq=sumb
      nit=nit+1
      if(nit-mit) 100,100,3000
272   write(mimp,6024)
      if(imp.ge.2)write(mecran,6024)
!
! *** end iteration
!
3000  continue
      if(imp.ge.2) then
! ... on stocke imp dans impold et on met imp=3 pour gass60.
        impold=imp
        imp=3
      endif
!     write(mimp,6009)
!     write(mimp,6021) (f(i),i=1,nob)
!     write(mimp,6010)
!     write(mimp,6021) (r(i),i=1,nob)
      do 1 i=1,nob
!     write(8,*)f(i),r(i)
   1  continue
      if(imp.eq.3) then
!       write(mecran,6009)
!       write(mecran,6021) (f(i),i=1,nob)
!       write(mecran,6010)
!       write(mecran,6021) (r(i),i=1,nob)
      do 2 i=1,nob
!     write(8,*)f(i),r(i)
   2  continue
      endif
      ssq=sumb
      idf=nob-np
      write(mimp,6012)
      if(imp.eq.3)  write(mecran,6012)
      i=0
      call matin(d,np,p,i,det)
      do 7692 i=1,np
      ii=i+np*(i-1)
7692  e(i)=sqrt(amax1(d(ii),underf))
! !!! protection
      do 340 i=1,np
      ji=i+np*(i-1)-1
      ij=i+np*(i-2)
      do 340 j=i,np
      ji=ji+1
      a(ji)=d(ji)/(e(i)*e(j))
      ij=ij+np
340   a(ij)=a(ji)
      call gass60(3,np,t1,t2,a,mecran,mimp,imp)
      write(mimp,6014)
      if(imp.eq.3)  write(mecran,6014)
      call gass60(1,np,e,t1,t2,mecran,mimp,imp)
      if(idf) 341,410,341
341   sdev=ssq/idf
      write(mimp,6011) sdev,idf
      if(imp.eq.3)  write(mecran,6011) sdev,idf
      sdev=sqrt(sdev)
      do 391 i=1,np
      p(i)=th(i)+2.0*e(i)*sdev
391   tb(i)=th(i)-2.0*e(i)*sdev
      write(mimp,6017)
      if(imp.eq.3)  write(mecran,6017)
      call gass60(2,np,tb,p,t2,mecran,mimp,imp)
      assign 414 to laos
      goto 101
414   do 415 k=1,nob
      temp=0
      do 420 i=1,np
      do 420 j=1,np
      jsub=k+nob*(i-1)
      debug1=delz(jsub)
!     debug1=delz(k+nob*(2-1))
      isub=k+nob*(j-1)
      debug2=delz(isub)
!     debug2=delz(k+nob*(j-1))
      ij=i+np*(j-1)
      debug3=d(ij)/(difz(i)*th(i)*difz(j)*th(j))
420   temp=temp+debug1*debug2*debug3
! !!! temp=abs(temp)
!      rajout domi du 2/11/95 d'apres commentaire  temp=abs(temp)  
      temp=abs(temp)
      temp=2.0*sqrt(temp)*sdev
      r(k)=f(k)+temp
415   s(k)=f(k)-temp
!     write(mimp,6006)
      if(imp.eq.3)  write(mecran,6006)
      ie=0
      do 425 i=1,nob,10
      ie=ie+10
      if(nob-ie) 430,435,435
430   ie=nob
435   continue
!     write(mimp,6021) (r(j),j=i,ie)
!     write(mimp,6021) (s(j),j=i,ie)
      if(imp.eq.3) then
        write(mecran,6021) (r(j),j=i,ie)
        write(mecran,6021) (s(j),j=i,ie)
      endif
425   continue
!
410   if(imp.ne.impold) then
! ... on restore imp
        imp=impold
      endif
! ... on reimprime les parametres et la somme des carres
      write(mimp,6005)
!     write(mimp,6023) (ibid,ibid=1,np)
      write(mimp,6021) (th(ibid),ibid=1,np)
      sumc=0.0
      do 501 i=1,nob
        sumc=sumc+(f(i)-y(i))**2
501   continue
      write(mimp,6027) sumc
      if(imp.ge.1) then
        write(mecran,6005)
!       write(mecran,6023) (ibid,ibid=1,np)
        write(mecran,6021) (th(ibid),ibid=1,np)
        write(mecran,6027) sumc
      endif
      goto 999
!
! ... erreur de parametres
99    write(mimp,6016)
      write(mecran,6016)
!
! ... fin
999   write(mimp,6015) nprob,nit
      if(imp.ge.2) write(mecran,6015)nprob,nit
      close(6)
      return
!
6000  format('    HAUS59 - ESTIMATION NON-LINEAIRE - PROBLEME No. ',&
     &i5/4x,53(1h-))
6026  format(a,/)
6013  format(/'  (',i3,' observations, ',i5,' parametres,',i7,&
     &' memoires de travail auxiliaires.)'/)
6001  format(/' VALEURS INITIALES DES PARAMETRES :')
6002  format(/' proportions utilisees en calculant la diff.',&
     &' des quotients :')
6003  format(/' somme des carres initiaux : ',e12.4)
!6004  format(/' *** ITERATION No.',i4,' ***')
6005  format(/' valeurs des parametres par regression :')
6006  format(/' intervalle de confiance approche pour chaque valeur',&
     &' de la fonction :')
6007  format(/' ARRET DES ITERATIONS - variation relative de chaque',&
     &' parametre < a ',e10.3)
6008  format(/' ARRET DES ITERATIONS - variation relative de la somme',&
     &' des carres < a ',e10.3)
!6009  format(/'  valeurs finales de la fonction :')
!6010  format(/'  residus :')
6011  format(/' variance des residus : ',e12.4,',',i4,' degres de',&
     &' liberte.')
6012  format(/' matrice de correlation :')
6014  format(/' elements normalisants :')
6015  format(//'    FIN DU PROBLEME No.',i5,3x,i5,' iterations.'/&
     & 4x,44(1h-))
6016  format(/' ERREUR DE PARAMETRE.')
6017  format(/' limites de confiance individuelles pour chaque',&
     &' parametre (sous hypothese lineaire) :')
!6018  format(/' lambda : ',e10.3,',  somme des carres apres',
!     &' regression : ',e15.7)
!6019  format(/' determinant : ',e12.4,',  angle en coord. scalaire : ',
!     &f5.2,' degres.'/)
!6020  format(' test ponctuel sur la somme des carres : ',e12.4)
6021  format(1x,10e12.4)
!6023  format(1x,i8,9i12)
6024  format(//' ON NE PEUT PAS RENDRE LA SOMME DES CARRES INFERIEURE',&
     &' A CELLE OBTENUE A L''ITERATION PRECEDENTE :'&
     &/' on arrete l''iteration.')
!6025  format(' test ponctuel sur les valeurs des parametres : ')
6027  format(/' somme finale des carres : ',e12.4)
!
      end
!
      subroutine gass60(itype,nq,a,b,c,me,mi,imp)
!
! *********************************************************************
! GASS60 - version 28 mai 88
!   "mise au propre",
!   sorties sur mi systematiques,
!   sorties sur me si imp=3.
! *********************************************************************
!
      dimension a(nq),b(nq),c(100)
!
      np = nq
      nr = np/10
      low = 1
      lup = 10
!
      if(imp.ne.3) then
! ....pas d'impression sur me
100    if(nr) 999,20,30
20     lup = np
       if(low.gt.lup) goto 999
30     write(mi,1000) (j,j = low,lup)
       goto(40,50,60), itype
40      write(mi,1010) (a(j),j = low,lup)
        goto 90
50      write(mi,1010) (b(j),j = low,lup)
        goto 40
60      do 70 i = low,lup
          jj = (i-1)*lup+low
          jk = (i-1)*lup+i
          write(mi,1020) i,(c(ll),ll = jj,jk)
70      continue
        low2 = lup+1
        if(low2.gt.np) goto 90
        do 80 i = low2,np
          jj = (i-1)*np+low
          jk = (i-1)*np+lup
          write(mi,1020) i,(c(ll),ll = jj,jk)
80      continue
90      low = low+10
        lup = lup+10
        nr = nr-1
        goto 100
! ... fin de la partie sans impression sur me
!
      else
! ... impression sur me
200     if(nr) 999,120,130
120     lup = np
        if(low.gt.lup) goto 999
130     write(mi,1000) (j,j = low,lup)
        write(me,1000) (j,j = low,lup)
        goto(140,150,160), itype
140     write(mi,1010) (a(j),j = low,lup)
        write(me,1010) (a(j),j = low,lup)
        goto 190
150     write(mi,1010) (b(j),j = low,lup)
        write(me,1010) (b(j),j = low,lup)
        goto 140
160     do 170 i = low,lup
          jj = (i-1)*lup+low
          jk = (i-1)*lup+i
          write(mi,1020) i,(c(ll),ll = jj,jk)
          write(me,1020) i,(c(ll),ll = jj,jk)
170     continue
        low2 = lup+1
        if(low2.gt.np) goto 190
        do 180 i = low2,np
          jj = (i-1)*np+low
          jk = (i-1)*np+lup
          write(mi,1020) i,(c(ll),ll = jj,jk)
          write(me,1020) i,(c(ll),ll = jj,jk)
180     continue
190     low = low+10
        lup = lup+10
        nr = nr-1
        goto 200
! ... fin de la partie avec impression sur me
      endif
!
999   return
!
1000  format(1x,i8,9i12)
1010  format(1x,10e12.4)
1020  format(1x,i3,1x,f7.4,9f12.4)
      end
!
      subroutine matin(a,nvar,b,nb,det)
!
! *********************************************************************
! MATIN - version 28 mai 88
!   "mise au propre".
! *********************************************************************
!
      dimension a(1),b(1)
!
      pivotm = a(1)
      det = 1.0
      do 100 icol = 1,nvar
        kkz = (icol-1)*nvar+icol
        pivot = a(kkz)
        pivotm = amin1(pivot,pivotm)
        det = pivot*det
!
! ....  divide pivot row by pivot element
!
        a(kkz) = 1.0
        pivot = amax1(pivot,1.0e-20)
        pivot = a(kkz)/pivot
        do 10 l = 1,nvar
          ll = (icol-1)*nvar+l
10      a(ll) = a(ll)*pivot
        if(nb.eq.0) goto 30
        do 20 l = 1,nb
          ll = (l-1)*nvar+icol
20      b(ll) = b(ll)*pivot
!
! ..... reduce non-pivot rows
!
30      do 100 l1 = 1,nvar
          if(l1.eq.icol) goto 100
          ll1 = (l1-1)*nvar+icol
          t = a(ll1)
          a(ll1) = 0.
          do 40 l = 1,nvar
            ll1 = (l1-1)*nvar+l
            ll = (icol-1)*nvar+l
40        a(ll1) = a(ll1)-a(ll)*t
          if(nb.eq.0) goto 100
          do 50 l = 1,nb
            ll1 = (l-1)*nvar+l1
            ll = (l-1)*nvar+icol
50        b(ll1) = b(ll1)-b(ll)*t
!
100   continue
!
      return
      end

