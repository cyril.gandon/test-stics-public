      function f3(par,npar,stempi)
!     =====================
      dimension par(npar)
      common /param/base,icod(5),var(5),icomb,nbpar,cst(5)
      common /x/stemp(366)
!
!   model Baret  a 3 parametres
!   ---------------------------
!
      if(icomb.eq.11100)then
          f3=par(1)*(1/(1+exp(-par(2)*(stempi-par(3))))&
     &     -exp(cst(4)*(stempi-cst(5))))
          goto 20
      endif
      if(icomb.eq.11010)then
          f3=par(1)*(1/(1+exp(-par(2)*(stempi-cst(3))))&
     &     -exp(par(3)*(stempi-cst(5))))  
!      write(*,*)'f3',f3
          goto 20
      endif
      if(icomb.eq.11001)then
          f3=par(1)*(1/(1+exp(-par(2)*(stempi-cst(3))))&
     &     -exp(cst(4)*(stempi-par(3))))
          goto 20
      endif
!
      if(icomb.eq.10110)then
          f3=par(1)*(1/(1+exp(-cst(2)*(stempi-par(2))))&
     &     -exp(par(3)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.10101)then
          f3=par(1)*(1/(1+exp(-cst(2)*(stempi-par(2))))&
     &     -exp(cst(4)*(stempi-par(3))))
          goto 20
      endif
!
      if(icomb.eq.10011)then
          f3=par(1)*(1/(1+exp(-cst(2)*(stempi-cst(3))))&
     &     -exp(par(2)*(stempi-par(3))))
          goto 20
      endif
!
      if(icomb.eq.01110)then
          f3=cst(1)*(1/(1+exp(-par(1)*(stempi-par(2))))&
     &     -exp(par(3)*(stempi-cst(5))))
          goto 20
      endif
!
      if(icomb.eq.01101)then
          f3=cst(1)*(1/(1+exp(-par(1)*(stempi-par(2))))&
     &     -exp(cst(4)*(stempi-par(3))))
          goto 20
      endif
      if(icomb.eq.01011)then
          f3=cst(1)*(1/(1+exp(-par(1)*(stempi-cst(3))))&
     &     -exp(par(2)*(stempi-par(3))))
          goto 20
      endif
!
      if(icomb.eq.00111)then
          f3=cst(1)*(1/(1+exp(-cst(2)*(stempi-par(1))))&
     &     -exp(par(2)*(stempi-par(3))))
          goto 20
      endif
!
  20  continue
      return
      end
