    

! ** declarations  integer
      integer ficrap(2),ficsort(2),ficsort2(2),ficbil(2)

      integer group(2),parapluie(2),codeinstal(2)

      integer nsencour(2),nsencourpre(2),nsencourprerac(2),ntrav(2,10)
      integer nnou(2),nbj0remp(2),nbjgel(2),nfauche(2,10)

      integer nlanobs(2),nlax(2),nrecobs(2),nlaxobs(2)
      integer nlev(2),nlevobs(2),nmatobs(2)
      integer nplt(2),ndrp(2),ndebsenrac(2)
      integer nbrecolte(2),nrecint(2,20),ndebdes(2),ntaille(2),nger(2)
      integer igers(2),inous(2),idebdess(2),ilans(2)
      integer iplts(2),compretarddrp(2)
      integer iamfs(2),idebdorms(2),idrps(2),ifindorms(2),iflos(2)
      integer ilaxs(2),namf(2)
      integer imats(2),irecs(2),isens(2),nsen(2),nsenobs(2)      
      
      integer ndebdorm(2),nfindorm(2),nbfeuille(2)
      integer nflo(2),nfloobs(2),nstopfeuille(2)
      integer mortplante(2)
      
      integer neclair(2),nrogne(2),neffeuil(2),jdepuisrec(2)
      integer namfobs(2),nlan(2)
      integer nrec(2),nrecbutoir(2)
      integer nmat(2),ndrpobs(2),ndebdesobs(2),ilevs(2)
      integer numcoupe(2),nst1coupe(2),nst2coupe(2),ndebsen(2)
! ** domi 22/06/04 pour calculer la tmoy entre 2 dates
      integer nbjTmoyIpltJuin(2),nbjTmoyIpltSept(2)

! ** offrnodu
      integer ndno(2),nfno(2),nfvino(2)
! domi 16/09/05 pour fixpot fixreel et offrenod indexation sur ao/as
      real fixpotC(2),fixreelC(2),offrenodC(2),fixmaxC(2),QfixC(2)
      real fixpotfno(2),propfixpot(2)

! ** NB le 14/12/04         
      integer nrecalpfmax(2)

! ** d�claration logique
      logical fauchediff(2),sioncoupe(2),onarretesomcourdrp(2)
      logical etatvernal(2)
      logical gelee(2)

! ** declarations real

      real LRACH(2,5)  
      real cinterpluie(2),totpl(2)
      real rc(2)  
      real lracz(2,1000),cumlracz(2)
      real dfol(2)
      real rombre(2),rsoleil(2)
      real somcourfauche(2)
      real QNplanteres(2)
      real udevlaires(2,10)
      real cescoupe(2),cetcoupe(2),cepcoupe(2),cetmcoupe(2)
      real cprecipcoupe(2),cep(2),cprecip(2)
      real cet(2),ces(2),cetm(2)
      real masectot(2),str1coupe(2),stu1coupe(2),str2coupe(2)
      real stu2coupe(2),inn1coupe(2),diftemp1coupe(2),inn2coupe(2)
      real diftemp2coupe(2)
      real QNressuite(2)
      real pfmax(2)
      real stemflow(2)
      real precrac(2,1000),lracsenz(2,1000)
      real drl(2,0:731,1000),somtemprac(2)
      real rl(2,0:731,1000),poussracmoy(2)
      real Emd(2)
      real diftemp1(2),diftemp2(2)
      real flrac(2,1000),ftemp(2),udevcult(2),udevair(2)
      real ebmax(2),efdensite(2),ulai(2,0:731)
      real caljvc(2),somelong(2),somger(2),cdemande(2)
        
    
! ** rajout domi vers 4.0
      real zrac(2),znonli(2),deltaz(2)
      real difrac(2),resrac(2),Scroira(2)

      
! ** variables qui etaientt AO/AS et qu'on a rendu communes 
      real cumraint(2),cumrg(2)
      
! ** NB le 12/11/98 version 4.0
      real irrigprof(2,1000)
      real stlevdrp0(2)
      real stsenlan0(2)
      real stlaxsen0(2),remobil(2)
      real somcourdrp(2),dtj(2,0:731),qressuite(2),CsurNressuite(2)
      real cumlraczmaxi(2)

! ** domi passe en common pour lecsortie 08/03/99
      real cumdevfr(2),nbfruit(2)
      real tetstomate(2),teturg(2)
      real rltot(2),rfpi(2),lracsentot(2),largeur(2)
      

! ** nadine modifs pour boucler bilan d'azote 8/03/99      
! domi 16/09/05 cuml�  car indexe sur ao,as
! DR 12/10/06 lrac est devevu locale dans offreN
!      real lrac(2)
      real utno(2),pfv(2),pfj(2),pftot(2)
      real pfe(2),pft(2),wcf(2),wct(2),wce(2)
      real proppfz(2)
      real stpltlev(2)  
      real stdrpsen(2)
      real stmatrec(2)
      real upvtutil(2),upvt(2,0:731),somcour(2),reajust(2)
      real upobs(2,0:731)
      real stlevamf0(2),stamflax0(2)
      real varrapforme(2)
      
      
      real anoxmoy(2),chargefruit(2),coeflev(2),cumflrac(2)
      real densitelev(2),densitesem(2),durvieI(2)
      real exobiom(2),exofac(2),exofac1(2),exofac2(2),exolai(2)
      real fco2(2),fgelflo(2),fgellev(2),fstressgel(2),ftempremp(2)    
      real gel1(2),gel2(2),gel3(2)
      real izrac(2),idzrac(2)
      real nbfrote(2),rmaxi(2)
      real somcourutp(2),somcourno(2),somfeuille(2)
      real somtemp(2),somupvt(2)
      real stdrpmatini(2),stflodrp0(2),stlevflo(2)
      real TmoyIpltJuin(2),TmoyIpltSept(2)
      real zracmax(2)


      real rfvi(2)
      
      real racnoy(2,1000)  
      real msrac(2,0:731),tdevelop(2,0:731),cu(2,0:731),utp(2,0:731)
      
      integer nst1(2),nst2(2)
      real inn1(2),inn2(2),str1(2),str2(2),stu1(2),stu2(2)
      real exofac1moy(2),exofac2moy(2),inn1moy(2),inn2moy(2)
      real swfac1moy(2),swfac2moy(2),turfac1moy(2),turfac2moy(2)

!** DR le 13/01/06 pour maintient des varaibles plante si codemsfinal=oui
      real mafraisCveille(2),pdsfruitfraisCveille(2),&      
      maboisCveille(2),H2OrecCveille(2),chargefruitveille(2)

! ** dr 14022006 pass� en common pour etre recupere dans Ngrain
      real nbgraingel
      real QNplanteCtot(2)
! dr 170306 rajout variables cumul�es
      real ctmoy(2),ctcult(2),cetp(2),crg(2)



!************************ les commons *********************
      common /varpltint/mortplante,nbfeuille,ndebdorm,nfindorm, &
      nflo,nfloobs,nstopfeuille,jdepuisrec,neclair,nrogne,neffeuil,   &
      ndno,nfno,nfvino,nst1,nst2,nsen,nsenobs,namfobs,nlan,nrec, &
      nrecbutoir,nmat,ndrpobs,ndebdesobs,ilevs,numcoupe,nst1coupe, &
      nst2coupe,nrecalpfmax
      
      common /varpltreal/anoxmoy,chargefruit,coeflev,cumflrac,   &
      densitelev,densitesem,durvieI,exobiom,exofac,exofac1,exofac2,  &
      exolai,fco2,fgelflo,fgellev,fixpotC,fixreelC,fixpotfno,fstressgel  &
      ,ftempremp,gel1,gel2,gel3,izrac,idzrac,nbfrote,propfixpot,rmaxi,   &
      somcourutp,somcourno,somfeuille,somtemp,somupvt,stdrpmatini,  &
      stflodrp0,stlevflo,TmoyIpltJuin,TmoyIpltSept,zracmax,   &
      nbjTmoyIpltJuin,nbjTmoyIpltSept,rfvi
     
      common /varpltreal2/inn1,inn2,str1,str2,stu1,stu2,exofac1moy,  &
      exofac2moy,inn1moy,inn2moy,swfac1moy,swfac2moy, &
      turfac1moy,turfac2moy
     
      common /varplttab/cu,msrac,racnoy,tdevelop,utp
      
      common /rayonnement/rombre,rsoleil

      common /parplt/stpltlev,stdrpsen,stmatrec



! *- NB - le 23/3/98
      common/parplt2/dfol,precrac,lracsenz,rl, &
      somtemprac,drl,poussracmoy,Emd



       
! *- common variables d'�tat

      common /varpltint1/nbrecolte,nrecint,ndebdes,ntaille,group,&
      parapluie,codeinstal,nsencour,nsencourpre,nsencourprerac,&
      ntrav,nnou,nbj0remp,nbjgel,nfauche,nlanobs,nlax,nrecobs,nlaxobs,&
      nlev,nlevobs,nmatobs,nplt,ndrp,ndebsenrac,ndebsen, &
      nger,igers,inous,idebdess,ilans,iplts, &
      compretarddrp,iamfs,idebdorms,idrps,ifindorms,iflos,ilaxs,namf, &
      imats,irecs,isens

      common /vatplt5/upobs,  &
      upvtutil,upvt,somcour,reajust, &
      zrac,znonli,deltaz,Scroira,difrac,resrac,rc
      
    
      common /varplt6/cep,cprecip,cet,ces,cetm,&
      diftemp1,diftemp2,lracz,cumlracz, &
      flrac,ftemp,udevcult,udevair,ebmax,efdensite,   &
      ulai,sioncoupe,onarretesomcourdrp, &
      cepcoupe,cprecipcoupe,str1coupe,&
      stu1coupe,str2coupe,stu2coupe,inn1coupe, &
      diftemp1coupe,inn2coupe,diftemp2coupe,fauchediff,  &
      somcourfauche,masectot,caljvc,  &
      somelong,somger,cdemande


      common /varplt7/QNressuite,QNplanteres, &
      pfmax,cescoupe,cetcoupe,cetmcoupe, &
      udevlaires,cinterpluie,totpl,varrapforme

 
      common /varpltnomfic/ficrap,ficsort,ficsort2,ficbil
     
     
      common /varplt1/cumraint,cumrg,QfixC,Remobil,stemflow

      
        

      common /varplt3/Somcourdrp,Dtj,  &
       Cumdevfr,Nbfruit,cumlraczmaxi, &
       Qressuite,CsurNressuite,LRACH,Tetstomate, &
       teturg,rltot,rfpi,lracsentot,largeur,  &
       utno,pfv,pfj,pftot,pfe, &
       pft,wcf,wct,wce,proppfz, &
       etatvernal,gelee,stlevamf0,stamflax0,  &
       stlaxsen0,stsenlan0,stlevdrp0


      common /varplt4/irrigprof,offrenodC,fixmaxC

      common/ dr130106/mafraisCveille,pdsfruitfraisCveille, &     
      maboisCveille,H2OrecCveille,chargefruitveille

      common /dr170106/QNplanteCtot

      common /dr140206/nbgraingel
      common /dr170306/crg,ctmoy,ctcult,cetp

! DR 20/07/06 on compte le nb de jours ou l'on a repousse la fauche
      integer nbrepoussefauche(2)
      common /dr200706/nbrepoussefauche

! DR 06/09/06
      real tempeff(2)
      common /dr060906/tempeff



