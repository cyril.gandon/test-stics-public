! *********************************************** c
! * >>>>>>>                             <<<<<<< * c
! * >>>>>>>>>>> Variables communes <<<<<<<<<<<< * c
! * >>>>>>>                             <<<<<<< * c
! *********************************************** c

! ** TRES IMPORTANT - cette directive empeche d'utiliser une variable non declar�e !!
!      implicit none



! **************************** c
! **>> VARIABLES ENTIERES <<** c
! **************************** c

      integer ipl, nbplantes

      integer ichsl, iwater, ifwater, ifwater0,dernier_n
      integer n,nbans,nbjanrec,nstoc,numcult
      integer pas

! *** le code d'ensolleillement, AO ou AS ***
      integer ens

      integer fichist,ficsta

! ** PB - indice du fichier de debugging - 16/01/2004
      integer ficdbg

      integer culturean,codoptim,codesuite,codeinitprec 
      integer nbjsemis,maxwth
      integer nstoprac
      integer numeroappN
      integer numdate
      integer bouchon
      integer nouvdensrac,codentete
      integer codeSIG
! ne sert plus 28/12/06,codevolat,codNH4,
      integer nappmulch
      integer ires,itrav
! DR le 28/12/06 ne sert � rien
!      integer coderapport,date1rap,date2rap
      integer codesensibilite

      integer ansemis
      integer annee(731)
      integer NH

! ** pour la version epistics    
      integer codepitaf

      
      integer codesoltrop,codeprofil

      integer codeulaivernal,codefrmur
      
! ** OFFRNODU
      integer codefxt,codecombi,codefxn
      
      integer julfin,julzero,nbjrecol,nbjrecol0,NHE,napini,napNini

! ** inicoupe DR le 7/11/05
      integer faucheannule
      integer nbjpourdecisemis,nbjpourdecirecolte



! *************************** c
! **>> VARIABLES REELLES <<** c
! *************************** c


! *- code vari�t� (real ou char ??)
! --      real codevar
      
      real Ninitf(5),Hinitf(5)
      real delta

      real devjourfr

      real esz(1000)
      real fdens,tustress
          
      real rdif
      real originehaut
      real tairveille
           
!  declaration pour sp senescen  18/03/98        
!      real coefamsres,coefbmsres,coefaifres,coefbifres,coefcifres

      real a,effN
      real hi,ha,hpf,rglo,hurlim
      real rnetS,rnet,albedolai
      real resmes
! ** Domi - 25/10/2004 - j'ai mis 0 � dacouche sinon pb le jour de la recolte 
! *- dans densirac (si codeculture = feuille)
      real dacouche(0:1000)
      real ruisselt,infilj(0:5),exces(0:5),anox(1000)
      real pluieruissel,sat(1000),cpreciptout
      real ruissel
      real QeauI,QeauFS,Qeau0
      real doi,Edirect,humidite
      real mouillmulch,Emulch,intermulch,cintermulch
      real ruisselsurf
      real ras,Nvolat
      real eptcult,TcultMin,TcultMax

      real dessecplt

! *- pour le calcul de la densit� equivalente
      real dassoiniteqv,dassoinit    
      
      real eo,eos,Ratm
      

      real hres(8),Wb(8),Wr(8)
      real kres(8),NCbio,saturation,qmulch

      real Ninit(5),Hinit(5),HR(5)
      real azomes,FsNH3,RsurRU,DRAT
      real QNdrp
      real esol,et,tnhc,tnrc,pluieN,irrigN,precip,precipN
  
      real cumoffrN
      real cumoffrN0,cumoffrN100
      real azorac0,azorac100,demandebrute,absodrp
      real cpluie,Chumt,Chumt0,Nhum,Nhum0,Nhumi,Nhumt,Nhumt0,Cr,Nr,Cb,Nb
! DR 28/12/06 inutile
!      real arnet,brnet
      real etm
      real precipamm,NH4initf(5),NH4init(5)

      real eaunoneffic,toteaunoneffic,raamax,raamin
      real laiTMP
!     real Chum(60),Cres(60,8),Cbio(60,8),Nbio(60,8)
!      real Chum(100),Cres(100,8),Cbio(100,8),Nbio(100,8)
! DR on mets sur la profsol      
! DR 13/11/06 on met sur 1000 comme le reste
!       real Chum(300),Cres(300,8),Cbio(300,8),Nbio(300,8)
       real Chum(1000),Cres(1000,8),Cbio(1000,8),Nbio(1000,8)
      
      real xmlch1,xmlch2,supres,stoc
      real cestout,pfz(1000),etz(1000),parapluieetz
      real totapN,Qminh,Qminr,QLES,TS(5),totapNres
      real tcult,tcultveille,tsol(0:1000),tsolveille(1000)
      real HUR(1000),hurmini(1000),HUCC(1000),HUMIN(1000)
      real AZamm(5),effamm
      
      real tauxcouv(0:731)

! ** pour thomas - 27/01/2004 - on passe azsup dans le common pour le calcul de combithomas
      real azsup
      
! * pour solnu
      real smes02,sumes0,sumes1,sumes2,sesj0,ses2j0
      real sum2 ,      esreste ,esreste2


! * pour lixiv
      real drain

! * pour offrnodu et lecsorti
      real fxa,fxn,fxt,fxw


! * variables AO/AS mais absolument n�cessaire m�me pour monoculture
      real surf(2,2)

! * tableau pour le cumul des absorptions (voir perteng.for)
      real absoTMP(5)

! * on garde les unites froids pour l'enchainement des perennes dans recup.tmp
      real cu0(2)
      real somelong0(2)
! 191206 Dr ET sAMUEL y'avait un soucis avec la valeur de vmax qu'on risque de perdre
      real vmax,cumdltaremobilN

! **************************** c
! **>> VARIABLES LOGIQUES <<** c
! **************************** c    

      logical posibsw,posibpe,repoussesemis,repousserecolte
      logical recolte1,datefin


! ***************************************** c
! **>> VARIABLES CHAINES DE CARACTERES <<** c
! ***************************************** c

! *- le num�ro de version 
      character*3 codeversion
! *- le type de simulation (culture ou feuille)
      character*12 codesimul
! *- les noms des fichiers de donn�es climatiques
      character*12 wdata1,wdata2     
! *- variable pour le stockage du mois
      character mois*3
! *- le nom du fichier usm
      character usm*7
! 27/05/05 DR on teste si c'est bon pour la generation du nom des fchiers de sortie
!      character usm*20
! *- le nom ou code du lieu des donn�es climatiques
      character wlieu*7

! DR 06/09/06 de 20 � 100
      character*19 valpar(100)

! ******************** c
! **>> CONSTANTES <<** c
! ******************** c
      integer AS,AO
      PARAMETER( AS = 1 )
      PARAMETER( AO = 2 )
      
      
! ** PARAMETRES TECHNIQUES AUTORIS�S CULTURE PURE SEULEMENT
! *- culture sous abri
      integer codabri,nouvre2,nouvre3
      real surfouvre1,surfouvre2,surfouvre3,transplastic
      real julouvre2,julouvre3

! *- code calcul automatique des fertilisations
      integer codecalferti
      
! *- apports
      integer naptot,napNtot
      real anit(731),airg(731),totir

! *- azote
      integer codetesthumN
      real dosimxN,ratiolN    
      
      
      

! ***************************************
! *                                     *
! *              COMMONS                *
! *                                     *
! ***************************************


      common /allint/ansemis,ens,codefrmur,codesensibilite,&
                   codeSIG,codeulaivernal,codoptim,dernier_n,fichist,&
                   ficdbg,ficsta,ipl,&
                   julfin,julzero,n,naptot,napNtot,napini,&
                   napNini,nbans,nbjanrec,nbjrecol,nbjrecol0,nbjsemis,&
                   nbplantes,nstoprac,numcult,&
                   numdate,numeroappN,pas,codeprofil

      common /tabloentiers/annee

      common /allchar/codeversion,usm,codesimul,mois,valpar,&
                      wdata1,wdata2,wlieu

      common /allreal/delta,doi,devjourfr,dassoiniteqv,&
                      dassoinit,Edirect,effN,eo,eos,eptcult,esz,&
                      fdens,fxa,fxn,fxt,fxw,&
                      humidite,laiTMP,originehaut,QeauFS,QeauI,&
                      raamax,raamin,ras,Ratm,rdif,resmes,rglo,&
                      saturation,surf,tcultmax,tcultmin,&
                      tustress,totir
! DR 281206 inutile arnet,brnet,
      common /tabreal/anit,airg,tauxcouv,absoTMP
     
      common /allogic/posibsw,posibpe         
 
      
      common /paramprecip/cpreciptout,dessecplt,precip,precipN

      common /per1/a,hi,ha,hpf,hurlim

      common/parsol2/Hinit,Ninit
!      ,codevolat,codNH4

      common /mineralres/kres,Wr,Wb,hres
      
      common /par_initial/NH
      
      common /parusm/ichsl,Hinitf,Ninitf,iwater,ifwater,ifwater0,&
                         culturean,codesuite,codeinitprec
     
      common/jorge/codesoltrop,effamm,&
          precipamm,NH4initf,NH4init,AZamm
     
      common/mulch/mouillmulch,Emulch,intermulch,cintermulch,&
                ruisselsurf,qmulch,&
                nappmulch,Qeau0,Nvolat      
     
     
! variables offrnodu
      common/offrnodul/cumoffrN,  &
       codefxn,cumoffrN0,cumoffrN100,azorac0,azorac100,&
       codefxt,codecombi,demandebrute,QNdrp,absodrp
     
      common /varsolnu/xmlch1,xmlch2,supres,stoc,nstoc,&
       cestout,smes02,sumes0,sumes1,sumes2,sesj0,ses2j0,&
       sum2 ,      esreste ,esreste2
     
      common /varlixiv/drain
     
      
      common /nitre/QLES,DRAT,HR,NHE,HUR,hurmini,HUCC,HUMIN,TS
      
      common /minenouv/Cres,Cbio,Nbio,Chum,cpluie,&
                          Chumt,Chumt0,Nhum,Nhum0,Nhumi,Nhumt,Nhumt0,&
                          Cr,Nr,Cb,Nb,NCbio,&
                          Qminh,Qminr

      common /entreeN/pluieN,irrigN,tnhc,tnrc
     
      common/minenew/azomes,ires,itrav,FsNH3,RsurRU
!     s    coderapport,date1rap,date2rap
      
      
      common/verti/ ruisselt,infilj,exces,&
               anox,pluieruissel,sat,ruissel,&
               codentete
     
      common /transp/tcult,tcultveille,esol,et,etm,tsol,tsolveille
      
      common /devel/tairveille
      
      common/densrac/dacouche,nouvdensrac
     
      common /sansnom/toteaunoneffic,eaunoneffic,pfz,etz,bouchon,parapluieetz,&
                totapN,maxwth,totapNres
     
      common/shutwal/rnetS,albedolai,rnet

      common /epistics/codepitaf,azsup

      common /cultabri/surfouvre1,surfouvre2,surfouvre3,transplastic,&
                       codabri,julouvre2,julouvre3,nouvre2,nouvre3

      common /calfertauto/dosimxN,ratiolN,codecalferti,codetesthumN

      common /dr071105/faucheannule
      
      common /dr050406/nbjpourdecisemis,repoussesemis&
       ,repousserecolte,nbjpourdecirecolte

      common/dr100506/recolte1,datefin

      common/dr170806/cu0,somelong0

! 191206 Dr ET sAMUEL y'avait un soucis avec la valeur de vmax qu'on risque de perdre
      common/dr1912061/vmax
! DR 29/12/06 pb d'initialisation signal� par samuel
      common/dr1912062/cumdltaremobilN(2)
! dr je mets un code mauvais format pour les climats      
      integer codemauvais
      common/DRclimat260609/codemauvais
      