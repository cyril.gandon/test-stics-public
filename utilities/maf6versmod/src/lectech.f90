      SUBROUTINE lectech

!*********************************************************************
!     lecture et initialisation des parametres culturaux
!             fichier *.tec
!     version 4.0 le 20/11/98
!*********************************************************************


! ** les COMMONS
      include 'common.inc'
      include 'compartec.inc'
      include 'comparsol.inc'
      include 'comparplt.inc'
      include 'comusmplt.inc'
!      include 'message.inc'
!      include 'format.inc'
      include 'comtransit.inc'

      common /fauches/nbfaucheoui,nbfauchenon,nbfaucheauto,nbfauchejour,nbfauchedegre

! ** les VARIABLES LOCALES
      character*60 para
      character*20 nompara,nomparapre
!      integer nbcoupe2, nbcoupe3
      integer i !,is
!     integer nlig
      character*15 ressuite2
      integer nbcar
      integer compteblanc

!---------------------------------------------------------------------

!      open (12,file=ftec(ipl),status='old',err=50)

!      write(*,*)'debug#reading of tec file'



! *************************************************************
! *- modif Bruno - 20/05/99
! *- test du format du fichier technique sur la 1�re ligne
! *- forme tempotec.sti  ou Winstics (*.tec)
! *-
      read(12,'(a20)') nomparapre
      
! ** si nomparapre = 'nbinterventions' alors, on sait que le fichier ftec(ipl) est
! *- d�j� sous la forme tempotec.sti. S'il ne l'est pas, alors, il faut le transformer.

      if(nomparapre.eq.'nbinterventions') then
! *- a) forme tempotec.sti
        close(12)
        open (36,file=ftec(ipl),status='old',err=9999)
      else
! *- b) forme Winstics (*.tec)
! *- transformation du fichier en un fichier transitoire  TEMPO.STI
! *-
        open (36,file='tempotec.sti',status='unknown')
10      read(12,'(69x,a20,a60)',end=15)nompara,para
          if(nomparapre.eq.'nbinterventions') then
! *- 08/07/99 - domi --> sous unix '' non correct
            nomparapre=' '
            goto 10
          endif
          if(nompara.ne.'                   ') then
            write(36,'(a20)') nompara
            write(36,'(a60)') para
            nomparapre=nompara
          endif
        goto 10
15      continue
! *- selon les compilateurs, la derni�re ligne sort avant traitement.
        if(nompara.ne.'                   ') then
            write(36,'(a20)') nompara
            write(36,'(a60)') para
            nomparapre=nompara
        endif
        close(36)
! *- ouverture du fichier TEMPOTEC.STI pour lecture
        open (36,file='tempotec.sti',status='old')
      endif
! *-
! *- fin modif Bruno - 20/05/99
! ****************************************************************

! *****************************************************************
! *- on lit les variables dans le fichier tempotec.sti
! *-
      read (36,*)
      read (36,*) napS(ipl)
! *- lecture des op�rations de travail du sol
      if (napS(ipl).gt.0) then
        do 42 i=1,napS(ipl)
          read(36,*)
          read(36,*,err=50) jultrav(ipl,i),profres(ipl,i),&
             proftrav(ipl,i),coderes(ipl,i),qres(ipl,i),Crespc(ipl,i),&
             CsurNres(ipl,i),Nminres(ipl,i),eaures(ipl,i)
42      continue
      endif
      read (36,*)
      read (36,*,err=50) iplt(ipl)
      read (36,*)
      read (36,*,err=50) profsem(ipl)
      read (36,*)
      read (36,*,err=50) densite(ipl)
      read (36,*)
      read (36,*,err=50) variete(ipl)
      read (36,*)
      read (36,*,err=50) codetradtec(ipl)
      read (36,*)
      read (36,*,err=50) interrang(ipl)
      read (36,*)
      read (36,*,err=50) orientrang(ipl)
      read (36,*)
      read (36,*,err=50) codestade(ipl)
      read (36,*)
      read (36,*,err=50)ilev(ipl)
      read (36,*)
      read (36,*,err=50)iamf(ipl)
      read (36,*)
      read (36,*,err=50)ilax(ipl)
      read (36,*)
      read (36,*,err=50)isen(ipl)
      read (36,*)
      read (36,*,err=50)ilan(ipl)
      read (36,*)
      read (36,*,err=50)iflo(ipl)
      read (36,*)
      read (36,*,err=50)idrp(ipl)
      read (36,*)
      read (36,*,err=50)imat(ipl)
      read (36,*)
      read (36,*,err=50)irec(ipl)
      read (36,*)
      read (36,*,err=50)irecbutoir(ipl)
      read (36,*)
      read (36,*,err=50)effirr(ipl)
      read (36,*)
      read (36,*,err=50)nap(ipl)


! *- lecture des irrigations
      if (nap(ipl).gt.0) then
        do 21 i=1,nap(ipl)
          read(36,*)
          read(36,*,err=50) julapI(ipl,i),doseI(ipl,i)
21      continue
      endif

      read (36,*)
      read (36,*,err=50)codlocirrig(ipl)
      read (36,*)
      read (36,*,err=50)locirrig(ipl)
      read (36,*)
      read (36,*,err=50)codecalirrig(ipl)
      read (36,*)
      read (36,*,err=50)ratiol(ipl)
      read (36,*)
      read (36,*,err=50)dosimx(ipl)
      read (36,*)
      read (36,*,err=50)profmes(ipl)
      read (36,*)
      read (36,*,err=50)engrais(ipl)
      read (36,*)
      read (36,*,err=50)concirr(ipl)
      read (36,*)
      read (36,*,err=50)napN(ipl)

! *- lecture des fertilisations
      if (napN(ipl).gt.0) then
        do 20 i=1,napN(ipl)
          read(36,*)
          if (codedateappN(1).ne.1) then
            read(36,*,err=50) julapN(ipl,i),doseN(ipl,i)
          else
            read(36,*,err=50) upvttapN(ipl,i),doseN(ipl,i)
          endif
20      continue
      endif

      read (36,*)
      read (36,*,err=50)codlocferti(ipl)
      read (36,*)
      read (36,*,err=50)locferti(ipl)
      read (36,*)
      read (36,*,err=50)codecalferti
      read (36,*)
      read (36,*,err=50)ratiolN
      read (36,*)
      read (36,*,err=50)dosimxN
      read (36,*)
      read (36,*,err=50) codetesthumN
      read (36,*)
      read (36,'(a15)',err=50) ressuite2
!  DR 28/01/08 on ecrit la chaine en elevant les blancs sinon pb
      nbcar=compteblanc(ressuite2)
      ressuite(ipl)=ressuite2(nbcar+1:nbcar+7)
      read (36,*)
      read (36,*,err=50) codcueille(ipl)
      read (36,*)
      read (36,*,err=50) nbcueille(ipl)
      read (36,*)
      read (36,*,err=50) cadencerec(ipl)
      read (36,*)
      read (36,*,err=50) codrecolte(ipl)
      read (36,*)
      read (36,*,err=50) codeaumin(ipl)
      read (36,*)
      read (36,*,err=50) h2ograinmin(ipl)
      read (36,*)
      read (36,*,err=50) h2ograinmax(ipl)
      read (36,*)
      read (36,*,err=50) sucrerec(ipl)
      read (36,*)
      read (36,*,err=50) CNgrainrec(ipl)
      read (36,*)
      read (36,*,err=50) huilerec(ipl)
      read (36,*)
      read (36,*,err=50)codefauche(ipl)
      read (36,*)
      read (36,*,err=50)codemodfauche(ipl)
      read (36,*)

      if(codemodfauche(ipl).eq.1) then
        lecfauche(ipl)=.false.
      else
        lecfauche(ipl)=.true.
      endif

      read (36,*,err=50)hautcoupedefaut(ipl)
      read (36,*)
      read (36,*,err=50)stadecoupedf(ipl)
      read (36,*)
      read (36,*,err=50) nbcoupe2

!      if(codemodfauche(ipl).eq.2) then
! *- on lit des jours
        do 81 i=1,nbcoupe2
          read(36,*)
          read(36,*,err=50)julfauche(ipl,i),hautcoupe(ipl,i),&
            lairesiduel(ipl,i),msresiduel(ipl,i),anitcoupe(ipl,i)
! --         write(ficdbg,'(i3,f16.12)')i,lairesiduel(ipl,i)
81      continue
!        nbcoupe2(ipl)=nbcoupe2
!      else
!        do 85 i=1,nbcoupe2
!          read(36,*)
!          read(36,*)
!85      continue
!      endif

      read (36,*)
      read (36,*,err=50) nbcoupe3
!      if(codemodfauche(ipl).eq.3) then
! *- on lit des degr�s.jours
        do 83 i=1,nbcoupe3
          read(36,*)
          read(36,*,err=50) tempfauche(ipl,i),hautcoupe2(ipl,i),lairesiduel2(ipl,i),msresiduel2(ipl,i),anitcoupe2(ipl,i)
83      continue
!        nbcoupe3(ipl)=nbcoupe3
!      else
!        do 84 i=1,nbcoupe3
!          read(36,*)
!          read(36,*)
!84      continue
!      endif

! *- domi - 4/02/98 - mscoupemini deplace
      read (36,*)
      read (36,*,err=50) mscoupemini(ipl)
! ** version <5.0
! --     read (36,*)
! --     read (36,*,err=50) codeffeuil(ipl)
! --     read (36,*)
! --     read (36,*,err=50) effeuil(ipl)
      read (36,*)
      read (36,*,err=50) codepaillage(ipl)
      read (36,*)
      read (36,*,err=50) qmulch0
      read (36,*)
      read (36,*,err=50) julappmulch
      read (36,*)
      read (36,*,err=50) typemulch
      read (36,*)
      read (36,*,err=50) couvermulch
      read (36,*)
! ** domi 21/10/2004 maintenant on lit les parametres mulch dans
!   param.par et on reaffecte albedomulch sous condition
      read (36,*,err=50)albedomulchtec
      read (36,*)

! -- d�plac� -- read (36,'(a7)',err=50) ressuite(ipl)

! ** version 5.0
      read (36,*,err=50) codrognage(ipl)
      read (36,*)
      read (36,*,err=50) largrogne(ipl)
      read (36,*)
      read (36,*,err=50) hautrogne(ipl)
      read (36,*)
      read (36,*,err=50) biorognem(ipl)
      read (36,*)
      read (36,*,err=50) codcalrogne(ipl)
      read (36,*)
      read (36,*,err=50) julrogne(ipl)
      read (36,*)
      read (36,*,err=50) margerogne(ipl)
      read (36,*)
      read (36,*,err=50) codeclaircie(ipl)
      read (36,*)
      read (36,*,err=50) juleclair(ipl)
      read (36,*)
      read (36,*,err=50) nbinfloecl(ipl)
      read (36,*)
      read (36,*,err=50) codeffeuil(ipl)
      read (36,*)
      read (36,*,err=50) codhauteff(ipl)
      read (36,*)
      read (36,*,err=50) codcaleffeuil(ipl)
      read (36,*)
      read (36,*,err=50) laidebeff(ipl)
      read (36,*)
      read (36,*,err=50) effeuil(ipl)
      read (36,*)
      read (36,*,err=50) juleffeuil(ipl)
      read (36,*)
      read (36,*,err=50) laieffeuil(ipl)  
      read (36,*)
      read (36,*,err=50) codetaille(ipl)
      read (36,*)
      read (36,*,err=50) jultaille(ipl)  
      read (36,*)
      read (36,*,err=50) codabri
      read (36,*)
      read (36,*,err=50) transplastic
      read (36,*)
      read (36,*,err=50) surfouvre1
      read (36,*)
      read (36,*,err=50) julouvre2
      read (36,*)
      read (36,*,err=50) surfouvre2
      read (36,*)
      read (36,*,err=50) julouvre3
      read (36,*)
      read (36,*,err=50) surfouvre3
! DR 23/03/06 ajout d'un code pour Inaki pour la lecture de
! hautmaxtec et largtec dans le fichier technique
!      if(codelectec.eq.1)then
!      write(*,*)'avant gnack'

!      if(codeplante(ipl).eq.'vig')then
!          codepalissage(ipl)=2
      read (36,*,end=200)
            read (36,*,err=200) hautmaxtec(ipl)
      read (36,*,end=200)
            read (36,*,err=200) largtec(ipl)
!      endif

      close(12)
      goto 61       
50    write(*,*)'error#',mes143
      codemauvais=1
!      stop
      goto 61

9999    write(*,*)'error#',mes1439,ftec(ipl)
      codemauvais=1
!      stop
61    continue


200   close(36)
    
      return          
      end
