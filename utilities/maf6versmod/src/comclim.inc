! include comclim.inc
!********************************************
! include des parametres climatiques
!********************************************
      character*2  nometp
      
      real ttrr(731),ttmin(731),ttmax(731),ttrg(731),ttetp(731)
      real ttpm(731),ttvent(731),co2(731)
      real tetp(731),trr(731),tmoy(0:731),tmin(731),tmax(731),trg(731)
      real alphapt,latitude,parsurrg,tpm(731),tvent(731)
      real trrext(731),tmoyext(0:731),tminext(731),tmaxext(731)
      real trgext(731),tpmext(731)
      
! ** micro-climat ??
      integer codaltitude,codadret,codemicheur
      integer nitetcult(0:731)
      
      real tutilrnet,difftcult,daylen,humimoy,humair,phoi      
      real etpp(0:731)
      real aks,bks,cvent,coefrnet,phiv0
      real patm,corecTrosee
      
      common/microclimatint/codaltitude,codadret,codemicheur,nitetcult
      
      common/microclimatreal/tutilrnet,difftcult,daylen,humimoy,&
       humair,phoi,etpp

! ** climat meteo + ajout patm le 08/04/05
      common/climatpar/alphapt,latitude,parsurrg,nometp
      common/climat1/ttmin,ttmax,ttrg,ttrr,ttetp,ttpm,ttvent,co2
      common/climat2/tmin,tmax,trg,trr,tetp,tpm,tvent,tmoy
      common/altiplano/patm,corecTrosee
    
! ** climat sous abri
      common /climab/trrext,tmoyext,tminext,tmaxext,trgext,tpmext
      common /climab2/aks,bks,cvent,coefrnet,phiv0

! * dr 09/01/06 rajout de variables pour rapport
      real Ctculttout,Ctairtout,somdifftculttair,somtroseecult,somtroseeair
      real Ctetptout,Cetmtout,Crgtout

      common/dr090106/Ctculttout,Ctairtout,somdifftculttair,somtroseecult &
        ,somtroseeair,Ctetptout,Cetmtout,Crgtout

! * DR 08/09/06 
      real tncultmat,      amptcultmat
      integer      dureelaxrec,nbjechaudage
      common/dr080906/tncultmat,amptcultmat,dureelaxrec,nbjechaudage

! * DR 30/03/07 on garde cette valeur dont on  abesoin dans humheure
      real trosemaxveille
      common/dr300307/trosemaxveille

! DR 22062011 
      integer codeetp
      common/dr_clim_22062011/codeetp