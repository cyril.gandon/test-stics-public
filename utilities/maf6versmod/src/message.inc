! *  include message.inc  * c
! ************************* c

! ** derniere modif DR 10/05/05

! ** pour la version unix on a tout mis dans un fichier.for 
! *- dernier message ajout� : 511


! ** develop et debour
      character*200 mes49,mes48
      common /msg1/mes49,mes48

! ** main et lectures
! domi 12/08/05 nettoyage pour tradiuction anglaise
      character*50 mes157
      character*100 mes433,mes434
      common /mgs2/mes157 ,mes433,mes434
      
!   apores
! --------      
! *- inutilis�
      character*60 mes10
      common /messapores/mes10


! grain
! -----
      character*40 mes51,mes52
      character*60 mes53
      common /messgrain/mes51,mes52,mes53

! inicoupe
! --------
      character*50 mes75
      common/messinicoupe/mes75

! initial
! NB le 23/05/05 + mes520

! -------
      character*50 mes80,mes81,mes82,mes83,mes84,mes85,mes86
      character*50 mes87,mes88,mes89,mes181,mes182,mes171
      character*100 mes198,mes199,mes334
      character*150 mes172,mes173,mes180,mes196,mes333
      character*60 mes191,mes192,mes193,mes194,mes195,mes197,mes330
      character*60 mes335,mes336,mes337,mes339,mes340
      character*200 mes338,mes341,mes342,mes520
      common/messinitial/mes80,mes81,mes180,mes82,mes83,mes84,mes85,&
         mes86,mes87,mes88,mes89,mes181,mes171,mes172,mes173,mes191,&
         mes192,mes193,mes194,mes195,mes196,mes197,mes198,mes199, &
         mes330,mes333,mes334,mes335,mes336,mes337,mes338,mes339, &
         mes340,mes341,mes342,mes182,mes520
! irrig
! -----
      character*70 mes90
      common /messirrig/ mes90

! rajout Domi 12/08/05 nettoyage message pour traduction anglaise
! fertil
! -----
      character*70 mes91
      common /messfertil/ mes91

! leclai
! ------
      character*50 mes100,mes101
      common /messleclai/ mes100,mes101
! lecoptim
! --------
      character*50 mes102,mes103
      common /messoptim/ mes102,mes103

! lecparam
! --------
      character*50 mes104,mes105,mes106,mes361,mes437
      character*100 mes360
      common /messlecparam/ mes104,mes105,mes106,mes360,mes361,mes437


! ** DR et ML le 23/03/04: creation d un fichier temporaire
! *- de parametres en attente d etre rediriges definitivement 
! *- vers leurs fichiers respectifs PARAMV6.PAR
! *- lecparamv6
! *------------
      character*50 mes105v6,mes106v6
      character*400 mes107v6
      common /messlecparamv6/ mes105v6,mes106v6,mes107v6

! lecplant  NB le 08/05/02
! --------
      character*50 mes107,mes108,mes109,mes113
      character*50 mes114,mes115,mes116,mes117,mes118
      character*50 mes121,mes122,mes123,mes124,mes125,mes126,mes127
      character*50 mes128,mes129,mes132,mes133,mes134,mes373,mes374
      character*100 mes370,mes371,mes372,mes232,mes130
      character*200 mes110,mes111,mes112,mes119,mes120,mes131
      character*150 mes377,mes438,mes378,mes375
      character*300 mes376

      common /messlecplant/ mes107,mes108,mes109,mes110,mes111,mes112,&
       mes113,mes114,mes115,mes116,mes117,mes118,mes119,mes120,mes121 &
       ,mes122,mes123,mes124,mes125,mes126,mes127,mes128,mes129&
       ,mes130,mes131,mes132,mes133,mes134,mes232,mes378,mes370&
       ,mes371,mes372,mes373,mes374,mes375,mes376,mes377,mes438

! lecsol   NB le 8/1/02
! ------
      character*50 mes135,mes137,mes138,mes211,mes215
      character*90 mes136,mes210,mes212,mes213,mes214
        character*200 mes216
      common /messlecsol/mes135,mes136,mes137,mes138,mes210,mes211   &
            ,mes212,mes213,mes214,mes215,mes216

! lecstat
! -------
      character*50 mes139
        character*150 mes442
      common /messlecstat/mes139,mes442

! lectech     le 8/1/02
! -------
      character*50 mes140,mes141,mes142,mes143,mes145
      character*50 mes148,mes149,mes150,mes151,mes170,mes380
      character*50 mes381,mes382,mes383,mes384,mes385,mes386,mes387
      character*50 mes388,mes389,mes390,mes391,mes392,mes393
      character*50 mes511
      character*90 mes144,mes1439
      character*200 mes395,mes396,mes397,mes399,mes147
      character*180 mes398
      
      character*90 mes146,mes394,mes152
      common /messlectech/mes140,mes141,mes142,mes143,mes144,mes145, &
         mes146,mes147,mes148,mes149,mes150,mes151,mes152,mes170, &
         mes380,mes381,mes382,mes383,mes384,mes385,mes386,mes387,&
         mes388,mes389,mes390,mes391,mes392,mes393,mes394,mes395, &
         mes511,mes396,mes397,mes398,mes399,mes1439

! lecusm
! -------
      character*50 mes156,mes158
      common /msgLecusm/mes156,mes158

! shutwall
! --------
      character*100 mes159,mes161,mes162
      character*80 mes160
      common/messshutwall/mes159,mes160,mes161,mes162

! solnu
! -----
      character*50 mes163,mes164
      common /messsolnu/mes163,mes164

! lecprofi
! --------
      character*90 mes165
      common /messlecprofi/mes165

! eauplant
! --------
      character*90 mes200
      common /messeauplant/mes200



! eauqual
! -------
      character*60 mes311,mes313
      character*300 mes312,mes314
      common /mess0205/mes311,mes312,mes313,mes314

! effeuil
!--------
      character*100 mes320,mes321
      common/messeffeuil/mes320,mes321

! fruit
!--------
      character*200 mes350
      character*100 mes351
      common/messfruit/mes350,mes351

! levee
!--------
      character*50 mes400
      common/messlevee/mes400

! recolte
!--------
      character*50 mes401,mes402
      common/messrecolte/mes401,mes402

! repartir
!--------
      character*100 mes403,mes404
      common/messrecolte/mes403,mes404

! bilan
!--------
      character*50 mes410,mes411,mes412,mes414,mes415,mes416
      character*50 mes417,mes418,mes419,mes420,mes421,mes422
      common/messbilan/mes410,mes411,mes412,mes414,mes415,&
        mes416,mes417,mes418,mes419,mes420,mes421,mes422

! domi 12/08/05 nettoyage pour traduction anglaise
! *- testtransrad
! *----------------
      character*350 mes430
        character*60 mes432,mes435,mes436
      common /messtransrad/mes430,mes432,mes435,mes436

! *- bfroid
! *----------
      character*100 mes431
      common /messbfroid/mes431
! * domi 29/09/05 dans lecplant
!*-----------------------------
      character*100 mes439,mes440
        common /mess290905/mes439,mes440

! DR 04/11/05 Dominver
! * ---------------------------------
      character*150 mes441
        common /mess041105/mes441


! dr 18/04/06
      character*120 mes443,mes444,mes446
      character*100 mes445      
      character*60 mes447
      common/mess180406/mes443,mes444,mes445,mes446,mes447


