! *******************************************************************************
! >>> SUBROUTINE LECSTAT <<<
! * lecture des donnees climatiques dans le fichier temporaire stat.dat 
! * lecture pour les cultures sur 1 annee 
! * version 3.3 18/03/98
! * derni�re modif NB le 26/3/98 pour vent et pm
!********************************************************************************

      SUBROUTINE lecstat(nbjourclim)

      include 'common.inc'
      include 'comclim.inc'
!      include 'message.inc'
      
      integer i  !,jul,retour
      character*7 station
      integer imois(366),ijour(366),ijul(366)
      common/climat/imois,ijour,ijul,station
      character toto*7
      character nomannee*4
      common/recupannee/nomannee
      integer nbjourclim
      ficsta=12

      codemauvais=0
      read(ficsta,*,err=250,end=80) nometp,alphapt,parsurrg,latitude

      read(ficsta,*)toto,nomannee
      backspace(ficsta)
      nbjourclim=0
      do 150 i=1,366 
! DR 06/04/2011 je suis oblig� de lire avec un formatcar il y a encore des vieux fichiers sans vent et hum
        read (ficsta,4201,err=250,end=80)station,annee(i),imois(i),&
!        read (ficsta,*,err=250,end=80)station,annee(i),imois(i),
          ijour(i),ijul(i),ttmin(i),ttmax(i),ttrg(i),ttetp(i),ttrr(i),ttvent(i),ttpm(i)
!     s     ttvent(i),ttpm(i),co2(i)
      if(co2(i).eq.0)then
          co2(i)=330
      endif
        nbjourclim=nbjourclim+1
! *- PB - 11/01/05 - on enl�ve les formats de lecture
150   continue 
! **  pour la deuxi�me ann�e �ventuelle
80    continue 

      do 151 i=1,366 
       if(ttvent(i).eq.0.and.ttpm(i).eq.0)ttvent(i)=-999.9
       if(ttpm(i).eq.0)ttpm(i)=-999.9
 151  continue

      goto 200

250   write (fichist,*)'error#error in the climatic file :',station
!      write(*,*)mes139
!270   format(5(/))
      codemauvais=1
!      stop

200   continue

! dr 30/10/07 je recupere annee pour nommer le fichier
        
4201    format(a7,1x,i4,1x,i2,1x,i2,1x,i3,1x,8(f7.1))
   
      return
      end
