      program misoform70 
!*********************************************************************
!     procedure de mise au format versions 6 ----->version 7.0 javastics
!     procedure pour tous les anciens fichiers d'un repertoire
!     *.tec, param.sol
!      pas de mise au format du fichier param.par
!*********************************************************************
!
!      INCLUDE 'flib.fi'
!      INCLUDE 'flib.fd'
      include 'common.inc' 
      include 'comform.inc'

      character*255 ancienchemin(9000), nouveauchemin(9000)      
!      character*255 dircourant
!      character*255 cheminexp,cheminutilold,
      character*255 cheminutilnew
      character*255 file_tec_mauvais(500),file_plt_mauvais(200)
      character*255 file_sol_mauvais(100)
      character*255 file_clim_mauvais(1000)
      character*255 file_obs_mauvais(500),file_par_mauvais(10)
!      character*140 commande,commande2
!      integer length
!      logical*4 result,result2
!      character* 12 FILNAME,ficpar
      character* 12 ficpar
      character*20 fictec(9000) !,listdir(20)
      character*20 ficplt(100),ficsol(100)
      character*20 ficobs(9000),ficcli(9000),newficcli(9000)
!      character*206 ligneobs
      character*20 toto,tata
!      CHARACTER  permit
!      real reel
      integer ll,ii,longmeteo,codemauvaisclim,codemauvaisplt
      integer codemauvaistec,codemauvaissol,codemauvaisobs
      integer codemauvaispar
      character*255 newtec,newsol,newobs,newplt,newcli,newsta,newpar
      character*255 oldtec,oldsol,oldobs,oldplt,oldcli,oldpar
!         common/com/para
      common/newfiles/newtec,newplt,newobs,newsol,newcli,newsta,newpar
      common/oldfiles/oldplt
      character nomannee*4
      character rougedeb*30,colorfin*10
      common/recupannee/nomannee
      integer ficclimat
      rougedeb='<span style="color:red;">'
      colorfin='</span>'
!
! je lis le nom des repertoires � traiter et le nom des fichiers dans le repertoire principal de javastics
!
! ouverture du fichier mouchard
      ficclimat=15
      open(ficclimat,file='nomclimat.txt',recl=400)

      fichist=14
!      open (fichist,file='history.sti',status='unknown',recl=180)
      write(*,*)'info#<u><b>Start translation maj6vers7</b></u>'
      write(*,*)'info#translation files version 6.* to  moduloStics for javastics'
      write(*,*)'info# see the file : log\translate-tool-log.txt,  for :'
      write(*,*)'info#- the list of the converted files and parameters values '
      write(*,*)'info#- detailled errors occured during the process'
      write(*,*)'info#',trim(rougedeb),' It is important to check this file before running Stics',colorfin

! on traite les fichiers type par type
! dr 13/06/07 on lit d'abord le paramv6.par pour avoir les valeurs des params � switcher
      write(*,*) 'debug#reading paramv6.par'
      call lecparamv6
! le fichiers param.par 
!***********************      
! on transforme un param.par utilisateur 
!  et on fournit un param.par XML de reference
!      write(*,*) //,'je lis votre param.par'
      nbpar=0
! pour param.par y'a un seul fichier
      i=1
      open(1,file='maj_par.txt')
      read(1,'(a255)',end=600)ancienchemin(i)
      read(1,'(a255)')nouveauchemin(i)
      read(1,*)ficpar
!      write(*,*)ficpar
      nbpar=1
      codemauvais=0
      codemauvaispar=0
600      close(1)
!      nbpar=0
      if(nbpar.ne.0)then
! dr 01/02/08 faudra voir ca si y'a pas de param � mettre au format ca pose pb
!600   write(*,*)'il ny a pas de fichier param.par on prend le 6.2 standard'
         oldpar=trim(ancienchemin(i))//'\'//ficpar
!   nom complet du fichier tec ecrit
! dr 10/08/2012 chemin integral         cheminutilnew=trim(dircourant)//'\'//'tmp'
         cheminutilnew=trim(nouveauchemin(i))
!      newpar=trim(cheminutilnew)//'\'//trim(nouveauchemin(i))//'\'
!     s//ficpar
         newpar=trim(cheminutilnew)//'\'//ficpar
         open (12,file=oldpar,status='old')
         call lecparam
         if(codemauvais.eq.0)then
             close(12)
             open (36,file=newpar,recl=150,status='unknown')
             call ecrinouvparampar
             call mouchard
             close(36)
             write(*,*)'debug#the file ',trim(oldpar),' had been translated in the file ',trim(newpar)
          else
                write(*,*)'error#the file ',trim(oldpar),' contains an error so it has not been translated '
                  codemauvaispar=codemauvaispar+1
!              commande = 'del "'//trim(newplt)
!                  commande2=trim(commande)//' "'
!                  result = SYSTEMQQ(commande2)
              file_par_mauvais(codemauvaispar)=oldpar
         endif
      endif
!******** c'est fini pour param.par

! les fichiers plantes 
!*********************      
      write(*,*) 'info#<u><b>reading of plant files</u></b>'
      ipl=1
      codemauvaisplt=0
      open(1,file='maj_plt.txt')
      nbplt=0
      do i= 1,100
      read(1,'(a255)',end=100)ancienchemin(i)
      read(1,'(a255)')nouveauchemin(i)
      nbplt=nbplt+1
      read(1,*)ficplt(nbplt)
      enddo
  100 close(1)
      if(nbplt.eq.0)then
            write(*,*)'info#no plant files to translate'
      else
         write(*,*)'info#new plant files are provided in the plant folder'
         write(*,*)'info#your plant files will be translated in the tmp folder'
         write(*,*)'info#',trim(rougedeb),'To use them you should copy in the plant folder',colorfin
        do 101 i=1,nbplt
            oldplt=trim(ancienchemin(i))//'\'//ficplt(i)
            write(*,*)'debug# start processing file : ',trim(oldplt)
! 10/08/2012            cheminutilnew=trim(dircourant)//'\'//'tmp'
          cheminutilnew=trim(nouveauchemin(i))
          newplt=trim(cheminutilnew)//'\'//ficplt(i)
!         lecture du fichier plt      
!---------------------------------
            if(ficplt(i).eq.'') goto 102
            open (12,file=oldplt,status='old')
            codemauvais=0
            call lecplant
            close(12)
            if(codemauvais.eq.0)then
                open (12,file=oldplt,status='old')
                call ecrinouvplt
                call mouchplt
                write(*,*)'debug#the file ',trim(ficplt(i)),' had been translated in the file ',trim(newplt)
            else
                write(*,*)"error#le fichier ",trim(oldplt)," contains an error so it has not been translated"
                  codemauvaisplt=codemauvaisplt+1
!                  commande = 'del "'//trim(newplt)
!                  commande2=trim(commande)//' "'
!                  result = SYSTEMQQ(commande2)
               file_plt_mauvais(codemauvaisplt)=oldplt
               write(*,*)"error#You can verify in your file 6.9 if the number of varieties in : &
              &'TV: les diff�rentes vari�t�s' is the number of varieties in the file"
               write(*,*)'error#if it is true you can verify that you have 22 before the name of the varieties'
               write(*,*)'error#for example :                    M01: pois                               22'
            endif
            close(12)
 102     continue
 101    continue
      endif
      if(codemauvaisplt.gt.0)then
            write(*,*)'error#Attention there is  ',codemauvaisplt,'  bad plant files '
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      else
          write(*,*)'info#',nbplt-codemauvaisplt,'plt files had been translated'
      endif
! //// c'est finit pour les plantes ////
! **************************************

! les fichiers techniques 
!*********************      
      write(*,*) 'info#<u><b>reading technical files</u></b>'
      open(1,file='maj_tec.txt')
      nbtec=0
      codemauvaistec=0
      do i= 1,9000
      read(1,'(a255)',end=200)ancienchemin(i)
      read(1,'(a255)')nouveauchemin(i)
      nbtec=nbtec+1
      read(1,*)fictec(nbtec)
      enddo
  200 close(1)
      if(nbtec.eq.0)then
            write(*,*)'info#no tec files to translate'
      else
!     les noms de tec sont stockes dans le tableau ftec(i)
        do 201 i=1,nbtec
!   nom complet du fichier tec de lecture
          oldtec=trim(ancienchemin(i))//'\'//fictec(i)
          write(*,*)'debug# start processing file : ',trim(oldtec)
!   nom complet du fichier tec ecrit
! 10/08/2012          cheminutilnew=trim(dircourant)//'\'//trim(nouveauchemin(i))
          cheminutilnew=trim(nouveauchemin(i))
          newtec=trim(cheminutilnew)//'\'//fictec(i)
!         lecture d'un fichier .tec      
!---------------------------------------------------------------------
            if(fictec(i).eq.'') goto 202
          close(12)
            open (12,file=oldtec,status='old')
            codemauvais=0
            call lectech
            call mouchtec
            if(codemauvais.eq.0)then
                call ecrinouvtec
                write(*,*)'debug#the file ',trim(fictec(i)),' has been translated in the file ',trim(newtec)
            else
                write(*,*)'error#the file ',trim(oldtec),' contains an error so it has not been translated '
                codemauvaistec=codemauvaistec+1
                file_tec_mauvais(codemauvaistec)=oldtec
!              pause
            endif
            close(12)
 202            continue
  201  continue
      endif
      if(codemauvaistec.gt.0)then
            write(*,*)'error#Attention there is  ',codemauvaistec,'  bad tec files '
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      else
         write(*,*)'info#',nbtec-codemauvaistec,'tec files had been translated'
      endif
! //// c'est finit pour les tech ////
! **************************************

! les fichiers sols 
!************************      
      write(*,*) 'info#<u><b>reading soil files</u></b>'
      open(1,file='maj_sol.txt')
      nbsol=0
      codemauvaissol=0
      do i= 1,100
      read(1,'(a255)',end=300)ancienchemin(i)
      read(1,'(a255)')nouveauchemin(i)
      nbsol=nbsol+1
      read(1,*)ficsol(nbsol)
      enddo
  300 close(1)
      if(nbsol.eq.0)then
            write(*,*)'info#no soil files'
      else
!     les noms de tec sont stockes dans le tableau ftec(i)
        do 301 i=1,nbsol
! on initialise le nb de sols potentiels � mettre au format 
            ichsl=900
!   nom complet du fichier tec de lecture
          oldsol=trim(ancienchemin(i))//'\'//ficsol(i)
            write(*,*)'debug# start processing file : ',trim(oldsol)
!   nom complet du fichier tec ecrit
! 10/08/2012            cheminutilnew=trim(dircourant)//'\'//trim(nouveauchemin(i))
            cheminutilnew=trim(nouveauchemin(i))
          newsol=trim(cheminutilnew)//'\'//ficsol(i)
!---------------------------------------------------------------------
            if(ficsol(i).eq.'') goto 302

              open (12,file=oldsol,status='old')
               open(36,file=newsol,recl=300)
            codemauvaissol=0

            call lecsol
            if(codemauvais.eq.0)then
!                call ecrinouvsol
!              call mouchsol
                write(*,*)'debug#the file : ',trim(oldsol),' has been translated in the file ',trim(newsol)
            else
                write(*,*)'error#the file ',trim(oldsol),'  contains an error so it has not been translated'
              codemauvaissol=codemauvaissol+1
              file_sol_mauvais(codemauvaissol)=oldsol
            endif
            close(12)
            close(36)
 302            continue
  301   continue
         write(*,*)'info#',nbsol-codemauvaissol,'soil files had been translated'
      endif
! //// c'est finit pour les param.sol ////
! **************************************

! les fichiers observes 
!*********************      
      write(*,*) 'info#<u><b>reading observed files</u></b>'
      open(1,file='maj_obs.txt')
      nbobs=0
      codemauvaisobs=0
      do i= 1,9000
      read(1,'(a255)',end=400)ancienchemin(i)
      read(1,'(a255)')nouveauchemin(i)
      nbobs=nbobs+1
      read(1,*)ficobs(nbobs)
      enddo
  400      close(1)
      if(nbobs.eq.0)then
            write(*,*)'info#no  obs files'
      else
!     les noms de tec sont stockes dans le tableau ftec(i)
        do 401 i=1,nbobs
!   nom complet du fichier tec de lecture
          oldobs=trim(ancienchemin(i))//'\'//ficobs(i)
            write(*,*)'debug# start processing file : ',trim(oldobs)

!   nom complet du fichier tec ecrit
! 10/08/2012      cheminutilnew=trim(dircourant)//'\'//trim(nouveauchemin(i))
            cheminutilnew=trim(nouveauchemin(i))
          newobs=trim(cheminutilnew)//'\'//ficobs(i)
!         lecture du fichier plt      
!---------------------------------
            if(ficobs(i).eq.'') goto 402
             open (12,file=oldobs,recl=374,status='old')
            codemauvais=0
            if(codemauvais.eq.0)then
                call ecrinouvobs
                 write(*,*)'debug#the file : '//trim(oldobs)//' has been translated'

            else
              codemauvaisobs=codemauvaisobs+1
                write(*,*)'error#the file ',trim(oldobs),' contains an error so it has not been translated'
              file_obs_mauvais(codemauvaisobs)=oldobs
            endif

            close(12)
 402            continue
 401    continue
            if(codemauvaisobs.gt.0)then
               write(*,*)'error#Attention there is  ',codemauvaisobs,'  bad observed files '
               write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
            else
               write(*,*)'info#',nbobs-codemauvaisobs,'obs files had been translated'
            endif
       endif
! //// c'est finit pour les fichiers obs ////
! **************************************
!      les usm sont ger�es par Mat
!     on traite les fichiers climatiques 
!  pour le climat on ajoute la colonne co2 � la fin 
! on enleve l'entete de colonne
! on genere les fichiers station.txt
!*********************      
      write(*,*) 'info#<u><b>reading weather files</u></b>'
      open(1,file='maj_clim.txt')
      nbcli=0
      codemauvaisclim=0
      do i= 1,9000
      read(1,'(a255)',end=500)ancienchemin(i)
      read(1,'(a255)')nouveauchemin(i)
      nbcli=nbcli+1
      read(1,*)ficcli(nbcli)
!      write(*,*)ficcli(nbcli)
      enddo
  500 close(1)
      if(nbcli.eq.0)then
            write(*,*)'info#no weather files'
      else
!     les noms de climat sont stockes dans le tableau fcli(i)
      write(*,*) 'info#the co2 is 330 ppm in the new climate files'
!      write(*,*)'dans les fichiers stations ont ete repris uniquement '
!      write(*,*)'les valeurs de latitude nometp et alphapt '
!      write(*,*)'les autres sont des valeurs par defaut'
            codemauvais=0
        do 501 i=1,nbcli
!   nom complet du fichier tec de lecture
          oldcli=trim(ancienchemin(i))//'\'//ficcli(i)
!   nom complet du fichier tec ecrit
! 10/08/2012            cheminutilnew=trim(dircourant)//'\'//trim(nouveauchemin(i))
            cheminutilnew=trim(nouveauchemin(i))
            write(*,*)'debug# start processing file : ',trim(oldcli)
            tata=ficcli(i)
            ii = min(10,len_trim(tata))
            do ll=1,ii
                  if(tata(ll:ll).eq.'.')longmeteo=ll
            enddo
            toto=tata(1:longmeteo-1)
            if(toto.eq.'fort           ')goto 501
! DR 30/10/07
!  on traite le nom du fichier pour avoir l'annee en entier
!          newcli=trim(cheminutilnew)//'\'//ficcli(i)
          newsta=trim(cheminutilnew)//'\'//trim(toto)//'.sta'
!         lecture du fichier climatique      
!---------------------------------
            if(ficcli(i).eq.'') goto 502
            open (12,file=oldcli,status='old',err=1214)
            call lecstat(nbjourclim)
            if(codemauvais.eq.0)then
                newcli=trim(cheminutilnew)//'\'//trim(toto)//'.'//nomannee
                newficcli(i)=trim(toto)//'.'//nomannee
                write(ficclimat,*)trim(nouveauchemin(i)),' ',ficcli(i),newficcli(i)
!                  write(ficclimat,*)ficcli(i),newficcli(i)
                open(36,file=newcli,err=1218)
                call ecrinouvcli(nbjourclim)
                close(36)
                call mouchclim
                write(*,*)'debug#the file : '//trim(oldcli)//' has been translated in the file '//trim(newsta)
            else
                write(*,*)'error#the file ',trim(oldcli),'  contains an error so it has not been translated'
                  codemauvaisclim=codemauvaisclim+1
                  file_clim_mauvais(codemauvaisclim)=oldcli
            endif
            close(12)
 502            continue
        goto 503
1218    write(*,*)'error#pb in ',newcli
 503     continue

        goto 504
1214    write(*,*)'error#pb in ',oldcli
 504     continue


 501    continue
      endif
      if(codemauvaisclim.gt.0)then
            write(*,*)'error#<b> ATTENTION </b>'
            write(*,*)'error#there is  ',codemauvaisclim,'  bad climatic files '
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      else
            write(*,*)'info#',nbcli-codemauvaisclim,'climatic files had been translated'
      endif
! //// c'est finit pour les fichiers climat ////
! **********************************************
!     on resume
      write(*,*)'info#<u><b>Summary of translation</b></u>'
! les plt
      if(codemauvaisplt.gt.0)then
        write(*,*)'error#<b> Attention there is  ',codemauvaisplt,'  bad plant files '


        do i=1,codemauvaisplt
            write(*,*)'error#',trim(file_plt_mauvais(i))
        enddo
            write(*,*)"error#You can verify in your file 6.9 if the number of varieties in : &
            &'TV: les diff�rentes vari�t�s' is the number of varieties in the file"
            write(*,*)'error#if it is true you can verify that you have 22 before the name of the varieties'
            write(*,*)'error#for example :                    M01: pois                               22'
      endif
! les tec
      if(codemauvaistec.gt.0)then
        write(*,*)'error#<b> Attention there is ',codemauvaistec,'  bad tec files  </b>'
        do i=1,codemauvaistec
          write(*,*)'error#',trim(file_tec_mauvais(i))
        enddo
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      endif
! les obs
      if(codemauvaisobs.gt.0)then
        write(*,*)'error#<b> Attention there is ',codemauvaisobs,' bad obs files '
        do i=1,codemauvaisobs
          write(*,*)trim(file_obs_mauvais(i))
        enddo
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      endif  
! les climatiques
      if(codemauvaisclim.gt.0)then
        write(*,*)'error#<b> Attention there is ',codemauvaisclim,'  bad climatic files '

        do i=1,codemauvaisclim
          write(*,*)'error#',trim(file_clim_mauvais(i))
        enddo
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      endif

! les sols
      if(codemauvaissol.gt.0)then
        write(*,*)'error#<b> Attention there is ',codemauvaissol,'  bad soil files '

        do i=1,codemauvaissol
          write(*,*)'error#',trim(file_sol_mauvais(i))
        enddo
            write(*,*)'error#You can check the file content to remove tabs or empty lines for example '
      endif



      if((codemauvaisplt.eq.0).and.(codemauvaistec.eq.0).and.(codemauvaissol.eq.0).and.(codemauvaisobs.eq.0))then
          if(codemauvaisclim.ne.0)then
           write(*,*)'error#Your formatter is made but some files climate have not been translated!!!!'
           WRITE(*,*)'error# see the log file : log\translate-tool-log.txt'
          else
           write(*,*)'info#<u>Congratulation</u>'
           write(*,*)'info#all the files have been translated to modulo pour Javastics'
           WRITE(*,*)'info#End of the translation maj6vers7'
          endif
      else
         write(*,*)'info#<u><b> Some files could not be translated</u></b>'
         WRITE(*,*)'info#<u><b> see the log file : log\translate-tool-log.html </u></b>'
         WRITE(*,*)'info#<u><b>End of the translation maj6vers7</u></b>'
      endif       

      close(ficclimat)
      END
