﻿!  parametres techniques
!***********************
      character*7 ressuite(2)
      character*3  stadecoupedf(2)
      
      logical lecfauche(2)

      integer codefauche(2),codeaumin(2),codetradtec(2)
      integer codlocirrig(2),codlocferti(2),codemodfauche(2)
      integer codeffeuil(2),codecalirrig(2),codestade(2)
      integer codcalrogne(2),codepaillage(2),codeclaircie(2)
      integer codcaleffeuil(2),codrognage(2)
      integer codhauteff(2),codetaille(2),codrecolte(2),codcueille(2)
      integer iplt(2),ilev(2),iamf(2),ilax(2),idrp(2),isen(2)
      integer imat(2),irec(2),irecbutoir(2),variete(2)
      integer nap(2),napN(2),napS(2)
      integer nbcoupe(2),julfauche(2,20),nbcueille(2)
      integer ilan(2),iflo(2),locirrig(2),engrais(2)
      real locferti(2)
      real cadencerec(2)
      integer julrogne(2),juleclair(2)
      integer juleffeuil(2),jultaille(2),jultrav(2,11),coderes(2,11)
      integer upvttapN(2,300),julapI(2,300),julapN(2,300)
      
      real profsem(2),ratiol(2),dosimx(2),profmes(2)
      real densite(2),concirr(2),effirr(2)
      real lairesiduel(2,0:20),hautcoupedefaut(2),hautcoupe(2,20)
      real msresiduel(2,0:20),anitcoupe(2,20),tempfauche(2,10)

      real lairesiduel2(2,0:20),hautcoupe2(2,20)
      real msresiduel2(2,0:20),anitcoupe2(2,20)

      real largrogne(2),margerogne(2),hautrogne(2)
      real effeuil(2)
      real interrang(2),orientrang(2)
      real Crespc(2,11),proftrav(2,11),profres(2,11)
      real CsurNres(2,11),Nminres(2,11),eaures(2,11),qres(2,11)
      real doseI(2,300),doseN(2,300),h2ograinmin(2),h2ograinmax(2)
      real CNgrainrec(2),huilerec(2),sucrerec(2)
      real mscoupemini(2)
      real nbinfloecl(2),laidebeff(2),laieffeuil(2),biorognem(2)


      common/codetec/codrecolte,codlocirrig,codlocferti,codemodfauche,&
                   codrognage,codeffeuil,codecalirrig,  &
                   codcalrogne,codepaillage,codeclaircie, &
                   codcaleffeuil,codhauteff,codetaille,codcueille
      common/partec/iplt,ilev,iamf,ilax,idrp,isen,imat,irec,irecbutoir,&
                   profsem,effirr,nap,julapI,ratiol,dosimx,profmes, &
                   napN,julapN,variete,densite,concirr,&
                   codefauche,nbcoupe,julfauche,lairesiduel, &    
                   tempfauche,hautcoupedefaut,hautcoupe,msresiduel, &
                   anitcoupe,ressuite,stadecoupedf         
      common/partec2/effeuil,nbcueille,largrogne,margerogne,hautrogne, &
                   codetradtec,interrang,orientrang,codestade,&
                   locirrig,engrais,locferti,doseN,cadencerec, &
                   h2ograinmin,h2ograinmax,sucrerec,CNgrainrec, &
                   huilerec
      common/partec3/lecfauche,mscoupemini,&
                   biorognem,julrogne,juleclair,&
                   nbinfloecl,laidebeff,juleffeuil,jultaille,laieffeuil 
      common/partec4/lairesiduel2,msresiduel2,hautcoupe2,anitcoupe2
      common/climatb2/codeaumin
      common/nouvtec2/napS,jultrav,proftrav,profres,coderes,qres,&
                   CsurNres,Nminres,eaures,Crespc,ilan,iflo,doseI
     
       common /appNenUpvtt/upvttapN



! -- inutilis�s
!      integer modexploit(2)

      real albedomulchtec
      common/dr280108/albedomulchtec

! DR 02/07/08 fractionnement de l'azote
      real fracN(2,300)
      integer codefracappN(2),Qtot_N(2)
      common/DR020708/fracN,codefracappN,Qtot_N
! dr 22062011 
!      real nbinfloecl(2)
!      common/drtec_22062011/nbinfloecl



