      subroutine mouchtec
      include 'common.inc'
      include 'comform.inc'
      include 'compartec.inc'
      include 'comusmplt.inc'
      include 'comvarplt.inc'
!      include 'format.inc'
      include 'comparsol.inc'
      include 'comtransit.inc'
      common/newfiles/newtec,newplt,newobs,newsol,newcli,newsta
      character*255 newtec,newsol,newobs,newplt,newcli,newsta
      character*35 para,paranw,parav6,paravv6,parachnom

!c ecriture dans le mouchard
      write(*,*)'debug#','parameters readed in the user file ',newtec

      para='parameter '
      paranw='new parameter'
      parav6='parameter from paramv6'
      paravv6='parameter moved to paramv6'
      parachnom='parameter with new name'


      ipl=1


       write(*,*)'debug#','the tillage operations were split between inputs of residue and tillage'
       write(*,*)'debug#','check it please'
       write(*,*)'debug#',parachnom  ,'albedomulchplastique',albedomulchtec
       write(*,*)'debug#',para  ,'biorognem(ipl)     ',biorognem(ipl)
       write(*,*)'debug#',para  ,'cadencerec(ipl)    ',cadencerec(ipl)
       write(*,*)'debug#',para  ,'CNgrainrec(ipl)    ',CNgrainrec(ipl)
       write(*,*)'debug#',para  ,'codabri                 ',codabri
       write(*,*)'debug#',para  ,'codcaleffeuil(ipl) ',codcaleffeuil(ipl)
       write(*,*)'debug#',para  ,'codcalrogne(ipl)   ',codcalrogne(ipl)
       if(codcueille(ipl).eq.0)write(*,*)'debug#','codcueille(ipl)=0 is a bad value, it changed to 1 '
       write(*,*)'debug#',para  ,'codcueille(ipl)        ',codcueille(ipl)
       write(*,*)'debug#',para  ,'codeaumin(ipl)        ',codeaumin(ipl)
       write(*,*)'debug#',para  ,'codecalirrig(ipl)  ',codecalirrig(ipl)
       write(*,*)'debug#',para  ,'codeclaircie(ipl)  ',codeclaircie(ipl)
       write(*,*)'debug#',paranw,'codedateappH2O(ipl)',codedateappH2O(ipl)
       write(*,*)'debug#',parav6,'codedateappN(ipl)   ',codedateappN(ipl)
       write(*,*)'debug#',parav6,'codedecirecolte(ipl)' ,codedecirecolte(ipl)
       write(*,*)'debug#',parav6,'codedecisemis(ipl)  ',codedecisemis(ipl)
       write(*,*)'debug#',parav6,'codeDST(ipl)          ',codeDST(ipl)
       write(*,*)'debug#',parav6,'codeDSTnbcouche(ipl)' ,codeDSTnbcouche(ipl)
       write(*,*)'debug#',parav6,'codeDSTtass(ipl)    ',codeDSTtass(ipl)
       write(*,*)'debug#',para  ,'codefauche(ipl)        ',codefauche(ipl)
       write(*,*)'debug#',para  ,'codeffeuil(ipl)        ',codeffeuil(ipl)
       write(*,*)'debug#',paranw,'codefracappN(ipl)   ',codefracappN(ipl)
       write(*,*)'debug#',para  ,'codemodfauche(ipl)  ',codemodfauche(ipl)
      if(codepaillage(ipl).eq.3)then
      write(*,*)'debug#','attention you had plastic mulch= 3 ','it becomes 2'
      endif
       write(*,*)'debug#',para  ,'codepaillage(ipl)   ',codepaillage(ipl)
       write(*,*)'debug#',paranw,'codepalissage(ipl)  ',codepalissage(ipl)
       write(*,*)'debug#',parav6,'coderecolteassoc    ',coderecolteassoc
       write(*,*)'debug#',para  ,'codestade(ipl)        ',codestade(ipl)
       write(*,*)'debug#',para  ,'codetaille(ipl)        ',codetaille(ipl)
       write(*,*)'debug#',para  ,'codetradtec(ipl)    ',codetradtec(ipl)
       write(*,*)'debug#',para  ,'codhauteff(ipl)        ',codhauteff(ipl)
       write(*,*)'debug#',para  ,'codlocferti(ipl)    ',codlocferti(ipl)
       write(*,*)'debug#',para  ,'codlocirrig(ipl)    ',codlocirrig(ipl)
       write(*,*)'debug#',para  ,'codrecolte(ipl)        ',codrecolte(ipl)
       write(*,*)'debug#',para  ,'codrognage(ipl)        ',codrognage(ipl)
       write(*,*)'debug#',para  ,'concirr(ipl)          ',concirr(ipl)
       write(*,*)'debug#',parachnom  ,'couvermulchplastique       ',couvermulch
       write(*,*)'debug#',parav6,'dachisel(ipl)          ',dachisel(ipl)
       write(*,*)'debug#',parav6,'dalabour(ipl)          ',dalabour(ipl)
       write(*,*)'debug#',parav6,'darecolte(ipl)          ',darecolte(ipl)
       write(*,*)'debug#',parav6,'dasemis(ipl)          ',dasemis(ipl)
       write(*,*)'debug#',para,'change de nom: densitesem',densite(ipl)
       write(*,*)'debug#',parav6,'doseirrigmin(ipl)   ',doseirrigmin(ipl)
       write(*,*)'debug#',para  ,'dosimx(ipl)          ',dosimx(ipl)
       write(*,*)'debug#',para  ,'effeuil(ipl)          ',effeuil(ipl)
       write(*,*)'debug#',para  ,'effirr(ipl)          ',effirr(ipl)
       write(*,*)'debug#',para  ,'engrais(ipl)          ',engrais(ipl)
       write(*,*)'debug#',para  ,'h2ograinmax(ipl)    ',h2ograinmax(ipl)
       write(*,*)'debug#',para  ,'h2ograinmin(ipl)    ',h2ograinmin(ipl)
       write(*,*)'debug#',para  ,'hautcoupedefaut(ipl)',hautcoupedefaut(ipl)
       write(*,*)'debug#',parav6,'hautmaxtec(ipl)          ',hautmaxtec(ipl)
       write(*,*)'debug#',para  ,'hautrogne(ipl)          ',hautrogne(ipl)
       write(*,*)'debug#',para  ,'huilerec(ipl)          ',huilerec(ipl)
       write(*,*)'debug#',para  ,'iamf(ipl)          ',iamf(ipl)
       write(*,*)'debug#',para  ,'idrp(ipl)          ',idrp(ipl)
       write(*,*)'debug#',para  ,'iflo(ipl)          ',iflo(ipl)
       write(*,*)'debug#',para  ,'ilan(ipl)          ',ilan(ipl)
       write(*,*)'debug#',para  ,'ilax(ipl)          ',ilax(ipl)
       write(*,*)'debug#',para  ,'ilev(ipl)          ',ilev(ipl)
       write(*,*)'debug#',para  ,'imat(ipl)          ',imat(ipl)
       write(*,*)'debug#',para  ,'interrang(ipl)          ',interrang(ipl)
       write(*,*)'debug#',para  ,'iplt(ipl)          ',iplt(ipl)
       write(*,*)'debug#',para  ,'irec(ipl)          ',irec(ipl)
       write(*,*)'debug#',para  ,'irecbutoir(ipl)          ',irecbutoir(ipl)
       write(*,*)'debug#',para  ,'isen(ipl)          ',isen(ipl)
       write(*,*)'debug#',para  ,'nap(ipl)           ',nap(ipl)
       write(*,*)'debug#',para,'julapI   doseI'
      do i=1,nap(ipl)
       write(*,*)'debug#',para  ,julapI(ipl,i),doseI(ipl,i)
      enddo
       write(*,*)'debug#',para  ,'napN(ipl)           ',napN(ipl)
      if(codedateappN(ipl).eq.2)then
       write(*,*)'debug#',para,'julapN   doseN'
      do i=1,napN(ipl)
       write(*,*)'debug#',para  ,julapN(ipl,i),doseN(ipl,i)
      enddo
      else
       write(*,*)'debug#',para, 'upvttapN   doseN'
      do i=1,napN(ipl)
       write(*,*)'debug#',para  ,upvttapN(ipl,i),doseN(ipl,i)
      enddo
      endif
       write(*,*)'debug#',para  ,'juleclair(ipl)          ',juleclair(ipl)
       write(*,*)'debug#',para  ,'juleffeuil(ipl)          ',juleffeuil(ipl)
       write(*,*)'debug#',para  ,'nbcoupe2          ',nbcoupe2
       write(*,*)'debug#','julfauche   hautcoupe   lairesiduel',&
      '   msresiduel   anitcoupe(ipl,i) '
      do i=1,nbcoupe2
      write(*,*)'debug#',para,julfauche(ipl,i),hautcoupe(ipl,i),&
        lairesiduel(ipl,i),msresiduel(ipl,i),anitcoupe(ipl,i) 
      enddo
      write(*,*)'debug#',para,'nbcoupe3          ',nbcoupe3
      write(*,*)'debug#','tempfauche   hautcoupe   lairesiduel', &
      '   msresiduel   anitcoupe(ipl,i) '
        do i=1,nbcoupe3
      write(*,*)'debug#',para,tempfauche(ipl,i),hautcoupe2(ipl,i),&
        lairesiduel2(ipl,i),msresiduel2(ipl,i),anitcoupe2(ipl,i)
       enddo
       write(*,*)'debug#',para  ,'julouvre2           ',julouvre2
       write(*,*)'debug#',para  ,'julouvre3           ',julouvre3
       write(*,*)'debug#',para  ,'julrogne(ipl)           ',julrogne(ipl)
       write(*,*)'debug#',para  ,'jultaille(ipl)           ',jultaille(ipl)
       write(*,*)'debug#',para  ,'laidebeff(ipl)           ',laidebeff(ipl)
       write(*,*)'debug#',para  ,'laieffeuil(ipl)           ',laieffeuil(ipl)
       write(*,*)'debug#',para  ,'largrogne(ipl)           ',largrogne(ipl)
       write(*,*)'debug#',parav6,'largtec(ipl)           ',largtec(ipl)
       write(*,*)'debug#',para  ,'locferti(ipl)           ',locferti(ipl)
       write(*,*)'debug#',para  ,'locirrig(ipl)           ',locirrig(ipl)
       write(*,*)'debug#',para  ,'margerogne(ipl)           ',margerogne(ipl)
       write(*,*)'debug#',para  ,'mscoupemini(ipl)     ',mscoupemini(ipl)
       write(*,*)'debug#',para  ,'napS(ipl)           ',napS(ipl)
         write(*,*)'debug#','jultrav   profres   CsurNres    Nminres    ',  &
      'eaures   proftrav   coderes   qres   Crespc   CsurNres   ', &
      'Nminres   eaures'
         do i=i,napS(ipl)
     write(*,*)'debug#',jultrav(ipl,i),profres(ipl,i),CsurNres(ipl,i),Nminres(ipl,i),eaures(ipl,i),&
    & proftrav(ipl,i),coderes(ipl,i),qres(ipl,i),Crespc(ipl,i),CsurNres(ipl,i),Nminres(ipl,i),eaures(ipl,i)
        enddo

       write(*,*)'debug#',para  ,'nbcueille(ipl)           ',nbcueille(ipl)
       write(*,*)'debug#',para  ,'change de nom: nbinfloecl' ,nbinfloecl(ipl)
       write(*,*)'debug#',parav6,'nbjmaxapresrecolte(ipl)',nbjmaxapresrecolte(ipl)
       write(*,*)'debug#',parav6,'nbjmaxapressemis(ipl)',nbjmaxapressemis(ipl)
       write(*,*)'debug#',parav6,'nbjseuiltempref(ipl)   ' ,nbjseuiltempref(ipl)
       write(*,*)'debug#',para  ,'orientrang(ipl)           ',orientrang(ipl)
       write(*,*)'debug#',parav6,'profhumrecolteuse(ipl) ' ,profhumrecolteuse(ipl)
       write(*,*)'debug#',parav6,'profhumsemoir(ipl)   ',profhumsemoir(ipl)
       write(*,*)'debug#',para  ,'profmes(ipl)             ',profmes(ipl)
       write(*,*)'debug#',para  ,'profsem(ipl)             ',profsem(ipl)
       write(*,*)'debug#',paranw,'Qtot_N(ipl)             ',Qtot_N(ipl)
       write(*,*)'debug#','qmulch0, julappmulch, typemulch are deleted'
       write(*,*)'debug#','mulch managed by residues'
       write(*,*)'debug#',para  ,'ratiol(ipl)             ',ratiol(ipl)
       write(*,*)'debug#',parav6,'rugochisel(ipl)         ',rugochisel(ipl)
       write(*,*)'debug#',parav6,'rugolabour(ipl)         ',rugolabour(ipl)
       write(*,*)'debug#',para  ,'stadecoupedf(ipl)    ',stadecoupedf(ipl)
       write(*,*)'debug#',para  ,'sucrerec(ipl)             ',sucrerec(ipl)
       write(*,*)'debug#',para  ,'surfouvre1             ',surfouvre1
       write(*,*)'debug#',para  ,'surfouvre2             ',surfouvre2
       write(*,*)'debug#',para  ,'surfouvre3             ',surfouvre3
       write(*,*)'debug#',para  ,'transplastic             ',transplastic
       write(*,*)'debug#',para  ,'ressuite(ipl)        ' ,trim(ressuite(ipl))
       write(*,*)'debug#',para  ,'variete(ipl)             ',variete(ipl)

       write(*,*)'debug#',paravv6  ,'codecalferti         ',codecalferti
       write(*,*)'debug#',paravv6  ,'ratioNl           ',ratiolN
       write(*,*)'debug#',paravv6  ,'dosimxN             ',dosimxN
       write(*,*)'debug#',paravv6  ,'codetesthumN      ',codetesthumN




!c fin mouchard
!200   continue
      return
      end
