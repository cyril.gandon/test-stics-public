c        format.inc        
c  *********************
c  

 1008 format(' Bilan de la simulation STICS ',a3,', mod�le ',
     s       A7/1x,48('*'))
 1009 format(' 1. DONNEES d''ENTREE',/1x,19('*'))
 1010 format(3x,'Fichier climatique            : ',A12)
 1011 format(3x,'Fichier techniques culturales : ',A12)
 1012 format(3x,'Fichier plante                : ',A15,6x,
     s             'Vari�t� : ',a10)
 1013 format(3x,'Fichier lai                   : ',A12)
 1014 format(3x,'Valeurs initiales du sol      : ',A12,/,
     +       6x,'Z (cm)',6x,'Eau (%)      NO3 (kg/ha)    NH4 (kg/ha)')
 1020 format(3x,'Valeurs initiales plante      : ',/,
     +       3x,'stade    LAI     MS   ZRAC MAGRAIN QNPLANTE INN')
 1021 format(/3x,'D�but de simulation :',I3,'-',A3,'-',I4,6x,'jour',I4)
 1022 format( 3x,'Fin de simulation   :',I3,'-',A3,'-',I4,6x,'jour',i4,
     s        3x,'(ou',i4,')')
 1023 format(/3x,'Irrigation:',11x,'Nombre d''arrosages =',i3)
 1025 format(5x,'d�cade d''arrosage',8x,'dose (mm)'/
     +       5x,'-----------------' ,8x,'---------')
 1024 format(5x,'date d''arrosage',  8x,'dose (mm)'/
     +       5x,'---------------',   8x,'---------')
 1125 format(5x,'Quantit� totale apport�e:',F7.0,' mm')
 1026 format(/3x,'Fertilisation:',8x,'Nombre d''apports =',i2,
     s        :,5x,'Type d''engrais =',i2)
 1027 format(5x,'date d''apport N',12x,'dose (kg N/ha)'/
     +       5x, 15('-'),12x,14('-'))
 1028 format(27x,'total',f7.0)
 1029 format(/3x,'R�sidus organiques et/ou travail du sol')
 1030 format(9x,'Aucun apport, aucun travail du sol')
 1031 format(5x,'Travail du sol ',7x,   'jour',i4,4x,'sur',f4.0,'cm')           
 1032 format(5x,'R�sidus de type',i3,4x,'jour',i4,4x,'sur',f4.0,'cm',
     s       4x,'MS=',f4.1,' t/ha',4x,'C/N=',f4.0)
 1033 format(//,' 2. DEVELOPPEMENT DE LA CULTURE',/,1x,30('*'))
 1040 format(5x,'type                   : plante de jours longs')
 1041 format(5x,'type                   : plante de jours courts')
 1042 format(5x,'unit� de d�veloppement : vernalo-photo-thermique')
 1043 format(5x,'unit� de d�veloppement : photo-thermique')
 1044 format(5x,'unit� de d�veloppement : thermique')
 1045 format(5x,'temp�rature consid�r�e : temp�rature de l''air')
 1046 format(5x,'temp�rature consid�r�e : temp�rature de culture')                                    
 1050 format(/5x,'Vernalisation impossible: il fait trop chaud')
 1051 format(/8x,'stade',15x,'date',11x,'unit�s',5x,'unit�s cumul�es',
     s  /5x,11('-'),7x,11('-'),9x,6('-'),5x,15('-'))      
 1052 format(5x,'stade initial : ',a3)
 1053 format('    Stades v�g�tatifs')
 1054 format('    Stades reproducteurs')
 1055 format(/,'    Dur�e du cycle :',i4,' jours')
 1056 format(/,' Attention: pour cette simulation la date de r�colte',
     s         ' est une date butoir',/,' la somme d''unit�s semis-',
     s         'r�colte peut ne pas avoir �t� atteinte � cette date')

 1057 format(//,' 3. CROISSANCE ET COMPOSANTES DU RENDEMENT'/
     s       1x,41('*')) 
c 
 1157 format(3x,'Le : ',I3,'-',A3,'-',I4) 
 1058 format(3x,'Biomasse a�rienne r�colte (0% eau)   =',f8.2,' t/ha'
     s   /3x,'Rendement grains,fruits   (0% eau)   =',f8.2,' t/ha'
     s   /3x,'Rendement grains,fruits (',f4.0,'% eau)   =',f8.2,' t/ha'
     s  //3x,'Nombre de grains,fruits              =',f8.0,' /m2')
c  
 1059 format(3x,'Densit� de plantes                   =',f8.1,' /m2'
     s      /3x,'Poids du grain,fruit    (',f4.0,'% eau)   =',f8.3,' g')
 1160 format(3x,'Nombre d''inflorescences               =',f8.1)
 1161 format(3x,'Nombre de fruits non murs =',f8.0,'/m2, soit un poids',
     s          ' sec =',f8.2,'t/ha')
 1159 format(3x,'Vit. de croissance (phase latence)   =', f8.1,
     s          ' mg/m2/j')
 
 1360 format(3x,'Poids de taille (0% eau)             =',f8.2,' t/ha')
 1361 format(3x,'R�serves fin simulation (0% eau)     =',f8.2,' t/ha')
c
 2092 format(/3x,'Nombre de feuilles �mises            =',i6)
 2100 format( 3x,'Nombre de jours �chaudants ou froids =',i6)
 1060 format(3x,'Biomasse a�rienne s�nescente (0% eau)=',f7.2,' t/ha')
c
 1061 format(/3x,'Quantit� N dans la culture         =',f8.0,' kg/ha'
     s       /3x,'Quantit� N dans les grains,fruits  =',f8.0,' kg/ha'
     s      //3x,'Teneur en N plante enti�re         =',f6.2,' % MS')
 1461 format( 3x,'Teneur en N grains,fruits          =',f6.2,' % MS',
     s       /3x,'Teneur en prot�ines grains,fruits  =',f6.1,' % MS')
c 
 1258 format(/3x,'Efficience de l''engrais azot�      =',f5.2)
 1158 format(/3x,'Indice de r�colte                  =',f5.2)
c
 1261 format(/2x,'Composition de la mati�re fraiche des grains/fruits',
     s       /3x,'Teneur en mati�re s�che            =',f6.0,' % MF'
     s       /3x,'Teneur en azote                    =',f6.2,' % MF')
c
 1262 format( 3x,'Teneur en sucre                    =',f6.1,' % MF')
 1263 format( 3x,'Teneur en huile                    =',f6.1,' % MF')
c modif Bruno 23/03/04 
 1062 format(//,' RESIDUS DE CULTURE arrivant au sol : ',a7,' + racines'
     s        ,6x,'MS =',f5.1,' t/ha',5x,'C/N =',f4.0)
 1063 format('4. BILANS EAU et AZOTE sur le cycle de culture'
     s     ,/,46('*'))
 1064 format(29x,'Somme ETM =',f6.0,' mm'/29x,'Somme ETR =',f6.0,' mm'
     s     ,/29x,'Somme ES  =',f6.0,' mm'/29x,'Somme TR  =',f6.0,' mm'
     s     ,/29x,'Somme P   =',f6.0,' mm'/)
 1065 format(5X,'R�serve en eau maximale utilis�e par la plante  =',f6.0
     s ,' mm',/5x,'Profondeur racinaire maximale     =',f6.0,' cm')   
c                 
 1066 format(/2x,'Indices moyens de STRESS :       swfac   turfac',
     s           '   inns   tcult-tair  exofac',/,
     s      3x,'phase v�g�tative    (lev-drp) ',5f8.2,/,
     s      3x,'phase reproductrice (drp-mat) ',5f8.2)

 1166 format(/3x,'D�g�ts de gel du feuillage : avant AMF ',f5.0,'%',
     s        4x,'apr�s AMF',f5.0,'%')
 1167 format(i4,':', 3x,'Gel de la plante et pas de reserve')
 1168 format( 3x,'D�g�ts de gel des fleurs ou fruits     ',f5.0,'%')
c
 1067 format('Stress azot� calcul� mais inactif sur les plantes') 
 1068 format('Stress hydrique calcul� mais inactif sur les plantes')
 1069 format(/,/,'5. BILANS EAU, AZOTE, CARBONE sur toute la ',
     s         'p�riode de simulation  (',i3,' jours)'/,77('*'))
 1070 format(3x,'Jours normalis�s �',f4.0,'�C :',4x,'Humus =',f5.0,
     s       6x,'R�sidus =',f5.0)
 4070 format(3x,'Vitesse potent. de min�ralisation =',f5.2,
     s          ' kg N/ha/jour',3x,'soit',f5.2,'% par an')    
 1071 format(/20x,'ENTREES',25x,'SORTIES')
 1072 format(' EAU (mm)')
 1073 format(
     s 10x,'pluie             ',f6.0,10x,'�vaporation          ',f6.0,
     s/10x,'irrigation        ',f6.0,10x,'transpiration        ',f6.0,
     s/10x,'remont�es         ',f6.0,10x,'ruissellement        ',f6.0, 
     s/44x,'infiltration profonde',f6.0, 
     s/44x,'drainage agricole    ',f6.0, 
     s/44x,'interception feuille ',f6.0, 
     s/44x,'interception mulch   ',f6.0, 
     s/44x,'irrigat non efficace ',f6.0, 
     s/10x,'Stock eau initial ',f6.0,10x,'Stock eau final      ',f6.0,
     s/10x,24('-')             ,     10x,27('-'),
     s/10x,'TOTAL             ',f6.0,10x,'TOTAL                ',f6.0)
 1074 format(/' AZOTE (kg/ha)'/)
 1075 format( 
     s 10x,'pluie             ',f6.0,10x,'exportation culture  ',f6.0,
     s/10x,'irrigation        ',f6.0,10x,'restitution culture  ',f6.0,
     s/10x,'engrais           ',f6.0,10x,'lixiviation          ',f6.0,
     s/10x,'fixation symbiot. ',f6.0,10x,'organisation engrais ',f6.0                             
     s/10x,'min�ralis. humus  ',f6.0,10x,'volatil. engrais     ',f6.0,                                 
     s/10x,'min�ralis. r�sidus',f6.0,10x,'volatil. amendements ',f6.0,
     s/44x,'d�nitrification      ',f6.0,                         
     s/10x,'N plante initial  ',f6.0,10x,'lixiviation drains   ',f6.0,
     s/10x,'NO3 sol initial   ',f6.0,10x,'NO3 sol final        ',f6.0,
     s/10x,'NH4 sol initial   ',f6.0,10x,'NH4 sol final        ',f6.0,
     s/10x, 24('-'),               10x,  27('-'),                            
     s/10x,'TOTAL             ',f6.0,10x,'TOTAL                ',f6.0//)    
 
c Bruno 14/10/2003
 1081  format(//' STOCK de MO sol',12x,'Initial',12x,'Final',
     s       //5x,'C organique humifi�',f10.2,f18.2,'   t/ha',
     s        /5x,'N organique humifi�',f10.0,f18.0,'  kg/ha',
     s          /5x,'N organique actif',f12.0,f18.0,'  kg/ha')
c
 1083 format(/5x,'Production cumul�e de CO2 par le sol',f11.0,
     s           '  kg C/ha')
c
c domi 29/04/2002
 3181  format(/,' &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ',i4,
     s       '  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&',/)
 3008    format(3x,'station m�t�o � une altitude de    ',f6.1,' m')
 3009    format(3x,'simulation � une altitude de       ',f6.1,' m')
 3110    format('  modalit� "adret"')
 3111    format('  modalit� "ubac"')
 3101   format('     r�colte n�',i2,' le ',i2,'-',a3,'-',i4,': ',
     s         f7.2,'t/ha (',f8.1,' fruits/m2 -- ',f6.2,'% MF)' )
c drainage
 3011    format(/,'B / Ldrains (cm) / Ksol (cm/j) / profdrain (cm)
     s       /sensanox /distdrain:',/,f5.2,1x,'/',f8.1,1x,'/',f8.1,
     s       3(1x,'/',f7.2))
 3015    format('  Infiltrabilit� � la base du profil (mm) : ',f5.2)


c bilcoupe
c --------
 2001 format(' Bilan de la simulation STICS ',a3,', mod�le ',A7,
     s       ' version fourrages'/1x,67('*'))
 2012 format(3x,'Fichier plante                : ',A15)
 2014 format(5x,'groupe vari�tal : ',i2)
 2017 format(5x,'Fin de simulation   : ',I2,'-',A3,'-',I4,6x,'jour',I4)          
 2018 format(' Dans cette simulation les dates de fauche',
     s       ' sont imposees par dates')
 2019 format(' Dans cette simulation les dates de fauche',
     s       ' sont imposees par somme de temp')
 2031 format('Dans cette simulation les sommes de fauche',
     s       ' sont calcul�es en unit�s photo-vernalo-thermiques')
 2032 format('Dans cette simulation les sommes de fauche',
     s       ' sont calcul�es en unit�s thermiques (t air)')
 2020 format(' Dans cette simulation les dates de fauche',
     s       ' sont calculees au stade ',a3)
 2021 format(' Irrigations lues')
 2022 format(' Irrigations calcul�es avec un seuil de satisfaction = ',
     s       f3.1)
 2023 format('  Fertilisations lues')
 2024 format('  Fertilisations calcul�es avec un stress admis >',f3.1)      
 2025 format(///,20x,20('%'),//,
     s      ' Bilan et r�capitulatif fin de culture '/1x,38('*'))
 2026 format(/3x,'Irrigation')
 2029 format(5x,'Nombre d''arrosages: ',I3)
 2030 format(/3x,'Fertilisation')
 2033 format(5x,'Quantit� totale apport�e jusqu''a la coupe',
     s       ' :',F9.0)
 2034 format(/,3x,'R�sidus de la culture pr�c�dente : pas de r�sidus')
 2035 format(/,3x,'R�sidus de la culture pr�c�dente :',/5x,
     s     'quantit� =',f5.1,' t/ha',9x,'C/N =',f5.0,   /5x,
     s     'incorpor�s le jour',i4,' sur une profondeur de',f4.0,' cm') 
 2036 format(/8x,'fauche',15x,'date'
     s       /5x,11('-'),7x,11('-'))
 2038 format(11x,'Biomasse a�rienne totale r�colt�e(0 %)  =',f7.1,
     s       ' t/ha'/)
 2039 format(11x,'Quantit� d''azote absorb� par la culture =',f5.0,
     s      ' kg/ha',/11x,'[N] plante enti�re = ',f6.2,' %',/) 
 2040 format(10x,' R�sidus pour la culture suivante :   ',a7,/11x,
     s       'Quantit� =',f5.1,' t/ha',9x,'C/N =',f4.0//)  
 2041 format('4. BILANS EAU et AZOTE',/,
     s       '   d�marrage coupe 1 - fin de simulation',/,50('*'))
 2064 format(///20x,' 1.FAUCHE NUMERO ',i2 ,/,20x,20('*')//)
 2065 format('    jour de la coupe : ',I2,'-',A3,'-',I4,' , jour ',i3)
 2066 format('    la date de fauche a �t� repouss�e ( biomasse a�rienne 
     s       < ',f5.2, 't/ha)')
 2067 format(5x,'Quantit� totale apport�e jusqu''a la coupe',
     s       ':',F7.0,' mm')
 2076 format(/8x,'stade',15x,'date',11x,'unit�s',5x,'unit�s cumul�es',
     s        5x,'lai',
     s       /5x,11('-'),7x,11('-'),9x,6('-'),5x,15('-'),5x,3('-'))
 2082 format(11x,'Biomasse a�rienne (0 %)  =',f7.1,' t/ha')
 2083 format(11x,'dont:',f7.1,' t/ha en vert',/11x,'et  :',f7.1,
     s           ' t/ha en s�nescent'/)
 2084 format(11x,'dont:',f7.1,' t/ha de biomasse r�siduelle provenant',
     s           ' du cycle pr�c�dent',/,11x,'et  :',f7.1,' t/ha', 
     s           ' de biomasse n�oform�e')
 2085 format(11x,'reste de biomasse r�siduelle verte          :',f7.1,
     s           ' t/ha')
 2086 format(11x,'mati�re s�che r�siduelle pour cycle suivant :',f7.1,
     s           ' t/ha',/)    
 2087 format(11x,'Densit� de plantes=',f8.2,' /m2'/)
 2088 format('4. BILANS EAU et AZOTE intercoupe (fauche',i2,'-fauche '
     s       ,i2,')',/,55('*'))
 2089 format(/2x,'Indices moyens de STRESS :swfac   turfac',
     s    '   inns   tcult-tair   exofac',/,
     s       3x,'phase v�g�tative    (lev-drp) ',4f8.2)
 2090 format(3x,'phase reproductrice (drp-mat) ',4f8.2)
 2091 format(2x,'derniere boite non arriv�e � maturit�')

 2094 format(/,5x,'entr�e en dormance ',7x,I2,'-',A3,'-',I4)
 2093 format(5x,'lev�e de dormance  ',7x,I2,'-',A3,'-',I4,
     s       4x,'Chill Units (Richardson)= ', f5.0)

 2095 format(5x,'lev�e de dormance  ',7x,I2,'-',A3,'-',I4,
     s       4x,'Chill Units (Bidabe)= ', f5.0)
c
c NB le08/01/02
c   lecplant
 3273 format(5(/),10X,'Je ne trouve pas le fichier plante ',a15)
 3274 format(5(/),10X,'Je ne trouve pas le fichier technique ',a15)
 
c lixiv
c----------
c domi 29/04/2002
  666 format(2(/),'*** Attention, vous avez � la fois un drainage',
     s ' profond et une valeur d''infiltration profonde non nulle ***')   
c initial
c---------
c domi 29/04/2002
 3045 format(' Simulations successives impossibles: mauvaises dates de',
     s       ' d�but et fin',/,' Attention si votre fichier climatique',  
     s       ' pr�c�dent n''�tait pas complet, c''est lui qui a pu',
     s       ' d�finir la fin de la simulation pr�c�dente !')
c c leclai
c---------
c domi 25/06/02
 4000 format('je ne trouve pas le fichier lai : ',a20)
c domi 29/08/03 pour caro si lecture des dates d'apport en upvt 
c dans bilan
 4001 format('   les dates des apports ont �t� fix�es en upvt')

c DR 08/11/05 rajout pour bilcoupe 
 2096 format('la fauche a ete repouss�e de',i4,
     s' jour par rapport � la date initiale')
 2098 format(11x,'Biomasse a�rienne r�colt�e(0 %)  =',f7.2,
     s       ' t/ha'/)

