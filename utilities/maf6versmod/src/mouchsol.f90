      subroutine mouchsol

!c  ecriture du nouveau fichier sol
      include 'common.inc'
      include 'comparsol.inc'
      include 'comtransit.inc'

      character*35 paranw

      paranw='newparameter'



     write(*,*)'debug#','       typsol    argi   Norg   profhum calc   pH &
     & concseuil albedo q0 ruisolnu obstarac  pluiebat  mulchbat   zesx  cfes  z0solnu P_CsurNsol P_penterui'
     write (*,72) numsol,typsol,argi,Norg,profhum,calc,pH, concseuil,albedo,q0,ruisolnu,&
      obstarac,pluiebat,mulchbat,zesx,cfes,z0solnu,P_CsurNsol,P_penterui
     write(*,*)'debug#','2 new parameters : P_CsurNsol et P_penterui'

72   format(' debug#',i5,a12,17f10.5)

     write (*,*)'debug#','codecailloux codemacropor codefente codrainage coderemontcap codenitrif'
     write (*,*)'debug#',numsol,codecailloux,codemacropor,codefente,codrainage,coderemontcap,codenitrif,codedenit
 
     write (*,*)'debug#','  numsol  profimper  ecartdrain  ksol  profdrain  capiljour  humcapil  profdenit  vpotdenit'
     write (*,*)'debug#',  numsol,profimper,ecartdrain,ksol,profdrain,capiljour,humcapil,profdenit,vpotdenit

     write (*,*)'debug#','numsol   epc(icou)   HCCF(icou)   HMINF(icou)  DAF(icou)   cailloux(icou)   &
     & typecailloux(icou) infil(icou) epd(icou)'
     do icou=1,5
        write (*,*)'debug#',numsol,epc(icou),HCCF(icou),HMINF(icou),DAF(icou)&
          ,cailloux(icou),typecailloux(icou), infil(icou),epd(icou)
      enddo

      return
      end
