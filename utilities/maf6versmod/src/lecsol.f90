! ****************************************************************** c
! *   SUBROUTINE de lecture et d'initialisation des infos SOL      * c
! * derniere modif NB le 15/07/05                                  * c
! ****************************************************************** c

      subroutine lecsol

! ** Declarations :
! ** les commons
      include 'common.inc'
      include 'comparsol.inc'
!      include 'message.inc'
      include 'comtransit.inc'

! ** les VARIABLES LOCALES
      integer i,icou


      write(*,174)
  174 format(/,' debug#the following parameters from the file param.par have been ground into the new soil file :')
      write(*,*)'debug#    zesx           cfes        z0solnu      codedenit      profdenit      vpotdenit'

      write(*,*) 'debug#',zesx,cfes,z0solnu,codedenit,profdenit,vpotdenit

!     open(12,file='param.sol',status='old')
      do 71 i=1,ichsl*7,7
! ** version 5.0
      read (12,*,end=81,ERR=120) numsol,typsol,argi,Norg,profhum,calc,pH,&
                        concseuil,albedo,q0,ruisolnu,obstarac
! DR 17/10/06 pour Infosol on met 3 parametres de param.par dans le fichier sol
! DR 27/10/06 pour infosol on rajoute vpotdenit 
!        if(codesoldst.eq.1)then
!           read(12,*)numsol,zesx,cfes,z0solnu,vpotdenit
!        endif
      read (12,*,ERR=120) numsol,codecailloux,codemacropor,codefente, codrainage,coderemontcap,codenitrif
      read (12,*,ERR=120) numsol,capiljour,humcapil,profimper,ecartdrain,ksol,profdrain
!66     format(i5,1x,a12,1x,f5.1,8(1x,f6.2),1x,f7.2)


      do 76 icou=1,5
         read (12,*,ERR=120) numsol,epc(icou),HCCF(icou),HMINF(icou), DAF(icou),cailloux(icou),typecailloux(icou), &
                           infil(icou),epd(icou)
76     continue
        call ecrinouvsol
        call mouchsol
71    continue
120   write(*,*) 'error# error in the soil file'
      codemauvais=1
      stop

81      continue
      close(12)

      return
      end
