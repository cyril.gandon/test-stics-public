      subroutine  message
!******************************
      IMPLICIT NONE
      
      include "message.inc"
      
      include "messagesCroira.inc"
      include "messagesDensirac.inc"
      include "messagesDevelop.inc"
      include "messagesIniclim.inc"
      include "messagesLixiv.inc"

!  dernier message 399

! les messages *****************************************************
!
!   apores
! --------      
      mes10 = "profondeur de travail du sol > profhum : impossible !"

! croira
! ------
      mes20 = "transpiration nulle car le syst�me racinaire est en sol sec,  jour"
      mes23 = "profsem > profondeur d'enracinement initiale !"
      mes24 = "incoh�rence dans l'initialisation du front racinaire"

! densirac
! --------
      mes30 = "la culture ne peut pas d�marrer : trop sec � la surface du sol"
      mes31 = "attention: il n'y a plus de racines efficaces"

! develop
! --------
! ML le 28/05/04
      mes32  = "il y a incoherence dans les initialisations:la simulation demarre en fin de vernalisation&
   &   (stade0=lev) mais a une date posterieure a julvernal!"
      mes41  = "lax observ� ant�rieur � amf"
      mes42  = "sen observ� ant�rieur � lax"
      mes43  = "lan observ� ant�rieur � sen"
      mes44  = "mat observ� ant�rieur � drp"
      mes45  = "rec observ� ant�rieur � mat"
      mes46  = "amf observ� ant�rieur � emergence"
      mes47  = "drp ant�rieur � flo"
      mes48  = "       attention vous recoltez avant maturit�"
! Bruno
      mes49 =    "si option �chelle horaire, alors temp�rature air pour	piloter le d�veloppement"
      mes50 = "il y a incoherence dans les initialisations: la simulation demarre en debut de vernalisation&
      & (stade0=dor) mais a une date anterieure a julvernal!"

! grain
! -----
      mes51 = "la plante n'arrive pas � lever"
      mes52 = "il faut diminuer le param�tre TGMIN"
      mes53 = "Vous etes arriv� � drp et vous n avez plus de masec"
! iniclim
! -------
      mes61 = "attention debut simulation posterieur au semis"
      mes62 = " : risque d'erreur dans les dates de sortie"
      mes63 =         "fichiers climatiques incomplets : encha�nement impossible"
      mes64 =    "le debut de la simulation est anterieur au d�but du fichier climatique"
      mes65 = "!! vos irrigations :"
      mes66 = "sont < au debut de la simulation :"
      mes67 = " corrigez cette date ! et relancez"
      mes68 = "le pg est interrompu regardez history.sti"
      mes69 = "!! vos apports d'azote :"
      mes60 =  "La simulation tourne mais le jour d'incorporation des residus > jour de semis"
      mes71 =  "pas les variables climatiques n�cessaires pour option mod�le r�sistif"
      mes72 =  "pas de valeurs d'ETP de r�f�rence en entr�e : calcul de ETP Priestley-Taylor"
      mes73 = "amf observ� et vernalisation impossible donc"
      mes74 = "stlevamf=0 et plantade"
      mes70 = "attention: la simulation demarrant avec la levee,la vernalisation demarre egalement avec &
      & la levee, et non a la germination, comme c est le cas pour les simulations demarrant au semis;  &
      & vous pouvez eventuellement reduire jvcmini, pour tenir compte de ce deficit de jours vernalisant"
      mes76 = "vous ne pouvez pas demarrer au stade dor (debut de dormance) si votre culture n a pas de &
      & besoins en froid"

! inicoupe
! --------
      mes75 = "lai < lairesiduel : on ne fauche pas, coupe "

! initial
! -------
      mes80 = "obstarac trop grand : obstarac = profondeur du sol"
      mes82 = "proftrav > profondeur du sol"
      mes83 = "stade initial non r�pertori�: "
      mes84 = "incoh�rence dans les initialisations"
      mes85 = "QNplante0 = 0 donc valeur critique"
      mes86 = "INITIALISATIONS PLANTE"
      mes87 = "incoh�rence entre stade0 et magrain0"
      mes88 = " profondeur des mesures = "
      mes89 = " sup�rieure � profondeur du sol  = "
      mes180 = "les sols > 1000 cm sont refus�s !"
      mes181 = "donc profmes = profsol"
      mes182 = "zesx > profsol donc zesx=profsol "
      mes171 = "profhum > profondeur du sol ,on fait profhum = profsol"
      mes172 = "enchainement de simulations impossible : mauvaise  dates de debut et de fin"
     mes173  =  "Attention : un fichier climatique pr�c�dent incompletd�finit la fin de la simulation en cours"
! 
      mes191 = "Attention vous avez choisi option taux de recouvr."
      mes192 = "interception eau par le feuillage interdit"
      mes193 = "besoin en eau calcule avec coefficient cultural"
      mes194 = "la culture ne peut pas etre fauchee"
      mes196 =    "la plante p�renne doit avoir des racines (densinitial dans l'usm)"
      mes197  = "profondeur du sol > imperm�able !"
      mes198  = "epd (�paisseur de dispersion, fichier sol) fix� � 1 cm"
      mes199  = "codrainage = 1 implique macroporosite"
      mes330  = "distdrain > ecartdrain/2"
      mes333  =  "impossible de simuler une r�colte �tal�e (nbceuille=2) avec l'option d�termin�e"
      mes334 = "pas d'�claircissage avec d�termin�es"
      mes335 = "fin de dormance forc�e avant le d�but de simulation"
      mes336 = "Attention humcapil  > hcc : risques d'exc�s d'eau"
      mes337 = " splaimin=splaimax ou spfrmin=spfrmax"
      mes338 = "stade0 = snu ou plt impossible avec une plante perenne, mettre stade0 = dor pour avant dormance/vernalisation &
      & et mettre stade0 = lev pour d�bourrement ou d�but croissance foliaire"
      mes339 = "Attention pas de reserve azotee"
      mes340 = "pb codeinitprec=2 et pas enchainement"
      mes341 = "Attention : Vous demarrez sur un sol nu donc vos initialisations plantes ont ete forc�es 0.0 &
      & (�tat de la plante au d�marrage)"
      mes342 = "Attention si demarrage au stade plt vous devez initialiser la plante par (laiplantule et &
      & nbfeuilplant dans les parametres du fichier plante)"
      mes520 ="pb avec lissage profil d'eau"

! irrig
! -----
      mes90 = "ATTENTION: ni pluie ni irrigation au semis"

! domi 12/08/05 nettoyage des message pour tradcution anglaise
! fertil
! ------
      mes91 = "attention : n+NoffN>365"

! leclai
! ------
      mes100 = "!!! attention !!!"
      mes101 = "le fichier des lai observes n'est pas consecutif"

! lecoptim
! --------
      mes102 = " le parametre numero "
      mes103 = " n'a pas ete trouve"

! lecparam
! --------
      mes104 = "PARAMETRES GENERAUX UTILES A LA SIMULATION"
      mes105 = "erreur dans le fichier PARAM.PAR"
      mes106 = "je ne trouve pas le fichier PARAM.PAR"
      mes437 = "STICS Version "

! lecparamv6 DR et ML le 23/03/04
! ----------
      mes105v6 = "erreur dans le fichier PARAMV6.PAR"
      mes106v6 = "je ne trouve pas le fichier PARAMV6.PAR"
      mes107v6 = "Attention. Il semblerait que votre fichier paramv6.par ne correspondent pas � la version de stics que vous &
      &utilisez. A titre informatif, voici ci dessous la liste des variables non initialis�es. Par pr�caution le programme a �t� &
      &arr�t�.  Pour toute question merci de contacter stics@avignon.inra.fr"
    
! LECPLANT  NB le 08/01/02
! --------
      mes107 = "semis trop profond pour cette vari�t�"
      mes108 = "incoh�rence dans les photop�riodes seuil"
      mes109 = "valeur non nulle � stlevsenms "
      mes110 = "error#impossible d'avoir 2 stades cons�cutifs confondus"
          mes111 = "codefauche=1 et msaer0 non nulle pas autoris�"
      mes438 = "En mode de simulation CAS, le codebeso est obligatoirement �gal � 2. On le force."
! Nb le 08/01/02
      mes112 = "Donnez une valeur coh�rente aux sla (entre 150 et 600)"
      mes113 = "rsmin bizarre !"
      mes114 = "hautmax bizarre !"
      mes115 = "hautbase bizarre !"
      mes116 = "hautmax < hautbase !"
      mes117 = "incoh�rence dans le profil type"
      mes118 = "h2ogrmat > h2ograin !"
      mes119 = "draclong faible : risque de probl�mes � l'ex�cution"
      mes120 = "error#2 stades consecutifs a la m�me date : impossible"
      mes121 = "PARAMETRES PLANTE UTILES A LA SIMULATION"
      mes122 = "temp�rature pilote air "
      mes123 = "temp�rature pilote cult "
! Nb le08/01/02
      mes124 = "retard stress hydrique avant stade DRP "
      mes125 = "temin=tcmin et temax=tcmax"
      mes126 = "incoh�rence entre codetransrad et codetradtec"
      mes127 = "extin utilis� pour Esol"
      mes128 = "temp�rature pilote racine=culture"
      mes129 = "temp�rature pilote racine=sol"
      mes130 = "impossibilit� de simuler la dormance sur des cultures annuelles"
      mes131 = "incoh�rence entre le stade initial choisi, stade0=dor,et le fait que cette culture annuelle &
      &n'ait pas de besoin en froid"
      mes132 = "l�gumineuse"
      mes232 = "effet azote sur nb fruits et grains"
      mes133 = "erreur dans le fichier plante. "
      mes134 = "je ne trouve pas le fichier plante "
      mes370 = "nombre de grains maxi=0"
      mes371 = "stoprac non valide"
      mes372 = "iplt non actif car plante p�renne"
      mes373 = "temp�rature jour"
      mes374 = "temp�rature heure"
      mes375 = "avec option laibrut le lairesiduel disparait entierementle premier jour de la senescence"
      mes376 = "si votre culture est plantee utilisez plutot les stades initiaux stade0=snu ou&
      & plt et les parametres d initialisation de la plantule (laiplantule et nbfeuilplant dans  &
      &le fichier desparametres plante,et masecplantule et zracplantule)"
      mes377 = "en mode de simulation 'cultures associ�es' le parametre coderacine des plantes &
      &doit etre egal � 2"
      mes378 = "abscission (fichier plante) doit etre compris entre 0 et 1.0, corrigez et recommencez"

! lecsol   NB le 8/1/02
! ------
      mes135 = "PARAMETRES SOL "
      mes136 = "le numero du sol a pas �t� trouv� !!!"
      mes137 = "erreur dans le fichier sol"
      mes138 = "profondeur d'humification > 60 cm: impossible"
      mes210 = "taux de cailloux dans la mauvaise unit�"
      mes211 = "profhum = 0 : impossible"
      mes360 = "seuils humidit� min�ralisation : min=HMIN et opt=HCC"
      mes361 = "alphaco2 > 2 : impossible"
      mes212 = "attention valeur bizarre de hccf (<1), on arrete"
      mes213 = "attention valeur bizarre de hminf (<1) on arrete"
      mes214 = "profhum  doit etre compris entre 0 et profsol (positif)"
      mes215 = "(1 actif, 0 inactif)"
      mes216 = "vous avez coch� la presence de cailloux et non renseigne le type ou le pourcentage, verifiez votre sol"
! lecstat
! -------
      mes139 = "Erreur dans le fichier STAT.DAT "
      mes442 = "vous avez coch� culture sur 2 ans alors que votre periode de simulation est sur 1 an, corrigez l USM et recommencez"

! lectech     le 8/1/02
! -------
      mes140 = "nom de stade de coupe non r�pertori�"
      mes141 = "d�marrez la culture au minimum 1 jour apr�s"
      mes142 = "le d�but de la simulation (d�finie dans l'USM)"
      mes143 = "erreur dans le fichier des techniques"
      mes1439 = "erreur a l'ouverture du fichier des techniques :"

      mes144 = "pas de date de semis (999),donc stics fera du solnu et donc ressuite=aucun"
      mes145 = "le jour d'incorporation des r�sidus est"
      mes146 = "jultrav ant�rieur au d�but de la simulation : mettez le � 0"
      mes147 = "(pas de residus) ou posterieur au debut de la simulation"
      mes149 = "jultrav=0 donc qres mis � 0"
      mes150 = "votre coderes est bizarre !"
      mes151 = "profondeur d'irrigation nulle ! modifiez locirrig"
      mes152 = "PARAMETRES TECHNIQUES UTILES A LA SIMULATION, fichier: "
      mes380 = "effeuillage par le bas "
      mes381 = "effeuillage par le haut"
      mes382 = "r�colte = moisson"
      mes383 = "r�colte = cueillette"
      mes384 = "1 en fin de cycle"
      mes385 =  "1 tous les "
      mes386 = "jours"
      mes387 = "        r�colte � maturit� physio"
      mes388 = "r�colte quand teneur en eau (fruit) >"
      mes389 = "r�colte quand teneur en eau (fruit) <"
      mes390 = "r�colte si teneur en sucre >"
      mes391 = "r�colte si teneur en azote>"
      mes392 = "r�colte si teneur en huile >"
      mes393 = "locirrig ne peut depasser 50, il sera affect� � 50"
      mes394 = "la valeur de ratiol doit etre comprise entre 0 et 1.0 elle a ete forc�e � 1.0"
      mes395 = "la valeur de ratiolN ne peut etre n�gative elle a ete forc�e � 1.0"
      mes396 = "la valeur de profsem ne peut etre 0 elle a ete forc�e � 1"
      mes511 = "Pas de cultures sous abri en cultures associ�es"
      mes397 = "Vous ne pouvez utiliser un stade reproducteur comme stade automatique de fauche"
      mes398 = "Attention !!  le forcage du stade flo est inutile car les stade reproducteurs sont&
     & calcul�s depuis la drp. Pour forcer les stades reproducteurs , forcez drp"
      mes399 = "le maintient des variables plantes apres la recolte (codemsfinal) avec le mode de &
     & recolte cueillette (codcueille) est incompatible"

! lixiv
! -----
      mes153 = "incompatibilit� entre les valeurs de da et hcc"
      mes154 = "conduisant � des macroporosit�s n�gatives :"
      mes155 = "macropor=(1 moins da/2.66) moins hcc*da/100"

! princip
! -------
      mes156 = " Erreur sur la lecture du fichier des USM !"
      mes157 = "je suis passe dans"
      mes158 = "je suis sorti de rapport"

! lectures
!---------
! domi 12/08/05
      mes433 = "pour les cultures associ�es, Shuttleworth et Wallace obligatoire"
      mes434 = "nometp a ete forc� � sw"

! shutwall
! --------
      mes159 =   "rayonnement nul le jour"
      mes160 =   "il est impossible de simuler des cultures associ�es et un mulch"
      mes161 = "hauteur culture A > hauteur culture P"
      mes162 = "mettre un referebtiel meteo (zr) superieur � la hauteur max de la culture (hautmax)"

! solnu
! -----
      mes163 ="l'�vaporation du sol a �t� stopp�e � cause de la "
      mes164 ="profondeur de sol affect�e (zesx dans param.par)"


! lecprofi
! --------
      mes165 = "pb avec vos dates de sorties de profil, elles ont �t� recalcul�es"

! eauplant
! --------
      mes200 = "pb avec calcul de h2orec magrain et h2orec =0"

! iniclim
! -------
      mes201 = "votre saison de culture fait plus de 1 an "
      mes202 = "l'enchainement est impossible, corrigez"
      mes203 = "la date de fin de simulation et recommencez"
      mes204 = "sans vent et humidit� : impossible de calculer le climat sous abri"
      mes205 ="sans vent et humidit� : impossible de calculer ETP Penman changer option calcul ETP"
      mes206="On a calcul� Priestley-Taylor � la place"

! eauqual
!--------
      mes311="trop forte deshydratation des fruits"
      mes312="Attention 100% d'eau dans les fruits: mauvais param�trage de h2ofrvert ou deshydbase"
      mes313="Biochimie des fruits mal param�tr�e"
! *- marie 23/03/04
      mes314="Attention la deshydratation des fruits ne peut etre calculee car stdrpdes a une valeur &
      &trop faible si vous ne vous interessez pas a la deshydratation des fruits, donnez a stdrpdes la &
      &meme valeur que stdrpmat"

! effeuil
!--------
      mes320 = "rognage trop pr�coce : revoir julrogne, hautrogne ou biorognem"
      mes321 = "effeuillage un peu drastique : il ne reste plus de feuille !"

! fruits
! ------
      mes350= "Votre parcours de d�veloppement est incoh�rent et conduit � simuler  &
      & le remplissage des fruits avant le d�but de croissance v�g�tative"
      mes351=" nombre de jours de retard pour drp effectif ="
      ! levee
!--------
      mes400="manques � la lev�e > densite=0"
! recolte
!--------
      mes401= "il faut deshydbase < 0"
      mes402= "il faut deshydbase > 0"
! repartir
!--------
      mes403  = "s�nescence trop forte : plus de feuilles vertes � LAX"
      mes404   = "attention : enveloppe des fruits (enfruit) ou rapport tigefeuil trop grand"
! bilan
!--------
      mes410="Rognage"
      mes411="Effeuillage"
      mes412="   Mode de r�colte : moisson"
      mes414="   Mode de r�colte : cueillette en fin de cycle"
      mes415="   Mode de r�colte : cueillette, une fois tous les"
      mes416="jours"
      mes417="   R�colte � maturit� physiologique"
      mes418="   R�colte en fonction de la teneur en eau"
      mes419="   R�colte en fonction de la teneur en sucre"
      mes420="   R�colte en fonction de la teneur en prot�ine"
      mes421="   R�colte en fonction de la teneur en huile"
      mes422="   Etalement de la r�colte"

! *- testtransrad
! *----------------
      mes430="Attention : la plante dominante doit utiliser le transfert &
   & radiatif. Mais le codetradtec est �gal � 2, ce qui signife que &
   & les param�tres interrang et orientrang ne sont pas renseign�s. &
   &  Pour faire tourner la simulation, vous devez mettre codetradtec � &
   &  1 et donner une valeur au moins � interrang > 0."

! domi 12/08/05 
      mes432 = " param�tres de transfert radiatif non renseign�s"
      mes435 = "penser � forcer les stades lev et lan"
      mes436 =  "erreur dans le fichier epitaf.lai"


! *- bfroid
! *----------
      mes431="Date de fin de simulation repouss�e. Vernalisation en cours."
! * domi 29/09/05 dans lecplant
!*-----------------------------
      mes439="attention : votre numero de variete est > au nb de varietes"
      mes440="le modele a tourn� avec la derniere variete"

! * domi 04/11/05 dominver
!*-----------------------------
      mes441 = "Attention, inversion de dominance, test sur les parametr  &
    & es de transferts radiatifs de la plante en passe de devenir dominante"
! * domi 16022006 initnonsol
!*-----------------------------
      mes443 = "attention, en enchainement de p�rennes on ne force pas l   &
    & es stades. Vous avez codeinitprec=2 et codeperenne=2"
     
      mes444 = "on ne peut plus repousser le semis "
      mes445 = "nb de jours pendant lequel le semis a �t� repouss�: "
      mes447 = "nb de jours pendant lequel la recolte a �t� repouss�e: "

      mes446 = "tcxstop doit etre > tdmax (si actif, sinon = 100)"
      return
      end
