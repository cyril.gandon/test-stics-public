! ************************************** c
! * le common des param�tres de plante * c
! * lus dans le fichier usm            * c
! ************************************** c



      character*20 ftec(2),fplt(2),flai(2)
      character *3 stade0(2)

! ** pas de distinction ombre/soleil pour les variables de d�part
! --      real lai0(2,2),masec0(2,2),magrain0(2,2),QNplante0(2,2)
      real lai0(2),masec0(2),magrain0(2),QNplante0(2)
      real densinitial(2,5),resperenne0(2),zrac0(2)

      common /usmchar/ftec,fplt,flai,stade0

      common /usmreal/lai0,masec0,zrac0,magrain0,QNplante0,densinitial,resperenne0
