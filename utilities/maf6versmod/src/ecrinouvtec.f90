      subroutine ecrinouvtec

      include 'common.inc'
      include 'comform.inc'
      include 'compartec.inc'
      include 'comparsol.inc'
      include 'comform.inc'
      include 'comtransit.inc'
      include 'comvarplt.inc'
      include 'comparplt.inc'

      common/newfiles/newtec,newplt,newobs,newsol,newcli,newsta
      character*255 newtec,newsol,newobs,newplt,newcli,newsta

      character*110 para
      character*15 new_ressuite


!      real profres1

!
! DR 07022012 on remet le paillage ou il faut 
      if (codepaillage(ipl).eq.2 )then
          naps(ipl)=naps(ipl)+1
          if (typemulch.eq. 1)then
                  coderes(ipl,naps(ipl))=1
                  CsurNres(ipl,naps(ipl))=60
                  Crespc(ipl,naps(ipl))=43
                  Nminres(ipl,naps(ipl))=0
                  eaures(ipl,naps(ipl))=25
            endif
! canne de mais on les mets en autre car on a pas
          if (typemulch.eq. 2)then
                  coderes(ipl,naps(ipl))=9
                  CsurNres(ipl,naps(ipl))=60
                  Crespc(ipl,naps(ipl))=43
                  Nminres(ipl,naps(ipl))=0
                  eaures(ipl,naps(ipl))=25
            endif
          if (typemulch.eq. 3)then
                  coderes(ipl,naps(ipl))=8
                  CsurNres(ipl,naps(ipl))=60
                  Crespc(ipl,naps(ipl))=43
                  Nminres(ipl,naps(ipl))=0
                  eaures(ipl,naps(ipl))=25
            endif
          jultrav(ipl,naps(ipl))=julappmulch
          qres(ipl,naps(ipl))=qmulch0
      endif


! DR 03/02/2012 si on a du paillage plastique
      if(codepaillage(ipl).eq.3) codepaillage(ipl)=2
! DR 03/02/2012 si on a un paillage vegetal on met pas de paillage
      if(codepaillage(ipl).eq.2) codepaillage(ipl)=1

! DR 02/07/08 fractionnement de l'azote
      codefracappN(1)=1
      Qtot_N(1)=100
! DR 29/04/2010 je force codedateappN=2(jours julien ) sinon il prend la valeur de paramv6 (codedateappN en temp. 1=oui, 2=non)
!      codedateappN(1)=2

! dr 08/02/2012 pour la vigne on fait un traitement un peu special
      if((densite(ipl).lt.1.1).and.(codcueille(ipl).eq.2).and.(codrecolte(ipl).eq.2).and.(codeaumin(ipl).eq.2))then
           codepalissage(ipl)=2
      endif


! DR 07/09/2012 je reecrit les ressuite tel que bruno v12 et en anglais
! DR 02/10/2012 par defat ressuite=roots
new_ressuite = "roots"
 if(ressuite(ipl)=='Rien'.or.ressuite(ipl)=='rien')new_ressuite = "roots"
 if(ressuite(ipl)=='Aucun'.or.ressuite(ipl)=='aucun'.or.ressuite(ipl)=='Aucuns'.or.ressuite(ipl)=='aucuns')&
 new_ressuite = "roots"
 if(ressuite(ipl)=='Racine'.or.ressuite(ipl)=='Racines'.or.ressuite(ipl)=='racine'.or.ressuite(ipl)=='racines' )&
 new_ressuite = "roots"
 if(ressuite(ipl)=='Culture'.or.ressuite(ipl)=='culture'.or.ressuite(ipl)=='Cultures'.or.ressuite(ipl)=='cultures')&
 new_ressuite = "whole_crop"
 if(ressuite(ipl)=='Paille'.or.ressuite(ipl)=='paille'.or.ressuite(ipl)=='Pailles'.or.ressuite(ipl)=='pailles')&
 new_ressuite = "straw+roots"
 if(ressuite(ipl)=='Chaume'.or.ressuite(ipl)=='chaume'.or.ressuite(ipl)=='Chaumes'.or.ressuite(ipl)=='chaumes')&
 new_ressuite = "stubble+roots"
!Pailles_residus9 =  "stubble of residu type 9 + roots "
!Pailles_residus10 =  "stubble of residu type 10 + roots "
 if(ressuite(ipl)=='Bois'.or.ressuite(ipl)=='bois')new_ressuite = "prunings"

! DR 01/10/2012 si le codcueille=0 on le met � 1
if(codcueille(ipl).eq.0)codcueille(ipl)=1


      open (1,file='config\trame.tec',status='old')
      open (36,file=newtec,recl=150,status='unknown',err=9999)
   12 format(a89,i3)
!  112 format(a89,i2)
  111 format(a89,i2)

   14 format(a89,2i3,5f7.2)
   24 format(a89,i3,2f6.2)
   15 format(a89,f10.5)
   16 format(a89,a3)
   18 format(a72,i1,17x,i4,4f7.2)
   28      format(68x,'ope',i1,17x,i4,4f7.2)
   128      format(68x,'ope',i1,17x,f4.0,4f8.2)
   !   27      format(a89,i3,2f7.2,i4,5f7.2)
!   20 format(a72,i1,17x,f7.1,4f7.2)
   19 format(a89,a15)
!   13 format(a59,i2)
   23 format(a89)

!   j'y met les parametres que je ne connais pas
!  demander � nadine les valeurs
!
      ipl=1

! DR 21022011 on teste sur les valeurs de qres pour connaitre le nb d'apport de residus
      napS_recalc=0.
            do i=1, naps(ipl)
              if (qres(ipl,i).ne.0)then
                  napS_recalc = napS_recalc + 1
              endif
            enddo
             read(1,'(a90)') para 
            write (36,23)para
            read(1,'(a89)')para
            write (36,12) para,napS_recalc
             read(1,'(a100)') para 
            write (36,'(a100)')para
            read(1,'(a89)')para
! DR 18/02/2011 on eclate le travail du sol et apport de residus en 2 trableaux
              do 201 i=1,napS(ipl)
              if(qres(ipl,i).gt.0)then
                if(coderes(ipl,i).eq.8)coderes(ipl,i)=9
                    write(36,14) para,jultrav(ipl,i),coderes(ipl,i) &
              ,qres(ipl,i), Crespc(ipl,i),CsurNres(ipl,i)  &
                  ,Nminres(ipl,i),eaures(ipl,i)
                endif
 201              continue




             read(1,'(a90)') para 
            write (36,23)para
            read(1,'(a89)')para
            write (36,12) para,napS(ipl)
             read(1,'(a100)') para 
            write (36,'(a100)')para
            read(1,'(a89)')para
              do 200 i=1,napS(ipl)
                  write(36,24) para,jultrav(ipl,i),profres(ipl,i) &
              ,proftrav(ipl,i)        
 200              continue

            read(1,'(a89)')para
            write (36,12)para
            read(1,'(a89)')para
            write (36,12) para,iplt(ipl)
            read(1,'(a89)')para
            write (36,15) para,profsem(ipl)
            read(1,'(a89)')para
            write (36,15) para,densite(ipl)
            read(1,'(a89)')para
              write (36,111) para,variete(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12) para,codetradtec(ipl)
            read(1,'(a89)')para
            write (36,15) para,interrang(ipl)
            read(1,'(a89)')para
            write (36,15) para,orientrang(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12) para,codedecisemis(ipl)
            read(1,'(a89)')para
            write (36,12) para,nbjmaxapressemis(ipl)
            read(1,'(a89)')para
            write (36,12) para,nbjseuiltempref(ipl)
            do i=1,4
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12) para,codestade(ipl)
            read(1,'(a89)')para
            write (36,12) para,ilev(ipl)
            read(1,'(a89)')para
            write (36,12) para,iamf(ipl)
            read(1,'(a89)')para
            write (36,12) para,ilax(ipl)
            read(1,'(a89)')para
            write (36,12) para,isen(ipl)
            read(1,'(a89)')para
            write (36,12) para,ilan(ipl)
            read(1,'(a89)')para
            write (36,12) para,iflo(ipl)
            read(1,'(a89)')para
            write (36,12) para,idrp(ipl)
            read(1,'(a89)')para
            write (36,12) para,imat(ipl)
            read(1,'(a89)')para
            write (36,12) para,irec(ipl)
            read(1,'(a89)')para
            write (36,12) para,irecbutoir(ipl)
! irrigations 
            read(1,'(a89)')para
            write (36,12)para
            read(1,'(a89)')para
            write (36,15) para,effirr(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12) para,codecalirrig(ipl)
            read(1,'(a89)')para
            write (36,15) para,ratiol(ipl)
            read(1,'(a89)')para
            write (36,15) para,dosimx(ipl)
            read(1,'(a89)')para
            write (36,15)para,doseirrigmin(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12) para,codedateappH2O(ipl)
            read(1,'(a89)')para
            write (36,12) para,nap(ipl)
            read(1,'(a89)')para
            write (36,12)para
            if(nap(ipl).eq.0)      read(1,'(a89)')para 
            if (nap(ipl).gt.0) then
                do 21 i=1,nap(ipl)
                  if(i.eq.1) read(1,'(a72)')para
                  write(36,18) para,i,julapI(ipl,i),doseI(ipl,i)
   21           continue
            endif
            do i=1,4
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12) para,codlocirrig(ipl)
            read(1,'(a89)')para
            write (36,12) para,locirrig(ipl)
            read(1,'(a89)')para
            write (36,15) para,profmes(ipl)
            read(1,'(a89)')para
            write (36,12)para
            read(1,'(a89)')para
            write (36,12) para,engrais(ipl)
            read(1,'(a89)')para
            write (36,15) para,concirr(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codedateappN(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codefracappN(ipl)
            read(1,'(a89)')para
            write (36,12)para,Qtot_N(ipl)
            read(1,'(a89)')para
            write (36,12) para,napN(ipl)
            read(1,'(a89)')para
            write (36,12)para
            if(napN(ipl).eq.0)      read(1,'(a89)')para 
            if (napN(ipl).gt.0) then
                do 22 i=1,napN(ipl)
                  if(i.eq.1) read(1,'(a72)')para
                  if (codedateappN(ipl).eq.2)then
                       write(36,18) para,i,julapN(ipl,i),doseN(ipl,i)
                  else
                       write(36,18) para,i,upvttapN(ipl,i),doseN(ipl,i)
                  endif
   22           continue
            endif
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codlocferti(ipl)
            read(1,'(a89)')para
            write (36,12)para,int(locferti(ipl))
! DR 06/07/2011 pass� dans paramv6
!            do i=1,3
!             read(1,'(a90)') para 
!            write (36,23)para
!             enddo
!            read(1,'(a89)')para
!            write (36,12)para,codecalferti
!            read(1,'(a89)')para
!            write (36,15)para,ratiolN
!            read(1,'(a89)')para
!            write (36,15)para,dosimxN
!            do i=1,3
!             read(1,'(a90)') para 
!            write (36,23)para
!             enddo
!            read(1,'(a89)')para
!            write (36,12)para,codetesthumN
            read(1,'(a89)')para
            write (36,19)para,trim(new_ressuite)
            do i=1,4
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codcueille(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,nbcueille(ipl)
            read(1,'(a89)')para
            write (36,12)para,int(cadencerec(ipl))
            do i=1,6
             read(1,'(a90)') para 
            write (36,23)para
             enddo
              read(1,'(a89)')para
            write (36,12)para,codrecolte(ipl)
             do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codeaumin(ipl)
            read(1,'(a89)')para
            write (36,15)para,h2ograinmin(ipl)
            read(1,'(a89)')para
            write (36,15)para,h2ograinmax(ipl)
            read(1,'(a89)')para
            write (36,15)para,sucrerec(ipl)
            read(1,'(a89)')para
            write (36,15)para,CNgrainrec(ipl)
            read(1,'(a89)')para
            write (36,15)para,huilerec(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,coderecolteassoc
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codedecirecolte(ipl)
            read(1,'(a89)')para
            write (36,12)para,nbjmaxapresrecolte(ipl)
!             read(1,'(a90)') para 
!            write (36,23)para
!            read(1,'(a89)')para
!            write (36,15)para,hautmaxtec(ipl)
!            read(1,'(a89)')para
!            write (36,15)para,largtec(ipl)
            do i=1,4
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codefauche(ipl)
! dr 19/08/2011 deplac� ici
            read(1,'(a89)')para
            write (36,15)para,mscoupemini(ipl)
            do i=1,4
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codemodfauche(ipl)
            read(1,'(a89)')para
            write (36,15)para,hautcoupedefaut(ipl)
            read(1,'(a89)')para
            write (36,16)para,stadecoupedf(ipl)
            read(1,'(a89)')para
!    les coupes
            write (36,12)para,nbcoupe2
            read(1,'(a89)')para
            write (36,12)para
                 do 81 i=1,nbcoupe2
                   write(36,28)i,julfauche(ipl,i),hautcoupe(ipl,i),&
               lairesiduel(ipl,i),msresiduel(ipl,i),anitcoupe(ipl,i) 
   81            continue 
            read(1,'(a89)')para
            write (36,12)para,nbcoupe3
            read(1,'(a89)')para
            write (36,12)para
                 do 83 i=1,nbcoupe3
                   write(36,128)i,tempfauche(ipl,i),hautcoupe2(ipl,i),&
                  lairesiduel2(ipl,i),msresiduel2(ipl,i),anitcoupe2(ipl,i)
   83            continue 
! dr 19/08/2011 deplac� plus haut 
!            read(1,'(a89)')para
!            write (36,15)para,mscoupemini(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codepaillage(ipl)
!            read(1,'(a89)')para
!            write (36,15)para,qmulch0
!            read(1,'(a89)')para
!            write (36,12)para,julappmulch
!            read(1,'(a89)')para
!            write (36,12)para,typemulch
            read(1,'(a89)')para
            write (36,15)para,couvermulch
            read(1,'(a89)')para
            write (36,15)para,albedomulchtec
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codrognage(ipl)
            read(1,'(a89)')para
            write (36,15)para,largrogne(ipl)
            read(1,'(a89)')para
            write (36,15)para,hautrogne(ipl)
            read(1,'(a89)')para
            write (36,15)para,biorognem(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codcalrogne(ipl)
            read(1,'(a89)')para
            write (36,12)para,julrogne(ipl)
            read(1,'(a89)')para
            write (36,15)para,margerogne(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codeclaircie(ipl)
            read(1,'(a89)')para
            write (36,12)para,juleclair(ipl)
            read(1,'(a89)')para
            write (36,15)para,nbinfloecl(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codeffeuil(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codhauteff(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codcaleffeuil(ipl)
            read(1,'(a89)')para
            write (36,15)para,laidebeff(ipl)
            read(1,'(a89)')para
            write (36,15)para,effeuil(ipl)
            read(1,'(a89)')para
            write (36,12)para,juleffeuil(ipl)
            read(1,'(a89)')para
            write (36,15)para,laieffeuil(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codetaille(ipl)
            read(1,'(a89)')para
            write (36,12)para,jultaille(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codepalissage(ipl)
            read(1,'(a89)')para
            write (36,15)para,hautmaxtec(ipl)
            read(1,'(a89)')para
            write (36,15)para,largtec(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codabri
            read(1,'(a89)')para
            write (36,15)para,transplastic
            read(1,'(a89)')para
            write (36,15)para,surfouvre1
            read(1,'(a89)')para
            write (36,12)para,int(julouvre2)
            read(1,'(a89)')para
            write (36,15)para,surfouvre2
            read(1,'(a89)')para
            write (36,12)para,int(julouvre3)
            read(1,'(a89)')para
            write (36,15)para,surfouvre3
            do i=1,4
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codeDST(ipl)
            read(1,'(a89)')para
            write (36,15)para,dachisel(ipl)
            read(1,'(a89)')para
            write (36,15)para,dalabour(ipl)
            read(1,'(a89)')para
            write (36,15)para,rugochisel(ipl)
            read(1,'(a89)')para
            write (36,15)para,rugolabour(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codeDSTtass(ipl)
            read(1,'(a89)')para
            write (36,15)para,profhumsemoir(ipl)
            read(1,'(a89)')para
            write (36,15)para,dasemis(ipl)
            read(1,'(a89)')para
            write (36,15)para,profhumrecolteuse(ipl)
            read(1,'(a89)')para
            write (36,15)para,darecolte(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)')para
            write (36,12)para,codeDSTnbcouche(ipl)

      goto 61
 9999    write(*,*)'error#',mes1439,newtec
61    continue

      close(36)
      close(1)
      return
      end
