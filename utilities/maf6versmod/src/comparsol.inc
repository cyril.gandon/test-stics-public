
      integer itrav1,itrav2,nstoc0,profcalc
    
      real Ndeneng,Norgeng,Nvolorg,Nvoleng,QNdeneng,QNorgeng,QNvolorg
      real Nvolatorg,Qminrcult,cumvminr,qdraincum,qdrain,remontee
      real hmax,hnappe,hph,hpb,qlesd,Ldrains,nitrifj  
      real profnappe,condenit,concno3les,azlesd, AZnit(5)
      real QNvoleng  
      real sumes00,sumes10,sumes20,supres0,ses2j00,sesj00,smes020,stoc0
      real minoffrN 
      real NO3init(5) 
      real CO2sol,QCO2sol,vminr,aevap 
      
      real amm(1000),nit(1000)
      
      common /solint/itrav1,itrav2,nstoc0,profcalc
      
      common /solreal/amm,nit,Ndeneng,Norgeng,Nvolorg,Nvoleng,QNdeneng,&
      QNorgeng,QNvolorg,Nvolatorg,Qminrcult,cumvminr,qdraincum,qdrain,&
      remontee,hmax,hnappe,hph,hpb,qlesd,Ldrains,nitrifj,profnappe,&
      condenit,concno3les,azlesd,AZnit,QNvoleng,sumes00,sumes10,&
      sumes20,supres0,ses2j00,sesj00,smes020,stoc0,minoffrN,NO3init,&
      CO2sol,QCO2sol,vminr,aevap

! ** parametres generaux

      integer codeh2oact,codeinnact,codecaltemp,codeminhum,codernet
      integer profdenit,codhnappe,codeminopt,codeprofmes
      integer codeactimulch
      integer codeclichange,codetycailloux
      integer codetypeng,codetypres,codemulch,codedenit
      integer iniprofil
      integer typemulch,julappmulch

      
      real aclim,beta,lvopt,rayon
      real FHUM,FMIN1,FMIN2,FMIN3,Wh,concrr,FTEM,TREF,zr
      real difN,ra,plNmin,proprac,coefb,NH3ref,aangst,bangst
      real zesx,cfes,irrlev,coefdevil
      real vpotdenit,distdrain,khaut,dacohes,daseuilhaut
      real daseuilbas,QNpltminINN,ftem1,ftem2,ftem3,ftem4,finert
      real pHminvol,pHmaxvol,Vabs2, Xorgmax,hminm,hoptm,hminn,hoptn
      real fpHnx,pHminnit,pHmaxnit,tnitopt1,tnitopt2,tnitmax,tnitmin
      real albveg,CO2_param,alphaCO2(2),altistation,altisimul,gradtn,gradtx
      real altinversion,gradtninv,cielclair,ombragetx,pminruis
      real z0solnu,diftherm,Bformnappe,rdrain,hcccx(10),masvolcx(10)
      real engamm(8),voleng(8),orgeng(8),deneng(8),kbio(21),yres(21)
      real akres(21),bkres(21),awb(21),bwb(21),cwb(21),ahres(21)
      real bhres(21),CNresmin(21),CNresmax(21)
! DR 30/07/2012 je rajoute le param de bruno CroCo etc
      real CroCo(21),y0msrac,fredNsup,Primingmax,P_CsurNsol,P_penterui

! ** domi - 21/10/2004 - on a pass� la typologie des mulch en tableau pour pouvoir
! *-   deplacer lecparam avant lectech 
! *-   (le 0 de albedomulch correspond au typemulch=0 --> codepaillage=3 plastique)
      real qmulchruis0(0:21),decomposmulch(21),mouillabilmulch(21)
      real kcouvmlch(21),albedomulch(0:21)
      real qmulch0,couvermulch
! ** NB le 13/02/2003 pour Guenaelle
      

        
      common /param/aclim,beta,lvopt,rayon,concrr,FTEM,TREF,FHUM,&
               FMIN1,FMIN2,FMIN3,iniprofil,Wh,difN,ra,plNmin,proprac,&
               coefb,zesx,cfes,irrlev,coefdevil,  &          
               profdenit,vpotdenit,distdrain
      common /paramcodes/codedenit,codhnappe,codecaltemp,codeh2oact,&
               codeinnact,&
               codeminopt,codeprofmes,codeactimulch, &
               codeminhum,codernet,codeclichange,&
               codetycailloux,codetypeng,codemulch

      common /param2/khaut,dacohes,daseuilhaut,daseuilbas,QNpltminINN,&
               ftem1,ftem2,ftem3,ftem4,finert,pHminvol,pHmaxvol,Vabs2, &
               Xorgmax,hminm,hoptm,hminn,hoptn,zr,NH3ref,aangst,bangst,&
               albveg,CO2_param,alphaCO2,altistation,altisimul,gradtn,&
                gradtx
      common/nb0304/fpHnx,pHminnit,pHmaxnit,tnitopt1,tnitopt2,tnitmax,&
               tnitmin
      common/nb0504/altinversion,gradtninv,cielclair,ombragetx 
      common/param3/pminruis,diftherm,Bformnappe,rdrain,masvolcx,&
               hcccx
      common/rugo/z0solnu
      common/paramengrais/engamm,voleng,orgeng,deneng
      common/parammine1/akres,bkres,awb,bwb,cwb,ahres,bhres,kbio,yres, &
               CNresmin,CNresmax,codetypres
      common /parammulch/qmulchruis0,decomposmulch,kcouvmlch,&
               mouillabilmulch,albedomulch,qmulch0,couvermulch
      common /paramBM2012/y0msrac,fredNsup,Primingmax,CroCo,P_CsurNsol,&
               P_penterui

      
 
! ** parametres sol
      character*12 typsol
        
      integer numsol,typecailloux(5),epd(5)
      integer codecailloux,codemacropor,codefente,codrainage,codenitrif
      integer coderemontcap  
  
      integer izcel(5),izc(5),ncel,icel(0:1000)
  
      real  NO3initf(5)
      
      real profhum,pH,q0,ruisolnu,obstarac,profimper,ecartdrain,Ksol
      real profdrain,DAF(5),HMINF(5),HCCF(5),da(5),epc(5),hcc(5),hmin(5)
      real cailloux(5),infil(0:5)
      real calc,argi,Norg,profsol,albedo,humcapil,capiljour,concseuil

      common /parsolcodes/codecailloux,codemacropor,codefente, &
               codrainage,codenitrif,coderemontcap

      common /parsolint/izcel,izc,ncel,icel,typemulch,julappmulch

      common /parsol/da,epc,hcc,hmin,calc,argi,Norg,profsol,albedo,epd,&
               capiljour,humcapil,profhum,pH,concseuil,q0,ruisolnu,&
               obstarac,profimper,ecartdrain,Ksol,profdrain,numsol        
      common /parsolchar/typsol
      common /parasol/DAF,HMINF,HCCF,NO3initf
      common /nouvsol/cailloux,typecailloux,infil