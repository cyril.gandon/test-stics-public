      subroutine ecrinouvcli(nbjourclim)
!  **********************************
!   transformation des fichiers climatiques
! on ecrit le fichier climat et le fichier station
!
      common/newfiles/newtec,newplt,newobs,newsol,newcli,newsta
      character*255 newtec,newsol,newobs,newplt,newcli,newsta
      character*7 station
      integer imois(366),ijour(366),ijul(366)
      common/climat/imois,ijour,ijul,station
            
      include 'common.inc'
      include 'comparsol.inc'
      include 'comform.inc'
      include 'comclim.inc'
!      include 'message.inc'
      integer nbjourclim

!      integer jul
      character*110 para


! DR 17/06/2011 les parametres ayant chang� de fichier
! issus de paramv6
! zr 2.5
! NH3ref 0.0
! ra 50.00000
! aangst 0.18
! bangst 0.62
! albveg 0.23
! codernet 2
! codecaltemp 2
! codeclichange 1
! codaltitude 1
! altistation 440.0
! altisimul 800.0
! gradtn -0.50
! gradtx -0.55
! altinversion 500.0
! gradtninv +1.3
! cielclair 0.8
! codadret 1
! ombragetx -1.4
! coefdevil 0.7
! aks 6.0
! bks 0.5
! cvent 0.16
! phiv0 4e-3
! coefrnet 0.59
      



      if(nometp.eq.'pe'.or.nometp.eq.'PE')codeetp=1
      if(nometp.eq.'pc'.or.nometp.eq.'PC')codeetp=2
      if(nometp.eq.'sw'.or.nometp.eq.'SW')codeetp=3
      if(nometp.eq.'pt'.or.nometp.eq.'PT')codeetp=4


! j'ecris le fichier climat
         open(36,file=newcli)

      n=1
! 12/11/07 pour benj les fichiers disposent deja du co2 donc je le lis
!      co2=330
      if(((annee(1)/4)*4).eq.annee(1))then
            nbjan=366
      else
            nbjan=365
      endif
      do 150 i=1,nbjourclim 
        write (36,4201)station,annee(i),imois(i),ijour(i),ijul(i),&
          ttmin(i),ttmax(i),ttrg(i),ttetp(i),ttrr(i),&
          ttvent(i),ttpm(i),co2(i)
  150 continue

4201    format(a7,1x,i4,1x,i2,1x,i2,1x,i3,1x,8(f7.1))

      close(36)
! j'ecris le fichier station

      open (1,file='config\trame.stat',status='old')
      open (36,file=newsta,recl=150,status='unknown')
   12 format(a89,i3)
!   14 format(a89,i3,f6.2,i3,4f6.2)
!   15 format(a89,f10.5)
   13 format(a89,f10.5)
!   16 format(a89,a3)
!   18 format(a72,i1,17x,i4,5f9.2)
!   19 format(a89,a7)
!   23 format(a89)
   25 format(a110)
! *** weather station ***
            read(1,'(a89)') para 
            write (36,13) para
            read(1,'(a89)') para 
            write (36,13) para,zr
            read(1,'(a89)') para 
            write (36,13) para,NH3ref
! 04/01/2012 on met ra,albveg,aangst,bangst et corectrosee dans microclimate
!            read(1,'(a89)') para
!            write (36,*) para,ra
!            read(1,'(a89)') para
!            write (36,*) para,aangst
!            read(1,'(a89)') para
!            write (36,*) para,bangst
!            read(1,'(a89)') para
!            write (36,*) para,albveg
! 04/01/2012 parsurrg va dans les parametres generaux
!            read(1,'(a89)') para
!            write (36,*) para,parsurrg
             read(1,'(a89)') para 
            write (36,13)para,latitude
             read(1,'(a89)') para 
            write (36,13)para,patm
             read(1,'(a89)') para
            write (36,*)para,aclim


! *** climate ***
            do i=1,6
            read(1,'(a110)') para 
            write (36,25)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeetp
             read(1,'(a89)') para 
            write (36,13)para,alphapt
            do i=1,3
            read(1,'(a110)') para 
            write (36,25)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeclichange
            do i=1,3
            read(1,'(a110)') para 
            write (36,25)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codaltitude
            read(1,'(a89)') para 
            write (36,13)para,altistation
            read(1,'(a89)') para 
            write (36,13)para,altisimul
            read(1,'(a89)') para 
            write (36,13)para,gradtn
            read(1,'(a89)') para 
            write (36,13)para,gradtx
            read(1,'(a89)') para 
            write (36,13)para,altinversion
            read(1,'(a89)') para 
            write (36,13)para,gradtninv
            read(1,'(a89)') para 
            write (36,13)para,cielclair
            do i=1,3
            read(1,'(a110)') para 
            write (36,25)para
             enddo
            read(1,'(a89)') para 
            write (36,12)para,codadret
            read(1,'(a89)') para 
            write (36,13)para,ombragetx
! *** Microclimate
            read(1,'(a110)') para 
            write (36,25)para
            read(1,'(a89)') para 
            write (36,13) para,ra
            read(1,'(a89)') para 
            write (36,13) para,albveg
            read(1,'(a89)') para 
            write (36,13) para,aangst
            read(1,'(a89)') para 
            write (36,13) para,bangst
             read(1,'(a89)') para 
            write (36,13)para,corecTrosee
            do i=1,3
            read(1,'(a110)') para 
            write (36,25)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codecaltemp
            do i=1,3
            read(1,'(a110)') para 
            write (36,25)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codernet
! *** climat sous abri ***
            read(1,'(a110)') para 
            write (36,25)para
            read(1,'(a89)') para 
            write (36,13)para,coefdevil
            read(1,'(a89)') para 
            write (36,13)para,aks
            read(1,'(a89)') para 
            write (36,13)para,bks
            read(1,'(a89)') para 
            write (36,13)para,cvent
            read(1,'(a89)') para 
            write (36,13)para,phiv0
            read(1,'(a89)') para 
            write (36,13)para,coefrnet
!100   continue
      close(36)
      close(1)



!/////  c'est finit pour un  climat ////
      return
      end
