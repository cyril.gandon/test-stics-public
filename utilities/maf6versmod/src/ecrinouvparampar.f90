      subroutine ecrinouvparampar
      include 'common.inc'
      include 'comform.inc'
      include 'comparplt.inc'
      include 'comvarplt.inc'
      include 'comtransit.inc'
      include 'comparsol.inc'
      include 'comclim.inc'

      common/newfiles/newtec,newplt,newobs,newsol,newcli,newpar
      character*255 newtec,newsol,newobs,newplt,newcli,newpar
!      
      character*110 para


!      real prophumtasssem, prophumtassrec

      open (1,file='config\trame.par',status='old')

!****************************************
! DR 17/06/2011 les parametres nouveaux
! on les a lu dans maj_paramv6.par
!            FTEMh=0.12
!            FTEMha=25.0
!      tref devient      trefh=15.0
!            trefh=15.0
!            FTEMr=0.103
!            FTEMra=12.0
!            trefr=15.0
!  tnitopt1 et tnitopt2 se rejoignent en tnitopt
!  non tnitopt 2 revient (BM) ! , je remets comme avant
!         tnitopt=25.0
!         rationit=0.05
!         ratiodenit=0.1
!                phvols=8.6
!            prophumtassrec=1.0
!            prophumtasssem=1.2
!            fhminsat=0.5
!            fredlN=0.5
!            fredkN=0.25
!            fNCbiomax=0.4
!            Cmulchdec=1.0
!            Qmulchdec=1.0

!****************************************
! les parametres ayant ete supprimes
!       codeminhum
!   tnitopt1 et tnitopt2
!! !! non reviennent
!****************************************
! les parametres ayant ete renomm�s
! FpHnx devient Fnx

!****************************************
! DR 17/06/2011 les parametres ayant chnag� de valeur 
! masvolcx (8) pass� de 2 � 2.2 et hcccx(8) pass� de 25 � 5  ??????????????
! DR 11/01/2011 ces parametres ont chang�s car l'equation a chang� entre 6.9 et 7.1
!            fmin1=6e-4
!          fmin2=0.0272
!          fmin3=0.0167
!            tnitmax= passe de 30 � 45

! les parametres residus 1 et 2 (residus de culture et residus de culture intermediaire)
!            akres(1)=0.098
!            bkres(1)=0.76
!            awb(1)=15.4
!            bwb(1)=-76
!            ahres(1)=0.73
!            bhres(1)=10.2
!            kbio(1)=0.0076
!            akres(2)=0.098
!            bkres(2)=0.76
!            awb(2)=15.4
!            bwb(2)=-76
!            ahres(2)=0.73
!            bhres(2)=10.2
!            kbio(2)=0.0076
! nouveaux residus  de 9 � 14
! 1 nouveau type de mulch CI

!****************************************
! DR 17/06/2011 les parametres ayant chang� de fichier
! ** issus de paramv6
!    codeoutscient 2
!    codeseprapport 1
!    separateurrapport ,
!    codesensibilite 2
!    dltamsminsen 0.005
!    masecmeta 0.04
!    codefxn 2
!    psihumin -1.5
!    psihucc -0.03
!    prophumtasssem 1.2
!    prophumtassrec 1.0
!    proflabour  15.0
!    proftravmin  5.0
    
! issus de la plante
      codesymbiose=2
      parsurrg=0.48

!****************************************
! les parametres ayant ete deplac�s
! ** vers station
! zr,NH3ref,ra,aangst,bangst,albveg
! codernet 2
! codecaltemp 2
! codeclichange 1
! codaltitude 1
! altistation 440.0
! altisimul 800.0
! gradtn -0.50
! gradtx -0.55
! altinversion 500.0
! gradtninv +1.3
! cielclair 0.8
! codadret 1
! ombragetx -1.4
! coefdevil 0.7
! aks 6.0
! bks 0.5
! cvent 0.16
! phiv0 4e-3
! coefrnet 0.59

! ** vers param.sol
! zesx,cfes, z0solnu


! DR 11/01/2011 ceux la on les force � une valeur correcte
!  attention on prend les valeurs du param.par utilisateur pour les codes 
! pour la version param_gen_ref.xml voir avec nadine poutr les valeurs
!            codeinitprec=2
!            codemsfinal=2
!            codeprofmes=1
!            codeactimulch=2
!            iniprofil=2
!            daseuilhaut= 2.0
!            daseuilbas=1.4
!            concrr=0.01
!            plNmin=10.0
!            QNpltminINN=0.0
!            hminm=0.25
!            hoptm=0.9
!            hminn=0.67
!            hoptn=1.0
!            aclim=20

   12 format(a89,i5)
! dr 10/07/02013 je renprends les formats plus precis
!   15 format(a89,f10.5)
!   13 format(a89,f10.2)
   15 format(a89,f12.7)
   13 format(a89,f12.4)

   16 format(a89,a3)
!   18 format(a72,i1,17x,i4,5f9.2)
!   19 format(a89,a7)
   23 format(a89)
!   17 format(a59,i1)
!   20 format(a100)


! j'ai lu les nouveaux parametres dans trame.par
            do i=1,4
            read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12) para,codeinnact
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)') para 
            write (36,12)para,codeh2oact
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codeminopt
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,iniprofil
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codeprofmes
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codeinitprec
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codemsfinal
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codeactimulch
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codefrmur
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codemicheur
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codeoutscient
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codeseprapport
             read(1,'(a89)') para 
            write (36,16)para,separateurrapport
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
            enddo
            read(1,'(a89)') para 
            write (36,12)para,codesensibilite
! dr 21/09/2012 pour flagecriture (remplacement de codesig)on ecrit ce qu'on lit
             read(1,'(a110)') para
            write (36,'(a100)')para
! 04/01/2012 on met parsurrg dans les parametres generaux
             read(1,'(a90)') para 
            write (36,23)para
            read(1,'(a89)') para 
            write (36,15)para,parsurrg
!
             read(1,'(a90)') para 
            write (36,23)para
            read(1,'(a89)') para 
            write (36,15)para,coefb
             read(1,'(a89)') para 
            write (36,15)para,proprac
! DR 30/07/2012 j'ajoute les nouveaux param BM
             read(1,'(a89)') para
            write (36,15)para,y0msrac
             read(1,'(a89)') para 
            write (36,15)para,khaut
!c DR 30/01/2012 passe dans le fichier plante  avec codestrphot
!             read(1,'(a89)') para
!            write (36,15)para,dltamsminsen(1)
!             read(1,'(a89)') para
!            write (36,15)para,alphaphot(1)
             read(1,'(a90)') para 
            write (36,23)para
             read(1,'(a89)') para 
            write (36,15)para,dacohes
             read(1,'(a89)') para 
            write (36,15)para,daseuilbas
             read(1,'(a89)') para 
            write (36,15)para,daseuilhaut
             read(1,'(a90)') para 
            write (36,23)para
             read(1,'(a89)') para 
            write (36,15)para,beta
             read(1,'(a89)') para 
            write (36,15)para,lvopt
             read(1,'(a89)') para 
            write (36,15)para,rayon
             read(1,'(a89)') para 
            write (36,15)para,difN
             read(1,'(a89)') para 
            write (36,15)para,concrr
             read(1,'(a89)') para 
            write (36,15)para,plNmin
             read(1,'(a89)') para 
            write (36,15)para,irrlev
             read(1,'(a89)') para 
            write (36,15)para,QNpltminINN
!c DR 30/01/2012 passe dans le fichier plante  avec codeplisoleN
!c             read(1,'(a89)') para
!c            write (36,15)para,masecmeta
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codesymbiose(1)
            do i=1,4
             read(1,'(a90)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codefxn
             read(1,'(a89)') para 
            write (36,12)para
!c pour les nouevaux parametres je mets les valeurs lu dans maj_paramv6
             read(1,'(a89)') para 
            write (36,15)para,FTEMh
             read(1,'(a89)') para 
            write (36,15)para,FTEMha
             read(1,'(a89)') para 
            write (36,15)para,trefh
             read(1,'(a89)') para 
            write (36,15)para,FTEMr
             read(1,'(a89)') para 
            write (36,15)para,FTEMra
             read(1,'(a89)') para 
            write (36,15)para,trefr
             read(1,'(a89)') para 
            write (36,15)para,FINERT
!c fmin1,fmin2,fmin3 ont chang�s et lu dans maj_paramv6
             read(1,'(a89)') para 
            write (36,15)para,FMIN1
             read(1,'(a89)') para 
            write (36,15)para,FMIN2
             read(1,'(a89)') para 
            write (36,15)para,FMIN3
             read(1,'(a89)') para 
            write (36,15)para,Wh
             read(1,'(a89)') para 
            write (36,15)para,pHminvol
             read(1,'(a89)') para 
            write (36,15)para,pHmaxvol
             read(1,'(a89)') para 
            write (36,15)para,Vabs2
             read(1,'(a89)') para 
            write (36,15)para,Xorgmax
!c hminm,hoptm,himin,hoptn et lu dans maj_paramv6
             read(1,'(a89)') para 
            write (36,15)para,hminm
             read(1,'(a89)') para 
            write (36,15)para,hoptm
             read(1,'(a89)') para 
            write (36,15)para,hminn
             read(1,'(a89)') para 
            write (36,15)para,hoptn
             read(1,'(a89)') para 
            write (36,15)para,FpHnx
             read(1,'(a89)') para 
            write (36,15)para,pHminnit
             read(1,'(a89)') para 
            write (36,15)para,pHmaxnit
             read(1,'(a89)') para 
            write (36,15)para,tnitmin
             read(1,'(a89)') para 
            write (36,15)para,tnitopt1
             read(1,'(a89)') para
            write (36,15)para,tnitopt2
!c tnitmax a chang� je mets les valeurs de la trame
             read(1,'(a89)') para 
            write (36,15)para,tnitmax
!c rationit et ratiodenit sont nouevaux et lu dans maj_paramv6
             read(1,'(a89)') para 
            write (36,15)para,rationit
             read(1,'(a89)') para 
            write (36,15)para,ratiodenit
!c alphaph,dphvolmax,phvols,sont nouevaux et lu dans maj_paramv6
             read(1,'(a89)') para 
            write (36,15)para,alphaph
             read(1,'(a89)') para 
            write (36,15)para,dphvolmax
             read(1,'(a89)') para 
            write (36,15)para,phvols
             read(1,'(a89)') para 
            write (36,15)para,fhminsat
             read(1,'(a89)') para 
            write (36,15)para,fredkN
             read(1,'(a89)') para 
            write (36,15)para,fredlN
             read(1,'(a89)') para 
            write (36,15)para,fncbiomin
             read(1,'(a89)') para
            write (36,15)para,fredNsup
             read(1,'(a89)') para
            write (36,15)para,Primingmax
!c on en est la !!!!!!!!!!!!!!!!!!!!!!!
             read(1,'(a89)') para 
            write (36,12)para
             read(1,'(a89)') para 
            write (36,15)para,pminruis
!             read(1,'(a89)') para
!            write (36,15)para,aclim
!c             read(1,'(a89)') para
!c            write (36,15)para,zesx
!c             read(1,'(a89)') para
!c            write (36,15)para,cfes
!c             read(1,'(a89)') para
!c            write (36,15)para,z0solnu
             read(1,'(a89)') para 
            write (36,15)para,diftherm
             read(1,'(a89)') para 
            write (36,15)para,bformnappe
             read(1,'(a89)') para 
            write (36,15)para,rdrain
             read(1,'(a89)') para 
            write (36,15)para,psihumin
             read(1,'(a89)') para 
            write (36,15)para,psihucc
             read(1,'(a89)') para 
            write (36,15)para,prophumtasssem
             read(1,'(a89)') para 
            write (36,15)para,prophumtassrec
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codhnappe
             read(1,'(a89)') para 
            write (36,13)para,distdrain
             read(1,'(a89)') para 
            write (36,12)para
             read(1,'(a89)') para 
            write (36,15)para,proflabour(1)
             read(1,'(a89)') para 
            write (36,15)para,proftravmin(1)
!c ** les cailloux
            do i=1,12
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codetycailloux
            do i=1,10
             read(1,'(a89)') para 
            write (36,15)para,masvolcx(i)
             read(1,'(a89)') para 
            write (36,15)para,hcccx(i)
            enddo
!c** les engrais
            do i=1,9
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codetypeng
            do i=1,8
             read(1,'(a89)') para 
            write (36,15)para,engamm(i)
             read(1,'(a89)') para 
            write (36,15)para,orgeng(i)
             read(1,'(a89)') para 
            write (36,15)para,deneng(i)
             read(1,'(a89)') para 
            write (36,15)para,voleng(i)
            enddo
!c** les residus sont lus dans la trame

             read(1,'(a100)') para 
            write (36,'(a100)')para
            do i=1,21
             read(1,'(a100)') para 
            write (36,'(a100)')para
             enddo
          read(1,'(a100)') para 
          write(36,'(a100)')para
!c DR 27/06/2011 les residus sont pris dans maj_nouveauresidus.par

          open(117,file='config\maj_nouveauresidus.par')
         do i=1,24
            read(117,*)
         enddo
            do i=1,21
             read(117,*) para,para,CroCo(i)
             read(117,*) para,para,akres(i) 
             read(117,*) para,para,bkres(i) 
              read(117,*)para, para,awb(i)  
             read(117,*)para, para,bwb(i)  
             read(117,*)para, para,cwb(i)   
             read(117,*)para, para,ahres(i)  
             read(117,*)para, para,bhres(i)  
             read(117,*)para, para,kbio(i)  
             read(117,*)para, para,yres(i)  
             read(117,*)para, para,CNresmin(i)  
             read(117,*)para, para,CNresmax(i) 
             read(117,*)para, para,qmulchruis0(i)  
             read(117,*)para, para,mouillabilmulch(i) 
             read(117,*)para, para,kcouvmlch(i)  
             read(117,*)para, para,albedomulch(i) 
             read(117,*)para, para,Qmulchdec(i) 

         enddo
            do i=1,21
             read(1,'(a89)') para
            write(36,15)para,CroCo(i)
             read(1,'(a89)') para
            write(36,15)para,akres(i) 
             read(1,'(a89)') para
            write(36,15)para,bkres(i) 
             read(1,'(a89)') para
            write(36,15)para,awb(i)  
             read(1,'(a89)') para 
            write(36,15)para,bwb(i)  
             read(1,'(a89)') para
            write(36,15)para,cwb(i)  
             read(1,'(a89)') para
            write(36,15)para,ahres(i) 
             read(1,'(a89)') para
            write(36,15)para,bhres(i) 
             read(1,'(a89)') para 
            write(36,15)para,kbio(i)  
             read(1,'(a89)') para
            write(36,15)para,yres(i)  
             read(1,'(a89)') para
            write(36,15)para,CNresmin(i) 
             read(1,'(a89)') para
            write(36,15)para,CNresmax(i) 
             read(1,'(a89)') para
            write(36,15)para,qmulchruis0(i) 
             read(1,'(a89)') para
            write(36,15)para,mouillabilmulch(i) 
             read(1,'(a89)') para
            write(36,15)para,kcouvmlch(i) 
             read(1,'(a89)') para
            write(36,15)para,albedomulch(i)
             read(1,'(a89)') para
            write(36,15)para,Qmulchdec(i) 
            enddo
!  100       continue
      close(1)
      close(12)

      return
      end
