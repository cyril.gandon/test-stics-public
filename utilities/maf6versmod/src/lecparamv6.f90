      SUBROUTINE lecparamv6 
!*********************************************************************
!C     lecture et initialisation des parametres nouvelle version
!C     en attente d'etre redirig� dans leurs fichiers respectifs
!C     fichier paramv6.par
!      derniere modif DR le 18/07/05 petit erreur dans l'ecriture d'un param 
!********************************************************************

! ** Declarations :
! *- les commons
      include 'common.inc'
      include 'comtransit.inc'
      include 'comparplt.inc'
!      include 'message.inc'
      include 'comclim.inc'
      include 'compartec.inc'
      include 'comparsol.inc'
      
          
! *- les variables locales
      character*20 npara,vpara
      integer codecritmouch, nbparamspot, nbparamslus
!      integer nl
      
! *- pour unix syntaxe de chemin d'acces differente : / � la place de \ 
! *- idem dans princip.for
      write(*,*)'info# reading of maj_paramv6.par '
      open (12,file='.\config\maj_paramv6.par',status='old',err=251)
      
! DR 14/01/08 certains parametres sont nouveaux on les affecte
!            FTEMh=0.12
!            FTEMha=25.0
!      tref devient      trefh=15.0
!            TREFh=15.0
!            FTEMr=0.103
!            FTEMra=12.0
!            trefr=15.0
!            tnitopt=25.0
!            rationit=0.05
!            ratiodenit=0.1
!            alphaph=0.005
!            dphvolmax=1.0
!            phvols=8.6
!            prophumtassrec=1.0
!            prophumtasssem=1.2
!            codedateappH2O(1)=2
!            fhminsat=0.5
! DR 02/07/08 fractionnement de l'azote
!            codefracappN(1)=1
!            Qtot_N(1)=100
!            codepalissage=1
!            alphaph=0.00500
!            pminruis=5.0
! DR 29/04/2010 je force codedateappN=2(jours julien ) sinon il prend la valeur de paramv6 (codedateappN en temp. 1=oui, 2=non)
!            codedateappN(1)=2

! DR 08/08/08 concernant le sol
!      zesx=60.0
!      cfes=5.0
!      z0solnu=0.01
!      codedenit=2
!      profdenit=20
!      vpotdenit=7.0



! *- le nombre de param�tres potentiellement lus.
! dr 11082012      nbparamspot = 142+18+4+2+3+4
      
!      nbparamspot = 142+18+4+2+3+4-1
      nbparamspot = 142+18+4+2+3+4-1-1+2+1+2



! *- le nombre de prama�tres effectivement lus
      nbparamslus = 0
  
5     read(12,'(69x,a20,a15)',end=15,err=250)npara,vpara

!      write(*,*) npara, vpara
      
      if (npara.eq.'codazorac') then
        read(vpara,'(i3)') codazorac
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'minefnra') then
        read(vpara,*) minefnra
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'minazorac') then
        read(vpara,*) minazorac 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'maxazorac') then
        read(vpara,*) maxazorac 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'repracmax(1)') then
        read(vpara,*) repracmax(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'repracmin(1)') then
        read(vpara,*) repracmin(1) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'kreprac(1)') then
        read(vpara,*) kreprac(1) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'repracmax(2)') then
        read(vpara,*) repracmax(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'repracmin(2)') then
        read(vpara,*) repracmin(2) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'kreprac(2)') then
        read(vpara,*) kreprac(2) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codtrophrac') then
        read(vpara,'(i3)') codtrophrac
        nbparamslus = nbparamslus + 1
      endif

! ** domi 21/10/04 j'ai reactiv� les 2 parametres suivants qui avaient ete vir�s par megarde
      if (npara.eq.'masecplantule(1)') then
        read(vpara,*)masecplantule(1) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'zracplantule(1)') then
        read(vpara,*)zracplantule(1) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'masecplantule(2)') then
        read(vpara,*)masecplantule(2) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'zracplantule(2)') then
        read(vpara,*)zracplantule(2) 
        nbparamslus = nbparamslus + 1
      endif

      if (npara.eq.'codefixpot') then
        read(vpara,'(i3)') codefixpot
! --        write(ficdbg,*)'codefixpot=',codefixpot
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'afixmaxN') then
        read(vpara,*) afixmaxN 
! --        write(ficdbg,*)'afixmaxN=',afixmaxN
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'bfixmaxN') then
        read(vpara,*) bfixmaxN 
! --        write(ficdbg,*)'bfixmaxN=',bfixmaxN
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fixmaxveg') then
        read(vpara,*) fixmaxveg 
! --        write(ficdbg,*)'fixmaxveg=',fixmaxveg
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fixmaxgr') then
        read(vpara,*) fixmaxgr 
! --        write(ficdbg,*)'fixmaxgr=',fixmaxgr
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codefxn') then
        read(vpara,'(i3)') codefxn 
        nbparamslus = nbparamslus + 1
      endif
      
! *- NB - le 07/06/2004 - potentiel de base
      if (npara.eq.'psihumin') then
        read(vpara,*) psihumin
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'psihucc') then
        read(vpara,*) psihucc
        nbparamslus = nbparamslus + 1
      endif
      
! *- Domi - le 20/09/2004 - calcul debourrement pour perenne
      if (npara.eq.'tdmindeb(1)') then
        read(vpara,*) tdmindeb(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tdmaxdeb(1)') then
        read(vpara,*) tdmaxdeb(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tdmindeb(2)') then
        read(vpara,*) tdmindeb(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tdmaxdeb(2)') then
        read(vpara,*) tdmaxdeb(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codegdhdeb(1)') then
        read(vpara,'(i3)') codegdhdeb(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codegdhdeb(2)') then
        read(vpara,'(i3)') codegdhdeb(2)
        nbparamslus = nbparamslus + 1
      endif
! domi 28/06/05   on vire codeoutfixreel qui n'etait pas correct
      if (npara.eq.'codeoutsti') then
        read(vpara,'(i3)') codeoutsti
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codeoutscient') then
        read(vpara,'(i3)') codeoutscient
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codeseprapport') then
        read(vpara,'(i3)') codeseprapport
        nbparamslus = nbparamslus + 1
      endif

      if (npara.eq.'separateurrapport') then
        read(vpara,'(a1)') separateurrapport
        nbparamslus = nbparamslus + 1
      endif
! dr  170206 rajout du separateur au choix de rapport (nicoullaud)
! domi 3/11/2005 on vire en double avec codepaillis dans tec
!      if (npara.eq.'codeactipaillis') then
!        read(vpara,'(i3)') codeactipaillis
!        nbparamslus = nbparamslus + 1
!      endif
! *- PB - 03/03/2005
      if (npara.eq.'ratioresfauche(1)') then
        read(vpara,*) ratioresfauche(1)
        nbparamslus = nbparamslus + 1
      endif

      if (npara.eq.'ratioresfauche(2)') then
        read(vpara,*) ratioresfauche(2)
        nbparamslus = nbparamslus + 1
      endif

! *- Nb - le 11/02/05
      if (npara.eq.'codeplisoleN') then
        read(vpara,'(i3)') codeplisoleN
        nbparamslus = nbparamslus + 1
      endif
! DR 16/01/06 je l'enleve NB 11/08/05
!      if (npara.eq.'bdilI') then
!        read(vpara,*) bdilI
!        nbparamslus = nbparamslus + 1
!      endif
      if (npara.eq.'masecmeta') then
        read(vpara,*) masecmeta
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Nmeta(1)') then
        read(vpara,*) Nmeta(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Nmeta(2)') then
        read(vpara,*) Nmeta(2) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Nres(1)') then
        read(vpara,*) Nres(1) 
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Nres(2)') then
        read(vpara,*) Nres(2) 
        nbparamslus = nbparamslus + 1
      endif
! domi 13/05/05 � verifier !!!!!!!!!
      if (npara.eq.'codeINN') then
        read(vpara,'(i3)') codeINN
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'INNimin') then
        read(vpara,*) INNimin
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codeHxN') then
        read(vpara,'(i3)') codeHxN
        nbparamslus = nbparamslus + 1
      endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! ** ajout NB le 26/01/05
      if (npara.eq.'patm') then
        read(vpara,*) patm
        nbparamslus = nbparamslus + 1
      endif
! NB le 26/01/05 introduction d'un facteur de correction pour passer de tmin a trosee
      if (npara.eq.'corecTrosee') then
        read(vpara,*) corecTrosee
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'potgermi(1)') then
        read(vpara,*) potgermi(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'potgermi(2)') then
        read(vpara,*) potgermi(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'nbjgerlim(1)') then
        read(vpara,'(i3)') nbjgerlim(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'nbjgerlim(2)') then
        read(vpara,'(i3)') nbjgerlim(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'propjgermin') then
        read(vpara,*) propjgermin(1)
        propjgermin(2)=propjgermin(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codenormfruit') then
        read(vpara,'(i3)') codenormfruit
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codefruitcroi') then
        read(vpara,'(i3)') codefruitcroi
        nbparamslus = nbparamslus + 1
      endif
! DR 13/06/06 ajout des parametres pour inaki 
      if (npara.eq.'cfpf(1)') then
        read(vpara,*) cfpf(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dfpf(1)') then
        read(vpara,*) dfpf(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'cfpf(2)') then
        read(vpara,*) cfpf(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dfpf(2)') then
        read(vpara,*) dfpf(2)
        nbparamslus = nbparamslus + 1
      endif
! *- ajout NB battance 13/05/05
      if (npara.eq.'pluiebat') then
        read(vpara,*) pluiebat
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'mulchbat') then
        read(vpara,*) mulchbat
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'vigueurbat(1)') then
        read(vpara,*) vigueurbat(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'vigueurbat(2)') then
        read(vpara,*) vigueurbat(2)
        nbparamslus = nbparamslus + 1
      endif

! *- ajout NB pour Inaki/vigne le 01/07
      if (npara.eq.'hautmaxtec(1)') then
        read(vpara,*) hautmaxtec(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'hautmaxtec(2)') then
        read(vpara,*) hautmaxtec(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'largtec(1)') then
        read(vpara,*) largtec(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'largtec(2)') then
        read(vpara,*) largtec(2)
        nbparamslus = nbparamslus + 1
      endif
! dr 16/12/05 pour stressphot sur vigne inaki
      if (npara.eq.'codestrphot(1)') then
        read(vpara,'(i3)') codestrphot(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'phobasesen(1)') then
        read(vpara,*)phobasesen(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dltamsminsen(1)') then
        read(vpara,*) dltamsminsen(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dltamsmaxsen(1)') then
        read(vpara,*) dltamsmaxsen(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'alphaphot(1)') then
        read(vpara,*) alphaphot(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codestrphot(2)') then
        read(vpara,'(i3)') codestrphot(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'phobasesen(2)') then
        read(vpara,*)phobasesen(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dltamsminsen(2)') then
        read(vpara,*) dltamsminsen(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dltamsmaxsen(2)') then
        read(vpara,*) dltamsmaxsen(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'alphaphot(2)') then
        read(vpara,*) alphaphot(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codepitaf') then
        read(vpara,'(i3)') codepitaf
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codedateappN') then
        read(vpara,'(i3)') codedateappN(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codesensibilite') then
        read(vpara,'(i3)') codesensibilite
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'coderecolteassoc') then
        read(vpara,'(i3)') coderecolteassoc
        nbparamslus = nbparamslus + 1
      endif
! NB 150206 introduction codedst
      if (npara.eq.'codeDST') then
        read(vpara,'(i3)') codeDST(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'proflabour') then
        read(vpara,*) proflabour(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'proftravmin') then
        read(vpara,*) proftravmin(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dachisel') then
        read(vpara,*) dachisel(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dalabour') then
        read(vpara,*) dalabour(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'rugochisel') then
        read(vpara,*) rugochisel(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'rugolabour') then
        read(vpara,*) rugolabour(1)
        nbparamslus = nbparamslus + 1
      endif
! DR le 31/03/06
!  on ajoute avce celia les parametres effet tassement et regle de
!   decision du semis 
      if (npara.eq.'codeDSTtass') then
        read(vpara,'(i3)') codeDSTtass(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'profhumsemoir') then
        read(vpara,*) profhumsemoir(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'humseuiltasssem') then
        read(vpara,*) humseuiltasssem(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dasemis') then
        read(vpara,*) dasemis(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'profhumrecolteuse') then
        read(vpara,*) profhumrecolteuse(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'humseuiltassrec') then
        read(vpara,*) humseuiltassrec(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'darecolte') then
        read(vpara,*) darecolte(1)
        nbparamslus = nbparamslus + 1
      endif

      if (npara.eq.'codeDSTnbcouche') then
        read(vpara,'(i3)') codeDSTnbcouche(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codedecisemis') then
        read(vpara,'(i3)') codedecisemis(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'nbjmaxapressemis') then
        read(vpara,*) nbjmaxapressemis(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'nbjseuiltempref') then
        read(vpara,*) nbjseuiltempref(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tnrefgelble') then
        read(vpara,*) tnrefgelble
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tnrefgelbet') then
        read(vpara,*) tnrefgelbet
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tnrefgelmais') then
        read(vpara,*) tnrefgelmais
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tnrefgelqui') then
        read(vpara,*) tnrefgelqui
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tmrefgelble') then
        read(vpara,*) tmrefgelble
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tmrefgelbet') then
        read(vpara,*) tmrefgelbet
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tmrefgelmais') then
        read(vpara,*) tmrefgelmais
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tmrefgelqui') then
        read(vpara,*) tmrefgelqui
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codedecirecolte') then
        read(vpara,'(i3)') codedecirecolte(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'nbjmaxapresrecolte') then
        read(vpara,'(i3)') nbjmaxapresrecolte(1)
        nbparamslus = nbparamslus + 1
      endif

!   dr fin ajout DST

! marie 17/02/06 parametres betterave
      if (npara.eq.'codebet') then
        read(vpara,'(i3)') codebet
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'remobfruit(1)') then
        read(vpara,*)remobfruit(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'seuiltustress(1)') then
        read(vpara,*)seuiltustress(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'seuilsp(1)') then
        read(vpara,*) seuilsp(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'splaiminmoins(1)') then
        read(vpara,*) splaiminmoins(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'splaiminplus(1)') then
        read(vpara,*) splaiminplus(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codelectec') then
        read(vpara,'(i3)') codelectec
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codesoldst') then
        read(vpara,'(i3)') codesoldst
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codetempfauche') then
        read(vpara,'(i3)') codetempfauche(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tcxstop(1)') then
        read(vpara,*) tcxstop(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'tcxstop(2)') then
        read(vpara,*) tcxstop(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codepluiepoquet') then
        read(vpara,'(i3)') codepluiepoquet
        nbparamslus = nbparamslus + 1
      endif

      if (npara.eq.'nbjoursrrversirrig') then
        read(vpara,'(i3)')nbjoursrrversirrig
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codedlaimin') then
        read(vpara,*) codedlaimin
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dlaimin(1)') then
        read(vpara,*) dlaimin(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dlaimin(2)') then
        read(vpara,*)dlaimin(2)
        nbparamslus = nbparamslus + 1
      endif
! NB le 080307
      if (npara.eq.'codedyntalle') then
        read(vpara,*) codedyntalle
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SurfApex(1)') then
        read(vpara,*) SurfApex(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SurfApex(2)') then
        read(vpara,*)SurfApex(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SeuilMorTalle(1)') then
        read(vpara,*) SeuilMorTalle(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SeuilMorTalle(2)') then
        read(vpara,*)SeuilMorTalle(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SigmaDisTalle(1)') then
        read(vpara,*) SigmaDisTalle(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SigmaDisTalle(2)') then
        read(vpara,*)SigmaDisTalle(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'VitReconsPeupl(1)') then
        read(vpara,*) VitReconsPeupl(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'VitReconsPeupl(2)') then
        read(vpara,*)VitReconsPeupl(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SeuilReconsPeupl(1)') then
        read(vpara,*) SeuilReconsPeupl(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'SeuilReconsPeupl(2)') then
        read(vpara,*)SeuilReconsPeupl(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'MaxTalle(1)') then
        read(vpara,*) MaxTalle(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'MaxTalle(2)') then
        read(vpara,*)MaxTalle(2)
        nbparamslus = nbparamslus + 1
      endif
! dr 10/08/2012 � regarder de pres
!      if (npara.eq.'swfacmin') then
!        read(vpara,*)swfacmin
!        nbparamslus = nbparamslus + 1
!      endif
      if (npara.eq.'codetranspitalle') then
        read(vpara,'(i3)')codetranspitalle
        nbparamslus = nbparamslus + 1
      endif
! DR en accord avec Sylvian on l'enleve ne sert plus � rien le pauvre
!      if (npara.eq.'coefsigma') then
!        read(vpara,*)coefsigma
!        nbparamslus = nbparamslus + 1
!      endif
! DR 11/09/07 rajout des parametres pour benjamin
      if (npara.eq.'codeirrigmin') then
        read(vpara,*)codeirrigmin
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'doseirrigmin(1)') then
        read(vpara,*)doseirrigmin(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'doseirrigmin(2)') then
        read(vpara,*)doseirrigmin(2)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codeforcedrpdes') then
        read(vpara,*)codeforcedrpdes
        nbparamslus = nbparamslus + 1
      endif
! dr 20/06/2011 nouevaux parametres
      if (npara.eq.'FTEMh') then
        read(vpara,*)FTEMh
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'FTEMha') then
        read(vpara,*)FTEMha
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'trefh') then
        read(vpara,*)trefh
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'FTEMr') then
        read(vpara,*)FTEMr
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'FTEMra') then
        read(vpara,*)FTEMra
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'trefr') then
        read(vpara,*)trefr
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'FMIN1') then
        read(vpara,*)FMIN1
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'FMIN2') then
        read(vpara,*)FMIN2
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'FMIN3') then
        read(vpara,*)FMIN3
        nbparamslus = nbparamslus + 1
      endif


      if (npara.eq.'hminm') then
        read(vpara,*)hminm
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'hoptm') then
        read(vpara,*)hoptm
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'hminn') then
        read(vpara,*)hminn
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'hoptn') then
        read(vpara,*)hoptn
        nbparamslus = nbparamslus + 1
      endif




!      if (npara.eq.'tnitopt') then
!        read(vpara,*)tnitopt
!        nbparamslus = nbparamslus + 1
!      endif
      if (npara.eq.'rationit') then
        read(vpara,*)rationit
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'ratiodenit') then
        read(vpara,*)ratiodenit
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'alphaph') then
        read(vpara,*)alphaph
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'dphvolmax') then
        read(vpara,*)dphvolmax
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'phvols') then
        read(vpara,*)phvols
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fhminsat') then
        read(vpara,*)fhminsat
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fredlN') then
        read(vpara,*)fredlN
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fredkN') then
        read(vpara,*)fredkN
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fNCbiomin') then
        read(vpara,*)fNCbiomin
        nbparamslus = nbparamslus + 1
      endif
! DR 30/07/2012 je lis les nouveaux param de bruno
      if (npara.eq.'y0msrac') then
        read(vpara,*)y0msrac
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'fredNsup') then
        read(vpara,*)fredNsup
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Primingmax') then
        read(vpara,*)Primingmax
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'prophumtassrec') then
        read(vpara,*)prophumtassrec
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'prophumtasssem') then
        read(vpara,*)prophumtasssem
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Cmulchdec') then
        read(vpara,*)Cmulchdec(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Qmulchdec') then
        read(vpara,*)Qmulchdec(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codedateappH2O') then
        read(vpara,'(i3)')codedateappH2O(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codefracappN') then
        read(vpara,'(i3)')codefracappN(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'Qtot_N') then
        read(vpara,*)Qtot_N(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'codepalissage') then
        read(vpara,'(i3)')codepalissage(1)
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'P_CsurNsol') then
        read(vpara,*)P_CsurNsol
        nbparamslus = nbparamslus + 1
      endif
      if (npara.eq.'P_penterui') then
        read(vpara,*)P_penterui
        nbparamslus = nbparamslus + 1
      endif


      goto 5

15    continue


      do i=2,4
      Cmulchdec(i)=Cmulchdec(1)
      Qmulchdec(i)=Qmulchdec(1)
      enddo

! *- ecriture dans le mouchard
! *- pour l'instant on ecrit rien car c'est en test 
      codecritmouch=1
      if(codecritmouch.eq.1)then
!      write(*,*)'info#Attention some parameters are new and are moved in others files :'
!      write(*,*)'info#check the values in the plant files'
!      write(*,*)'dans les fichiers plantes'
!        write(*,*)'*************************'
!        write (fichist,*)'alphaco2          ='
!     s       ,'1.1 pour les c4 et 1.2 pour les c3'
!        write (fichist,*)'codazorac         =',codazorac
!        write (fichist,*)'minefnra          =',minefnra
!        write (fichist,*)'minazorac         =',minazorac
!        write (fichist,*)'maxazorac         =',maxazorac
!        write (fichist,*)'repracpermin      =',repracmin(1)
!        write (fichist,*)'repracpermax      =',repracmax(1)
!        write (fichist,*)'krepracperm       =',kreprac(1)
!        write (fichist,*)'repracseumin      =',repracmin(1)
!        write (fichist,*)'repracseumax      =',repracmax(1)
!        write (fichist,*)'krepracseu        =',kreprac(1)
!        write (fichist,*)'codtrophrac       =',codtrophrac
!        write (fichist,*)'masecplantule     =',masecplantule(1)
!        write (fichist,*)'zracplantule      =',zracplantule(1)
!        write (fichist,*)'codefixpot        =',codefixpot
!        write (fichist,*)'fixmaxveg         =',fixmaxveg
!        write (fichist,*)'fixmaxgr          =',fixmaxgr
!        write (fichist,*)'codegdhdeb        =',codegdhdeb(1)
!        write (fichist,*)'tdmindeb          =',tdmindeb(1)
!        write (fichist,*)'tdmaxdeb          =',tdmaxdeb(1)
!        write (fichist,*)'codeplisoleN      =',codeplisoleN
!        write (fichist,*)'Nmeta             =',Nmeta(1)
!        write (fichist,*)'Nres              =',Nres(1)
!        write (fichist,*)'codeINN           =',codeINN
!        write (fichist,*)'INNimin           =',INNimin
!        write (fichist,*)'potgermi          =',potgermi(1)
!        write (fichist,*)'nbjgerlim         =',nbjgerlim(2)
!        write (fichist,*)'propjgermin       =',propjgermin
!        write (fichist,*)'cfpf              =',cfpf(1)
!        write (fichist,*)'dfpf              =',dfpf(1)
!        write (fichist,*)'vigueurbat        =',vigueurbat(1)
!        write (fichist,*)'codestrphot       =',codestrphot(1)
!        write (fichist,*)'phobasesen        =',phobasesen(1)
!        write (fichist,*)'dltamsmaxsen      =',dltamsmaxsen(1)
!        write (fichist,*)'tauxrecouvmax     =',tauxrecouvmax(1)
!        write (fichist,*)'tauxrecouvkmax    =',tauxrecouvkmax(1)
!        write (fichist,*)'pentrecouv        =',pentrecouv(1)
!        write (fichist,*)'infrecouv         =',infrecouv(1)

!        write (fichist,*)'tcxstop           =',tcxstop(1)

!        write(*,*)
!        write(*,*)'dans le fichier param.par'
!        write(*,*)'*************************'


      endif
  

! *- si on a pas lu le meme nombre de param�tres que ce qu'on peut en lire
! *- alors on sort la liste des param�tres potentiellement non initialis�s (=zero)
      if (nbparamspot.ne.nbparamslus) then
        write(*,*)'error#', mes107v6
        if(codefixpot.eq.0) write(fichist,*)'error#','codefixpot=',codefixpot
        if(fixmaxgr.eq.0) write(fichist,*)'error#','fixmaxgr=',fixmaxgr
        if(fixmaxveg.eq.0) write(fichist,*)'error#','fixmaxveg=',fixmaxveg
        if(afixmaxN.eq.0) write(fichist,*)'error#','afixmaxN=',afixmaxN
        if(bfixmaxN.eq.0) write(fichist,*)'error#','bfixmaxN=',bfixmaxN
        if(codazorac.eq.0) write(fichist,*)'error#', 'codazorac ',codazorac
        if(minefnra.eq.0) write(fichist,*)'error#', 'minefnra',  minefnra
        if(minazorac.eq.0) write(fichist,*)'error#', 'minazorac',  minazorac
        if(maxazorac.eq.0) write(fichist,*)'error#', 'maxazorac',  maxazorac
        if(repracmax(1).eq.0) write(fichist,*)'error#','repracmax(1)',  repracmax(1)
        if(repracmin(1).eq.0) write(fichist,*)'error#','repracmin(1)',  repracmin(1)
        if (kreprac(1).eq.0) write(fichist,*)'error#', 'kreprac(1)',  kreprac(1)
        if (repracmax(2).eq.0) write(fichist,*)'error#',  'repracmax(2)',  repracmax(2)
        if (repracmin(2).eq.0) write(fichist,*)'error#',  'repracmin(2)',  repracmin(2)
        if (kreprac(2).eq.0) write(fichist,*)'error#', 'kreprac(2)',  kreprac(2)
        if (codtrophrac.eq.0) write(fichist,*)'error#', 'codtrophrac',  codtrophrac
        if (masecplantule(1).eq.0) write(fichist,*)'error#', 'masecplantule(1)', masecplantule(1)
        if (zracplantule(1).eq.0) write(fichist,*)'error#','zracplantule(1)', zracplantule(1)
        if (masecplantule(2).eq.0) write(fichist,*)'error#','masecplantule(2)', masecplantule(2)
        if (zracplantule(2).eq.0) write(fichist,*)'error#', 'zracplantule(2)', zracplantule(2)
        if (codefxn.eq.0) write(fichist,*)'error#','codefxn=',codefxn
        if (psihumin.eq.0) write(fichist,*)'error#','psihumin=',psihumin
        if (psihucc.eq.0) write(fichist,*)'error#','psihucc=',psihucc
        if (tdmindeb(1).eq.0) write(fichist,*)'error#','tdmindeb(1)=',tdmindeb(1)
        if (tdmindeb(2).eq.0) write(fichist,*)'error#','tdmindeb(2)=',tdmindeb(2)
        if (tdmaxdeb(1).eq.0) write(fichist,*)'error#','tdmaxdeb(1)=',tdmaxdeb(1)
        if (tdmaxdeb(2).eq.0) write(fichist,*)'error#','tdmaxdeb(2)=',tdmaxdeb(2)
        if (codegdhdeb(1).eq.0) write(fichist,*)'error#', 'codegdhdeb(1)=',codegdhdeb(1)
        if (codegdhdeb(2).eq.0) write(fichist,*)'error#', 'codegdhdeb(2)=',codegdhdeb(2)
        if (codeoutsti.eq.0) write(fichist,*)'error#',   'codeoutsti=',codeoutsti
        if (codeplisoleN.eq.0) write(fichist,*)'error#',  'codeplisoleN=',codeplisoleN
!        if(bdilI.eq.0) write(fichist,*)'error#',
!     s 'bdilI=',bdilI
        if (masecmeta.eq.0) write(fichist,*)'error#',  'masecmeta=',masecmeta
        if (Nmeta(1).eq.0) write(fichist,*)'error#', 'Nmeta(1)=',Nmeta(1)
        if (Nmeta(2).eq.0) write(fichist,*)'error#', 'Nmeta(2)=',Nmeta(2)
        if (Nres(1).eq.0) write(fichist,*)'error#',  'Nres(1)=',Nres(1)
        if (Nres(2).eq.0) write(fichist,*)'error#',  'Nres(2)=',Nres(2)
        if (ratioresfauche(1).eq.0) write(fichist,*)'error#', 'ratioresfauche(1)=',ratioresfauche(1)
        if (ratioresfauche(2).eq.0) write(fichist,*)'error#', 'ratioresfauche(2)=',ratioresfauche(2)
        if (codeINN.eq.0) write(fichist,*)'error#', 'codeINN',codeINN
        if (INNimin.eq.0) write(fichist,*)'error#', 'INNimin',INNimin
        if (codeHxN.eq.0) write(fichist,*)'error#',  'codeHxN',codeHxN
        if (patm.eq.0) write(fichist,*)'error#',   'patm',patm
        if (corecTrosee.eq.0) write(fichist,*)'error#', 'corecTrosee',corecTrosee
        if (potgermi(1).eq.0) write(fichist,*)'error#',  'potgermi(1)',potgermi(1)
        if (potgermi(2).eq.0) write(fichist,*)'error#',  'potgermi(2)',potgermi(2)
        if (nbjgerlim(1).eq.0) write(fichist,*)'error#',  'nbjgerlim(1)',nbjgerlim(1)
        if (nbjgerlim(2).eq.0) write(fichist,*)'error#',  'nbjgerlim(2)',nbjgerlim(2)
        if (propjgermin(1).eq.0) write(fichist,*)'error#', 'propjgermin(1)',propjgermin(1)
        if (codenormfruit.eq.0) write(fichist,*)'error#',  'codenormfruit',codenormfruit
        if (codefruitcroi.eq.0) write(fichist,*)'error#', 'codefruitcroi',codefruitcroi
        if (cfpf(1).eq.0) write(fichist,*)'error#',    'cfpf(1)',cfpf(1)
        if (dfpf(1).eq.0) write(fichist,*)'error#',   'dfpf(1)',dfpf(1)
        if (cfpf(2).eq.0) write(fichist,*)'error#',  'cfpf(2)',cfpf(2)
        if (dfpf(2).eq.0) write(fichist,*)'error#', 'dfpf(2)',dfpf(2)

! *- ajout NB battance 13/05/05
        if(pluiebat.eq.0) write(fichist,*)'error#',  'pluiebat',pluiebat
        if(mulchbat.eq.0) write(fichist,*)'error#',  'mulchbat',mulchbat
        if(vigueurbat(1).eq.0) write(fichist,*)'error#', 'vigueurbat(1)',vigueurbat(1)
        if(vigueurbat(2).eq.0) write(fichist,*)'error#', 'vigueurbat(2)',vigueurbat(2)
        if(hautmaxtec(1).eq.0) write(fichist,*)'error#', 'hautmaxtec(1)',hautmaxtec(1)
        if(hautmaxtec(2).eq.0) write(fichist,*)'error#', 'hautmaxtec(2)',hautmaxtec(2)
        if(largtec(1).eq.0) write(fichist,*)'error#',  'largtec(1)',largtec(1)
        if(largtec(2).eq.0) write(fichist,*)'error#',  'largtec(2)',largtec(2)
        if(codeoutscient.eq.0) write(fichist,*)'error#', 'codeoutscient',codeoutscient
! dr 03/01/06 pour stressphot sur vigne inaki
      if (codestrphot(1).eq.0)  write(fichist,*)'error#',   'codestrphot(1)',codestrphot(1)
      if (phobasesen(1).eq.0)  write(fichist,*)'error#',   'phobasesen(1)',phobasesen(1)
      if (dltamsminsen(1).eq.0)  write(fichist,*)'error#',   'dltamsminsen(1)',dltamsminsen(1)
      if (dltamsmaxsen(1).eq.0)  write(fichist,*)'error#',   'dltamsmaxsen(1)',dltamsminsen(1)
      if (alphaphot(1).eq.0)  write(fichist,*)'error#',     'alphaphot(1)',alphaphot(1)
      if (codestrphot(2).eq.0)  write(fichist,*)'error#',  'codestrphot(2)',codestrphot(2)
      if (phobasesen(2).eq.0)  write(fichist,*)'error#',   'phobasesen(2)',phobasesen(2)
      if (dltamsminsen(2).eq.0)  write(fichist,*)'error#',  'dltamsminsen(2)',dltamsminsen(2)
      if (dltamsmaxsen(2).eq.0)  write(fichist,*)'error#',   'dltamsmaxsen(2)',dltamsminsen(2)
      if (alphaphot(2).eq.0)  write(fichist,*)'error#',    'alphaphot(2)',alphaphot(2)
      if (codepitaf.eq.0)  write(fichist,*)'error#',  'codepitaf',codepitaf
      if (codedateappN(1).eq.0)  write(fichist,*)'error#',   'codedateappN',codedateappN
      if (codesensibilite.eq.0)  write(fichist,*)'error#',   'codesensibilite',codesensibilite
      if (coderecolteassoc.eq.0)  write(fichist,*)'error#',  'coderecolteassoc',coderecolteassoc
! NB 15/02/06 parametres betterave
      if (codeDST(1).eq.0)  write(fichist,*)'error#',    'codeDST',codeDST(1)
      if (proflabour(1).eq.0)  write(fichist,*)'error#',   'proflabour',proflabour
      if (proftravmin(1).eq.0)  write(fichist,*)'error#',  'proftravmin',proftravmin
      if (dachisel(1).eq.0)  write(fichist,*)'error#',    'dachisel',dachisel(1)
      if (dalabour(1).eq.0)  write(fichist,*)'error#',    'dalabour',dalabour(1)
      if (rugochisel(1).eq.0)  write(fichist,*)'error#',  'rugochisel',rugochisel(1)
      if (rugolabour(1).eq.0)  write(fichist,*)'error#',   'rugolabour',rugolabour(1)
! marie 17/02/06 parametres betterave
      if (codebet.eq.0)  write(fichist,*)'error#',     'codebet',codebet
      if (remobfruit(1).eq.0)  write(fichist,*)'error#',   'remobfruit(1)',remobfruit(1)
      if (seuiltustress(1).eq.0)  write(fichist,*)'error#',  'seuiltustress(1)',seuiltustress(1)
      if (seuilsp(1).eq.0)  write(fichist,*)'error#',    'seuilsp(1)',seuilsp(1)
      if (splaiminmoins(1).eq.0)  write(fichist,*)'error#',  'splaiminmoins(1)',splaiminmoins(1)
      if (splaiminplus(1).eq.0)  write(fichist,*)'error#',   'splaiminplus(1)',splaiminplus(1)
      if (codelectec.eq.0)  write(fichist,*)'error#',    'codelectec',codelectec
      if (codesoldst.eq.0)  write(fichist,*)'error#',   'codesoldst',codesoldst



        stop

      endif


      goto 200
250   write (fichist,270)
      write (fichist,*) mes105v6 
270   FORMAT(5(/))
      stop
251   write(*,270) 
      write(*,*)mes106v6
      stop
200   continue
    
      close(12)
    
      end
