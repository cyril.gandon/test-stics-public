      subroutine ecrinouvobs
!  **********************************
!   transformation des fichiers obs

      include 'common.inc'
      include 'comform.inc'

      common/newfiles/newtec,newplt,newobs,newsol,newcli,newsta
      character*255 newtec,newsol,newobs,newplt,newcli,newsta
      logical ecrire

      character*20 nomvar(45)
      common/varaibles/nomvar

      character*8 var(732,45) !,v(732,45)
      integer numvar(45),ian(732),imois(732),jour(732),jourjul(732)
      integer numcol(45)
      integer test,nbvar
      character*1 sep
      sep=';'

! Domi pour les obs faut mettre les entetes de colonne
! on lit var.mod
      open(99,file='config\var_maj.txt')
      do k=1,45
            read(99,*)nomvar(k)
      enddo
      close(99)

      do i=1,45
            numvar(i)=0
      enddo
      
!      open(36,file=newobs,err=200,status='new')
      open(36,file=newobs,err=200)

      do jul=1,731
!            read(12,100,end=99)ian(jul),imois(jul),jour(jul),j
        read(12,100,end=99)ian(jul),imois(jul),jour(jul),jourjul(jul)&
           ,(var(jul,i),i=1,45)
            do i=1,45
                  test=0
                  test=index(var(jul,i),'        ')
                        if(test.eq.0)then
                              numvar(i)=1
!                              write(*,*)jul,i,numvar(i),var(jul,i)
                        else
                              var(jul,i)=' -999.99'
                        endif
            enddo
      enddo

99      close(12)
! on ecrit l'entete
      write(36,121)
 121  format('ian;mo;jo;jul',$)
! 122  format(';',a20,$)
! on compte combien de variables on a gardé
100   format(i4,1x,i2,1x,i2,1x,i3,45a8)
      nbvar=1
            do k=1,45
                  if(numvar(k).eq.1)then
                  numcol(nbvar)=k
                  nbvar=nbvar+1
! ecrit dans oteblanc
!                        write(36,122)trim(nomvar(k))
                  call oteblanc(nomvar(k))
                  endif
            enddo
            nbvar=nbvar-1
      write(36,*)

! on ecrit le nouveau fichier
! on a tout teste, on reecrit ca dans le nouveau fichier 
! on ecrit les entetes de colonne
      
      do  j=1,731
            ecrire=.FALSE.
            do i=1,nbvar
                  test=1
                  test=index(var(j,numcol(i)),' -999.99')
                  if(test.eq.0)then
                        ecrire=.TRUE.
                  endif
            enddo
            if(ecrire)then
!                  write(36,119)ian(j),sep,imois(j),sep,jour(j),sep,j,
!     s            (sep,var(j,numcol(n)),n=1,nbvar)
                  write(36,119)ian(j),sep,imois(j),sep,jour(j),sep,&
                jourjul(j),(sep,var(j,numcol(n)),n=1,nbvar)
            endif
119    format(3(i4,a1),i4,45(a1,a8))
      enddo

      goto 220
  200 write(*,*)'error#pb dans le fichier obs'
  220 continue

!/////  c'est finit pour un  obs ////
      return
      close(36)
      end
