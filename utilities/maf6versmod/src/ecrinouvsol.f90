      subroutine ecrinouvsol
!  **********************************
!  ecriture du nouveau fichier sol
      include 'common.inc'
      include 'comparsol.inc'
      include 'comform.inc'
      include 'comtransit.inc'


! DR 25/07/07 faut initialiser les valeurs de zesx,cfes,z0solnu,codedenit,profdenit,vpotdenit
! je prends les valeurs du� param.par de la base , sauf pour benj
! DR 22062011 je prends ces valeurs dans param.par
!      zesx=39.0
!      cfes=3.5
!      z0solnu=0.001
!      codedenit=2
!      profdenit=20
!      vpotdenit=7.0


!         open(36,file=newsol)

!  ecriture du nouveau sol
!         do 71 i=1,100*7,7
!        write(36,66) numsol,typsol,argi,Norg,profhum,calc,pH,
!     s              concseuil,albedo,q0,ruisolnu,obstarac,pluiebat,
!     s              mulchbat,zesx,cfes,z0solnu
!        write(36,68) numsol,codecailloux,codemacropor,codefente,
!     s            codrainage,coderemontcap,codenitrif
!        write(36,67) numsol,capiljour,humcapil,profimper,ecartdrain,
!     s            ksol,profdrain
!        write(36,69) numsol,codedenit,profdenit,vpotdenit

! DR 15/07/08 on change les codes en 0/1 pour les mettre en 2/1 comme logique
      if(codecailloux.eq.0)codecailloux=2
      if(codefente.eq.0)codefente=2
      if(codemacropor.eq.0)codemacropor=2
      if(coderemontcap.eq.0)coderemontcap=2
      if(codrainage.eq.0)codrainage=2
      if(codenitrif.eq.0)codenitrif=2

! DR 30/09/2013 pour vpotdenit on force la valeur � 1kg
! au lieu de 16 kg qui est beaucoup trop d'apres Joel leonard

      P_CsurNsol = 0.0
      P_penterui = 0.33
      vpotdenit = 1.0


        write(36,66) numsol,typsol,argi,Norg,profhum,calc,pH,&
                   concseuil,albedo,q0,ruisolnu,obstarac,pluiebat,  &
                   mulchbat,zesx,cfes,z0solnu,P_CsurNsol, P_penterui !DR 22/08/2012 j'ajoute les 2 parametres

! DR 30/07/2012 on ajoute les 2 parametres  1 pour BM et un pour les sols bresiliens: P_CsurNsol, P_penterui

       write(36,68) numsol,codecailloux,codemacropor,codefente, &
                 codrainage,coderemontcap,codenitrif
       write(36,67) numsol,capiljour,humcapil,profimper,ecartdrain,&
                 ksol,profdrain
        write(36,71) numsol,codedenit,profdenit,vpotdenit

66      format(i5,1x,a12,1x,f5.1,8(1x,f10.4),8(1x,f10.4))
67        format(i5,7(1x,f10.4),i5)
68        format(8i5)
!69        format(2i5,i5,2(1x,f8.2))
70        format(i5,5(1x,f8.2),1x,i5,1x,f8.2,1x,i5)
71      format(3i5,f10.4)
        do 76 icou=1,5
!          write(36,70) numsol,epc(icou),HCCF(icou),HMINF(icou),
!     s                      DAF(icou),cailloux(icou),typecailloux(icou),
!     s                      infil(icou),epd(icou)
          write(36,70) numsol,epc(icou),HCCF(icou),HMINF(icou),DAF(icou),cailloux(icou),typecailloux(icou), &
                      &     infil(icou),epd(icou)
76      continue 
!  71     continue
       goto 700
! 120   write(*,*) 'error#erreur dans le fichier sol'
!       write(*,*) 'error#fichier param.sol sol numero:',numsol
!       stop
  700  continue

!/////  c'est finit pour le sol ////
      return
      end
