      
!  parametres plantes
!c***********************     
        

!c -- common.inc --      integer codeulaivernal(2),codefrmur(2)
      

      real adens(2,12),adil(2),afruitpot(2,12),bdens(2),bdil(2)
      real belong(2)
      real celong(2),cgrain(2),cgrainv0(2),coefmshaut(2)
      real contrdamax(2),croirac(2,12)
      real debsenrac(2),deshydbase(2),dlaimax(2),draclong(2)
      real dureefruit(2,12),efcroijuv(2),efcroirepro(2),efcroiveg(2)
      real elmax(2),extin(2),fixmax(2,12)
      real h2ofeuiljaune(2),h2ofeuilverte(2),h2ofrvert(2),h2oreserve(2)
      real h2otigestruc(2),inflomax(2),jvc(2,12)
      real Kmabs1(2),Kmabs2(2),kmax(2),kstemflow(2)
      real longsperac(2),lvfront(2)
      real mouillabil(2),nbinflo(2)
      real pentinflores(2),pgrainmaxi(2,12),phosat(2),psisto(2)
      real psiturg(2),rsmin(2)
      real sensrsec(2),spfrmax(2),spfrmin(2),splaimax(2),splaimin(2)
      real stamflax(2,12),stemflowmax(2),stlevamf(2,12),stpltger(2)
      real tcmin(2),tcmax(2),tdebgel(2),tdmin(2),tdmax(2),tempdeshyd(2)
      real tgellev10(2),tgellev90(2),tgmin(2)
      real tletale(2),tmaxremp(2),tminremp(2)
      real vitircarb(2),vitirazo(2),vitpropsucre(2),vitprophuile(2)
      real Vmax1(2),Vmax2(2),zlabour(2),zprlim(2),zpente(2)




!c --       real coefamsres(2),coefbmsres(2),coefaifres(2),coefbifres(2),coefcifres

      real adilmax(2),bdilmax(2),masecNmax(2),INNmin(2)
      real inngrain1(2),inngrain2(2),stlevdno(2),stdnofno(2)
      real stfnofvino(2),vitno(2)
      real profnod(2),concNnodseuil(2),concNrac0(2),concNrac100(2)
      real hunod(2),tempnod1(2),tempnod2(2),tempnod3(2),tempnod4(2)
      real stlevdrp(2,12),stflodrp(2,12),stlaxsen(2,12),stsenlan(2,12)
      real stdrpmat(2,12)
      real durvieF(2,12),sensiphot(2,12),nbgrmax(2,12)
      real coeflevamf(2),coefamflax(2),coeflaxsen(2),coefsenlan(2)
      real coefdrpmat(2),coefflodrp(2),coeflevdrp(2)
      real ratiodurvieI(2),ratiosen(2),ampfroid(2),laicomp(2)
      real phobase(2),stressdev(2),jvcmini(2),tfroid(2)
        integer julvernal(2)
      real q10(2),stdordebour(2),phyllotherme(2)
!c ** NB et ML - le 23/02/04 - pour demarrage plantule, ajout de masecplantule et zracplantule
      real laiplantule(2),masecplantule(2),zracplantule(2)
      real hautbase(2),hautmax(2),vlaimax(2),pentlaimax(2),udlaimax(2)
      real abscission(2),parazofmorte(2),innturgmin(2),tustressmin(2)
      real durviesupmax(2),innsen(2),rapsenturg(2),dlaimaxbrut(2)
      real tauxrecouvmax(2),tauxrecouvkmax(2)
      real pentrecouv(2),infrecouv(2),rapforme(2),ktrou(2)
      real adfol(2),dfolbas(2),dfolhaut(2),remobres(2)
      real temin(2),teopt(2),temax(2),teoptbis(2)
      real slamin(2),slamax(2),tigefeuil(2),envfruit(2),sea(2)
      real nbgrmin(2),irmax(2),vitircarbT(2),stdrpdes(2)
      real afpf(2),bfpf(2),stdrpnou(2),sensanox(2),allocfrmax(2)
      real tgeljuv10(2),tgeljuv90(2),tgelveg10(2)
      real tgelveg90(2),tgelflo10(2),tgelflo90(2)
      

      
      
      common/parplt1/adens,adil,afruitpot,bdens,bdil,belong, &
       celong,cgrain,cgrainv0,coefmshaut, contrdamax,croirac, &
       debsenrac,deshydbase,dlaimax,draclong,    &
       dureefruit,efcroijuv,efcroirepro,efcroiveg,   &
       elmax,extin,fixmax,&
       h2ofeuiljaune,h2ofeuilverte,h2ofrvert,h2oreserve,     &
       h2otigestruc,inflomax,jvc,   &
       Kmabs1,Kmabs2,kmax,kstemflow,  &
       longsperac,lvfront,&
       mouillabil,nbinflo,&
       pentinflores,pgrainmaxi,phosat,psisto,&
       psiturg,rsmin,    &
       sensrsec,spfrmax,spfrmin,splaimax,splaimin,    &
       stamflax,stemflowmax,stlevamf,stpltger,&
       tcmin,tcmax,tdebgel,tdmin,tdmax,tempdeshyd,&
       tgellev10,tgellev90,tgmin,    &
       tletale,tmaxremp,tminremp, &
       vitircarb,vitirazo,vitpropsucre,vitprophuile, &
       Vmax1,Vmax2,zlabour,zprlim,zpente


      common/parplt3/sensanox,tustressmin

      common/parplt4/stlevdrp,nbgrmin,vitircarbT, &
                   adilmax,bdilmax,masecNmax,INNmin,stsenlan, &
                   stlaxsen,nbgrmax,stdrpmat

      common/parplt5/ratiodurvieI,ratiosen,parazofmorte,innturgmin, &
                    dlaimaxbrut,innsen,rapsenturg,tauxrecouvmax, &
                    tauxrecouvkmax,pentrecouv,infrecouv,&
                    irmax,teopt,temax,temin,inngrain1,inngrain2, &
                    coeflevamf,coefamflax,coeflaxsen,coefsenlan,&
                    coeflevdrp,coefdrpmat,coefflodrp
     
      common/parplt6/q10,stdordebour,laiplantule,masecplantule,&
                    zracplantule,phyllotherme,laicomp,  &
                    hautbase,hautmax,vlaimax,pentlaimax
     
      
      common/parplt8/durvieF,slamax,envfruit,slamin,  &
                   remobres,adfol,dfolbas,dfolhaut,    &
                   tigefeuil,udlaimax,durviesupmax,abscission
     
      common/parplt9/stlevdno,stdnofno,stfnofvino,vitno,profnod, &
                   concNnodseuil,concNrac0,concNrac100,hunod,tempnod1,   &
                    tempnod2,tempnod3,tempnod4,stflodrp,sensiphot
     
      common /sup3/tfroid,ampfroid

      common/parpltdevble/phobase,stressdev,jvcmini,julvernal
 
      
      common/parpltgel/tgeljuv10,tgeljuv90,tgelveg10,tgelveg90, &
                          tgelflo10,tgelflo90
      
      common /parpltradia/ktrou,rapforme,teoptbis,allocfrmax, &
                   stdrpnou

      common /parpltradia2/forme

      common /tom2domi/afpf,bfpf

!c ** le common eaupl ne doit pas etre compl�t�
!c --      common/eaupl/deshydbase,tempdeshyd

      common/eauplante/stdrpdes

      common/nb1003/sea

      
      integer codelegume(2),codcalinflo(2),codgeljuv(2),codebeso(2)
      integer codeintercept(2),codesymbiose(2),codeindetermin(2)
      integer codetremp(2),codetemprac(2),coderacine(2),codgellev(2)
      integer codazofruit(2),codemonocot(2)
      integer codetemp(2),codegdh(2)
      integer codephot(2),coderetflo(2),codebfroid(2),codedormance(2)
      integer codeperenne(2),codegermin(2),codehypo(2),codeir(2)
      integer codelaitr(2),codlainet(2),codetransrad(2),codetransradb(2)
      integer codtefcroi(2),codgelveg(2),codgelflo(2)
      
      
      common/codeplt1/codelegume,codcalinflo,codgeljuv,codebeso,&
                     codeintercept,codesymbiose,codeindetermin,   &
                     codetremp,codetemprac,coderacine,codgellev,&
                     codazofruit,codemonocot,codetemp,codegdh,  &              
                     codephot,coderetflo,codebfroid,codedormance,  &
                     codeperenne,codegermin,codehypo,codeir,   &
                     codelaitr,codlainet,codetransrad,codetransradb,  &
                     codtefcroi,codgelveg,codgelflo
      
      
      integer nbjgrain(2),nbfgellev(2),nboite(2)
      integer idebdorm(2),ifindorm(2),nlevlim1(2),nlevlim2(2)
      integer nbfeuilplant(2)
      real forme(2)
      
      common /parpltint/nbjgrain,nbfgellev,nboite,idebdorm,ifindorm,&
                        nlevlim1,nlevlim2,nbfeuilplant
     
      character*3  stoprac(2)
      character*10 codevar(2,12)
      character*3  codeplante(2)
      
      common/parpltchar/stoprac,codevar,codeplante


!c      real alphaco2(2)
      character*3 stadebbch(2,13) 
      common/drplt_20062011/stadebbch