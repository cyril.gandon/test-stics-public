      subroutine ecrinouvplt
      include 'common.inc'
      include 'comform.inc'
      include 'comparplt.inc'
      include 'comvarplt.inc'
      include 'comtransit.inc'
      include 'comparsol.inc'

      common/newfiles/newtec,newplt,newobs,newsol,newcli
      character*255 newtec,newsol,newobs,newplt,newcli,oldplt

      common/oldfiles/oldplt

      character*3 stadebbchbanane(13),stadebbchble(13)&
     ,stadebbchbetterave(13),stadebbchcanne(13)&
     ,stadebbchciMout(13),stadebbchcolza(13)&
     ,stadebbchfetuque(13),stadebbchfraise(13)&
     ,stadebbchluzerne(13),stadebbchmais(13), stadebbchorge(13)&
     ,stadebbchpatate(13), stadebbchpois(13), stadebbchprairie(13)&
     ,stadebbchsalade(13), stadebbchsorgho(13), stadebbchtomate(13)&
     ,stadebbchtournesol(13),stadebbchvigne(13),stadebbchrien(13)

! DR 25/02/2010 on ajoute les stades pour les indiquer dans le bilan

      data stadebbchbanane/' 00','-99',' 10',' 21','-99','-99',' 60','-99','-99','-99','-99',' 99','-99'/
      data stadebbchble/' 00',' 05',' 09',' 35',' 55',' 85',' 65',' 71','-99',' 75',' 89',' 99','-99'/
      data stadebbchbetterave/' 00',' 05',' 09','-99','-99','-99','-99',' 49','-99','-99','-99',' 99','-99'/
      data stadebbchcanne/' 00','-99',' 09',' 35','-99','-99','-99',' 49','-99','-99','-99',' 99','-99'/
! DR mou et rgi et lin idem                                            
      data stadebbchciMout/' 00',' 05',' 09','-99','-99','-99','-99','-99','-99','-99','-99',' 99','-99'/
      data stadebbchcolza/' 00',' 05',' 09',' 35','-99','-99',' 65',' 71'    ,'-99',' 80',' 89',' 99','-99'/
      data stadebbchfetuque/' 00',' 05',' 09',' 35',' 55','-99','-99','-99','-99','-99','-99','-99','-99'/
      data stadebbchfraise/' 00','-99',' 03','-99','-99','-99',' 65' ,' 71',' 73','-99',' 81',' 99','-99'/
      data stadebbchluzerne/' 00','-99','-99','-99','-99','-99','-99' ,'-99','-99','-99','-99',' 99','-99'/
      data stadebbchmais/' 00',' 05',' 09',' 35',' 55',' 85',' 65',' 71'  ,'-99',' 75',' 89',' 99','-99'/
      data stadebbchorge/' 00',' 05',' 09',' 35',' 55',' 85',' 65',' 71'  ,'-99',' 75',' 89',' 99','-99'/
      data stadebbchpatate/' 00','-99',' 09','-99','-99',' 95',' 65'  ,' 71','-99',' 81',' 89',' 99','-99'/
      data stadebbchpois/' 00',' 05',' 09','-99','-99','-99','65','-99' ,'-99','-99',' 89',' 99','-99'/
      data stadebbchprairie/' 00',' 05',' 09',' 35',' 55','-99','-99','-99','-99','-99','-99','-99','-99'/
      data stadebbchsalade/' 00','-99',' 09','-99',' 49','-99',' 65'  ,'-99','-99','-99','-99',' 99','-99'/
      data stadebbchsorgho/' 00',' 05',' 09',' 35',' 55',' 85',' 65' ,' 71','-99',' 75',' 89',' 99','-99'/
      data stadebbchtomate/' 00','-99',' 09','-99','-99','-99',' 65'  ,' 71','-99',' 81',' 89',' 99','-99'/
      data stadebbchtournesol/' 00',' 05',' 09','-99','-99','-99',' 65','-99','-99','-99',' 89',' 99','-99'/
      data stadebbchvigne/' 00','-99',' 09','-99','-99','-99',' 65' ,'-99','-99',' 85','-99',' 99',' 01'/
      data stadebbchrien/'-99','-99','-99','-99','-99','-99','-99','-99'  ,'-99','-99','-99',' 99','-99'/


!      
      character*110 para
      character*110 paravar(8)
      character*69 ligne
      real nbgrmax_new

! DR 11/06/07 nouveaux parametres (codegdhdeb,tdmindeb,tdmaxdeb,stdordebour, codeperenne)
! DR 11/06/07 nouveaux parametres (humecgraine,nbjgerlim)


! DR 14/02/2012 je reinitialise les valeurs par defaut du paramvo par defaut sinon je garde les valeurs des dernieres plantes
              cfpf(ipl)=0
                  codefixpot=1
                  codeINN=1
                  codeplisoleN=1
                  codestrphot(ipl)=2
              codtrophrac=3
                  dlaimin(ipl)=0.0
                  innimin=-0.5
                  kreprac(ipl)=1.27
                  Nmeta(ipl)=6.47
                  Nres(ipl)=1.5
              repracmax(ipl)=0.4
                  repracmin(ipl)=0.2
                  tcxstop(ipl)=100.0



            if(codeplante(ipl).eq.'mai'.or.codeplante(ipl).eq.'sor'.or. codeplante(ipl).eq.'can')then
                  alphaco2(ipl)=1.06
            else
                  alphaco2(ipl)=1.2
            endif       
! DR 30/01/2012 on force les parametres du paramv6 pour chaque plante avant le switch
          if(codeplante(ipl).eq.CODE_VINE)then
                  codeplisoleN=2
                  Nmeta(ipl)=6.0
                  Nres(ipl)=1.6
              codeINN=2
                  innimin=-0.49
              cfpf(ipl)=15
                  codestrphot(ipl)=1
            endif
          if(codeplante(ipl).eq.'ble')then
              codeINN=2
            endif
          if(codeplante(ipl).eq.'mai')then
                  codeplisoleN=2
                  Nmeta(ipl)=4.8
                  tcxstop(ipl)=35
            endif
          if(codeplante(ipl).eq.'fou')then
              codtrophrac=2
                  codeplisoleN=2
                  tcxstop(ipl)=30
                  dlaimin(ipl)=0.1
            endif

          if(codeplante(ipl).eq.'poi')then
              repracmax(ipl)=1.0
                  repracmin(ipl)=0.03
                  kreprac(ipl)=0.003
                  codefixpot=2
                  codeplisoleN=2
                  Nres(ipl)=0.5
            endif
          if(codeplante(ipl).eq.'bet')then
                  codeplisoleN=2
                  dlaimin=0.3
            endif
          if(codeplante(ipl).eq.'org')then
                  codeplisoleN=2
              repracmax(ipl)=1.0
                  repracmin(ipl)=0.06
                  kreprac(ipl)=1.02
            endif
          if(codeplante(ipl).eq.'luz')then
                  codeplisoleN=2
            endif
          if(codeplante(ipl).eq.'lin')then
                  codeplisoleN=2
                  Nres(ipl)=4.41
                  codeINN=2
                  innimin=-1.07
            endif
! DR 08/06/2012 manquait le cimout
          if(codeplante(ipl).eq.'mou')then
                  codeINN=2
            endif
          if(codeplante(ipl).eq.'rgi')then
                  codeINN=2
            endif
! DR 23/08/2012 nbgrmax du sorgho est mauvais
          if(codeplante(ipl).eq.'sor')then
                nbgrmax_new=40000
          endif

! DR 15/06/2012 pour les plantes avec codetefcroi qui n'existe plus dans modulo faut forcer la valuer de P_temin et P_temax � P_tcmin et P_tcmax
          if(codtefcroi(ipl).eq.1)then
              temin=tcmin
                  temax=tcmax
             endif


! DR on regarde quelle est la plante pour determiner quelles sont les stades � ecrire
            if(codeplante(ipl).eq.'ban')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchbanane(i)
                  enddo
            elseif(codeplante(ipl).eq.'ble')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchble(i)
                  enddo
            elseif(codeplante(ipl).eq.'bet')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchbetterave(i)
                  enddo
            elseif(codeplante(ipl).eq.'can')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchcanne(i)
                  enddo
            elseif(codeplante(ipl).eq.'mou'.or.codeplante(ipl).eq.'rgi'&
            .or.codeplante(ipl).eq.'lin')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchciMout(i)
                  enddo
            elseif(codeplante(ipl).eq.'col')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchcolza(i)
                  enddo
            elseif(codeplante(ipl).eq.'fet')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchfetuque(i)
                  enddo
            elseif(codeplante(ipl).eq.'fra')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchfraise(i)
                  enddo
            elseif(codeplante(ipl).eq.'luz')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchluzerne(i)
                  enddo
            elseif(codeplante(ipl).eq.'mai')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchmais(i)
                  enddo
            elseif(codeplante(ipl).eq.'luz')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchluzerne(i)
                  enddo
            elseif(codeplante(ipl).eq.'org')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchorge(i)
                  enddo
            elseif(codeplante(ipl).eq.'pat')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchpatate(i)
                  enddo
            elseif(codeplante(ipl).eq.'poi')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchpois(i)
                  enddo
            elseif(codeplante(ipl).eq.'pra')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchprairie(i)
                  enddo
            elseif(codeplante(ipl).eq.'sal')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchsalade(i)
                  enddo
            elseif(codeplante(ipl).eq.'sor')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchsorgho(i)
                  enddo
            elseif(codeplante(ipl).eq.'tom')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchtomate(i)
                  enddo
            elseif(codeplante(ipl).eq.'tou')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchtournesol(i)
                  enddo
            elseif(codeplante(ipl).eq.'vig')then
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchvigne(i)
                  enddo
            else
                  do i=1,13
                        stadebbch(ipl,i)=stadebbchrien(i)
                  enddo
            endif




! dr 14/06/07 parametre varietal qui ne l'est plus donc on l'impose
!      fixmax(ipl,1)=8.9
      open (1,file='config\trame.plt',status='old')
      open (36,file=newplt,recl=150,status='unknown')
   12 format(a89,i5)
!   14 format(a89,i3,f6.2,i3,4f6.2)
!   15 format(a89,f12.7) : dr 23/08/2012 je change le format il y a trop de precision
!  DR 02/07/2013 je remets le format plus precis ca posait pb pour ceratins parametres
!   15 format(a89,f12.5)
   15 format(a89,f14.7)

!   13 format(a89,f10.3)
    13 format(a89,f14.7)
   16 format(a89,a3)
!   18 format(a72,i1,17x,i4,5f9.2)
!   19 format(a89,a7)
   23 format(a89)
            read(1,'(a89)') para 
            write (36,12)para
            read(1,'(a89)') para 
            write (36,16) para,codeplante(ipl)
            do i=1,3
             read(1,'(a90)') para 
            write (36,23)para
             enddo
            read(1,'(a89)') para 
            write (36,12)para,codemonocot(ipl)
             read(1,'(a90)') para 
            write (36,23)para
            read(1,'(a89)') para 
            write (36,15)para,alphaco2(ipl)
             read(1,'(a89)') para 
            write (36,12)para


            read(1,'(a89)') para 
            write (36,15)para,tdmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tdmax(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codetemp(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo

             read(1,'(a89)') para 
            write (36,12)para,codegdh(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coeflevamf(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coefamflax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coeflaxsen(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coefsenlan(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coeflevdrp(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coefdrpmat(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coefflodrp(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codephot(ipl)
             read(1,'(a89)') para 
            write (36,13)para,phobase(ipl)
             read(1,'(a89)') para 
            write (36,13)para,phosat(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,coderetflo(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stressdev(ipl)
            do i=1,4
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codebfroid(ipl)
             read(1,'(a89)') para 
            write (36,15)para,jvcmini(ipl)
             read(1,'(a89)') para 
            write (36,12)para,julvernal(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tfroid(ipl)
             read(1,'(a89)') para 
            write (36,15)para,ampfroid(ipl)
             read(1,'(a89)') para 
            write (36,13)para,stdordebour(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tdmindeb(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tdmaxdeb(ipl)
            do i=1,4
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codedormance(ipl)
             read(1,'(a89)') para 
            write (36,12)para,ifindorm(ipl)
             read(1,'(a89)') para 
            write (36,15)para,q10(ipl)
             read(1,'(a89)') para 
            write (36,12)para,idebdorm(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codegdhdeb(ipl)
            do i=1,4
             read(1,'(a100)') para 
            write (36,23)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeperenne(ipl)
            do i=1,3
             read(1,'(a100)') para 
            write (36,23)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codegermin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stpltger(ipl)
             read(1,'(a89)') para 
            write (36,15)para,potgermi(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nbjgerlim(ipl)
             read(1,'(a89)') para 
            write (36,15)para,propjgermin(ipl)

            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codehypo(ipl)
             read(1,'(a89)') para 
            write (36,15)para,belong(ipl)
             read(1,'(a89)') para 
            write (36,15)para,celong(ipl)
             read(1,'(a89)') para 
            write (36,15)para,elmax(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nlevlim1(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nlevlim2(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vigueurbat(ipl)
             read(1,'(a89)') para 
            write (36,15)para,laiplantule(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nbfeuilplant(ipl)
             read(1,'(a89)') para 
            write (36,15)para,masecplantule(ipl)
             read(1,'(a89)') para 
            write (36,15)para,zracplantule(ipl)
             read(1,'(a89)') para 
            write (36,12)para
             read(1,'(a89)') para 
            write (36,15)para,phyllotherme(ipl)
             read(1,'(a89)') para 
            write (36,15)para,bdens(ipl)
             read(1,'(a89)') para 
            write (36,15)para,laicomp(ipl)
             read(1,'(a89)') para 
            write (36,15)para,hautbase(ipl)
             read(1,'(a89)') para 
            write (36,15)para,hautmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tcxstop(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codelaitr(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vlaimax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,pentlaimax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,udlaimax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,ratiodurvieI(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tcmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tcmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,ratiosen(ipl)
             read(1,'(a89)') para 
            write (36,15)para,abscission(ipl)
             read(1,'(a89)') para 
            write (36,15)para,parazofmorte(ipl)
             read(1,'(a89)') para 
            write (36,15)para,innturgmin(ipl)
! dr 06/07/2011 dlaimin passe de paramv6 vers plante
             read(1,'(a89)') para 
            write (36,15)para,dlaimin(ipl)

            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codlainet(ipl)
             read(1,'(a89)') para 
            write (36,15)para,dlaimax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tustressmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,dlaimaxbrut(ipl)
             read(1,'(a89)') para 
            write (36,15)para,durviesupmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,innsen(ipl)
             read(1,'(a89)') para 
            write (36,15)para,rapsenturg(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codestrphot(ipl)
             read(1,'(a89)') para 
            write (36,15)para,phobasesen(ipl)
             read(1,'(a89)') para 
            write (36,15)para,dltamsmaxsen(ipl)
! DR 30/01/2012 ajout� ici plus coherent
             read(1,'(a89)') para 
            write (36,15)para,dltamsminsen(ipl)
            read(1,'(a89)') para 
            write (36,15)para,alphaphot(1)
             read(1,'(a89)') para 
            write (36,15)para,tauxrecouvmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tauxrecouvkmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,pentrecouv(ipl)
             read(1,'(a89)') para 
            write (36,15)para,infrecouv(ipl)
            do i=1,4
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codetransrad(ipl)
             read(1,'(a89)') para 
            write (36,15)para,extin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,ktrou(ipl)
             read(1,'(a89)') para 
            write (36,12)para,int(forme(ipl))
             read(1,'(a89)') para 
            write (36,15)para,rapforme(ipl)
             read(1,'(a89)') para 
            write (36,15)para,adfol(ipl)
             read(1,'(a89)') para 
            write (36,15)para,dfolbas(ipl)
             read(1,'(a89)') para 
            write (36,15)para,dfolhaut(ipl)
            do i=1,1
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,15)para,temin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,temax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,teopt(ipl)
             read(1,'(a89)') para 
            write (36,15)para,teoptbis(ipl)
             read(1,'(a89)') para 
            write (36,15)para,efcroijuv(ipl)
             read(1,'(a89)') para 
            write (36,15)para,efcroiveg(ipl)
             read(1,'(a89)') para 
            write (36,15)para,efcroirepro(ipl)
             read(1,'(a89)') para 
            write (36,15)para,remobres(ipl)
             read(1,'(a89)') para 
            write (36,15)para,coefmshaut(ipl)
             read(1,'(a89)') para 
            write (36,12)para
             read(1,'(a89)') para 
            write (36,13)para,slamax(ipl)
             read(1,'(a89)') para 
            write (36,13)para,slamin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tigefeuil(ipl)
             read(1,'(a89)') para 
            write (36,15)para,envfruit(ipl)
             read(1,'(a89)') para 
            write (36,15)para,sea(ipl)
            do i=1,4
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeindetermin(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nbjgrain(ipl)
             read(1,'(a89)') para 
            write (36,13)para,cgrain(ipl)
             read(1,'(a89)') para 
            write (36,13)para,cgrainv0(ipl)
             read(1,'(a89)') para 
            write (36,13)para,nbgrmin(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
            read(1,'(a89)') para 
            write (36,12)para,codeir(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vitircarb(ipl)
             read(1,'(a89)') para 
            write (36,15)para,irmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vitircarbT(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nboite(ipl)
             read(1,'(a89)') para 
            write (36,15)para,allocfrmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,afpf(ipl)
             read(1,'(a89)') para 
            write (36,15)para,bfpf(ipl)
             read(1,'(a89)') para 
            write (36,15)para,cfpf(ipl)
             read(1,'(a89)') para 
            write (36,15)para,dfpf(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stdrpnou(ipl)
             read(1,'(a89)') para 
            write (36,15)para,spfrmin(ipl)
             read(1,'(a89)') para
            write (36,15)para,spfrmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,splaimin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,splaimax(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codcalinflo(ipl)
             read(1,'(a89)') para 
            write (36,15)para,nbinflo(ipl)
             read(1,'(a89)') para 
            write (36,15)para,inflomax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,pentinflores(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codetremp(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tminremp(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tmaxremp(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vitpropsucre(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vitprophuile(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vitirazo(ipl)
             read(1,'(a89)') para 
            write (36,12)para
             read(1,'(a89)') para 
            write (36,15)para,sensanox(ipl)
             read(1,'(a89)') para 
            write (36,16)para,stoprac(ipl)
             read(1,'(a89)') para 
            write (36,15)para,sensrsec(ipl)
             read(1,'(a89)') para 
            write (36,15)para,contrdamax(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codetemprac(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,coderacine(ipl)
             read(1,'(a89)') para 
            write (36,15)para,zlabour(ipl)
             read(1,'(a89)') para 
            write (36,15)para,zpente(ipl)
             read(1,'(a89)') para 
            write (36,15)para,zprlim(ipl)
             read(1,'(a89)') para 
            write (36,13)para,draclong(ipl)
             read(1,'(a89)') para 
            write (36,13)para,debsenrac(ipl)
             read(1,'(a89)') para 
            write (36,15)para,lvfront(ipl)
             read(1,'(a89)') para 
            write (36,13)para,longsperac(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codazorac
             read(1,'(a89)') para 
            write (36,15)para,minefnra
             read(1,'(a89)') para 
            write (36,15)para,minazorac
             read(1,'(a89)') para 
            write (36,15)para,maxazorac
            do i=1,4
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codtrophrac
             read(1,'(a89)') para 
! dr 11/06/07 faire gaffe � la lecture de ces parametres avec un nouveau nom
            write (36,15)para,repracmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,repracmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,kreprac(ipl)
             read(1,'(a89)') para 
            write (36,15)para,repracmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,repracmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,kreprac(ipl)
             read(1,'(a89)') para 
            write (36,15)para
             read(1,'(a89)') para 
            write (36,15)para,tletale(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tdebgel(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codgellev(ipl)
             read(1,'(a89)') para 
            write (36,12)para,nbfgellev(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgellev10(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgellev90(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codgeljuv(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgeljuv10(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgeljuv90(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codgelveg(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgelveg10(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgelveg90(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codgelflo(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgelflo10(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tgelflo90(ipl)
             read(1,'(a89)') para 
            write (36,12)para
             read(1,'(a89)') para 
            write (36,15)para,psisto(ipl)
             read(1,'(a89)') para 
            write (36,15)para,psiturg(ipl)
             read(1,'(a89)') para 
            write (36,15)para,h2ofeuilverte(ipl)
             read(1,'(a89)') para 
            write (36,15)para,h2ofeuiljaune(ipl)
             read(1,'(a89)') para 
            write (36,15)para,h2otigestruc(ipl)
             read(1,'(a89)') para 
            write (36,15)para,h2oreserve(ipl)
             read(1,'(a89)') para 
            write (36,15)para,h2ofrvert(ipl)
             read(1,'(a89)') para 
            write (36,15)para,deshydbase(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tempdeshyd(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codebeso(ipl)
             read(1,'(a89)') para 
            write (36,15)para,kmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,rsmin(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeintercept(ipl)
             read(1,'(a89)') para 
            write (36,15)para,mouillabil(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stemflowmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,kstemflow(ipl)
             read(1,'(a89)') para 
            write (36,15)para
             read(1,'(a89)') para 
            write (36,15)para,Vmax1(ipl)
             read(1,'(a89)') para 
            write (36,13)para,Kmabs1(ipl)
             read(1,'(a89)') para 
            write (36,15)para,Vmax2(ipl)
             read(1,'(a89)') para 
            write (36,13)para,Kmabs2(ipl)
             read(1,'(a89)') para 
            write (36,15)para,adil(ipl)
             read(1,'(a89)') para 
            write (36,15)para,bdil(ipl)
             read(1,'(a89)') para 
            write (36,15)para,masecNmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,INNmin(ipl)
             read(1,'(a89)') para 
            write (36,15)para,INNimin
             read(1,'(a89)') para 
            write (36,15)para,inngrain1(ipl)
             read(1,'(a89)') para 
            write (36,15)para,inngrain2(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeplisoleN
             read(1,'(a89)') para 
            write (36,15)para,adilmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,bdilmax(ipl)
             read(1,'(a89)') para 
            write (36,15)para,Nmeta(ipl)
! DR 30/01/2012 ajout� ici car il va avec codeplisolN
             read(1,'(a89)') para 
            write (36,15)para,masecmeta
!
             read(1,'(a89)') para 
            write (36,15)para,Nres(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codeINN
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codelegume(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stlevdno(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stdnofno(ipl)
             read(1,'(a89)') para 
            write (36,15)para,stfnofvino(ipl)
             read(1,'(a89)') para 
            write (36,15)para,vitno(ipl)
             read(1,'(a89)') para 
            write (36,15)para,profnod(ipl)
             read(1,'(a89)') para 
            write (36,15)para,concNnodseuil(ipl)
             read(1,'(a89)') para 
            write (36,15)para,concNrac0(ipl)
             read(1,'(a89)') para 
            write (36,15)para,concNrac100(ipl)
!             read(1,'(a89)') para 
!            write (36,15)para,hunod(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tempnod1(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tempnod2(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tempnod3(ipl)
             read(1,'(a89)') para 
            write (36,15)para,tempnod4(ipl)
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codefixpot
             read(1,'(a89)') para 
            write (36,15)para,fixmax(ipl,1)
             read(1,'(a89)') para 
            write (36,15)para,fixmaxveg
             read(1,'(a89)') para 
            write (36,15)para,fixmaxgr
            do i=1,3
             read(1,'(a89)') para 
            write (36,12)para
             enddo
             read(1,'(a89)') para 
            write (36,12)para,codazofruit(ipl)
             read(1,'(a89)') para 
            write (36,12)para
! DR 07/12/09 on ajoute les stadeBBCH suivant la plante
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,1)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,2)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,3)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,4)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,5)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,6)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,7)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,8)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,9)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,10)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,11)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,12)
                  read(1,'(a89)')para
                  write(36,16)para, stadebbch(ipl,13)
! fin codebbch



             read(1,'(a89)') para 
            write (36,12)para
              close(1)
! DR 11/06/07 c'est parti pour les varietes
! DR 21/06/07 pour les varietes je lis dans le fichier plante d'origine
! DR 06/02/08 pour les varietes c'ets le boxon on deplace des parametres donc c'est � ecrire
! !!!!!!!!!!!!!!!!!!!!!!!! a faire !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!            write(36,*)'F: vari�tal '
              open (12,file=oldplt,status='old')
10              read(12,'(a89)',end=10)para
              test=index(para,'TV:')
              if(test.eq.0)goto 10
!            write (36,'(a89)')para
            write (36,'(a39,a30)')'         TV: genotypes                 ',para(40:70)
!                                 '         TV: les diff�rentes vari�t�s                      '
            do j=1,nbvari
             read(12,'(a89)',end=100) para 
            write (36,'(a24,a10,25x,a2)')para,codevar(ipl,j),'22'
            enddo
            do j=1,nbvari
! le nom de la variete
                  read(12,'(a110)',end=100) para 
                  write (36,'(a110)')para
           read(12,'(a69)')ligne
           backspace(12)
              do kk=1,8
                  read(12,'(a110)',end=100) paravar(kk) 
              enddo

              write (36,'(a110)')paravar(1)
            write (36,'(a110)')paravar(2)
            write (36,'(a110)')paravar(3)
! stfflodrp
            write (36,'(a110)')paravar(7)
! stdrpdes
!            write (36,'(a69,a10,10x,f8.2)')ligne,'stdrpdes  ',stdrpdes

            write (36,'(a69,a20,f8.2)')ligne,'stdrpdes             ' ,stdrpdes(ipl)          
              write (36,'(a110)')paravar(4)
            write (36,'(a110)')paravar(5)
            write (36,'(a110)')paravar(6)
            write (36,'(a110)')paravar(8)

              do kk=1,12
                  read(12,'(a110)',end=100) para
                  test=index(para,'nbgrmax  ')
                  if(test.ne.0.and.codeplante(ipl).eq.'sor')then
                      write (36,'(a69,a20,f8.2)')para(1:69),'nbgrmax              ' ,nbgrmax_new
                  else
                      write (36,'(a110)')para
                  endif
              enddo

! dr 28/06/2011 on vire codelegume et fixmax qui sont ailleurs
              do kk=1,2
                  read(12,'(a110)',end=100) para 
              enddo

            enddo

!  17  format(a59,i1)
  100       continue
      close(36)
      close(1)
      close(12)

      return
      end
