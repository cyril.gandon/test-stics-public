      subroutine mouchclim
      include 'common.inc'
!      include 'message.inc'
      include 'comusmplt.inc'
      include 'comparplt.inc'
      include 'comparsol.inc'
      include 'compartec.inc'
      include 'comtransit.inc'
      include 'comclim.inc'
      common/newfiles/newtec,newplt,newobs,newsol,newcli,newsta
      character*255 newtec,newsol,newobs,newplt,newcli,newsta
      character*40 para,paranw,parav6,parast
!c �criture des param�tres actifs dans le mouchard HISTORY.STI
!c----------------------------------------------------------------------
      write(*,*)'debug#','station file'

      para='parameter from param.par'

      paranw='new parameter'

      parav6='parameter from paramv6'
      parast='parameter from climate file'


           write(*,*)'debug#',para,'aangst       ',aangst
           write(*,*)'debug#',para,'aks          ',aks
           write(*,*)'debug#',para,'albveg       ',albveg
           write(*,*)'debug#',parast,'alphapt      ',alphapt
           write(*,*)'debug#',para,'altinversion ',altinversion
           write(*,*)'debug#',para,'altisimul    ',altisimul
           write(*,*)'debug#',para,'altistation  ',altistation
           write(*,*)'debug#',para,'bangst       ',bangst
           write(*,*)'debug#',para,'bks          ',bks
           write(*,*)'debug#',para,'cielclair    ',cielclair
           write(*,*)'debug#',para,'codaltitude  ',codaltitude
           write(*,*)'debug#',para,'codeadret    ',codadret
           write(*,*)'debug#',para,'codecaltemp  ',codecaltemp
           write(*,*)'debug#',para,'codeclichange',codeclichange
           write(*,*)'debug#',para,'codernet previously codeetp 1=PE,2=PC,3=SW,4=PT     ',codernet
           write(*,*)'debug#',parast,'codetp       ',codeetp
           write(*,*)'debug#',para,'coefdevil    ',coefdevil
           write(*,*)'debug#',para,'coefrnet     ',coefrnet
           write(*,*)'debug#',parav6,'corecTrosee  ',corecTrosee
           write(*,*)'debug#',para,'cvent        ',cvent
           write(*,*)'debug#',para,'gradtn       ',gradtn
           write(*,*)'debug#',para,'gradtninv    ',gradtninv
           write(*,*)'debug#',para,'gradtx       ',gradtx
           write(*,*)'debug#',parast,'latitude     ',latitude
           write(*,*)'debug#',para,'NH3ref       ',NH3ref
           write(*,*)'debug#',para,'ombragetx    ',ombragetx
!c           write(*,*)'debug#',parast,'parsurrg     ',parsurrg
           write(*,*)'debug#',parav6,'patm         ',patm
           write(*,*)'debug#',para,'aclim           ',aclim
           write(*,*)'debug#',para,'phiv0        ',phiv0
           write(*,*)'debug#',para,'ra           ',ra
           write(*,*)'debug#',para,'zr           ',zr



      return
      end
