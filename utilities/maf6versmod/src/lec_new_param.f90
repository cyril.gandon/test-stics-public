      subroutine mouchard
      include 'common.inc'
      include 'comform.inc'
      include 'comparplt.inc'
      include 'comvarplt.inc'
      include 'comtransit.inc'
      include 'comparsol.inc'
      include 'comclim.inc'

! subroutine de lecture des valeurs des nouveaux parametres
      character*45 parav6,paraplt,parach,paranw
      character*45 para ,paranf,parasp,parast

! ============================= param.par ===========================
! ===================================================================
! ceux lus dans le param.par utilisateurs
     write(*,*)'info# You can use 2 general parameter files'
     write(*,*)'info# param_gen.xml : default parameter file provided with javastics in the config folder'
     write(*,*)"info# param<user folder>_gen.xml : your translated param.par file in the tmp folder"
     write(*,*)'info# by default the first one is used by the application'
     write(*,*)"info# if you prefer to use the second one you must replace param_gen.xml "&
      &"by param<user folder>_gen.xml and rename it"


       para='parameter readed in param.par'
       parav6='parameter from paramv6'
       paraplt='parameter from plant'
       parach='a change de valeur'
       paranw='new parameter'
       paranf='parameter readed in maj_nouveauxresidus.par'
       parasp='parameter deleted'
       parast='parameter from climate file'

            write(*,*)'debug#',para,'codeinnact      ',codeinnact
            write(*,*)'debug#',para,'codeactimulch   ',codeactimulch
            write(*,*)'debug#',para,'codefrmur       ',codefrmur
            write(*,*)'debug#',parav6,'codefxn         ',codefxn
            write(*,*)'debug#',para,'codeh2oact      ',codeh2oact
            write(*,*)'debug#',para,'codeinitprec    ',codeinitprec
            write(*,*)'debug#',para,'codemicheur     ',codemicheur
            write(*,*)'debug#',para,'codeminopt      ',codeminopt
            write(*,*)'debug#',para,'codemsfinal     ',codemsfinal
            write(*,*)'debug#',parav6,'codeoutscient   ',codeoutscient
            write(*,*)'debug#',para,'codeprofmes     ',codeprofmes
            write(*,*)'debug#',parav6,'codesensibilite ',codesensibilite
            write(*,*)'debug#',parav6,'codeseprapport  ',codeseprapport
            write(*,*)'debug#',para,'codeSIG         ',codeSIG

! DR 04/01/2012 celui la on le lit dans le fichier station et maintenant il est fix� � 0.48
      write(*,*)'debug#','!! fixed to 0.48 !!'
            write(*,*)'debug#',parast,'parsurrg         ',parsurrg


! DR 29/06/2011 celui la pose pb on a pas encore lu la plante
      write(*,*)'debug#','!! attention fixed to 2 !!'
            write(*,*)'debug#',paraplt,'codesymbiose(1) ',codesymbiose(1)
            write(*,*)'debug#',para,'codetycailloux  ',codetycailloux
            write(*,*)'debug#',para,'codetypeng      ',codetypeng
            write(*,*)'debug#',para,'codhnappe       ',codhnappe
            write(*,*)'debug#',para,'iniprofil       ',iniprofil
            write(*,*)'debug#',para,'distdrain       ',distdrain
!            write(*,*)'debug#',para,'alphaphot(1)    ',alphaphot(1)
            write(*,*)'debug#',para,'beta            ',beta
            write(*,*)'debug#',para,'bformnappe      ',bformnappe
            write(*,*)'debug#',para,'coefb           ',coefb
            write(*,*)'debug#',para,'concrr          ',concrr
            write(*,*)'debug#',para,'dacohes         ',dacohes
            write(*,*)'debug#',para,'daseuilbas      ',daseuilbas
            write(*,*)'debug#',para,'daseuilhaut     ',daseuilhaut
            write(*,*)'debug#',para,'difN            ',difN
            write(*,*)'debug#',para,'diftherm        ',diftherm
!            write(*,*)'debug#',parav6,'dltamsminsen(1) ',dltamsminsen(1)
            write(*,*)'debug#',para,'FINERT          ',FINERT
            write(*,*)'debug#',parach,'FMIN1           ',FMIN1
            write(*,*)'debug#',parach,'FMIN2           ',FMIN2
            write(*,*)'debug#',parach,'FMIN3           ',FMIN3
            write(*,*)'debug#',para,'FpHnx devient Fnx ',FpHnx
            write(*,*)'debug#',parach,'hminm           ',hminm
            write(*,*)'debug#',parach,'hminn           ',hminn
            write(*,*)'debug#',parach,'hoptm           ',hoptm
            write(*,*)'debug#',parach,'hoptn           ',hoptn
            write(*,*)'debug#',para,'irrlev          ',irrlev
            write(*,*)'debug#',para,'khaut           ',khaut
            write(*,*)'debug#',para,'lvopt           ',lvopt
!            write(*,*)'debug#',parav6,'masecmeta       ',masecmeta
            write(*,*)'debug#',para,'pHmaxnit        ',pHmaxnit
            write(*,*)'debug#',para,'pHmaxvol        ',pHmaxvol
            write(*,*)'debug#',para,'pHminnit        ',pHminnit
            write(*,*)'debug#',para,'pHminvol        ',pHminvol
            write(*,*)'debug#',para,'plNmin          ',plNmin
            write(*,*)'debug#',para,'pminruis        ',pminruis
            write(*,*)'debug#',parav6,'proflabour(1)   ',proflabour(1)
            write(*,*)'debug#',parav6,'proftravmin(1)  ',proftravmin(1)
            write(*,*)'debug#',para,'proprac         ',proprac
            write(*,*)'debug#',parav6,'psihucc         ',psihucc
            write(*,*)'debug#',parav6,'psihumin        ',psihumin
            write(*,*)'debug#',para,'QNpltminINN     ',QNpltminINN
            write(*,*)'debug#',para,'rayon           ',rayon
            write(*,*)'debug#',para,'rdrain          ',rdrain
            write(*,*)'debug#',parav6,'separateurrapport  ',separateurrapport
            write(*,*)'debug#',parach,'tnitmax         ',tnitmax
            write(*,*)'debug#',para,'tnitmin         ',tnitmin
            write(*,*)'debug#',para,'Vabs2           ',Vabs2
            write(*,*)'debug#',para,'Wh              ',Wh
            write(*,*)'debug#',para,'Xorgmax         ',Xorgmax
! ** les cailloux
            write(*,*)'debug#',para,'codetycailloux ',codetycailloux
      write(*,*)'debug#',para,'masvolcx(i)     hcccx(i)    '
      do i=1,10
      write(*,*)'debug#','cailloux             ',i,':',masvolcx(i),hcccx(i)
      enddo



!** les engrais
            write(*,*)'debug#',para,'codetypeng ',codetypeng
      write(*,*)'debug#',para,'deneng(i)    engamm(i)    orgeng(i)',  &
     '    voleng(i)'

      do i=1,8
      write(*,*)'debug#','engrais       ',i,':',deneng(i),engamm(i), &
         orgeng(i),voleng(i)
      enddo
!** les residus

         write(*,*)'debug#','les residus sont lus dans maj_nouveauxresidus.par'
            write(*,*)'debug#',para,'codetypres ',codetypres
         write(*,*)'debug#',parach,'akres '
         write(*,*)'debug#',parach,'bkres '
         write(*,*)'debug#',parach,'awb '
         write(*,*)'debug#',parach,'bwb '
         write(*,*)'debug#',parach,'ahres '
         write(*,*)'debug#',parach,'bhres '
         write(*,*)'debug#',parach,'kbio '
      write(*,*)'debug#',paranf,'akres(i)  bkres(i)  awb(i)  bwb(i)  ',&
       'cwb(i)  ahres(i) bhres(i)  kbio(i)   yres(i)   yres(i) ',&
       'CNresmin(i) CNresmax(i)'

      do i=1,21
      write(*,325)i,akres(i),bkres(i),awb(i),&
       bwb(i),cwb(i),ahres(i),bhres(i),kbio(i),yres(i),yres(i), &
       CNresmin(i),CNresmax(i)
  325 format('debug#residus      ',i2,':',27x,12(f10.4))
      enddo
! ** les mulchs
         write(*,*)'debug#','parameters are readed in ',&
          ' maj_nouveauxresidus.par in the same time of ', &
      ' residues parameters'
         write(*,*)'debug#',parasp,'codemulch '
         write(*,*)'debug#',paranw,'Qmulchdec '
      write(*,*)'debug#',paranf,'qmulchruis0(i)  ',&
      'mouillabilmulch(i)  kcouvmlch(i)  albedomulch(i) ',  &
      'Qmulchdec(i)   '

      do i=1,21
      write(*,*)'debug#','mulchs     ',i,':',   &
      qmulchruis0(i),mouillabilmulch(i),kcouvmlch(i),albedomulch(i), &
      Qmulchdec(i)
      enddo


! les nouveaux
!           write(*,*)'debug#',paranw,'Cmulchdec        ',Cmulchdec
           write(*,*)'debug#',paranw,'alphaph          ',alphaph
           write(*,*)'debug#',paranw,'dphvolmax        ',dphvolmax
           write(*,*)'debug#',paranw,'fhminsat         ',fhminsat
           write(*,*)'debug#',paranw,'fNCbiomin        ',fNCbiomin
           write(*,*)'debug#',paranw,'fredNsup         ',fredNsup
           write(*,*)'debug#',paranw,'Primingmax       ',Primingmax
           write(*,*)'debug#',paranw,'y0msrac          ',y0msrac


           write(*,*)'debug#',paranw,'fredkN           ',fredkN
           write(*,*)'debug#',paranw,'fredlN           ',fredlN
           write(*,*)'debug#',paranw,'FTEMh            ',FTEMh
           write(*,*)'debug#',paranw,'FTEMha           ',FTEMha
           write(*,*)'debug#',paranw,'FTEMr            ',FTEMr
           write(*,*)'debug#',paranw,'FTEMra           ',FTEMra
           write(*,*)'debug#',paranw,'phvols           ',phvols
           write(*,*)'debug#',paranw,'prophumtassrec   ',prophumtassrec
           write(*,*)'debug#',paranw,'prophumtasssem   ',prophumtasssem
!           write(*,*)'debug#',paranw,'Qmulchdec        ',Qmulchdec
           write(*,*)'debug#',paranw,'ratiodenit       ',ratiodenit
           write(*,*)'debug#',paranw,'rationit         ',rationit
!           write(*,*)'debug#',paranw,'tnitopt          ',tnitopt
           write(*,*)'debug#',paranw,'trefh            ',trefh
           write(*,*)'debug#',paranw,'trefr            ',trefr
! les supprim�s
! tnitopt1 et tnitopt2
      para= 'parameter deleted '
          write(*,*)'debug#',para,'codeminhum'
!          write(*,*)'debug#',para,'tnitopt1 et tnitopt2'

      
      return
      end
