! ********************************************************** c
! *    lecture et initialisation des parametres generaux   * c
! *    fichier param.par                                   * c
! !!!!! lecture du param.par utilisateur pour transformation!!!!!!!!!!!!* c
! *    version 5.1 - 23/02/2004                            * c
! ********************************************************** c

      subroutine lecparam 


! ** les d�clarations :

! ** les COMMONS
      include 'common.inc'
      include 'comparsol.inc'
! ** pour codepaillage,codeabri  
      include 'compartec.inc'
      include 'comparplt.inc'
      include 'comtransit.inc'
      include 'comclim.inc'
!      include 'message.inc'

! ** les VARIABLES LOCALES
      character*15 para
      character*20 nompara
      integer icx,ieng
!  integer nligne

! ** ouverture du fichier param.par
! *!* A VOIR : definir le chemin (./ ou ../)
!      open (12,file='../param.par',status='old',err=251)

! ** transformation du fichier en un fichier transitoire  TEMPO.STI
      open (36,file='tempopar.sti',status='unknown')


10    read(12,'(69x,a20,a15)',end=15)nompara,para
      if(nompara.ne.'                   ') then
        write(36,'(a15)') nompara
        write(36,'(a15)') para
      endif
      goto 10

15    continue
! *- selon les compilateurs, la derni�re ligne sort avant traitement.
      if(nompara.ne.'                   ') then
        write(36,'(a15)') nompara
        write(36,'(a15)') para
      endif
      close(36)

! ** lecture du fichier TEMPO.STI
      open (36,file='tempopar.sti',status='old')

      read (36,*) para
      read (36,*,err=250) codeinnact
      read (36,*)
      read (36,*,err=250) codeh2oact
      read (36,*)
      read (36,*,err=250) codeminopt
      read (36,*)
      read (36,*,err=250) iniprofil
      read (36,*)
      read (36,*,err=250) codeprofmes
      read (36,*)
      read (36,*,err=250) codeinitprec
! ** 29/04/03 - rajout de la suppression le 09/01/02
! *- codemsfinal=1 -->maintient de recolte � fin
! *- codemsfinal=2 -->masec � 0 � la recolte
      read (36,*)
      read (36,*,err=250) codemsfinal
      read (36,*)
      read (36,*,err=250) codeactimulch
! ** si codeulaivernal = 1 la vernalisation joue sur ulai, si = 0 joue pas
! *- on met en dur dans initial.for : codeulaivernal = 1
! --      read (36,*)
! --      read (36,*,err=250) codeulaivernal
      read (36,*)
      read (36,*,err=250) codefrmur
      read (36,*)
      read (36,*,err=250) codemicheur
      read (36,*)
      read (36,*,err=250) codeSIG
      read (36,*)
      read (36,*,err=250) coefb
      read (36,*)
      read (36,*,err=250) proprac
      read (36,*)
! --      read (36,*,err=250) splaimin
! --      read (36,*)
      read (36,*,err=250) khaut
      read (36,*)
      read (36,*,err=250) dacohes
      read (36,*)
      read (36,*,err=250) daseuilbas
      read (36,*)
      read (36,*,err=250) daseuilhaut
      read (36,*)      
      read (36,*,err=250) beta
      read (36,*)
      read (36,*,err=250) lvopt
      read (36,*)
      read (36,*,err=250) rayon
      read (36,*)
      read (36,*,err=250) difN
      read (36,*)
      read (36,*,err=250) concrr
      read (36,*)
      read (36,*,err=250) plNmin
      read (36,*)
! ** NB - le 28/03/02
      read (36,*,err=250) irrlev
      read (36,*)
      read (36,*,err=250) QNpltminINN
      read (36,*)
      read (36,*,err=250) FTEM
      read (36,*)
      read (36,*,err=250) TREF
      read (36,*)
      read (36,*,err=250) FTEM1
      read (36,*)
      read (36,*,err=250) FTEM2
      read (36,*)
      read (36,*,err=250) FTEM3
      read (36,*)
      read (36,*,err=250) FTEM4
      read (36,*)
      read (36,*,err=250) FHUM
! modif Bruno : introduction d'une fraction inerte de MO humifi�e
      read (36,*)
      read (36,*,err=250) FINERT
! fin de modif
      read (36,*)
      read (36,*,err=250)
!       FMIN1
      read (36,*)
      read (36,*,err=250)
!       FMIN2
      read (36,*)
      read (36,*,err=250)
!       FMIN3  
      read (36,*)
      read (36,*,err=250) Wh
      read (36,*)
      read (36,*,err=250) pHminvol
      read (36,*)
      read (36,*,err=250) pHmaxvol
      read (36,*)
      read (36,*,err=250) Vabs2
      read (36,*)
      read (36,*,err=250) Xorgmax
      read (36,*)
      read (36,*,err=250) codeminhum
      read (36,*)
      read (36,*,err=250)
!       hminm
      read (36,*)
      read (36,*,err=250)
!       hoptm
      read (36,*)
      read (36,*,err=250)
!       hminn
      read (36,*)
      read (36,*,err=250)
!       hoptn
      read (36,*)
      read (36,*,err=250) FpHnx
      read (36,*)
      read (36,*,err=250) pHminnit
      read (36,*)
      read (36,*,err=250) pHmaxnit
      read (36,*)
      read (36,*,err=250) tnitmin
      read (36,*)
      read (36,*,err=250) tnitopt1
      read (36,*)
      read (36,*,err=250) tnitopt2
      read (36,*)
      read (36,*,err=250) tnitmax
      read (36,*)

! **    pertes gazeuses: d�nitrification et volatilisation
      read (36,*,err=250) codedenit
      read (36,*)
      read (36,*,err=250) profdenit
      read (36,*)
      read (36,*,err=250) vpotdenit
      read (36,*)
      read (36,*,err=250) zr
      read (36,*)
      read (36,*,err=250) NH3ref
      read (36,*)
      read (36,*,err=250) ra
      read (36,*)
      read (36,*,err=250) aangst
      read (36,*)
      read (36,*,err=250) bangst
      read (36,*)
      read (36,*,err=250) albveg
      read (36,*)
      read (36,*,err=250) codernet
      read (36,*)
      read (36,*,err=250) codecaltemp
      read (36,*)
      read (36,*,err=250) codeclichange
      read (36,*)
      read (36,*,err=250) CO2_param
      read (36,*)
      read (36,*,err=250) alphaCO2(1)
      read (36,*)
      read (36,*,err=250) codaltitude
      read (36,*)
      read (36,*,err=250) altistation
      read (36,*)
      read (36,*,err=250) altisimul
      read (36,*)
      read (36,*,err=250) gradtn
      read (36,*)
      read (36,*,err=250) gradtx
      read (36,*)
      read (36,*,err=250) altinversion
      read (36,*)
      read (36,*,err=250) gradtninv
      read (36,*)
      read (36,*,err=250) cielclair
      read (36,*)
      read (36,*,err=250) codadret
      read (36,*)
      read (36,*,err=250) ombragetx
      read (36,*)
      read (36,*,err=250) coefdevil
      read (36,*)
      read (36,*,err=250) aks
      read (36,*)
      read (36,*,err=250) bks
      read (36,*)
      read (36,*,err=250) cvent
      read (36,*)
      read (36,*,err=250) phiv0
      read (36,*)
      read (36,*,err=250) coefrnet
      read (36,*)
      read (36,*,err=250) pminruis
      read (36,*)
      read (36,*,err=250) aclim
      read (36,*)
      read (36,*,err=250) zesx
      read (36,*)
      read (36,*,err=250) cfes
      read (36,*)
      read (36,*,err=250) z0solnu
      read (36,*)
      read (36,*,err=250) diftherm
      read (36,*)
      read (36,*,err=250) Bformnappe
      read (36,*)
      read (36,*,err=250) rdrain
      read (36,*)
      read (36,*,err=250) codhnappe
      read (36,*)
      read (36,*,err=250) distdrain
      read (36,*)

! ** cailloux
      read (36,*,err=250) codetycailloux
      read (36,*)
      do 21 icx=1,10
        read (36,*,err=250) masvolcx(icx)
        read (36,*)
        read (36,*,err=250) hcccx(icx)
        read (36,*)
21    continue 

! ** engrais
      read (36,*,err=250) codetypeng
      read (36,*)
      do 17 ieng=1,8
        read (36,*,err=250) engamm(ieng)
        read (36,*)
        read (36,*,err=250) orgeng(ieng)
        read (36,*)
        read (36,*,err=250) deneng(ieng)
        read (36,*)
        read (36,*,err=250) voleng(ieng)
        read (36,*)
17    continue


!v dr 29/06/2011 c'est lu dans maj_paramv6.par
      goto 1200
! ** r�sidus
      read (36,*,err=250) codetypres
      read (36,*)
      do 19 ires=1,8
        read (36,*,err=250) akres(ires)
        read (36,*)
        read (36,*,err=250) bkres(ires)
        read (36,*)
        read (36,*,err=250) awb(ires)
        read (36,*)
        read (36,*,err=250) bwb(ires)
        read (36,*)
        read (36,*,err=250) cwb(ires)
        read (36,*)
        read (36,*,err=250) ahres(ires)
        read (36,*)
        read (36,*,err=250) bhres(ires)
        read (36,*)
        read (36,*,err=250) kbio(ires)
        read (36,*)
        read (36,*,err=250) yres(ires)
        read (36,*)
        read (36,*,err=250) CNresmin(ires)
        read (36,*)
        read (36,*,err=250) CNresmax(ires)
        read (36,*)
19    continue      

! ** domi 21/10/2004 on a pass� la typologie des mulch en tableau pour pouvoir
!    deplacer lecparam avant lectech 
      read (36,*,err=250) codemulch
!      if(codepaillage(1).eq.2) then
!        do 11 icx=1,typemulch*5,5
        do 11 icx=1,5
          read (36,*,end=150)
          read (36,*,err=250,end=150) decomposmulch(icx)
          read (36,*,end=150)
          read (36,*,err=250,end=150) qmulchruis0(icx)
          read (36,*)
          read (36,*,err=250) mouillabilmulch(icx)
          read (36,*)
          read (36,*,err=250) kcouvmlch(icx)
          read (36,*)
          read (36,*,err=250) albedomulch(icx)
11        continue 
!      endif

 1200 continue

        
150   continue


! --      coefb=coefb/100.0 
      goto 280

250   write(*,270)
      write(*,*) 'error#',mes105
      codemauvais=1
!      write(*,*) mes105
      stop

      write (fichist,270)
      write(*,*) 'error#',mes106
!      write(*,*) mes106
      stop
      
270   FORMAT(5(/))

280   continue
      
      close(12)
      close(36)
      end

