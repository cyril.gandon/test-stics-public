! *********************************** c
! *     variables transitoires      * c
! *********************************** c

    

! ** pour Guenaelle - diff�rentes pour chaque plante ??

      integer codazorac,codtrophrac,codemsfinal
      
      real minefnra,maxazorac,minazorac
      real repracmax(2),repracmin(2),kreprac(2)
      
      common /guenaint/codazorac,codtrophrac,codemsfinal
      
      common /guenareal/minefnra,maxazorac,minazorac, &
                       repracmax,repracmin,kreprac


! ** pour ESA - fixation symbiotique
      integer codefixpot
      real afixmaxN,bfixmaxN,fixmaxveg,fixmaxgr

      common /fixsymb/codefixpot,afixmaxN,bfixmaxN,fixmaxveg,fixmaxgr


! ** pour thomas - culture pure uniquement

      integer epitN,epijul1(7),epijul2(7)
      integer codhumoffN,Noffn,epibN
      
      real epiQNmax,juloffN(366)
      real pondealpha,pondebeta,epik(7)
      real dosiNmin,dosiWmin,normealpha,normebeta
! --      real epibN
      
      real epiQN(720),epikc(720),epiqnplmax(720),tempoffN(720)
      real hoffrn(0:720)
      
      common /epitafint/epitN,epijul1,epijul2,codhumoffN,Noffn,epibN
      
      common /epitafreal/epiQNmax,juloffN,pondealpha,pondebeta,&
      epik,dosiNmin,dosiWmin,normealpha,normebeta
   
      common /epitaftab/epiQN,epikc,epiqnplmax,tempoffN,hoffrN

! ** NB - le 07/06/2004 - calcul de psibase
      real psihumin,psihucc,psibase(2)

      common/psibasecom/psihumin,psihucc,psibase

! ** Domi - le 20/09/2004 - calcul du debourrement pour perenne
      real tdmindeb(2),tdmaxdeb(2)
      integer codegdhdeb(2)
      common/debourcom/tdmindeb,tdmaxdeb,codegdhdeb

! ** Domi - le 30/09/2004 - ecriture "speciales" dans s*.sti
! ** DR 23/06/05  rajout de codeoutQfix
! ** domi 28/06/05   on vire codeoutfixreel qui n'etait pas correct
! ** domi 09/01/06 on rajoute pour inaki ecriture de mabois et de H2Orec
      integer codeoutsti,coderecolteassoc
      common/codesorties/codeoutsti,coderecolteassoc

! ** PB - 03/03/2005 - on recalcule le matigestruc � la fauche en fontion de la 
! *- biomasse r�siduelle et du param�tre plante <ratioresfauche>
      real ratioresfauche(2)
        common /perennes03032005/ratioresfauche
        
! ** NB le 11/02/05 nouveaut�s courbes de dilution N
! r�vis� le 11/09/05
      integer codeplisoleN
      real bdilI(2),masecmeta,Nmeta(2),Nres(2),adilI(2)
      real adilmaxI(2),bdilmaxI(2)
      common/innovN/codeplisoleN,masecmeta,Nmeta,Nres,adilI,   &
     adilmaxI,bdilmaxI,bdilI
! ** NB le 07/04/05 nouveaut�s INNI
      integer codeINN,codeHxN
      real inni,deltabso,INNimin,dltamsN
      common/innov070405/codeINN,inni(2,2),deltabso(2,2),INNimin, &
     dltamsN(2,2),codeHxN
! NB le 11/04/05 nouveaut�s lev�es
      logical humectation
      integer nbjgerlim,nbjhumec
      real densiteger,potgermi
      common/innovlevee/humectation(2),nbjgerlim(2),nbjhumec(2) &
     ,densiteger(2),potgermi(2)
! NB le 12/04/05 nouveaut�s fruit
      integer codenormfruit,codefruitcroi
      real pfmax2
      common/innovfruit/codenormfruit,codefruitcroi,pfmax2(2)
! NB le 13/05/05 battance
      real vigueurbat,pluiebat,mulchbat,pluiesemis
      common/bat/vigueurbat(2),pluiebat,mulchbat,pluiesemis
! NB le 30/05/05
      real QNplantetombe
      common/nb300505/QNplantetombe
! NB le 01/07/05 hauteur et largeur techniques
      real hautmaxtec(2),largtec(2)
      common/formetec/hautmaxtec,largtec
! DR le 04/11/05
        integer codeoutscient
        common/formatout/codeoutscient
! DR le 03/01/06 parametres inaki stressphot
      real dltamsminsen,alphaphot,phobasesen,dltamsmaxsen
      integer  codestrphot
        common/dr030106/dltamsminsen(2),dltamsmaxsen(2),alphaphot(2),&
       codestrphot(2),phobasesen(2)

! dr et ml 170206 parametres
      integer codebet
      real remobfruit, seuiltustress
      real seuilsp, splaiminmoins, splaiminplus
! dr et ml 170206 varaibles
      real deltatustress,deltasp,tustressdelaveille
      real sourcepuitsdelaveille
      integer codeseprapport
      character*1 separateurrapport
      common /dr170206/codebet, remobfruit(2), seuiltustress(2)&
      ,seuilsp(2), splaiminmoins(2), splaiminplus(2), deltatustress&
      ,deltasp(2,2), tustressdelaveille, sourcepuitsdelaveille(2,2)& 
      , codeseprapport,separateurrapport
! NB le 15/02/06 parametres DST
      integer  codeDST(2)
      real QH2Oi,QNO3i,QNH4i,proflabour(2),proftravmin(2),dachisel(2)
      real rugochisel(2),rugolabour(2),dalabour(2)
        common/NB150206/codeDST,QH2Oi,QNO3i,QNH4i,proflabour,  &
             proftravmin,dachisel,dalabour,rugochisel,rugolabour
! DR le 23/03/06 un code pour lire hautbasetec et largtec dans le fichier technique pour inaki
      integer codelectec
      common/DR230306/codelectec
! DR et celia 31/03/06
      integer codeDSTtass(2),codedecisemis(2),codedecirecolte(2)
      integer nbjmaxapressemis(2), nbjseuiltempref(2)
      integer codeDSTnbcouche(2),nbjmaxapresrecolte(2)
      real profhumsemoir(2),profhumrecolteuse(2),humseuiltasssem(2)
      real humseuiltassrec(2)
      real dasemis(2),tnrefgelble,tnrefgelbet,tnrefgelmais,tnrefgelqui
      real tmrefgelble,tmrefgelbet,tmrefgelmais,tmrefgelqui,darecolte(2)
      common /dr310306/codeDSTtass,codedecisemis,codedecirecolte   &
     ,nbjmaxapressemis, nbjseuiltempref,nbjmaxapresrecolte  &
     ,codeDSTnbcouche,profhumsemoir,profhumrecolteuse,humseuiltasssem&
     ,humseuiltassrec,dasemis,tnrefgelble,tnrefgelbet &
     ,tnrefgelmais,tmrefgelble,tmrefgelbet,tmrefgelmais,darecolte &
     ,tnrefgelqui,tmrefgelqui

      integer codetempfauche(2)
      real tcxstop(2)
      common/dr100406/codetempfauche,tcxstop


      real cfpf(2),dfpf(2),dltaremobilN(2,2)
      common/dr130606/cfpf,dfpf,dltaremobilN

      real spfourrage,nspfourrage
      common/nb070706/spfourrage,nspfourrage

! DR le 17/10/06 un code pour lire zesx,cfes,z0solnu dans le fichier sol pour nicoullaud
      integer codesoldst
      common/DR171006/codesoldst
! DR 160107 pour specificit�s quinoa en poquet , les pluies peuvent etre prise en compte
! comme des irrigations en profondeur 
      integer codepluiepoquet,nbjoursrrversirrig
      common/dr160107/codepluiepoquet,nbjoursrrversirrig
! dr 14/06/07 les parametres ont ete renomm�s par marie
      integer nbvari
      common/dr210607/nbvari
      integer nbcoupe2,nbcoupe3,codepalissage(2),codedateappH2O(2)
      integer codedateappN(2)
      common/dr220607/nbcoupe2,nbcoupe3,codepalissage,codedateappH2O &
     ,codedateappN


! modifs NB integrees le 30/08/07
! NB le 22/02/07 pour germination
      real propjgermin,somtemphumec(2)
      common/nb220207/propjgermin(2),somtemphumec
! NB le 28/02/07 pour croissance foilaire prairie
      real dlaimin(2)
      common/nb280207/dlaimin
! NB le 08/03/07
      real SurfApex(2),SeuilMorTalle(2),SigmaDisTalle(2)
      real VitReconsPeupl(2),SeuilReconsPeupl(2),MaxTalle(2)
      real swfacmin,LAIapex(2),mortreserve(2)
      integer codetranspitalle,codedyntalle
      common/nb080307/SurfApex,SeuilMorTalle,SigmaDisTalle,&
      VitReconsPeupl,SeuilReconsPeupl,MaxTalle,swfacmin,codetranspitalle  &
     ,LAIapex,mortreserve,codedyntalle
! DR le 12/09/07 pour benjamin 
      real codeirrigmin,doseirrigmin(2),codeforcedrpdes
      common/dr120907/codeirrigmin,doseirrigmin,codeforcedrpdes
! DR le 14/09/07 pour benjamin date des irrigations pour l'instant 100 irrigations possibles
      integer dateirr(2,100)
      integer nbapirr(2)
      common/dr140907/dateirr,nbapirr
      integer dateN(2,100),nbapN(2)
      common/dr170907/dateN,nbapN

! 03/07/08 nouveaux parametres
      real FTEMh,FTEMha,FTEMr,FTEMra,trefr,tnitopt,rationit,ratiodenit
      real alphaph,dphvolmax,phvols,prophumtassrec,prophumtasssem
      real fhminsat
      common/dr020708/ FTEMh,FTEMha,FTEMr,FTEMra,trefr,tnitopt,rationit,&
      ratiodenit,alphaph,dphvolmax,phvols,prophumtassrec,prophumtasssem&
      ,fhminsat
! DR 20/06/2011 ajout des nouveaux param
      real fncbiomin, fredlN,fredkN,trefh,Qmulchdec(21),cmulchdec(4)

      common/dr20062011/ fncbiomin, fredlN,fredkN,trefh,Qmulchdec &
     ,cmulchdec




