################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
F90_SRCS += \
../src/compteblanc.f90 \
../src/ecrinouvcli.f90 \
../src/ecrinouvobs.f90 \
../src/ecrinouvparampar.f90 \
../src/ecrinouvplt.f90 \
../src/ecrinouvsol.f90 \
../src/ecrinouvtec.f90 \
../src/lec_new_param.f90 \
../src/lecparam.f90 \
../src/lecparamv6.f90 \
../src/lecplant.f90 \
../src/lecsol.f90 \
../src/lecstat.f90 \
../src/lectech.f90 \
../src/lecusm.f90 \
../src/message.f90 \
../src/misoform70.f90 \
../src/mouchclim.f90 \
../src/mouchplt.f90 \
../src/mouchsol.f90 \
../src/mouchtec.f90 \
../src/oteblanc.f90 \
../src/trimo.f90 

OBJS += \
./src/compteblanc.o \
./src/ecrinouvcli.o \
./src/ecrinouvobs.o \
./src/ecrinouvparampar.o \
./src/ecrinouvplt.o \
./src/ecrinouvsol.o \
./src/ecrinouvtec.o \
./src/lec_new_param.o \
./src/lecparam.o \
./src/lecparamv6.o \
./src/lecplant.o \
./src/lecsol.o \
./src/lecstat.o \
./src/lectech.o \
./src/lecusm.o \
./src/message.o \
./src/misoform70.o \
./src/mouchclim.o \
./src/mouchplt.o \
./src/mouchsol.o \
./src/mouchtec.o \
./src/oteblanc.o \
./src/trimo.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.f90
	@echo 'Building file: $<'
	@echo 'Invoking: GNU Fortran Compiler'
	gfortran -funderscoring -O0 -g -Wall -c -fmessage-length=0 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/compteblanc.o: ../src/compteblanc.f90

src/ecrinouvcli.o: ../src/ecrinouvcli.f90

src/ecrinouvobs.o: ../src/ecrinouvobs.f90

src/ecrinouvparampar.o: ../src/ecrinouvparampar.f90

src/ecrinouvplt.o: ../src/ecrinouvplt.f90

src/ecrinouvsol.o: ../src/ecrinouvsol.f90

src/ecrinouvtec.o: ../src/ecrinouvtec.f90

src/lec_new_param.o: ../src/lec_new_param.f90

src/lecparam.o: ../src/lecparam.f90

src/lecparamv6.o: ../src/lecparamv6.f90

src/lecplant.o: ../src/lecplant.f90

src/lecsol.o: ../src/lecsol.f90

src/lecstat.o: ../src/lecstat.f90

src/lectech.o: ../src/lectech.f90

src/lecusm.o: ../src/lecusm.f90

src/message.o: ../src/message.f90

src/misoform70.o: ../src/misoform70.f90

src/mouchclim.o: ../src/mouchclim.f90

src/mouchplt.o: ../src/mouchplt.f90

src/mouchsol.o: ../src/mouchsol.f90

src/mouchtec.o: ../src/mouchtec.f90

src/oteblanc.o: ../src/oteblanc.f90

src/trimo.o: ../src/trimo.f90


