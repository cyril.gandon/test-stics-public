module utils
   use, intrinsic :: iso_fortran_env, only: output_unit
   use stdlib_string_type
   use stdlib_stringlist_type
   use stdlib_strings, only: to_string
   use stdlib_io
   use fortran202x_split

   implicit none

contains

   subroutine execute_or_exit(command)
      character(*), intent(in) :: command

      integer :: exitstat
      character(len=255) :: current_working_directory

      call getcwd(current_working_directory)

      call write_screen("Executing command ["//command//"] in directory ["//trim(current_working_directory)//"]")
      call execute_command_line(command, exitstat=exitstat)
      if (exitstat .ne. 0) then
         call exit_error("Fail to run command ["//command//"]. Exit status = "//to_string(exitstat)//"...")
      end if
   end subroutine

   !> Execute a `which` in unix, a `where` on windows
   subroutine which(program)
      character(*), intent(in) :: program
      if (is_unix()) then
         call execute_or_exit('which '//program)
      else
         call execute_or_exit('where '//program)
      end if
   end subroutine

   logical function compare_files(actual, expected) result(success)
      type(string_type), allocatable, intent(in) :: actual(:), expected(:)
      type(string_type) :: actual_row, expected_row

      type(stringlist_type) :: compare_lines_errors
      integer :: row_index
      logical :: compare_lines_success

      success = .true.
      if (size(actual) .ne. size(expected)) then
         call write_screen('compare_files error: actual length = '//to_string(size(actual))// &
                           ' - expected length = '//to_string(size(expected)))
         success = .false.
         return
      end if

      do row_index = 1, size(actual)
         actual_row = actual(row_index)
         expected_row = expected(row_index)
         compare_lines_errors = compare_lines(actual_row, expected_row)
         if (compare_lines_errors%len() .gt. 0) then
            call write_screen('difference on line = '//to_string(row_index))
            call write_screen(char(actual_row))
            call write_screen(char(expected_row))

            call write_screen_messages(compare_lines_errors)
            success = .false.
         end if
      end do
   end function

   type(stringlist_type) function compare_lines(actual, expected) result(errors)
      type(string_type), intent(in) :: actual, expected
      character(1) :: actual_char, expected_char
      integer :: char_index

      if (len(actual) .ne. len(expected)) then
         errors = errors//('compare_lines error: actual length = '//to_string(len(actual))// &
                           ' - expected length = '//to_string(len(expected)))
         return
      end if

      do char_index = 1, len(actual)
         actual_char = char(actual, char_index, char_index)
         expected_char = char(expected, char_index, char_index)

         if (actual_char /= expected_char) then
            errors = errors//('Column ['//to_string(char_index)// &
                              ']:  ['//expected_char//'] != ['//actual_char//']')
         end if
      end do
   end function

   logical function try_parse_lines(str, separator, parsed) result(success)
      character(*), intent(in) :: str, separator
      character(:), allocatable :: tokens(:)
      real, allocatable, intent(out) :: parsed(:)
      character(:), allocatable :: token
      type(string_type), allocatable :: filtered(:)
      integer :: i
      call split(str, separator, tokens)

      filtered = filter_empty(tokens)
      allocate (parsed(size(filtered)))
      do i = 1, size(parsed)
         token = char(filtered(i))

         success = try_parse_real(token, parsed(i))
         if (success .eqv. .false.) then
            return
         end if
      end do
   end function

   function filter_empty(strings_in) result(filtered)
      character(:), allocatable, intent(in) :: strings_in(:)
      type(string_type), allocatable :: filtered(:)
      type(string_type) :: value_in
      integer :: i, non_empty_count, filtered_index

      filtered_index = 1
      non_empty_count = 0
      do i = 1, size(strings_in)
         if (trim(strings_in(i)) .ne. "") then
            non_empty_count = non_empty_count + 1
         end if
      end do
      allocate (filtered(non_empty_count), source=string_type())
      do i = 1, size(strings_in)
         value_in = strings_in(i)
         if (trim(value_in) .ne. "") then
            filtered(filtered_index) = value_in
            filtered_index = filtered_index + 1
         end if
      end do
   end function

    !> Compare two lines containing integers
   type(stringlist_type) function compare_integer_lines(actual_values, expected_values) result(errors)
      type(string_type), intent(in) :: actual_values(:), expected_values(:)

      integer :: value_index, iostat

      if (size(actual_values) .ne. size(expected_values)) then
         errors = errors//('compare_integer_lines error: actual length = '//to_string(size(actual_values))// &
                           ' - expected length = '//to_string(size(expected_values)))
         return
      end if

      do value_index = 1, size(expected_values)
         block
            integer :: expected_value, actual_value, diff
            character(:), allocatable :: expected_str, actual_str

            expected_str = char(expected_values(value_index))
            if (try_parse_int(expected_str, expected_value) .eqv. .false.) then
               errors = errors//("cannot parse expected value ["//expected_str//"]")
            end if

            actual_str = char(actual_values(value_index))
            if (try_parse_int(actual_str, actual_value) .eqv. .false.) then
               errors = errors//("cannot parse actual value ["//actual_str//"]")
            end if
            if (errors%len() .gt. 0) then
               return
            end if
            if (expected_value .ne. actual_value) then
               errors = errors//('Line ['//to_string(value_index)// &
                                 ']:  ['//to_string(expected_value)//'] != ['//to_string(actual_value)//']')
            end if
         end block
      end do
   end function

   !> Compare two lines containing reals
   type(stringlist_type) function compare_real_lines(actual_values, expected_values, error_threshold) result(errors)
      type(string_type), intent(in) :: actual_values(:), expected_values(:)
      real, intent(in) :: error_threshold

      integer :: value_index, iostat

      if (size(actual_values) .ne. size(expected_values)) then
         errors = errors//('compare_number_lines error: actual length = '//to_string(size(actual_values))// &
                           ' - expected length = '//to_string(size(expected_values)))
         return
      end if

      do value_index = 1, size(expected_values)
         block
            real :: expected_value, actual_value, diff
            character(:), allocatable :: expected_str, actual_str

            expected_str = char(expected_values(value_index))
            if (try_parse_real(expected_str, expected_value) .eqv. .false.) then
               errors = errors//("cannot parse expected value ["//expected_str//"]")
            end if

            actual_str = char(actual_values(value_index))
            if (try_parse_real(actual_str, actual_value) .eqv. .false.) then
               errors = errors//("cannot parse actual value ["//actual_str//"]")
            end if
            if (errors%len() .gt. 0) then
               return
            end if
            diff = relative_difference(expected_value, actual_value)
            if (diff .ge. error_threshold) then
               errors = errors//('Line ['//to_string(value_index)// &
                                 ']:  ['//to_string(expected_value)//'] != ['//to_string(actual_value)//']'// &
                                 " relative difference = "//percent_str(diff))
            end if
         end block
      end do
   end function

   real pure function relative_difference(theoric, experimental) result(diff)
      real, intent(in) :: theoric, experimental
      if (theoric .eq. 0) then
         if (experimental .eq. 0) then
            diff = 0.
         else
            ! Relatvie difference here is basically infinity.
            ! Returns a difference of 100%
            diff = 1.
         end if
      else
         diff = abs((experimental - theoric)/theoric)
      end if
   end function

   subroutine write_file(path, rows)
      character(*), intent(in) :: path
      type(stringlist_type), intent(in) :: rows

      integer :: unit, iostat, row_index
      type(string_type) :: row

      open (newunit=unit, file=path, status='unknown', action='write', iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error('Error opening path for write action: path=['//path//'], iostat='//to_string(iostat))
      end if

      do row_index = 1, rows%len()
         row = rows%get(fidx(row_index))
         write (unit, '(A)') char(row)
      end do

      close (unit)
   end subroutine

   logical function try_load_file(path, rows) result(success)
      character(*), intent(in) :: path
      type(string_type), allocatable, intent(out) :: rows(:)

      integer :: unit, iostat_open, iostat_read, i, line_count
      character(5012) :: read_buffer

      open (newunit=unit, file=path, status='old', action='read', iostat=iostat_open)
      success = iostat_open .eq. 0
      if (success .eqv. .false.) then
         return
      end if

      line_count = number_of_rows(unit)
      allocate (rows(line_count))

      i = 1
      do
         read (unit, "(A)", iostat=iostat_read) read_buffer
         if (iostat_read /= 0) exit
         rows(i) = trim(read_buffer)
         i = i + 1
      end do
      close (unit)
   end function

   character(len=1) function filesep()
      if (is_unix()) then
         filesep = '/'
      else
         filesep = '\'
      end if
   end function

   logical function is_unix()
      character(len=255) :: cwd
      call getcwd(cwd)
      if (index(trim(cwd), '/') .gt. 0) then
         is_unix = .true.
      else
         is_unix = .false.
      end if
   end function

   subroutine write_screen_messages(messages)
      type(stringlist_type), intent(in) :: messages
      integer :: i
      do i = 1, messages%len()
         call write_screen(char(messages%get(fidx(i))))
      end do
   end subroutine

   subroutine write_screen(message)
      character(*), intent(in) :: message

      write (output_unit, *) message
   end subroutine

   subroutine exit_error(error_message)
      character(*), intent(in) :: error_message

      write (output_unit, *) error_message
      call exit(9)
   end subroutine

   function read_dir_content(path) result(content)
      character(*), intent(in) :: path

      integer :: unit, i, rows_count, status
      type(string_type), allocatable :: content(:)
      character(:), allocatable :: temp_file_name
      character(255) :: buffer

      !! file separator
      character(1) :: s
      s = filesep()

      temp_file_name = ".read_dir_content_temp"
      ! get the files
      if (is_unix()) then
         call system('ls '//path//' > '//temp_file_name, status=status)
      else
         !! /B = Bare format (no heading, file sizes or summary).
         call system('dir '//path//' /B > '//temp_file_name, status=status)
      end if
      if (status .ne. 0) then
         call exit_error('Cannot execute the command to read the content of directory'//path)
      end if
      unit = open (temp_file_name)
      rows_count = number_of_rows(unit)
      allocate (content(rows_count), source=string_type())
      do i = 1, rows_count
         read (unit, *) buffer
         content(i) = trim(buffer)
      end do
      close (unit, status='delete')
   end function

   !> Count the number of rows of a unit by looping until the end
   integer function number_of_rows(unit) result(row_count)
      integer, intent(in)::unit
      integer :: ios

      rewind (unit)
      row_count = 0
      do
         read (unit, *, iostat=ios)
         if (ios /= 0) exit
         row_count = row_count + 1
      end do

      rewind (unit)

   end function number_of_rows

   type(string_type) function join(strings, separator) result(joined)
      type(stringlist_type), intent(in) :: strings
      character(*), intent(in), optional :: separator

      integer :: i
      character(:), allocatable :: set_separator

      if (present(separator)) then
         set_separator = separator
      else
         set_separator = ", "
      end if

      if (strings%len() .eq. 0) then
         joined = ""
         return
      end if
      joined = strings%get(fidx(1))
      do i = 2, strings%len()
         joined = joined//set_separator//strings%get(fidx(i))
      end do
   end function

   logical function try_parse_int(str, parsed) result(success)
      character(*), intent(in) :: str
      integer, intent(out) :: parsed
      integer :: iostat

      if (trim(str) == "") then
         success = .true.
         parsed = 0.
         return
      end if
      read (str, *, iostat=iostat) parsed
      success = iostat .eq. 0
   end function

   logical function try_parse_real(str, parsed) result(success)
      character(*), intent(in) :: str
      real, intent(out) :: parsed
      integer :: iostat

      if (trim(str) == "") then
         success = .true.
         parsed = 0.
         return
      end if
      read (str, *, iostat=iostat) parsed
      success = iostat .eq. 0
   end function

   subroutine delete_file(path)
      character(*), intent(in)  :: path
      integer unit, iostat
      logical exist
      inquire (file=path, exist=exist)
      if (.not. exist) then
         return
      end if
      open (newunit=unit, file=path, status='old', iostat=iostat)
      if (iostat .ne. 0) then
         call exit_error("Cannot delete the file ["//path//"]")
      end if
      close (unit, status='delete')
   end subroutine

   pure function split_string(string, separator) result(result)
      character(*), intent(in) :: string
      character(*), intent(in) :: separator

      type(string_type), allocatable :: result(:)
      integer :: i

      character(:), allocatable :: tokens(:)
      call split(string, separator, tokens)

      allocate (result(size(tokens)), source=string_type())
      do i = 1, size(tokens)
         result(i) = adjustl(trim(tokens(i)))
      end do
   end function

   pure function percent_str(value) result(str)
      real, intent(in) :: value
      character(:), allocatable :: str

      str = trim(adjustl(to_string(value*100, "F10.3")//"%"))
   end function

   !> Returns true if the string is a blank space separated list of numbers, false othewise
   logical function only_numbers(string) result(res)
      character(*), intent(in) :: string
      type(string_type), allocatable :: tokens(:)
      integer :: i
      real :: dummy
      tokens = split_string(string, " ")
      if (size(tokens) .eq. 0) then
         res = .false.
         return
      end if
      do i = 1, size(tokens)
         if (try_parse_real(char(tokens(i)), dummy) .eqv. .false.) then
            res = .false.
            return
         end if
      end do
      res = .true.
   end function
end module utils
