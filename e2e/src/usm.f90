module usm_m
   use stdlib_io
   use utils
   implicit none
   type usm_
      character(:), allocatable :: name
      integer :: plant_count
      integer :: successive
   end type
contains

   type(usm_) function get_usm_from_file(path) result(usm)
      character(*), intent(in) :: path
      logical :: exist
      integer :: unit, iostat
      character(len=255) :: buffer

      inquire (file=path, exist=exist)
      if (.not. exist) then
         call exit_error("Usm file doesn't exist: ["//path//"]")
      end if
      unit = open (path, 'r', iostat)
      if (iostat .ne. 0) then
         call exit_error("Error opening usm file: ["//path//"]")
      end if

      read (unit, *) buffer
      read (unit, *) buffer
      read (unit, *) buffer
      read (unit, *) buffer
      read (unit, *) buffer
      read (unit, *) usm%successive
      read (unit, *) buffer
      read (unit, *) usm%plant_count

      read (unit, *) buffer
      read (unit, *) buffer
      usm%name = adjustl(trim(buffer))

      close (unit)
   end function

end module
