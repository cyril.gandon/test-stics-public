module mod_rapport_tests
   use mod_rapport_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   use utils

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_parse_file", test_parse_file) &
                  ]

   end subroutine

   subroutine check_array(error, actual, expected)
      real, intent(in):: actual(:), expected(:)
      type(error_type), allocatable, intent(out) :: error
      integer :: i

      call check(error, size(actual), size(expected))

      do i = 1, size(actual)
         call check(error, actual(i), expected(i))
      end do
   end subroutine check_array

   subroutine test_parse_file(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: path
      type(mod_rapport_) :: report
      character(len=1) :: sep
      character(len=255) :: cwd
      character(:), allocatable :: expected_headers
      real :: expected_values(4, 29)

      expected_values = 0.
      sep = filesep()

      call getcwd(cwd)
      path = trim(cwd)//sep//'src'//sep//'data_tests'//sep//'mod_rapport_test.sti'

      report = parse_file(path)

      expected_headers =&
      &"masec(n);mafruit;iflos;imats;irecs;laimax;cprecip;cet;QNplante;Qles;lai(n);&
      &azomes;resmes;hauteur;cumraint;cumrg;fapar;tcult;&
      &ilevs;cep;inn;swfac;turfac;densite;densiteequiv;Qminh;Qminr;Qdrain;zrac"
      call check(error, size(report%rows), 4)
      call check(error, size(report%headers), 29)
      call check(error, char(join(stringlist_type(report%headers), ";")), expected_headers)

      call check(error, report%P_usm, "banana")
      call check(error, report%wlieu, "climbanj")
      call check(error, report%nomversion, "10.0_r3388_2022-10-26")
      call check(error, report%P_ichsl, 1)
      call check(error, report%P_codeplante, "ban")

      call check(error, report%rows(1)%ansemis, 1996)
      call check(error, report%rows(2)%ansemis, 1997)
      call check(error, report%rows(3)%ansemis, 1998)
      call check(error, report%rows(4)%ansemis, 1999)

      call check(error, report%rows(1)%P_iwater, 30)
      call check(error, report%rows(2)%P_iwater, 31)
      call check(error, report%rows(3)%P_iwater, 32)
      call check(error, report%rows(4)%P_iwater, 33)

      call check(error, report%rows(1)%ancours, 2000)
      call check(error, report%rows(2)%ancours, 2001)
      call check(error, report%rows(3)%ancours, 2002)
      call check(error, report%rows(4)%ancours, 2003)

      call check(error, report%rows(1)%ifin, 33)
      call check(error, report%rows(2)%ifin, 192)
      call check(error, report%rows(3)%ifin, 264)
      call check(error, report%rows(4)%ifin, 300)

      call check(error, report%rows(1)%nbdays, 4)
      call check(error, report%rows(2)%nbdays, 163)
      call check(error, report%rows(3)%nbdays, 235)
      call check(error, report%rows(4)%nbdays, 271)

      call check(error, report%rows(1)%group, 0)
      call check(error, report%rows(2)%group, 1)
      call check(error, report%rows(3)%group, 2)
      call check(error, report%rows(4)%group, 3)

      call check(error, report%rows(1)%stade, "lev")
      call check(error, report%rows(2)%stade, "flo")
      call check(error, report%rows(3)%stade, "recphy")
      call check(error, report%rows(4)%stade, "end")

      
      expected_values(1, :) = (/0.500, 0.000, 0.000, 0.000, 0.000, 0.100, 2.700, 6.521, 0.003, &
                                0.000, 0.100, 106.300, 257.745, 0.356, 0.482, 16.800, 0.060, 25.290, 33.000, 0.066, &
                                0.000, 0.225, 0.200, 0.180, 0.180, 4.896, -15.270, 0.000, 22.592/)
      expected_values(2, :) = (/3.556, 0.000, 192.000, 0.000, 0.000, 2.603, 597.648, &
                                453.611, 97.708, 0.000, 2.603, 273.522, 267.300, 2.140, 349.642, 2863.900, 0.775, &
                                28.150, 33.000, 55.868, 1.255, 1.000, 1.000, 0.180, 0.180, 191.870, -4.981, 0.000, 80.000/)
      expected_values(3, :) = (/7.120, 3.560, 192.000, 264.000, 264.000, 2.603, &
                                1063.989, 679.933, 164.022, 20.477, 0.013, 150.169, 267.300, 0.000, 837.353, &
                        4210.301, 0.008, 27.250, 33.000, 179.361, 1.361, 1.000, 1.000, 0.180, 0.180, 278.751, -4.961, 0.000, 0.000/)
      expected_values(4, :) = (/7.120, 3.560, 192.000, 264.000, 264.000, 2.603, &
                                1063.989, 679.933, 164.022, 100.793, 0.000, 150.909, 267.300, 0.000, 837.353, &
                        4210.301, 0.000, 25.300, 33.000, 179.361, 1.361, 1.000, 1.000, 0.180, 0.180, 319.140, -1.579, 0.000, 0.000/)

      call check_array(error, report%rows(1)%values, expected_values(1, :))
   end subroutine
end module
