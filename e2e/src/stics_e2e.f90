module stics_e2e
   use stdlib_string_type
   use stdlib_stringlist_type
   use stdlib_strings, only: to_string, starts_with
   use fortran202x_split
   use mod_rapport_m

   use utils
   use usm_m

   implicit none

   character(*), parameter :: actual_output_folder = "actual_outputs"
   character(*), parameter :: expected_output_folder = "expected_outputs"
   real, parameter :: ERROR_THRESHOLD = 0.01 ! 1%
contains

   subroutine run()
      character(:), allocatable :: stics_exe, e2e_main_dir, flags, usms_folder
      type(stringlist_type) :: usms, failed_usms

      character(len=255) :: current_working_directory
      logical :: e2e_success
      !! file separator
      character(1) :: s
      integer :: usm_row_index

      s = filesep()

      call getcwd(current_working_directory)
      e2e_main_dir = trim(current_working_directory)
      stics_exe = e2e_main_dir//s//"bin"//s//"Stics"

      call write_screen("Starting E2E in directory "//e2e_main_dir)

      call chdir(".."//s//"pre_stics")
      call execute_or_exit("fpm run")
      call chdir(".."//s//"stics")

      call which("gfortran")
      call execute_or_exit("gfortran -v")

      flags = "-fcheck=no-bounds,no-array-temps -Wall -Wextra -Wimplicit-interface -fPIC &
      &-fmax-errors=1 -g -fbacktrace -fcoarray=single -fimplicit-none"
      call execute_or_exit("fpm install --verbose --prefix "//e2e_main_dir//' --flag "'//flags//'"')

      e2e_success = .true.

      call write_screen("Stics executable path = "//stics_exe)

      usms_folder = e2e_main_dir//s//"usms"
      call write_screen("Reading usms from folder ["//usms_folder//"]")
      usms = stringlist_type(read_dir_content(e2e_main_dir//s//"usms"))
      call write_screen("Found "//to_string(usms%len())//" usms : "//char(join(usms)))
      if (usms%len() .lt. 5) then
         ! Ensure we have read at least some usms
         ! Protect against problem in reading directory list
         call exit_error("❌ Error: Too few usms read, lower than 5. Expected more than 5 test cases.")
      end if
      do usm_row_index = 1, usms%len()
         block
            type(usm_) :: usm
            character(:), allocatable :: usm_name, usm_dir
            logical :: file_success

            usm_name = char(usms%get(fidx(usm_row_index)))
            usm_dir = usms_folder//s//usm_name
            usm = get_usm_from_file(usm_dir//s//"new_travail.usm")
            file_success = run_stics_and_check_usm(usm_dir, stics_exe, usm)
            if (file_success .eqv. .false.) then
               failed_usms = failed_usms//usm_name
            end if
         end block
      end do

      if (failed_usms%len() .eq. 0) then
         call write_screen("✅ End to end tests have succeeded. ✨")
      else
         call exit_error("❌ End to end tests have failed! "//to_string(failed_usms%len()) &
            //"/"//to_string(usms%len())//" USMs have failed: ("//char(join(failed_usms))//")")
      end if

   end subroutine run

   logical function run_stics_and_check_usm(usm_dir, stics_exe, usm) result(success)
      character(*), intent(in) :: stics_exe, usm_dir
      type(usm_), intent(in) :: usm

      logical :: test_file_exists_result, test_file_identic_result
      integer :: usm_row_index, chdir_status, stics_run_exitstat
      !! file separator
      character(1) :: s

      s = filesep()
      call write_screen("Starting usm "//usm_dir//"...")
      chdir_status = chdir(usm_dir)
      if (chdir_status .ne. 0) then
         call exit_error("chdir("//usm_dir//") command failed with status = "//to_string(chdir_status))
      end if

      !> Clean the output folder before generating a new run
      call delete_output_files(usm_dir)

      call execute_or_exit(stics_exe//" --output-path="//actual_output_folder)

      call write_screen("Run of usm "//usm%name//" finished, comparing outputs...")

      test_file_exists_result = test_file_exists(usm_dir)
      test_file_identic_result = test_file_identic(usm_dir)
      success = test_file_exists_result .and. test_file_identic_result
   end function

   subroutine delete_output_files(usm_dir)
      character(*), intent(in) :: usm_dir

      integer :: i_file
      character(:), allocatable :: folder, filepath
      type(string_type), allocatable :: files(:)
      character(1) :: s
      logical :: exist
      type(usm_) :: usm
      s = filesep()

      folder = usm_dir//s//actual_output_folder

      usm = get_usm_from_file(usm_dir//s//"new_travail.usm")

      call write_screen("Cleaning folder ["//folder//"]...")
      inquire (file=folder, exist=exist)
      if (exist .eqv. .false.) then
         ! Folder does not exist, nothing to clean..
         return
      end if
      files = read_dir_content(folder)
      do i_file = 1, size(files)
         ! Successive case, reading recup.tmp file
         if ((usm%successive == 1) .AND. &
         (char(files(i_file)) == "recup.tmp")) continue
         filepath = folder//s//char(files(i_file))
         call delete_file(filepath)
      end do
   end subroutine

   logical function test_file_identic(usm_dir) result(success)
      character(*), intent(in) :: usm_dir

      type(stringlist_type) :: mod_rapport_errors
      type(string_type), allocatable :: output_files(:), expected_outputs(:), actual_outputs(:)
      character(:), allocatable :: output_filename, actual_filepath, expected_filepath
      integer :: output_files_index
      logical :: output_file_exist, compare_files_success, has_compared
      !! file separator
      character(1) :: s
      s = filesep()
      output_files = read_dir_content(usm_dir//s//expected_output_folder)
      success = .true.
      do output_files_index = 1, size(output_files)
         has_compared = .true.

         output_filename = char(output_files(output_files_index))

         expected_filepath = expected_output_folder//s//output_filename
         actual_filepath = actual_output_folder//s//output_filename
         if (try_load_file(expected_filepath, expected_outputs) .eqv. .false.) then
            call write_screen("❌ Error ["//expected_filepath//"] does not exists")
            success = .false.
         end if
         if (try_load_file(actual_filepath, actual_outputs) .eqv. .false.) then
            call write_screen("❌ Error ["//actual_filepath//"] does not exists")
            success = .false.
         end if
         if (starts_with(output_filename, "mod_s")) then
            compare_files_success = compare_mod_s_files(actual_outputs, expected_outputs)
         else if (starts_with(output_filename, "mod_profil")) then
            compare_files_success = compare_mod_profil_files(actual_outputs, expected_outputs)
         else if (starts_with(output_filename, "mod_rapport_AgMIP")) then
            !! Ignore this file for comparaison
            has_compared = .false.
            compare_files_success = .true.
         else if (starts_with(output_filename, "mod_rapportA_AgMIP")) then
            !! Ignore this file for comparaison
            has_compared = .false.
            compare_files_success = .true.
         else if (starts_with(output_filename, "mod_rapportP_AgMIP")) then
            !! Ignore this file for comparaison
            has_compared = .false.
            compare_files_success = .true.
         else if (starts_with(output_filename, "mod_rapport")) then
            block
               type(mod_rapport_) actual_report, expected_report
               actual_report = parse_file(actual_filepath)
               expected_report = parse_file(expected_filepath)
               mod_rapport_errors = compare_mod_rapport_files(expected_report, actual_report, ERROR_THRESHOLD)
               if (mod_rapport_errors%len() .gt. 0) then
                  call write_screen_messages(mod_rapport_errors)
                  compare_files_success = .false.
               end if
            end block
         else if (starts_with(output_filename, "mod_b")) then
            compare_files_success = compare_files(actual_outputs, expected_outputs)
         else
            !! Ignore this file for comparaison
            has_compared = .false.
            compare_files_success = .true.
         end if

         if (has_compared) then
            if (compare_files_success) then
               call write_screen("✅ File ["//actual_filepath//"] is the same as expected file ["//expected_filepath//"]")
            else
               call write_screen("❌ Error ["//output_filename//"]: File ["//actual_filepath// &
                                 "] is DIFFERENT from expected file ["//expected_filepath//"]")
            end if
         else
            call write_screen("🛈 File ["//actual_filepath//"] not comparable, skipping..")
         end if
         success = success .and. compare_files_success
      end do
   end function

   !> Read the folder expected_outputs, and check that eveything exists in the actual_outputs folder
   logical function test_file_exists(usm_dir) result(success)
      character(*), intent(in) :: usm_dir

      type(string_type), allocatable :: output_files(:)
      character(:), allocatable :: output_file_name
      integer :: output_files_index
      logical :: output_file_exist
      character(:), allocatable :: path
      !! file separator
      character(1) :: s

      s = filesep()

      output_files = read_dir_content(usm_dir//s//expected_output_folder)

      success = .true.
      do output_files_index = 1, size(output_files)
         output_file_name = char(output_files(output_files_index))
         path = actual_output_folder//s//output_file_name
         inquire (file=path, exist=output_file_exist)
         if (output_file_exist) then
            call write_screen("✅ File ["//path//"] exists.")
         else
            call write_screen(path//" does not exist.")
            call write_screen("❌ Error: File ["//path//"] does not exist!")
            success = .false.
         end if
      end do
   end function

   logical function compare_mod_profil_files(actual, expected) result(success)
      type(string_type), allocatable, intent(in) :: actual(:), expected(:)
      character(:), allocatable :: columns(:)
      character(:), allocatable :: first_row
      integer :: i, row_index
      type(stringlist_type) :: compare_lines_errors, compare_header_errors
      type(string_type) :: actual_row, expected_row
      type(string_type), allocatable :: actual_split(:), expected_split(:)

      first_row = char(expected(1))

      call split(first_row, ';', columns)

      success = .true.
      if (size(actual) .ne. size(expected)) then
         call write_screen('compare_files error: actual length = '//to_string(size(actual))// &
                           ' - expected length = '//to_string(size(expected)))
         success = .false.
         return
      end if

      do row_index = 1, size(actual)
         actual_row = actual(row_index)
         expected_row = expected(row_index)
         if (only_numbers(char(expected_row))) then
            actual_split = split_string(char(actual_row), " ")
            expected_split = split_string(char(expected_row), " ")
            compare_lines_errors = compare_real_lines(actual_split, expected_split, ERROR_THRESHOLD)
            if (compare_lines_errors%len() .gt. 0) then
               call write_screen('difference on line = '//to_string(row_index))
               call write_screen(char(actual_row))
               call write_screen(char(expected_row))

               call write_screen_messages(compare_lines_errors)
               success = .false.
            end if
         else
            compare_header_errors = compare_lines(actual_row, expected_row)
            if (compare_header_errors%len() .gt. 0) then
               call write_screen('difference on header lines number '//to_string(row_index))
               call write_screen(char(actual_row))
               call write_screen(char(expected_row))

               call write_screen_messages(compare_header_errors)
               success = .false.
            end if
         end if
      end do
   end function

   logical function compare_mod_s_files(actual, expected) result(success)
      type(string_type), allocatable, intent(in) :: actual(:), expected(:)
      integer :: i, row_index
      type(stringlist_type) :: compare_lines_errors
      type(string_type) :: actual_row, expected_row
      type(string_type) :: actual_code, expected_code

      type(string_type), allocatable :: actual_split(:), expected_split(:)
      type(string_type), allocatable :: actual_integers(:), expected_integers(:)
      type(string_type), allocatable :: actual_reals(:), expected_reals(:)

      success = .true.
      if (size(actual) .ne. size(expected)) then
         call write_screen('compare_files error: actual length = '//to_string(size(actual))// &
                           ' - expected length = '//to_string(size(expected)))
         success = .false.
         return
      end if

      if(size(actual) .eq. 0) then
         !! Everything is empty
         success = .true.
         return
      end if

      !! Compare headers
      compare_lines_errors = compare_lines(actual(1), expected(1))
      if (compare_lines_errors%len() .gt. 0) then
         call write_screen('difference on line = '//to_string(1))
         call write_screen(char(actual_row))
         call write_screen(char(expected_row))

         call write_screen_messages(compare_lines_errors)
         success = .false.
      end if

      do row_index = 2, size(actual)
         actual_row = actual(row_index)
         expected_row = expected(row_index)
         actual_split = split_string(char(actual_row), ";")
         expected_split = split_string(char(expected_row), ";")

         !! day, month, year, julian
         actual_integers = actual_split(1:4)
         expected_integers = expected_split(1:4)
         compare_lines_errors = compare_integer_lines(actual_integers, expected_integers)
         if (compare_lines_errors%len() .gt. 0) then
            call write_screen('difference on line = '//to_string(row_index))
            call write_screen(char(actual_row))
            call write_screen(char(expected_row))

            call write_screen_messages(compare_lines_errors)
            success = .false.
         end if

         !! plant code
         actual_code = actual_split(5)
         expected_code = expected_split(5)
         if (actual_code .ne. expected_code) then
            call write_screen('difference on line = '//to_string(row_index))
            call write_screen(char(actual_code))
            call write_screen(char(expected_code))
            success = .false.
         end if

         !! all others values that should be real
         actual_reals = actual_split(6:size(actual_split))
         expected_reals = expected_split(6:size(expected_split))

         compare_lines_errors = compare_real_lines(actual_reals, expected_reals, ERROR_THRESHOLD)
         if (compare_lines_errors%len() .gt. 0) then
            call write_screen('difference on line = '//to_string(row_index))
            call write_screen(char(actual_row))
            call write_screen(char(expected_row))

            call write_screen_messages(compare_lines_errors)
            success = .false.
         end if
      end do
   end function
end module stics_e2e
