module utils_tests
   use stdlib_string_type
   use stdlib_stringlist_type
   use utils
   use testdrive, only: error_type, unittest_type, new_unittest, check
   implicit none

contains
   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_compare_lines", test_compare_lines) &
                  , new_unittest("test_read_dir_content", test_read_dir_content) &
                  , new_unittest("test_join", test_join) &
                  , new_unittest("test_try_parse_lines", test_try_parse_lines) &
                  , new_unittest("test_filter_empty", test_filter_empty) &
                  , new_unittest("test_relative_difference", test_relative_difference) &
                  , new_unittest("test_percent_str", test_percent_str) &
                  , new_unittest("test_only_numbers", test_only_numbers) &
                  ]

   end subroutine

   subroutine test_compare_lines(error)
      type(error_type), allocatable, intent(out) :: error
      type(string_type) :: left, right
      type(stringlist_type) :: errors
      logical :: idx
      left = "toto"
      right = "tata"
      errors = compare_lines(left, right)
      call check(error, errors%len(), 2)

      left = "toto"
      right = "toto"
      errors = compare_lines(left, right)
      call check(error, errors%len(), 0)

      left = "toto"
      right = "abc"
      errors = compare_lines(left, right)
      call check(error, errors%len(), 1)
   end subroutine

   subroutine test_read_dir_content(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: path
      character(len=1) :: sep
      character(len=255) :: cwd
      type(string_type), allocatable :: content(:)

      sep = filesep()

      call getcwd(cwd)
      path = trim(cwd)//sep//'src'//sep//'data_tests'

      content = read_dir_content(path)

      call check(error, size(content), 4)
      call check(error, char(content(1)), "dummy_folder1")
      call check(error, char(content(2)), "dummy_folder2")
      call check(error, char(content(3)), "mod_rapport_test.sti")
      call check(error, char(content(4)), "new_travail_test.usm")


   end subroutine

   subroutine test_join(error)
      type(error_type), allocatable, intent(out) :: error
      type(stringlist_type) :: strings

      call check(error, char(join(strings)), "")

      strings = strings // "first"
      call check(error, char(join(strings)), "first")

      strings = strings // "second"
      call check(error, char(join(strings)), "first, second")
   end subroutine

   subroutine test_try_parse_lines(error)
      type(error_type), allocatable, intent(out) :: error
      real, allocatable :: parsed(:)
      logical :: success
      integer :: i
      success = try_parse_lines("   80    0.00000    0.00000    0.00000    0.00000    0.00000    0.00000    ", " ", parsed)
      call check(error, success, .true.)
      call check(error, size(parsed), 7)
      call check(error, parsed(1), 80.)
      call check(error, parsed(2), 0.)
      call check(error, parsed(3), 0.)
      call check(error, parsed(4), 0.)
      call check(error, parsed(5), 0.)
      call check(error, parsed(6), 0.)
      call check(error, parsed(7), 0.)
   end subroutine

   subroutine test_filter_empty(error)
      type(error_type), allocatable, intent(out) :: error
      real, allocatable :: parsed(:)
      logical :: success
      integer :: i
      type(string_type), allocatable :: filtered(:)
      character(:), allocatable :: list(:)

      list = (/"  ", " 1", " 2", "  ", "3 ", "  "/)
      filtered = filter_empty(list)
      call check(error, size(filtered), 3)
      call check(error, char(filtered(1)), " 1")
      call check(error, char(filtered(2)), " 2")
      call check(error, char(filtered(3)), "3 ")
   end subroutine

   subroutine test_relative_difference(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, relative_difference(1., 1.), 0.)

      call check(error, relative_difference(1., 2.), 1.)
      call check(error, relative_difference(1., 3.), 2.)

      call check(error, relative_difference(2., 1.), 0.5)

      call check(error, relative_difference(0., 0.), 0.)
      call check(error, relative_difference(1., 0.), 1.)
      call check(error, relative_difference(2., 0.), 1.)

      call check(error, relative_difference(0., 1.), 1.)
      call check(error, relative_difference(0., 2.), 1.)

      call check(error, relative_difference(2.2, 2.3), 0.0454545021)
   end subroutine

   subroutine test_percent_str(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, percent_str(1.), "100.000%")
      call check(error, percent_str(2.), "200.000%")

      call check(error, percent_str(5.), "500.000%")
      call check(error, percent_str(0.5), "50.000%")
      call check(error, percent_str(0.05), "5.000%")
      call check(error, percent_str(0.005), "0.500%")
      call check(error, percent_str(0.0005), "0.050%")
   end subroutine
   
   subroutine test_only_numbers(error)
      type(error_type), allocatable, intent(out) :: error
      call check(error, only_numbers(""), .false.)
      call check(error, only_numbers("toto"), .false.)
      call check(error, only_numbers("1"), .true.)
      call check(error, only_numbers("1 2 3"), .true.)
      call check(error, only_numbers("1 2 3 toto"), .false.)
   end subroutine
   
end module

