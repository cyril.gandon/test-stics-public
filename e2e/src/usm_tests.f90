module usm_tests
   use usm_m
   use testdrive, only: error_type, unittest_type, new_unittest, check
   use utils

   implicit none
contains

   subroutine collect(testsuite)
      type(unittest_type), allocatable, intent(out) :: testsuite(:)

      testsuite = [ &
                  new_unittest("test_get_usm_from_file", test_get_usm_from_file) &
                  ]

   end subroutine

   subroutine test_get_usm_from_file(error)
      type(error_type), allocatable, intent(out) :: error
      character(:), allocatable :: path
      type(usm_) :: usm
      character(len=1) :: sep
      character(len=255) :: cwd
      sep = filesep()

      call getcwd(cwd)
      path = trim(cwd)//sep//'src'//sep//'data_tests'//sep//'new_travail_test.usm'

      usm = get_usm_from_file(path)

      call check(error, 2, usm%plant_count)
      call check(error, "TEST_NAME_USM", usm%name)
   end subroutine
end module
