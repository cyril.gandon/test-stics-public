module mod_rapport_m
   use stdlib_string_type
   use utils

   implicit none

   !> Structure of a single row of the output file mod_rapport
   type mod_rapport_row_
      integer :: ansemis
      integer :: P_iwater
      integer :: ancours
      integer :: ifin
      integer :: nbdays
      integer :: group
      character(:), allocatable :: stade

      ! List of variable data that comes from rap.mod
      real, allocatable :: values(:)
   end type

   !> Structure of the output file mod_rapport, see Ecritue_Rapport.f90
   type mod_rapport_
      type(mod_rapport_row_), allocatable :: rows(:)
      type(string_type), allocatable :: headers(:)
      character(:), allocatable :: nomversion
      character(:), allocatable :: P_usm
      character(:), allocatable :: wlieu
      integer :: P_ichsl
      character(:), allocatable :: P_codeplante
   end type
contains

   type(mod_rapport_) function parse_file(path) result(report)
      character(*), intent(in) :: path
      logical :: exist
      integer :: unit, iostat, rows_count, i, j, column_index, values_count
      character(len=4096) :: buffer
      type(string_type), allocatable :: split_buffer(:)
      logical :: success

      !> Number of fixed data in the file
      integer, parameter :: fixed_header_count = 12

      inquire (file=path, exist=exist)
      if (.not. exist) then
         call exit_error("Report file doesn't exist: ["//path//"]")
      end if
      unit = open (path, 'r', iostat)
      if (iostat .ne. 0) then
         call exit_error("Error opening report file: ["//path//"]")
      end if

      rows_count = number_of_rows(unit)
      if (rows_count .eq. 0) then
         call exit_error("Cannot parse file ["//path//"]: file has no row")
      end if

      ! Allocate minus 1 for counting the first line as the header
      allocate (report%rows(rows_count - 1))

      ! headers row
      read (unit, '(A)') buffer

      split_buffer = split_string(buffer, ";")

      !> Get all variables headers
      report%headers = split_buffer(fixed_header_count + 1:size(split_buffer))
      if (size(report%headers) .eq. 0) then
         call exit_error("Cannot parse file ["//path//"]: not enough values to parse!")
      end if
      do i = 1, rows_count - 1
         column_index = 0
         read (unit, '(A)') buffer
         split_buffer = split_string(buffer, ";")

         column_index = column_index + 1
         report%P_usm = char(trim(split_buffer(column_index)))

         column_index = column_index + 1
         report%wlieu = char(trim(split_buffer(column_index)))

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%rows(i)%ansemis)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [ansemis] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%rows(i)%P_iwater)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [P_iwater] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%rows(i)%ancours)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [ancours] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%rows(i)%ifin)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [ifin] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%rows(i)%nbdays)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [nbdays] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%P_ichsl)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [P_ichsl] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         success = try_parse_int(char(split_buffer(column_index)), report%rows(i)%group)
         if (success .eqv. .false.) then
            call exit_error("Cannot parse [group] in file ["//path//"] on line ["//to_string(i)//"]")
         end if

         column_index = column_index + 1
         report%P_codeplante = char(trim(split_buffer(column_index)))

         column_index = column_index + 1
         report%rows(i)%stade = char(trim(split_buffer(column_index)))

         column_index = column_index + 1
         report%nomversion = char(trim(split_buffer(column_index)))

         !> Each values row should be the same size as the headers
         allocate (report%rows(i)%values(size(report%headers)))
         do j = 1, size(report%headers)
            success = try_parse_real(char(split_buffer(fixed_header_count + j)), report%rows(i)%values(j))
            if (success .eqv. .false.) then
               call exit_error("Cannot parse column ["//to_string(j)//"] &
&                   in file ["//path//"] on line ["//to_string(i)//"]")
            end if
         end do
      end do
   end function

   !> Parse the file mod_rapport and compare values
   type(stringlist_type) function compare_mod_rapport_files(expected, actual, error_threshold) result(errors)
      type(mod_rapport_), intent(in) :: expected, actual
      real, intent(in) :: error_threshold

      type(stringlist_type) :: compare_lines_errors
      type(string_type) :: actual_headers_str, expected_headers_str
      integer :: row_index

      actual_headers_str = join(stringlist_type(actual%headers))
      expected_headers_str = join(stringlist_type(actual%headers))

      errors = compare_lines(actual_headers_str, expected_headers_str)

      if (size(actual%rows) .ne. size(expected%rows)) then
         errors = errors // ("Error in mod_rapport files: actual rows size is ["//to_string(size(actual%rows))//"] &
&          while expected rows size is ["//to_string(size(expected%rows))//"]")
         return
      end if

      if (actual%nomversion .ne. expected%nomversion) then
         errors = errors // "nomversion are different"
      end if
      if (actual%P_codeplante .ne. expected%P_codeplante) then
         errors = errors //  "P_codeplante are different"
      end if
      if (actual%P_ichsl .ne. expected%P_ichsl) then
         errors = errors // "P_ichsl are different"
      end if
      if (actual%P_usm .ne. expected%P_usm) then
         errors = errors // "P_ichsl are different"
      end if
      if (actual%wlieu .ne. expected%wlieu) then
         errors = errors // "wlieu are different"
      end if
      do row_index = 1, size(actual%rows)
         errors = errors // &
         compare_mod_rapport_rows(expected%rows(row_index), actual%rows(row_index), actual%headers, error_threshold)
      end do
   end function

   type(stringlist_type)  function compare_mod_rapport_rows(expected, actual, headers, error_threshold) result(errors)
      type(mod_rapport_row_), intent(in) :: expected, actual
      type(string_type), allocatable, intent(in) :: headers(:)
      real, intent(in) :: error_threshold
      integer :: value_index
      real :: actual_value, expected_value, diff

      if (actual%ancours .ne. expected%ancours) then
         errors = errors // "ancours are different"
      end if
      if (actual%ansemis .ne. expected%ansemis) then
         errors = errors // "ansemis are different"
      end if
      if (actual%group .ne. expected%group) then
         errors = errors // "group are different"
      end if
      if (actual%ifin .ne. expected%ifin) then
         errors = errors // "ifin are different"
      end if
      if (actual%nbdays .ne. expected%nbdays) then
         errors = errors // "nbdays are different"
      end if
      if (actual%P_iwater .ne. expected%P_iwater) then
         errors = errors // "P_iwater are different"
      end if
      if (actual%stade .ne. expected%stade) then
         errors = errors // "stade are different"
      end if

      if (size(actual%values) .ne. size(expected%values)) then
         errors = errors // ("Error in mod_rapport files: actual values size is ["// &
          to_string(size(actual%values))//"] &
&          while expected values size is ["//to_string(size(expected%values))//"]")
         return
      end if

      do value_index = 1, size(actual%values)
         expected_value = expected%values(value_index)
         actual_value = actual%values(value_index)

         diff = relative_difference(expected_value, actual%values(value_index))
         if (diff .ge. error_threshold) then
            errors = errors // ("Value ["//char(headers(value_index))//"]:"// &
                              "  ["//to_string(expected_value)//"] != ["//to_string(actual_value)//"]"// &
                              " relative difference = "//percent_str(diff))
         end if
      end do
   end function
end module
