program tester
   use, intrinsic :: iso_fortran_env, only: error_unit
   use testdrive, only: run_testsuite, testsuite_type, new_testsuite
   use utils_tests, only: collect_utils_tests => collect
   use usm_tests, only: collect_usm_tests => collect
   use mod_rapport_tests, only: collect_mod_rapport_tests => collect

   integer :: stat, is
   type(testsuite_type), allocatable :: testsuites(:)

   testsuites = [ &
                new_testsuite("utils_tests", collect_utils_tests) &
                , new_testsuite("usm_tests", collect_usm_tests) &
                , new_testsuite("mod_rapport_tests", collect_mod_rapport_tests) &
                ]

   stat = 0
   do is = 1, size(testsuites)
      write (error_unit, '("#", *(1x, a))') "Testing:", testsuites(is)%name
      call run_testsuite(testsuites(is)%collect, error_unit, stat)
   end do

   if (stat > 0) then
      write (error_unit, '(i0, 1x, a)') stat, "test(s) failed!"
      error stop
   end if

end program tester
